<?php

defined('BASEPATH') or exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
 */
$route['default_controller'] = 'auth';
$route['404_override'] = 'my404';

// return constants for use in /calendar/old/kalender/bdd.php
$route['return_constants/(:any)']['GET'] = 'home/return_db_env/$1';

$route['api/v1/counselors']['get'] = "api/get_pa";
$route['api/v1/payment/status']['get'] = "api/payment/$1";
$route['api/v1/schedule/(:num)/day']['get'] = "api/schedule/$1";
$route['api/v1/payment/briva']['get'] = "api/briva";
$route['api/v1/examination/(:num)/type']['get'] = "api/examination/$1";
$route['api/v1/transcript']['get'] = "api/transcript";
$route['api/v1/studycardresult']['get'] = "api/get_khs";

$route['api/v1/tahunakademik']['get'] = "api/tahunakademik";
$route['api/v1/tahunakademik/(:num)']['get'] = "api/tahunakademik/$1";
// prodi
$route['api/v1/prodi']['get'] = "api/prodi";
$route['api/v1/prodi/(:num)']['get'] = "api/prodi/$1";
// Students
$route['api/v1/students']['get'] = "api/students";
$route['api/v1/students/(:num)']['get'] = "api/students/$1";
$route['api/v1/student/(:num)']['get'] = "api/student/$1";
$route['api/v1/student/(:num)/prodi']['get'] = "api/prodi_student/$1";
$route['api/v1/kelas-mhs']['post'] = "api/kelas-mhs";

$route['api/v1/user']['post'] = "api/user";

// Dosen
$route['api/v1/dosen/(:num)']['get'] = "api/dosen/$1";
$route['api/v1/dosen-ajar-prodi']['post'] = "api/dosenAjarTaProdi";
$route['api/v1/dosen-ajar-ta']['post'] = "api/dosenAjarTa";


$route['v1/rooms/list'] = "api/booking/rooms";
$route['v1/(:num)/rooms'] = "api/booking/detailRoom/$1";
$route['api/v1/usedroom'] = "api/booking/usedRoomList";
// api for perpus
$route['api/v1/getAllStudents']['GET'] = 'api/api_perpus/getAllStudents';
$route['api/v1/getDetailStudent']['GET'] = 'api/api_perpus/getDetailStudent';
// api for sarpras
$route['api/v1/sarpras/login']['POST'] = 'api/api_sarpras/attemp_login';
$route['translate_uri_dashes'] = TRUE;

/**
| -------------------------------------------------------------------------
| API For Unit
| -------------------------------------------------------------------------
| 
| List All Units
| Get All Units from database db_siakadlive on tbl_divisi
| Examples : host/api/v1/unit
|
| Detail Unit By Kode
| Get Units from database db_siakadlive on tbl_divisi by kd_divisi
| Examples : host/api/v1/unit?kode=[kode unit]
|
| Detail Unit By ID
| Get Units from database db_siakadlive on tbl_divisi by id_divisi
| Examples : host/api/v1/unit/:id
|
 */
$route['api/v1/unit']['GET'] = 'api/api_units/getUnits';
// GET Unit By ID
$route['api/v1/unit/(:any)']['GET'] = 'api/api_units/getUnit/$1';
