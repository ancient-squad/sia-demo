
<div class="row">
<?php 
  $log = $this->session->userdata('sess_login');
  $akt = substr($log['userid'], 0,4);
  $thn = $this->app_model->getdetail('tbl_tahunakademik','status',1,'kode','asc')->row();
  if ($notif == 'mhs') {
    $krs = $this->temph_model->notif_krs($log['userid'].$thn->kode)->row();
    $khs = $this->temph_model->notif_khs($log['userid'].$thn->kode)->row();
    if ($krs->status_verifikasi == 1 and is_null($krs->notif)) { ?>
      <?php if ($akt != date('Y')) { ?>
        <div class="span6">
          <div class="alert alert-success">
            <button type="button" class="close" data-dismiss="alert">×</button>
            <strong>Perhatian!</strong> KRS Anda telah disetujui oleh pembimbing akademik. 
            <a href="<?php echo base_url('home/notif_update_krs/'.$log['userid'].$thn->kode); ?>" onclick="" title=""><b>Lihat ...</b></a>
          </div>
        </div>
      <?php } ?>
  <?php }
    if ($khs->flag_publikasi == 2 and is_null($khs->notif)) { ?>
      <div class="span6">
        <div class="alert alert-danger">
          <button type="button" class="close" data-dismiss="alert">×</button>
          <strong>Perhatian!</strong> Nilai anda telah dapat dilihat. 
          <a href="<?php echo base_url('home/notif_update_khs/'.$log['userid'].'/'.$thn->kode); ?>" onclick="" title=""><b>Lihat ...</b></a>
        </div>
      </div>
  <?php } 
    if (date('Y-m-d') < '2017-12-30') { ?>
      <div class="span6">
        <div class="alert alert-danger">
          <!-- <button type="button" class="close" data-dismiss="alert">×</button> -->
          <strong>Perhatian!</strong>  Kalender Akademik Pengisisan KRS <b>2017 / 2018 - Ganjil.</b>
          <a href="#popcalendar" data-toggle="modal" title=""><b>Lihat ...</b></a>
        </div>
      </div>
  <?php }       
    if (date('Y-m-d') < '2017-12-31') { ?>
      <div class="span6">
        <div class="alert alert-danger">
          <button type="button" class="close" data-dismiss="alert">×</button>
          <strong>Perhatian!</strong> Panduan pengisisan KRS untuk mahasiswa tahun ajaran 2017/2018 - Ganjil. 
          <a href="<?php echo base_url('PANDUAN_PENGISIAN_KRS.pdf'); ?>" onclick="" title=""><b>Lihat ...</b></a>
        </div>
      </div>
  <?php }
  } elseif ($notif == 'dpa') {
    $ajukrs = $this->temph_model->notif_for_pa($log['userid'],$thn->kode)->result();
    if (count($ajukrs) > 0) { ?>
      <div class="span6">
        <div class="alert alert-danger">
          <button type="button" class="close" data-dismiss="alert">×</button>
          <strong>Perhatian!</strong> <?php echo count($ajukrs); ?> mahasiswa melakukan pengajuan KRS dan belum ditindak lanjuti. 
          <a href="<?php echo base_url('home/notif_pa/'.$thn->kode); ?>" onclick="" title=""><b>Lihat ...</b></a>
        </div>
      </div>
  <?php }
    
  } elseif ($notif == 'baa') {
    $newkrs = $this->temph_model->loadnewkrs('baa',$thn->kode)->result();
    $apvkrs = $this->temph_model->loadapvkrs('baa',$thn->kode)->result();
    if (date('Y-m-d') > '2017-08-13' and date('Y-m-d') < '2017-08-29') { ?>
      <div class="span6">
        <div class="alert alert-danger">
          <button type="button" class="close" data-dismiss="alert">×</button>
          <strong>Perhatian!</strong> <?php echo count($newkrs); ?> mahasiswa melakukan pengajuan KRS dan <?php echo count($apvkrs); ?> sudah ditindak lanjuti dosen PA pada semua prodi. 
          <a href="<?php echo base_url('home/notif_baa/'.$thn->kode); ?>" onclick="" title=""><b>Lihat ...</b></a>
        </div>
      </div>
  <?php }
    
  } elseif ($notif == 'prd') {
    $yearsp = $thn->kode+2;
    $ctclass = $this->temph_model->countimpvclass($log['userid'],$yearsp)->row();
    $ctmhs = $this->temph_model->mhsimpclass($log['userid'],$yearsp)->row();
    if (date('Y-m-d') < '2017-12-30') { ?>
      <div class="span6">
        <div class="alert alert-danger">
          <!-- <button type="button" class="close" data-dismiss="alert">×</button> -->
          <strong>Perhatian!</strong> Kalender Akademik Pengisisan KRS <b>2017 / 2018 - Ganjil</b>.
          <a href="#popcalendar" data-toggle="modal" title=""><b>Lihat ...</b></a>
        </div>
      </div>
      <div class="span6">
        <div class="alert alert-danger">
          <!-- <button type="button" class="close" data-dismiss="alert">×</button> -->
          Terdapat <b><?php echo $ctclass->jum; ?></b> kelas perbaikan dan <b><?php echo $ctmhs->mhs; ?></b> mahasiswa mengikuti kelas perbaikan.
          <a href="<?php echo base_url('home/notif_prd_sp/'.$thn->kode); ?>" onclick="" title=""><b>Lihat ...</b></a>
        </div>
      </div>
  <?php }
    $newkrs = $this->temph_model->loadnewkrs($log['userid'],$thn->kode)->result();
    $apvkrs = $this->temph_model->loadapvkrs($log['userid'],$thn->kode)->result();
    $nonact = $this->temph_model->nonactivemhs($log['userid'],$thn->kode)->row();
    $cuti   = $this->temph_model->cutimhs($log['userid'],$thn->kode)->row();
    if (date('Y-m-d') < '2017-12-30') { ?>
        <div class="span6">
          <div class="alert alert-danger">
            <!-- <button type="button" class="close" data-dismiss="alert">×</button> -->
            <strong>Status mahasiswa <?php echo $thn->tahun_akademik; ?>: </strong> 
            
              <strong><?php echo count($newkrs); ?></strong> mahasiswa aktif, 
              <strong><?php echo $nonact->jum; ?></strong> mahasiswa non-aktif, 
              <strong><?php echo $cuti->jum; ?></strong> mahasiswa cuti
              <?php // echo count($newkrs); ?>  <?php // echo count($apvkrs); ?> 
            <!-- <a href="<?php // echo base_url('home/loadkrsprodi/'.$log['userid'].'/'.$thn->kode); ?>" onclick="" title=""><b>Lihat ...</b></a> -->
            <a href="<?php echo base_url('home/load_stmhs/'.$log['userid'].'/'.$thn->kode); ?>" onclick="" title=""><b>Lihat ...</b></a>
          </div>
        </div>

  <?php } 

  } elseif ($notif == 'dsn') {
    if (date('Y-m-d') < '2017-12-30') { ?>
      <div class="span6">
        <div class="alert alert-danger">
          <!-- <button type="button" class="close" data-dismiss="alert">×</button> -->
          <strong>Perhatian!</strong>  Kalender Akademik Pengisisan KRS <b>2017 / 2018 - Ganjil.</b>
          <a href="#popcalendar" data-toggle="modal" title=""><b>Lihat ...</b></a>
        </div>
      </div>
  <?php }
  }
?>
</div>
<div class="row">

  <!-- /span6 -->

  <!--div class="span12">
    <div class="widget">
      <div class="widget-header"> <i class="icon-bookmark"></i>
        <h3>Important Shortcuts</h3>
      </div>
      <!-- /widget-header
      <div class="widget-content">
        <div class="shortcuts"> 
         <?php //$q = $this->role_model->getallmenu()->result(); foreach ($q as $menu) { ?>
          <a href="<?php //echo $menu->url; ?>" class="shortcut"><i class="shortcut-icon icon-github-sign"></i><span class="shortcut-label"><?php //echo $menu->menu; ?></span> </a> 
         <?php //} ?>
        </div>
        <!-- /shortcuts
      </div>
      <!-- /widget-content
    </div>
  </div-->

  <div class="span8">

    <div class="widget widget-nopad">

      <div class="widget-header"> <i class="icon-list-alt"></i>

        <h3> Kalender Akademik <?= $this->ORG_NAME ?></h3>

      </div>

      <div class="widget-content">
        <div class="tabbable">
            <ul class="nav nav-tabs">
              <li class="active">
                <a href="#formcontrols" data-toggle="tab">Kalender Akademik</a>
              </li>
              <li><a href="#pra" data-toggle="tab">Pra Perkuliahan</a></li>
              <li><a href="#kuliah" data-toggle="tab">Semester Perkuliahan</a></li>
              <li><a href="#bayar" data-toggle="tab">Pembayaran Perkuliahan</a></li>
            </ul>
            
              <div class="tab-content">
                <div class="tab-pane  active" id="formcontrols">
                  <iframe 
                    src="<?= base_url('calendar/kalender'); ?>" 
                    style="border: 0" 
                    width="750" 
                    height="670" 
                    frameborder="0" 
                    scrolling="no"></iframe>
                </div>
                
                <div class="tab-pane" id="pra">
                  <b><center>KEGIATAN PRA PERKULIAHAN</center></b><br>

                  <table class="table table-bordered table-striped">
                        <thead>
                            <tr> 
                                <th>No</th>
                                <th>Kegiatan</th>
                                <th>Waktu Kegiatan</th>
                            </tr>
                        </thead>
                        <tbody>
                          <?php $no=1; foreach ($calender1 as $isi) { 

                            if ($isi->mulai == $isi->ahir) {
                                $waktu = TanggalIndo($isi->mulai);
                            }else{
                                $waktu = TanggalIndoRange($isi->mulai,$isi->ahir);
                            } 
                            ?>

                            <tr>
                                <td><?php echo $no ?></td>
                                <td><?php echo $isi->kegiatan ?></td>
                                <td><?php echo $waktu ?></td>
                            </tr>
                            <?php $no++; } ?>
                        </tbody>
                    </table>
                </div>
                <div class="tab-pane" id="kuliah">
                  <b><center>KEGIATAN PERKULIAHAN</center></b><br>
                  <table class="table table-bordered table-striped">
                        <thead>
                            <tr> 
                                <th>No</th>
                                <th>Kegiatan</th>
                                <th>Waktu Kegiatan</th>
                            </tr>
                        </thead>
                        <tbody>
                          <?php $no=1; foreach ($calender2 as $isi) { 

                            if ($isi->mulai == $isi->ahir) {
                                $waktu = TanggalIndo($isi->mulai);
                            }else{
                                $waktu = TanggalIndoRange($isi->mulai,$isi->ahir);
                            } 
                            ?>

                            <tr>
                                <td><?php echo $no ?></td>
                                <td><?php echo $isi->kegiatan ?></td>
                                <td><?php echo $waktu ?></td>
                            </tr>
                            <?php $no++; } ?>
                        </tbody>
                    </table>
                </div>
                <div class="tab-pane" id="bayar">
                  <b><center>PEMBAYARAN PERKULIAHAN</center></b><br>
                  <table class="table table-bordered table-striped">
                        <thead>
                            <tr> 
                                <th>No</th>
                                <th>Kegiatan</th>
                                <th>Waktu Kegiatan</th>
                            </tr>
                        </thead>
                        <tbody>
                          <?php $no=1; foreach ($calender3 as $isi) { 

                            if ($isi->mulai == $isi->ahir) {
                                $waktu = TanggalIndo($isi->mulai);
                            }else{
                                $waktu = TanggalIndoRange($isi->mulai,$isi->ahir);
                            } 
                            ?>

                            <tr>
                                <td><?php echo $no ?></td>
                                <td><?php echo $isi->kegiatan ?></td>
                                <td><?php echo $waktu ?></td>
                            </tr>
                            <?php $no++; } ?>
                        </tbody>
                    </table>  
                </div>
                
              </div>
              
              
            </div>   
      </div>

    </div>

  </div>



  <div class="span4">

    <div class="widget widget-nopad">

      <div class="widget-header"> <i class="icon-list-alt"></i>

        <h3> Recent News </h3>

      </div>

      <!-- /widget-header -->

      <div class="widget-content">

        <ul class="news-items">

          
          <!--<?php 
            //foreach($x->channel->item as $entry) {
             //   echo "<li><a href='$entry->link' title='$entry->title'>" . $entry->title . "</a></li>";
            //}
          ?> --> 
          <li><a href='https://life.idntimes.com/inspiration/stella/10-film-inspiratif-ini-siap-menemanimu-cari-jati-diri-di-usia-20-an/full' target="_blank">10 Film Inspiratif yang Siap Menemanimu Cari Jati Diri di Usia 20-an</a></li>
          <li><a href="http://kopertis3.or.id/v2/2017/03/20/program-bantuan-seminar-luar-negeri-bagi-dosenpeneliti-di-perguruan-tinggi/" target="_blank">Program Bantuan Seminar Luar Negeri bagi Dosen/Peneliti di Perguruan Tinggi</a></li>
          <li><a href="http://lib.<?= $this->URL ?>/home" target="_blank">Website Perpustakaan UNIVERSITAS ABCD</a></li>
          <li><a href="http://203.77.248.52/slims/" target="_blank">Cari Buku di Perpustakaan via Online</a></li>
          <li><a href="https://life.idntimes.com/education/erny/14-situs-untuk-menyelesaikan-skripsi-cepat-waktu/full" target="_blank">Situs Jurnal Untuk Membantu Menyelesaikan Skripsi/Penulisan Ilmiah</a></li>
          <li><a href="http://<?= $this->URL ?>/wp-ubj/2017/03/13/universitas-bhayangkara-jakarta-raya-dan-university-of-mindanao-filipina-kerjasama-kembangkan-profesi-di-bidang-kriminologi/" target="_blank">UNIVERSITAS ABCD dan University of Mindanao Filipina Kerjasama Kembangkan Profesi di Bidang Kriminologi</a></li>
          <li><a href="http://<?= $this->URL ?>/wp-ubj/2017/02/21/dpp-granat-rayon-ubhara-jaya-selenggarakan-pelatihan-penanggulangan-penyalahgunaan-dan-peredaran-gelap-narkoba/" target="_blank">DPP Granat Rayon  Selenggarakan Pelatihan Penanggulangan Penyalahgunaan dan Peredaran Gelap Narkoba</a></li>
          <li><a href="https://life.idntimes.com/inspiration/arief-hany/inspirasi-desain-rak-buku-kreatif-c1c2/full" target="_blank">10 Inspirasi Desain Rak Buku Super Kreatif</a></li>
          <li><a href="https://life.idntimes.com/inspiration/minute-maid-pulpy/baikitunyata-6-kebaikan-yang-sering-kita-terima-tapi-jarang-disadari#WmGQKCAjkJ3LoCWD.97/full" target="_blank">#BaikItuNyata: 6 Kebaikan yang Sering Kita Terima Tapi Jarang Disadari</a></li>
          <li><a href="http://actioncoachsouthjakarta.com/9-langkah-untuk-membuat-sebuah-action-plan/?utm_source=taboola&utm_medium=idntimes&utm_term=9+Langkah+untuk+Membuat+Sebuah+Action+Plan&utm_content=http%3A%2F%2Fcdn.taboolasyndication.com%2Flibtrc%2Fstatic%2Fthumbnails%2F8929ac6c2b96d1187be201dc99a2161d.jpg&id=Desktop" target="_blank">9 Langkah untuk Membuat Sebuah Action Plan</a></li>
        </ul>

      </div>

      <!-- /widget-content --> 

    </div>

  </div>

</div>

<div class="modal fade" id="popcalendar" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Kalender Akademik Pengisian KRS 2017/2018 - Ganjil</h4>
            </div>
            <form class ='form-horizontal' action="<?php echo base_url();?>organisasi/ruang/save_ruang" method="post" enctype="multipart/form-data">
                <div class="modal-body">  
                    <table class="table table-bordered table-striped">
                      <thead>
                        <tr>
                          <td rowspan="2" style="text-align:center">NO</td>
                          <td rowspan="2" style="text-align:center">PRODI</td>
                          <td colspan="2" style="text-align:center">PERIODE</td>
                        </tr>
                        <tr>
                          <td style="text-align:center">I</td>
                          <td style="text-align:center">II</td>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <td>1</td>
                          <td>Manajemen, Akuntansi, dan Psikologi</td>
                          <td>14-16 Agustus 2017</td>
                          <td>23-25 Agustus 2017</td>
                        </tr>
                        <tr>
                          <td>2</td>
                          <td>Teknik Industri, Teknik Kimia, Teknik Perminyakan, Teknik Lingkungan, dan Ilmu Komunikasi</td>
                          <td>17-19 Agustus 2017</td>
                          <td>25-26 Agustus 2017</td>
                        </tr>
                        <tr>
                          <td>3</td>
                          <td>Magister Manajemen, Magister Ilmu Hukum, Ilmu Hukum, dan Teknik Informatika</td>
                          <td>20-22 Agustus 2017</td>
                          <td>26-27 Agustus 2017</td>
                        </tr>
                        <tr>
                          <td>4</td>
                          <td>Mahasiswa Baru 2017</td>
                          <td colspan="2">25-26 Agustus 2017/mengikuti jadwal PMB</td>
                        </tr>
                      </tbody>
                    </table>           
                </div> 
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Keluar</button>
                </div>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->