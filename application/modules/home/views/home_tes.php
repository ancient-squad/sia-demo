<div class="row">
  <?php if (!empty($versi)) {
    $spn = "span8";
  } else {
    $spn = "span12";
  } ?>
  <div class="<?= $spn; ?>">
    <div class="widget">
      <div class="widget-header"> <i class="icon-list-alt"></i>
        <h3> Kalender Akademik <?= $this->ORG_NAME ?></h3>
      </div>
      <div class="widget-content">
        <div class="tabbable">
          <ul class="nav nav-tabs">
            <!-- <li class="active">
              <a href="#formcontrols" data-toggle="tab">Kalender Akademik</a>
            </li> -->
            <li class="active"><a href="#pra" data-toggle="tab">Pra Perkuliahan</a></li>
            <li><a href="#kuliah" data-toggle="tab">Semester Perkuliahan</a></li>
            <li><a href="#bayar" data-toggle="tab">Pembayaran Perkuliahan</a></li>
          </ul>

          <div class="tab-content">
            <!-- <div class="tab-pane  active" id="formcontrols">
              <embed src="<?= base_url('calendar/old/kalender/') ?>" style="border: 0" width="750" height="670" frameborder="0" scrolling="no" />
            </div> -->

            <div class="tab-pane active" id="pra">
              <b><center>KEGIATAN PRA PERKULIAHAN</center></b>
              <br>
              <table class="table table-bordered table-striped">
                <thead>
                  <tr>
                    <th>No</th>
                    <th>Kegiatan</th>
                    <th>Waktu Kegiatan</th>
                  </tr>
                </thead>
                <tbody>
                  <?php $no = 1;
                  foreach ($calender1 as $isi) {

                    if ($isi->mulai == $isi->ahir) {
                      $waktu = TanggalIndo($isi->mulai);
                    } else {
                      $waktu = TanggalIndoRange($isi->mulai, $isi->ahir);
                    } ?>

                    <tr>
                      <td><?= $no ?></td>
                      <td><?= $isi->kegiatan ?></td>
                      <td><?= $waktu ?></td>
                    </tr>
                  <?php $no++;
                  } ?>
                </tbody>
              </table>
            </div>
            <div class="tab-pane" id="kuliah">
              <b>
                <center>KEGIATAN PERKULIAHAN</center>
              </b><br>
              <table class="table table-bordered table-striped">
                <thead>
                  <tr>
                    <th>No</th>
                    <th>Kegiatan</th>
                    <th>Waktu Kegiatan</th>
                  </tr>
                </thead>
                <tbody>
                  <?php
                  $no = 1;
                  foreach ($calender2 as $isi) { ?>
                    <tr>
                      <td><?= $no ?></td>
                      <td><?= $isi->kegiatan ?></td>
                      <td>
                        <?= $isi->mulai == $isi->ahir ? TanggalIndo($isi->mulai) :TanggalIndoRange($isi->mulai, $isi->ahir); ?>
                      </td>
                    </tr>
                  <?php $no++; } ?>
                </tbody>
              </table>
            </div>
            <div class="tab-pane" id="bayar">
              <b>
                <center>PEMBAYARAN PERKULIAHAN</center>
              </b><br>
              <table class="table table-bordered table-striped">
                <thead>
                  <tr>
                    <th>No</th>
                    <th>Kegiatan</th>
                    <th>Waktu Kegiatan</th>
                  </tr>
                </thead>
                <tbody>
                  <?php $no = 1;
                  foreach ($calender3 as $isi) {

                    if ($isi->mulai == $isi->ahir) {
                      $waktu = TanggalIndo($isi->mulai);
                    } else {
                      $waktu = TanggalIndoRange($isi->mulai, $isi->ahir);
                    } ?>

                    <tr>
                      <td><?= $no ?></td>
                      <td><?= $isi->kegiatan ?></td>
                      <td><?= $waktu ?></td>
                    </tr>
                  <?php $no++;
                  } ?>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <?php if (!empty($versi)) { ?>
    <div class="span4">
      <!-- panel notif -->
      <div class="widget">
        <div class="widget-header"> <i class="icon-bell"></i>
          <h3>Pemberitahuan</h3>
        </div>
        <div class="widget-content">
          <?php
          $log = $this->session->userdata('sess_login');
          $akt = substr($log['userid'], 0, 4);
          $actYear = getactyear();

          /* notifikasi mahasiswa non aktif untuk prodi */
          if ($notif == "prd") {
            if ($notif_mhs->num_rows() > 0) {
              foreach ($notif_mhs->result() as $key => $value) : ?>
                <div class="alert alert-warning">
                  <button type="button" class="close close-notif" data-id="<?= $value->id ?>" data-dismiss="alert">×</button>
                  <p><strong>Informasi!</strong></p>
                  <p>
                    <?= $value->message ?> klik 
                    <a href="#mhs_non" data-toggle="modal" data-tooltip="tooltip" title="list mahasiswa non aktif">
                      <strong class="detail">disini</strong>
                    </a> untuk detail
                  </p>
                </div>
              <?php endforeach;
            }
          }

          // notif for mahasiswa
          if ($notif == 'mhs') {
            $krs = $this->temph_model->notif_krs($log['userid'] . $actYear);
            $khs = $this->temph_model->notif_khs($log['userid'] . $actYear);

            if (count($notifikasi_mhs) > 0) {
              foreach ($notifikasi_mhs as $key => $value) : ?>
                <!-- Alert -->
                <div class="alert alert-danger">
                  <button type="button" class="close close-notif" data-id="<?= $value->id ?>" data-dismiss="alert">×</button>
                  <strong> <?= $value->message ?>.</strong>
                </div>
            <?php endforeach; } ?>

            <?php
            if ($krs->num_rows() > 0) {
              $krs = $krs->row();
              if ($krs->status_verifikasi == 1 and is_null($krs->notif)) {
                if ($akt != date('Y')) : ?>
                  <!-- KRS Disetujui -->
                  <div class="alert alert-success">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                    <strong>Perhatian!</strong> KRS Anda telah disetujui oleh pembimbing akademik.
                    <a href="<?= base_url('home/notif_update_krs/' . $log['userid'] . $actYear); ?>" onclick="" title="">
                      <b>Lihat ...</b>
                    </a>
                  </div>
                <?php endif;
              }
            }

            if ($khs->num_rows() > 0) {
              $khs = $khs->row();
              if ($khs->flag_publikasi == 2 and is_null($khs->notif)) : ?>
                <!-- KHS -->
                <div class="alert alert-danger">
                  <button type="button" class="close" data-dismiss="alert">×</button>
                  <strong>Perhatian!</strong> Nilai anda telah dapat dilihat.
                  <a href="<?= base_url('home/notif_update_khs/' . $log['userid'] . '/' . $actYear); ?>" onclick="" title="">
                    <b>Lihat ...</b>
                  </a>
                </div>
              <?php endif;
            }

          // notif for dosen pembimbing
          } elseif ($notif == 'dpa') {
            $ajukrs = $this->temph_model->notif_for_pa($log['userid'], $actYear)->result();
            if (count($ajukrs) > 0) : ?>
              <div class="alert alert-danger">
                <button type="button" class="close" data-dismiss="alert">×</button>
                <strong>Perhatian!</strong> <?= count($ajukrs); ?> mahasiswa melakukan pengajuan KRS dan belum ditindak lanjuti.
                <a href="<?= base_url('home/notif_pa/' . $actYear); ?>" onclick="" title=""><b>Lihat ...</b></a>
              </div>
          <?php endif; } ?>
        </div>
      </div>

      <!-- panel versi -->
      <div class="widget">
        <div class="widget-header"> <i class="icon-list-alt"></i>
          <h3>Version&nbsp;<?= $versi->versi ?></h3>
        </div>
        <div class="widget-content">
          <ul class="news-items">
            <li>
              <div class="news-item-date">
                <span class="news-item-day">
                  <?= datetimeIdnday($versi->created_at) ?>
                </span>
                <span class="news-item-month">
                  <?= datetimeIdnmon($versi->created_at) ?>
                </span>
              </div>
              <div class='news-item-detail'>
                <p class='news-item-preview'><?= $versi->keterangan ?></p>
              </div>
            </li>
          </ul>
          <ul class="messages_layout">
            <?php
            if (!empty($dtl_version)) { ?>
              <center>
                <table class="table table-striped" style="width:80%;border-top:0px;">
                  <tbody>
                    <?php
                    $n = 1;
                    foreach ($dtl_version as $row) {
                      $pisah = explode(",", $row->vlog);
                      foreach ($pisah as $satuan) { ?>
                        <tr style="border-top:0px;">
                          <td style="border-top:0px;width:10%;"><?= $n ?></td>
                          <td style="border-top:0px;"><?= $satuan ?> </td>
                        </tr>
                    <?php $n++; } } ?>
                  </tbody>
                </table>
              </center>
            <?php } else { echo 'Tidak ada pembaruan'; } ?>
          </ul>
          <p align="center"><b><a href="<?= base_url('extra/changelog') ?>">Changelog detail</a></b></p>
        </div>
      </div>
    </div>
  <?php } ?>
</div>

<?php if ($notif == "prd") {
  if ($notif_mhs->num_rows() > 0) : ?>
    <!-- Modal List Mahasiswa Non Aktif -->
    <div class="modal fade" id="mhs_non" tabindex="-1" role="dialog" aria-labelledby="mhs_nonLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="modal-title">Mahasiswa Non Aktif Di <?php get_thajar(getactyear()); ?></h4>
          </div>
          <div class="modal-body">
            <table class="table table-bordered table-striped" id="example9">
              <thead>
                <tr>
                  <th style="text-align:center">NO</th>
                  <th style="text-align:center">NPM</th>
                </tr>
              </thead>
              <tbody>
                <?php $i = 1;
                foreach ($list_mhs->result() as $key => $value) : ?>
                  <tr>
                    <td><?= $i++; ?></td>
                    <td><?= $value->NIMHSMSMHS . " - " . $value->NMMHSMSMHS ?></td>
                  </tr>
                <?php endforeach ?>

              </tbody>
            </table>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Keluar</button>
          </div>
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
    <!-- Modal List Mahasiswa Non Aktif -->
<?php endif; } ?>

<script type="text/javascript">
  $(function() {
    $('[data-toggle="tooltip"]').tooltip()

    <?php if ($notif == "prd" || $notif == "mhs") : ?>
      $(".close-notif").on("click", function(e) {
        let id = document.querySelector(".close-notif")
        $.post("<?= base_url("home/read-notif") ?>", {
          rid: id.getAttribute("data-id")
        })
      })
    <?php endif ?>
  })
</script>