<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Pemulihan extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		error_reporting(0);
	}

	function index(){
		$this->load->view('forgot_pass');
	}

	function send_pass(){
		$npm = $this->input->post('npm');
		
		$q=$this->db->where('npm',$npm)->get('tbl_bio_mhs',1)->row();
		
		$email = $q->email;

		$pass =  $this->db->where('userid',$npm)->get('tbl_user_login',1)->row();

		

		if (is_null($email) || $email == '' || $q == FALSE) {
			echo "<script>alert('Anda belum mendaftarkan email pemulihan, anda dapat menghubungi prodi untuk layanan lupa password');history.go(-1);</script>";
		}else{
			$this->send_mail($pass->userid,$pass->user_type);
			$this->load->view('forgot_redirect');
		}

	}

	

	function send_mail($userid,$type){
		$this->db->where('userid', $userid);
		$this->db->delete('tbl_lupa_pass');

		if ($type == 1) {
			$get_mail = $this->db->where('nid',$userid)->get('tbl_biodata_dosen',1)->row()->email;
		}elseif ($type == 2) {
			$get_mail = $this->db->where('npm',$userid)->get('tbl_bio_mhs',1)->row()->email;
		}else{
			exit();
		}

		$admin = 'it@'.$this->URL;

		$email = $get_mail.','.$admin;

		$hash = NULL;
        $n = 20; // jumlah karakter yang akan di bentuk.
        $chr = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvqxyz0123456789";
         
        for ($i = 0; $i < $n; $i++) {
            $rIdx = rand(1, strlen($chr));
            $hash .=substr($chr, $rIdx, 1);
        }

        $key = $hash.date('ymdHis');
        $object = array('userid' => $userid,
        				'key' => $key,
        				'create_date' => date('Y-m-d H:i:s'),
        				'flag' => 0);

        $this->db->insert('tbl_lupa_pass', $object);



		$isi = '<p>Hai,</p>
				<p>Terimakasih telah menggunakan aplikasi Sistem Informasi Akademik '.$this->ORG_NAME.'.</p>
				<p>Untuk login dan rubah password SIA anda bisa menggunakan LINK di bawah ini.</p>'.anchor('http://sia.'.$this->URL.'/auth/pemulihan/ganti_password/'.$key).'';
		
		

		//$isi = $this->load->view('forgot_mail');
		
		$judul='Pemulihan Password Siakad';

		$config = Array(
	        'protocol' => 'smtp',
	        'smtp_host' => 'ssl://smtp.gmail.com',
	        'smtp_port' => 465,
	        //'smtp_user' => 'nurfan.test@gmail.com', //email id
	        //'smtp_pass' => 'KIKi270293',// password
	        'smtp_user' => 'it@'.$this->URL, //email id
	        'smtp_pass' => 'Pusk0mUBJ',
	        'mailtype'  => 'html', 
	        'charset'   => 'iso-8859-1'
	    );
	    
	    $this->load->library('email', $config);
	    $this->email->set_newline("\r\n");

	    $this->email->from('it@'.$this->URL,'IT & Komputer UNIVERSITAS '.$this->ORG_NAME);
	    $this->email->to($email); // email array
	    $this->email->subject($judul);   
	    $this->email->message($isi);

	    $result = $this->email->send();
	}


	function ganti_password($id){

		$key = substr($id, 0,60);

		$cek_key = $this->db->where('key',$key)->get('tbl_lupa_pass',1);
		$count = $cek_key->num_rows();

		//$email_date = $cek_key->row()->create_date;
		//$limit = $email_date->modify('+1 day');
		//$now = date('Y-m-d H:i:s');

		//if ($now >= $limit) {
		//	echo "<script>alert('email permintaan pemulihan kadaluarsa mohon ulangi kembali');document.location.href='".base_url()."';</script>";	
		//}

		if ($count >= 1) {

			$userid = $cek_key->row()->userid;

			$flag = array('flag' => 1);

			$update_flag = $this->db->where('userid',$userid)->update('tbl_lupa_pass', $flag);

			$account = $this->db->where('userid',$userid)->get('tbl_user_login',1)->row();

			$user = $account->username;

			$pass = $account->password_plain;

			$cek = $this->login_model->cekuser($user,$pass)->result();

			if (count($cek) > 0) {
				foreach ($cek as $row) {
					$type = $row->user_type;
				}

				$user_log = $this->login_model->datauser($user,$pass,$type)->result();
				//var_dump($user);exit();

				foreach ($user_log as $key) {
					$session['username'] = $key->username;

					$session['userid'] = $key->userid;

					$session['id_user_group'] = $key->id_user_group;

					$session['password_plain'] = $key->password_plain;

					$session['user_type'] = $key->user_type;

					$this->session->set_userdata('sess_login',$session);

					$this->session->set_userdata('userid',$session['userid']);

					helper_log("login", "login siakad");
					$data['last_login'] = date('Y-m-d h:i:s');
					$data['logged'] = 1;
					$this->app_model->updatedata('tbl_user_login','username',$key->username,$data);

					redirect('auth/pemulihan/ganti_pass','refresh');
					
					}	
			} else {

			//echo "Gagal Login , <a href=".base_url()."auth> Back >> </a>";

			echo "<script>alert('Gagal Login');history.go(-1);</script>";

			}
		}
	}

	function ganti_pass(){
		$this->load->view('forgot_change');
	}

	function simpan(){#nomenu
		$user = $this->session->userdata('sess_login');
		$new = $this->input->post('new', TRUE);
		
		$data['password_plain']= $new;
		$data['password']= sha1(md5($data['password_plain']).key);

		$update = $this->app_model->updatedata('tbl_user_login','userid',$user['userid'],$data);
			if ($update == TRUE) {
				$cekmail = $this->db->where('npm',$user['userid'])->get('tbl_bio_mhs',1)->row();
				echo "<script>alert('Berhasil');document.location.href='".base_url()."home';</script>";
			} else {
				echo "<script>alert('Gagal Edit Data');history.go(-1);</script>";
			}
	}

}

/* End of file Pemulihan.php */
/* Location: ./application/controllers/Pemulihan.php */