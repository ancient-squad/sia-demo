<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8">
    
    <meta name="description" content="Sistem Informasi Akademik ">
    <meta name="keywords" content="SIA,<?= $this->ORG_NAME ?>,SIA <?= $this->ORG_NAME ?>,<?= $this->ORG_NAME ?>">
    <meta name="author" content="litedev">

    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

    <meta name="apple-mobile-web-app-capable" content="yes">

    <title>SISTEM INFORMASI AKADEMIK</title>

    <!-- Bootstrap core CSS -->
    <link href="<?php echo base_url(); ?>assets/temp_adm/assets/css/bootstrap.css" rel="stylesheet">
    <!--external css-->
    <link href="<?php echo base_url(); ?>assets/temp_adm/assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
        
    <!-- Custom styles for this template -->
    <link href="<?php echo base_url(); ?>assets/temp_adm/assets/css/style.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assets/temp_adm/assets/css/style-responsive.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>

  <body>

      <!-- **********************************************************************************************************************************************************
      MAIN CONTENT
      *********************************************************************************************************************************************************** -->

	<div id="login-page" style="padding-bottom:50px">
	  	<div class="container">
	  		<center>
	  			<a href="#" style="color:#f4f2f2;hover:#eae9e9">
		  			<div style="margin-top:80px;">
		  				<h1>SISTEM INFORMASI AKADEMIK</h1>
		  				
		  			</div>
	  			</a>
	  		</center>
	  		
		    <form class="form-login" action="<?php echo base_url();?>auth/pemulihan/send_pass" method="post" style="margin-top:30px;">
		        <h2 class="form-login-heading">
		        	<!-- <img src="<?php echo base_url();?>assets/logo.png" style="width:20%;margin-top:-12px;margin-left:-1px;margin-bottom:-9px;padding-right:10px;"> -->
		        	<i class="fa fa-unlock"></i>
		        	LUPA PASSWORD
		        	<!-- <img src="<?php echo base_url();?>assets/img/puskom.png" style="width:20%;margin-top:-12px;margin-left:10px;margin-bottom:-9px;padding-right:10px;"> -->
		        </h2>
		        <div class="login-wrap">
		            <input type="text" name="npm" class="form-control" placeholder="Masukan Nomor Pokok Mahasiswa Anda">
		            <br>
		            <button class="btn btn-theme btn-block" type="submit"> SEND PASSWORD TO EMAIL </button>
		            <br>
		            <a href="<?php echo base_url();?>auth"><p>Sign In</p></a>
		            <!-- <hr>
		            <div class="login-social-link centered">
		            	<p>Hubungi UPT IT dan Komputer jika terjadi kendala</p>
		            </div> -->
		        </div>
		    </form>	
		    <br>
		    <center>
		    	<a href="<?php echo base_url(); ?>" style="color:#f4f2f2;hover:#eae9e9">
		    		<h5>&copy; 2021 - Sistem Informasi Akademik</h5>  
		    	</a>
		    </center>
		    <br>
		    <center>
		    	<a href="http://<?= $this->URL ?>/" target="blank" style="color:#f4f2f2;hover:#eae9e9">
		    		<h5>UNIVERSITAS <?= $this->ORG_NAME ?></h5>  
		    	</a>
		    </center>
		    
	  	</div>
	</div>

    <!-- js placed at the end of the document so the pages load faster -->
    <script src="<?php echo base_url(); ?>assets/temp_adm/assets/js/jquery.js"></script>
    <script src="<?php echo base_url(); ?>assets/temp_adm/assets/js/bootstrap.min.js"></script>

    <!--BACKSTRETCH-->
    <!-- You can use an image of whatever size. This script will stretch to fit in any screen size.-->
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/temp_adm/assets/js/jquery.backstretch.min.js"></script>
    <script>
        $.backstretch("<?php echo base_url(); ?>assets/temp_adm/assets/img/image-edu.jpg", {speed: 500});
    </script>


  </body>
</html>
