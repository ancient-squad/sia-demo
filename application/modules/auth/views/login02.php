<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8">
    <meta name="description" content="Sistem Informasi Akademik">
    <meta name="keywords" content="SIA,SIAKAD,sistem informasi akademik">
    <meta name="author" content="AprilLabs">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">

    <title><?= $this->APP_NAME ?></title>
    <!-- <link rel="shortcut icon" href="<?php echo base_url(); ?>assets/logo.ico"/> -->
    <!-- Bootstrap core CSS -->
    <link href="<?php echo base_url(); ?>assets/temp_adm/assets/css/bootstrap.css" rel="stylesheet">
    <!--external css-->
    <link href="<?php echo base_url(); ?>assets/temp_adm/assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
        
    <!-- Custom styles for this template -->
    <link href="<?php echo base_url(); ?>assets/temp_adm/assets/css/style.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assets/temp_adm/assets/css/style-responsive.css" rel="stylesheet">

  </head>

  <body style="background: #cafcbf !important">

      <!-- **********************************************************************************************************************************************************
      MAIN CONTENT
      *********************************************************************************************************************************************************** -->

    <style type="text/css">
    	.logo-image {
    		width:20%;
    		margin-top:-12px;
    		margin-left:-1px;
    		margin-bottom:-9px;
    		padding-right:10px;
    	}

      body {
        font-family: 'sans-serif' !important;
      }
    </style>

  	<div id="login-page" style="padding-bottom:50px">
    	<div class="container">
    		<center>
    			<a href="<?= base_url() ?>" style="color:#5f6360;">
  	  			<div style="margin-top:80px;">
                <img src="<?= base_url(); ?>assets/img/logo-stkip-bima.ico" style="width:80px;">
  	  				<h3>
                <?= $this->APP_NAME ?></h3>
  	  			</div>
    			</a>
    		</center>
    		
  	    <form class="form-login" action="<?php echo base_url();?>auth/login" method="post" style="margin-top:30px;">
          <h2 class="form-login-heading" style="background: #218657 !important">
            <i class="fa fa-sign-in"></i>
          	<!-- <img src="<?= base_url('assets/img/puskom.png');?>" style="margin-left: 5	px !important" class="logo-image"> -->
          	SIGN IN
          	<!-- <img src="<?= base_url('assets/logo.png');?>" class="logo-image"> -->
          </h2>
          <div class="login-wrap">
            <input type="text" name="username" class="form-control" placeholder="Username" autofocus required>
            <br>
            <input type="password" name="password" class="form-control" placeholder="Password" required>
            <br>
            <button class="btn btn-default btn-block" type="submit"><i class="fa fa-unlock"></i> SIGN IN</button>
            <br>
            <a href="<?= base_url('auth/pemulihan');?>"><p>Lupa Password</p></a>
          </div>
  	    </form>	
  	    <br>
  	    <center>
  	    	<a href="<?php echo base_url(); ?>" style="color:#5f6360">
  	    		<h5>&copy; 2021 - <?= $this->ORG_NAME?></h5>  
  	    	</a>
  	    </center>
  	    
    	</div>
  	</div>

    <!-- js placed at the end of the document so the pages load faster -->
    <script src="<?php echo base_url(); ?>assets/temp_adm/assets/js/jquery.js"></script>
    <script src="<?php echo base_url(); ?>assets/temp_adm/assets/js/bootstrap.min.js"></script>

    <!--BACKSTRETCH-->
    <!-- You can use an image of whatever size. This script will stretch to fit in any screen size.-->
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/temp_adm/assets/js/jquery.backstretch.min.js"></script>
    <!-- <script>
        $.backstretch("<?php echo base_url(); ?>assets/temp_adm/assets/img/image-edu.jpg", {speed: 500});
    </script> -->

  </body>
</html>
