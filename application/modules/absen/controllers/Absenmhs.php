<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Absenmhs extends MY_Controller {

	function __construct()
	{
		parent::__construct();
		if (!$this->session->userdata('sess_login')) {
			echo "<script>alert('Silahkan login kembali untuk memulai sesi!');</script>";
			redirect(base_url('auth/logout'),'refresh');
		}
		// error_reporting(0);
		$this->sess = $this->session->userdata('sess_login');
		$this->load->model('absen/absen_model', 'absen');
	}

	public function index()
	{
		$tgl = strtotime(date('Y-m-d'));
		$day = dayToNumber(date('l', $tgl));
		$actyear = getactyear();
		$kdkrs = $this->sess['userid'].$actyear;
		$data['list'] = $this->absen->loadListJadwal($kdkrs,$day)->result();
		$data['page'] = "v_verifabsenmhs";
		$this->load->view('template/template', $data);
	}

	function loadDetailAbs($id)
	{
		$act = getactyear();
		$kdjdl = get_kd_jdl($id);
		$data['kania'] = $this->absen->loadPresence($kdjdl);

		if ($data['kania']->num_rows() > 0) {
			
			// untuk memunculkan button validasi jika pertemuan tersebut blm divalidasi
			foreach ($data['kania']->result() as $key) {
				$kuncian = $key->transaksi;
				$meets = $key->pertemuan;
			}

			$check = $this->app_model->getdetail('tbl_transaksi_absen_'.$act,'kd_transaksi',$kuncian,'id','asc')->result();
			if ($check) {
				$data['btnvalid'] = '<a href="javascript:void(0);" class="btn btn-danger" onclick="alert(\'Pertemuan ini sudah divalidasi!\')">Validasi</a>';
			} else {

				$data['btnvalid'] = '<a href="'.base_url('absen/absenmhs/validasi/'.$kuncian.'/'.$meets).'" class="btn btn-success">Validasi</a>';
			}
			// end
			
		}

		$this->load->view('modal_detail_presence', $data);
	}

	function validasi($trk,$meet)
	{
		$act = getactyear();
		/*
		lakukan pengecekan apakah pertemuan sebelumnya sudah divalidasi
		*/

		// jika pertemuan pertama, cieh
		if ($meet > 1) {
			// get presence before
			$beef = $meet - 1;

			// get detail validasi on presence before
			$gets = $this->app_model->getdetail('tbl_absensi_mhs_new_'.$act,'transaksi',$trk,'id_absen','asc')->result();
			foreach ($gets as $val) {
				$kdjdl = $val->kd_jadwal;
			}

			// key presence before
			$hash = md5($kdjdl.$beef);

			// check availability last presence
			$last = $this->app_model->getdetail('tbl_transaksi_absen_'.$act,'kd_transaksi',$hash,'id','asc')->result();

			if ($last) {
				$this->absen->validAbsen($trk);
			} else {
				echo "<script>alert('Pertemuan sebelumnya belum divalidasi!');history.go(-1);</script>";
			}
		
		// jika bukan pertemuan pertama
		} else {
			$this->absen->validAbsen($trk);
		}
	}

}

/* End of file Absenmhs.php */
/* Location: .//tmp/fz3temp-1/Absenmhs.php */