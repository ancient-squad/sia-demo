<script>
    function getdetail(id) {
        $('#content').load('<?= base_url('extra/announcement/detail/') ?>' + id)
    }
</script>

<style>
    .modal-custom { 
        left: 25% !important;
        width: 1200px !important;
     }
</style>

<div class="row">
    <div class="span12">                    
        <div class="widget ">
            <div class="widget-header">
                <i class="icon-file"></i>
                <h3>Pengumuman</h3>
            </div> <!-- /widget-header -->
            
            <div class="widget-content">
                <div class="span11">
                    <button class="btn btn-primary" data-toggle="modal" data-target="#myModal">Unggah Pengumuman</button>
                    <hr>
                    <form id="edit-profile" class="form-horizontal" method="post" action="">
                        <fieldset>
                            <table id="example1" class="table table-bordered table-striped">
                                <thead>
                                    <tr> 
                                        <th>No</th>
                                        <th>Pengumuman</th>
                                        <th>Tahun Ajaran</th>
                                        <th width="80">Lihat</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $no=1; foreach ($announcement as $rows): ?>
                                        <tr>
                                            <td><?= $no; ?></td>
                                            <td><?= $rows->category ?></td>
                                            <td><?= get_thnajar($rows->tahunakademik) ?></td>
                                            <td>
                                                <button class="btn btn-success" data-toggle="modal" data-target="#myModals" onclick="getdetail(<?= $rows->id ?>)"><i class="icon-eye-open"></i></button>
                                            </td>
                                        </tr>
                                    <?php $no++; endforeach ?>
                                </tbody>
                            </table>
                        </fieldset>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content" id="absen">
            <form id="" action="<?=  base_url('extra/announcement/upload') ?>" method="post" enctype="multipart/form-data">
                
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Tambah Pengumuman</h4>
                </div>

                <div class="modal-body">

                    <div class="control-group">
                        <label class="control-label">Tahun Akademik</label>
                        <div class="controls">
                            <select class="form-control span5" name="tahunakademik">
                                <option disabled="" selected="">--Pilih Tahun Akademik--</option> 
                                <?php foreach ($tahunakademik as $val) { ?>
                                    <option value="<?= $val->kode ?>"><?= $val->tahun_akademik ?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>

                    <div class="control-group form-inline">
                        <label class="control-label">File</label>
                        <div class="controls">
                            <input class="form-control span5" type="file" name="userfile">
                        </div>
                    </div>

                    <div class="control-group">
                        <label class="control-label">Kategori</label>
                        <div class="controls">
                            <select class="form-control span5" name="category">
                                <option disabled="" selected="">--Pilih kategori--</option> 
                                <option value="jdl-keu">Keuangan Mahasiswa</option>
                                <option value="jdl-skr">Jadwal Sidang Skripsi</option>
                                <option value="jdl-prk">Jadwal Sidang Kerja Praktek</option>
                            </select>
                        </div>
                    </div>

                    <div class="control-group">
                        <label class="control-label">Catatan</label>
                        <div class="controls">
                            <textarea name="note" id="" class="form-control span5"></textarea>
                        </div>
                    </div>

                </div>

                <div class="modal-footer">
                    <button type="submit" class="btn btn-success">Submit</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>

            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div class="modal modal-custom fade" id="myModals" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-lg">
        <div class="modal-content" id="content">
            
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->