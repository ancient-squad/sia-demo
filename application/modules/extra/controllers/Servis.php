<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Servis extends MY_Controller {

	function __construct()
	{
		parent::__construct();
		if ($this->session->userdata('sess_login') == TRUE) {
			$cekakses = $this->role_model->cekakses(139)->result();
			if ($cekakses != TRUE) {
				echo "<script>alert('Anda Tidak Berhak Mengakses !!');history.go(-1);</script>";
			}
		} else {
			redirect('auth','refresh');
		}
	}

	public function index()
	{
		$data['page'] = 'v_caripw';
		$this->load->view('template/template', $data);
	}

	function cari()
	{
		$ini = $this->input->post('npm');
		$pecah = explode(' - ', $ini);
		$data['query'] = $this->db->query('SELECT * from tbl_user_login where userid = "'.$pecah[0].'" and id_user_group = "5"')->row();
		if ($data['query'] == FALSE) {
			echo "<script>alert('Akun Tidak Ditemukan! Periksa User ID Pengguna!');document.location.href='".base_url()."extra/servis';</script>";
		} else {
			$data['page'] = 'v_hasilcari';
			$this->load->view('template/template', $data);
		}
	}

	function cari_dosen()
	{
		$ini = $this->input->post('nid');
		$pecah = explode(' - ', $ini);
		$data['query'] = $this->db->query('SELECT * from tbl_user_login where userid = "'.$pecah[0].'"')->row();
		if ($data['query'] == FALSE) {
			echo "<script>alert('Akun Tidak Ditemukan! Periksa User ID Pengguna!');document.location.href='".base_url()."extra/servis';</script>";
		} else {
			$data['page'] = 'v_hasilcari_dosen';
			$this->load->view('template/template', $data);
		}
	}

	function load_mhs_autocomplete()
	{
		$bro = $this->session->userdata('sess_login');
		$prodi = $bro['userid'];
        $this->db->distinct();
        $this->db->select("a.NIMHSMSMHS,a.NMMHSMSMHS");
        $this->db->from('tbl_mahasiswa a');
        $this->db->like('a.NMMHSMSMHS', $_GET['term'], 'both');
        $this->db->or_like('a.NIMHSMSMHS', $_GET['term'], 'both');
        $sql  = $this->db->get();

        $data = array();
        foreach ($sql->result() as $row) {
            $data[] = [
                	'value' => $row->NIMHSMSMHS.' - '.$row->NMMHSMSMHS
                ];
        }
        echo json_encode($data);
    }

	function ganti()
	{
		$a = $this->input->post('old');
		$pecah = explode(' - ', $a);
		$e = $this->db->query('SELECT * from tbl_user_login where userid = "'.$pecah[0].'" and id_user_group = "5"')->row();
		if ($e == FALSE) {
			echo "<script>alert('Akun Tidak Ditemukan! Periksa User ID Pengguna!');document.location.href='".base_url()."extra/servis/';</script>";
		} else {
			$kiw = sha1(md5('STKIPB1M4').key);
			$this->db->query('UPDATE tbl_user_login set password = "'.$kiw.'", password_plain = "STKIPB1M4" where userid = "'.$pecah[0].'"');
			echo "<script>alert('Reset Password Berhasil !');document.location.href='".base_url()."extra/servis/';</script>";
		}
	}

	function load_dosen_autocomplete()
	{

		$bro = $this->session->userdata('sess_login');

		$prodi = $bro['userid'];

        $this->db->distinct();

        $this->db->select("nid,nama");

        $this->db->from('tbl_karyawan');

        $this->db->like('nama', $_GET['term'], 'both');

        $this->db->or_like('nid', $_GET['term'], 'both');

        $sql  = $this->db->get();

        $data = array();

        foreach ($sql->result() as $row) {

            $data[] = array(

                            'value' => $row->nid.' - '.$row->nama

                            );

        }

        echo json_encode($data);

    }

    function changeClass()
    {
    	$data['page'] = "v_editkelas";
    	$this->load->view('template/template', $data);
    }

    function executeChangeClass()
    {
    	extract(PopulateForm());

    	$explode = explode(' - ', $npm);

    	$actyear = getactyear();

    	$data = ['kelas' => $kelas];
    	$this->db->where('npm_mahasiswa', $explode[0]);
    	$this->db->where('tahunajaran', $year);
    	$this->db->update('tbl_verifikasi_krs', $data);

    	echo "<script>alert('Berhasil!');history.go(-1);</script>";
    }

}

/* End of file Servis.php */
/* Location: ./application/modules/extra/controllers/Servis.php */