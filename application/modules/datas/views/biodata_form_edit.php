<link href="<?php echo base_url();?>assets/css/plugins/bootstrap.css" rel="stylesheet">

<link rel="stylesheet" href="<?php echo base_url();?>assets/js/jquery-ui/css/ui-lightness/jquery-ui-1.9.2.custom.css">

<script src="<?php echo base_url();?>assets/js/jquery-ui/js/jquery-ui.js"></script>

<script src="<?php echo base_url();?>assets/js/timepicker/jquery.ui.timepicker.js"></script>

<link rel="stylesheet" href="<?php echo base_url();?>assets/js/timepicker/jquery.ui.timepicker.css">



<script type="text/javascript">

$(document).ready(function() {

	$('#alamat_ortu').hide();



	// $('#cek_alamat').change(function() {

	// 	$('#alamat_ortu').show();

	// });

$('#tanggal_lahir').datepicker({dateFormat: 'yy-mm-dd',

								yearRange: "1945:2015",

								changeMonth: true,

      							changeYear: true

      							});

$('#tgl_bayar').datepicker({dateFormat: 'yy-mm-dd'});

	

});

</script>



<div class="row">

	<div class="span12">      		  		

  		<div class="widget ">

  			<div class="widget-header">

  				<i class="icon-user"></i>

  				<h3>Form Biodata</h3>

			</div> <!-- /widget-header -->

			

			<div class="widget-content">

				<div class="tabbable">

							

							<br>

							<form id="edit-profile" class="form-horizontal" method="POST" action="<?php echo base_url();?>data/biodata/save_data">

								<div class="tab-content ">

									<div class="tab-pane active" id="formcontrols">

										

									<fieldset>

										<h3 style="text-align:center" >IDENTITAS DIRI</h3>

										<br>

										<input type="hidden" name="bio_type" value="<?php echo $cek; ?>">


											<div class="control-group">											

											<label class="control-label" for="npm">NIK</label>

											<div class="controls">

												<input type="text" class="span6" name="nik" id="nik" value="<?php echo $nopok->nid; ?>" readonly>

												<p class="help-block">

											</div> <!-- /controls -->				

										</div> <!-- /control-group -->

									

										

										<div class="control-group">											

											<label class="control-label" for="nama">Nama Lengkap</label>

											<div class="controls">

												<input type="text" class="span6" placeholder="Isi dengan Nama Lengkap" id="nama" name="nama" value="<?php echo $nopok->nama;  ?>">

											</div> <!-- /controls -->				

										</div> <!-- /control-group -->



										<div class="control-group" id="jekel">

					                        <label class="control-label">Jenis kelamin</label>

					                        <div class="controls">

					                            <input type="radio" name="jekel" value="1" <?php echo ($bio->jenis_kelamin==1)?'checked':'' ?> required/> Laki-Laki

					                            <input type="radio" name="jekel" value="2" <?php echo ($bio->jenis_kelamin==2)?'checked':'' ?> required/> Perempuan

					                        </div>

					                    </div>

										

										

										<div class="control-group">											

											<label class="control-label" for="tempat_lahir">Tempat/Tanggal Lahir</label>

											<div class="controls">

												<input type="text" class="span4" placeholder="Tempat Lahir" id="danu" name="danu" value="<?php echo $bio->tempat_lahir ?>" >

												<input type="text" class="span2" placeholder="Tanggal Lahir" id="tanggal_lahir" name="tanggal_lahir" value="<?php echo $bio->tanggal_lahir ?>" >

											</div> <!-- /controls -->				

										</div> <!-- /control-group -->
	

										<!-- <br />

										<h3 style="text-align:center">IDENTITAS DIRI</h3>

										<br>
 -->
										

										<div class="control-group">											

											<label class="control-label" for="jalan">Jalan/Dusun</label>

											<div class="controls">

												<input type="text" placeholder="Isi dengan nama Jalan" class="span4" id="jalan" name="jalan" value="<?php echo $bio->nama_jalan ?>"  >

											</div> <!-- /controls -->				

										</div> <!-- /control-group -->

										

										

										<div class="control-group">											

											<label class="control-label" for="rtrw">RT/RW</label>

											<div class="controls">

												<input type="text" class="span1" placeholder="RT" id="rt" name="rt" value="<?php echo $bio->rt ?>">

												/

												<input type="text" class="span1" placeholder="RW" id="rw" name="rw" value="<?php echo $bio->rw ?>">

											</div> <!-- /controls -->				

										</div> <!-- /control-group -->



										<div class="control-group">											

											<label class="control-label" for="kel">Kelurahan</label>

											<div class="controls">

												<input type="text" class="span4" placeholder="Kelurahan" id="kel" name="kel" value="<?php echo $bio->kelurahan ?>" >

											</div> <!-- /controls -->				

										</div> <!-- /control-group -->



										<div class="control-group">											

											<label class="control-label" for="kec">Kecamatan</label>

											<div class="controls">

												<input type="text" class="span4" placeholder="Kecamatan" id="kec" name="kec" value="<?php echo $bio->kecamatan ?>" >

											</div> <!-- /controls -->				

										</div> <!-- /control-group -->



										<div class="control-group">											

											<label class="control-label" for="kab">Kabupaten</label>

											<div class="controls">

												<input type="text" class="span4" placeholder="Kabupaten" id="kab" name="kab" value="<?php echo $bio->kabupaten ?>" >

											</div> <!-- /controls -->				

										</div> <!-- /control-group -->



										<div class="control-group">											

											<label class="control-label" for="kota">Kota</label>

											<div class="controls">

												<input type="text" class="span4" id="kota" placeholder="Kota" name="kota" value="<?php echo $bio->kota ?>" >

												<input type="text" class="span2" id="kdpos" placeholder="Kode POS" name="kdpos" value="<?php echo $bio->kode_pos ?>" >

											</div> <!-- /controls -->				

										</div> <!-- /control-group -->



										<div class="control-group">											

											<label class="control-label" for="kab">Provinsi</label>

											<div class="controls">

												<select class="span4" name="provinsi" id="provinsi">

													<option>--Pilih Provinsi--</option>

													<?php foreach ($provinsi->result() as $key): ?>

														<option value="<?php echo $key->lokasi_ID; ?>"><?php echo $key->lokasi_nama; ?></option>

													<?php endforeach ?>

												</select>

											</div> <!-- /controls -->				

										</div> <!-- /control-group -->



										<div class="control-group">											

											<label class="control-label" for="kota">Telepon</label>

											<div class="controls">

												<input type="text" class="span3" id="tlp" placeholder="No Telepon" name="tlp" value="<?php echo $bio->telepon; ?>" >

												<input type="text" class="span3" id="hp" placeholder="No HP" name="hp" value="<?php echo $bio->handphone; ?>" >

											</div> <!-- /controls -->				

										</div> <!-- /control-group -->



										<div class="control-group">											

											<label class="control-label" for="email">Email</label>

											<div class="controls">

												<input type="text" class="span4" placeholder="Email" id="email" name="email" value="<?php echo $bio->email; ?>" >

											</div> <!-- /controls -->				

										</div> <!-- /control-group -->

										

										

                                        

											

										 <br />


										<div class="form-actions">

											<!-- <button type="submit" class="btn btn-primary">UNGGAH</button>

											<button type="submit" class="btn btn-primary">CETAK</button>  -->

											<button type="submit" class="btn btn-primary">SIMPAN</button>

										</div> <!-- /form-actions -->

										

										</fieldset>

									</div>

									</div>

								

								</form>

						</div>

				

			</div>

		</div>

	</div>

</div>