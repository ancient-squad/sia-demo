<?php error_reporting(0); ?>
<div class="row">
	<div class="span12">      		  		
  		<div class="widget ">
  			<div class="widget-header">
  				<i class="icon-user"></i>
  				<h3>Data Ruangan <?php echo $ruang->kode_ruangan.' - '.$ruang->ruangan; ?></h3>
			</div> <!-- /widget-header -->
			
			<div class="widget-content">
				<div class="span11">
					<table id="example1" class="table table-bordered table-striped">
	                	<thead>
	                        <tr> 
	                        	<th>No</th>
	                            <th>Kelas</th>
	                            <th>Prodi</th>
	                            <th>Hari</th>
	                            <th>Jam</th>
	                        </tr>
	                    </thead>
	                    <tbody>
							<?php $no = 1; foreach($getdata as $row){ ?>
							<?php switch ($row->hari) {
								case '1':
									$namahari = 'Senin';
									break;
								case '2':
									$namahari = 'Selasa';
									break;
								case '3':
									$namahari = 'Rabu';
									break;
								case '4':
									$namahari = 'Kamis';
									break;
								case '5':
									$namahari = 'Jumat';
									break;
								case '6':
									$namahari = 'Sabtu';
									break;
								
								default:
									$namahari = 'Minggu';
									break;
							} ?>
	                        <tr>
	                        	<td><?php echo $no; ?></td>
	                        	<td>
	                        		<a href="<?= base_url('datas/ruang/classParticipant/'.$row->id_jadwal) ?>">
	                        			<?php echo $row->kelas; ?>
	                        		</a>
	                        	</td>
	                        	<?php $prodi = $this->app_model->getdetail('tbl_jurusan_prodi','kd_prodi',substr($row->kd_jadwal, 0,5),'kd_prodi','asc')->row()->prodi; ?>
	                        	<td><?php echo $prodi; ?></td>
	                        	<td><?php echo $namahari; ?></td>
	                        	<td><?php echo substr($row->waktu_mulai, 0,5).' - '.substr($row->waktu_selesai, 0,5); ?></td>
	                        </tr>
							<?php $no++;} ?>
	                      
	                    </tbody>
	               	</table>
				</div>
			</div>
		</div>
	</div>
</div>