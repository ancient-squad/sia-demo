<div class="row">
	<div class="span12">      		  		
  		<div class="widget ">
  			<div class="widget-header">
  				<i class="icon-user"></i>
  				<h3>Data Ruangan</h3>
			</div> <!-- /widget-header -->
			
			<div class="widget-content">
				<div class="span11">
					<table>
						<tr>
							<td>Keterangan warna</td>
							<td>:</td>
							<td><button type="button" class="btn" style="background: #7FFFD4">Ruangan di plot di tahun akademik aktif</button></td>
						</tr>
					</table>

					<hr>
					
					<table id="example1" class="table table-bordered table-striped">
	                	<thead>
	                        <tr> 
	                        	<th>No</th>
	                            <th>Lokasi</th>
	                            <th>Lantai</th>
	                            <th>Ruang</th>
	                            <th>Kuota</th>
	                            <th width="40">Aksi</th>
	                        </tr>
	                    </thead>
	                    <tbody>
							<?php $no = 1; foreach($getdata as $row) { ?>
							<?php $actyear = getactyear(); ?>
							<?php $isRoomUsed = $this->db->query("SELECT id_jadwal from tbl_jadwal_matkul where kd_ruangan = '".$row->id_ruangan."' and kd_tahunajaran = '".$actyear."' ")->num_rows(); ?>
							<?php if ($isRoomUsed > 0) {
								$flagColor = "style='background:#7FFFD4'";
							} else {
								$flagColor = "";
							}
							 ?>
	                        <tr>
	                        	<td <?= $flagColor ?> ><?php echo $no; ?></td>
	                        	<td <?= $flagColor ?> ><?php echo $row->gedung; ?></td>
	                        	<td <?= $flagColor ?> ><?php echo $row->lantai; ?></td>
	                        	<td <?= $flagColor ?> ><?php echo $row->ruangan; ?></td>
	                        	<td <?= $flagColor ?> ><?= $row->kuota  ?></td>
	                        	<td class="td-actions" <?= $flagColor ?> >
									<a class="btn btn-small btn-success" href="<?php echo base_url(); ?>datas/ruang/detail/<?php echo $row->id_ruangan; ?>"><i class="btn-icon-only icon-ok"> </i></a>
								</td>
	                        </tr>
							<?php $no++;} ?>
	                      
	                    </tbody>
	               	</table>
				</div>
			</div>
		</div>
	</div>
</div>