<?php
header("Content-Type: application/xls");    
header("Content-Disposition: attachment; filename=data_maba_s2.xls");  
header("Pragma: no-cache"); 
header("Expires: 0");
?>

<style>
table, td, th {
    border: 1px solid black;
}

th {
    background-color: blue;
    color: black;
}
</style>
                    
<table>
	<thead>
        <tr> 
        	<th>No</th>
            <th>Nomor Registrasi</th>
            <th>Nama</th>
            <th>Penghasilan</th>
            <th>Agama</th>
            <th>Status</th>
            <th>NPM</th>
            <th>Prodi</th>
            <th width="120">Gelombang</th>
            <th>Telepon</th>
        </tr>
    </thead>
    <tbody>
		<?php $no = 1; foreach($owe as $row){?>
        <tr>
        	<td><?php echo $no; ?></td>
        	<td><?php echo $row->ID_registrasi; ?></td>
        	<td><?php echo $row->nama; ?></td>
            <td><?php echo $row->nm_univ; ?></td>
            
            <td>
                <?php if ($row->penghasilan == "1-2") {
                    echo "Rp 1,000,000 - 2,000,000";
                } elseif ($row->penghasilan == "2,1-4") {
                    echo "Rp 2,100,000 - 4,000,000";
                } elseif ($row->penghasilan == "4,1-6") {
                    echo "Rp 4,100,000 - 6,000,000";
                } elseif ($row->penghasilan == ">6") {
                    echo "> Rp 6,000,000";
                } else {
                    echo "0";
                }
                ?>
            </td>
            <td><?php echo $row->agama; ?></td>
            <td>
                <?php if ($row->status < 1) {
                    echo "Registrasi";
                } elseif($row->status == 1) {
                    echo "Lulus Tes";
                } elseif ($row->status > 1) {
                    echo "Daftar Ulang";
                }?>
            </td>
            <td><?php echo $row->npm_baru; ?></td>
            <td>
                <?php switch ($row->opsi_prodi_s2) {
                    case '61101':
                        $prods = 'Magister Manajemen';
                        break;
                    case '74101':
                        $prods = 'Magister Hukum';
                        break;
                }
                echo $prods; ?>
            </td>
            <td><?php echo $row->gelombang; ?></td>
            <td><?php echo $row->tlp; ?></td>
        </tr>
		<?php $no++; } ?>
    </tbody>
</table>