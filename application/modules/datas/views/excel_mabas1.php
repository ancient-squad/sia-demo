<?php
header("Content-type: application/vnd-ms-excel");
 
// Mendefinisikan nama file ekspor "hasil-export.xls"
header("Content-Disposition: attachment; filename=export-data-pmb.xls");
?>

<style>
table, td, th {
    border: 1px solid black;
}

th {
    background-color: blue;
    color: black;
}
</style>

<table>
    <thead>
        <tr> 
            <th>No</th>
            <th>ID Registrasi</th>
            <th>Nama</th>
            <th>Negeri/Swasta</th>
            <th>Asal Sekolah</th>
            <th>Program Pilihan</th>
            <th>Kampus</th>
        </tr>
    </thead>
    <tbody>
        <?php $no=1; foreach ($look as $row) : ?>
        <tr>
            <td><?php echo $no;?></td>
            <td><?php echo $row->nomor_registrasi;?></td>
            <td><?php echo $row->nama;?></td>
            <td><?= $row->kategori_skl == 'ngr' ? 'Negeri' : 'Swasta' ?></td>
            <td><?php echo $row->asal_sch_maba; ?></td>
            <td><?php echo $row->prodi;?></td>
            <td><?= $row->kampus == 'bima' ? 'Kota Bima' : 'Kota Bima'; ?></td>
            <td><?php echo $uang;?></td>
        </tr>
        <?php $no++; endforeach; ?>
    </tbody>
</table>