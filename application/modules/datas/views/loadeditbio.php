<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
    <h4 class="modal-title">Edit Biodata Mahasiswa</h4>
</div>
<form id="edit-profile" class="form-horizontal" method="post" action="<?php echo base_url();?>datas/load_edit_bio/updatebaa">
<div class="modal-body">
	<fieldset>

		<div class="control-group">											

			<label class="control-label">NIM Mahasiswa*</label>

			<div class="controls">

				<input type="text" class="span6" name="npm" placeholder="Nomor Induk Mahasiswa" value="<?php echo $mhs->NIMHSMSMHS; ?>"  required>


			</div> <!-- /controls -->				

		</div> <!-- /control-group -->

		<div class="control-group">											

			<label class="control-label">Nama Mahasiswa*</label>

			<div class="controls">

				<input type="text" class="span6" name="nama" placeholder="Nama Mahasiswa" value="<?php echo $mhs->NMMHSMSMHS; ?>"  required >

				
			</div> <!-- /controls -->				

		</div> <!-- /control-group -->

		<?php if (!is_null($kelamin->KDJEKMSMHS) || $kelamin->KDJEKMSMHS != '') { ?>
			
			<div class="control-group">
				<label class="control-label">Jenis Kelamin</label>
				<div class="controls">
					<input type="radio" name="jk" <?php if ($kelamin->KDJEKMSMHS == 'L') { echo "checked=''"; } ?> value="L" required> Laki - Laki <br>
					<input type="radio" name="jk" <?php if ($kelamin->KDJEKMSMHS == 'P') { echo "checked=''"; } ?> value="P"> Perempuan
				</div>
			</div>
		<?php } else { ?>
			<div class="control-group">
				<label class="control-label">Jenis Kelamin</label>
				<div class="controls">
					<input type="radio" name="jk" value="L" required> Laki - Laki <br>
					<input type="radio" name="jk" value="P"> Perempuan
				</div>
			</div>
		<?php } ?>
		

		<div class="control-group">											

			<label class="control-label">Nama Ibu Mahasiswa*</label>

			<div class="controls">

				<input type="text" class="span6" name="ibu" placeholder="Nama Ibu Mahasiswa" value="<?php echo $mhs->nama_ibu; ?>"  required >

				

			</div> <!-- /controls -->				

		</div> <!-- /control-group -->

		<div class="control-group">											

			<label class="control-label">Nomor KTP*</label>

			<div class="controls">

				<input type="text" class="span6" name="nik" placeholder="Nomor Kartu Tanda Penduduk" value="<?php echo $mhs->NIKMSMHS; ?>" minlength=16 required>

			</div> <!-- /controls -->				

		</div> <!-- /control-group -->

		<div class="control-group">											

			<label class="control-label">NISN*</label>

			<div class="controls">

				<input type="text" class="span6" name="nisn" placeholder="Nomor Induk Siswa Nasional" value="<?php echo $mhs->NISNMSMHS; ?>"  required>

				<p class="help-block">Nomor Induk Siswa Nasional dapat anda lihat pada ijazah terakhir.</p>

			</div> <!-- /controls -->				

		</div> <!-- /control-group -->

		<div class="control-group">											

			<label class="control-label">Tempat Lahir*</label>

			<div class="controls">

				<input type="text" class="span6" name="tempat" placeholder="Tempat Lahir Sesuai KTP" value="<?php echo $mhs->TPLHRMSMHS; ?>"  required />
				

			</div> <!-- /controls -->				

		</div> <!-- /control-group -->

		<script>
            $(function() {
                $( "#tgl" ).datepicker({
                    changeMonth: true,
                    changeYear: true,
                    yearRange: "-50:+0",
                    dateFormat: 'yy-mm-dd'
                });
            });
        </script>

		<div class="control-group">											

			<label class="control-label">Tanggal Lahir*</label>

			<div class="controls">

				<input type="text" class="span6" id="tgl" name="tgl" placeholder="Tanggal Lahir Sesuai KTP" value="<?php echo $mhs->TGLHRMSMHS; ?>"  required />
				

			</div> <!-- /controls -->				

		</div> <!-- /control-group -->

		<hr>

		<script>
         $(document).ready(function(){
           $('#prov').change(function(){
             $.post('<?php echo base_url()?>datas/load_edit_bio/get_kota/'+$(this).val(),{},function(get){
               $('#kota').html(get);
             });
           });
         });
        </script>

        <script>
         $(document).ready(function(){
           $('#kota').change(function(){
             $.post('<?php echo base_url()?>datas/load_edit_bio/get_kec/'+$(this).val(),{},function(get){
               $('#kec').html(get);
             });
           });
         });
        </script>
		

		<script>
         $(document).ready(function(){
           $('#kec').change(function(){
             $.post('<?php echo base_url()?>datas/load_edit_bio/get_kel/'+$(this).val(),{},function(get){
               $('#kel').html(get);
             });
           });
         });
        </script>

        <div class="control-group">											

			<label class="control-label">Provinsi*</label>

			<div class="controls">

				<select name="prov" id="prov" class="span6">

					<option>-- pilih provinsi --<option>
					<?php foreach ($provinces as $key) {
						if ($mhs2->provinsi == $key->id) { ?>                
							<option value="<?php echo $key->id; ?>" selected><?php echo $key->name; ?></option>
						<?php }else{ ?>
							<option value="<?php echo $key->id; ?>"><?php echo $key->name; ?></option>
						<?php }
					} ?>
				</select>

			</div> <!-- /controls -->				

		</div> <!-- /control-group -->
		

		<?php if (isset($mhs2->provinsi) || !is_null($mhs2->provinsi) || $mhs2->provinsi <> '') { ?>

		<div class="control-group">											

			<label class="control-label">Kota/Kabupaten*</label>

			<div class="controls">

				<select name="kota" id="kota" class="span6">

					<option>-- pilih Kota/Kabupaten --<option>
					<?php foreach ($kota as $key) {
						if ($mhs2->kota == $key->id) { ?>
							<option value="<?php echo $key->id; ?>" selected><?php echo $key->name; ?></option>
						<?php }else{ ?>
							<option value="<?php echo $key->id; ?>"><?php echo $key->name; ?></option>
						<?php }
					} ?>
				</select>

			</div> <!-- /controls -->				

		</div> <!-- /control-group -->

		<div class="control-group">											

			<label class="control-label">Kecamatan*</label>

			<div class="controls">

				<select name="kecamatan" id="kec" class="span6">

					<option>-- pilih Kecamatan --<option>
					<?php foreach ($kecamatan as $key) {
						if ($mhs2->kecamatan == $key->id) { ?>
							<option value="<?php echo $key->id; ?>" selected><?php echo $key->name; ?></option>
						<?php }else{ ?>
							<option value="<?php echo $key->id; ?>"><?php echo $key->name; ?></option>
						<?php }
					} ?>
				</select>

			</div> <!-- /controls -->				

		</div> <!-- /control-group -->

		<div class="control-group">											

			<label class="control-label">Kelurahan*</label>

			<div class="controls">

				<select name="kelurahan" id="kel" class="span6">

					<option>-- pilih Kecamatan --<option>
					<?php foreach ($kelurahan as $key) {
						if ($mhs2->kelurahan == $key->id) { ?>
							<option value="<?php echo $key->id; ?>" selected><?php echo $key->name; ?></option>
						<?php }else{ ?>
							<option value="<?php echo $key->id; ?>"><?php echo $key->name; ?></option>
						<?php }
					} ?>
				</select>

			</div> <!-- /controls -->				

		</div> <!-- /control-group -->

		<?php } else {  ?>

		<div class="control-group">											

			<label class="control-label">Kota*</label>

			<div class="controls">

				<select class="span6" id="kota" name="kota" required>
                    <option disabled selected> -- pilih kota -- </option>
                </select>

			</div> <!-- /controls -->				

		</div> <!-- /control-group -->

		<div class="control-group">											

			<label class="control-label">Kecamatan*</label>

			<div class="controls">

				<select class="span6" id="kec" name="kecamatan" required>
                    <option disabled selected> -- pilih kecamatan -- </option>
                </select>

			</div> <!-- /controls -->				

		</div> <!-- /control-group -->

		<div class="control-group">											

			<label class="control-label">Kelurahan*</label>

			<div class="controls">

				<select class="span6" id="kel" name="kelurahan" required>
                    <option disabled selected> -- pilih kelurahan -- </option>
                </select>

			</div> <!-- /controls -->				

		</div> <!-- /control-group -->

		<?php } ?>



	

		<div class="control-group">											

			<label class="control-label">Alamat*</label>

			<div class="controls">

				<textarea type="text" class="span6" name="alamat" required><?php echo $mhs2->alamat;?>

				</textarea>

			</div> <!-- /controls -->				

		</div> <!-- /control-group -->

		<hr>

		<div class="control-group">											

			<label class="control-label">Telepon*</label>

			<div class="controls">

				<input type="text" class="span6" name="tlpn" placeholder="Masukan Nomor Telepon" value="<?php echo $mhs2->no_hp;?>"  required>

			</div> <!-- /controls -->				

		</div> <!-- /control-group -->

		<div class="control-group">											

			<label class="control-label">Email*</label>

			<div class="controls">

				<input type="email" class="span6" name="email" placeholder="Masukan Email Mahasiswa" value="<?php echo $mhs2->email;?>"  required>

				<p class="help-block">Wajib di isi untuk pemulihan password akun Sistem Informasi Akademik.</p>

			</div> <!-- /controls -->				

		</div> <!-- /control-group									
		
			
		<div class="form-actions">
			<button type="submit" class="btn btn-primary">Save</button> 
			<button class="btn">Cancel</button>
		</div> <!-- /form-actions -->
	</fieldset>
</div>
<?php $logged = $this->session->userdata('sess_login'); if ($logged['id_user_group'] == 10) { ?>
	<div class="modal-footer">
	    <button type="button" class="btn btn-default" data-dismiss="modal">Keluar</button>
	    <input type="submit" class="btn btn-primary" value="Simpan"/>
	</div>
<?php } ?>

</form>
						
					