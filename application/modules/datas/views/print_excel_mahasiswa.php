<?php
header("Content-Type: application/xls");    
header("Content-Disposition: attachment; filename=data_mahasiswa.xls");  
header("Pragma: no-cache"); 
header("Expires: 0");
?>

<style>
table, td, th {
    border: 1px solid black;
}

th {
    background-color: blue;
    color: black;
}
</style>

<table border='1'>
	<thead>
        <tr>
            <th colspan="7">Daftar Data Mahasiswa</th>
        </tr>
        <tr> 
        	<th>No</th>
            <th>NIM</th>
        	<th>Mahasiswa</th>
            <th>Fakultas</th>
            <th>Jurusan</th>
            <th>Angkatan</th>
            <th>Status</th>
        </tr>
    </thead>
    <tbody>
        <?php $no = 1; foreach ($getData as $row) { ?>
        <tr>
        	<td><?php echo number_format($no); ?></td>
            <td><?php echo $row->NIMHSMSMHS; ?></td>
            <td><?php echo $row->NMMHSMSMHS; ?></td>
            <td><?= get_fak_byprodi($row->KDPSTMSMHS) ?></td>
            <td><?= get_jur($row->KDPSTMSMHS) ?></td>
            <td><?php echo $row->TAHUNMSMHS; ?></td>
            <td><?php switch ($row->STMHSMSMHS) {
                case 'A':
                    $sts = 'Aktif';
                    break;

                case 'C':
                case 'CA':
                    $sts = 'Cuti';
                    break;
                
                case 'K':
                    $sts = 'Keluar';
                    break;

                case 'N':
                case 'NA':
                    $sts = 'Non aktif';
                    break;

                case 'L':
                    $sts = 'Lulus';
                    break;

                default:
                    $sts = 'Dropout';
                    break;
            } echo $sts ?></td>
        </tr>
        <?php $no++; } ?>
    </tbody>
</table>