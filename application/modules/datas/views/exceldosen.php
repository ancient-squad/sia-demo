<?php
header("Content-Type: application/xls");    
header("Content-Disposition: attachment; filename=Export_Data_Dosen.xls");  
header("Pragma: no-cache"); 
header("Expires: 0");
?>

<style>
table, td, th {
    border: 1px solid black;
}

th {
    background-color: blue;
    color: black;
}
</style>

<table>
<thead>
	<tr>
		<th colspan="17" style="text-align: center;">DATA DOSEN</th>
	</tr>
	<tr>
		<td>NO</td>
		<td>NAMA</td>
		<td>NID</td>
		<td>NIDN</td>
		<td>NUPN</td>
		<td>NIK</td>
		<td>JENIS KELAMIN</td>
		<td>ALAMAT</td>
		<td>TELEPON</td>
		<td>E MAIL</td>
		<td>JABATAN FUNGSIONAL</td>
		<td>TANGGAL JABATAN FUNGSIONAL</td>
		<td>PRODI</td>
		<td>STATUS</td>
		<td>DOSEN TETAP</td>
		<td>HOMEBASE</td>
		<td>STRUKTURAL</td>
	</tr>
</thead>
<tbody>
	<?php $no = 1; foreach ($load as $val) { ?>
	<tr>
		<?php 
		if($val->status=='1'){
			$status='Aktif';
		} else {
			$status='Tidak Aktif';
		} 
		if($val->jns_kel=='P'){
			$jenkel='Laki laki';
		} elseif($val->jns_kel=='W'){
			$jenkel='Perempuan';
		} else {
			$jenkel='';
		}
		
		if($val->jabfung=='TPD'){
			$fung='Tenaga Pendidik';
		} elseif($val->jabfung=='SAH'){
			$fung='Asisten Ahli';
		} elseif($val->jabfung=='LKT'){
			$fung='Lektor';
		} elseif($val->jabfung=='LKK'){
			$fung='Lektor Kepala';
		} elseif($val->jabfung=='BIG'){
			$fung='Guru Besar';
		}else{
			$fung='Tidak ada';
		}

		if ($val->homebase == '1') {
			$hb = $this->ORG_NAME;
		} else {
			$hb = 'NON ' . $this->ORG_NAME;
		} 
		if ($val->struktural == '1') {
			$struktural = 'Dosen Struktural';
		} else {
			$struktural = 'Dosen Non Struktural';
		} 
		if ($val->tetap == '1') {
			$tetap = 'DOSEN TETAP';
		} else {
			$tetap = 'DOSEN TIDAK TETAP';
		} 
		?>
		<td><?php echo $no; ?></td>
		<td><?php echo $val->nama; ?></td>
		<td><?php echo $val->nid; ?></td>
		<td><?php echo $val->nidn; ?></td>
		<td><?php echo $val->nupn; ?></td>
		<td><?php echo $val->nik; ?></td>
		<td><?php echo $jenkel; ?></td>
		<td><?php echo $val->alamat; ?></td>
		<td><?php echo $val->hp; ?></td>
		<td><?php echo $val->email; ?></td>
		<td><?php echo $fung; ?></td>
		<td><?php echo $val->tmt_jabfung; ?></td>
		<td><?php echo get_jur($val->jabatan_id); ?></td>
		<td><?php echo $status; ?></td>
		<td><?php echo $tetap; ?></td>
		<td><?php echo $hb; ?></td>
		<td><?php echo $struktural; ?></td>
	</tr>
	<?php $no++; } ?>
</tbody>
</table>