		  		

<div class="widget ">

	<div class="widget-header">

		<i class="icon-user"></i>

		<h3>Data Mahasiswa</h3>

</div> <!-- /widget-header -->



<div class="widget-content" style="padding:30px;">
<div class="tabbable">
<ul class="nav nav-tabs">
	<li><a class="active" href="#tab_aktif" data-toggle="tab">Aktif</a></li>
	<li><a class="active" href="#tab_lulus" data-toggle="tab">Lulus</a></li>
	<li><a class="active" href="#tab_cuti" data-toggle="tab">Cuti/Non Aktif</a></li>
	<li><a class="active" href="#tab_keluar" data-toggle="tab">Keluar</a></li>
</ul>           
<br>

            
  <div class="tab-content">
  	<div class="tab-pane active" id="tab_aktif">
		<div class="span11">
		        <a href="<?php echo base_url(); ?>perkuliahan/jdl_kuliah/cetak_jadwal_all" class="btn btn-info"> Print Data</a>
		        <hr>
				<table id="example1" class="table table-bordered table-striped">
            	<thead>
                    <tr> 
                    	<th>No</th>
                        <th>NPM</th>
                        <th>Nama Mahasiswa</th>
                        <th>Fakultas</th>
                        <th>Jurusan</th>
                        <th>Tahun Masuk</th>
                        <td>Status</td>
                        <th width="80">Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $no = 1; foreach ($aktif as $value) {?>
                    <tr>
                    	<td><?php echo number_format($no); ?></td>
                        <td><?php echo $value->NIMHSMSMHS; ?></td>
                        <td><?php echo $value->NMMHSMSMHS; ?></td>
                        <?php $jurusan=$this->app_model->getdetail('tbl_jurusan_prodi','kd_prodi',$value->KDPSTMSMHS,'kd_prodi','asc')->row(); 
                        $fakultas=$this->app_model->getdetail('tbl_fakultas','kd_fakultas',$jurusan->kd_fakultas,'kd_fakultas','asc')->row(); ?>
                        <td><?php echo $fakultas->fakultas; ?></td>
                        <td><?php echo $jurusan->prodi; ?></td>
                    	<td><?php echo $value->TAHUNMSMHS; ?></td>
                        <?php switch ($value->STMHSMSMHS) {
                            case 'A':
                                $stts = 'AKTIF';
                                break;
                            case 'L':
                                $stts = 'LULUS';
                                break;
                            case 'K':
                                $stts = 'KELUAR';
                                break;
                        } ?>
                        <td><?php echo $stts; ?></td>
                    	<td class="td-actions">
							<a class="btn btn-success btn-small" data-toggle="tooltip" data-placement="right" target="_blank" title="KRS Aktif" href="<?php echo base_url(); ?>datas/mahasiswa/krs_aktif/<?php echo $value->NIMHSMSMHS; ?>" ><i class="btn-icon-only icon-file-text"> </i></a>
                            <!-- <a onclick="edit(<?php //echo $value->NIMHSMSMHS;?>)" class="btn btn-primary btn-small" href="#editModal" data-toggle="modal"><i class="btn-icon-only icon-pencil"> </i></a> -->
							<!-- <a onclick="return confirm('Apakah Anda Yakin?');" class="btn btn-danger btn-small" href="#"><i class="btn-icon-only icon-remove"> </i></a> -->
						</td>
                    </tr>
                    <?php $no++; } ?>
                </tbody>
           	</table>
			</div>
		</div>
		<div class="tab-pane" id="tab_lulus">
		<div class="span11">
		        <a href="<?php echo base_url(); ?>perkuliahan/jdl_kuliah/cetak_jadwal_all" class="btn btn-info"> Print Data</a>
		        <hr>
				<table id="example2" class="table table-bordered table-striped">
            	<thead>
                    <tr> 
                    	<th>No</th>
                        <th>NPM</th>
                        <th>Nama Mahasiswa</th>
                        <th>Fakultas</th>
                        <th>Jurusan</th>
                        <th>Tahun Masuk</th>
                        <td>Status</td>
                        <th width="80">Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $no = 1; foreach ($lulus as $value) {?>
                    <tr>
                    	<td><?php echo number_format($no); ?></td>
                        <td><?php echo $value->NIMHSMSMHS; ?></td>
                        <td><?php echo $value->NMMHSMSMHS; ?></td>
                        <?php $jurusan=$this->app_model->getdetail('tbl_jurusan_prodi','kd_prodi',$value->KDPSTMSMHS,'kd_prodi','asc')->row(); 
                        $fakultas=$this->app_model->getdetail('tbl_fakultas','kd_fakultas',$jurusan->kd_fakultas,'kd_fakultas','asc')->row(); ?>
                        <td><?php echo $fakultas->fakultas; ?></td>
                        <td><?php echo $jurusan->prodi; ?></td>
                    	<td><?php echo $value->TAHUNMSMHS; ?></td>
                        <?php switch ($value->STMHSMSMHS) {
                            case 'A':
                                $stts = 'AKTIF';
                                break;
                            case 'L':
                                $stts = 'LULUS';
                                break;
                            case 'K':
                                $stts = 'KELUAR';
                                break;
                        } ?>
                        <td><?php echo $stts; ?></td>
                    	<td class="td-actions">
							<a class="btn btn-success btn-small" data-toggle="tooltip" data-placement="right" target="_blank" title="Cetak Transkip" href="<?php echo base_url(); ?>datas/mahasiswa/cetak_transkip/<?php echo $value->NIMHSMSMHS; ?>" ><i class="btn-icon-only icon-file-text"> </i></a>
						</td>
                    </tr>
                    <?php $no++; } ?>
                </tbody>
           	</table>
			</div>
		</div>
		<div class="tab-pane" id="tab_cuti">
		<div class="span11">
		        <a href="<?php echo base_url(); ?>perkuliahan/jdl_kuliah/cetak_jadwal_all" class="btn btn-info"> Print Data</a>
		        <hr>
				<table id="example3" class="table table-bordered table-striped">
            	<thead>
                    <tr> 
                    	<th>No</th>
                        <th>NPM</th>
                        <th>Nama Mahasiswa</th>
                        <th>Fakultas</th>
                        <th>Jurusan</th>
                        <th>Tahun Masuk</th>
                        <td>Status</td>
                        <th width="80">Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $no = 1; foreach ($cuti as $value) {?>
                    <tr>
                    	<td><?php echo number_format($no); ?></td>
                        <td><?php echo $value->NIMHSMSMHS; ?></td>
                        <td><?php echo $value->NMMHSMSMHS; ?></td>
                        <?php $jurusan=$this->app_model->getdetail('tbl_jurusan_prodi','kd_prodi',$value->KDPSTMSMHS,'kd_prodi','asc')->row(); 
                        $fakultas=$this->app_model->getdetail('tbl_fakultas','kd_fakultas',$jurusan->kd_fakultas,'kd_fakultas','asc')->row(); ?>
                        <td><?php echo $fakultas->fakultas; ?></td>
                        <td><?php echo $jurusan->prodi; ?></td>
                    	<td><?php echo $value->TAHUNMSMHS; ?></td>
                        <?php switch ($value->STMHSMSMHS) {
                            case 'C':
                                $stts = 'Cuti';
                                break;
                            case 'N':
                                $stts = 'Non Aktif';
                                break;
                            case 'K':
                                $stts = 'KELUAR';
                                break;
                        } ?>
                        <td><?php echo $stts; ?></td>
                    	<td class="td-actions">
							<a class="btn btn-success btn-small" data-toggle="tooltip" data-placement="right" title="Riwayat Studi" href="#" ><i class="btn-icon-only icon-file-text"> </i></a>
                            <!-- <a onclick="edit(<?php //echo $value->NIMHSMSMHS;?>)" class="btn btn-primary btn-small" href="#editModal" data-toggle="modal"><i class="btn-icon-only icon-pencil"> </i></a> -->
							<!-- <a onclick="return confirm('Apakah Anda Yakin?');" class="btn btn-danger btn-small" href="#"><i class="btn-icon-only icon-remove"> </i></a> -->
						</td>
                    </tr>
                    <?php $no++; } ?>
                </tbody>
           	</table>
			</div>
		</div>
		<div class="tab-pane" id="tab_keluar">
		<div class="span11">
		        <a href="<?php echo base_url(); ?>perkuliahan/jdl_kuliah/cetak_jadwal_all" class="btn btn-info"> Print Data</a>
		       	<hr>
				<table id="example4" class="table table-bordered table-striped">
            	<thead>
                    <tr> 
                    	<th>No</th>
                        <th>NPM</th>
                        <th>Nama Mahasiswa</th>
                        <th>Fakultas</th>
                        <th>Jurusan</th>
                        <th>Tahun Masuk</th>
                        <td>Status</td>
                        <th width="80">Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $no = 1; foreach ($keluar as $value) {?>
                    <tr>
                    	<td><?php echo number_format($no); ?></td>
                        <td><?php echo $value->NIMHSMSMHS; ?></td>
                        <td><?php echo $value->NMMHSMSMHS; ?></td>
                        <?php $jurusan=$this->app_model->getdetail('tbl_jurusan_prodi','kd_prodi',$value->KDPSTMSMHS,'kd_prodi','asc')->row(); 
                        $fakultas=$this->app_model->getdetail('tbl_fakultas','kd_fakultas',$jurusan->kd_fakultas,'kd_fakultas','asc')->row(); ?>
                        <td><?php echo $fakultas->fakultas; ?></td>
                        <td><?php echo $jurusan->prodi; ?></td>
                    	<td><?php echo $value->TAHUNMSMHS; ?></td>
                        <?php switch ($value->STMHSMSMHS) {
                            case 'C':
                                $stts = 'Cuti';
                                break;
                            case 'N':
                                $stts = 'Non Aktif';
                                break;
                            case 'K':
                                $stts = 'KELUAR';
                                break;
                        } ?>
                        <td><?php echo $stts; ?></td>
                    	<td class="td-actions">
							<a class="btn btn-success btn-small" data-toggle="tooltip" data-placement="right" title="Riwayat Studi" href="#" ><i class="btn-icon-only icon-file-text"> </i></a>
                            <!-- <a onclick="edit(<?php //echo $value->NIMHSMSMHS;?>)" class="btn btn-primary btn-small" href="#editModal" data-toggle="modal"><i class="btn-icon-only icon-pencil"> </i></a> -->
							<!-- <a onclick="return confirm('Apakah Anda Yakin?');" class="btn btn-danger btn-small" href="#"><i class="btn-icon-only icon-remove"> </i></a> -->
						</td>
                    </tr>
                    <?php $no++; } ?>
                </tbody>
           	</table>
			</div>
		</div>
	</div>
</div> <!-- /widget-content -->