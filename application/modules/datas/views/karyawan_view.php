<script>
    $(document).ready(function() {

        $('#tgl').datepicker({
            dateFormat: "yy-mm-dd",

            yearRange: "1995:<?php echo date('Y'); ?>",

            dateFormat: 'dd/mm/yy',

            changeMonth: true,

            changeYear: true
        });

    });

    function edit(idk) {

        $('#editkary').load('<?php echo base_url(); ?>datas/karyawan/load_edit/' + idk);

    }
</script>
<script language="javascript">
    function numeric(e, decimal) {
        var key;
        var keychar;
        if (window.event) {
            key = window.event.keyCode;
        } else
        if (e) {
            key = e.which;
        } else return true;

        keychar = String.fromCharCode(key);
        if ((key == null) || (key == 0) || (key == 8) || (key == 9) || (key == 13) || (key == 27)) {
            return true;
        } else
        if ((("0123456789").indexOf(keychar) > -1)) {
            return true;
        } else
        if (decimal && (keychar == ".")) {
            return true;
        } else return false;
    }
</script>
<div class="row">

    <div class="span12">

        <div class="widget ">

            <div class="widget-header">

                <i class="icon-user"></i>

                <h3>Data Dosen</h3>

            </div> <!-- /widget-header -->



            <div class="widget-content">

                <div class="span11">
                    <?php if ($crud->create > 0) { ?>
                        <a data-toggle="modal" href="#myModal" class="btn btn-primary"> Tambah Data </a>
                    <?php } ?>
                    <a data-toggle="modal" href="#myModal2" class="btn btn-success"> Export Data </a>

                    <br>
                    <hr>

                    <table id="example1" class="table table-bordered table-striped">

                        <thead>

                            <tr>

                                <th>No</th>

                                <th>NIDN</th>

                                <th>Nama</th>

                                <th>Telepon</th>

                                <th>Prodi</th>

                                <th>Jabatan</th>

                                <th width="120">Aksi</th>

                            </tr>

                        </thead>

                        <tbody>

                            <?php $no = 1;
                            foreach ($karyawan as $row) { ?>

                                <tr>

                                    <td><?php echo $no; ?></td>

                                    <td><?php echo $row->nidn; ?></td>

                                    <td><?php echo $row->nama; ?></td>

                                    <td><?php echo $row->hp; ?></td>

                                    <td><?php echo $row->prodi; ?></td>
                                    <?php if ($row->tetap == '1') {
                                        $x = 'DOSEN TETAP';
                                    } else {
                                        $x = 'DOSEN TIDAK TETAP';
                                    }
                                    ?>
                                    <td><?php echo $x; ?></td>

                                    <td class="td-actions">

                                        <a class="btn btn-success btn-small" href="<?= base_url('datas/karyawan/detlKaryawan/' . $row->nid); ?>">
                                            <i class="btn-icon-only icon-eye-open"> </i>
                                        </a>

                                        <?php if ($crud->edit > 0) { ?>
                                            <a data-toggle="modal" href="#editModal" class="btn btn-primary btn-small" onclick="edit(<?php echo $row->id_kary; ?>)">
                                                <i class="btn-icon-only icon-pencil"> </i>
                                            </a>
                                        <?php } ?>

                                        <?php if ($crud->delete > 0) { ?>
                                            <a onclick="return confirm('Apakah Anda Yakin?');" class="btn btn-danger btn-small" href="<?= base_url('datas/karyawan/del_karyawan/' . $row->id_kary); ?>">
                                                <i class="btn-icon-only icon-remove"> </i>
                                            </a>
                                        <?php } ?>
                                    </td>

                                </tr>

                            <?php $no++;
                            } ?>

                        </tbody>

                    </table>

                </div>

            </div>

        </div>

    </div>

</div>

<div class="modal fade" id="myModal2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">

    <div class="modal-dialog">

        <div class="modal-content">

            <div class="modal-header">

                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>

                <h4 class="modal-title">Ekspor Data Dosen</h4>

            </div>
            <form action="<?= base_url('datas/karyawan/exprt') ?>" method="post">
                <div class="modal-body">
                    <div class="control-group" id="">
                        <label class="control-label">Jenis</label>
                        <div class="controls">
                            <select class="span4" name="tipedosen" id="" class="form-control">
                                <option value="ALL">Semua Dosen</option>
                                <option value="1">Dosen Tetap</option>
                                <option value="NULL">Dosen Tidak Tetap</option>
                                <option value="2">Dosen MKDU</option>
                            </select>
                        </div>
                    </div>
                    <div class="control-group" id="">
                        <label class="control-label">Jabatan Fungsional</label>
                        <div class="controls">
                            <select class="span4" name="jabfung" id="" class="form-control">
                                <option value="ALL">Semua</option>
                                <option value="TPD">Tenaga Pendidik</option>
                                <option value="SAH">Asisten Ahli</option>
                                <option value="LKT">Lektor</option>
                                <option value="LKK">Lektor Kepala</option>
                                <option value="BIG">Guru Besar</option>
                            </select>
                        </div>
                    </div>
                    <div class="control-group" id="">
                        <label class="control-label">Homebase</label>
                        <div class="controls">
                            <select class="span4" name="home" id="lembaga" class="form-control" required>
                                <option value="1">Homebase</option>
                                <option value="ALL">Mengajar</option>
                            </select>
                        </div>
                    </div>
                    <div class="control-group" id="">
                        <label class="control-label">Prodi</label>
                        <div class="controls">
                            <select class="span4" name="prodi" id="lembaga" class="form-control" required>
                                <?php foreach ($divisi->result() as $row) { ?>
                                    <option value="<?= $row->kd_prodi; ?>"><?= $row->prodi; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Keluar</button>
                    <input type="submit" class="btn btn-primary" value="Export" />
                </div>
            </form>

        </div><!-- /.modal-content -->

    </div><!-- /.modal-dialog -->

</div><!-- /.modal -->



<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">

    <div class="modal-dialog">

        <div class="modal-content">

            <div class="modal-header">

                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>

                <h4 class="modal-title">Tambah Data Dosen</h4>

            </div>

            <form class='form-horizontal' action="<?= base_url(); ?>datas/karyawan/save_karyawan" method="post" enctype="multipart/form-data">

                <div class="modal-body" style="margin-left: -30px;">

                    <div class="control-group" id="">

                        <label class="control-label">NID</label>

                        <div class="controls">

                            <input type="text" class="span4" name="nid" placeholder="Input Nomor Identitas" class="form-control" value="" />

                        </div>

                    </div>

                    <div class="control-group" id="">

                        <label class="control-label">NIDN</label>

                        <div class="controls">

                            <input type="text" class="span4" name="nidn" placeholder="Input Nomor Identitas" class="form-control" value="" />

                        </div>

                    </div>



                    <div class="control-group" id="">

                        <label class="control-label">NUPN / NIDK</label>

                        <div class="controls">

                            <input type="text" class="span4" name="nupn" placeholder="Input Nomor Identitas" class="form-control" value="" />

                        </div>

                    </div>

                    <div class="control-group" id="">

                        <label class="control-label">Nama</label>

                        <div class="controls">

                            <input type="text" class="span4" name="nama" placeholder="Input Nama" class="form-control" value="" />

                        </div>

                    </div>

                    <div class="control-group" id="">

                        <label class="control-label">Jenis Kelamin</label>

                        <div class="controls">

                            <label class="radio inline">

                                <input type="radio" name="jk" value="P" />

                                Pria

                            </label>

                            <label class="radio inline">

                                <input type="radio" name="jk" value="W" />

                                Wanita

                            </label>

                        </div>

                    </div>

                    <div class="control-group" id="">

                        <label class="control-label">Telepon</label>

                        <div class="controls">

                            <input type="text" class="span4" name="telepon" id="telepon" onkeypress="return numeric(event, false)" placeholder="Input Telepon" class="form-control" value="" />

                        </div>

                    </div>

                    <div class="control-group" id="">

                        <label class="control-label">Email</label>

                        <div class="controls">

                            <input type="email" class="span4" name="email" placeholder="Input Email" class="form-control" value="" />

                        </div>

                    </div>

                    <div class="control-group" id="">

                        <label class="control-label">Alamat</label>

                        <div class="controls">

                            <textarea class="span4" name="alamat" placeholder="Input Alamat" class="form-control" value=""></textarea>

                        </div>

                    </div>

                    <div class="control-group" id="">
                        <label class="control-label">Jabatan Fungsional</label>
                        <div class="controls">
                            <select class="span4" name="jabfung" id="lembaga" class="form-control">
                                <option selected disabled> -- </option>
                                <option value="TPD">Tenaga Pendidik</option>
                                <option value="SAH">Asisten Ahli</option>
                                <option value="LKT">Lektor</option>
                                <option value="LKK">Lektor Kepala</option>
                                <option value="BIG">Guru Besar</option>
                            </select>
                        </div>
                    </div>

                    <div class="control-group" id="">

                        <label class="control-label">TMT Jabfung</label>

                        <div class="controls">

                            <input type="date" class="span4" name="tmt" placeholder="Input TMT Jabatan Fungsional" class="form-control" value="" />

                        </div>

                    </div>

                    <div class="control-group" id="">

                        <label class="control-label">Prodi</label>

                        <div class="controls">

                            <select class="span4" name="prodi" id="lembaga" class="form-control" value="">

                                <option selected disabled> -- </option>

                                <?php foreach ($divisi->result() as $row) { ?>

                                    <option value="<?php echo $row->kd_prodi; ?>"><?php echo $row->prodi; ?></option>

                                <?php } ?>

                            </select>

                        </div>

                    </div>



                    <div class="control-group" id="">

                        <label class="control-label">Status</label>

                        <div class="controls">

                            <label class="radio inline">

                                <input type="radio" name="status" value="1" />

                                Tetap

                            </label>

                            <label class="radio inline">

                                <input type="radio" name="status" value="NULL" />

                                Tidak Tetap

                            </label>

                            <label class="radio inline">

                                <input type="radio" name="status" value="2" />

                                MKDU

                            </label>

                        </div>

                    </div>


                    <div class="control-group" id="">

                        <label class="control-label">Status Aktif</label>

                        <div class="controls">

                            <label class="radio inline">

                                <input type="radio" name="aktf" value="1" />

                                Aktif

                            </label>

                            <label class="radio inline">

                                <input type="radio" name="aktf" value="0" />

                                Non aktif

                            </label>

                        </div>

                    </div>


                    <div class="control-group" id="">

                        <label class="control-label">Homebase</label>

                        <div class="controls">

                            <label class="radio inline">

                                <input type="radio" name="homebase" value="1" />

                                Ya

                            </label>

                            <label class="radio inline">

                                <input type="radio" name="homebase" value="NULL" />

                                Tidak

                            </label>

                        </div>

                    </div>

                    <div class="control-group" id="">

                        <label class="control-label">Struktural</label>

                        <div class="controls">

                            <label class="radio inline">

                                <input type="radio" name="struktural" value="1" />

                                Ya

                            </label>

                            <label class="radio inline">

                                <input type="radio" name="struktural" value="NULL" />

                                Tidak

                            </label>

                        </div>

                    </div>

                </div>

                <div class="modal-footer">

                    <button type="button" class="btn btn-default" data-dismiss="modal">Keluar</button>

                    <input type="submit" class="btn btn-primary" value="Simpan" />

                </div>

            </form>

        </div><!-- /.modal-content -->

    </div><!-- /.modal-dialog -->

</div><!-- /.modal -->



<div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">

    <div class="modal-dialog">

        <div class="modal-content" id="editkary">



        </div><!-- /.modal-content -->

    </div><!-- /.modal-dialog -->

</div><!-- /.modal -->