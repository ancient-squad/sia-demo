<script>
function edit(id){
$('#myModal1').load('<?php echo base_url();?>datas/mahasiswapmb/load_lengkap2/'+id);
}
</script>

<div class="row">
	<div class="span12">      		  		
  		<div class="widget ">
  			<div class="widget-header">
  				<i class="icon-list"></i>
  				<h3>Daftar Calon Mahasiswa S2</h3>
			</div> <!-- /widget-header -->
			
			<div class="widget-content">
				<div class="span11">
                    <?php 
                    $logged = $this->session->userdata('sess_login');
                    $pecah = explode(',', $logged['id_user_group']);
                    $jmlh = count($pecah);
                    for ($i=0; $i < $jmlh; $i++) { 
                        $grup[] = $pecah[$i];
                    }
                    if ( (in_array(13, $grup))) { ?>
                        <a data-toggle="modal" href="#myModal" class="btn btn-success"><i class="icon-excel"></i> Print Harian</a> 
                        <a data-toggle="modal" href="#yuModal" class="btn btn-primary"><i class="icon-excel"></i> Print All</a>
                        <a href="<?php echo base_url(); ?>datas/mahasiswapmb/printstatuss2" class="btn btn-warning"><i class="icon-excel"></i> Print Status Kelengkapan</a><br>
                    <?php } ?>
                    <a href="<?php echo base_url(); ?>datas/mahasiswapmb/cetak" class="btn btn-success">Cetak Excel</a>
                    <!-- <a href="<?php //echo base_url(); ?>datas/mahasiswapmb/printexcels2" class="btn btn-primary"><i class="icon-excel"></i> Print All</a> --><br><hr>
					<table id="example1" class="table table-bordered table-striped">
	                	<thead>
	                        <tr> 
	                        	<th width="40">No</th>
	                        	<th>ID Registrasi</th>
                                <th>Nama</th>
                                <th width="150">Program Pilihan</th>
                                <th width="140">Asal Universitas</th>
                                <th>Status</th>
                                <?php if ( (in_array(13, $grup))) { ?>
                                    <th>Aksi</th>
                                <?php } ?>
                                <th>Berkas</th>
	                        </tr>
	                    </thead>
	                    <tbody>
                            <?php $no=1; foreach ($look as $row) { ?>
	                        <tr>
                                <td><?php echo $no;?></td>
	                        	<td><?php echo $row->ID_registrasi;?></td>
	                        	<td><?php echo $row->nama;?></td>
	                        	<td><?php echo $row->prodi;?></td>
                                <td><?php echo $row->nm_univ;?></td>
                                <?php if ($row->status == 1) {
                                    $st = 'Lulus Tes';
                                } elseif ($row->status > 1) {
                                    $st = 'Daftar Ulang';
                                } else {
                                    $st = 'Registrasi';
                                }
                                 ?>
                                <td><?php echo $st; ?></td>
                                <?php if ( (in_array(13, $grup))) { ?>
	                        	<td>
                                    <a href="<?php echo base_url(); ?>datas/mahasiswapmb/detils2/<?php echo $row->ID_registrasi; ?>" class="btn btn-success"><i class="icon-check"></i></a>
                                    <a data-toggle="modal" onclick="edit(<?php echo $row->id_s2; ?>)" href="#myModal1" class="btn btn-primary"><i class="icon-ok"></i></a>
                                </td>
	                           <?php } ?>
                               <?php $ask = $this->db->query("select status_kelengkapan from tbl_pmb_s2 where ((status_kelengkapan LIKE '%FT%' AND status_kelengkapan LIKE '%SKL%' AND status_kelengkapan LIKE '%IJZ%' AND status_kelengkapan LIKE '%SKHUN%' AND status_kelengkapan LIKE '%RP%' AND status_kelengkapan LIKE '%KTP%' AND status_kelengkapan LIKE '%KK%' AND status_kelengkapan LIKE '%AKT%') OR status_kelengkapan LIKE '%LLKP%') AND ID_registrasi = '".$row->ID_registrasi."'")->result(); if ($ask == true) { ?>
                                    <td><i class="icon-ok"></i></td>
                                <?php } else { ?>
                                    <td> - </td>
                                <?php } ?>
                            </tr>
                            <?php $no++; } ?>
							
	                    </tbody>
	               	</table>
				</div>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery-ui/js/jquery-ui.js"></script>
<script>
   $(document).ready(function() {
     $( "#tgl" ).datepicker({
          changeMonth: true,
          changeYear: true,
          dateFormat: "yy-mm-dd"
      });
   });
      
</script>
<div class="modal fade" id="myModal1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">

    <div class="modal-dialog">

        <div class="modal-content" id="absen">

            

        </div><!-- /.modal-content -->

    </div><!-- /.modal-dialog -->

</div><!-- /.modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">

    <div class="modal-dialog">

        <div class="modal-content">

            <div class="modal-header">

                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>

                <h4 class="modal-title">Print Data Pendaftaran</h4>

            </div>

            <form class ='form-horizontal' action="<?php echo base_url(); ?>datas/mahasiswapmb/printexcelharis2" method="post" enctype="multipart/form-data">

                <div class="modal-body" style="margin-left: 30px;">   
                	<div class="control-group" id="">
	                	<label class="control-label">Tanggal </label>
	                	<div class="controls">
		                	<input type="text" id="tgl" name="rekap_hari"  class="form-control" placeholder="">
		                </div>
		            </div>
                    <div class="control-group" id="">
	                	<label class="control-label">Fakultas </label>
	                	<div class="controls">
		                	<select name="fakultas" class="form-control">
		                		<option value="6">Pascasarjana</option>
		                	</select>
		                </div>
                    </div>
				</div>
                <div class="modal-footer">

                    <button type="button" class="btn btn-default" data-dismiss="modal">Keluar</button>

                    <input type="submit" class="btn btn-primary" value="Simpan"/>

                </div>

            </form>

        </div><!-- /.modal-content -->

    </div><!-- /.modal-dialog -->

</div><!-- /.modal -->

<div class="modal fade" id="yuModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">

    <div class="modal-dialog">

        <div class="modal-content">

            <div class="modal-header">

                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>

                <h4 class="modal-title">Print Data Pendaftaran</h4>

            </div>

            <form class ='form-horizontal' action="<?php echo base_url(); ?>datas/mahasiswapmb/printexcels2" method="post" enctype="multipart/form-data">
                <div class="modal-body" style="margin-left: 30px;">   
                	<div class="control-group" id="">
	                	<label class="control-label">Fakultas </label>
	                	<div class="controls">
		                	<select name="fakultas" class="form-control">
		                		<option value="6">Pascasarjana</option>
		                	</select>
		                </div>
                    </div>
				</div>
                <div class="modal-footer">

                    <button type="button" class="btn btn-default" data-dismiss="modal">Keluar</button>

                    <input type="submit" class="btn btn-primary" value="Simpan"/>

                </div>

            </form>

        </div><!-- /.modal-content -->

    </div><!-- /.modal-dialog -->

</div><!-- /.modal -->