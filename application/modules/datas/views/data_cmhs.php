<div class="row">
	<div class="span12">      		  		
  		<div class="widget ">
  			<div class="widget-header">
                <i class="icon-home"></i>
  				<h3>Data PMB</h3>
			</div>
			<div class="widget-content">
				<form class="form-horizontal" action="<?php echo base_url(); ?>datas/mahasiswapmb/simpan_sesi" method="post">
					<fieldset>
						<?php 
		                    $logged = $this->session->userdata('sess_login');
		                    $pecah = $logged['id_user_group'];
		                    $ses = $logged['userid'];
		                    
		                    if ($pecah == '8') {?>
		                        	<input type="hidden" name="jenjang" value="s1" placeholder="">
		                       
		                        <input type="hidden" name="prodi" value="<?php echo $ses; ?>">
		                        <div class="control-group">
									<label class="control-label">Pilih Tahun</label>
									<div class="controls">
										<select class="form-control span4" name="tahun" required>
											<option disabled selected value="">--Pilih Tahun--</option>
											<?php for ($i=date("Y")-1; $i < (date("Y")+2); $i++) { 
												echo "<option value=".$i.">".$i."</option>";
											} ?>
										</select>
									</div>
								</div>
	                		<?php } else { ?>
	                			<div class="control-group">
									<label class="control-label">Pilih Tahun</label>
									<div class="controls">
										<select class="form-control span4" name="tahun" required>
											<option disabled selected value="">--Pilih Tahun--</option>
											<?php for ($i=date("Y")-1; $i < (date("Y")+2); $i++) { 
												echo "<option value=".$i.">".$i."</option>";
											} ?>
										</select>
									</div>
								</div>
	                			<div class="control-group">
									<label class="control-label">Pilih Pendaftaran</label>
									<div class="controls">
										<select class="form-control span4" name="jenjang" required>
											<option disabled selected value="">--Pilih Pendaftaran--</option>
											<option value="s1">S1</option>
										</select>
									</div>
								</div>
	                	<?php } ?>
						<!-- <div class="control-group">
							<label class="control-label">Pilih Pendaftaran</label>
							<div class="controls">
								<select class="form-control span4" name="jenjang" required/>
									<option disabled selected value="">--Pilih Pendaftaran--</option>
									<option value="s1">S1</option>
									<option value="s2">S2</option>
								</select>
							</div>
						</div> -->
						<div class="control-group">
							<label class="control-label">Pilih Jenis</label>
							<div class="controls">
								<select class="form-control span4" name="jenis" required>
									<option disabled selected value="">--Pilih Jenis--</option>
									<option value="ALL">Semua Jenis</option>
									<option value="KV">Konversi</option>
									<option value="MB">Mahasiswa Baru</option>
								</select>
							</div>
						</div>
						<div class="control-group">
							<label class="control-label">Pilih Gelombang</label>
							<div class="controls">
								<select class="form-control span4" name="gel" required>
									<option disabled selected value="">--Pilih Gelombang--</option>
									<!-- <option value="0">Semua Gelombang</option>
									<option value="1">1</option>
									<option value="2">2</option>
									<option value="3">3</option>
									<option value="4">4</option>
									<option value="5">Ekstra</option> -->
									<?php
									foreach($gelombang as $value){
										echo "<option value=".$value->gelombang.">".$value->gelombang."</option>";
									}
									?>
								</select>
							</div>
						</div>
						<div class="form-actions">
							<input class="btn btn-large btn-primary" type="submit" value="Submit">
							<input class="btn btn-large btn-default" type="reset" value="Clear">
						</div>
					</fieldset>
				</form>
			</div>
		</div>
	</div>
</div>

