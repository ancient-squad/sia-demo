<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mahasiswa extends MY_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->library('Cfpdf');
		// error_reporting(0);
		if ($this->session->userdata('sess_login') == TRUE) {
			$akses = $this->role_model->cekakses(32)->result();
			if ($akses != TRUE) {
				redirect('home','refresh');
			}
		} else {
			redirect('auth','refresh');
		}
	}

	function index()
	{
        $this->session->unset_userdata('tahun');
        $this->session->unset_userdata('jurusan');

        $logged = $this->session->userdata('sess_login');
        $pecah = explode(',', $logged['id_user_group']);
        $jmlh = count($pecah);

        for ($i=0; $i < $jmlh; $i++) { 
            $grup[] = $pecah[$i];
        }
        
        if ((in_array(10, $grup)) or (in_array(11, $grup))) { //baa
            $data['fakultas'] = $this->app_model->getdata('tbl_fakultas', 'id_fakultas', 'ASC')->result();

            $data['page'] = 'v_data_mhs_baa';

        } elseif ((in_array(9, $grup))) { // fakultas
            $data['jurusan']=$this->app_model->getdetail('tbl_jurusan_prodi','kd_fakultas',$logged['userid'],'prodi','asc')->result();

            $data['page'] = 'v_data_mhs_fak';

        } elseif ((in_array(8, $grup)) or (in_array(19, $grup))) { // prodi
            $data['jurusan']=$this->db->where('kd_prodi',$logged['userid'])->get('tbl_jurusan_prodi')->result();
            $data['angkatan'] = $this->db->query("SELECT DISTINCT mhs.`TAHUNMSMHS` FROM tbl_mahasiswa mhs 
										WHERE mhs.`KDPSTMSMHS` = ".$logged['userid']." 
										ORDER BY mhs.`TAHUNMSMHS` ASC ")->result();

            $data['page'] = 'v_data_mhs_prodi';

        } elseif ((in_array(1, $grup))) { // admin
            $data['fakultas']=$this->app_model->getdata('tbl_fakultas', 'id_fakultas', 'ASC')->result();
            $data['akademik'] = $this->db->get('tbl_tahunakademik')->result();

            $data['page'] = 'v_data_mhs_baa';
        }

        $this->load->view('template/template', $data);   
    }

    function view(){
        $jurusan = $this->input->post('jurusan');
        $tahun = $this->input->post('tahun');

        $this->session->set_userdata('tahun',$tahun);
        $this->session->set_userdata('jurusan',$jurusan);

        redirect(base_url().'datas/mahasiswa/view_mhs','refresh');
    }

    function view_mhs(){
    	$logged = $this->session->userdata('sess_login');
    	$jur = $this->session->userdata('jurusan');
    	$thn = $this->session->userdata('tahun');
    	$data['crud'] = $this->db->query('SELECT * from tbl_role_access where menu_id = 32 and user_group_id = '.$logged['id_user_group'].' ')->row();
    	$exp_jur = explode('_', $jur);

        $data['getData']=$this->db->where('KDPSTMSMHS',$exp_jur[0])->where('TAHUNMSMHS',$thn)->get('tbl_mahasiswa')->result();

        $data['page']='mahasiswa_lihat';
        $this->load->view('template/template', $data);
    }


	function view_mhs2(){
		$sess_prodi = $this->input->post('jurusan', TRUE);
		$this->session->set_userdata('prodi',$sess_prodi);
		$data['getData']=$this->app_model->getdetail('tbl_mahasiswa','KDPSTMSMHS',$sess_prodi,'NIMHSMSMHS','asc')->result();
		$data['page'] = 'mahasiswa_lihat';
		$this->load->view('template/template',$data);
	}

	function view_mhs_tab(){
		$jur = $this->session->userdata('jurusan');
    	$thn = $this->session->userdata('tahun');
    	$exp_jur = explode('_', $jur);

        $data['aktif']=$this->db->where('KDPSTMSMHS',$exp_jur[0])->where('TAHUNMSMHS',$thn)->where('STMHSMSMHS','A')->get('tbl_mahasiswa')->result();
        $data['lulus']=$this->db->where('KDPSTMSMHS',$exp_jur[0])->where('TAHUNMSMHS',$thn)->where('STMHSMSMHS','L')->get('tbl_mahasiswa')->result();
        $data['cuti']=$this->db->where('KDPSTMSMHS',$exp_jur[0])->where('TAHUNMSMHS',$thn)
        						->where('STMHSMSMHS','C')
        						->get('tbl_mahasiswa')->result();
        $data['keluar']=$this->db->where('KDPSTMSMHS',$exp_jur[0])->where('TAHUNMSMHS',$thn)->where('STMHSMSMHS','K')->get('tbl_mahasiswa')->result();

		$data['page'] = 'mahasiswa_lihat_tab';
		$this->load->view('template/template',$data);
	}

	function krs_aktif($id){
		$kd_krs = $this->db->where('npm_mahasiswa',$id)
						->order_by('tahunajaran','DESC')
						->get('tbl_verifikasi_krs', 1)->row()->kd_krs;

		$data['pembimbing']=$this->app_model->get_pembimbing_krs($kd_krs)->row_array();
		$data['detail_krs'] = $this->app_model->get_detail_krs_mahasiswa($kd_krs)->result();
		$data['mhs'] = $this->db->where('NIMHSMSMHS',$id)->get('tbl_mahasiswa', 1)->row();

		$data['kd_krs'] = $kd_krs;
		$data['page'] = 'krs_aktif';
		$this->load->view('template/template',$data);
	}

	function cetak_transkip($id){
		$data['head'] = $this->db->query('SELECT * from tbl_mahasiswa a join tbl_jurusan_prodi b
										on a.`KDPSTMSMHS`=b.`kd_prodi` join tbl_fakultas c
										on b.`kd_fakultas`=c.`kd_fakultas`
										where a.`NIMHSMSMHS` = "'.$id.'"')->row();

		$data['q'] = $this->db->query('SELECT nl.`KDKMKTRLNM`,
										IF(nl.`THSMSTRLNM`<"20151",(SELECT DISTINCT nama_matakuliah FROM tbl_matakuliah_copy WHERE kd_matakuliah = nl.`KDKMKTRLNM` AND kd_prodi = nl.`KDPSTTRLNM` AND tahunakademik = nl.`THSMSTRLNM` ORDER BY id_matakuliah DESC LIMIT 1) ,
											(SELECT DISTINCT nama_matakuliah FROM tbl_matakuliah WHERE kd_matakuliah = nl.`KDKMKTRLNM` AND kd_prodi = nl.`KDPSTTRLNM` ORDER BY id_matakuliah DESC LIMIT 1) ) AS nama_matakuliah,
										IF(nl.`THSMSTRLNM`<"20151",(SELECT DISTINCT sks_matakuliah FROM tbl_matakuliah_copy WHERE kd_matakuliah = nl.`KDKMKTRLNM` AND kd_prodi = nl.`KDPSTTRLNM` AND tahunakademik = nl.`THSMSTRLNM` ORDER BY id_matakuliah DESC LIMIT 1) ,
											(SELECT DISTINCT sks_matakuliah FROM tbl_matakuliah WHERE kd_matakuliah = nl.`KDKMKTRLNM` AND kd_prodi = nl.`KDPSTTRLNM` ORDER BY id_matakuliah DESC LIMIT 1) ) AS sks_matakuliah,
										MIN(nl.`NLAKHTRLNM`) AS NLAKHTRLNM,MAX(nl.`THSMSTRLNM`) AS THSMSTRLNM,MAX(nl.`BOBOTTRLNM`) AS BOBOTTRLNM FROM tbl_transaksi_nilai nl 
										WHERE nl.`NIMHSTRLNM` = "'.$id.'"
										GROUP BY nl.`KDKMKTRLNM` ORDER BY THSMSTRLNM ASC
										')->result();

		$data['npm'] = $id;

		//var_dump($data['q']);die();

		$this->load->view('transkrip_pdf', $data);
	}

	function get_angkatan($id){
		$jur = $this->session->userdata('jurusan');

        $jurusan = $this->db->query("SELECT DISTINCT mhs.`TAHUNMSMHS` FROM tbl_mahasiswa mhs 
										WHERE mhs.`KDPSTMSMHS` = ".$id." 
										ORDER BY mhs.`TAHUNMSMHS` ASC ")->result();

		$out = "<select class='form-control' name='tahun' id='angk'><option>--Pilih Angkatan--</option>";
        foreach ($jurusan as $row) {
            $out .= "<option value='".$row->TAHUNMSMHS."'>".$row->TAHUNMSMHS. "</option>";
        }
        $out .= "</select>";
        echo $out;
	}

	function get_jurusan($id){
        $jurusan = $this->app_model->getdetail('tbl_jurusan_prodi', 'kd_fakultas', $id, 'id_prodi', 'ASC')->result();
		$out = "<select class='form-control' name='jurusan' id='jurs'><option>--Pilih Jurusan--</option>";
        foreach ($jurusan as $row) {
            $out .= "<option value='".$row->kd_prodi."'>".$row->prodi. "</option>";
        }
        $out .= "</select>";
        echo $out;
	}

	function printdata()
	{
		$logged = $this->session->userdata('sess_login');
		$pecah = explode(',', $logged['id_user_group']);
		$jmlh = count($pecah);
		for ($i=0; $i < $jmlh; $i++) { 
			$grup[] = $pecah[$i];
		}
		if ((in_array(8, $grup))) {
			$data['getData']=$this->app_model->getdetail('tbl_mahasiswa','KDPSTMSMHS',$logged['userid'],'NIMHSMSMHS','asc')->result();
		} elseif (in_array(10, $grup) OR in_array(1, $grup)) {
			$data['getData']=$this->app_model->getdetail('tbl_mahasiswa','KDPSTMSMHS',$this->session->userdata('jurusan'),'NIMHSMSMHS','asc')->result();
		} else {
			$data['getData']=$this->app_model->getdetail('tbl_mahasiswa','KDPSTMSMHS',$this->session->userdata('prodi'),'NIMHSMSMHS','asc')->result();
		}
		$this->load->view('print_excel_mahasiswa', $data);
	}

}

/* End of file mahasiswa.php */
/* Location: ./application/modules/data/controllers/mahasiswa.php */