<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Load_edit_bio extends MY_Controller {

	function __construct()
	{
		parent::__construct();
		error_reporting(0);
		//$this->load->model('app_model2');
		//$this->db2 = $this->load->database('sqlserver', TRUE);
		//$id_menu = 32 (database); cek apakah user memiliki akses
		// if ($this->session->userdata('sess_login') == TRUE) {
		// 	$akses = $this->role_model->cekakses(32)->result();
		// 	if ($akses != TRUE) {
		// 		redirect('home','refresh');
		// 	}
		// } else {
		// 	redirect('auth','refresh');
		// }
	}

	public function index()
	{
		
	}

	function load($nim)
	{
		$data['provinces']=$this->app_model->getdata('tbl_provinsis','id', 'ASC')->result();
		$data['mhs2'] = $this->app_model->getdetail('tbl_bio_mhs','npm',$nim,'npm','asc')->row();
		$data['kelamin'] = $this->app_model->getdetail('tbl_mahasiswa','NIMHSMSMHS',$nim,'NIMHSMSMHS','asc')->row();
		$data['kota']=$this->app_model->getdata('tbl_kokab','id', 'ASC')->result();
		$data['kecamatan']=$this->app_model->getdata('tbl_kecamatans','id', 'ASC')->result();
		$data['kelurahan']=$this->app_model->getdata('tbl_kelurahans','id', 'ASC')->result();
		$data['mhs'] = $this->app_model->editbio($nim)->row();
		$this->load->view('loadeditbio', $data);
	}

	function get_kota($id)
	{
        $kota = $this->app_model->getdetail('tbl_kokab', 'province_id', $id, 'id', 'ASC')->result();
		$out = "<select class='form-control' name='kota' id='kota'>";
        foreach ($kota as $row) {
            $out .= "<option value='".$row->id."'>".$row->name. "</option>";
        }
        $out .= "</select>";
        echo $out;
	}

	function get_kec($id)
	{
        $kota = $this->app_model->getdetail('tbl_kecamatans', 'regency_id', $id, 'id', 'ASC')->result();
		$out = "<select class='form-control' name='kec' id='kec'>";
        foreach ($kota as $row) {
            $out .= "<option value='".$row->id."'>".$row->name. "</option>";
        }
        $out .= "</select>";
        echo $out;
	}

	function get_kel($id)
	{
        $kota = $this->app_model->getdetail('tbl_kelurahans', 'district_id', $id, 'id', 'ASC')->result();
		$out = "<select class='form-control' name='kel' id='kel'>";
        foreach ($kota as $row) {
            $out .= "<option value='".$row->id."'>".$row->name. "</option>";
        }
        $out .= "</select>";
        echo $out;
	}

	function updatebaa()
	{
		// var_dump($this->input->post('tgl'));exit();
		$tblmhs['NIMHSMSMHS']	= $this->input->post('npm');
		$tblmhs['NMMHSMSMHS']	= strtoupper($this->input->post('nama'));
		$tblmhs['NIKMSMHS']		= $this->input->post('nik');
		$tblmhs['NISNMSMHS']	= $this->input->post('nisn');
		$tblmhs['TPLHRMSMHS']	= strtoupper($this->input->post('tempat'));
		$tblmhs['TGLHRMSMHS']	= $this->input->post('tgl');
		$tblmhs['NMIBUMSMHS']	= strtoupper($this->input->post('ibu'));
		$tblmhs['KDJEKMSMHS']	= $this->input->post('jk');

		$this->app_model->updatedata('tbl_mahasiswa','NIMHSMSMHS',$this->input->post('npm'),$tblmhs);

		$cek = $this->db->query("SELECT * from tbl_bio_mhs where npm = '".$this->input->post('npm')."'")->row();
		if ($cek->npm == NULL) {
			$datt = array(
					'npm'			=> $this->input->post('npm'),
					'nama_ibu'		=> strtoupper($this->input->post('ibu')),
					'provinsi'		=> $this->input->post('prov'),
					'kota'			=> $this->input->post('kota'),
					'kecamatan'		=> $this->input->post('kecamatan'),
					'kelurahan'		=> $this->input->post('kelurahan'),
					'alamat'		=> $this->input->post('alamat'),
					'no_hp'			=> $this->input->post('tlpn'),
					'email'			=> $this->input->post('email'),
					'tgl_update'	=> date('Y-m-d H:i:s')
				);
			// var_dump($datt);exit();
			$this->db->insert('tbl_bio_mhs',$datt);
		} else {
			$datt = array(
					'npm'			=> $this->input->post('npm'),
					'nama_ibu'		=> strtoupper($this->input->post('ibu')),
					'provinsi'		=> $this->input->post('prov'),
					'kota'			=> $this->input->post('kota'),
					'kecamatan'		=> $this->input->post('kecamatan'),
					'kelurahan'		=> $this->input->post('kelurahan'),
					'alamat'		=> $this->input->post('alamat'),
					'no_hp'			=> $this->input->post('tlpn'),
					'email'			=> $this->input->post('email'),
					'tgl_update'	=> date('Y-m-d H:i:s')
				);
			// var_dump($datt);exit();
			$this->app_model->updatedata('tbl_bio_mhs','npm',$this->input->post('npm'),$datt);
		}

		echo "<script>alert('Berhasil!');document.location.href='".base_url()."datas/mahasiswa/view_mhs';</script>";exit();
	}

}

/* End of file Load_edit_bio.php */
/* Location: ./application/modules/datas/controllers/Load_edit_bio.php */