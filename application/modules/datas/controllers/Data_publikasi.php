<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Data_publikasi extends MY_Controller {

	function __construct()
	{
		parent::__construct();
		// if ($this->session->userdata('sess_login') == TRUE) {
		// 	$cekakses = $this->role_model->cekakses(49)->result();
		// 	if ($cekakses != TRUE) {
		// 		echo "<script>alert('Anda Tidak Berhak Mengakses !!');history.go(-1);</script>";
		// 	}
		// } else {
		// 	redirect('auth','refresh');
		// }
	}

	function index()
	{
		$logged = $this->session->userdata('sess_login');
		$pecah = explode(',', $logged['id_user_group']);
		$jmlh = count($pecah);
		for ($i=0; $i < $jmlh; $i++) { 
			$grup[] = $pecah[$i];
		}

		if ((in_array(10, $grup)) or (in_array(1, $grup))) {
			$data['fakultas']=$this->app_model->getdata('tbl_fakultas', 'id_fakultas', 'ASC')->result();
			$data['tahunajar']=$this->app_model->getdata('tbl_tahunakademik', 'kode', 'ASC')->result();
			$data['page']= "data/pub_select";
		} elseif ((in_array(9, $grup))) {
			$data['prodi'] = $this->app_model->getdetail('tbl_jurusan_prodi', 'kd_fakultas',$logged['userid'],'kd_fakultas', 'ASC')->result();
			$data['tahunajar']=$this->app_model->getdata('tbl_tahunakademik', 'kode', 'ASC')->result();
			$data['page']= "data/pub_select";
		} elseif ((in_array(8, $grup))) {
			$data['tahunajar']=$this->app_model->getdata('tbl_tahunakademik', 'kode', 'ASC')->result();
			$data['page']= "data/pub_select";
		} else {
			redirect('auth','refresh');
		}
		
		$this->load->view('template/template', $data);
	}

	function get_jurusan($id)
	{
        $jurusan = $this->app_model->getdetail('tbl_jurusan_prodi', 'kd_fakultas', $id, 'id_prodi', 'ASC')->result();
		$out = "<select class='form-control' name='jurusan' id='jurs'><option>--Pilih Jurusan--</option>";
        foreach ($jurusan as $row) {
            $out .= "<option value='".$row->kd_prodi."'>".$row->prodi. "</option>";
        }
        $out .= "</select>";
        echo $out;
	}

	function simpan_sesi()
	{
		$fakultas = $this->input->post('fakultas');
		$jurusan = $this->input->post('prodi');
        $tahunajaran = $this->input->post('tahunajaran');
		$semester = $this->input->post('semester');


        $this->session->set_userdata('tahunajaran', $tahunajaran);
		$this->session->set_userdata('prodi', $jurusan);
		$this->session->set_userdata('fakultas', $fakultas);
		$this->session->set_userdata('semester', $semester);

		// die(''.$this->session->userdata('jurusan').''
		// 	.$this->session->userdata('tahunajaran').''
		// 	.$this->session->userdata('fakultas').''
		// 	.$this->session->userdata('semester').''
		// 	);
      
		redirect(base_url('akademik/publikasi/view'));
	}

	function loadpub()
	{
		$logged = $this->session->userdata('sess_login');
		$pecah = explode(',', $logged['id_user_group']);
		$jmlh = count($pecah);
		for ($i=0; $i < $jmlh; $i++) { 
			$grup[] = $pecah[$i];
		}
		$prodi = $logged['userid'];
		//$prodi = '26201';
		//$ta = '20151';
		$ta = $this->session->userdata('tahunajaran');
		$semester = $this->session->userdata('semester');
		$data['getData'] = $this->app_model->getdatapublish($prodi,$ta,$semester)->result();
		$data['page']= "data/pub_view";
		$this->load->view('template/template', $data);
	}

}

/* End of file Data_publikasi.php */
/* Location: .//tmp/fz3temp-1/Data_publikasi.php */