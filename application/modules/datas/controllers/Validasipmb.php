<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Validasipmb extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		//error_reporting(0);
		if ($this->session->userdata('sess_login') == TRUE) {
			$cekakses = $this->role_model->cekakses(91)->result();
			if ($cekakses != TRUE) {
				echo "<script>alert('Anda Tidak Berhak Mengakses !!');history.go(-1);</script>";
			}
		} else {
			redirect('auth','refresh');
		}
		date_default_timezone_set('Asia/Jakarta'); 
	}

	public function index()
	{
		$data['list'] = $this->db->query('SELECT * from tbl_calon_mhs a join tbl_jurusan_prodi b on a.kd_prodi = b.kd_prodi')->result();
		$data['page'] = 'validasi_data_cm';
		$this->load->view('template/template', $data);
	}

	function validasi()
	{
		$stt = $this->input->post('stat');
		$reg = $this->input->post('reg');

		$cm = $this->app_model->getdetail('tbl_calon_mhs','ID_registrasi',$reg,'ID_registrasi','asc')->row();
		$fi = $cm->ID_registrasi;
		$nm = $cm->nama;
		$asl = $cm->asal_skl;
		$telp = $cm->tlp;
		$mail = $cm->email;
		$alm = $cm->alamat;
		$pro = $cm->prodi;

		$datas['ID_registrasi'] = $fi;
		$datas['nama'] = $nm;
		$datas['asal_skl'] = $asl;
		$datas['tlp'] = $telp;
		$datas['email'] = $mail;
		$datas['alamat'] = $alm;
		if ($stt == '') {
			$datas['status'] = NULL;
		} else {
			$datas['status'] = $stt;
		}
		$datas['kd_prodi'] = $pro;
		//var_dump($datas);exit();
		$que = $this->db->query('SELECT * from tbl_calon_mhs where ID_registrasi = '.$fi.'')->row();
		if ($que == TRUE) {
			$try = $que->id;
			$again = $this->app_model->getdetail('tbl_calon_mhs','id',$try,'id','asc')->row();
			$yah = $again->ID_registrasi;
			$this->app_model->updatedata('tbl_calon_mhs','ID_registrasi',$yah,$datas);
		} else {
			$this->app_model->insertdata('tbl_calon_mhs',$datas);
		}

		echo "<script>alert('Sukses');
		document.location.href='".base_url()."data/validasipmb/';</script>";
	}

	function savevalidasi()
	{
		$jumlahmhs = count($this->input->post('reg'));

		for ($i=0; $i < $jumlahmhs; $i++) { 
			$pecah = explode('zzz', $this->input->post('reg['.$i.']'));
			$param = $pecah[0];
			$data['status'] = $param;
			$mhs = $pecah[1];
			$this->sync_login($mhs);
			$this->app_model->updatedata('tbl_calon_mhs','ID_registrasi',$mhs,$data);
		}
		echo "<script>alert('Sukses');
		document.location.href='".base_url()."data/validasipmb/';</script>";
	}

	function print_xls()
	{
		$data['cmb'] = $this->db->query("SELECT * from tbl_calon_mhs a join tbl_jurusan_prodi b on a.`kd_prodi`=b.`kd_prodi`")->result();
		//var_dump($data);exit();
		$this->load->view('print_dt_cm', $data);
	}

	function print_kareg($kode){
		//die($kode);

		$characters = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
	   	$randomString = '';
	   	for ($i = 0; $i < 6; $i++) {
	    	$randomString .= $characters[rand(0, strlen($characters) - 1)];
	   	}

	   	$this->db->where('ID_registrasi', $kode);
	   	$this->db->from('tbl_calon_mhs b');
	   	$this->db->join('tbl_jurusan_prodi a', 'a.kd_prodi = b.kd_prodi');
	   	$data['camaba'] = $this->db->get()->row();

	   	$data['user'] = $kode;
	   	$data['pass'] = $kode;
	   	$data['qr']  = '7ff3f5432a62ebc00a27dde6c27fbfcc';

	   	$this->load->view('welcome/print/kartu_regist', $data);
	}

	function sync_login($user)
	{
		$data1['username']= substr(str_replace(' ', '', $user), 0,12);

		$data1['userid']= str_replace(' ', '', $user);

		$data1['status']= 1;

		//$pass = explode('-', $value->TGLHRMSMHS);

		//$password = $pass[2].$pass[1].$pass[0];

		$data1['password_plain']= str_replace(' ', '', $data1['username']);

		$data1['user_type'] = 4;

		$data1['password']= sha1(md5($data1['username']).key);

		$data1['id_user_group']= 4;

		$this->app_model->insertdata('tbl_user_login',$data1);
	}

}

/* End of file Validasipmb.php */
/* Location: ./application/modules/data/controllers/Validasipmb.php */