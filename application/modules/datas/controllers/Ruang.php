<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ruang extends MY_Controller {

	function __construct()
	{
		parent::__construct();
		//$this->load->library('Cfpdf');
		if ($this->session->userdata('sess_login') == TRUE) {
			$cekakses = $this->role_model->cekakses(138)->result();
			if ($cekakses != TRUE) {
				echo "<script>alert('Anda Tidak Berhak Mengakses !!');history.go(-1);</script>";
			}
		} else {
			redirect('auth','refresh');
		}
	}

	public function index()
	{
		$data['tahunajar']=$this->app_model->getdata('tbl_tahunakademik', 'kode', 'ASC')->result();
		$data['page'] = 'datas/ruang_select';
		$this->load->view('template/template', $data);		
	}

	function save_session_ruang()
	{
		$ta = $this->input->post('tahunajaran', TRUE);
		$this->session->set_userdata('ta',$ta);
		redirect('datas/ruang/view','refresh');
	}

	function view()
	{
		//var_dump($this->session->userdata('ta'));
		$data['getdata'] = $this->db->query("SELECT DISTINCT a.`id_gedung`,a.`gedung`,b.`lantai`,c.`id_ruangan`,c.`kode_ruangan`, c.ruangan, c.kuota FROM tbl_gedung a
											JOIN tbl_lantai b ON a.`id_gedung` = b.`id_gedung`
											JOIN tbl_ruangan c ON b.`id_lantai` = c.`id_lantai`
											ORDER BY a.`id_gedung`,`lantai` ASC")->result();
		$data['page'] = 'datas/ruang_view';
		$this->load->view('template/template', $data);
	}

	function detail($id)
	{
		$data['ruang'] = $this->app_model->getdetail('tbl_ruangan','id_ruangan',$id,'id_ruangan','asc')->row();
		$data['getdata'] = $this->db->query("SELECT * FROM tbl_jadwal_matkul
											WHERE kd_ruangan = ".$id." AND kd_tahunajaran = '".$this->session->userdata('ta')."'
											ORDER BY hari,waktu_mulai ASC")->result();	
		$data['page'] = 'datas/ruang_detail';
		$this->load->view('template/template', $data);	
	}

	function classParticipant($id)
	{
		error_reporting(0);
		$data['kelas'] = $this->db->query("SELECT kelas from tbl_jadwal_matkul where id_jadwal = '".$id."'")->row()->kelas;
		$data['participant'] = $this->db->query("SELECT a.npm_mahasiswa, b.NMMHSMSMHS from tbl_jadwal_matkul c 
												join tbl_krs a ON a.kd_jadwal = c.kd_jadwal
												join tbl_mahasiswa b on b.NIMHSMSMHS = a.npm_mahasiswa
												where c.id_jadwal = '".$id."'")->result();
		$data['page'] = "v_list_classparticipant";
		$this->load->view('template/template', $data);
	}

}

/* End of file Ruang.php */
/* Location: ./application/modules/datas/controllers/Ruang.php */