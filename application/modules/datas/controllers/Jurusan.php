<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Jurusan extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		if ($this->session->userdata('sess_login') == TRUE) {
			$akses = $this->role_model->cekakses(29)->result();
			if ($akses != TRUE) {
				redirect('home','refresh');
			}
		} else {
			redirect('auth','refresh');
		}
	}

	function index()
	{
		$data['fakultas']=$this->app_model->getdata('tbl_fakultas','id_fakultas','asc')->result();
		$data['jurusan']=$this->app_model->get_jurusan($this->session->userdata('sess_login')['userid']);
		$data['page']="jurusan_view";
		$this->load->view('template/template', $data);
	}

	function del_jurusan($id)
	{
		$this->app_model->deletedata('tbl_jurusan_prodi','id_prodi',$id);
		echo "<script>alert('Berhasil');
		document.location.href='".base_url()."data/jurusan';</script>";
	}

	function save_jurusan()
	{
		$data = array(
		'kd_prodi'		=> $this->form_validation->set_rules('kd_jur','Kode Jurusan', 'trim|required'),
		'prodi'			=> $this->form_validation->set_rules('nm_jur','Nama Jurusan', 'trim|required'),
		'kd_fakultas'	=> $this->input->post('fakuls')
		);

		if ($this->form_validation->run() == FALSE) {
			echo "<script>alert('Gagal Simpan Data');history.go(-1);</script>";
		} else {
			$data = array(
			'kd_prodi'		=> $this->input->post('kd_jur',TRUE),
			'prodi'			=> $this->input->post('nm_jur',TRUE),
			'kd_fakultas'	=> $this->input->post('fakuls')
			);
			$this->app_model->insertdata('tbl_jurusan_prodi',$data);
			echo "<script>alert('Sukses');
			document.location.href='".base_url()."data/jurusan';</script>";
		}
		
			//$this->app_model->insertdata('tbl_jurusan_prodi',$data);
			//echo "<script>alert('Sukses');
			//document.location.href='".base_url()."data/jurusan';</script>";
		
	}

	function update_jurusan()
	{
		$data = array(
		'kd_prodi'		=> $this->form_validation->set_rules('kode_jur', 'Kode Fakultas', 'trim|required'),
		'prodi'			=> $this->form_validation->set_rules('jurusan', 'Kode Fakultas', 'trim|required'),
		'kd_fakultas'	=> $this->form_validation->set_rules('fakuls', 'Kode Fakultas', 'trim|required')
		);

		if ($this->form_validation->run() == FALSE) {
			echo "<script>alert('Gagal Simpan Data');history.go(-1);</script>";
		} else {
			$data = array(
			'kd_prodi'		=> $this->input->post('kode_jur'),
			'prodi'			=> $this->input->post('jurusan'),
			'kd_fakultas'	=> $this->input->post('fakuls')
			);
			$this->app_model->updatedata('tbl_jurusan_prodi','id_prodi',$this->input->post('id_jurusan'),$data);
			echo "<script>alert('Sukses');
			document.location.href='".base_url()."data/jurusan';</script>";
		}
	}

	// function load_data($idj)
	// {
	// 	$data['jurusan']=$this->app_model->get_jurusan();
	// 	$this->load->view('data/jurusan_view', $data);
	// }

	// function get_list_jurs($id)
	// {
	// 	$this->db->select('*');
	// 	$this->db->from('tbl_jurusan_prodi');
	// 	$this->db->where('id_prodi',$id);
	// }

	function view_edit_jurs($idj)
	{
		$data['fakultas']=$this->app_model->getdata('tbl_fakultas','id_fakultas','asc')->result();
		$data['query'] = $this->db->query("select * from tbl_jurusan_prodi where id_prodi = '$idj'")->row();
		$this->load->view('data/jurusan_edit',$data);
	}
}

/* End of file  */
/* Location: ./application/controllers/ */