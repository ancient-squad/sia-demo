<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Parse_login extends MY_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->model('login_model');
	}

	public function index()
	{
		
	}

	function getLogin()
	{
		$u = $this->input->post('username');
		$p = $this->input->post('password');

		// cek ketersediaan akun berdasarkan username
		$chk = $this->app_model->getdetail('tbl_user_login','userid',$u,'userid','asc');

		// jika akun tersedia
		if ($chk->num_rows() > 0) {
			$hs = sha1(md5($p).key);

			// buat kecocokan antara password hasil post dengan hasil load by username
			if (sha1($chk->row()->password.android) == sha1($hs.android)) {
				$log = $this->login_model->cekuser($u,$p);

				// jika cocok
				if ($log->num_rows() > 0) {
					$json['alrt'] = 'Selamat datang darling!';

				// jika, ah sudahlah
				} else {
					$json['alrt'] = 'Kamu Bohongin Aku Ya !?';
				}

				// parse kr json
				echo json_encode($json);

			// jika hasil post dengan hasil load by username tidak cocok
			} else {
				$json['alrt'] = 'Go Home, you\'re drunk man!';
				echo json_encode($json);
			}

		// jika akun tidak tersedia
		} else {
			$json['alrt'] = 'Anda gagal, 3 juta melayang!';
			echo json_encode($json);
		}
	}

}

/* End of file Parse.php */
/* Location: ./application/modules/android/controllers/Parse.php */