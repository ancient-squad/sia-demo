<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Apiwha extends MY_Controller {

	// public function index() {
	// 	$this->load->library('whatsapp');
	// 	$wa = new Whatsapp;
	// 	$wa->hookMsg();
	// }

	// function hook() {
	// 	// This is your webhook. You must configure it in the number settings section.

	// 	$data = json_decode($_POST["data"]);

	// 	// When you receive a message an INBOX event is created
	// 	if ($data->event == "INBOX") {
	// 		// Default answer
	// 		$my_answer = "This is an autoreply from APIWHA!. You (" . $data->from . ") wrote: " . $data->text;

	// 		// You can evaluate the received message and prepare your new answer.
	// 		if (!(strpos(strtoupper($data->text), "PRICING") === false)) {
	// 			$my_answer = "Sing up in our platform and you will get our pricing list!";

	// 		} else if (!(strpos(strtoupper($data->text), "INFORMATION") === false)) {
	// 			$my_answer = "Of course! For more information you can access to our website http://www.apiwha.com!";

	// 		}

	// 		$response = new StdClass();
	// 		$response->apiwha_autoreply = $my_answer; // Attribute "apiwha_autoreply" is very important!

	// 		echo json_encode($response); // Don't forget to reply in JSON format

	// 		/* You don't need any APIKEY to answer to your customer from a webhook
	// 	}
	// }*/
	//

	public function index($value = '') {
		echo "string";
	}

	public function transcript($phone) {
		$isPhoneExist = $this->isPhoneExist($phone);
		if (!$isPhoneExist->result()) {
			$response = ['status' => 'bad request', 'message' => 'phone number not found'];
			$this->output
				->set_status_header(400)
				->set_content_type('application/json', 'utf-8')
				->set_output(json_encode($response))
				->_display();
			exit();

		}

		$npm = $isPhoneExist->row()->npm;

		$data['head'] = $this->db->query('SELECT * from tbl_mahasiswa a join tbl_jurusan_prodi b
										on a.`KDPSTMSMHS`=b.`kd_prodi` join tbl_fakultas c
										on b.`kd_fakultas`=c.`kd_fakultas`
										where a.`NIMHSMSMHS` = "' . $npm . '"')->row();
		$data['q'] = $this->db->query('SELECT nl.`KDKMKTRLNM`,
										IF(nl.`THSMSTRLNM`<"20151",(SELECT DISTINCT nama_matakuliah FROM tbl_matakuliah_copy WHERE kd_matakuliah = nl.`KDKMKTRLNM` AND kd_prodi = nl.`KDPSTTRLNM` AND tahunakademik = nl.`THSMSTRLNM` ORDER BY id_matakuliah DESC LIMIT 1) ,
											(SELECT DISTINCT nama_matakuliah FROM tbl_matakuliah WHERE kd_matakuliah = nl.`KDKMKTRLNM` AND kd_prodi = nl.`KDPSTTRLNM` ORDER BY id_matakuliah DESC LIMIT 1) ) AS nama_matakuliah,
										IF(nl.`THSMSTRLNM`<"20151",(SELECT DISTINCT sks_matakuliah FROM tbl_matakuliah_copy WHERE kd_matakuliah = nl.`KDKMKTRLNM` AND kd_prodi = nl.`KDPSTTRLNM` AND tahunakademik = nl.`THSMSTRLNM` ORDER BY id_matakuliah DESC LIMIT 1) ,
											(SELECT DISTINCT sks_matakuliah FROM tbl_matakuliah WHERE kd_matakuliah = nl.`KDKMKTRLNM` AND kd_prodi = nl.`KDPSTTRLNM` ORDER BY id_matakuliah DESC LIMIT 1) ) AS sks_matakuliah,
										MIN(nl.`NLAKHTRLNM`) AS NLAKHTRLNM,MAX(nl.`THSMSTRLNM`) AS THSMSTRLNM,MAX(nl.`BOBOTTRLNM`) AS BOBOTTRLNM FROM tbl_transaksi_nilai nl
										WHERE nl.`NIMHSTRLNM` = "' . $npm . '"
										GROUP BY nl.`KDKMKTRLNM` ORDER BY THSMSTRLNM ASC
										')->result();
		$data['q_konversi'] = $this->db->query('SELECT nl.`KDKMKTRLNM`,
										IF(nl.`THSMSTRLNM`<"20151",(SELECT DISTINCT nama_matakuliah FROM tbl_matakuliah_copy WHERE kd_matakuliah = nl.`KDKMKTRLNM` AND kd_prodi = nl.`KDPSTTRLNM`  ORDER BY id_matakuliah DESC LIMIT 1) ,
											(SELECT DISTINCT nama_matakuliah FROM tbl_matakuliah WHERE kd_matakuliah = nl.`KDKMKTRLNM` AND kd_prodi = nl.`KDPSTTRLNM` ORDER BY id_matakuliah DESC LIMIT 1) ) AS nama_matakuliah,
										IF(nl.`THSMSTRLNM`<"20151",(SELECT DISTINCT sks_matakuliah FROM tbl_matakuliah_copy WHERE kd_matakuliah = nl.`KDKMKTRLNM` AND kd_prodi = nl.`KDPSTTRLNM` ORDER BY id_matakuliah DESC LIMIT 1) ,
											(SELECT DISTINCT sks_matakuliah FROM tbl_matakuliah WHERE kd_matakuliah = nl.`KDKMKTRLNM` AND kd_prodi = nl.`KDPSTTRLNM` ORDER BY id_matakuliah DESC LIMIT 1) ) AS sks_matakuliah,
										MIN(nl.`NLAKHTRLNM`) AS NLAKHTRLNM,MAX(nl.`THSMSTRLNM`) AS THSMSTRLNM,MAX(nl.`BOBOTTRLNM`) AS BOBOTTRLNM FROM tbl_transaksi_nilai_konversi nl
										WHERE nl.`NIMHSTRLNM` = "' . $npm . '"
										GROUP BY nl.`KDKMKTRLNM` ORDER BY THSMSTRLNM ASC
										')->result();

		$this->load->library('Cfpdf');

		$data['npm'] = $npm;
		$data['output'] = 'F';
		$filename = "transcript-" . $npm . ".pdf";

		if (!file_exists($_SERVER['DOCUMENT_ROOT'] . '/downloaded/transkrip/' . $filename)) {
			$data['pathPdf'] = $_SERVER['DOCUMENT_ROOT'] . '/downloaded/transkrip/' . $filename;
			$this->load->view('akademik/transkrip_pdf3', $data);
		}

		$this->output
			->set_status_header(200)
			->set_header('Content-Disposition: attachment; filename="transcript.pdf"')
			->set_content_type('application/pdf') // You could also use ".jpeg" which will have the full stop removed before looking in config/mimes.php
			->set_output(file_get_contents($_SERVER['DOCUMENT_ROOT'] . '/downloaded/transkrip/' . $filename))
			->_display();
		exit();
	}

	/**
	 * Check Phone Number Exist
	 *  @param   $phone phone number
	 *  @return  Object
	 */
	public function isPhoneExist($phone = null) {
		$this->db->where('no_hp', $phone);
		$isPhoneExist = $this->db->get('tbl_bio_mhs');
		return $isPhoneExist;
	}

}

/* End of file Apiwha.php */
/* Location: .//tmp/fz3temp-1/Apiwha.php */