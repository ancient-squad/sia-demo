<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class API_Controller extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Create response for all request
	 * @param int 	$status_code
	 * @param array $response
	 * @return void
	 */
	private function _create_response($status_code, $response)
	{
		$this->output
			->set_status_header($status_code)
			->set_content_type('application/json', 'utf-8')
			->set_output(json_encode($response))
			->_display();
		exit();
	}

	/**
	 * Create response for success data 
	 * 
	 * @return void
	 */
	protected function success_response($payload)
	{
		$response = ['status' => 1, 'message' => 'success', 'data' => $payload];
		$this->_create_response(200, $response);
	}

	/**
	 * Create response for fail teaching data 
	 * 
	 * @return void
	 */
	protected function empty_response()
	{
		$response = ['status' => 23, 'message' => 'data is empty'];
		$this->_create_response(204,$response);
	}

	/**
	 * To check is object has valid property
	 * 
	 * @param object 	$class
	 * @param array 	$property
	 * @return bool
	 */
	protected function is_property_exist($class, $property)
	{
		$i = 0;
		foreach ($class as $key => $value) {
			if (!property_exists($class, $property[$i])) {
				$data[] = 'invalid parameter for '.strtoupper($key);
			}
			$i++;
		}

		if (isset($data)) {
			$response = ['status' => 4, 'message' => 'invalid parameter', 'desc' => $data];
			$this->_create_response(400, $response);
		}
		return;
	}

	/**
	 * Check the number of parameter should be sent, if invalid then reject
	 * 
	 * @param object 	$prop
	 * @param int 		$total_param
	 * @return void
	 */
	protected function is_number_of_property_valid($prop, $total_param)
	{
		if (count((array)$prop) != $total_param) {
			$response = [
				'status' => 4, 
				'message' => 'invalid parameter', 
				'desc' => 'number of parameter is invalid'
			];
			$this->_create_response(400, $response);
		}
		return;
	}

	/**
	 * Validation for body request
	 * 
	 * @param object 	$property
	 * @param int 		$allowedNumberOfProperty
	 * @param array  	$allowedProperties
	 * @return bool
	 */
	public function request_validation($property, $allowedNumberOfProperty, array $allowedProperties)
	{
		$this
			->is_number_of_property_valid($property, $allowedNumberOfProperty)
			->is_property_exist($property, $allowedProperties);
		return TRUE;
	}

}

/* End of file API_Controller.php */
/* Location: ./application/modules/api/controllers/API_Controller.php */