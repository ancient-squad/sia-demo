<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Api_lppmp extends MY_Controller
{

    public function __construct()
    {
        parent::__construct();
        if ($this->input->get_request_header('x-lppmp-key') !== APPKEYS) {
            $response = ['status' => 90, 'message' => 'App key not valid'];
            $this->_create_response(400, $response);
        }
        $this->load->model('api_lppmp_model', 'lppmpm');
    }

    /**
     * Handle login from manual input
     *
     * @return void
     */
    public function attemp_login()
    {
        $payload        = file_get_contents('php://input');
        $decode_payload = json_decode($payload);
        $is_user_exist  = $this->login_model->cekuser($decode_payload->username, $decode_payload->password);

        if ($is_user_exist->num_rows() > 0) {
            $is_authorized_user = $this->_is_authorized($is_user_exist->row()->id_user_group);

            if ($is_authorized_user) {
                $data_user = $this->lppmpm->get_data_user($decode_payload->username, $is_user_exist->row()->user_type)->row();
                $session   = $this->_set_payload($data_user, $is_user_exist->row()->user_type);
                $response  = ['status' => 1, 'message' => 'success', 'data' => $session];
                $this->_create_response(200, $response);
            }
        }

        $response = ['status' => 3, 'message' => 'invalid login'];
        $this->_create_response(400, $response);
    }

    /**
     * Handle login using cookie
     *
     * @return void
     */
    public function cookie_login()
    {
        $payload        = file_get_contents('php://input');
        $decode_payload = json_decode($payload);
        $data_user      = $this->lppmpm->get_data_user($decode_payload->username, $decode_payload->usertype)->row();

        if ($data_user) {
            $session  = $this->_set_payload($data_user, $decode_payload->usertype);
            $response = ['status' => 1, 'message' => 'success', 'data' => $session];
            $this->_create_response(200, $response);
        } else {
            $response = ['status' => 3, 'message' => 'invalid login'];
            $this->_create_response(401, $response);
        }
    }

    /**
     * Set payload for each user type. Lecturer has more payload than other authorized user.
     *
     * @return void
     */
    private function _set_payload($data, $usertype)
    {
        switch ($usertype) {
            case 1:
                return $this->_lecture_payload($data);
                break;

            default:
                return $this->_administrator_payload($data);
                break;
        }
    }

    /**
     * Set payload for lecturer
     * @param object $data
     * @return void
     */
    private function _lecture_payload($data)
    {
        $session = [
            'username' => $data->nid,
            'usertype' => $data->user_type,
            'userid'   => $data->nid,
            'nidn'     => $data->nidn,
            'group'    => $data->id_user_group,
            'name'     => $data->nama,
            'nik'      => $data->nik,
            'tplhr'    => $data->tpt_lahir,
            'tglhr'    => $data->tgl_lahir,
            'jenkel'   => $data->jns_kel,
            'hp'       => $data->tlp,
            'jabfung'  => $data->jabfung,
            'email'    => $data->email,
            'fakultas' => get_kdfak_byprodi($data->jabatan_id)
        ];
        return $session;
    }

    /**
     * Set payload for non lecturer
     * @param object $data
     * @return void
     */
    private function _administrator_payload($data)
    {
        $session = [
            'username' => $data->username,
            'usertype' => $data->user_type,
            'userid'   => $data->userid,
            'group'    => $data->id_user_group,
            'name'     => $data->divisi,
            'nik'      => '',
            'tplhr'    => '',
            'tglhr'    => '',
            'jenkel'   => '',
            'hp'       => '',
            'email'    => ''
        ];
        return $session;
    }

    /**
     * Check whether user is has authorization
     * @param int $group
     * @return void
     */
    private function _is_authorized($group)
    {
        /**
         * Group list:
         * 5= mahasiswa 6=dosen, 7=dosen PA,26=lsp
         */
        $groups          = get_group($group);
        $authorized_user = [6, 7, 9, 11, 25];

        for ($i = 0; $i < count($groups); $i++) {
            if (in_array($groups[$i], $authorized_user)) {
                return TRUE;
            }
        }

        $response = ['status' => 8, 'message' => 'invalid permission'];
        $this->_create_response(403, $response);
    }

    /**
     * Create response for all request
     * @param int 	$status_code
     * @param array $response
     * @return void
     */
    private function _create_response($status_code, $response)
    {
        $this->output
            ->set_status_header($status_code)
            ->set_content_type('application/json', 'utf-8')
            ->set_output(json_encode($response))
            ->_display();
        exit();
    }


    public function nmdosen()
    {
        $payload        = file_get_contents('php://input');
        $data           = nama_dsn($payload);
        $response       = ['status' => 1, 'message' => 'success', 'data' => $data];
        $this->_create_response(200, $response);
    }
    public function dosen()
    {
        $payload        = file_get_contents('php://input');
        $decode_payload = $payload;
        $this->db->distinct();
        $this->db->select("tbl_karyawan.nid,tbl_karyawan.nama,tbl_karyawan.nidn,tbl_biodata_dosen.tlp,tbl_biodata_dosen.email,tbl_biodata_dosen.jabfung,tbl_karyawan.jabatan_id");
        $this->db->from('tbl_karyawan');
        $this->db->join('tbl_biodata_dosen', 'tbl_biodata_dosen.nid = tbl_karyawan.nid');
        $this->db->join('tbl_user_login', 'tbl_user_login.userid = tbl_karyawan.nid');
        $this->db->where('tbl_user_login.status', 1);
        $this->db->where('tbl_karyawan.nidn IS NOT NULL', NULL, false);
        $this->db->where('tbl_biodata_dosen.jabfung IS NOT NULL', NULL, false);
        $this->db->group_start();
        $this->db->like('tbl_karyawan.nama', $decode_payload, 'both');
        $this->db->or_like('tbl_karyawan.nid', $decode_payload, 'both');
        $this->db->group_end();
        $sql  = $this->db->get();
        $data = array();

        foreach ($sql->result() as $row) {
            $data[] = array(
                'label'        => $row->nid . ' - ' . $row->nama,
                'value'        => $row->nid . ' - ' . $row->nama,
                'save'         => $row->nid,
                'name'         => $row->nama,
                'nidn'         => $row->nidn,
                'telp'         => $row->tlp,
                'email'        => $row->email,
                'fakultas'     => get_kdfak_byprodi($row->jabatan_id),
                'fakultasname' => get_fak(get_kdfak_byprodi($row->jabatan_id)),
                'jabfung'      => $row->jabfung,
                'jabfungname'  => jabfung($row->jabfung)

            );
        }

        $dt = json_encode($data);
        $response = ['status' => 1, 'message' => 'success', 'data' => $dt];
        $this->_create_response(200, $response);
    }
    public function prodi()
    {
        $payload        = file_get_contents('php://input');
        $decode_payload = json_decode($payload);
        $data           = get_jur($decode_payload);
        $response       = ['status' => 1, 'message' => 'success', 'data' => $data];
        $this->_create_response(200, $response);
    }

    public function allProdi()
    {
        $years    = $this->db->get('tbl_jurusan_prodi')->result();
        $response = ['status' => 1, 'message' => 'success', 'data' => $years];
        $this->_create_response(200, $response);
    }
    public function fakultas()
    {
        $payload        = file_get_contents('php://input');
        $decode_payload = json_decode($payload);
        $data           = get_fak($decode_payload);
        $response       = ['status' => 1, 'message' => 'success', 'data' => $data];
        $this->_create_response(200, $response);
    }
    public function allFakultas()
    {
        $years    = $this->db->get('tbl_fakultas')->result();
        $response = ['status' => 1, 'message' => 'success', 'data' => $years];
        $this->_create_response(200, $response);
    }
    public function active_year()
    {
        $payload = ['nama_tahun' => get_thnajar(getactyear()), 'kode_tahun' => getactyear()];
        $response = ['status' => 1, 'message' => 'success', 'data' => $payload];
        $this->_create_response(200, $response);
    }
}

/* End of file Api_bkd.php */
/* Location: ./application/modules/api/controllers/Api_bkd.php */