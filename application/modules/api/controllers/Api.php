<?php if (!defined('BASEPATH')) {
	exit('No direct script access allowed');
}

class Api extends MY_Controller
{

	var $client_key;

	public function __construct()
	{
		parent::__construct();

		if ($this->input->get_request_header('app-key') !== APPKEYS) {
			$response = ['status' => 90, 'message' => 'app key not valid'];
			$this->output
				->set_status_header(400)
				->set_content_type('application/json', 'utf-8')
				->set_output(json_encode($response))
				->_display();
			exit();
		}

		$this->client_key = $this->input->get_request_header('client-key');
	}

	public function index()
	{
		echo "ini dia";
	}

	function cek_nomer($nomer)
	{
		$query = "SELECT COUNT(*) AS jml , mhs.`NMMHSMSMHS` AS nama FROM tbl_bio_mhs bio
									JOIN tbl_mahasiswa mhs ON bio.`npm` = mhs.`NIMHSMSMHS` WHERE no_hp LIKE '%" . $nomer . "' LIMIT 1";

		$rs = $this->db->query($query)->row();

		if ($rs->jml >= 1) {
			$data['response'] = 10;
			$data['nama'] = $rs->nama;
		} elseif ($rs->jml = 0) {
			$data['response'] = 11;
		}

		echo json_encode($data);
	}

	/**
	 * Get academic conselor
	 * @return object
	 */
	function get_pa()
	{
		$isPhoneExist = $this->isPhoneExist($this->client_key);

		// if phone number doesn't exist
		if (!$isPhoneExist->result()) {
			$response = ['status' => 60, 'error' => 'phone number not found'];
			$this->_responseJSON(400, $response);
		} else {

			// get data nama_dosenm nid, and tlp_dosen
			$this->db->select('a.nama as nama_dosen, a.nid, c.tlp as tlp_dosen');
			$this->db->from('tbl_karyawan a');
			$this->db->join('tbl_pa b', 'a.nid = b.kd_dosen');
			$this->db->join('tbl_biodata_dosen c', 'c.nid = a.nid', 'left');
			$this->db->where('b.npm_mahasiswa', $isPhoneExist->row()->npm);
			$getResponse = $this->db->get();

			// if data below not fount
			if (!$getResponse->result()) {
				$response = ['status' => 23, 'message' => 'data not found'];
				$this->_responseJSON(200, $response);
			} else {
				$dataResponse = [
					'nama_dosen' => $getResponse->row()->nama_dosen,
					'nid' => $getResponse->row()->nid,
					'tlp_dosen' => $getResponse->row()->tlp_dosen,
				];

				$response = ['status' => 1, 'message' => 'success', 'data' => $dataResponse];
				$this->_responseJSON(200, $response);
			}
		}
	}

	function get_khs2($semester)
	{
		$this->load->library('Cfpdf');

		$this->db->where('no_hp', $this->client_key);
		$isPhoneExist = $this->db->get('tbl_bio_mhs')->row();

		// if phone number doesn't exist
		if (!$isPhoneExist) {
			$response = ['status' => 60, 'error' => 'phone number not found'];
			$this->_responseJSON(400, $response);
		} else {
			$nim = $isPhoneExist->npm;
			$prodi = get_mhs_jur($nim);
			$tas = $this->db->query("SELECT distinct kd_krs from tbl_krs where npm_mahasiswa = '" . $nim . "'
										and semester_krs = " . $semester . " ")->row();
			// if kd_krs not found
			if ($tas->kd_krs) {
				$data['ta'] = $tas;
				$data['krs'] = $this->db->query('SELECT distinct b.nama_matakuliah, b.sks_matakuliah, b.kd_matakuliah
													FROM tbl_krs a
													JOIN tbl_matakuliah b ON a.`kd_matakuliah` = b.`kd_matakuliah`
													where a.`npm_mahasiswa` = ' . $nim . '
													and b.kd_prodi = ' . $prodi . '
													and a.semester_krs = ' . $semester . '')->result();
				$data['semester'] = $semester;
				$data['npm'] = $nim;

				// if data below already exists
				if (!file_exists(base_url() . 'downloaded/khs/KHS_' . $nim . '_' . substr($tas->kd_krs, 9, 5) . '.pdf')) {
					$this->load->view('api/print_khs', $data);
				}

				$this->output
					->set_status_header(200)
					->set_header('Content-Disposition: attachment; filename="KHS_' . $nim . '_' . substr($tas->kd_krs, 9, 5) . '.pdf"')
					->set_content_type('application/pdf')
					->set_output(file_get_contents(base_url() . 'downloaded/khs/KHS_' . $nim . '_' . substr($tas->kd_krs, 9, 5) . '.pdf'))
					->_display();
				exit();
			} else {
				$response = ['status' => 23, 'message' => 'KHS not found'];
				$this->_responseJSON(400, $response);
			}
		}
	}

	function get_khs()
	{
		$semester = yearBefore();

		$this->load->library('Cfpdf');

		$this->db->where('no_hp', $this->client_key);
		$isPhoneExist = $this->db->get('tbl_bio_mhs')->row();

		// if phone number doesn't exist
		if (!$isPhoneExist) {
			$response = ['status' => 60, 'error' => 'phone number not found'];
			$this->_responseJSON(400, $response);
		} else {
			$nim = $isPhoneExist->npm;
			$prodi = get_mhs_jur($nim);
			$tas = $this->db->query("SELECT distinct kd_krs from tbl_krs where kd_krs like '" . $nim . $semester . "%'")->row();
			// if kd_krs not found
			if ($tas->kd_krs) {
				$data['ta'] = $tas;
				$data['krs'] = $this->db->query("SELECT distinct b.nama_matakuliah, b.sks_matakuliah, b.kd_matakuliah
													FROM tbl_krs a
													JOIN tbl_matakuliah b ON a.`kd_matakuliah` = b.`kd_matakuliah`
													where a.`npm_mahasiswa` = '" . $nim . "'
													and b.kd_prodi = '" . $prodi . "'
													and a.kd_krs like '" . $nim . $semester . "%'")->result();
				$data['semester'] = $semester;
				$data['npm'] = $nim;

				// if data below already exists
				if (!file_exists(base_url() . 'downloaded/khs/KHS_' . $nim . '_' . substr($tas->kd_krs, 9, 5) . '.pdf')) {
					$this->load->view('api/print_khs', $data);
				}

				$dataResponse = ['status' => 1, 'data' => base_url() . 'downloaded/khs/KHS_' . $nim . '_' . substr($tas->kd_krs, 9, 5) . '.pdf'];

				$this->_responseJSON(200, $dataResponse);
			} else {
				$response = ['status' => 23, 'message' => 'KHS not found'];
				$this->_responseJSON(400, $response);
			}
		}
	}

	function payment()
	{
		$this->db->where('no_hp', $this->client_key);
		$isPhoneExist = $this->db->get('tbl_bio_mhs');

		// if phone number doesn't exist
		if (!$isPhoneExist->result()) {
			$response = ['status' => 60, 'error' => 'phone number not found'];
			$this->output
				->set_status_header(400)
				->set_content_type('application/json', 'utf-8')
				->set_output(json_encode($response))
				->_display();
			exit();
		} else {

			$activeAcademicYear = getactyear();

			// get payment in active year
			$this->db->select('tahunajaran, transaksi_terakhir as tgl_validasi, status, briva_mhs as briva, semester');
			$this->db->from('tbl_sinkronisasi_renkeu');
			$this->db->where('npm_mahasiswa', $isPhoneExist->row()->npm);
			$this->db->where('tahunajaran', $activeAcademicYear);
			$getResponse = $this->db->get();

			// if data below not fount
			if (!$getResponse->result()) {
				$response = ['status' => 23, 'message' => 'data not found'];
				$this->_responseJSON(200, $response);
			} else {

				switch ($getResponse->row()->status) {
					case '1':
						$status = 'Daftar ulang';
						break;

					case '2':
						$status = 'Telah melakukan pembayaran hingga UTS';
						break;

					case '3':
						$status = 'Telah melakukan pembayaran UTS susulan';
						break;

					case '4':
						$status = 'Telah melakukan pembayaran hingga UAS';
						break;

					case '5':
						$status = 'Cuti';
						break;
				}

				$dataResponse = [
					'tahunajaran' => get_thnajar($getResponse->row()->tahunajaran),
					'tgl_validasi' => $getResponse->row()->tgl_validasi,
					'status' => $status,
					'semester' => $getResponse->row()->semester,
				];
				$response = ['status' => 1, 'message' => 'success', 'data' => $dataResponse];
				$this->_responseJSON(200, $response);
			}
		}
	}

	function schedule($day)
	{
		$this->db->where('no_hp', $this->client_key);
		$isPhoneExist = $this->db->get('tbl_bio_mhs');

		if (!$isPhoneExist->result()) {
			$response = ['status' => 60, 'error' => 'phone number not found'];
			$this->_responseJSON(400, $response);
		} else {
			$activeAcademicYear = getactyear();

			$this->db->select('a.hari, a.waktu_mulai, a.waktu_selesai, a.kd_dosen as dosen, a.kd_ruangan as ruang, a.kd_matakuliah as matakuliah, a.kd_jadwal');
			$this->db->from('tbl_jadwal_matkul a');
			$this->db->join('tbl_krs b', 'a.kd_jadwal = b.kd_jadwal');
			$this->db->where('a.hari', $day);
			$this->db->like('b.kd_krs', $isPhoneExist->row()->npm . $activeAcademicYear, 'after');
			$getResponse = $this->db->get();

			if (!$getResponse->result()) {
				$response = ['status' => 23, 'message' => 'data is empty'];
				$this->_responseJSON(200, $response);
			} else {

				foreach ($getResponse->result() as $key) {
					$dataResponse[] = [
						'hari' => notohari($key->hari),
						'jam_masuk' => $key->waktu_mulai,
						'jam_keluar' => $key->waktu_selesai,
						'dosen' => nama_dsn($key->dosen),
						'ruang' => get_room($key->ruang),
						'matakuliah' => get_nama_mk($key->matakuliah, substr($key->kd_jadwal, 0, 5)),
					];
				}

				$response = ['status' => 1, 'message' => 'success', 'data' => $dataResponse];
				$this->_responseJSON(200, $response);
			}
		}
	}

	function briva()
	{
		$this->db->where('no_hp', $this->client_key);
		$isPhoneExist = $this->db->get('tbl_bio_mhs');

		if (!$isPhoneExist->result()) {
			$response = ['status' => 60, 'message' => 'phone number not found'];
			$this->_responseJSON(400, $response);
		} else {
			$this->db->where('NIMHSMSMHS', $isPhoneExist->row()->npm);
			$getResponse = $this->db->get('tbl_mahasiswa');

			if (!$getResponse->result()) {
				$response = ['status' => 23, 'message' => 'data is empty'];
				$this->output
					->set_status_header(200)
					->set_content_type('application/json', 'utf-8')
					->set_output(json_encode($response))
					->_display();
				exit();
			} else {
				$dataResponse = [
					'nomor_briva' => '70306' . substr($getResponse->row()->NIMHSMSMHS, 2),
					'nama' => $getResponse->row()->NMMHSMSMHS,
				];

				$response = ['status' => 1, 'message' => 'success', 'data' => $dataResponse];
				$this->_responseJSON(200, $response);
			}
		}
	}

	function examination($examptype)
	{
		$this->db->where('no_hp', $this->client_key);
		$isPhoneExist = $this->db->get('tbl_bio_mhs');

		if ($examptype === 1) {
			$tipeuji = 'UTS';
		} else {
			$tipeuji = 'UAS';
		}

		if (!$isPhoneExist->result()) {
			$response = ['status' => 60, 'message' => 'phone number not found'];
			$this->_responseJSON(400, $response);
		} else {
			$activeAcademicYear = getactyear();

			$this->db->like('kd_krs', $isPhoneExist->row()->npm . $activeAcademicYear, 'after');
			$listKrs = $this->db->get('tbl_krs')->result();

			foreach ($listKrs as $key) {
				$this->db->where('kd_jadwal', $key->kd_jadwal);
				$this->db->where('tipe_uji', $examptype);
				$dataExam = $this->db->get('tbl_jadwaluji')->result();

				foreach ($dataExam as $val) {
					$listExam[] = [
						'nama_matakuliah' => get_nama_mk(get_dtl_jadwal($val->kd_jadwal)['kd_matakuliah'], substr($val->kd_jadwal, 0, 5)),
						'tanggal_ujian' => $val->start_date,
						'jenis_ujian' => $tipeuji,
						'ruang' => get_room($val->kd_ruang),
					];
				}
			}

			if (empty($listExam)) {
				$response = ['status' => 23, 'message' => 'data is empty',];
			} else {
				$response = ['status' => 1, 'message' => 'success', 'data' => $listExam];
			}

			$this->_responseJSON(200, $response);
		}
	}

	/**
	 * Transcript
	 * @param   $phone [for validate phone number]
	 * @return  File Pdf
	 */
	public function transcript()
	{
		$isPhoneExist = $this->isPhoneExist($this->client_key);
		if (!$isPhoneExist->result()) {
			$response = ['status' => 60, 'message' => 'phone number not found'];
			$this->output
				->set_status_header(400)
				->set_content_type('application/json', 'utf-8')
				->set_output(json_encode($response))
				->_display();
			exit();
		}

		$npm = $isPhoneExist->row()->npm;

		$data['head'] = $this->db->query('SELECT * from tbl_mahasiswa a join tbl_jurusan_prodi b
										on a.`KDPSTMSMHS`=b.`kd_prodi` join tbl_fakultas c
										on b.`kd_fakultas`=c.`kd_fakultas`
										where a.`NIMHSMSMHS` = "' . $npm . '"')->row();
		$data['q'] = $this->db->query('SELECT nl.`KDKMKTRLNM`,
										IF(nl.`THSMSTRLNM`<"20151",(SELECT DISTINCT nama_matakuliah FROM tbl_matakuliah_copy WHERE kd_matakuliah = nl.`KDKMKTRLNM` AND kd_prodi = nl.`KDPSTTRLNM` AND tahunakademik = nl.`THSMSTRLNM` ORDER BY id_matakuliah DESC LIMIT 1) ,
											(SELECT DISTINCT nama_matakuliah FROM tbl_matakuliah WHERE kd_matakuliah = nl.`KDKMKTRLNM` AND kd_prodi = nl.`KDPSTTRLNM` ORDER BY id_matakuliah DESC LIMIT 1) ) AS nama_matakuliah,
										IF(nl.`THSMSTRLNM`<"20151",(SELECT DISTINCT sks_matakuliah FROM tbl_matakuliah_copy WHERE kd_matakuliah = nl.`KDKMKTRLNM` AND kd_prodi = nl.`KDPSTTRLNM` AND tahunakademik = nl.`THSMSTRLNM` ORDER BY id_matakuliah DESC LIMIT 1) ,
											(SELECT DISTINCT sks_matakuliah FROM tbl_matakuliah WHERE kd_matakuliah = nl.`KDKMKTRLNM` AND kd_prodi = nl.`KDPSTTRLNM` ORDER BY id_matakuliah DESC LIMIT 1) ) AS sks_matakuliah,
										MIN(nl.`NLAKHTRLNM`) AS NLAKHTRLNM,MAX(nl.`THSMSTRLNM`) AS THSMSTRLNM,MAX(nl.`BOBOTTRLNM`) AS BOBOTTRLNM FROM tbl_transaksi_nilai nl
										WHERE nl.`NIMHSTRLNM` = "' . $npm . '"
										GROUP BY nl.`KDKMKTRLNM` ORDER BY THSMSTRLNM ASC
										')->result();
		$data['q_konversi'] = $this->db->query('SELECT nl.`KDKMKTRLNM`,
										IF(nl.`THSMSTRLNM`<"20151",(SELECT DISTINCT nama_matakuliah FROM tbl_matakuliah_copy WHERE kd_matakuliah = nl.`KDKMKTRLNM` AND kd_prodi = nl.`KDPSTTRLNM`  ORDER BY id_matakuliah DESC LIMIT 1) ,
											(SELECT DISTINCT nama_matakuliah FROM tbl_matakuliah WHERE kd_matakuliah = nl.`KDKMKTRLNM` AND kd_prodi = nl.`KDPSTTRLNM` ORDER BY id_matakuliah DESC LIMIT 1) ) AS nama_matakuliah,
										IF(nl.`THSMSTRLNM`<"20151",(SELECT DISTINCT sks_matakuliah FROM tbl_matakuliah_copy WHERE kd_matakuliah = nl.`KDKMKTRLNM` AND kd_prodi = nl.`KDPSTTRLNM` ORDER BY id_matakuliah DESC LIMIT 1) ,
											(SELECT DISTINCT sks_matakuliah FROM tbl_matakuliah WHERE kd_matakuliah = nl.`KDKMKTRLNM` AND kd_prodi = nl.`KDPSTTRLNM` ORDER BY id_matakuliah DESC LIMIT 1) ) AS sks_matakuliah,
										MIN(nl.`NLAKHTRLNM`) AS NLAKHTRLNM,MAX(nl.`THSMSTRLNM`) AS THSMSTRLNM,MAX(nl.`BOBOTTRLNM`) AS BOBOTTRLNM FROM tbl_transaksi_nilai_konversi nl
										WHERE nl.`NIMHSTRLNM` = "' . $npm . '"
										GROUP BY nl.`KDKMKTRLNM` ORDER BY THSMSTRLNM ASC
										')->result();

		$this->load->library('Cfpdf');

		$data['npm'] = $npm;
		$data['output'] = 'F';
		$filename = "transcript-" . $npm . ".pdf";

		if (!file_exists($_SERVER['DOCUMENT_ROOT'] . '/downloaded/transkrip/' . $filename)) {
			$data['pathPdf'] = $_SERVER['DOCUMENT_ROOT'] . '/downloaded/transkrip/' . $filename;
			$this->load->view('akademik/transkrip_pdf3', $data);
		}

		$response = ['status' => 1, 'data' => base_url() . 'downloaded/transkrip/' . $filename];
		$this->_responseJSON(200, $response);
		// $this->output
		// 	->set_status_header(200)
		// 	// ->set_header('Content-Disposition: attachment; filename="transcript-'.$npm.'.pdf"')
		// 	->set_content_type('application/json', 'utf-8') // You could also use ".jpeg" which will have the full stop removed before looking in config/mimes.php
		// 	// ->set_output(file_get_contents($_SERVER['DOCUMENT_ROOT'] . '/downloaded/transkrip/' . $filename))
		// 	->set_output(json_encode($response))
		// 	->_display();
		// exit();
	}

	public function get_krs()
	{
		$isPhoneExist = $this->isPhoneExist($this->client_key);

		if (!$isPhoneExist->result()) {
			# code...
		} else {
			# code...
		}
	}

	public function thesisSchedule()
	{
		$isPhoneExist = $this->isPhoneExist($this->client_key);

		// if phone number doesn't exist
		if (!$isPhoneExist->result()) {
			$response = ['status' => 60, 'error' => 'phone number not found'];
			$this->_responseJSON(400, $response);
		} else {

			// get data nama_dosenm nid, and tlp_dosen
			$this->db->select('*');
			$this->db->from('tbl_announcement');
			$this->db->where('category', 'jdl-skr');
			$getResponse = $this->db->get();

			// if data below not fount
			if (!$getResponse->result()) {
				$response = ['status' => 23, 'message' => 'data not found'];
				$this->_responseJSON(200, $response);
			} else {
				$dataResponse = [
					'category' => 'jadwal sidang skripsi',
					'tahunakademik' => $getResponse->row()->tahunakademik,
					'note' => $getResponse->row()->note,
				];

				$response = ['status' => 1, 'message' => 'success', 'data' => $dataResponse];
				$this->_responseJSON(200, $response);
			}
		}
	}

	/**
	 * Check Phone Number Exist
	 *  @param   $phone phone number
	 *  @return  Object
	 */
	public function isPhoneExist($phone = null)
	{
		$this->db->where('no_hp', $phone);
		$isPhoneExist = $this->db->get('tbl_bio_mhs');
		return $isPhoneExist;
	}

	/**
	 * Get API of jadwal kuliah
	 * 
	 * @return void
	 */
	function get_jadwal()
	{
		$activeYear = getactyear();
		$jadwal = $this->db->query("SELECT * FROM tbl_jadwal_matkul WHERE kd_tahunajaran = $activeYear");
		// if data below not fount
		if (!$jadwal->result()) {
			$response = ['status' => 23, 'message' => 'data not found'];
			$this->_responseJSON(200, $response);
		} else {
			foreach ($jadwal->result() as $data) {
				$dataResponse[] = [
					'id_jadwal'       => $data->id_jadwal,
					'kode_jadwal'     => $data->kd_jadwal,
					'id_matakuliah'   => $data->id_matakuliah,
					'kode_matakuliah' => $data->kd_matakuliah,
					'hari'            => $data->hari,
					'waktu_mulai'     => $data->waktu_mulai,
					'waktu_selesai'   => $data->waktu_selesai,
					'kategori_kelas'  => $data->waktu_kelas,
					'id_ruangan'      => $data->kd_ruangan,
					'nama_kelas'      => $data->kelas,
					'kelas_gabungan'  => $data->gabung,
					'induk_kelas'     => $data->referensi,
					'tahun_ajaran'    => $data->kd_tahunajaran,
					'kode_dosen'      => $data->kd_dosen
				];
			}

			$response = ['status' => 1, 'message' => 'success', 'data' => $dataResponse];
			$this->_responseJSON(200, $response);
		}
	}

	/**
	 * Prodi
	 * Menampilkan seluruh prodi jika kode prodi kosong
	 * @param int $kd_prodi <kode prodi yang akan di cari (optional)>
	 * @return JSON
	 */
	public function prodi($kd_prodi = "")
	{
		$sql = "SELECT * FROM tbl_jurusan_prodi";

		if (!empty($kd_prodi)) {
			$sql .= " WHERE kd_prodi = " . $kd_prodi;
		}

		$list = $this->db->query($sql);

		if ($list->num_rows() > 0) {
			$response = ['status' => 1, 'message' => 'success', 'data' => $list->result_array()];
			$this->_responseJSON(200, $response);
		} elseif ($list->num_rows() == 0) {
			$response = ['status' => 23, 'message' => 'data not found', 'data' => []];
			$this->_responseJSON(200, $response);
		}
	}

	/**
	 * Mahasiswa
	 * Untuk menampilkan info mahasiswa berdasarkan table tbl_mahasiswa
	 * @param  string $npm [nomor pokok mahasiswa (optional)]
	 * @param int $limit limitation
	 * @param int $offset offset data
	 * @return JSON
	 */
	public function students($limit = 0, $offset = 0)
	{
		$sql = "SELECT * FROM tbl_mahasiswa";

		if ($limit == 0) {
			$sql .= " LIMIT 10";
		} elseif ($limit != 0) {
			$sql .= " LIMIT " . $limit;
		}

		$list = $this->db->query($sql);

		if ($list->num_rows() > 0) {
			$response = ['status' => 1, 'message' => 'success', 'data' => $list->result_array()];
			$this->_responseJSON(200, $response);
		} elseif ($list->num_rows() == 0) {
			$response = ['status' => 23, 'message' => 'data not found', 'data' => []];
			$this->_responseJSON(200, $response);
		}
	}

	/**
	 * Mahasiswa
	 * Untuk menampilkan info mahasiswa berdasarakan npm di table tbl_mahasiswa dengan 
	 * @param  string $npm [nomor pokok mahasiswa (optional)]
	 * @return JSON
	 */
	public function student($npm = '')
	{
		$sql = "SELECT * FROM tbl_mahasiswa";

		if (!empty($npm)) {
			$sql .= " WHERE NIMHSMSMHS = '" . $npm . "'";
		}

		$list = $this->db->query($sql);

		if ($list->num_rows() > 0) {
			$response = ['status' => 1, 'message' => 'success', 'data' => $list->result_array()];
			$this->_responseJSON(200, $response);
		} elseif ($list->num_rows() == 0) {
			$response = ['status' => 23, 'message' => 'data not found', 'data' => []];
			$this->_responseJSON(200, $response);
		}
	}

	public function prodi_student($npm = "", $outputAPI = true)
	{
		if (empty($npm)) {
			$response = ['status' => 23, 'message' => 'parameter not valid, npm is required', 'data' => []];
			$this->_responseJSON(204, $response);
		} else {

			$sql = "SELECT KDPSTMSMHS FROM tbl_mahasiswa WHERE NIMHSMSMHS = '" . $npm . "'";
			$list = $this->db->query($sql);
			if ($list->num_rows() > 0) {
				// outputAPI for api request
				if ($outputAPI) {
					$response = ['status' => 1, 'message' => 'success', 'data' => $list->row()->KDPSTMSMHS];
					$this->_responseJSON(200, $response);
				} else {
					return $list->row()->KDPSTMSMHS;
				}
			} elseif ($list->num_rows() == 0) {
				if ($outputAPI) {
					$response = ['status' => 23, 'message' => 'data not found', 'data' => []];
					$this->_responseJSON(200, $response);
				} else {
					return "";
				}
			}
		}
	}

	public function kelas_mhs()
	{
		$req = json_decode(file_get_contents('php://input'));

		$npm = $req->npm;
		$kode = @$req->kode ? $req->kode : [];

		$actyear = getactyear();
		$prodi = $this->prodi_student($npm, FALSE);

		$kodezz = $npm . $actyear;

		$this->db->distinct();
		$this->db->select('c.nama,c.nid,d.nama_matakuliah,b.kd_matakuliah,a.kd_jadwal');
		$this->db->from('tbl_krs a');
		$this->db->join('tbl_jadwal_matkul b', 'a.kd_jadwal = b.kd_jadwal');
		$this->db->join('tbl_karyawan c', 'b.kd_dosen = c.nid');
		$this->db->join('tbl_matakuliah d', 'b.kd_matakuliah = d.kd_matakuliah');
		$this->db->like('a.kd_krs', $kodezz, 'after');
		$this->db->where('d.kd_prodi', $prodi);
		$this->db->where('a.kd_jadwal is NOT NULL', NULL, FALSE);
		$namamk = "d.`nama_matakuliah` NOT LIKE 'skripsi%' AND d.`nama_matakuliah` NOT LIKE '%tesis%' AND d.`nama_matakuliah` NOT LIKE '%kerja praktek%' AND d.`nama_matakuliah` NOT LIKE '%magang%'";
		$this->db->where($namamk);
		if (!empty($kode)) {
			$this->db->where_not_in('a.kd_jadwal', $kode);
		}
		$list = $this->db->get();

		if ($list->num_rows() > 0) {
			$response = ['status' => 1, 'message' => 'success', 'data' => $list->result()];
			$this->_responseJSON(200, $response);
		} elseif ($list->num_rows() == 0) {
			$response = ['status' => 23, 'message' => 'data not found', 'data' => []];
			$this->_responseJSON(200, $response);
		}
	}


	/**
	 * List Tahun Akademik
	 * menampilkan tahun akademik aktif
	 * @return JSON
	 */
	function tahunakademik($status = "")
	{

		$sql = "SELECT * FROM tbl_tahunakademik";

		if (!empty($status)) {
			$sql .= " WHERE status = " . $status;
		}


		$list = $this->db->query($sql);

		$header = 500;
		$response = ['status' => 23, 'message' => ""];

		if ($list->num_rows() > 0) {
			$header = 200;

			if (!empty($statu)) {
				$response = [
					'status' => 1,
					'message' => 'success',
					'data' => $list->row()->kode,
				];
			} else {
				$response = [
					'status' => 1,
					'message' => 'success',
					'data' => $list->result_array(),
				];
			}
		} elseif ($list->num_rows() == 0) {
			$header = 200;
			$response = [
				'status' => 23,
				'message' => 'data not found',
				'data' => [],
			];
		}

		$this->_responseJSON($header, $response);
	}

	/**
	 * User
	 * Get information user login from tbl_user_login
	 * @param string $username username yang akan di cari
	 * @return JSON
	 */
	public function user()
	{
		$this->load->model('login_model', 'login');

		$req = json_decode(file_get_contents('php://input'));


		$username = $req->username;
		$password = $req->password;

		$user 	= $this->login->cekuser($username, $password);

		if ($user->num_rows() > 0) {
			$response = ['status' => 1, 'message' => 'Success', 'data' => $user->result()];
			$this->_responseJSON(200, $response);
		}

		$response = ['status' => 23, 'message' => 'data not found', 'data' => []];
		$this->_responseJSON(200, $response);
	}

	/**
	 * Detail Dosen
	 * @param int $nid nomor induk dosen
	 * @return  mixed 
	 */
	public function dosen($nid = '')
	{
		$list = $this->db->get('tbl_karyawan', ['nid' => $nid]);

		if ($list->num_rows() > 0) {
			$response = ['status' => 1, 'message' => 'Success', 'data' => $list->result()];
			$this->_responseJSON(200, $response);
		}

		$response = ['status' => 23, 'message' => 'data not found', 'data' => []];
		$this->_responseJSON(200, $response);
	}

	/**
	 * getdosenajartaprodi
	 * @param string kd_jadwal
	 * @param int $tahun 
	 * @param int $nid 
	 * @return mixed 
	 */

	public function dosenAjarTaProdi()
	{
		$req = json_decode(file_get_contents("php://input"));

		$prodi = $req->prodi;
		$tahun = $req->tahun;
		$nid = $req->nid;

		$sql  = "SELECT a.`kd_matakuliah`,b.`nama_matakuliah`,a.`kd_jadwal`,a.`kelas` FROM tbl_jadwal_matkul a
				JOIN tbl_matakuliah b ON a.`kd_matakuliah` = b.`kd_matakuliah`
				WHERE a.`kd_tahunajaran` = '" . $tahun . "' AND a.`kd_jadwal` LIKE '" . $prodi . "%' AND a.`kd_dosen` = '" . $nid . "' AND b.`kd_prodi` = '" . $prodi . "'";
		$list = $this->db->query($sql);

		if ($list->num_rows() > 0) {
			$data = [];

			foreach ($list->result() as $key => $value) {
				$mhs = $this->db->query("SELECT COUNT(DISTINCT npm_mahasiswa) as akhir FROM tbl_krs WHERE kd_jadwal = '" . $value->kd_jadwal . "'")->row()->akhir;
				$data[$key] = $value;
				$data[$key]->mhs = $mhs;
			}

			$response = ['status' => 1, 'message' => 'Success', 'data' => $data];
			$this->_responseJSON(200, $response);
		}

		$response = ['status' => 23, 'message' => 'data not found', 'data' => []];
		$this->_responseJSON(200, $response);
	}

	/**
	 * Dosen Ajar TA
	 * @param int $prodi kode prodi
	 * @param int $tahun 
	 * @return mixed 
	 */
	public function dosenAjarTa()
	{
		$req = json_decode(file_get_contents("php://input"));

		$prodi = $req->prodi;
		$tahun = $req->tahun;

		$sql = "SELECT DISTINCT c.`nama`,c.`nid`,(SELECT SUM(mk.`sks_matakuliah`) AS sks FROM tbl_jadwal_matkul jdl
				JOIN tbl_matakuliah mk ON jdl.`id_matakuliah` = mk.`id_matakuliah`
				WHERE jdl.`kd_dosen` = c.`nid` AND
				jdl.kd_tahunajaran = '" . $tahun . "' AND mk.kd_prodi = '" . $prodi . "'
				AND jdl.kd_jadwal LIKE '" . $prodi . "%' AND (mk.`nama_matakuliah` NOT LIKE '%skripsi%' AND mk.`nama_matakuliah` NOT LIKE '%tesis%' AND mk.`nama_matakuliah` NOT LIKE '%kerja praktek%' AND mk.`nama_matakuliah` NOT LIKE '%magang%')) AS sks FROM tbl_jadwal_matkul b
				JOIN tbl_karyawan c ON b.`kd_dosen` = c.`nid`
				JOIN tbl_matakuliah d ON b.`kd_matakuliah` = d.`kd_matakuliah`
				WHERE b.`kd_jadwal` LIKE '" . $prodi . "%' AND b.`kd_tahunajaran` = '" . $tahun . "' AND (d.`nama_matakuliah` NOT LIKE '%skripsi%' AND d.`nama_matakuliah` NOT LIKE '%tesis%' AND d.`nama_matakuliah` NOT LIKE '%kerja praktek%' AND d.`nama_matakuliah` NOT LIKE '%magang%')";
		$list =  $this->db->query($sql);

		if ($list->num_rows() > 0) {
			$response = ['status' => 1, 'message' => 'Success', 'data' => $list->result()];
			$this->_responseJSON(200, $response);
		}

		$response = ['status' => 23, 'message' => 'data not found', 'data' => []];
		$this->_responseJSON(200, $response);
	}

	public function _responseJSON($header, $data)
	{
		$this->output
			->set_status_header($header)
			->set_content_type('application/json', 'utf-8')
			->set_output(json_encode($data))
			->_display();
		exit();
	}
}

/* End of file Api.php */
/* Location: ./application/controllers/Api.php */
