<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Api_pmb extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->library('api_response', NULL, 'api');
		$this->api->header_validation($this->input->get_request_header('x-pmb-key'));
		$this->load->model('api/api_pmb_model', 'pmb');
	}

	public function getProdi()
	{
		$payload = file_get_contents('php://input');
		$content = json_decode($payload);

		$this->api->request_validation($content, 1, ['kodeprodi']);

		$prodi = $this->pmb->getProdi($content->kodeprodi);
		$payload = [
			'kode' => $prodi->kd_prodi,
			'nama' => $prodi->prodi,
			'kode_fakultas' => $prodi->kd_fakultas,
			'fakultas' => $prodi->fakultas
		];
		$this->api->success_response($payload);
	}

}

/* End of file Api_pmb.php */
/* Location: ./application/modules/api/controllers/Api_pmb.php */