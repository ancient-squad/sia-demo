<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Api_units extends MY_Controller
{

	public $table = 'tbl_divisi';

	public function __construct()
	{
		parent::__construct();
		if ($this->input->get_request_header('x-units-key') !== APPKEYS) {
			$response = ['status' => 90, 'message' => 'app key not valid'];
			$this->output
				->set_status_header(400)
				->set_content_type('application/json', 'utf-8')
				->set_output(json_encode($response))
				->_display();
			exit();
		}
	}

	/**
	 * Get All Units
	 * @return [type] 
	 */
	function getUnits()
	{
		if (array_key_exists('kode', $_GET)) {
			$this->getUnitByKode($_GET['kode']);
		}

		$units = $this->db->get($this->table);
		$this->responseJSON($units);
	}


	/**
	 * Get Unit by Kode Divisi
	 * @param  [type] $kd_divisi   
	 * @return [type]              
	 */
	function getUnitByKode($kd_divisi)
	{
		$units = $this->db->get_where($this->table, ['kd_divisi' => $kd_divisi]);
		$this->responseJSON($units);
	}

	/**
	 * Get Unit by ID
	 * @param  [type] $id 
	 * @return [type]              
	 */
	function getUnit($id)
	{
		$units = $this->db->get_where($this->table, ['id_divisi' => $id]);
		$this->responseJSON($units);
	}

	function responseJSON($data)
	{
		$status = 500;

		if ($data->num_rows() > 0) {
			$status = 200;
			$message = "Berhasil";
		}else{
			$status = 200;
			$message = "Tidak Ada Data";
		}

		$response = [
			'status'  => $status,
			'message' => $message,
			'data'    => $data->result(),
		];

		$this->output
			 ->set_status_header($status)
			 ->set_content_type('application/json')
			 ->set_output(json_encode($response))
			 ->_display();

		exit();
	}
}

/* End of file Api_units.php */
/* Location: ./application/modules/api/controllers/Api_units.php */
