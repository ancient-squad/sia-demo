<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Booking extends MY_Controller {

	var $client_key;

	public function __construct() {
		parent::__construct();

		if ($this->input->get_request_header('app-key') !== APPKEYS) {
			$response = ['status' => 90, 'message' => 'app key not valid'];
			$this->output
				->set_status_header(400)
				->set_content_type('application/json', 'utf-8')
				->set_output(json_encode($response))
				->_display();
			exit();
		}
		$this->client_key = $this->input->get_request_header('client-key');
	}

	public function index()
	{
		echo "Bookin' API";
	}

	/**
	* Show list class rooms
	* @return JSON object
	*/
	public function rooms()
	{
		$this->db->select('a.id_gedung, a.gedung, b.id_lantai, b.lantai, c.id_ruangan, c.kode_ruangan, c.ruangan, c.deskripsi, c.kuota');
		$this->db->from('tbl_gedung a');
		$this->db->join('tbl_lantai b', 'a.id_gedung = b.id_gedung');
		$this->db->join('tbl_ruangan c', 'c.id_lantai = b.id_lantai');
		$this->db->not_like('c.ruangan', '99');
		$allRoms = $this->db->get();

		foreach ($allRoms->result() as $room) {
			$dataResponse[] = [
				'id_gedung' => $room->id_gedung,
				'gedung' => $room->gedung,
				'id_lantai' =>$room->id_lantai,
				'lantai' => $room->lantai,
				'id_ruangan' => $room->id_ruangan,
				'kode_ruang' => $room->kode_ruangan,
				'ruang' => $room->ruangan,
				'deskripsi_ruang' => $room->deskripsi,
				'kuota_ruang' => $room->kuota
			];
		}

		$response = ['status' => 1, 'message' => 'success', 'data' => $dataResponse];
		$this->output
			->set_status_header(200)
			->set_content_type('application/json', 'utf-8')
			->set_output(json_encode($response))
			->_display();
		exit();
	}

	/**
	* Show list class rooms
	* @param $id int
	* @return JSON object
	*/
	public function detailRoom($id)
	{
		$this->db->select('a.id_gedung, a.gedung, b.id_lantai, b.lantai, c.id_ruangan, c.kode_ruangan, c.ruangan, c.deskripsi, c.kuota');
		$this->db->from('tbl_gedung a');
		$this->db->join('tbl_lantai b', 'a.id_gedung = b.id_gedung');
		$this->db->join('tbl_ruangan c', 'c.id_lantai = b.id_lantai');
		$this->db->where('c.id_ruangan', $id);
		$this->db->not_like('c.ruangan', '99');
		$allRoms = $this->db->get();

		// check if room exist
		if ($allRoms->result()) {
			foreach ($allRoms->result() as $room) {
				$dataResponse[] = [
					'id_gedung' => $room->id_gedung,
					'gedung' => $room->gedung,
					'id_lantai' =>$room->id_lantai,
					'lantai' => $room->lantai,
					'id_ruangan' => $room->id_ruangan,
					'kode_ruang' => $room->kode_ruangan,
					'ruang' => $room->ruangan,
					'deskripsi_ruang' => $room->deskripsi,
					'kuota_ruang' => $room->kuota
				];
			}
			$response = ['status' => 1, 'message' => 'success', 'data' => $dataResponse];
			$this->output
				->set_status_header(200)
				->set_content_type('application/json', 'utf-8')
				->set_output(json_encode($response))
				->_display();
			exit();
		} else {
			$response = ['status' => 23, 'message' => 'not found', 'data' => 'room not found'];
			$this->output
				->set_status_header(204)
				->set_content_type('application/json', 'utf-8')
				->set_output(json_encode($response))
				->_display();
			exit();
		}

	}

	public function usedRoomList()
	{
		$actyear = getactyear();

		$roomSchedule 	= $this->db->select('a.waktu_mulai, a.waktu_selesai, a.hari, b.kode_ruangan, b.id_ruangan, b.ruangan, b.deskripsi, b.kuota')
									->from('tbl_jadwal_matkul a')
									->join('tbl_ruangan b','a.kd_ruangan = b.id_ruangan')
									->where('a.kd_tahunajaran', $actyear)
									->not_like('b.ruangan', '99')
									->get()->result();

		foreach ($roomSchedule as $rooms) {
			$dataResponse[] = [
				'kode_ruang' => $rooms->kode_ruangan,
				'hari' => notohari($rooms->hari),
				'waktu_mulai' => $rooms->waktu_mulai,
				'waktu_selesai' => $rooms->waktu_selesai,
				'nama_ruang' => $rooms->ruangan,
				'deskripsi' => $rooms->deskripsi,
				'kuota' => $rooms->kuota,
				'id_ruang' => $rooms->id_ruangan
			];
		}

		$response = [
			'status' => 1,
			'message' => 'success',
			'data' => $dataResponse
		];

		$this->output
			->set_status_header(200)
			->set_content_type('application/json', 'utf-8')
			->set_output(json_encode($response))
			->_display();
		exit();
	}

}

/* End of file Booking.php */
/* Location: .//tmp/fz3temp-1/Booking.php */