<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class API_pmb_model extends CI_Model {

	public function getProdi($kodeprodi)
	{
		$this->db->select('pro.kd_prodi, pro.prodi, pro.jenjang, fak.kd_fakultas, fak.fakultas');
		$this->db->from('tbl_jurusan_prodi pro');
		$this->db->join('tbl_fakultas fak', 'pro.kd_fakultas = fak.kd_fakultas');
		$this->db->where('kd_prodi', $kodeprodi);
		$prodi = $this->db->get()->row();
		return $prodi;
	}	

}

/* End of file API_pmb_model.php */
/* Location: ./application/modules/api/models/API_pmb_model.php */