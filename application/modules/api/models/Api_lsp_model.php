<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Api_lsp_model extends CI_Model
{

	public function get_data_user($userid, $usertype)
	{
		switch ($usertype) {
			case 1:
				return $this->_data_dosen($userid);
				break;

			case 2:
				return $this->_data_mahasiswa($userid);
				break;

			default:
				return $this->_data_administrator($userid);
				break;
		}
	}

	private function _data_dosen($userid)
	{
		$user = $this->db->query(
			"SELECT
				usr.id_user_group,
				usr.user_type,
				kry.nid,
				kry.nama,
				kry.nik,
				bio.tpt_lahir,
				bio.tgl_lahir,
				kry.jns_kel,
				bio.tlp,
				bio.email
			FROM
				tbl_user_login usr
				JOIN tbl_karyawan kry
					ON kry.nid = usr.userid
				LEFT JOIN tbl_biodata_dosen bio
					ON bio.nid = usr.userid
			WHERE usr.username =  $userid
				AND usr.status = 1"
		);
		return $user;
	}

	private function _data_mahasiswa($userid)
	{
		$mahasiswa = $this->db->query(
			"SELECT
				usr.id_user_group,
				usr.user_type,
				mhs.NIMHSMSMHS,
				mhs.KDPSTMSMHS,
				mhs.NMMHSMSMHS,
				mhs.NIKMSMHS,
				mhs.TPLHRMSMHS,
				mhs.TGLHRMSMHS,
				mhs.KDJEKMSMHS,
				bio.no_hp,
				bio.email
			FROM
				tbl_user_login usr
				JOIN tbl_mahasiswa mhs
					ON mhs.NIMHSMSMHS = usr.userid
				LEFT JOIN tbl_bio_mhs bio
					ON bio.npm = usr.userid
			WHERE usr.username = $userid
				AND usr.status = 1"
		);
		return $mahasiswa;
	}

	private function _data_administrator($userid)
	{
		$this->db->select('a.userid, a.id_user_group, a.user_type, c.divisi,a.username');
		$this->db->from('tbl_user_login a');
		$this->db->join('tbl_divisi c', 'a.userid = c.kd_divisi');
		$this->db->where('a.username', $userid);
		$this->db->where('a.status', 1);
		return $this->db->get();
	}
}

/* End of file Api_bkd_model.php */
/* Location: ./application/modules/api/models/Api_bkd_model.php */