<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Matakuliah extends MY_Controller {

	private $userid, $usergroup;

	public function __construct(){

		parent::__construct();	
		error_reporting(0);
		if ($this->session->userdata('sess_login') == TRUE) {

			$cekakses = $this->role_model->cekakses(34)->result();
			if (!$cekakses) {
				echo "<script>alert('Anda Tidak Berhak Mengakses!');history.go(-1);</script>";
			}

		} else {
			redirect('auth','refresh');
		}	

		$this->load->library('user_agent');
		$this->userid = $this->session->userdata('sess_login')['userid'];
		$this->usergroup = $this->session->userdata('sess_login')['id_user_group'];
	}

	public function index()
	{
		$usergroup = get_group($this->usergroup);

		if (in_array(1, $usergroup) || in_array(10, $usergroup)) {
			$data['page']='v_dashboard';

		} elseif (in_array(9, $usergroup)) {
			$data['jurusan'] = $this->app_model->getdetail('tbl_jurusan_prodi','kd_fakultas',$this->userid,'kd_prodi','asc')->result();
			$data['page']='v_dashboard_fakultas';

		} elseif (in_array(8, $usergroup) || in_array(19, $usergroup)) {
			$prodi = $this->app_model->getdetail('tbl_jurusan_prodi','kd_prodi',$this->userid,'kd_prodi','asc')->row_array();
			redirect(base_url('perkuliahan/matakuliah/view_matakuliah/'.$prodi["kd_fakultas"].'/'.$prodi["kd_prodi"]));
		}

		$data['fakultas'] = $this->app_model->getdata('tbl_fakultas', 'id_fakultas', 'ASC')->result();
		$this->load->view('template/template', $data);
	}

	function save_session()
	{
		if ($this->usergroup == 9) {
			$fakultas = $this->userid;
			$jurusan  =  $this->input->post('jurusan');

		} else {
			$fakultas = $this->input->post('fakultas');
			$jurusan  =  $this->input->post('jurusan');
		}

		redirect(base_url('perkuliahan/matakuliah/view_matakuliah/'.$fakultas.'/'.$jurusan));
	}

	function view_matakuliah($fakultas, $jurusan)
	{		
		$data['aktif'] 	= $this->db->query("SELECT * FROM tbl_cpanel WHERE kode_cpanel = 'kodemk'")->row();
		$data['crud']  	= $this->db->query("SELECT * FROM tbl_role_access 
											WHERE menu_id = 34 
											AND user_group_id = '$this->usergroup' ")->row();
		$data['page']  	= 'v_matakuliah';
		$data['matakuliah'] = $this->app_model->get_detail_matakuliah($fakultas, $jurusan);
		$this->load->view('template/template', $data);
	}

	function get_jurusan($id)
	{
        $jurusan = $this->app_model->getdetail('tbl_jurusan_prodi', 'kd_fakultas', $id, 'id_prodi', 'ASC')->result();
		$out = "<select class='form-control' name='jurusan' id='jurs'><option>--Pilih Jurusan--</option>";

        foreach ($jurusan as $row) {
            $out .= "<option value='".$row->kd_prodi."'>".$row->prodi. "</option>";
        }

        $out .= "</select>";
        echo $out;
	}

	

	function add_new()
	{
		$user = $this->session->userdata('sess_login');

		$data['kd_matakuliah']   = str_replace(' ', '', $this->input->post('kd_matakuliah'));
		$data['nama_matakuliah'] = $this->input->post('nama_matakuliah');
		$data['en_name']         = $this->input->post('en_name');
		$data['sks_matakuliah']  = $this->input->post('sks_matakuliah');
		$data['sks_tatapmuka']   = $this->input->post('sks_tatap');
		$data['sks_praktikum']   = $this->input->post('sks_prak');
		$data['sks_praklap']     = $this->input->post('sks_praklap');
		$data['sks_simulasi']    = $this->input->post('sks_sim');
		$data['status_wajib']    = $this->input->post('wajib');
		$data['mk_ta']			 = $this->input->post('tugasakhir');

		$i   = 0;
		$len = count($this->input->post('prasyarat_matakuliah'));
		$data['prasyarat_matakuliah'] = '[';

		foreach($this->input->post('prasyarat_matakuliah') as $row){
			if ($i == $len - 1) {
				$data['prasyarat_matakuliah'] .=$row;
			} else {
				$data['prasyarat_matakuliah'] .=$row.',';
			}
			$i++;
		}

		$data['prasyarat_matakuliah'] .= ']';

		$j    = 0;
		$len2 = count($this->input->post('konversi_matakuliah'));
		$data['konversi_matakuliah'] = '[';

		foreach($this->input->post('konversi_matakuliah') as $row2){
			if ($j == $len2 - 1) {
				$data['konversi_matakuliah'] .=$row2;
			}else{
				$data['konversi_matakuliah'] .=$row2.',';
			}
			$i++;
		}

		$data['konversi_matakuliah'] .= ']';

		$data['kd_fakultas'] = $this->input->post('fakultas');

		$data['kd_prodi'] = $this->input->post('jurusan');

		$cek 	= $this->db->query("SELECT kd_matakuliah from tbl_matakuliah 
									where REPLACE(kd_matakuliah,' ','') = '".$data['kd_matakuliah']."'
									AND kd_prodi = '".$user['userid']."'")->num_rows();

		if ($cek > 0) {
			echo "<script>alert('Kode Sudah Dipakai !!');history.go(-1);</script>";
			return;
		}
		
		$this->app_model->insertdata('tbl_matakuliah', $data);
		
		redirect($this->agent->referrer());

	}
	

	function update_data()
	{
		$logged = $this->session->userdata('sess_login');
		$kd_lama = $this->input->post('kd_matakuliah_lama');
		$data['kd_matakuliah']   = $this->input->post('kd_matakuliah');
		$data['nama_matakuliah'] = $this->input->post('nama_matakuliah');
		$data['en_name']         = $this->input->post('en_name');
		$data['sks_matakuliah']  = $this->input->post('sks_matakuliah');
		$data['sks_tatapmuka']   = $this->input->post('sks_etatap', TRUE);
		$data['sks_praktikum']   = $this->input->post('sks_eprak', TRUE);
		$data['sks_praklap']     = $this->input->post('sks_epraklap', TRUE);
		$data['sks_simulasi']    = $this->input->post('sks_esim', TRUE);
		$data['status_wajib']    = $this->input->post('wajib');
		$data['mk_ta']           = $this->input->post('tugasakhir');
		//$data['semester_matakuliah'] = $this->input->post('semester_matakuliah');

		$i = 0;
		$len = count($this->input->post('prasyarat_matakuliah'));
		$data['prasyarat_matakuliah'] = '[';
		foreach($this->input->post('prasyarat_matakuliah') as $row){
			if ($i == $len - 1) {
				$data['prasyarat_matakuliah'] .=$row;
			}else{
				$data['prasyarat_matakuliah'] .=$row.',';
			}
			$i++;
		}

		echo $len;
		$data['prasyarat_matakuliah'] .= ']';
		$j = 0;
		$len2 = count($this->input->post('konversi_matakuliah'));
		$data['konversi_matakuliah'] = '[';
		foreach($this->input->post('konversi_matakuliah') as $row2){
			if ($j == $len2 - 1) {
				$data['konversi_matakuliah'] .=$row2;
			}else{
				$data['konversi_matakuliah'] .=$row2.',';
			}
			$i++;
		}
		$data['konversi_matakuliah'] .= ']';
		$data['kd_fakultas'] = $this->input->post('fakultas');

		$this->app_model->updatedatamk('tbl_matakuliah','kd_matakuliah',$kd_lama,'kd_prodi',$logged['userid'], $data);

		redirect($this->agent->referrer());
	}	

	function delete_data($id)
	{
		$user = $this->session->userdata('sess_login');

		// fetch all matakuliah's datas
		$fetchData = $this->db->where('id_matakuliah', $id)->get('tbl_matakuliah')->row();
		$dataStored = [
			'kd_matakuliah'        => $fetchData->kd_matakuliah,
			'nama_matakuliah'      => $fetchData->nama_matakuliah,
			'sks_matakuliah'       => $fetchData->sks_matakuliah,
			'sks_tatapmuka'        => $fetchData->sks_tatapmuka,
			'sks_praktikum'        => $fetchData->sks_praktikum,
			'sks_praklap'          => $fetchData->sks_praklap,
			'sks_simulasi'         => $fetchData->sks_simulasi,
			'prasyarat_matakuliah' => $fetchData->prasyarat_matakuliah,
			'konversi_matakuliah'  => $fetchData->konversi_matakuliah,
			'kd_prodi'             => $fetchData->kd_prodi,
			'kd_fakultas'          => $fetchData->kd_fakultas,
			'semester_matakuliah'  => $fetchData->semester_matakuliah,
			'status_wajib'         => $fetchData->status_wajib,
			'deleted_at'           => date('Y-m-d H:i:s'),
			'deleted_by'           => $user['userid']
		];
		$this->db->insert('tbl_deleted_matakuliah', $dataStored);

		$this->app_model->deletedata('tbl_matakuliah','id_matakuliah',$id);

		redirect($this->agent->referrer());

	}

}

?>