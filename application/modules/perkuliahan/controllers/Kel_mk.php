<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kel_mk extends MY_Controller {

	function __construct()
	{
		parent::__construct();
		if ($this->session->userdata('sess_login') == TRUE) {
			$cekakses = $this->role_model->cekakses(69)->result();
			if ($cekakses != TRUE) {
				echo "<script>alert('Anda Tidak Berhak Mengakses !!');history.go(-1);</script>";
			}
		} else {
			redirect('auth','refresh');
		}
	}

	function index()
	{
		$data['getData'] = $this->app_model->getdata('tbl_tipe_mk','id_tipe','tipe_mk')->result();
		$data['page'] = 'perkuliahan/tipe_view';
		$this->load->view('template/template',$data);
	}

	function save()
	{	
		$data['kode_tipe'] = $this->input->post('kode',TRUE);
		$data['tipe_mk'] = $this->input->post('tipe',TRUE);	
		$insert = $this->app_model->insertdata('tbl_tipe_mk',$data);
		if ($insert == TRUE) {
			echo "<script>alert('Berhasil');document.location.href='".base_url()."perkuliahan/kel_mk';</script>";
		} else {
			echo "<script>alert('Gagal Simpan Data');history.go(-1);</script>";
		}
	}

	function edit($id)
	{
		$data['getEdit'] = $this->app_model->getdetail('tbl_tipe_mk','id_tipe',$id,'id_tipe','asc')->row();
		//var_dump($data);exit();
		$this->load->view('perkuliahan/edit_tipe',$data);
	}

	function update()
	{
		$id = $this->input->post('id', TRUE);
		$data['kode_tipe'] = $this->input->post('kode',TRUE);
		$data['tipe_mk'] = $this->input->post('tipe',TRUE);	
		$insert = $this->app_model->updatedata('tbl_tipe_mk','id_tipe',$id,$data);
		if ($insert == TRUE) {
			echo "<script>alert('Berhasil');document.location.href='".base_url()."perkuliahan/kel_mk';</script>";
		} else {
			echo "<script>alert('Gagal Simpan Data');history.go(-1);</script>";
		}
	}

	function delete($id)
	{
		$delete = $this->app_model->deletedata('tbl_tipe_mk','id_tipe',$id);
		if ($delete == TRUE) {
			echo "<script>alert('Berhasil');document.location.href='".base_url()."perkuliahan/kel_mk';</script>";
		} else {
			echo "<script>alert('Gagal Hapus Data');history.go(-1);</script>";
		}
	}
	
}

/* End of file Kurikulum.php */
/* Location: ./application/modules/akademik/controllers/Kurikulum.php */