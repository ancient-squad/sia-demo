<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Konversi_matakuliah extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		if (!$this->session->userdata('sess_login')) {
			redirect('auth','refresh');
			exit();
		}
		$this->user = $this->session->userdata('sess_login');
	}

	public function index()
	{
		$data['user'] = $this->user['userid'];
		$data['listmk'] = $this->app_model->getdetail('tbl_konversi_matkul_temp','kd_prodi',$this->user['userid'],'id','asc')->result();
		$data['page'] = "v_konv_mk";
		$this->load->view('template/template', $data);
	}

	function add_konv()
	{
		extract(PopulateForm());

		if ($btnsbm == 'Simpan') {
			$fx_newmk = explode(' - ', $new_mk);
			$fx_lstmk = explode(' - ', $last_mk);

			$data = [
				'kd_baru' => $fx_newmk[0],
				'kd_lama' => $fx_lstmk[0],
				'kd_prodi' => $this->user['userid'],
				'flag' => 1
			];

			var_dump($data);exit();

			$this->app_model->insertdata('tbl_konversi_matkul_temp',$data);
			echo "<script>alert('Berhasil!');history.go(-1);</script>";
		} else {
			$this->update_data($new_mk,$last_mk,$idkonv);
		}
	}

	function load_data($id)
	{
		$data = $this->app_model->getdetail('tbl_konversi_matkul_temp','id',$id,'id','asc')->row();
		echo json_encode($data);
	}

	function update_data($new_mk,$last_mk,$idkonv)
	{

		$fx_newmk = explode(' - ', $new_mk);
		$fx_lstmk = explode(' - ', $last_mk);

		$data = [
			'kd_baru' => $fx_newmk[0],
			'kd_lama' => $fx_lstmk[0],
			'kd_prodi' => $this->user['userid'],
			'flag' => 1
		];

		$this->db->where('id', $idkonv);
		$this->db->update('tbl_konversi_matkul_temp', $data);

		echo "<script>alert('Berhasil!');history.go(-1);</script>";
	}

	function remove_data($id)
	{
		$this->db->where('id', $id);
		$this->db->delete('tbl_konversi_matkul_temp');

		echo "<script>alert('Berhasil!');history.go(-1);</script>";
	}

	function autocomplete()
	{
		$this->db->select('kd_matakuliah, nama_matakuliah');
		$this->db->from('tbl_matakuliah');
		$this->db->like('kd_prodi', $this->user['userid'], 'AFTER');
		$this->db->like('kd_matakuliah', $_GET['term'], 'BOTH');
		$this->db->or_like('nama_matakuliah', $_GET['term'], 'BOTH');
		$data = $this->db->get()->result();

		$mk = [];

		foreach ($data as $key) {
			$mk[] = [
				'value' => $key->kd_matakuliah.' - '.$key->nama_matakuliah
			];
		}

		echo json_encode($mk);
	}

}

/* End of file Konversi_matakuliah.php */
/* Location: .//tmp/fz3temp-1/Konversi_matakuliah.php */