<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kurikulum extends MY_Controller {

	function __construct()
	{
		parent::__construct();
		if ($this->session->userdata('sess_login') == TRUE) {
			$cekakses = $this->role_model->cekakses(43)->result();
			//$aktif = $this->setting_model->getaktivasi('kurikulum')->result();
			if ($cekakses != TRUE) {
				echo "<script>alert('Anda Tidak Berhak Mengakses !!');history.go(-1);</script>";
			}
		} else {
			redirect('auth','refresh');
		}
	}

	function index()
	{
		//$aktif = $this->setting_model->getaktivasi('krs')->result();
		//$data['aktif'] = $this->db->query("SELECT * from tbl_cpanel where kode_cpanel = 'kurikulum'")->row();
		$data['aktif'] = 1;
		$logged = $this->session->userdata('sess_login');
		$grup = get_group($logged['id_user_group']);
		if ((in_array(8, $grup)) or (in_array(19, $grup))) {
			$data['panel']       = $this->db->query('select * from tbl_cpanel')->row();
			$data['tahunajaran'] = $this->app_model->getdata('tbl_tahunakademik', 'kode', 'ASC')->result();
			$data['data_table']  = $this->app_model->get_detail_kurikulum_prodi($logged['userid']);
			$data['fakultas']    = $this->app_model->getdata('tbl_fakultas', 'id_fakultas', 'ASC')->result();
			$data['page']        = 'perkuliahan/kurikulum_view';
			$this->load->view('template/template',$data);

		} elseif ((in_array(9, $grup))) {
			$data['panel']       = $this->db->query('select * from tbl_cpanel')->row();
			$data['tahunajaran'] = $this->app_model->getdata('tbl_tahunakademik', 'kode', 'ASC')->result();
			$data['data_table']  = $this->app_model->get_detail_kurikulum_fakultas($logged['userid']);
			$data['fakultas']    = $this->app_model->getdata('tbl_fakultas', 'id_fakultas', 'ASC')->result();
			$data['kdfak']       = $logged['userid'];
			$data['listprodi']   = $this->app_model->getdetail('tbl_jurusan_prodi', 'kd_fakultas',$logged['userid'],'kd_prodi', 'ASC')->result();
			$data['page']        = 'perkuliahan/kurikulum_view';
			$this->load->view('template/template',$data);

		} else {
			$data['panel']       = $this->db->query('select * from tbl_cpanel')->row();
			$data['tahunajaran'] = $this->app_model->getdata('tbl_tahunakademik', 'kode', 'ASC')->result();
			$data['data_table']  = $this->app_model->get_detail_kurikulum();
			$data['fakultas']    = $this->app_model->getdata('tbl_fakultas', 'id_fakultas', 'ASC')->result();
			$data['page']        = 'perkuliahan/kurikulum_view';
			$this->load->view('template/template',$data);
		}
	}
	
	function cek_dup(){
		$id=$_POST['data'];
		echo $this->app_model->getdetail('tbl_kurikulum_new', 'kd_kurikulum', $id, 'kd_kurikulum', 'ASC')->num_rows();
	}

	function view_kurikulum($id)
	{
		$logged = $this->session->userdata('sess_login');
        $data['crud'] = $this->db->query('SELECT * from tbl_role_access where menu_id = 43 and user_group_id = '.$logged['id_user_group'].' ')->row();
		$data['kurikulum']=$id;
		$data['data_table1']=$this->app_model->get_matakuliah_kurikulum_new($id, 1);
		$data['data_table2']=$this->app_model->get_matakuliah_kurikulum_new($id, 2);
		$data['data_table3']=$this->app_model->get_matakuliah_kurikulum_new($id, 3);
		$data['data_table4']=$this->app_model->get_matakuliah_kurikulum_new($id, 4);
		$data['data_table5']=$this->app_model->get_matakuliah_kurikulum_new($id, 5);
		$data['data_table6']=$this->app_model->get_matakuliah_kurikulum_new($id, 6);
		$data['data_table7']=$this->app_model->get_matakuliah_kurikulum_new($id, 7);
		$data['data_table8']=$this->app_model->get_matakuliah_kurikulum_new($id, 8);
		$data['data_matakuliah']=$this->app_model->get_matakuliah_baru_new($id);
		$data['page'] = 'perkuliahan/kurikulum_detail';
		$this->load->view('template/template',$data);	
	}
	
	function get_jurusan($id){
        $jurusan = $this->app_model->getdetail('tbl_jurusan_prodi', 'kd_fakultas', $id, 'id_prodi', 'ASC')->result();
		$out = "<select class='form-control' name='jurusan' id='jurs'><option>--Pilih Jurusan--</option>";
        foreach ($jurusan as $row) {
            $out .= "<option value='".$row->kd_prodi."'>".$row->prodi. "</option>";
        }
        $out .= "</select>";
        echo $out;
	}

	function add_new(){
		$id_data = $this->input->post('id_kurikulum');
		$data['kd_kurikulum'] = $this->input->post('kd_kurikulum');
		$data['kurikulum'] = $this->input->post('kurikulum');
		$data['kd_prodi'] = $this->input->post('kd_prodi');
		$data['kd_fakultas'] = $this->input->post('kd_fakultas');
		$data['status'] = $this->input->post('status');
		$data['tahun_ajaran_kurikulum'] = $this->input->post('tahun_ajaran_kurikulum');
		
		if($id_data == ''){
			$this->app_model->insertdata('tbl_kurikulum_new', $data);
		}else{
			$this->app_model->updatedata('tbl_kurikulum_new','id_kurikulum',$id_data, $data);
		}
		
		redirect(base_url('perkuliahan/kurikulum'));
	}
	
	function save_matakuliah(){
		$data['kd_matakuliah'] = $this->input->post('matakuliah');
		$data['kd_kurikulum'] = $this->input->post('kd_kurikulum');
		$data['semester_kd_matakuliah'] = $this->input->post('semester_matakuliah');
		$datas['semester_matakuliah'] = $this->input->post('semester_matakuliah',TRUE);
		if ($this->input->post('status_wajib') != 1) {
			$data['status_wajib'] = 0;
		} else {
			$data['status_wajib'] = $this->input->post('status_wajib');
		}
		
		$this->app_model->updatedata('tbl_matakuliah','kd_matakuliah',$data['kd_matakuliah'], $datas);
		$this->app_model->insertdata('tbl_kurikulum_matkul_new', $data);
		redirect(base_url('perkuliahan/kurikulum/view_kurikulum/'.$data['kd_kurikulum']));
	}
	
	function delte_matakuliah_kurikulum($id, $id_kur){
		$this->app_model->deletedata('tbl_kurikulum_matkul_new','id_kurmat',$id);
		redirect(base_url('perkuliahan/kurikulum/view_kurikulum/'.$id_kur));
	}
	
	function delte_kurikulum($id){
		$getkurikulum = $this->db->get_where('tbl_kurikulum_new', ['id_kurikulum' => $id])->row_array();
		$getkurikulum['deleted_at'] = date('Y-m-d H:i:s');
		$this->db->insert('tbl_kurikulum_new_deleted', $getkurikulum);
		$this->app_model->deletedata('tbl_kurikulum_new','id_kurikulum',$id);
		redirect(base_url('perkuliahan/kurikulum'));
	}
	
	function get_data(){
		var_dump($_POST['data']);
	}
}

/* End of file Kurikulum.php */
/* Location: ./application/modules/akademik/controllers/Kurikulum.php */