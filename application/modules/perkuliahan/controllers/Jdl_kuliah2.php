<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Jdl_kuliah2 extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
        if (!$this->session->userdata('sess_login')) {
            redirect('auth/logout');
        }
	}

	public function index()
    {

        $logged = $this->session->userdata('sess_login');
        //var_dump($logged['id_user_group']);exit();
        $pecah = explode(',', $logged['id_user_group']);
        $jmlh = count($pecah);
        for ($i=0; $i < $jmlh; $i++) { 
            $grup[] = $pecah[$i];
        }

        if ((in_array(1, $grup)) || (in_array(10, $grup))) { //BAA & ADMIN

            $data['fakultas'] =$this->app_model->getdata('tbl_fakultas', 'id_fakultas', 'ASC')->result();
            $data['tahunajar']=$this->app_model->getdata('tbl_tahunakademik', 'kode', 'ASC')->result();
            $data['page']='jdl_kuliah_view2';
            $this->load->view('template/template', $data);

        }elseif ((in_array(8, $grup))) { //PRODI

            $q  = $this->db->query('SELECT * FROM tbl_jurusan_prodi
                                    JOIN tbl_fakultas ON tbl_fakultas.`kd_fakultas` = tbl_jurusan_prodi.`kd_fakultas`
                                    WHERE tbl_jurusan_prodi.`kd_prodi`="'.$logged['userid'].'"')->row();

            $this->db->select('kode');
            $this->db->from('tbl_tahunakademik');
            $this->db->where('status', 1);
            $thn_ak = $this->db->get()->row();

            $data['tahunajar'] = $this->db->get('tbl_tahunakademik')->result();

            $tahunajaran = substr($thn_ak->kode, 0,4);

            $this->session->set_userdata('id_fakultas_prasyarat', $q->kd_fakultas);
            $this->session->set_userdata('nama_fakultas_prasyarat', $q->fakultas);
            $this->session->set_userdata('id_jurusan_prasyarat', $q->kd_prodi);
            $this->session->set_userdata('nama_jurusan_prasyarat', $q->prodi);

            //redirect(base_url('perkuliahan/jdl_kuliah/view_matakuliah'));

            $data['page']='v_jdl_kuliah_select_semester';

            $this->load->view('template/template', $data);

        } elseif ((in_array(9, $grup))) { //FAKULTAS
            $data['tahunajar']=$this->app_model->getdata('tbl_tahunakademik', 'kode', 'ASC')->result();
            $data['prodi'] = $this->app_model->getdetail('tbl_jurusan_prodi', 'kd_fakultas',$logged['userid'],'kd_fakultas', 'ASC')->result();
            $data['page']='jdl_kuliah_fak';

            $this->load->view('template/template', $data);
        } elseif ((in_array(5, $grup))) { #MAHASISWA
            $this->session->set_userdata('tahunajaran', '20171');

            $logged = $this->session->userdata('sess_login');

            $getprd = $this->app_model->getdetail('tbl_mahasiswa','NIMHSMSMHS',$logged['userid'],'NIMHSMSMHS','asc')->row()->KDPSTMSMHS;

            $getjur = $this->db->query("select a.*,b.fakultas from tbl_jurusan_prodi a join tbl_fakultas b on a.kd_fakultas = b.kd_fakultas where a.kd_prodi = '".$getprd."'")->row();

            $this->session->set_userdata('id_fakultas_prasyarat', $getjur->kd_fakultas);

            $this->session->set_userdata('nama_fakultas_prasyarat', $getjur->fakultas);

            $this->session->set_userdata('id_jurusan_prasyarat', $getjur->kd_prodi);

            $this->session->set_userdata('nama_jurusan_prasyarat', $getjur->prodi);

            redirect(base_url('perkuliahan/jdl_kuliah/view_matakuliah'));
            
        } else {
            $data['page']='jdl_kuliah_mhs';
            $this->load->view('template/template', $data);
        }
    }

	function get_jurusan($id)
    {
		$jrs = explode('-',$id);
        $jurusan = $this->app_model->getdetail('tbl_jurusan_prodi', 'kd_fakultas', $jrs[0], 'id_prodi', 'ASC')->result();
		$out = "<select class='form-control' name='jurusan' id='jurs'><option>--Pilih Program Studi--</option>";
        foreach ($jurusan as $row) {
            $out .= "<option value='".$row->kd_prodi."-".$row->prodi."'>".$row->prodi. "</option>";
        }
        $out .= "</select>";
        echo $out;
	}

	function load_dosen($idk)
    {
		$data['jadwal'] = $this->db->where('id_jadwal',$idk)->get('tbl_jadwal_matkul')->row();
		$data['dosen']=$this->db->get('tbl_karyawan')->result();
		$this->load->view('penugasan_dosen',$data);
	}

    function load_dosen_autocomplete()
    {
        $this->db->select("*");
        $this->db->from('tbl_karyawan');
        $this->db->like('nama', $_GET['term'], 'both');
        $sql  = $this->db->get();
        $data = array();
        foreach ($sql->result() as $row) {
            $data[] = [
                        'id_kary'       => $row->id_kary,
                        'nid'           => $row->nid,
                        'value'         => $row->nama
                    ];
        }
        echo json_encode($data);
    }



    function save_session_prodi()
    {
        $tahunajaran = $this->input->post('tahunajaran');
        $semester = $this->input->post('smt');
        $this->session->set_userdata('tahunajaran', $tahunajaran);
        $this->session->set_userdata('semester', $semester); 
        $this->session->set_userdata('id_jurusan_prasyarat', $this->session->userdata('sess_login')['userid']);       
        redirect(base_url('perkuliahan/jdl_kuliah2/view_matakuliah'));
    }


	function save_session()
    {
		$fakultas = explode('-',$this->input->post('fakultas'));
		$jurusan = explode('-',$this->input->post('jurusan'));
        $tahunajaran = $this->input->post('tahunajaran');
        $semester = $this->input->post('smt');
        $this->session->set_userdata('semester', $semester);
        $this->session->set_userdata('tahunajaran', $tahunajaran);
		$this->session->set_userdata('id_fakultas_prasyarat', $fakultas[0]);
		$this->session->set_userdata('nama_fakultas_prasyarat', $fakultas[1]);
		$this->session->set_userdata('id_jurusan_prasyarat', $jurusan[0]);
		$this->session->set_userdata('nama_jurusan_prasyarat', $jurusan[1]);
		redirect(base_url('perkuliahan/jdl_kuliah2/view_matakuliah'));
	}

	function view_matakuliah()
    {
        $this->load->model('setting_model');
        $sesi = $this->session->userdata('sess_login');
        $data['crud']   = $this->db->query('SELECT * from tbl_role_access where menu_id = 35 and user_group_id = '.$sesi['id_user_group'].' ')->row();
        $data['aktif']  = $this->setting_model->getaktivasi('jadwal')->result(); 
        $data['sess']   = $sesi['id_user_group'];
        $data['gedung'] = $this->db->get('tbl_gedung')->result();
        $data['jadwal'] = $this->app_model->jdl_matkul_by_semester($this->session->userdata('semester'));
        $data['page']   = 'v_jdl_kuliah_detail_semester';
		$this->load->view('template/template', $data);
	}

    function view_peserta($id_jadwal='')
    {
        $kd_jadwal = get_kd_jdl($id_jadwal);
        $data      = $this->db->query("SELECT count(npm_mahasiswa) AS peserta FROM tbl_krs WHERE kd_jadwal = '$kd_jadwal'")->row()->peserta;
        echo $data;
    }

    function see_child($id)
    {
        $kodeJadwal           = get_kd_jdl($id);
        $data['parentDetail'] = $this->db->where('kd_jadwal', $kodeJadwal)->get('tbl_jadwal_matkul')->row();
        $data['jadwals']      = $this->db->where('referensi', $kodeJadwal)->get('tbl_jadwal_matkul')->result();
        $this->load->view('modal_see_child', $data);
    }


    function see_parent($id)
    {
        $getParent            = $this->db->where('id_jadwal', $id)->get('tbl_jadwal_matkul')->row();
        $data['parentDetail'] = $this->db->where('kd_jadwal', $getParent->referensi)->get('tbl_jadwal_matkul')->row();
        $this->load->view('modal_see_parent', $data);
    }

	function get_detail_matakuliah_by_semester($id)
    {
        $user = $this->session->userdata('sess_login');
        $kode = $user['userid'];
        $data   = $this->db->query("SELECT DISTINCT a.kd_matakuliah, a.nama_matakuliah, a.id_matakuliah 
                                    FROM tbl_matakuliah a
                                    join tbl_kurikulum_matkul_new b on a.kd_matakuliah = b.kd_matakuliah
                                    join tbl_kurikulum_new c on c.kd_kurikulum = b.kd_kurikulum
                                    WHERE a.kd_prodi = '$kode' 
                                    AND b.semester_kd_matakuliah = '$id' 
                                    AND b.`kd_kurikulum` LIKE 'K{$kode}%'
                                    AND (a.kd_matakuliah not like 'MKU-%' AND a.kd_matakuliah not like 'MKDU-%')
                                    AND c.status = '1'
                                    order by nama_matakuliah asc")->result();
		$list = "<option> -- </option>";

		foreach($data as $row){
            $list .= "<option value='".$row->id_matakuliah."'>".$row->kd_matakuliah." - ".$row->nama_matakuliah."</option>";
		}

		die($list);
	}

    function get_kode_mk($id){

        $data = $this->db->query('select * from tbl_matakuliah where id_matakuliah = '.$id.'')->row();

        echo $data->kd_matakuliah;

    }

	function get_lantai($id){

		$data = $this->app_model->get_lantai($id);

		$list = "<option> -- </option>";

		foreach($data as $row){

		$list .= "<option value='".$row->id_lantai."'>".$row->lantai."</option>";

		}

		die($list);

	}

	function get_ruangan($id)
    {
		$data = $this->app_model->get_ruang($id);

		$list = "<option> -- </option>";

		foreach($data as $row){
		  $list .= "<option value='".$row->id_ruangan."'>".$row->kode_ruangan.' - '.$row->ruangan."</option>";
		}

		die($list);
	}

	function getdosen()
    {
        $this->db->select("*");
        $this->db->from('tbl_karyawan');
        $this->db->where('jabatan_id', 8);
        //$this->db->join('obat_satuan', 'obat.satuan = obat_satuan.id');
        $this->db->like('nama', $_GET['term'], 'both');

        $sql  = $this->db->get();

        $data = array();

        foreach ($sql->result() as $row) {
            $data[] = [
                        'nid' 	 => $row->nid,
                        'value'      => $row->nama,
                    ];
        }

        echo json_encode($data);
    }

    function save_data()
    {
    	date_default_timezone_set("Asia/Jakarta"); 
    	extract(PopulateForm());

        $q=$this->db->where('id_matakuliah', $nama_matakuliah)->get('tbl_matakuliah')->row();

        $w = ($q->sks_matakuliah)*50;        

        $ambil_kd = $this->db->query('select * from tbl_matakuliah where id_matakuliah = '.$nama_matakuliah.'')->row(); 
        $kd_mk = $ambil_kd->kd_matakuliah;

            //$id_jur = $this->session->userdata('id_jurusan_prasyarat');
            $session = $this->session->userdata('sess_login');

            $prodi = $session['username'];

            $q_jur = $this->db->where('kd_prodi',$prodi)->get('tbl_jurusan_prodi')->row();

            $gpass = NULL;
            $n = 6; // jumlah karakter yang akan di bentuk.
            $chr = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvqxyz0123456789";
             
            for ($i = 0; $i < $n; $i++) {
                $rIdx = rand(1, strlen($chr));
                $gpass .=substr($chr, $rIdx, 1);
            }

            $cc=$this->db->query('SELECT count(kd_jadwal) as jml FROM tbl_jadwal_matkul WHERE kd_jadwal LIKE "'.$prodi.'%" and kd_tahunajaran = '.$this->session->userdata('tahunajaran').'')->row();

            $hasil=$cc->jml;

            if($hasil==0) {
                $hasilakhir="".$q_jur->kd_prodi."/".$gpass."/001";
            } elseif($hasil < 10) {
                $hasilakhir="".$q_jur->kd_prodi."/".$gpass."/00".($hasil+1);
            } elseif($hasil < 100) {
                $hasilakhir="".$q_jur->kd_prodi."/".$gpass."/0".($hasil+1);
            } elseif ($hasil < 1000) {
                $hasilakhir="".$q_jur->kd_prodi."/".$gpass."/".($hasil+1);
            }elseif ($hasil < 10000){
                $hasilakhir="".$q_jur->kd_prodi."/".$gpass."/".($hasil+1);
            }
            //edit danu
            $time2 = strtotime($jam_masuk) + $w*60;

            $jumlah_time = date('H:i', $time2);

            $session = $this->session->userdata('sess_login');

            $user = $session['username'];

            $cekpemakaianruangan = $this->db->query("select * from tbl_jadwal_matkul where (waktu_selesai > '".$jam_masuk."' and waktu_mulai < '".$jumlah_time."') and kd_ruangan = '".$ruangan."' and hari = ".$hari_kuliah." and kd_tahunajaran = '".$this->session->userdata('tahunajaran')."'")->result();

            if (count($cekpemakaianruangan) > 0) {
                echo "<script>alert('Ruangan Sudah Digunakan Pada Waktu Tersebut');history.go(-1);</script>";
            } else {
                $data = array(

                    'kelas'         => $kelas,

                    'waktu_kelas'   => $st_kelas,

                    'kd_jadwal'     => $hasilakhir,

                    'hari'          => $hari_kuliah,

                    'id_matakuliah' => $nama_matakuliah,

                    'kd_matakuliah' => $kd_mk,

                    'kd_ruangan'    => $ruangan,

                    'waktu_mulai'   => $jam_masuk,

                    'waktu_selesai' => $jumlah_time,

                    'kd_tahunajaran'=> $this->session->userdata('tahunajaran'),

                    'audit_date'    => date('Y-m-d H:i:s'),

                    'audit_user'    => $user

                    );

                $this->db->insert('tbl_jadwal_matkul', $data);

                redirect('perkuliahan/jdl_kuliah2/view_matakuliah','refresh');
            }
    }


    function update_dosen(){

        $dosen = $this->input->post('kd_dosen');

        $id    = $this->input->post('id_jadwal');


        $jadwal = $this->db->where('id_jadwal', $id)
                               ->get('tbl_jadwal_matkul')->row();

        $jadwal_dosen = $this->db->query('SELECT * FROM tbl_jadwal_matkul jdl 
                                          WHERE jdl.`kd_tahunajaran` = "'. $this->session->userdata('tahunajaran').'" AND 
                                          jdl.`kd_dosen` = "'.$dosen.'" AND jdl.`hari` = '.$jadwal->hari.'')->result();


        $cekjumlahsks = $this->db->query('SELECT SUM(mk.`sks_matakuliah`) AS jumlah FROM tbl_jadwal_matkul jdl
                                    JOIN tbl_matakuliah mk ON jdl.`kd_matakuliah` = mk.`kd_matakuliah`
                                    WHERE (mk.nama_matakuliah not like "tesis%" and mk.nama_matakuliah not like "skripsi%" and mk.nama_matakuliah not like "kerja praktek%" and mk.nama_matakuliah not like "magang%" and mk.nama_matakuliah not like "kuliah kerja%") and jdl.kd_tahunajaran = "'.$this->session->userdata('tahunajaran').'" and jdl.`kd_dosen` = "'.$dosen.'" AND SUBSTR(jdl.`kd_jadwal`,1,5) = mk.`kd_prodi` and jdl.gabung != 1')->row()->jumlah;

        if ($cekjumlahsks > 16) {
            echo "<script>alert('SKS dosen melebihi');history.go(-1);</script>";
                exit();
        }
        
        // var_dump($jadwal_dosen);die();

        $jadwal_masuk1 = $jadwal->waktu_mulai;
        $jadwal_keluar1= $jadwal->waktu_selesai;
         
        $msk1 = explode(":", $jadwal_masuk1);
        $menit_masuk1 = $msk1[0] * 60 + $msk1[1];

        $klr1 = explode(":", $jadwal_keluar1);
        $menit_keluar1 = $klr1[0] * 60 + $klr1[1];

        foreach ($jadwal_dosen as $key) {

            $jadwal_masuk2 = $key->waktu_mulai;
            $jadwal_keluar2= $key->waktu_selesai;
             
            $msk2 = explode(":", $jadwal_masuk2);
            $menit_masuk2 = $msk2[0] * 60 + $msk2[1];

            $klr2 = explode(":", $jadwal_keluar2);
            $menit_keluar2 = $klr2[0] * 60 + $klr2[1];

            if (($menit_masuk1 > $menit_masuk2 && $menit_masuk1 < $menit_keluar2) || ($menit_keluar1 > $menit_masuk2 && $menit_keluar1 < $menit_keluar2) || ($menit_masuk1 == $menit_masuk2)) {
                echo "<script>alert('Jadwal Dosen Bentrok');
                document.location.href='".base_url()."perkuliahan/jdl_kuliah2/view_matakuliah';</script>";
                exit();
            }
            
            
        }
        

        $data = array( 'kd_dosen' => $dosen );

        $this->db->where('id_jadwal',$id)

                 ->update('tbl_jadwal_matkul', $data);

        redirect('perkuliahan/jdl_kuliah/view_matakuliah','refresh');

    }

    function delete($id)
    {
        $this->db->query('delete from tbl_jadwal_matkul where id_jadwal = '.$id.'');
        redirect('perkuliahan/jdl_kuliah2/view_matakuliah','refresh');
    }

    function edit_jadwal($id){
        //die($id);
        $session = $this->session->userdata('sess_login');

        $user = $session['username'];

        $data['rows'] = $this->db->query('SELECT DISTINCT * FROM tbl_jadwal_matkul mk
                            LEFT JOIN tbl_matakuliah a ON mk.`id_matakuliah`=a.`id_matakuliah`
                            left JOIN tbl_ruangan r ON r.`id_ruangan` = mk.`kd_ruangan`
                            left JOIN tbl_fakultas b ON a.`kd_fakultas` = b.`kd_fakultas`
                            LEFT JOIN tbl_karyawan kry ON kry.`nik` = mk.`kd_dosen`
                            WHERE mk.`id_jadwal` = '.$id.' AND SUBSTR(mk.kd_jadwal,1,5) = a.kd_prodi' )->row();

        $data['gedung'] = $this->db->get('tbl_gedung')->result();
        $this->load->view('v_jdl_kuliah_edit', $data);

    }

    function edit_jadwal_prodi($id){
        $data['rows'] = $this->db->query('SELECT * FROM tbl_jadwal_matkul mk
                                LEFT JOIN tbl_matakuliah a ON mk.`id_matakuliah`=a.`id_matakuliah`
                                left JOIN tbl_ruangan r ON r.`id_ruangan` = mk.`kd_ruangan`
                                left JOIN tbl_fakultas b ON a.`kd_fakultas` = b.`kd_fakultas`
                                LEFT JOIN tbl_karyawan kry ON kry.`nik` = mk.`kd_dosen`
                                WHERE mk.`id_jadwal` = '.$id.' AND SUBSTR(mk.kd_jadwal,1,5) = a.kd_prodi')->row();

        $data['gedung'] = $this->db->get('tbl_gedung')->result();
    
        $this->load->view('v_jdl_kuliah_edit_prodi', $data);

    }

    function update_data_matakuliah()
    {
        date_default_timezone_set("Asia/Jakarta"); 

        extract(PopulateForm());

        $q = $this->db->where('id_matakuliah', $nama_matakuliah)->get('tbl_matakuliah')->row();

        $w = ($q->sks_matakuliah)*50;

        $ambil_kd = $this->db->query('SELECT * from tbl_matakuliah where id_matakuliah = '.$nama_matakuliah.'')->row(); 
       
        $kd_mk = $ambil_kd->kd_matakuliah;

        $id_jur = $this->session->userdata('id_jurusan_prasyarat');

        $q_jur = $this->db->where('kd_prodi',$id_jur)->get('tbl_jurusan_prodi')->row();

        $time2 = strtotime($jam_masuk) + $w*60;

        $jumlah_time = date('H:i', $time2);

        $cekpemakaianruangan    = $this->db->query("SELECT * from tbl_jadwal_matkul where (waktu_selesai > '".$jam_masuk."' 
                                                    and waktu_mulai < '".$jumlah_time."') and kd_ruangan = '".$ruangan."' 
                                                    and hari = ".$hari_kuliah." 
                                                    and kd_tahunajaran = '".$this->session->userdata('tahunajaran')."' 
                                                    and id_jadwal != ".$id_jadwal."")->result();

        if (count($cekpemakaianruangan) > 0) {

            echo "<script>alert('Ruangan Sudah Digunakan Pada Waktu Tersebut');history.go(-1);</script>";

        } else {

            $data = array(

                'kelas'         => $kelas,

                'hari'          => $hari_kuliah,

                'id_matakuliah' => $nama_matakuliah,

                'kd_matakuliah' => $kd_mk,

                'kd_ruangan'    => $ruangan,

                'waktu_mulai'   => $jam_masuk,

                'waktu_selesai' => $jumlah_time,

                'waktu_kelas'   => $st_kelas,

                'kd_tahunajaran'=> $this->session->userdata('tahunajaran')

                );

            $this->db->where('id_jadwal', $id_jadwal)->update('tbl_jadwal_matkul',$data);

            redirect(base_url('perkuliahan/jdl_kuliah2/view_matakuliah'));
        }

    }

    function update_data_matakuliah_prodi()
    {
        date_default_timezone_set("Asia/Jakarta"); 

            $data = array(

                'kelas'         => $this->input->post('kelas'),

                'kd_ruangan'    => $this->input->post('ruangan')

                );

            $this->db->where('id_jadwal', $this->input->post('idjad'))
                      ->update('tbl_jadwal_matkul',$data);

            redirect(base_url('perkuliahan/jdl_kuliah/view_matakuliah'));
    }

    function view_matakuliah_test()
    {
        $data['matakuliah'] = $this->db->query('SELECT * FROM tbl_jadwal_matkul mk
                                LEFT JOIN tbl_matakuliah a ON mk.`kd_matakuliah`=a.`kd_matakuliah`
                                LEFT JOIN tbl_ruangan r ON r.`id_ruangan` = mk.`kd_ruangan`
                                LEFT JOIN tbl_fakultas b ON a.`kd_fakultas` = b.`kd_fakultas`
                                LEFT JOIN tbl_karyawan kry ON kry.`nid` = mk.`kd_dosen`
                                WHERE mk.`kd_jadwal` LIKE "%'.$this->session->userdata('id_jurusan_prasyarat').'%"
                                AND a.`kd_prodi` = "'.$this->session->userdata('id_jurusan_prasyarat').'"')->result();
        

        $data['rows'] = $this->db->get('tbl_jadwal_matkul')->result();

        $data['matkul'] = $this->app_model->get_detail_matakuliah($this->session->userdata('id_fakultas_prasyarat'), $this->session->userdata('id_jurusan_prasyarat'));

        $data['gedung'] = $this->db->get('tbl_gedung')->result();

        $data['rows']   = $this->db->select('*')

                                  ->from('tbl_jadwal_matkul a')

                                   ->join('tbl_matakuliah b','a.kd_matakuliah = b.kd_matakuliah')

                                   ->join('tbl_ruangan c','a.kd_ruangan = c.id_ruangan')

                                   ->join('tbl_karyawan d','a.kd_dosen = d.nik','left')

                                  ->get()->result();

        $data['page'] ='v_jdl_kuliah_detail_test';

        $this->load->view('template/template', $data);
    }

    function cetak_jadwal_all(){
        $logged = $this->session->userdata('sess_login');
        $userid = $logged['userid'];

        //$data['prodi'] = $this->db->query('select * from tbl_jurusan_prodi where kd_prodi = '.$this->session->userdata('id_jurusan_prasyarat').'')->row()->kd_prodi;

        if ($this->session->userdata('tahunajaran') > 20162) {
            $data['rows'] = $this->db->query('SELECT * FROM tbl_jadwal_matkul mk
                                JOIN tbl_matakuliah a ON mk.`kd_matakuliah`=a.`kd_matakuliah`
                                LEFT JOIN tbl_ruangan r ON r.`id_ruangan` = mk.`kd_ruangan`
                                JOIN tbl_fakultas b ON a.`kd_fakultas` = b.`kd_fakultas`
                                LEFT JOIN tbl_karyawan kry ON kry.`nid` = mk.`kd_dosen`
                                JOIN tbl_kurikulum_matkul_new km ON mk.`kd_matakuliah` = km.`kd_matakuliah`
                                JOIN tbl_kurikulum_new kur ON kur.`kd_kurikulum` = km.`kd_kurikulum` 
                                WHERE mk.`kd_jadwal` LIKE "'.$userid.'%"
                                AND a.`kd_prodi` = "'.$userid.'"
                                AND kur.`kd_prodi` = "'.$userid.'"
                                AND mk.`kd_tahunajaran` LIKE "'.$this->session->userdata('tahunajaran').'%"
                                GROUP BY mk.`kd_jadwal`
                                ORDER BY km.`semester_kd_matakuliah`')->result();
        } else {
            $data['rows'] = $this->db->query('SELECT * FROM tbl_jadwal_matkul mk
                                JOIN tbl_matakuliah a ON mk.`kd_matakuliah`=a.`kd_matakuliah`
                                LEFT JOIN tbl_ruangan r ON r.`id_ruangan` = mk.`kd_ruangan`
                                JOIN tbl_fakultas b ON a.`kd_fakultas` = b.`kd_fakultas`
                                LEFT JOIN tbl_karyawan kry ON kry.`nid` = mk.`kd_dosen`
                                JOIN tbl_kurikulum_matkul km ON mk.`kd_matakuliah` = km.`kd_matakuliah`
                                JOIN tbl_kurikulum kur ON kur.`kd_kurikulum` = km.`kd_kurikulum` 
                                WHERE mk.`kd_jadwal` LIKE "'.$userid.'%"
                                AND a.`kd_prodi` = "'.$userid.'"
                                AND kur.`kd_prodi` = "'.$userid.'"
                                AND mk.`kd_tahunajaran` LIKE "'.$this->session->userdata('tahunajaran').'%"
                                GROUP BY mk.`kd_jadwal`
                                ORDER BY km.`semester_kd_matakuliah`')->result();
        }

        $this->load->view('print_cetak_jadwal', $data);

    }

    function edit_jadwal_new($id){
        //die($id);
        $session = $this->session->userdata('sess_login');

        $user = $session['username'];

            
            $data['rows'] = $this->db->query('SELECT DISTINCT * FROM tbl_jadwal_matkul mk
                                JOIN tbl_matakuliah a ON mk.`kd_matakuliah`=a.`kd_matakuliah`
                                left JOIN tbl_ruangan r ON r.`id_ruangan` = mk.`kd_ruangan`
                                left JOIN tbl_fakultas b ON a.`kd_fakultas` = b.`kd_fakultas`
                                LEFT JOIN tbl_karyawan kry ON kry.`nik` = mk.`kd_dosen`
                                WHERE mk.`id_jadwal` = '.$id.' AND SUBSTR(mk.kd_jadwal,1,5) = a.kd_prodi' )->row();
        

        $data['gedung'] = $this->db->get('tbl_gedung')->result();
        $this->load->view('v_jdl_edit', $data);

    }

    function penugasan_new($id){
        $data['id_jadwal'] = $id;
        $this->load->view('v_jdl_penugasan', $data);
    }

    function save_dosen(){
        $nid  = $this->input->post('kd_dosen');
        $jdl = $this->input->post('id_jadwal');
        $data = array('kd_dosen' => $nid);
        
        $cekjumlahsks = $this->db->query('SELECT SUM(mk.`sks_matakuliah`) AS jumlah FROM tbl_jadwal_matkul jdl
                                    JOIN tbl_matakuliah mk ON jdl.`kd_matakuliah` = mk.`kd_matakuliah`
                                    WHERE (mk.nama_matakuliah not like "tesis%" and mk.nama_matakuliah not like "skripsi%" and mk.nama_matakuliah not like "kerja praktek%" and mk.nama_matakuliah not like "magang%" and mk.nama_matakuliah not like "kuliah kerja%") and jdl.kd_tahunajaran = "'.$this->session->userdata('tahunajaran').'" and jdl.`kd_dosen` = "'.$nid.'" AND SUBSTR(jdl.`kd_jadwal`, 1, 5) = mk.`kd_prodi` and jdl.gabung != 1')->row()->jumlah;

        if ($cekjumlahsks > 18) {
            echo "<script>alert('SKS dosen melebihi');history.go(-1);</script>";
                exit();
        }

        $jadwal = $this->db->where('id_jadwal', $jdl)
                               ->get('tbl_jadwal_matkul')->row();

        $jadwal_dosen = $this->db->query('SELECT * FROM tbl_jadwal_matkul jdl 
                                          WHERE jdl.`kd_tahunajaran` = "'. $this->session->userdata('tahunajaran').'" AND 
                                          jdl.`kd_dosen` = "'.$nid.'" AND jdl.`hari` = '.$jadwal->hari.'')->result();
        
        // var_dump($jadwal_dosen);die();

        $jadwal_masuk1 = $jadwal->waktu_mulai;
        $jadwal_keluar1= $jadwal->waktu_selesai;
         
        $msk1 = explode(":", $jadwal_masuk1);
        $menit_masuk1 = $msk1[0] * 60 + $msk1[1];

        $klr1 = explode(":", $jadwal_keluar1);
        $menit_keluar1 = $klr1[0] * 60 + $klr1[1];

        foreach ($jadwal_dosen as $key) {

            $jadwal_masuk2 = $key->waktu_mulai;
            $jadwal_keluar2= $key->waktu_selesai;
             
            $msk2 = explode(":", $jadwal_masuk2);
            $menit_masuk2 = $msk2[0] * 60 + $msk2[1];

            $klr2 = explode(":", $jadwal_keluar2);
            $menit_keluar2 = $klr2[0] * 60 + $klr2[1];

            if (($menit_masuk1 > $menit_masuk2 && $menit_masuk1 < $menit_keluar2) || ($menit_keluar1 > $menit_masuk2 && $menit_keluar1 < $menit_keluar2) || ($menit_masuk1 == $menit_masuk2)) {
                echo "<script>alert('Jadwal Dosen Bentrok');
                document.location.href='".base_url()."perkuliahan/jdl_kuliah/view_matakuliah';</script>";
                exit();
            }
            
            
        }

        $this->db->where('id_jadwal',$jdl)->update('tbl_jadwal_matkul', $data);
        redirect('perkuliahan/jdl_kuliah/view_matakuliah','refresh');
    }

    function edit_nama_kelas($id)
    {
        $data['gedung'] = $this->db->get('tbl_gedung')->result();
        $data['jadwal'] = $this->db->where('id_jadwal', $id)
                               ->get('tbl_jadwal_matkul')->row();
        $data['id_jadwal'] = $id;
        $this->load->view('v_jdl_edit_kelas', $data);
    }

    function save_nama_kelas(){
        $kelas  = $this->input->post('kelas');
        $jdl = $this->input->post('id_jadwal');
        $ruang = $this->input->post('ruangan');
        $data = array('kelas' => $kelas,'kd_ruangan' => $ruang);

        $this->db->where('id_jadwal',$jdl)->update('tbl_jadwal_matkul', $data);
        redirect('perkuliahan/jdl_kuliah/view_matakuliah','refresh');
    }

}



/* End of file jdl_kuliah.php */

/* Location: ./application/controllers/jdl_kuliah.php */
