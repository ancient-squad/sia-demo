<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ruang extends MY_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->model('temph_model');
	}

	public function index()
	{
		$data['buil'] = $this->app_model->getdata('tbl_gedung','id_gedung','asc')->result();
		$data['room'] = $this->temph_model->load_room()->result();
		$data['page'] = "v_addroom";
		$this->load->view('template/template', $data);
	}

	function add_room()
	{
		extract(PopulateForm());
		$data = array(
			'kode_ruangan'	=> $kode,
			'ruangan'		=> $room,
			'deskripsi'		=> $desc,
			'id_lantai'		=> $lants,
			'kuota'			=> $kuota,
			'tipe'			=> $tipe
			);
		$this->db->insert('tbl_ruangan', $data);
		echo "<script>alert('Berhasil Menyimpan!');history.go(-1);</script>";
	}

	function getfloor($id)
	{
		$data = $this->app_model->getdetail('tbl_lantai','id_gedung',$id,'id_lantai','asc')->result();
		$out = "<select class='form-control' name='lants' id='lantai'>";
        foreach ($data as $row) {
            $out .= "<option value='".$row->id_lantai."'>".$row->lantai."</option>";
        }
        $out .= "</select>";
        echo $out;
	}

	function del_room($id)
	{
		$this->db->where('id_ruangan', $id);
		$this->db->delete('tbl_ruangan');
		echo "<script>alert('Berhasil Menghapus!');history.go(-1);</script>";
	}

	function load_edit($id)
	{
		$data['ruang'] = $this->temph_model->load_edit_room($id)->row();
		$this->load->view('v_edit_room', $data);
	}

	function change_room()
	{
		extract(PopulateForm());
		$data = array(
			'kode_ruangan'	=> $kode,
			'ruangan'		=> $room,
			'deskripsi'		=> $desc,
			'id_lantai'		=> $lants,
			'kuota'			=> $kuota,
			'tipe'			=> $tipe
			);
		$this->db->where('id_ruangan', $id);
		$this->db->update('tbl_ruangan', $data);
		echo "<script>alert('Berhasil Merubah!');history.go(-1);</script>";
	}

}

/* End of file Ruang.php */
/* Location: ./application/modules/perkuliahan/controllers/Ruang.php */