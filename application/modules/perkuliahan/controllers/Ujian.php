<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Ujian extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		if ($this->session->userdata('sess_login') == TRUE) {
			$cekakses = $this->role_model->cekakses(35)->result();
			if ($cekakses != TRUE) {
				echo "<script>alert('Anda Tidak Berhak Mengakses !!');history.go(-1);</script>";
			}
		} else {
			redirect('auth','refresh');
		}
	}

	public function index()
	{
		$data['fakultas'] =$this->app_model->getdata('tbl_fakultas', 'id_fakultas', 'ASC')->result();
		$data['tahunajar']=$this->app_model->getdata('tbl_tahunakademik', 'kode', 'ASC')->result();

		$data['page'] ='form_cetak_kartu';
		$this->load->view('template/template', $data);
	}

	function save_session(){

		$fakultas = explode('-',$this->input->post('fakultas'));

		$jurusan = explode('-',$this->input->post('jurusan'));

        $tahunajaran = $this->input->post('tahunajaran');

       

        $this->session->set_userdata('tahunajaran', $tahunajaran);

		$this->session->set_userdata('id_fakultas_prasyarat', $fakultas[0]);

		$this->session->set_userdata('nama_fakultas_prasyarat', $fakultas[1]);

		$this->session->set_userdata('id_jurusan_prasyarat', $jurusan[0]);

		$this->session->set_userdata('nama_jurusan_prasyarat', $jurusan[1]);

        

		redirect(base_url('perkuliahan/ujian/load_angkatan'));

	}

	function get_jurusan($id){

		$jrs = explode('-',$id);

        $jurusan = $this->app_model->getdetail('tbl_jurusan_prodi', 'kd_fakultas', $jrs[0], 'id_prodi', 'ASC')->result();

		$out = "<select class='form-control' name='jurusan' id='jurs'><option>--Pilih Jurusan--</option>";

        foreach ($jurusan as $row) {

            $out .= "<option value='".$row->kd_prodi."'>".$row->prodi. "</option>";

        }

        $out .= "</select>";

        echo $out;

	}

	function load_angkatan(){

		//die($this->session->userdata('id_jurusan_prasyarat'));


		$data['rows'] = $this->db->query('SELECT DISTINCT a.`TAHUNMSMHS`, jrs.`prodi`,jrs.`kd_prodi`
											FROM tbl_mahasiswa a
											RIGHT JOIN tbl_verifikasi_krs krs ON krs.`npm_mahasiswa` = a.`NIMHSMSMHS`
											JOIN tbl_jurusan_prodi jrs ON jrs.`kd_prodi` = a.`KDPSTMSMHS`
											WHERE a.`KDPSTMSMHS` = "'.$this->session->userdata('id_jurusan_prasyarat').'"
											ORDER BY a.`TAHUNMSMHS`'
										)->result();
		//var_dump($data['rows']);die();

		$data['page'] ='load_angkatan';
		$this->load->view('template/template', $data);
	}

	function cetak_kartu($kd_prodi,$tahun){
		$this->load->library('Cfpdf');

		 // $data['rows'] = $this->db->query('SELECT COUNT(*)
			// 									FROM tbl_mahasiswa a
			// 									RIGHT JOIN tbl_verifikasi_krs krs ON krs.`npm_mahasiswa` = a.`NIMHSMSMHS`
			// 									JOIN tbl_jurusan_prodi jrs ON jrs.`kd_prodi` = a.`KDPSTMSMHS`
			// 									RIGHT JOIN tbl_sinkronisasi_renkeu renkeu ON renkeu.`npm_mahasiswa` = a.`NIMHSMSMHS`
			// 									WHERE a.`KDPSTMSMHS` = "74201" AND a.TAHUNMSMHS = "2012" '
		 // 								)->result();

		$data['name'] = ''.$kd_prodi.''.$tahun.'';

		$this->db->select('*');
		$this->db->from('tbl_mahasiswa');
		$this->db->join('tbl_verifikasi_krs', 'tbl_verifikasi_krs.npm_mahasiswa = tbl_mahasiswa.NIMHSMSMHS', 'right');
		$this->db->where('tbl_mahasiswa.KDPSTMSMHS', $kd_prodi);
		$this->db->where('tbl_mahasiswa.TAHUNMSMHS', $tahun);
		$data['rows'] = $this->db->get()->result();

		$this->load->view('welcome/print/kartu_uts_p',$data);
	}

	// function cetak_kartu(){
	// 	$this->load->library('Cfpdf');

	// 	$id="201210225045";

	// 	$data['npm'] = substr($id, 0,12);
	// 	$data['ta']  = substr($id, 12,4);
		
	// 	$a=substr($id, 16,1);

	// 	//var_dump($data);die();

	// 	if ($a == 1) {
	// 		$b = 'Ganjil';
	// 	} else {
	// 		$b = 'Genap';
	// 	}
		

	// 	$data['gg']  = $b;

	// 	$this->load->view('welcome/print/kartu_uts',$data);
	// }

}

/* End of file ujian.php */
/* Location: ./application/controllers/ujian.php */