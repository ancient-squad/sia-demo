<div class="modal-header">
    <h3>Detail Kelas Turunan</h3>
</div>
<div class="modal-body">
    <table>
        <tr>
            <td>Matakuliah</td>
            <td>:</td>
            <td>&nbsp;<b><?= get_nama_mku($parentDetail->kd_matakuliah).' ('.$parentDetail->kd_matakuliah.')' ?></b></td>
        </tr>
        <tr>
            <td>Dosen</td>
            <td>:</td>
            <td>&nbsp;<b><?= nama_dsn($parentDetail->kd_dosen) ?></b></td>
        </tr>
        <tr>
            <td>Kelas</td>
            <td>:</td>
            <td>&nbsp;<b><?= $parentDetail->kelas ?></b></td>
        </tr>
        <tr>
            <td>Kategori Kelas</td>
            <td>:</td>
            <td>&nbsp;<b><?= kategori_kelas($parentDetail->waktu_kelas) ?></b></td>
        </tr>
    </table>
    <hr>
    <table class="table table-bordered">
        <thead>
            <tr>
                <th>No</th>
                <th>Kode Matakuliah</th>
                <th>Nama Matakuliah</th>
                <th>Fakultas</th>
                <th>Dosen</th>
                <th>Kelas</th>
                <th>Kategori Kelas</th>
            </tr>
        </thead>
        <tbody>
            <?php $no=1; foreach ($jadwals as $jadwal) { ?>
                <tr>
                    <td><?= $no ?></td>
                    <td><?= $jadwal->kd_matakuliah ?></td>
                    <td><?= get_nama_mku($jadwal->kd_matakuliah) ?></td>
                    <td><?= get_fak(substr($jadwal->kd_jadwal, 0,1)) ?></td>
                    <td><?= nama_dsn($jadwal->kd_dosen) ?></td>
                    <td><?= $jadwal->kelas ?></td>
                    <td><?= kategori_kelas($jadwal->waktu_kelas) ?></td>
                </tr>        
            <?php $no++; } ?>
        </tbody>
    </table>
</div>
<div class="modal-footer">
    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
</div>