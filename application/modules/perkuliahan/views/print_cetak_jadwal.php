<?php
header("Content-Type: application/xls");    
header("Content-Disposition: attachment; filename=jadwal_perkuliahan.xls");  
header("Pragma: no-cache"); 
header("Expires: 0");
?>


<style>
table, td, th {
    border: 1px solid black;
}



th {
    background-color: blue;
    color: black;
}
</style>

<!-- <h4>JADWAL MATAKULIAH <?php echo $prodi; ?> TAHUNAJARAN </h4> -->

<table border="3">
    <thead>
        <tr>
            <th colspan="13">Daftar Jadwal Perkuliahan Tahun Ajaran <?= get_thajar($this->session->userdata('tahunajaran')) ?> Semester <?= $this->session->userdata('semester') ?></th>
        </tr>
        <tr> 

            <th>No</th>

            <th>Semester</th>

            <th>Kelas</th>

            <th>Hari</th>

            <th>Kode MK</th>

            <th>Matakuliah</th>

            <th>SKS</th>

            <th>Waktu</th>

            <th>Ruang</th>

            <th>Kapasitas</th>

            <!-- <th>Peserta</th> -->

            <th>Dosen</th>

            <th>Kode Jadwal</th>

            <th>Jumlah Peserta</th>

        </tr>
    </thead>
    <tbody>
        
        <?php $no=1; foreach ($rows as $key) { 

            $jumlahPeserta = $this->db->query('SELECT DISTINCT npm_mahasiswa FROM tbl_krs WHERE kd_jadwal = "'.$key->kd_jadwal.'"');
            
            if (substr($key->kuota, 0,2) == NULL ) {
                $warna ='';
            }elseif (substr($key->kuota, 0,2) < $jumlahPeserta->num_rows()) {
                $warna = 'style="background:#FFB200;"';
            }else{
                $warna ='';
            }
        ?>
        
        <tr>
            <td <?= $warna; ?>><?= $no ?></td>

            <td <?= $warna; ?>><?= $key->semester_kd_matakuliah ?></td> 

            <td <?= $warna; ?>><?= $key->kelas ?></td>

            <td <?= $warna; ?>><?= notohari($key->hari); ?></td>

            <td <?= $warna; ?>><?= $key->kd_matakuliah ?></td>

            <td <?= $warna; ?>><?= $key->nama_matakuliah ?></td>

            <td <?= $warna; ?>><?= $key->sks_matakuliah ?></td>

            <td <?= $warna; ?>><?= $key->waktu_mulai.'-'.$key->waktu_selesai ?></td>

            <?php if ($key->kode_ruangan == NULL || $key->kode_ruangan == '') { ?>
                <td <?= $warna; ?>>-</td> 
                <td <?= $warna; ?>>-</td> 
            <?php }else{ ?>
                <td <?= $warna; ?>><?= $key->kode_ruangan; ?></td> 
                <td <?= $warna; ?>><?= substr($key->kuota, 0,2)?></td>
            <?php }?>

            <td <?= $warna; ?>><?= $key->nama ?></td>

            <?php $pecah = explode('/', $key->kd_jadwal);  ?>
            <td><?= $pecah[1]; ?></td>

            <td><?= $jumlahPeserta->num_rows(); ?></td>

            <?php $no++; } ?>

        </tr>
        
         
    </tbody>
</table>