<div class="modal-header">
    <h3>Detail Kelas Induk</h3>
</div>
<div class="modal-body">
    <table>
        <tr>
            <td>Matakuliah</td>
            <td>:</td>
            <td>&nbsp;<b><?= get_nama_mku($parentDetail->kd_matakuliah).' ('.$parentDetail->kd_matakuliah.')' ?></b></td>
        </tr>
        <tr>
            <td>Dosen</td>
            <td>:</td>
            <td>&nbsp;<b><?= nama_dsn($parentDetail->kd_dosen) ?></b></td>
        </tr>
        <tr>
            <td>Kelas</td>
            <td>:</td>
            <td>&nbsp;<b><?= $parentDetail->kelas ?></b></td>
        </tr>
        <tr>
            <td>Hari/Jam</td>
            <td>:</td>
            <td>&nbsp;<b><?= notohari($parentDetail->hari) ?> / <?= $parentDetail->waktu_mulai ?> - <?= $parentDetail->waktu_selesai ?></b></td>
        </tr>
        <tr>
            <td>Kategori Kelas</td>
            <td>:</td>
            <td>&nbsp;<b><?= kategori_kelas($parentDetail->waktu_kelas) ?></b></td>
        </tr>
    </table>
</div>
<div class="modal-footer">
    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
</div>