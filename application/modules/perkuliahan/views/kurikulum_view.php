<style>
	#dup label{
		margin-bottom: 0px;
	}
</style>

<div class="row">
	<div class="span12">      		  		
  		<div class="widget ">
  			<div class="widget-header">
  				<i class="icon-list"></i>
  				<h3>Data Kurikulum</h3>
			</div> <!-- /widget-header -->
			
			<div class="widget-content">
				<div class="span11">
					<?php if ($aktif == 1) { ?>
						<a data-toggle="modal" id="new" class="btn btn-primary"> New Data </a> <br><hr>
					<?php } ?>
					<table id="tabel_kurikulum" class="table table-bordered table-striped">
	                	<thead>
	                        <tr> 
	                        	<th>No</th>
								<th>Kode Kurikulum</th>
	                        	<th>Kurikulum</th>
	                        	<th>Tahun Ajaran</th>
	                        	<th>Prodi</th>
                                <th>Fakultas</th>
                                <th>Status</th>
	                            <th width="120">Aksi</th>
								<th></th>
								<th></th>
								<th></th>
								<th></th>
	                        </tr>
	                    </thead>
	                    <tbody>
                            <?php $no = 1; foreach ($data_table as $row) { ?>
	                        <tr>
								<td><?= $no; ?></td>
	                        	<td><?= $row->kd_kurikulum; ?></td>
	                        	<td><?= $row->kurikulum; ?></td>
                                <td><?= $row->tahun_akademik; ?></td>
                                <td><?= $row->prodi; ?></td>
                                <td><?= $row->fakultas; ?></td>
                                <td><?= $row->status_kurikulum === '1' ? 'Aktif' : 'Tidak Aktif'; ?></td>
	                        	<td class="td-actions">
									<a class="btn btn-success btn-small" href="<?= base_url('perkuliahan/kurikulum/view_kurikulum/'.$row->kd_kurikulum); ?>">
										<i class="btn-icon-only icon-eye-open"></i>
									</a>
									<?php if ($aktif == 1) { ?>
                                    	<button type="button" class="btn btn-primary btn-small edit"><i class="btn-icon-only icon-pencil"> </i></button>
									<?php } ?>
									<a href="<?= base_url('perkuliahan/kurikulum/delte_kurikulum/' . $row->id_kurikulum) ?>" class="btn btn-small btn-danger" onclick="return confirm('Yakin ingin menghapus data ini?')"><i class="icon-trash"></i></a>
								</td>
								<td><?= $row->id_kurikulum; ?></td>
								<td><?= $row->kd_prodi; ?></td>
								<td><?= $row->kd_fakultas; ?></td>
								<td><?= $row->tahun_ajaran_kurikulum; ?></td>
	                        </tr>
	                        <?php $no++; } ?>
	                    </tbody>
	               	</table>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="edit_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">BUAT KURIKULUM BARU</h4>
            </div>
            <form class ='form-horizontal' action="<?= base_url();?>perkuliahan/kurikulum/add_new" method="post">
                <div class="modal-body">    
                    <div class="control-group" id="">
                        <label class="control-label">Kode Kurikulum</label>
                        <div class="controls">
							<input type="hidden" id="id_kurikulum" name="id_kurikulum" />
                            <input type="text" id="kd_kurikulum" name="kd_kurikulum" class="span3" placeholder="Input Kode Kurikulum" class="form-control" required/>
							<div id="dup">
								<p></p>
								<label class="alert">Kode Sudah Ada</label>
							</div>
						</div>
                    </div>
                    <div class="control-group" id="">
                        <label class="control-label">Kurikulum</label>
                        <div class="controls">
                            <input type="text" id="kurikulum" name="kurikulum" class="span3" placeholder="Input Kurikulum" class="form-control" required/>
                        </div>
                    </div>

					<script>
						$(document).ready(function(){
							$('#faks').change(function(){
								$.post('<?= base_url()?>perkuliahan/kurikulum/get_jurusan/'+$(this).val(),{},function(get){
									$('#jurs').html(get);
								});
							});
						});
                  	</script>

                    <?php 	
						$logged = $this->session->userdata('sess_login');
						$grup   = get_group($logged['id_user_group']);
						if ((in_array(1, $grup)) or (in_array(10, $grup))) { ?>

                    	<div class="control-group" id="">
	                        <label class="control-label">Fakultas</label>
	                        <div class="controls">
	                            <select class="form-control span3" name="kd_fakultas" id="faks">
                                    <option>--Pilih Fakultas--</option>
                                    <?php foreach ($fakultas as $row) { ?>
                                    <option value="<?= $row->kd_fakultas; ?>"><?= $row->fakultas;?></option>
                                    <?php } ?>
                              	</select>
	                        </div>
	                    </div>
	                    <div class="control-group" id="">
	                        <label class="control-label">Prodi</label>
	                        <div class="controls">
	                            <select class="form-control span3" name="kd_prodi" id="jurs">
                                    <option>--Pilih Jurusan--</option>
                              	</select>
	                        </div>
	                    </div>

                    <?php } elseif ((in_array(9, $grup))) { ?>
                    		<input type="hidden" value="<?= $kdfak; ?>" name="kd_fakultas"/>
                    		<div class="control-group" id="">
		                        <label class="control-label">Prodi</label>
		                        <div class="controls">
		                            <select class="form-control span3" name="kd_prodi" id="jurs">
	                                    <option>--Pilih Jurusan--</option>
	                                    <?php foreach ($listprodi as $key) {
	                                    	echo "<option value='".$key->kd_prodi."'> ".$key->prodi." </option>";
	                                    } ?>
                                  	</select>
		                        </div>
		                    </div>
                    <?php } else { $kdfak = $this->app_model->getdetail('tbl_jurusan_prodi','kd_prodi',$logged['userid'],'kd_prodi','asc')->row(); ?>
                    	<input type="hidden" value="<?= $logged['userid']; ?>" name="kd_prodi"/>
                    	<input type="hidden" value="<?= $kdfak->kd_fakultas; ?>" name="kd_fakultas"/>
                    <?php } ?>

                    <div class="control-group" id="">
                        <label class="control-label">Tahun Ajaran</label>
                        <div class="controls">
                            <select class="form-control span3" name="tahun_ajaran_kurikulum" id="tahun_ajaran_kurikulum" required="">
                                <option value="">--Pilih Tahun Ajaran--</option>
                                <?php foreach ($tahunajaran as $row) { ?>
                                	<option value="<?= $row->kode;?>"><?= $row->tahun_akademik;?></option>
                                <?php } ?>
                          	</select>
                        </div>
                    </div>
                    <div class="control-group" id="">
                        <label class="control-label">Aktif</label>
                        <div class="controls">
                            <input value="1" id="status" type="checkbox" name="status">
                        </div>
                    </div>
                </div> 
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
                    <input type="submit" name="save" class="btn btn-primary" value="Simpan"/>
                </div>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script type="text/javascript">
$(document).ready(function(){
	var table = $('#tabel_kurikulum');
	
	var oTable = table.dataTable({
		"aoColumnDefs": [{ "bVisible": false, "aTargets": [8,9,10,11] }]
	});
	
	$('#dup').hide();
	$('#kd_kurikulum').keyup(function(){
		var teks = $('#kd_kurikulum').val();
		$.ajax({
			url: "<?= base_url('perkuliahan/kurikulum/cek_dup'); ?>",
            type: "post",
            data: {data:teks},
            success: function(d) {
				if(d!=0){
					$('#dup').show();
				}else{
					$('#dup').hide();
				}
            }
        });
    });
	
	$('#new').click(function () {
		$('#edit_modal').modal('show');
		$('#id_kurikulum').val('');
		$('#kd_kurikulum').val('');
		$('#kurikulum').val('');
		$('#tahun_ajaran_kurikulum').val('');
		$('#faks').val('');
		$('#jurs').val('');
    });
	
	table.on('click', '.edit', function(e) {
		$('#edit_modal').modal('show');
		var nRow = $(this).parents('tr')[0];
        var aData = oTable.fnGetData(nRow);
		
		$('#id_kurikulum').val(aData[8]);
		$('#kd_kurikulum').val(aData[1]);
		$('#kd_kurikulum').attr('readonly', true);
		$('#kurikulum').val(aData[2]);
		$('#tahun_ajaran_kurikulum').val(aData[11]);
		$('#faks').val(aData[10]);
		$.post('<?= base_url()?>perkuliahan/kurikulum/get_jurusan/'+$('#faks').val(),{},function(get){
			$('#jurs').html(get);
			$('#jurs').val(aData[9]);
        });
		$('#status').attr('checked', true);
	});
});
</script>