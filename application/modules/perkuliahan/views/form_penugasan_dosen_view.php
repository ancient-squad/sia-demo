<script type="text/javascript">
function edit(idk){
        $('#edit').load('<?php echo base_url();?>form/penugasandosen/load_detail/'+idk);
    }
</script>
<div class="row">
	<div class="span12">      		  		
  		<div class="widget ">
  			<div class="widget-header">
  				<i class="icon-user"></i>
  				<h3>Data Pembimbing</h3>
			</div> <!-- /widget-header -->
			
			<div class="widget-content">
                <!-- <a href="#" class="btn btn-primary"><i class="btn-icon-only icon-print"> </i> Print</a>
                <hr> -->
				<div class="span11">
                    <table id="example1" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>NPM</th>
                                <th>Nama</th>
                                <th>Judul</th>
                                <th>Tahun Ajaran</th>
                                <th>Pembimbing 1</th>
                                <th>Pembimbing 2</th>
                                <th width="40">Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>No</td>
                                <td>NPM</td>
                                <td>Nama</td>
                                <td>Judul</td>
                                <td>Tahun Ajaran</td>
                                <td>Pembimbing 1</td>
                                <td>Pembimbing 2</td>
                                <td class="td-actions">
                                    <a class="btn btn-primary btn-small" onclick="edit(<?php //echo ''.$query->NIMHSMSMHS.$key->smtr.''; ?>)"  data-toggle="modal" href="#myModal"><i class="btn-icon-only icon-ok"> </i></a>
                                </td>
                            </tr>
                        </tbody>
                    </table>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content" id="edit">
            
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->