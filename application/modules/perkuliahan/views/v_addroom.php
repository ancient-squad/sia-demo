<script type="text/javascript">
    $(document).ready(function(){
        $('#gedung').change(function(){
            $.post('<?php echo base_url(); ?>perkuliahan/ruang/getfloor/'+$(this).val(),{},function(get){
                $('#lantai').html(get);
            });
        });
    });

    function edit(id)
    {
        $('#edit_ruang').load('<?php echo base_url(); ?>perkuliahan/ruang/load_edit/' + id);
    }
</script>

<div class="row">
	<div class="span12">      		  		
  		<div class="widget ">
  			<div class="widget-header">
                <i class="icon-home"></i>
  				<h3>Tambah Ruang Kelas</h3>
			</div> <!-- /widget-header -->
			
			<div class="widget-content">
				<div class="span11">
                    <a class="btn btn-primary" data-toggle="modal" href="#myModal" >Tambah Data</a><br><hr>
                    <table id="example1" class="table table-bordered table-striped">
                        <thead>
                            <tr> 
                                <th>No</th>
                                <th>Gedung</th>
                                <th>Lantai</th>
                                <th>Kode Ruang</th>
                                <th>Nama Ruang</th>
                                <th>Kuota</th>
                                <th>Tipe</th>
                                <th>Keterangan</th>
                                <th width="80">Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $no=1; foreach ($room as $row) { ?>
                            <tr>
                                <td><?php echo $no; ?></td>
                                <td><?php echo $row->gedung; ?></td>
                                <td><?php echo $row->lantai; ?></td>
                                <td><?php echo $row->kode_ruangan; ?></td>
                                <td><?php echo $row->ruangan; ?></td>
                                <td><?php echo $row->kuota; ?></td>
                                <?php if ($row->tipe == 1) {
                                    $type = 'Kelas';
                                } elseif ($row->tipe == 2) {
                                    $type = 'Non Kelas';
                                } else {
                                    $type = '-';
                                }
                                 ?>
                                 <td><?php echo $type; ?></td>
                                <td><?php echo $row->deskripsi; ?></td>
                                <td class="td-actions">
                                    <a class="btn btn-success btn-small" onclick="edit('<?php echo $row->id_ruangan; ?>')" href="#editModal" data-toggle="modal"><i class="btn-icon-only icon-pencil"> </i></a>
                                    <a onclick="return confirm('Apakah Anda Yakin Ingin Menghapus Ruang Tersebut?');" class="btn btn-danger btn-small" href="<?php echo base_url();?>perkuliahan/ruang/del_room/<?php echo $row->id_ruangan;?>"><i class="btn-icon-only icon-remove"> </i></a>
                                </td>
                            </tr>
                            <?php $no++; } ?>
                        </tbody>
                    </table>
                </div>                
            </div>
        </div>
    </div>
</div>

<!-- edit modal -->
<div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content" id="edit_ruang">
            
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Tambah Data</h4>
            </div>
            <form class ='form-horizontal' action="<?php echo base_url();?>perkuliahan/ruang/add_room" method="post" enctype="multipart/form-data">
                <div class="modal-body" style="margin-left: -60px;">  
                    <div class="control-group" id="">
                        <label class="control-label">Gedung</label>
                        <div class="controls">
                            <select class="form-control span4" name='building' id="gedung">
                                <option>--Pilih Gedung--</option>
                                <?php foreach ($buil as $row) { ?>
                                    <option value="<?php echo $row->id_gedung;?>"><?php echo $row->gedung;?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div> 
                    <div class="control-group" id="">
                        <label class="control-label">Lantai</label>
                        <div class="controls">
                            <select class="form-control span4" name='lants' id="lantai">
                                <option selected="" disabled="">--Pilih Lantai--</option>
                            </select>
                        </div>
                    </div>
                    <hr>
                    <div class="control-group" id="">
                        <label class="control-label">Kode Ruang</label>
                        <div class="controls">
                            <input type="text" name="kode" value="" class="form-control span4" placeholder="Kode Ruang">
                        </div>
                    </div>
                    <div class="control-group" id="">
                        <label class="control-label">Ruang</label>
                        <div class="controls">
                            <input type="text" name="room" value="" class="form-control span4" placeholder="Nama Ruang">
                        </div>
                    </div>
                    <div class="control-group" id="">
                        <label class="control-label">Kuota</label>
                        <div class="controls">
                            <input type="number" name="kuota" value="" class="form-control span4" placeholder="Kuota Ruang">
                        </div>
                    </div>
                    <div class="control-group" id="">
                        <label class="control-label">Tipe</label>
                        <div class="controls">
                            <select class="form-control span4" name='tipe' id="">
                                <option selected="" disabled="">--Pilih Tipe--</option>
                                <option value="1">Kelas</option>
                                <option value="2">Non Kelas</option>
                            </select>
                        </div>
                    </div>
                    <div class="control-group" id="">
                        <label class="control-label">Deskripsi</label>
                        <div class="controls">
                            <textarea name="desc" value="" class="form-control span4"></textarea>
                        </div>
                    </div>              
                </div> 
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Keluar</button>
                    <input type="submit" class="btn btn-primary" value="Simpan"/>
                </div>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->