<div class="row">
	<div class="span12">      		  		
  		<div class="widget ">
  			<div class="widget-header">
  				<i class="icon-user"></i>
  				<h3>Daftar Jadwal Konflik Untuk MKDU - <?= $year ?></h3>
			</div> <!-- /widget-header -->
			
			<div class="widget-content">
                <a href="javascript:void(0);" onclick="history.go(-1)" class="btn btn-warning"><i class="icon-chevron-left"> </i> Back</a>
                <hr>
				<div class="span11">
                    <table id="example1" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Kode Matakuliah</th>
                                <th>Nama Matakuliah</th>
                                <th>Dosen</th>
                                <th>Ruang</th>
                                <th>Hari</th>
                                <th>Waktu Mulai</th>
                                <th>Waktu Selesai</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $no=1; foreach ($jadwals as $jadwal) { ?>
                                <tr>
                                    <td><?= $no; ?></td>
                                    <td><?= $jadwal->kd_matakuliah ?></td>
                                    <td><?= get_nama_mk($jadwal->kd_matakuliah, substr($jadwal->kd_jadwal,0,1)) ?></td>
                                    <td><?= nama_dsn($jadwal->kd_dosen) ?></td>
                                    <td><?= get_room($jadwal->kd_ruangan) ?></td>
                                    <td><?= notohari($jadwal->hari) ?></td>
                                    <td><?= $jadwal->waktu_mulai ?></td>
                                    <td><?= $jadwal->waktu_selesai ?></td>
                                </tr>
                            <?php $no++; } ?>
                        </tbody>
                    </table>
				</div>
			</div>
		</div>
	</div>
</div>