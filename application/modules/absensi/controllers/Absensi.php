<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Absensi extends MY_Controller
{

	public $userid;
	private $tableMateri = 'tbl_materi_abs';

	function __construct()
	{
		parent::__construct();
		error_reporting(0);
		if ($this->session->userdata('sess_login') == TRUE) {
			$cekakses = $this->role_model->cekakses(137)->result();
			if ($cekakses != TRUE) {
				echo "<script>alert('Anda Tidak Berhak Mengakses !!');history.go(-1);</script>";
			}
			$this->userid = $this->session->userdata('userid');
		} else {
			redirect('auth', 'refresh');
		}
	}


	function index()
	{
		$this->home();
	}

	/**
	 * Form Absensi
	 * @return HTML
	 */
	function form_absen()
	{

		$ses_kelas = $this->session->userdata('form-absen')['kelas'];
		$ses_kd_matkul = $this->session->userdata('form-absen')['kd_matkul'];


		$arrMatkul = explode("/", $this->input->post('matkul', TRUE));

		$kelas = empty($ses_kelas) ? $arrMatkul[1] : $ses_kelas;
		$kdMatkul = empty($ses_kd_matkul) ? $arrMatkul[0] : $ses_kd_matkul;

		// $user = $this->session->userdata('sess_jadwal');
		$user = $this->db->get_where('tbl_jadwal_matkul', ['kd_matakuliah' => $kdMatkul, 'kd_tahunajaran' => getactyear(), 'kd_dosen' => $this->userid, 'kelas' => $kelas])->row();

		$kadal = $this->role_model->cek_abs($user->kd_jadwal)->row();

		// Create Session
		$session['userid'] = $user->kd_dosen;
		$session['kd_jdl'] = $user->kd_jadwal;
		$session['kd_ta']  = $user->kd_tahunajaran;
		$session['kelas']  = $user->kelas;
		$session['kd_mk']  = $user->kd_matakuliah;
		$session['id_jdl'] = $user->id_jadwal;

		$this->session->set_userdata('sess_jadwal', $session);

		if ($kadal->open != 1) {
			echo "<script>alert('Jadwal Anda Belum Dapat Diakses. Harap Hubungi Prodi !');history.go(-1);</script>";
		} else {

			$data['kelas'] = $this->app_model->get_kelas_mahasiswa($user->id_jadwal);

			$tanggal = $this->db->query("SELECT distinct tanggal,max(pertemuan) AS pertemuan from tbl_absensi_mhs_new_20171 where kd_jadwal = ? order by id_absen desc limit 1", array($user->kd_jadwal))->row();

			$data['tang'] = $tanggal->pertemuan + 1;

			$this->db->select('a.nid,a.nama,b.kelas,c.nama_matakuliah')
				->from('tbl_karyawan a')
				->join('tbl_jadwal_matkul b', 'a.nid = b.kd_dosen')
				->join('tbl_matakuliah c', 'c.kd_matakuliah = b.kd_matakuliah')
				->where('b.kd_jadwal', $user->kd_jadwal);
			$data['nm'] = $this->db->get()->row();
			
			$data['max'] = $this->db->query("SELECT count(npm_mahasiswa) as mhs from tbl_krs where kd_jadwal = ? ", array($user->kelas))->row()->mhs;
			$data['id'] = $user->id_jadwal;

			$data['page'] = "v_abs_dsn";
			$this->load->view('template/template', $data);
		}
	}

	public function home()
	{
		$this->session->unset_userdata('sess_jadwal');
		$this->session->unset_userdata('form-absen');

		$list = $this->db->get_where('tbl_jadwal_matkul', ['kd_dosen' => $this->userid, 'kd_tahunajaran' => getactyear()])->result();

		$data['page'] = 'v_abs_home';
		$data['list'] = $list;
		$this->load->view('template/template', $data);
	}

	function login()
	{
		$this->load->view('v_login_jdl');
	}

	function verif_kode($kd_jadwal)
	{
		$user = $this->input->post('kd', TRUE);
		$id = $this->session->userdata('sess_login');
		//$pecah = explode('/', $user);

		$kd_jadwal = isset($user) ? $user : $kd_jadwal;

		$cek = $this->login_model->cek_kode($kd_jadwal, $this->userid)->result();

		if (count($cek) > 0) {

			foreach ($cek as $key) {
				//die('tolol');

				$session['userid'] = $key->kd_dosen;

				$session['kd_jdl'] = $key->kd_jadwal;

				$session['kd_ta'] = $key->kd_tahunajaran;

				$session['kelas'] = $key->kelas;

				$session['kd_mk'] = $key->kd_matakuliah;

				$session['id_jdl'] = $key->id_jadwal;

				$this->session->set_userdata('sess_jadwal', $session);
			}
		}
	}

	function list_abs()
	{
		$sesi = $this->session->userdata('sess_jadwal');

		$this->_set_session();

		$data['code']      = $sesi['kd_jdl'];
		$data['pertemuan'] = $this->db->query('SELECT DISTINCT pertemuan,tanggal FROM tbl_absensi_mhs_new_20171 WHERE kd_jadwal = ? GROUP BY pertemuan,tanggal', array($sesi['kd_jdl']))->result();
		$data['id_jadwal'] = $sesi['id_jdl'];
		$data['page']      = "v_list_abs";
		$data['kode_mk']   = $sesi['kd_mk'];
		$data['kelas']     = $sesi['kelas'];

		$this->load->view('template/template', $data);
	}

	function out()
	{
		$this->session->unset_userdata('sess_jadwal');

		redirect(base_url(), 'refresh');
	}

	/**
	 * Store data absen to database
	 * @param  $id 			[id_jadwal dari tbl_jadwal_matkul]
	 * @return Object     	[message]
	 */
	function kelas_absen($id)
	{
		
		date_default_timezone_set('Asia/Jakarta');
		$this->_set_session();

		$jumlahabsen = $this->input->post('jumlah', TRUE);
		$kode = $this->app_model->getdetail('tbl_jadwal_matkul', 'id_jadwal', $id, 'id_jadwal', 'asc')->row();
		$cek = $this->db->query("SELECT count(distinct tanggal) as jml from tbl_absensi_mhs_new_20171 where kd_jadwal = ? ", array($kode->kd_jadwal))->row()->jml;
		$tanggal = $this->db->query("SELECT distinct tanggal,max(pertemuan) AS pertemuan from tbl_absensi_mhs_new_20171 where kd_jadwal = ? order by id_absen desc limit 1", array($kode->kd_jadwal))->row();

		// cek sks mk
		$prodi = substr($kode->kd_jadwal, 0, 5);
		$sks = $this->_totalSKS($kode->kd_matakuliah, $prodi);
		
		$fixtime = $this->_timeSKS($sks);

		// cek jumlah tanggal absen berdasarkan kd_jadwal
		if ($cek > 0) {
			$pertemuan = $tanggal->pertemuan +  1;
		}else{
			$pertemuan = 1;
		}

		if ($tanggal->tanggal == $this->input->post('tgl', TRUE)) {
			echo "<script>alert('Absensi Sudah Dilakukan Pada Tanggal Tersebut');document.location.href='" . base_url() . "absensi/form-absen/';</script>";
		} else {
			$a = 1;
			for ($i = 0; $i < $jumlahabsen - 1; $i++) {
				$hasil = $this->input->post('absen' . $a . '[0]', TRUE);
				$pecah = explode('-', $hasil);

				$datax[] = array(
					'npm_mahasiswa' => trim($pecah[1]),
					'kd_jadwal' => trim($kode->kd_jadwal),
					'tanggal' => date('Y-m-d', strtotime($this->input->post('tgl', TRUE))),
					'pertemuan' => $pertemuan,
					'kehadiran' => $pecah[0]
				);

				if (($tanggal->pertemuan + 1) > 16) {
					echo "<script>alert('Absen Melebihi Jumlah 16 Pertemuan');
					document.location.href='" . base_url() . "absensi/list-abs/';</script>";
					exit();
				}
				$a++;
			}

			$this->db->insert_batch('tbl_absensi_mhs_new_20171', $datax);

			$dataInsertMateri = array(
				'kd_jadwal'  => trim($kode->kd_jadwal),
				'pertemuan'  => $pertemuan,
				'tgl'        => date('Y-m-d', strtotime($this->input->post('tgl', TRUE))),
				'materi'     => $this->input->post('bahas', TRUE),
				'jam_masuk'  => date('H:i:s'),
				'jam_keluar' => $fixtime,
				'created_by' => trim($this->userid),
				'created_at' => date('Y-m-d H:i:s'),
			);
			
			$this->db->insert($this->tableMateri, $dataInsertMateri);

			echo "<script>alert('Sukses');
			document.location.href='" . base_url() . "absensi/list-abs/';</script>";
		}
	}

	/**
	 * Modal
	 * Untuk melakukan ubah absensi
	 * @param  $id        [id_jadwal dari tbl_jadwal_matkul]
	 * @param  $pertemuan 
	 * @return HTML       
	 */
	function modal_edit_absen($id, $pertemuan)
	{
		clearstatcache();
		$actyear = getactyear();

		$q = $this->db->query('select * from tbl_jadwal_matkul where id_jadwal = ?', array($id))->row();

		$data['pertemuan'] = $pertemuan;
		$data['kode_jadwal'] = $q->kd_jadwal;

		$bahasan = $this->db->get_where($this->tableMateri, ['pertemuan' => $pertemuan, 'kd_jadwal' => $q->kd_jadwal, 'created_by' => $this->userid])->row();

		$data['bahasan'] = $bahasan;

		$data['tanggal'] = $this->db->query('SELECT DISTINCT tanggal FROM tbl_absensi_mhs_new_20171 
											WHERE pertemuan = ? 
											AND kd_jadwal = ?', array($pertemuan,$q->kd_jadwal))->row()->tanggal;

		if ($q->gabung > 0) {
			$data['kelas'] 	= $this->db->query("SELECT
												    distinct b.`npm_mahasiswa`,
												    mhs.`NMMHSMSMHS`,
												    b.`kd_jadwal`,
												    (
												        SELECT
												            kehadiran
												        FROM
												            tbl_absensi_mhs_new_20171
												        WHERE
												           kd_jadwal IN ( select kd_jadwal from tbl_jadwal_matkul where referensi = '$q->referensi' )
												            AND pertemuan = '$pertemuan'
												            AND npm_mahasiswa = b.`npm_mahasiswa`
												    ) as kehadiran
												FROM
												    tbl_krs b
												    JOIN tbl_verifikasi_krs v ON b.kd_krs = v.kd_krs
												    JOIN tbl_mahasiswa mhs ON b.`npm_mahasiswa` = mhs.`NIMHSMSMHS`
												WHERE
												v.status_verifikasi = 1
												AND
												    b.kd_jadwal IN (
												        SELECT
												            kd_jadwal
												        FROM
												            tbl_absensi_mhs_new_20171
												        where
												             kd_jadwal IN ( select kd_jadwal from tbl_jadwal_matkul where referensi = '$q->referensi' )
												    )
												ORDER BY
												    mhs.NIMHSMSMHS ASC ")->result();
		} else {
			$data['kelas'] 	= $this->db->query('SELECT distinct 
												b.`npm_mahasiswa`,
												mhs.`NMMHSMSMHS`,
												b.`kd_jadwal`, 
												(SELECT kehadiran FROM tbl_absensi_mhs_new_20171 
												WHERE kd_jadwal = "' . $q->kd_jadwal . '" 
												AND pertemuan = ' . $pertemuan . ' 
												AND npm_mahasiswa = b.`npm_mahasiswa`) as kehadiran
											FROM tbl_krs b
											JOIN tbl_verifikasi_krs v ON b.kd_krs = v.kd_krs
											JOIN tbl_mahasiswa mhs ON b.`npm_mahasiswa` = mhs.`NIMHSMSMHS`
											WHERE v.status_verifikasi = 1 AND b.kd_jadwal = "' . $q->kd_jadwal . '"
											ORDER BY mhs.NIMHSMSMHS ASC')->result();
		}

		$data['h'] = $this->db->query("SELECT count(pertemuan) as jums from tbl_absensi_mhs_new_20171 where kd_jadwal = '" . $q->kd_jadwal . "' AND pertemuan = '" . $pertemuan . "' and kehadiran = 'H'")->row();
		$data['a'] = $this->db->query("SELECT count(pertemuan) as jums from tbl_absensi_mhs_new_20171 where kd_jadwal = '" . $q->kd_jadwal . "' AND pertemuan = '" . $pertemuan . "' and kehadiran = 'A'")->row();
		$data['i'] = $this->db->query("SELECT count(pertemuan) as jums from tbl_absensi_mhs_new_20171 where kd_jadwal = '" . $q->kd_jadwal . "' AND pertemuan = '" . $pertemuan . "' and kehadiran = 'I'")->row();
		$data['s'] = $this->db->query("SELECT count(pertemuan) as jums from tbl_absensi_mhs_new_20171 where kd_jadwal = '" . $q->kd_jadwal . "' AND pertemuan = '" . $pertemuan . "' and kehadiran = 'S'")->row();


		$data['id'] = $id;

		$this->load->view('v_list_edit', $data);
	}

	/**
	 * Update data absen to database
	 * @param  string $id [description]
	 * @return [type]     [description]
	 */
	function edit_kelas_absen($id = '')
	{
		$this->load->model('akademik/absen_model', 'absenmod');
		$jumlahabsen = $this->input->post('jumlah', TRUE);
		$pertemuan   = $this->input->post('pertemuan', TRUE);
		$kd_jadwal  = $this->input->post('id_jadwal', TRUE);
		$tanggal    = $this->input->post('tanggal', TRUE);
		$tanggal_sebelumnya    = $this->input->post('tanggal_sebelumnya', TRUE);

		$kode_jadwal = array_unique($this->input->post('kd_jadwal', TRUE), SORT_REGULAR);
		$kode = $this->db->query("SELECT kd_jadwal,kd_matakuliah FROM tbl_jadwal_matkul WHERE id_jadwal = ?", array($id))->row();
		
		$this->db->where_in('kd_jadwal', $kode_jadwal);
		$this->db->where('pertemuan', $pertemuan);
		$res = $this->db->delete('tbl_absensi_mhs_new_20171');

		if ($res) {
			$a = 1;
			for ($i = 0; $i < $jumlahabsen; $i++) {
				$hasil = $this->input->post('absen' . $a . '[0]', TRUE);

				$pecah = explode('-', $hasil);

				$data[] = [
					'sblm_update' => $tanggal_sebelumnya,
					'pertemuan' => $pertemuan,
					'kehadiran' => $pecah[0],
					'npm_mahasiswa' => trim($pecah[1]),
					'kd_jadwal' => trim($pecah[2]),
					'tanggal' => date('Y-m-d', strtotime($tanggal)),
					'user_update' => trim($this->userid),
				];

				$a++;
			}

			$this->db->insert_batch('tbl_absensi_mhs_new_20171', $data);

			if (array_key_exists('materi', $_POST)) {
				
				$data = [
					'materi' => $_POST['materi'],
					'tgl' => date('Y-m-d',strtotime($tanggal)),
					'created_at' => date('Y-m-d H:i:s'),
				];

				$condition = [
					'kd_jadwal' => trim($kd_jadwal),
					'pertemuan' => $pertemuan,
					'created_by' => trim($this->userid),
				];

				$exist = $this->absenmod->isExist($this->tableMateri, $condition);

				if ($exist) {
					$this->db->update($this->tableMateri, $data, $condition);
				}else{
					$prodi = substr($kode->kd_jadwal, 0, 5);
					$sks = $this->_totalSKS($kode->kd_matakuliah, $prodi);
					$fixtime = $this->_timeSKS($sks);
					$dataInsert = array_merge($data, $condition, ['jam_masuk'  => date('H:i:s'), 'jam_keluar' => $fixtime]);
					
					$this->db->insert($this->tableMateri, $dataInsert);
				}

			}

			echo "<script>alert('Sukses');
				document.location.href='" . base_url() . "absensi/form-absen';</script>";
		} else {
			echo "<script>alert('Gagal Edit Absen');
				document.location.href='" . base_url() . "absensi/form-absen';</script>";
		}
	}

	function modal_detail_absen($id, $pertemuan)
	{
		clearstatcache();
		$q = $this->db->query('select * from tbl_jadwal_matkul where id_jadwal = ' . $id . '')->row();

		$data['pertemuan'] = $pertemuan;
		$data['kode_jadwal'] = $q->kd_jadwal;
		$data['tanggal'] = $this->db->query('SELECT DISTINCT tanggal FROM tbl_absensi_mhs_new_20171 WHERE pertemuan = ? AND kd_jadwal = ?', array($pertemuan, $q->kd_jadwal))->row()->tanggal;
		
		if ($q->gabung > 0) {
			$data['kelas'] 	= $this->db->query("SELECT
												    distinct b.`npm_mahasiswa`,
												    mhs.`NMMHSMSMHS`,
												    b.`kd_jadwal`,
												    (
												        SELECT
												            kehadiran
												        FROM
												            tbl_absensi_mhs_new_20171
												        WHERE
												           kd_jadwal IN ( select kd_jadwal from tbl_jadwal_matkul where referensi = '$q->referensi' )
												            AND pertemuan = '$pertemuan'
												            AND npm_mahasiswa = b.`npm_mahasiswa`
												    ) as kehadiran
												FROM
												    tbl_krs b
												    JOIN tbl_verifikasi_krs v ON b.kd_krs = v.kd_krs
												    JOIN tbl_mahasiswa mhs ON b.`npm_mahasiswa` = mhs.`NIMHSMSMHS`
												WHERE
												v.status_verifikasi = 1
												AND
												    b.kd_jadwal IN (
												        SELECT
												            kd_jadwal
												        FROM
												            tbl_absensi_mhs_new_20171
												        where
												             kd_jadwal IN ( select kd_jadwal from tbl_jadwal_matkul where referensi = '$q->referensi' )
												    )
												ORDER BY
												    mhs.NIMHSMSMHS ASC ")->result();
		} else {
			$data['kelas'] 	= $this->db->query('SELECT distinct 
												b.`npm_mahasiswa`,
												mhs.`NMMHSMSMHS`,
												b.`kd_jadwal`, 
												(SELECT kehadiran FROM tbl_absensi_mhs_new_20171 
												WHERE kd_jadwal = "' . $q->kd_jadwal . '" 
												AND pertemuan = ' . $pertemuan . ' 
												AND npm_mahasiswa = b.`npm_mahasiswa`) as kehadiran
											FROM tbl_krs b
											JOIN tbl_verifikasi_krs v ON b.kd_krs = v.kd_krs
											JOIN tbl_mahasiswa mhs ON b.`npm_mahasiswa` = mhs.`NIMHSMSMHS`
											WHERE v.status_verifikasi = 1 AND b.kd_jadwal = "' . $q->kd_jadwal . '"
											ORDER BY mhs.NIMHSMSMHS ASC')->result();
		}

		$data['h'] = $this->db->query("SELECT count(pertemuan) as jums from tbl_absensi_mhs_new_20171 where kd_jadwal = '" . $q . "' AND pertemuan = '" . $pertemuan . "' and kehadiran = 'H'")->row();
		$data['a'] = $this->db->query("SELECT count(pertemuan) as jums from tbl_absensi_mhs_new_20171 where kd_jadwal = '" . $q . "' AND pertemuan = '" . $pertemuan . "' and kehadiran = 'A'")->row();
		$data['i'] = $this->db->query("SELECT count(pertemuan) as jums from tbl_absensi_mhs_new_20171 where kd_jadwal = '" . $q . "' AND pertemuan = '" . $pertemuan . "' and kehadiran = 'I'")->row();
		$data['s'] = $this->db->query("SELECT count(pertemuan) as jums from tbl_absensi_mhs_new_20171 where kd_jadwal = '" . $q . "' AND pertemuan = '" . $pertemuan . "' and kehadiran = 'S'")->row();
		$data['id'] = $id;
		$this->load->view('v_list', $data);
	}

	function cetak()
	{
		// var_dump($id);exit();
		$sesi = $this->session->userdata('sess_jadwal');
		$data['code'] = $sesi['kd_jdl'];
		$data['look'] = $this->db->query("SELECT a.nama_matakuliah,b.kelas,a.kd_matakuliah from tbl_matakuliah a join tbl_jadwal_matkul b on a.kd_matakuliah = b.kd_matakuliah where b.kd_jadwal = '" . $sesi['kd_jdl'] . "'")->row();
		$data['pertemuan'] = $this->db->query('SELECT DISTINCT pertemuan,tanggal FROM tbl_absensi_mhs_new_20171 WHERE kd_jadwal = "' . $sesi['kd_jdl'] . '" GROUP BY pertemuan,tanggal')->result();

		$this->load->view('excel_print', $data);
	}

	public function _set_session()
	{
		$sesi = $this->session->userdata('sess_jadwal');

		$array = array(
			'form-absen' => [
				'kelas' => $sesi['kelas'],
				'kd_matkul' => $sesi['kd_mk'],
			],
		);

		$this->session->set_userdata($array);
	}

	/**
	 * Update anomali
	 * Untuk cleansing data yang nilai tanggalnya = 000-00-00 di table tbl_mater_abs dan tbl_absensi_mhs_new_20171
	 * 
	 * @return [type]        [description]
	 */
	public function update_anomali()
	{
		$sql = 'select * from tbl_materi_abs where tgl = "0000-00-00"  ORDER by created_at ASC;';
		$data = $this->db->query($sql)->result();

		foreach ($data as $key => $value) {
			$dataAbsen = $this->db->query("select * from tbl_absensi_mhs_new_20171 where kd_jadwal = '$value->kd_jadwal' and pertemuan = $value->pertemuan");

			if ($dataAbsen->num_rows() > 0) {
				$absen = $dataAbsen->result();

				if (strtotime($value->tgl) == strtotime($absen[0]->tanggal)) {

					$pertemuan = $value->pertemuan - 1;
					$dateBefore = $this->db->query("select tanggal from tbl_absensi_mhs_new_20171 where kd_jadwal = '$value->kd_jadwal' and pertemuan = $pertemuan")->row();
					$date = new DateTime($dateBefore->tanggal);
					$date->modify('+7 day');
					$tanggal = $date->format('Y-m-d');
					
					$dataUpdateAbsen = [
						'tanggal' => $tanggal
					];

					$this->db->update('tbl_absensi_mhs_new_20171', $dataUpdateAbsen, ['kd_jadwal' => $value->kd_jadwal, 'pertemuan' => $value->pertemuan]);
					
					$this->db->update($this->tableMateri, ['tgl' => $tanggal, 'created_at' => date('Y-m-d')], ['id' => $value->id]);
				}elseif (!empty($absen[0]->tanggal)) {
					$this->db->update($this->tableMateri, ['tgl' => $absen[0]->tanggal, 'created_at' => date('Y-m-d')], ['id' => $value->id]);
				}elseif ($value->pertemuan > $absen[0]->pertemuan) {
					$this->db->delete($this->tableMateri, ['id' => $value->id]);
				}
			}else{
				$this->db->delete($this->tableMateri, ['id' => $value->id]);
			}
		}
	}

	/**
	 * Menghitung Total SKS
	 * Berdasarkan Kode Matakuliah Dan Prodi
	 * @param  string $kd_matakuliah 
	 * @param  string $prodi         
	 * @return string $sks 				[total sks]                
	 */
	private function _totalSKS($kd_matakuliah= '', $prodi = '')
	{
		// cek sks mk
		$sks = $this->db->query("SELECT sks_matakuliah from tbl_matakuliah where kd_matakuliah = ? and kd_prodi = ?", array($kd_matakuliah, $prodi))->row()->sks_matakuliah;
		return $sks;
	}

	private function _timeSKS($sks){
		$timekuy = date('H:i:s');

		$date = new DateTime($timekuy);

		if ($sks == '2') {
			$date->add(new DateInterval('PT90M'));
		} else {
			$date->add(new DateInterval('PT135M')); // 3 SKS
		}

		$fixtime = $date->format('H:i:s');

		return $fixtime;
	}
}

/* End of file Absen.php */
/* Location: ./application/modules/absensi/controllers/Absen.php */
