

<link href="https://gitcdn.github.io/bootstrap-toggle/2.2.0/css/bootstrap-toggle.min.css" rel="stylesheet">

<script src="https://gitcdn.github.io/bootstrap-toggle/2.2.0/js/bootstrap-toggle.min.js"></script>

<script>
function edit(id,id2){
    $('.modal-content').empty();
    $('#absenModal').removeData('bs.modal');
    $('#edit-absen').load('<?php echo base_url();?>absensi/modal_edit_absen/'+id+'/'+id2);
}
function detail(id,id2){
    $('.modal-content').empty();
    $('#myModal').removeData('bs.modal');
    $('#absen').load('<?php echo base_url();?>absensi/modal_detail_absen/'+id+'/'+id2);
}
</script>

<div class="row">

    <div class="span12">                    

        <div class="widget ">

            <div class="widget-header">

                <i class="icon-user"></i>

                <h3>Daftar Data Absensi <?php echo $kode_mk; ?> / Kelas <?php echo $kelas; ?></h3>

            </div> <!-- /widget-header -->

            

            <div class="widget-content">

                <div class="span11">
                    <a href="<?php echo site_url('absensi/form-absen') ?>"  class="btn btn-primary"><i class="icon-chevron-left"></i> Kembali</a>
                    <a href="<?php echo base_url(); ?>absensi/cetak/" class="btn btn-success"><i class="icon-print"></i> Print Excel</a>
                    <hr>
                   <!-- <a href="<?php //echo base_url();?>akademik/ajar/cetak/" class="btn btn-success "><i class="btn-icon-only icon-print"> </i> Export Excel</a><hr> -->

                    <table id="example1" class="table table-bordered table-striped">

                        <thead>

                            <tr> 

                                <th>No</th>
                                <th>Pertemuan</th>
                                <th>Bahasan</th>
                                <th>Tanggal</th>
                                <th width="100">Aksi</th>

                            </tr>

                        </thead>

                        <tbody>

                            <?php $no=1; foreach ($pertemuan as $isi): ?>

                                <tr>

                                    <td><?php echo number_format($no); ?></td>

                                    <td><?php echo "Pertemuan Ke-".$isi->pertemuan; ?></td>
                                    <?php $mat = $this->db->query("SELECT materi from tbl_materi_abs where pertemuan = '".$isi->pertemuan."' and kd_jadwal = '".$code."'")->row();?>
                                    <td>
                                        <?php echo empty($mat->materi) ? "" : $mat->materi; ?>
                                    </td>

                                    <td><?php echo $isi->tanggal; ?></td>

                                    <td class="td-actions">
                                        <a data-toggle="modal" onclick='detail(<?php echo "".$id_jadwal.",".$isi->pertemuan."";?>)' href="#myModal" class="btn btn-success btn-small" data-toggle="tooltip" title="lihat rincian absensi"><i class="btn-icon-only icon-eye-open"> </i></a>
                                        <a data-toggle="modal" onclick='edit(<?php echo "".$id_jadwal.",".$isi->pertemuan."";?>)' href="#absenModal" class="btn btn-warning btn-small" data-toggle="tooltip" title="edit absensi"><i class="btn-icon-only icon-edit"> </i></a>
                                        <!-- <a onclick="return confirm('Are you sure you want to delete this item?');" href="<?php echo base_url();?>akademik/ajar/hapus_pertemuan/<?php echo $id_jadwal.'/'.$isi->pertemuan;?>" class="btn btn-default btn-small">Hapus <i class="btn-icon-only icon-trash"> </i></a> -->
                                    </td>

                                </tr>    

                            <?php $no++; endforeach ?>

                        </tbody>

                    </table>

                </div>

            </div>

        </div>

    </div>

</div>


<!-- Modal Detail Absen -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content" id="absen">
        </div><!-- /.modal-content -->
    </div>
</div>

<!-- Modal Edit Absen  -->
<div class="modal fade" id="absenModal" tabindex="-1" role="dialog" aria-labelledby="absenModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content" id="edit-absen">        
        </div>
    </div>
</div>