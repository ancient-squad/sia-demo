<div class="row">
  <div class="span12">
    <div class="widget ">
      <div class="widget-header">
        <i class="icon-user"></i>
        <h3>Kegiatan Perkuliahan</h3>
      </div>
      <!-- /widget-header -->      
      <div class="widget-content">
        <div class="span11">
          <form method="post" class="form-horizontal" action="<?php echo site_url('absensi/form-absen') ?>" id="form-abs">
            <fieldset>
              <div class="control-group">
                <label class="control-label">Mata Kuliah</label>
                <div class="controls">
                  <select name="matkul" class="form-control" style="width: auto;">
                    <option selected disabled value="">-- Pilih Mata Kuliah / (Kelas) --</option>
                    <?php foreach ($list as $key => $matkul): ?>
                      <?php if ($matkul->gabung > 0) {
                        $prodi = substr($matkul->kd_jadwal, 0, 1);
                      }else{
                        $prodi = substr($matkul->kd_jadwal, 0, 5);
                      } ?>
                      <option value="<?php echo $matkul->kd_matakuliah ."/". $matkul->kelas ?>"><?php echo get_nama_mk($matkul->kd_matakuliah, $prodi) ." / ". $matkul->kelas ?></option>
                    <?php endforeach ?>
                  </select>
                </div>
              </div>
              <div class="form-actions">
                <input type="submit" class="btn btn-large btn-success" id="find" value="Cari"/> 
              </div>
              <!-- /form-actions -->
            </fieldset>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
  $("#find").on("click", function () {
    $("#form-abs").submit();
  })
</script>