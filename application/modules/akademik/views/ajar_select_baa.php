<div class="row">
  <div class="span12">                
    <div class="widget ">
      <div class="widget-header">
        <i class="icon-user"></i>
        <h3>Kegiatan Perkuliahan</h3>
      </div>     

      <div class="widget-content">
        <div class="span11">
          <form method="post" class="form-horizontal" action="<?= base_url(); ?>akademik/ajar/save_session_baa">
            <fieldset>
              <div class="control-group">
                <label class="control-label">Prodi</label>
                <div class="controls">
                  <select class="form-control span6" name="jurusan" id="prodi">
                    <option>-- Pilih Prodi --</option>
                    <?php foreach ($prodi as $rows) { ?>
                    <option value="<?= $rows->kd_prodi;?>"><?= $rows->prodi;?></option>
                    <?php } ?>
                  </select>
                </div>
              </div> 
              <div class="control-group">
                <label class="control-label">Tahun Akademik</label>
                <div class="controls">
                  <select class="form-control span6" name="tahunajaran" id="tahunajaran">
                    <option>--Pilih Tahun Akademik--</option>
                    <?php foreach ($tahunajar as $row) { ?>
                    <option value="<?= $row->kode;?>"><?= $row->tahun_akademik;?></option>
                    <?php } ?>
                  </select>
                </div>
              </div> 
              <div class="form-actions">
                <input type="submit" class="btn btn-large btn-success" value="Cari"/> 
              </div>
            </fieldset>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>

