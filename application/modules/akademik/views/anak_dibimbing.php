<div class="row">
	<div class="span12">      		  		
  		<div class="widget ">
  			<div class="widget-header">
  				<i class="icon-user"></i>
  				<h3>Data Mahasiswa Bimbingan</h3>
			</div> <!-- /widget-header -->
			
			<div class="widget-content">
				<div class="span11">
					<table id="example1" class="table table-bordered table-striped">
	                	<thead>
	                        <tr> 
	                        	<th>No</th>
	                        	<th>NIM</th>
	                        	<th>NAMA</th>
	                        	<th>ANGKATAN</th>
	                            <th width="120">Aksi</th>
	                        </tr>
	                    </thead>
	                    <tbody>
                            <?php $no = 1; foreach ($getData as $row) { ?>
	                        <tr>
	                        	<td><?php echo number_format($no); ?></td>
                                <td><?php echo $row->NIMHSMSMHS; ?></td>
                                <td><?php echo $row->NMMHSMSMHS; ?></td>
                                <td><?php echo $row->TAHUNMSMHS; ?></td>
	                        	<td class="td-actions">
									<a class="btn btn-success btn-small" href="<?php echo base_url(); ?>akademik/bimbingan/viewkrsmhs/<?php echo $row->NIMHSMSMHS; ?>"><i class="btn-icon-only icon-ok"> </i></a>
								</td>
	                        </tr>
	                        <?php $no++; } ?>
	                    </tbody>
	               	</table>
				</div>
			</div>
		</div>
	</div>
</div>