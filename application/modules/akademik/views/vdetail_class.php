<div class="row">
    <div class="span12">                    
        <div class="widget ">
            <div class="widget-header">
                <i class="icon-user"></i>
                <h3>Detail Mahasiswa Aktif Kelas <?= classType($clss) ?> Prodi <?= get_jur($prod) ?></h3>
            </div> <!-- /widget-header -->           

            <div class="widget-content">
                <div class="span11">
                    <a href="<?= base_url('akademik/amount_stdn/export_dtl/'.$prod.'/'.$clss) ?>" class="btn btn-success">Export Excel</a>
                    <hr>
                    <table id="example1" class="table table-bordered table-striped">
                        <thead>
                            <tr> 
                                <th>No</th>
                                <th>NPM</th>
                                <th>Nama</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $no=1; foreach ($data as $isi): ?>
                                <tr>
                                    <td><?= $no; ?></td>
                                    <td><?= $isi->npm_mahasiswa; ?></td>
                                    <td><?= get_nm_mhs($isi->npm_mahasiswa) ?></td>
                                </tr>    
                            <?php $no++; endforeach ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>