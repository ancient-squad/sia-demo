<script type="text/javascript">
$(function () {
    $('#beban2').highcharts({
        chart: {
            type: 'column'
        },
        title: {
            text: 'Beban Mengajar Dosen Tetap'
        },
        subtitle: {
            text: 'Source: SIA <?= $this->ORG_NAME ?>'
        },
        xAxis: {
            categories: [
                <?php 
                        foreach ($beban as $row) {
                            $a = substr($row->nama, 0,10);
                            echo '"'.$a.'",';
                            
                        }
                    ?>
                // 'Jan',
                // 'Dec'
            ],
            crosshair: true
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Total'
            }
        },
        tooltip: {
            headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                '<td style="padding:0"><b>{point.y:f}</b></td></tr>',
            footerFormat: '</table>',
            shared: true,
            useHTML: true
        },
        plotOptions: {
            column: {
                pointPadding: 0.2,
                borderWidth: 0
            }
        },
        series: [{
            name: 'Jumlah SKS',
            data: [<?php
                        foreach ($beban as $row) {
                            echo $row->jumlah.",";
                        }
                    ?>
                ]

        }]
    });
    
});
</script>
<div id='beban2' style='min-width: 300px; height: 400px; max-width: 1500px; margin: 0 auto'></div>