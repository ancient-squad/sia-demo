<script type="text/javascript">
    $(function() {
        $("#example4").dataTable({
            "bPaginate": false
        });
    });
</script>

<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
    <h4 class="modal-title">Daftar Hadir Mahasiswa</h4>
</div>
<form class='form-horizontal' action="<?php echo base_url(); ?>akademik/ajar/kelas_absen/<?php echo $id; ?>" method="post">
    <div class="modal-body">
        <div class="control-group" id="">
            <script>
                $(function() {
                    $("#tgl").datepicker({
                        changeMonth: true,
                        changeYear: true,
                        dateFormat: "yy-mm-dd",
                        minDate: -200,
                        maxDate: 0,
                    });
                    $("#tgl").keydown(function(event) {
                        event.preventDefault();
                    });
                });
            </script>
            <script type="text/javascript">
                $(document).ready(function() {
                    $("#RemoveAll").click(function() {
                        if ($(this).is(':checked'))
                            $(".idabsen1").attr("checked", "checked");
                        else
                            $(".idabsen1").removeAttr("checked");
                    });
                });
            </script>
            <fieldset>
                <div class="control-group" style="margin-left: -20px;">
                    <label class="control-label">Tanggal Pertemuan</label>
                    <div class="controls">
                        <input class="span2" type="text" name="tgl" id="tgl" required />
                    </div>
                    <label class="control-label">Pertemuan Ke </label>
                    <div class="controls" style="padding-top: 5px;">
                        <select name="pertemuanke" id="pertemuanke" required>
                            <option value="" selected disabled>-- Pilih Pertemuan --</option>
                            <?php foreach ($pertemuan_tersedia as $key => $value) : ?>
                                <option value="<?php echo $value ?>"><?php echo $value ?></option>
                            <?php endforeach ?>
                        </select>
                    </div>
                    <hr>
                    <!-- <label class="control-label">Pembahasan/Materi</label>
                    <div class="controls">
                        <textarea class="span2" name="pembahasan" required></textarea>
                    </div> -->
                    <label class="control-label">Hadir Semua</label>
                    <div class="controls">
                        <input type="checkbox" id="RemoveAll" name="absenall" />
                    </div>
                </div>
            </fieldset>
            <table id="example2" class="table table-bordered table-striped">
                <thead>
                    <tr>
                        <th>No</th>
                        <th width="90">NIM</th>
                        <th>NAMA</th>
                        <th width="20">HADIR</th>
                        <th width="20">SAKIT</th>
                        <th width="20">IZIN</th>
                        <th width="20">ALPA</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $no = 1;
                    foreach ($kelas as $value) { ?>
                        <!--input type="hidden" name="jadwal" value="<?php //echo $value->kd_jadwal; 
                                                                        ?>"/-->
                        <tr>
                            <td><?php echo number_format($no); ?></td>
                            <td><?php echo $value->NIMHSMSMHS; ?></td>
                            <td><?php echo $value->NMMHSMSMHS; ?></td>
                            <td>
                                <input type="radio" class="idabsen1" name="absen<?php echo $no; ?>[]" value="H-<?php echo $value->NIMHSMSMHS . '-' . $value->kd_jadwal; ?>" required />
                            </td>
                            <td>
                                <input type="radio" name="absen<?php echo $no; ?>[]" value="S-<?php echo $value->NIMHSMSMHS . '-' . $value->kd_jadwal; ?>" />
                            </td>
                            <td>
                                <input type="radio" name="absen<?php echo $no; ?>[]" value="I-<?php echo $value->NIMHSMSMHS . '-' . $value->kd_jadwal; ?>" />
                            </td>
                            <td>
                                <input type="radio" name="absen<?php echo $no; ?>[]" value="A-<?php echo $value->NIMHSMSMHS . '-' . $value->kd_jadwal; ?>" />
                            </td>
                        </tr>
                    <?php $no++;
                    } ?>
                </tbody>
            </table>
            <input type="hidden" name="jumlah" value="<?php echo $no; ?>" />

        </div>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Keluar</button>
        <input type="submit" class="btn btn-primary" value="Simpan" />
    </div>