<script src='https://www.google.com/recaptcha/api.js'></script>
<script type="text/javascript">
  jQuery(document).ready(function($) {
    $('input[name^=dosen]').autocomplete({
      source: '<?php echo base_url('akademik/krs_mhs/load_dosen_autocomplete');?>',
      minLength: 1,
      select: function (evt, ui) {
        this.form.dosen.value = ui.item.value;
        this.form.kd_dosen.value = ui.item.nid;
      }
    });
  });
</script>

<?php $sesi = $this->session->userdata('sess_login');?>

<div class="row">
  <div class="span12">                
      <div class="widget ">
        <div class="widget-header">
          <i class="icon-file"></i>
          <h3>Form KRS</h3>
      </div> <!-- /widget-header -->
      <?php $logged = $this->session->userdata('sess_login'); ?>
      <div class="widget-content">
        <div class="span11">
          <form class="form-horizontal" method="post" action="<?= base_url('akademik/krs_mhs/create_session'); ?>">
            <fieldset>
              <div class="control-group">                     
                <label class="control-label">Penasehat Akademik Anda</label>
                <div class="controls">
                  <input 
                    type="text" 
                    class="span3" 
                    name="dosen" 
                    placeholder="Ketikan Nama Dosen" 
                    value="<?= get_nm_pa($mhs->kd_dosen)?>" 
                    id="dosen" 
                    readonly="">
                  <input type="hidden" id='kd_dosen' name="kd_dosen" value="<?= $mhs->kd_dosen; ?>">
                </div> <!-- /controls -->       
              </div> <!-- /control-group -->

              <div class="control-group">                     
                <label class="control-label">Kelompok Kelas</label>
                <div class="controls">
                  <input type="hidden" name="kelas" value="<?= $mhs->kelas_mhs ?>">
                  <select class="span3" disabled="">
                    <option disabled="" value="">-- Pilih kelompok Kelas --</option>
                    <option value="PG" <?= $mhs->kelas_mhs == 'PG' ? 'selected=""' : NULL; ?> >A</option>
                    <option value="SR" <?= $mhs->kelas_mhs == 'SR' ? 'selected=""' : NULL; ?> >B</option>
                    <option value="PK" <?= $mhs->kelas_mhs == 'PK' ? 'selected=""' : NULL; ?> >C</option>
                  </select>
                  <p class="help-block">
                    <i>*Bila terdapat ketidaksesuaian pembimbing akademik/kelompok kelas mohon hubungi program studi</i>
                  </p>
                </div> <!-- /controls -->       
              </div> <!-- /control-group -->
              <input type="hidden" name="mhs" value="<?= $sesi['userid']; ?>" placeholder="">
              <div class="form-actions">
                <input type="submit" class="btn btn-primary" id="save" value="Submit"/> 
                <input type="reset" class="btn btn-warning" value="Reset"/>
              </div> <!-- /form-actions -->
            </fieldset>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>