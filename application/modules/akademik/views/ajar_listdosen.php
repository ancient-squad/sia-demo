<script>
    function edit(id) {
        var URL = '<?= base_url() ?>akademik/ajar/check_pertemuan/' + id;
        $.post(URL, function(data) {
            var res = JSON.parse(data)
            if (res.success) {
                $("#myModal").modal("toggle")
                $('#absen').load('<?= base_url(); ?>akademik/ajar/view_absen/' + id);
            } else {
                alert(res.message)
            }
        });
    }

    function loadMeeting(idx) {
        $('#jml').load('<?= base_url('akademik/ajar/loadMeetAmount/') ?>' + idx);
    }

    function loadCourse(idx,nid) {
        $("#detail").load('<?= site_url('akademik/ajar/detail-course/');?>' + idx + "/" + nid)
    }
</script>

<div class="row">
    <div class="span12">
        <div class="widget ">
            <div class="widget-header">
                <a 
                    href="<?= $back_url ?>" 
                    class="btn btn-default"
                    data-toggle="tooltip"
                    title="kembali"
                    style="margin-left: 10px">
                    <i class="icon-chevron-left" style="margin-left: 0"></i>
                </a>
                <h3>Jadwal Mengajar Dosen <?= (in_array(10, $group)) ? get_jur($prodi) : '' ?> Tahun Akademik <?= get_thajar($ta) ?></h3>
            </div> <!-- /widget-header -->

            <div class="widget-content">
                <div class="span11">
                    <table id="example1" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Kode MK</th>
                                <th>Mata Kuliah</th>
                                <th>Kelas</th>
                                <th>Dosen</th>
                                <th>Waktu</th>
                                <th width="40">Status Kelas</th>

                                <?php if (in_array(19, $group) && $ta == getactyear()) {
                                    $width = 310;
                                } elseif (in_array(19, $group) && $ta != getactyear()) {
                                    $width = 230;
                                } else {
                                    $width = 150;
                                } ?>
                                <th width="<?= $width ?>" style="text-align: center">Aksi</th>
                            </tr>
                        </thead>
                        <tbody>

                            <?php $no = 1;
                            foreach ($dosen as $isi) : ?>

                                <tr>
                                    <td><?= number_format($no); ?></td>
                                    <td><?= $isi->kd_matakuliah; ?></td>
                                    <td><?= get_nama_mk($isi->kd_matakuliah, substr($isi->kd_jadwal, 0, 5)); ?></td>
                                    <td><?= $isi->kelas; ?></td>
                                    <td><?= nama_dsn($isi->kd_dosen); ?></td>
                                    <td>
                                        <?= notohari($isi->hari) . ' / ' . substr($isi->waktu_mulai, 0, 5) . '-' . substr($isi->waktu_selesai, 0, 5); ?>
                                    </td>
                                        
                                    <?php if ($isi->open > 0) {
                                        $akses = 0;
                                        $desk = "OPEN";
                                        $icwarna = "btn-primary";
                                    } else {
                                        $akses = 1;
                                        $desk = "CLOSE";
                                        $icwarna = "btn-danger";
                                    } ?>
                                    <td>
                                        <?php if (!in_array(10, $group)) : ?>
                                        <a 
                                            href="<?= base_url('akademik/ajar/open_absen/'.$isi->id_jadwal.'/'.$akses) ?>"
                                            class="btn <?= $icwarna; ?> btn-small">
                                            <?= $desk; ?>
                                        </a>
                                        <?php else : echo $desk; endif ?>
                                    </td>

                                    <td>
                                        <?php if (in_array(19, $group) && $ta == getactyear()) : ?>
                                            <a 
                                                data-toggle="modal" 
                                                onclick="edit(<?= $isi->id_jadwal; ?>)" 
                                                href="#" 
                                                class="btn btn-info btn-small tooltip-sia" 
                                                title="Input Absensi">
                                                <i class="btn-icon-only icon-plus"> </i>
                                            </a>

                                            <a 
                                                href="<?= base_url('akademik/ajar/edit_absen/'.$isi->id_jadwal); ?>" target='_blank' 
                                                class="btn btn-info btn-small tooltip-sia" 
                                                title="Edit Absensi">
                                                <i class="btn-icon-only icon-edit"></i>
                                            </a>
                                        <?php endif ?>

                                        <?php if (in_array(10, $group)) {
                                            $visibility = "style='display:none'";
                                        } else {
                                            $visibility = "";
                                        } ?>

                                        <a 
                                            href="<?= base_url('akademik/absenDosen/event/'.$isi->id_jadwal); ?>" 
                                            target="_blank" 
                                            class="btn btn-info btn-small tooltip-sia" 
                                            title="Cetak Berita Acara Perkuliahan">
                                            <i class="icon icon-file"></i>
                                        </a>

                                        <a 
                                            <?= $visibility ?>
                                            href="<?= base_url('akademik/ajar/cetak_absensi/'.$isi->id_jadwal); ?>" 
                                            target="_blank" 
                                            class="btn btn-info btn-small tooltip-sia"
                                            title="Print Absensi Kehadiran">
                                            <i class="btn-icon-only icon-print"></i>
                                        </a>

                                        <a 
                                            class="btn btn-info btn-small tooltip-sia"
                                            href="#meetAmount" 
                                            data-toggle="modal" 
                                            onclick="loadMeeting(<?= $isi->id_jadwal; ?>)" 
                                            title="Lihat Jumlah Pertemuan">
                                            <i class="icon icon-eye-open"></i>
                                        </a>

                                        <a 
                                            <?= $visibility ?>
                                            href="<?= base_url('akademik/ajar/cetak_absensi_polos/'.$isi->id_jadwal); ?>" 
                                            target="_blank" 
                                            class="btn btn-info btn-small tooltip-sia"
                                            title="Print Absensi Kosong">
                                            <i class="btn-icon-only icon-print"> </i>
                                        </a>

                                        <a 
                                            href="#detailCourse" 
                                            target="_blank" 
                                            class="btn btn-info btn-small tooltip-sia"
                                            onclick="loadCourse('<?= $isi->id_jadwal ?>','<?= $isi->kd_dosen ?>')" 
                                            data-toggle="modal" title="Rincian Materi">
                                            <i class="btn-icon-only icon-book"> </i>
                                        </a>

                                         <!-- Cetak BA -->
                                        <a 
                                            href="<?= base_url();?>akademik/ajar/cetak_ba/<?= $isi->id_jadwal; ?>" 
                                            target="_blank" 
                                            class="btn btn-info btn-small tooltip-sia" 
                                            title="Cetak Berita Acara Ujian">
                                            <i class="btn-icon-only icon-print"></i>
                                        </a>

                                    </td>
                                </tr>

                            <?php $no++; endforeach ?>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content" id="absen">

        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->



<div class="modal fade" id="meetAmount" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content" id="jml">

        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>

<div class="modal fade" id="detailCourse" tabindex="-1" role="dialog" aria-labelledby="detailCourseLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content" id="detail">

        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>

<script type="text/javascript">
    $(function () {
       $(".tooltip-sia").tooltip()
    })
</script>