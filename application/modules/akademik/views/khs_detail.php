<script type="text/javascript">
    function detail(idj, nim) {
        $("#x").load('<?= base_url() ?>akademik/khs/nilai_detail/' + idj + '/' + nim);
    }

    function detailold(id, nim) {
        $("#x").load('<?= base_url() ?>akademik/khs/nilai_old/' + id + '/' + nim);
    }

    function edit(idj) {
        $("#edit").load('<?= base_url() ?>akademik/khs/view_edit/' + idj);
    }
</script>

<div class="row">
    <div class="span12">
        <div class="widget">
            <div class="widget-header">
                <i class="icon-user"></i>
                <h3>Data Kartu Hasil Studi <?= substr($kode->kd_krs,0,12) ?></h3>
            </div> <!-- /widget-header -->

            <div class="widget-content">
                <div class="span11">

                    <?php
                    $grup = get_group($this->usergroup);
                    if (in_array(7, $grup)) { ?>
                        <a href="<?= base_url('akademik/bimbingan/dp_view/'.$mhs->NIMHSMSMHS); ?>" class="btn btn-warning"> 
                            &laquo; Kembali
                        </a> 
                    <?php } else { ?> 
                        <a href="<?= base_url('akademik/khs'); ?>" class="btn btn-success"> &laquo; Kembali</a> 
                    <?php } ?> 

                    <?php $prodi = $mhs->KDPSTMSMHS;
                                        
                    if ($tahunakademik < '20151') {
                        $hitung_ips = $this->db->query('SELECT distinct 
                                                            a.`NIMHSTRLNM`,a.`KDKMKTRLNM`,a.`NLAKHTRLNM`,a.`BOBOTTRLNM`,b.`sks_matakuliah` 
                                                        FROM tbl_transaksi_nilai a
                                                        JOIN tbl_matakuliah_copy b ON a.`KDKMKTRLNM` = b.`kd_matakuliah`
                                                        WHERE b.`tahunakademik` = "' . $tahunakademik . '" 
                                                        AND kd_prodi = "' . $prodi . '" 
                                                        AND NIMHSTRLNM = "' . $mhs->NIMHSMSMHS . '" 
                                                        and THSMSTRLNM = "' . $tahunakademik . '" ')->result();
                    } else {
                        $hitung_ips = $this->db->query('SELECT distinct 
                                                            a.`NIMHSTRLNM`,a.`KDKMKTRLNM`,a.`NLAKHTRLNM`,a.`BOBOTTRLNM`,b.`sks_matakuliah` 
                                                        FROM tbl_transaksi_nilai a
                                                        JOIN tbl_matakuliah b ON a.`KDKMKTRLNM` = b.`kd_matakuliah`
                                                        WHERE a.`kd_transaksi_nilai` IS NOT NULL 
                                                        AND kd_prodi = "' . $prodi . '" 
                                                        AND NIMHSTRLNM = "' . $mhs->NIMHSMSMHS . '" 
                                                        and THSMSTRLNM = "' . $tahunakademik . '" 
                                                        and a.NLAKHTRLNM <> "T" ')->result();
                    }

                    $sks   = 0;
                    $score = 0;
                    foreach ($hitung_ips as $iso) {
                        $init  = 0;
                        $init  = $iso->BOBOTTRLNM * $iso->sks_matakuliah;
                        $score = $score + $init;
                        $sks   = $sks + $iso->sks_matakuliah;
                    }
                    $ips = $score / $sks;

                    $kd_krs    = substr($kode->kd_krs, 0, 9);
                    $tahunajar = substr($kode->kd_krs, 9, 5);
                    $cek = $this->db->query("SELECT * from tbl_sinkronisasi_renkeu 
                                            WHERE npm_mahasiswa = '{$kd_krs}'
                                            AND (status >= 1 AND status < 8) 
                                            AND tahunajaran = '{$tahunajar}'")->result();


                    $status_krs = $this->app_model->getdetail('tbl_verifikasi_krs', 'kd_krs', $kode->kd_krs, 'kd_krs', 'asc')->row()->status_verifikasi;
                                                           
                    if ($cek && ($status_krs != 9 || $status_krs != 10)) { 
                        if (in_array(5, $grup) || in_array(8, $grup) || in_array(19, $grup)) { ?> 
                            <a href="<?= base_url('akademik/khs/printkhs/'.$npm.'/'.$semester); ?>" target="_blank" class="btn btn-primary">
                                <i class="btn-icon-only icon-print"></i> Print KHS
                            </a>
                        <?php } ?>
                    <?php } ?>

                    <hr>
                    <table>
                        <tr>
                            <td>NPM</td>
                            <td>:</td>
                            <td width="200"><?= $mhs->NIMHSMSMHS; ?></td>
                            <td>Semester</td>
                            <td>:</td>
                            <td><?= $this->app_model->get_semester_khs($mhs->SMAWLMSMHS, $tahunakademik); ?></td>
                        </tr>
                        <tr>
                            <td>Nama</td>
                            <td>:</td>
                            <td width="200"><?= $mhs->NMMHSMSMHS; ?></td>
                            <td>IPS</td>
                            <td>:</td>
                            <td><?= number_format($ips, 2); ?></td>
                        </tr>
                    </table>

                    <br>

                    <table id="example1" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Kode MK</th>
                                <th>Mata Kuliah</th>
                                <th>SKS</th>
                                <th>Nilai</th>
                                <th>Detail</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $no = 1;
                            foreach ($detail_khs as $row) { ?>
                                <tr>
                                    <td><?= $no; ?></td>
                                    <td><?= $row->kd_matakuliah; ?></td>
                                    <td><?= $row->nama_matakuliah; ?></td>
                                    <td><?= $row->sks_matakuliah ?></td>

                                    <?php
                                    $nilai = $this->db->get_where('tbl_transaksi_nilai', [
                                        'NIMHSTRLNM' => $mhs->NIMHSMSMHS,
                                        'KDKMKTRLNM' => $row->kd_matakuliah,
                                        'THSMSTRLNM' => $tahunakademik
                                    ])->row();
                                    ?>
                                    <td><?= ($status_krs != 10) ? $nilai->NLAKHTRLNM : '-'; ?></td>

                                    <td style="text-align: center">
                                        <?php if ($tahunakademik >= '20162') { ?>
                                            <button data-toggle="modal" data-target="#xx" class="btn btn-warning" onclick="detail('<?= get_id_jdl($row->kd_jadwal).'/'.$mhs->NIMHSMSMHS; ?>')">
                                                <i class="icon icon-info"></i>
                                            </button>
                                        <?php } else { ?>
                                            <button data-toggle="modal" data-target="#xx" class="btn btn-warning" onclick="detailold('<?= get_id_jdl($row->kd_jadwal).'/'.$mhs->NIMHSMSMHS; ?>')">
                                                <i class="icon icon-info"></i>
                                            </button>
                                        <?php } ?>
                                    </td>
                                </tr>
                            <?php $no++;
                            } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="xx" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">

    <div class="modal-dialog modal-sm">

        <div class="modal-content" id="x">



        </div><!-- /.modal-content -->

    </div><!-- /.modal-dialog -->

</div><!-- /.modal -->

<div class="modal fade" id="detil" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">

    <div class="modal-dialog">

        <div class="modal-content" id="edit">



        </div><!-- /.modal-content -->

    </div><!-- /.modal-dialog -->

</div><!-- /.modal -->