
<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
    <h4 class="modal-title">Upload SKEP.</h4>
</div>
<form class ='form-horizontal' action="<?php echo base_url(); ?>akademik/kalender/exe_upload_skep" method="post" enctype="multipart/form-data">
    <div class="modal-body">
        <div class="control-group" id="">
            <label class="control-label">Upload File (.xls) </label>
            <div class="controls">
                <input type="file" class="span4 form-control" name="userfile" required/>
                <input type="hidden" name="id" value="<?php echo $id ?>" />
            </div>
        </div>
    </div>
    <div class="modal-footer">
      <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
      <input type="submit" class="btn btn-primary" value="Submit"/>
    </div>
</form>