<?php
if ($tipe == 1) {
    $typ = 'UTS';
} else {
    $typ = 'UAS';
}

$used = get_jur($user);
$prod = str_replace(" ", "_", $used);

header("Content-Type: application/xls");    
header("Content-Disposition: attachment; filename=JADWAL_".$typ."_".$prod."_".$akad.".xls");  
header("Pragma: no-cache"); 
header("Expires: 0");
?>

<style>
table, td, th {
    border: 1px solid black;
}

th {
    background-color: blue;
    color: black;
}
</style>

<table>
    <thead>
        <tr>
            <th colspan="8">Jadwal <?= $typ ?> Tahun <?= get_thajar($akad) ?></th>
        </tr>
        <tr> 
            <th>No</th>
            <th>Kelas</th>
            <th>Kode Mata Kuliah</th>
            <th>Mata Kuliah</th>
            <th>Dosen</th>
            <th>Tanggal Mulai</th>
            <th>Jam Mulai</th>
            <th>Ruang</th>
        </tr>
    </thead>
    <tbody>
        <?php $no=1; foreach ($load->result() as $row) { ?>
        <tr>
            <td><?= $no;?></td>
            <td><?= $row->kelas;?></td>
            <td><?= $row->kd_matakuliah;?></td>
            <td><?= get_nama_mk($row->kd_matakuliah,$row->prodi); ?></td>
            <td><?= nama_dsn($row->kd_dosen);?></td>
            <td><?= $row->start_date; ?></td>
            <td><?= $row->start_time; ?></td>
            <td><?= get_room($row->kd_ruang); ?></td>
        </tr>
        <?php $no++; } ?>        
    </tbody>
</table>