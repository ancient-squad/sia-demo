<script type="text/javascript">
    $(document).ready(function() {
        $('#showHere').load('<?= base_url('akademik/void_new_student/loadData/'.$sess) ?>');
    });
</script>

<style>
    .loader {
      border: 16px solid #f3f3f3;
      border-radius: 50%;
      border-top: 16px solid #20B2AA;
      border-bottom: 16px solid #20B2AA;
      width: 70px;
      height: 70px;
      -webkit-animation: spin 2s linear infinite;
      animation: spin 2s linear infinite;
    }

    @-webkit-keyframes spin {
      0% { -webkit-transform: rotate(0deg); }
      100% { -webkit-transform: rotate(360deg); }
    }

    @keyframes spin {
      0% { transform: rotate(0deg); }
      100% { transform: rotate(360deg); }
    }
</style>

<div class="row">
    <div class="span12">                    
        <div class="widget ">
            <div class="widget-header">
                <i class="icon-user"></i>
                <h3>Data Mahasiswa Baru <?= $sess ?></h3>
            </div> <!-- /widget-header -->           

            <div class="widget-content">
                <div class="span11" id="showHere">
                    <center>
                        <div class="loader"></div>
                    </center>
                </div>
            </div>
        </div>
    </div>
</div>