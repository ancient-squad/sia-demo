<link rel="stylesheet" href="<?= base_url('assets/sa/sweetalert.css') ?>">
<script>
    $(function() {
        $( "#tgl1" ).datepicker({
            changeMonth: true,
            changeYear: true,
            dateFormat: "yy-mm-dd",
            minDate: "-1m",
            maxDate: "+1m",
        });
        $("#tgl1").keydown(function (event) {
            event.preventDefault();
        });
    });
</script>
<script>
    $(function() {
        $( "#tgl2" ).datepicker({
            changeMonth: true,
            changeYear: true,
            dateFormat: "yy-mm-dd",
            minDate: "-1m",
            maxDate: "+7m",
        });
        $("#tgl2").keydown(function (event) {
            event.preventDefault();
        });
    });
</script>
<div class="row">
    <div class="span12">                    
        <div class="widget ">
            <div class="widget-header">
                <h3><i class="icon-calendar"></i> Kelola Tahun Akademik</h3>
            </div>          

            <div class="widget-content">
                <div class="span11">
                    <?php if ($this->session->flashdata('success')): ?>
                        <div class="alert alert-success alert-dismissible">
                            <a href="javascript:;" class="close" data-dismiss="alert">&times;</a>
                            <?= $this->session->flashdata('success') ?>
                        </div>                        
                    <?php endif ?>
                    <?php if ($this->session->flashdata('fail')): ?>
                        <div class="alert alert-danger alert-dismissible">
                            <a href="javascript:;" class="close" data-dismiss="alert">&times;</a>
                            <?= $this->session->flashdata('fail') ?>
                        </div> 
                    <?php endif ?>
                    <a href="" class="btn btn-warning" style="margin-bottom: 20px">&laquo; Kembali</a>
                    <button class="btn btn-primary" data-toggle="modal" data-target="#myModal" style="margin-bottom: 20px">
                        <i class="icon-plus"></i> Tambah Data
                    </button>
                    <table class="table table-striped" id="example1">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Kode</th>
                                <th>Nama</th>
                                <th>Tanggal Efektif Mulai</th>
                                <th>Tanggal Efektif Akhir</th>
                                <th>Status</th>
                                <th style="text-align: center;">Edit</th>
                                <th style="text-align: center;">Aktifkan</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $no=1; foreach ($datalist as $value): ?>
                                <tr>
                                    <td><?= $no ?></td>
                                    <td><?= $value->kode ?></td>
                                    <td><?= $value->tahun_akademik ?></td>
                                    <td><?= $value->tglmulai ?></td>
                                    <td><?= $value->tglakhir ?></td>
                                    <td><?= ($value->status === '1' ? '<span class="label label-success">Aktif</span>' : 'Tidak Aktif') ?></td>
                                    <td style="text-align: center;">
                                        <button data-toggle="modal" data-target="#myModal" title="edit" onclick="load_edit('<?= $value->kode ?>')" class="btn btn-primary"><i class="icon-pencil"></i></button>
                                    </td>
                                    <td style="text-align: center;">
                                        <?php if ($value->status !== '1'): ?>
                                            <a 
                                                title="ubah menjadi aktif" 
                                                onclick="return confirm('Anda yakin ingin merubah tahun akademik aktif?')" 
                                                href="<?= base_url('akademik/tahunakademik/activate/'.$value->kode) ?>" 
                                                class="btn btn-default">
                                                <i class="icon-refresh"></i>
                                            </a>                                            
                                        <?php endif ?>                                        
                                    </td>
                                </tr>                               
                            <?php $no++; endforeach ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="myModal" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title" id="title">Tambah Data</h4>
            </div>
            <form action="<?= base_url('akademik/tahunakademik/store') ?>" method="post" id="form-act" class="form-horizontal">
                <div class="modal-body">
                    <div class="control-group">
                        <label for="kode" class="control-label">Kode</label>
                        <div class="controls">
                            <input type="text" name="kode" id="kode" minlength="5" maxlength="5" class="form-control span3" required />
                            <input type="hidden" id="is-update" value="0" name="is_update">
                        </div>
                    </div>
                    <div class="control-group">
                        <label for="nama" class="control-label">Nama</label>
                        <div class="controls">
                            <input type="text" name="nama" id="nama" class="form-control span3" required />
                        </div>
                    </div>
                    <div class="control-group">
                        <label for="nama" class="control-label">Tanggal Efektif Mulai</label>
                        <div class="controls">
                            <input type="text" name="tglmulai" id="tgl1" class="form-control span3" required />
                        </div>
                    </div>
                    <div class="control-group">
                        <label for="nama" class="control-label">Tanggal Efektif Akhir</label>
                        <div class="controls">
                            <input type="text" name="tglakhir" id="tgl2" class="form-control span3" required />
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary" id="btn-act">Submit</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </form>
        </div>

    </div>
</div>

<script type="text/javascript" src="<?= base_url('assets/sa/sweetalert.min.js') ?>"></script>
<script>
    $(document).ready(function() {
        $('#myModal').on('hide.bs.modal', function() {
            document.getElementById('form-act').reset();
            document.getElementById('kode').readOnly = false;
            document.getElementById('btn-act').innerHTML = 'Submit';
            document.getElementById('title').innerHTML = 'Tambah Data';
            document.getElementById('is-update').value = 0;
        });

        $('#kode').on('input', function (event) { 
            this.value = this.value.replace(/[^0-9]/g, '');
        });
    });

    function load_edit(id) {
        document.getElementById('btn-act').innerHTML = 'Update';
        document.getElementById('title').innerHTML = 'Ubah Data';
        const req = new XMLHttpRequest();
        req.open('POST', '<?= base_url('akademik/tahunakademik/get_year/') ?>' + id);
        req.onreadystatechange = function() {
            if (req.readyState === XMLHttpRequest.DONE) {
                let response = JSON.parse(req.responseText);
                if (response.editable === 0) {
                    document.getElementById('kode').readOnly = true;
                }
                document.getElementById('kode').value = response.kode;
                document.getElementById('nama').value = response.nama;
                document.getElementById('tgl1').value = response.tglmulai;
                document.getElementById('tgl2').value = response.tglakhir;
                document.getElementById('is-update').value = response.kode;
            }
        }
        req.send();
    }
</script>