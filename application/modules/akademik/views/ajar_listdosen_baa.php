<div class="row">
    <div class="span12">                    
        <div class="widget ">
            <div class="widget-header">
                <a 
                    href="<?= base_url(); ?>master/dataajar" 
                    class="btn btn-default" 
                    data-toggle="tooltip" 
                    title="kembali" 
                    style="margin-left: 10px">
                    <i class="icon-chevron-left" style="margin-left: 0"></i>
                </a>
                <h3>Data Dosen Mengajar Tahun Akademik <?= get_thnajar($tahunakademik) ?></h3>
            </div>          

            <div class="widget-content">
                <div class="span11">
                    <a href="<?= base_url(); ?>akademik/ajar/export_excel" class="btn btn-success ">
                        <i class="icon-download"></i> Export Data
                    </a>
                    <p class="pull-right">
                        <i>* Total SKS yang dihitung tidak termasuk skripsi, tesis ataupun kerja praktek/magang</i>
                    </p>
                    <hr>

                    <table id="example1" class="table table-bordered table-striped">
                        <thead>
                            <tr> 
                                <th>No</th>
                                <th>NID</th>
                                <th>Nama Dosen</th>
                                <th>Total Sks</th>
                                <th width="80">Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $no=1; foreach ($dosen as $list): ?>
                                <tr>
                                    <td><?= $no; ?></td>
                                    <td><?= $list->nid; ?></td>
                                    <td><?= $list->nama; ?></td>
                                    <td><?= $list->sks; ?></td>
                                    <td class="td-actions">
                                        <a 
                                            href="<?= base_url();?>akademik/ajar/show_dosen_by_prodi/<?= $list->nid; ?>" 
                                            class="btn btn-primary btn-small"
                                            data-toggle="tooltip"
                                            title="detail mengajar">
                                            <i class="btn-icon-only icon-list"></i>
                                        </a>
                                        <a 
                                            href="<?= base_url('akademik/ajar/print_surat_tugas/'.$list->nid)?>" 
                                            class="btn btn-success btn-small"
                                            data-toggle="tooltip"
                                            title="cetak surat tugas">
                                            <i class="btn-icon-only icon-print"></i>
                                        </a>
                                    </td>
                                </tr>    
                            <?php $no++; endforeach ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>



