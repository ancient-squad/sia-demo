<div class="modal-header">
	<h3>Kehadiran selama <?php echo $pertemuan ?> Pertemuan	</h3>
</div>
<div class="modal-body">
	<table class="table">
		<tr>
			<th width="30%">Hadir</th>
			<th width="5%">:</th>
			<td><?php echo $hadir ?></td>
		</tr>
		<tr>
			<th>Alpha</th>
			<th>:</th>
			<td><?php echo $alpha ?></td>
		</tr>
		<tr>
			<th>Sakit</th>
			<th>:</th>
			<td><?php echo $sakit ?></td>
		</tr>
		<tr>
			<th>Ijin</th>
			<th>:</th>
			<td><?php echo $ijin ?></td>
		</tr>
		<tr>
			<th>Persentase Kehadiran</th>
			<th>:</th>
			<td><?php echo $persentase ?> %</td>
		</tr>
	</table>
</div>