<?php //var_dump($rows);die(); ?>

<script>
    function edit(idk){
        $('#edit').load('<?php echo base_url();?>akademik/kalender/edit_kalender/'+idk);
    }

    function kosong(){
        alert('SKEP belum di upload oleh BAA');
    }
</script>

<div class="row">
    <div class="span12">                    
        <div class="widget ">
            <div class="widget-header">
                <i class="icon-list"></i>
                <h3>Data Kalender Akademik</h3>
            </div> <!-- /widget-header -->
            <div class="widget-content">
                <div class="span11">
                    <hr>
                    <table id="example1" class="table table-bordered table-striped">
                        <thead>
                            <tr> 
                                <th>No</th>
                                <th>Tahun Akademik</th>
                                <th>No SEKP.</th>
                                <th>Tanggal SKEP.</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php 
                                $no = 1; foreach ($rows as $row) { 
                                if ($row->flag == '1') { 
                                    $ta = substr($row->kd_kalender, 0,5);
                                    $no_skep = 'menunggu persetujuan';
                                    $tgl_skep = 'menunggu persetujuan';
                                } elseif ($row->flag == '2') {
                                    $ta = substr($row->kd_kalender, 0,5);
                                    if(is_null($row->no_skep)){ $no_skep = '-';}else{$no_skep = $row->no_skep;};
                                    if(is_null($row->tgl_skep)){$tgl_skep= '-';}else{$tgl_skep = $row->tgl_skep;};
                                    if (!is_null($row->file)) {
                                        $act2 = '<a href="'.base_url().'upload/skep_kamik/'.$row->file.'" class="btn btn-defaut btn-small edit"><i class="btn-icon-only icon-download-alt"> </i></a>';    
                                    }else{
                                        $act2 = '<a href="#" onClick="kosong()" class="btn btn-defaut btn-small edit"><i class="btn-icon-only icon-download-alt"> </i></a>';
                                    }
                                }
                            ?>
                            <tr>
                                <td><?php echo $no ?></td>
                                <td><?php echo $ta ?></td>
                                <td><?php echo $no_skep ?></td>
                                <td><?php echo $tgl_skep ?></td>
                                <td>
                                    <a href="<?php echo base_url(); ?>akademik/kalender/isi_kalender_prodi/<?php echo $row->kd_kalender; ?>" class="btn btn-info btn-small edit"><i class="btn-icon-only icon-list"> </i></a>
                                    <?php echo $act2; ?>
                                </td>
                            </tr>
                            <?php $no++; } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>