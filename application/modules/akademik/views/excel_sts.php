<?php
header("Content-Type: application/xls");    
header("Content-Disposition: attachment; filename=jumlah_mahasiswa_per_prodi.xls");  
header("Pragma: no-cache"); 
header("Expires: 0");
?>


<style>
table, td, th {
    border: 2px solid black;
}


th {
    background-color: blue;
    color: black;
}
</style>

<table>
    <h3>Jumlah Mahasiswa Tahun Ajaran <?php echo $tahun; ?></h3>
    <thead>
        <tr>
            <th rowspan="2">NO</th>
            <th rowspan="2">Prodi</th>
            <th rowspan="2">Jumlah Mahasiswa</th>
            <th colspan="<?php echo $hit; ?>">Angkatan</th>
        </tr>
        <tr>
            <?php foreach ($angkt as $e) { ?>
                <th><?php echo $e->tahun; ?></th>
            <?php } ?>
        </tr>
    </thead>
    <tbody>
        <?php 
        $no = 1;
        foreach ($status as $key) { ?>
        <tr>
            <td><?php echo $no; ?></td>
            <td><?php echo $key->prodi; ?></td>
            <td><?php echo $key->jum; ?></td>
            <?php foreach ($angkt as $e) { 
            $q = $this->db->query("SELECT DISTINCT COUNT(DISTINCT kd_krs) AS mhs FROM tbl_verifikasi_krs a
                                    JOIN tbl_jurusan_prodi b ON a.kd_jurusan = b.kd_prodi
                                    WHERE tahunajaran = '".$tahun."' AND b.kd_prodi = '".$key->kd_prodi."' AND SUBSTR(npm_mahasiswa,1,4) = ".$e->tahun." ")->row();
            //foreach ($q as $kuy)  ?>
                <td><?php echo $q->mhs;?></td>
            <?php } ?>
        </tr>
        <?php $no++; } ?>
    </tbody>
</table>