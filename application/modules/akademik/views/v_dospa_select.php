<script type="text/javascript">
	jQuery(document).ready(function($) {

		$('input[name^=dosen]').autocomplete({

			source: '<?php echo base_url('akademik/dospa/load_dosen_autocomplete'); ?>',

			minLength: 1,

			select: function(evt, ui) {

				this.form.dosen.value = ui.item.value;

				this.form.kd_dosen.value = ui.item.nid;

			}

		});

		$('input[name^=dosen_baru]').autocomplete({

			source: '<?php echo base_url('akademik/dospa/load_dosen_autocomplete'); ?>',

			minLength: 1,

			select: function(evt, ui) {

				this.form.dosen_baru.value = ui.item.value;

				this.form.kd_dosen_baru.value = ui.item.nid;

			}

		});

		$('input[name^=mahasiswa]').autocomplete({

			source: '<?php echo base_url('akademik/kelas/load_mhs_autocomplete'); ?>',

			minLength: 4,

			select: function(evt, ui) {

				this.form.mahasiswa.value = ui.item.value;

				this.form.npm.value = ui.item.npm;
				this.form.nama.value = ui.item.nama;

			}

		});

	});
</script>
<script>
	$(document).ready(function() {
		$("#search").on("click", function(e) {
			$.ajax({
				type: "POST",
				url: "<?php echo base_url("/akademik/kelas/load_mhs_kls/") ?>",
				data: $('#cari_npm').serialize(),
				cache: false,
				dataType: "HTML",
				success: function(data) {
					$('#show').html(data);
				},
				error: function(jqXHR, textStatus, errorThrown) {
					console.log(textStatus);
				}
			});
		});
	});
</script>

<div class="row">
	<div class="span12">
		<div class="widget ">
			<div class="widget-header">
				<i class="icon-user"></i>
				<h3>PEMBIMBING AKADEMIK</h3>
			</div> <!-- /widget-header -->

			<div class="widget-content">
				<ul class="nav nav-tabs">
					<li class="active"><a data-toggle="tab" href="#pa">Update PA</a></li>
					<li><a data-toggle="tab" href="#jln">Update PA Berjalan</a></li>
					<li><a data-toggle="tab" href="#kelas">Update Kelas</a></li>
				</ul>
				<div class="tab-content">
					<div id="pa" class="tab-pane fade in active">
						<div class="span11">
							<b>
								<center>PEMBIMBING AKADEMIK</center>
							</b><br>
							<form id="edit-profile" class="form-horizontal" method="post" action="<?php echo base_url(); ?>akademik/dospa/save_sess">
								<fieldset>
									<div class="control-group">
										<label class="control-label">Dosen PA Mahasiswa</label>
										<div class="controls">
											<input type="text" class="span3" name="dosen" placeholder="Masukan NID Dosen" id="dosen" required>
											<input type="hidden" id='kd_dosen' name="kd_dosen">
										</div> <!-- /controls -->
									</div> <!-- /control-group -->

									<div class="control-group">
										<label class="control-label">Dosen PA Baru</label>
										<div class="controls">
											<input type="text" class="span3" name="dosen_baru" placeholder="Masukan NID Dosen" id="dosen_baru" required>
											<input type="hidden" id='kd_dosen_baru' name="kd_dosen_baru">
										</div> <!-- /controls -->
									</div> <!-- /control-group -->


									<div class="form-actions">
										<input type="submit" class="btn btn-primary" id="save" value="Submit" />
										<input type="reset" class="btn btn-warning" value="Reset" />
									</div> <!-- /form-actions -->
								</fieldset>
							</form>
						</div>
					</div>
					<div id="jln" class="tab-pane fade">
						<div class="span11">
							<b>
								<center>PEMBIMBING AKADEMIK</center>
							</b><br>
							<form id="edit-profile" class="form-horizontal" method="post" action="<?php echo base_url(); ?>akademik/dospa/save_sess2">
								<fieldset>
									<div class="control-group">
										<label class="control-label">Dosen PA Mahasiswa</label>
										<div class="controls">
											<input type="text" class="span3" name="dosen" placeholder="Masukan NID Dosen" id="dosen" required>
											<input type="hidden" id='kd_dosen' name="kd_dosen">
										</div> <!-- /controls -->
									</div> <!-- /control-group -->

									<div class="control-group">
										<label class="control-label">Dosen PA Baru</label>
										<div class="controls">
											<input type="text" class="span3" name="dosen_baru" placeholder="Masukan NID Dosen" id="dosen_baru" required>
											<input type="hidden" id='kd_dosen_baru' name="kd_dosen_baru">
										</div> <!-- /controls -->
									</div> <!-- /control-group -->


									<div class="form-actions">
										<input type="submit" class="btn btn-primary" id="save" value="Submit" />
										<input type="reset" class="btn btn-warning" value="Reset" />
									</div> <!-- /form-actions -->
								</fieldset>
							</form>
						</div>
					</div>
					<div id="kelas" class="tab-pane fade">
						<div class="span11">
							<b>
								<center>PEMBIMBING AKADEMIK DAN KELAS</center>
							</b><br>
							<fieldset>
								<form id="cari_npm" class="form-horizontal">
									<div class="control-group">
										<label class="control-label">NPM</label>
										<div class="controls">
											<input type="text" class="span3" name="mahasiswa" placeholder="Masukan NPM Mahasiswa" id="mahasiswa" required>
											<input type="hidden" id='nama' name="nama">
											<input type="hidden" id='npm' name="npm">
											<input type="button" class="btn btn-primary" id="search" value="Cari" />
										</div> <!-- /controls -->
									</div> <!-- /control-group -->
								</form>
								<div id="show">
								</div>
							</fieldset>
						</div>
					</div>
				</div>

			</div>
		</div>
	</div>
</div>