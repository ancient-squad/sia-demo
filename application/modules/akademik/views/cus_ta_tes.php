<script>
  jQuery(document).ready(function($) {

    $('input[name^=dosen]').autocomplete({

        source: '<?php echo base_url('akademik/krs_mhs/load_dosen_autocomplete');?>',

        minLength: 1,

        select: function (evt, ui) {



            // $('#hargasatuan').html(ui.item.harga_jual);

            // $('#satuan').html(ui.item.satuan);

            // $('#stok').html(ui.item.stok);

            // this.form.kode.value = ui.item.kode;

            this.form.dosen.value = ui.item.value;

            this.form.kd_dosen.value = ui.item.nid;

            // this.form.hargabeli.value = ui.item.harga_beli;

            // this.form.hargajual.value = ui.item.harga_jual;

            //$('#qtyk').focus();

        }

    });

});
</script>

<div class="row">

  <div class="span12">                

      <div class="widget ">

        <div class="widget-header">

          <i class="icon-user"></i>

          <h3>Kegiatan Perkuliahan</h3>

      </div> <!-- /widget-header -->

      <?php $sess = $this->session->userdata('sess_login'); $logdosen = $sess['userid']; ?>

      <div class="widget-content">

        <div class="span11">

          <ul class="nav nav-tabs">
            <li class="active"><a data-toggle="tab" href="#home">Mahasiswa Bimbingan</a></li>
            <li><a data-toggle="tab" href="#menu1">Print Mahasiswa Bimbingan</a></li>
          </ul>

          <div class="tab-content">
            <div id="home" class="tab-pane fade in active">
              <form method="post" class="form-horizontal" action="<?php echo base_url(); ?>akademik/bimbingan/bimsalabim_tes">

                <fieldset>


                      <div class="control-group">

                        <label class="control-label">Tahun Akademik</label>

                        <div class="controls">

                          <select class="form-control span6" name="tahunajaran" id="tahunajaran" required>

                            <option disabled selected>--Pilih Tahun Akademik--</option>

                            <?php foreach ($tahunajar as $row) { ?>

                            <option value="<?php echo $row->kode;?>"><?php echo $row->tahun_akademik;?></option>

                            <?php } ?>

                          </select>

                        </div>

                      </div>
                      

                    <div class="form-actions">

                        <input type="submit" class="btn btn-large btn-success" value="Cari"/> 

                    </div> <!-- /form-actions -->

                </fieldset>

              </form>
            </div>
            <div id="menu1" class="tab-pane fade">
              <form method="post" class="form-horizontal" action="<?php echo base_url(); ?>akademik/bimbingan/cetak_bimb_byprodi">

                <fieldset>

                  <div class="control-group">

                  <?php  
                      $logged = $this->session->userdata('sess_login');
                      $pecah = explode(',', $logged['id_user_group']);
                      $jmlh = count($pecah);
                      for ($i=0; $i < $jmlh; $i++) { 
                        $grup[] = $pecah[$i];
                      }
                  ?>

                  <?php if (in_array(8, $grup)) { ?>
                    <label class="control-label">Dosen Pembimbing</label>

                    <div class="controls">

                       <input type="text" class="span4" name="dosen" value="" placeholder="Ketik Nama Dosen Pembimbing">

                       <input type="hidden" name="kd_dosen" value="" placeholder="">
                       <input type="hidden" name="prodi" value="<?php echo $logdosen; ?>" placeholder="">

                    </div>
                  <?php } else { ?>
                    <label class="control-label">Angkatan</label>

                    <div class="controls">

                      <a href="<?php echo base_url(); ?>akademik/bimbingan/cetak_bimb/<?php echo $logdosen; ?>" class="btn btn-success" title="">Cetak Daftar Mahasiswa Bimbingan</a> 

                    </div>
                  <?php } ?>

                  <?php if (in_array(8, $grup)) { ?>
                    <div class="form-actions">

                        <input type="submit" class="btn btn-large btn-success" value="Cari"/> 

                    </div> <!-- /form-actions -->
                  <?php } ?>

                </fieldset>

              </form>
            </div>
          </div>

        
        </div>

      </div>

    </div>

  </div>

</div>

