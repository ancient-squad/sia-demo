<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
    <h4 class="modal-title"> Daftar Bidang /  Tugas <?php echo $nama ?></h4>
</div>
<div class="modal-body">
	<table class="table table-bordered table-stripped" id="example1">
		<thead>
			<tr>
				<th>No</th>
				<th>Bidang / Tugas</th>
				<th>Sks</th>
				<th>Status</th>
				<th>Aksi</th>
			</tr>
		</thead>
		<?php $i=1; ?>
		<tbody>
		<?php if ($totalData == 0): ?>
			<tr>
				<td colspan="4">Tidak Ada Data</td>
			</tr>
		<?php else: ?>
			<!-- Pendidikan/ Pengajaran -->
			<tr>
				<td></td>
				<td><b>Pendidikan / Pengajaran</b></td>
				<td></td>
				<td></td>
				<td></td>
			</tr>
			<?php 
				foreach ($aditional as $key => $value): 
                $type = 1;
				$link = 'akademik/validasi-bkd/approve/'.$type.'/'.$value->id;	
			?>
			<tr>
				<td><?php echo $i++ ?></td>
				<td><?php echo $value->komponen ?></td>
				<td><?php echo $value->sks ?></td>
				<td><?php echo approval_teaching_status($value->status) ?></td>
				<td>
					<?php if ($userGroup == 21 ) :?>
					    <?php if (is_null($value->status) || $value->status == 4 || $value->status == 9) :?>
					        <a class="btn btn-success btn-small" href="<?php echo base_url($link) ?>"  title="Setujui" href="" ><i class="btn-icon-only icon-ok"> </i></a>

					        <a class="btn btn-warning btn-small revisi" type="<?php echo $type ?>" lrid="<?php echo $value->id?>" title="Revisi" href="#revisiModal" data-toggle="modal"><i class="btn-icon-only icon-edit"> </i></a>
					    <?php endif ?>
					<?php elseif ($userGroup == 22 ) :?>
					    <?php if ($value->status == 1 || $value->status == 5 || $value->status == 9) :?>
					        <a class="btn btn-success btn-small" href="<?php echo base_url($link) ?>"  title="Setujui" href="" ><i class="btn-icon-only icon-ok"> </i></a>

					        <a class="btn btn-warning btn-small revisi" type="<?php echo $type ?>" lrid="<?php echo $value->id?>" title="Revisi" href="#revisiModal" data-toggle="modal"><i class="btn-icon-only icon-edit"> </i></a>
					    <?php endif ?>
					<?php elseif ($userGroup == 23 ) :?>
					    <?php if ($value->status == 1 || $value->status == 2 || $value->status == 6): ?>
					        <a class="btn btn-success btn-small" href="<?php echo base_url($link) ?>"  title="Setujui" href="" ><i class="btn-icon-only icon-ok"> </i></a>

					        <a class="btn btn-warning btn-small revisi" type="<?php echo $type ?>" lrid="<?php echo $value->id?>" title="Revisi" href="#revisiModal" data-toggle="modal"><i class="btn-icon-only icon-edit"> </i></a>
					    <?php endif ?>
					<?php endif ?>
				</td>
			</tr>
			<?php endforeach ?>

			<!-- Penelitian -->
			<tr>
				<td></td>
				<td><b>Penelitian</b></td>
				<td></td>
				<td></td>
				<td></td>
			</tr>
			<?php 
				foreach ($research as $key => $value):
				$type = 2;
				$link = 'akademik/validasi-bkd/approve/'.$type.'/'.$value->id;	
			?>
			<tr>
				<td><?php echo $i++ ?></td>
				<td><?php echo $value->program ?></td>
				<td><?php echo $value->sks ?></td>
				<td><?php echo approval_teaching_status($value->status)?></td>
				<td>
					<?php if ($userGroup == 21 ) : ?>
					    <?php if (is_null($value->status) || $value->status == 4 || $value->status == 9) :?>
					        <a class="btn btn-success btn-small" href="<?php echo base_url($link) ?>"  title="Setujui" href="" ><i class="btn-icon-only icon-ok"> </i></a>

					        <a class="btn btn-warning btn-small revisi" type="<?php echo $type ?>" lrid="<?php echo $value->id?>" title="Revisi" href="#revisiModal" data-toggle="modal"><i class="btn-icon-only icon-edit"> </i></a>
					    <?php endif ?>
					<?php elseif ($userGroup == 22 ) :?>
					    <?php if ($value->status == 1 || $value->status == 5 || $value->status == 9) :?>
					        <a class="btn btn-success btn-small" href="<?php echo base_url($link) ?>"  title="Setujui" href="" ><i class="btn-icon-only icon-ok"> </i></a>

					        <a class="btn btn-warning btn-small revisi" type="<?php echo $type ?>" lrid="<?php echo $value->id?>" title="Revisi" href="#revisiModal" data-toggle="modal"><i class="btn-icon-only icon-edit"> </i></a>
					    <?php endif ?>
					<?php elseif ($userGroup == 23 ) :?>
					    <?php if ($value->status == 1 || $value->status == 2 || $value->status == 6): ?>
					        <a class="btn btn-success btn-small" href="<?php echo base_url($link) ?>"  title="Setujui" href="" ><i class="btn-icon-only icon-ok"> </i></a>

					        <a class="btn btn-warning btn-small revisi" type="<?php echo $type ?>" lrid="<?php echo $value->id?>" title="Revisi" href="#revisiModal" data-toggle="modal"><i class="btn-icon-only icon-edit"> </i></a>
					    <?php endif ?>
					<?php endif ?>
				</td>
			</tr>
			<?php endforeach ?>

			<!-- Pengabdian -->
			<tr>
				<td></td>
				<td><b>Pengabdian</b></td>
				<td></td>
				<td></td>
				<td></td>
			</tr>
			<?php 
				foreach ($devotion as $key => $value):
				$type = 3;
				$link = 'akademik/validasi-bkd/approve/'.$type.'/'.$value->id;	
			?>
			<tr>
				<td><?php echo $i++ ?></td>
				<td><?php echo $value->program ?></td>
				<td><?php echo $value->sks ?></td>
				<td><?php echo approval_teaching_status($value->status)?></td>
				<td>
					<?php if ($userGroup == 21 ) :?>
					    <?php if (is_null($value->status) || $value->status == 4 || $value->status == 9) :?>
					        <a class="btn btn-success btn-small" href="<?php echo base_url($link) ?>"  title="Setujui" href="" ><i class="btn-icon-only icon-ok"> </i></a>

					        <a class="btn btn-warning btn-small revisi" type="<?php echo $type ?>" lrid="<?php echo $value->id?>" title="Revisi" href="#revisiModal" data-toggle="modal"><i class="btn-icon-only icon-edit"> </i></a>
					    <?php endif ?>
					<?php elseif ($userGroup == 22 ) :?>
					    <?php if ($value->status == 1 || $value->status == 5 || $value->status == 9) :?>
					        <a class="btn btn-success btn-small" href="<?php echo base_url($link) ?>"  title="Setujui" href="" ><i class="btn-icon-only icon-ok"> </i></a>

					        <a class="btn btn-warning btn-small revisi" type="<?php echo $type ?>" lrid="<?php echo $value->id?>" title="Revisi" href="#revisiModal" data-toggle="modal"><i class="btn-icon-only icon-edit"> </i></a>
					    <?php endif ?>
					<?php elseif ($userGroup == 23 ) :?>
					    <?php if ($value->status == 1 || $value->status == 2 || $value->status == 6): ?>
					        <a class="btn btn-success btn-small" href="<?php echo base_url($link) ?>"  title="Setujui" href="" ><i class="btn-icon-only icon-ok"> </i></a>

					        <a class="btn btn-warning btn-small revisi" type="<?php echo $type ?>" lrid="<?php echo $value->id?>" title="Revisi" href="#revisiModal" data-toggle="modal"><i class="btn-icon-only icon-edit"> </i></a>
					    <?php endif ?>
					<?php endif ?>
				</td>
			</tr>
			<?php endforeach ?>

			<!-- Lain2 -->
			<tr>
				<td></td>
				<td><b>Lain2</b></td>
				<td></td>
				<td></td>
				<td></td>
			</tr>
			<?php 
				foreach ($others as $key => $value):
				$type = 4;
				$link = 'akademik/validasi-bkd/approve/'.$type.'/'.$value->id;	
			?>
			<tr>
				<td><?php echo $i++ ?></td>
				<td><?php echo $value->program ?></td>
				<td><?php echo $value->sks ?></td>
				<td><?php echo approval_teaching_status($value->status)?></td>
				<td>
					<?php if ($userGroup == 21 ) :?>
					    <?php if (is_null($value->status) || $value->status == 4 || $value->status == 9) :?>
					        <a class="btn btn-success btn-small" href="<?php echo base_url($link) ?>"  title="Setujui" href="" ><i class="btn-icon-only icon-ok"> </i></a>

					        <a class="btn btn-warning btn-small revisi" type="<?php echo $type ?>" lrid="<?php echo $value->id?>" title="Revisi" href="#revisiModal" data-toggle="modal"><i class="btn-icon-only icon-edit"> </i></a>
					    <?php endif ?>
					<?php elseif ($userGroup == 22 ) :?>
					    <?php if ($value->status == 1 || $value->status == 5 || $value->status == 9) :?>
					        <a class="btn btn-success btn-small" href="<?php echo base_url($link) ?>"  title="Setujui" href="" ><i class="btn-icon-only icon-ok"> </i></a>

					        <a class="btn btn-warning btn-small revisi" type="<?php echo $type ?>" lrid="<?php echo $value->id?>" title="Revisi" href="#revisiModal" data-toggle="modal"><i class="btn-icon-only icon-edit"> </i></a>
					    <?php endif ?>
					<?php elseif ($userGroup == 23 ) :?>
					    <?php if ($value->status == 1 || $value->status == 2 || $value->status == 6): ?>
					        <a class="btn btn-success btn-small" href="<?php echo base_url($link) ?>"  title="Setujui" href="" ><i class="btn-icon-only icon-ok"> </i></a>

					        <a class="btn btn-warning btn-small revisi" type="<?php echo $type ?>" lrid="<?php echo $value->id?>" title="Revisi" href="#revisiModal" data-toggle="modal"><i class="btn-icon-only icon-edit"> </i></a>
					    <?php endif ?>
					<?php endif ?>
				</td>
			</tr>
			<?php endforeach ?>
		<?php endif ?>
		</tbody>
	</table>
</div>


<!-- Modal Revisi -->
<div class="modal fade" id="revisiModal" tabindex="-1" role="dialog" aria-labelledby="revisiModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content" id="revModal">
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->


<script type="text/javascript">
	 $(function () {
        $(".revisi").on("click", function (e) {
            e.preventDefault();
            lrid = $(this).attr("lrid")
            type = $(this).attr("type")
            $('#revModal').load('<?php echo base_url();?>akademik/validasi-bkd/revisi-modal/' + type +'/'+ lrid);
        })
    })
</script>