<div class="row">

    <div class="span12">

        <div class="widget ">

            <div class="widget-header">

                <i class="icon-user"></i>

                <h3><?php echo $title ?></h3>

            </div>
            <!-- /widget-header -->

            <div class="widget-content">

                <div class="span11">

                    <a class="btn btn-success" href="<?php echo base_url() ?>"> <i class="btn-icon-only icon-arrow-left"></i> Kembali </a>
                    <br>
                    <hr>

                    <table id="tbl-list_validasi-bkd" class="table table-bordered table-striped">

                        <thead>

                            <tr>

                                <th>No</th>
                                <th>NID</th>
                                <th>Nama</th>
                                <th width="150">Aksi</th>

                            </tr>

                        </thead>

                        <tbody>
                            <?php if (count($list) > 0) : ?>
                                <?php 
                                    $i=1; 
                                    foreach ($list as $key => $value) :
                                ?>
                                    <tr>
                                        <td> <?php echo $i++; ?> </td>
                                        <td> <?php echo $value->user ?> </td>
                                        <td> <?php echo $value->nama?> </td>
                                        <td class="td-actions <?php echo $value->status_id."-".$user ?> ">
                                             <a class="btn btn-default btn-small list-tugas" href="#tugasModal" user="<?php echo $value->user ?>" data-toggle="modal"  title="List Kegiatan" href="" ><i class="btn-icon-only icon-list"> </i></a>
                                        </td>
                                    </tr>
                                <?php endforeach ?>
                            <?php else: ?>
                            <tr>
                              <td align="center" colspan="8" style="text-align: center;"><i>Tidak Ada Data</i></td>
                            </tr>
                            <?php endif ?>
                        </tbody>

                    </table>

                </div>

            </div>

        </div>

    </div>

</div>

<!-- Begin Kegiatan Modal -->
<div class="modal fade" id="tugasModal" tabindex="-1" role="dialog" aria-labelledby="tugasLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content" id="tugasModal">
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- End Kegiatan Modal -->

<script type="text/javascript">
    $(function () {
        // Load modal
        $(".list-tugas").on("click", function(e){
            e.preventDefault()

            user = $(this).attr("user")
            $("#tugasModal").load("<?php echo base_url();?>akademik/validasi-bkd/list-tugas/" + user)
        })
    })
</script>