<link href="https://gitcdn.github.io/bootstrap-toggle/2.2.0/css/bootstrap-toggle.min.css" rel="stylesheet">
<script src="https://gitcdn.github.io/bootstrap-toggle/2.2.0/js/bootstrap-toggle.min.js"></script>

<div class="row">
	<div class="span12">      		  		
  		<div class="widget ">
  			<div class="widget-header">
  				<i class="icon-user"></i>
  				<h3>Data Kartu Rencana Studi</h3>
			</div> <!-- /widget-header -->
			
			<div class="widget-content">
				<div class="span11">
					<table id="example1" class="table table-bordered table-striped">
	                	<thead>
	                        <tr> 
	                        	<th>No</th>
	                        	<th>NIM</th>
	                        	<th>Nama MHS</th>
	                        	<th>Tahun Masuk</th>
	                            <th width="40">Aksi</th>
	                        </tr>
	                    </thead>
	                    <tbody>
	                    	<?php $no=1; foreach ($rows as $value) { ?>
	                        <tr>
	                        	<td><?php echo number_format($no); ?></td>
	                        	<td><?php echo $value->NIMHSMSMHS; ?></td>
	                        	<td><?php echo $value->NMMHSMSMHS; ?></td>
                                <td><?php echo $value->TAHUNMSMHS; ?></td>
                                <?php $slugcoy = $this->app_model->getdetail('tbl_verifikasi_krs','npm_mahasiswa',$value->NIMHSMSMHS,'npm_mahasiswa','asc')->row(); ?>
	                        	<td class="td-actions">
	                        	<?php if (is_null($slugcoy->slug_url)) {
	                        		$lanjut = $row->kd_krs;
	                        	} else {
	                        		$lanjut = $slugcoy->slug_url;
	                        	}
	                        	 ?>
									<a class="btn btn-success btn-small" href="<?php echo base_url(); ?>akademik/viewkrs/viewkrsmhs/<?php echo $lanjut; ?>"><i class="btn-icon-only icon-ok"> </i></a>
								</td>
	                        </tr>
	                        <?php $no++;} ?>
	                    </tbody>
	               	</table>
				</div>
			</div>
		</div>
	</div>
</div>