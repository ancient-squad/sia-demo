<?php
header("Content-Type: application/xls");    
header("Content-Disposition: attachment; filename=jumlah_mahasiswa_reg_dan_nonreg.xls");  
header("Pragma: no-cache"); 
header("Expires: 0");
?>

<style>
    table, td {
        border: 2px solid black;
    }
</style>

<table>
    <tr>
        <td colspan="5" style="text-align: center;"><b>Jumlah Mahasiswa Reguler dan Non-reguler</b></td>
    </tr>
    <tr> 
        <th>No</th>
        <th>Prodi</th>
        <th>Kelas A</th>
        <th>Kelas B</th>
        <th>Kelas C</th>
    </tr>
    <?php $no=1; foreach ($data as $isi): ?>
        <tr>
            <td><?= $no; ?></td>
            <td><?= $isi->prodi; ?></td>
            <td><?= amountStudent($isi->kd_prodi,'PG',$sess) ?></td>
            <td><?= amountStudent($isi->kd_prodi,'SR',$sess) ?></td>
            <td><?= amountStudent($isi->kd_prodi,'PK',$sess) ?></td>
        </tr>    
    <?php $no++; endforeach ?>
</table>