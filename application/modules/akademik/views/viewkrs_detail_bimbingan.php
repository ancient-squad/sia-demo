
<div class="row">
	<div class="span12">      		  		
  		<div class="widget ">
  			<div class="widget-header">
  				<i class="icon-user"></i>
  				<h3>Data Kartu Rencana Studi</h3>
			</div> <!-- /widget-header -->
			
			<div class="widget-content">
				<div class="span11">
					<a href="<?php echo base_url(); ?>akademik/bimbingan/list_bimbingan" class="btn btn-success">&laquo; Kembali</a>
					<?php 
						$aktif = $this->setting_model->getaktivasi('krs')->result();
						$aktif2 = $this->setting_model->getaktivasi('krss2')->result();
						$rev = $this->setting_model->getaktivasi('revisikrs')->result();
						$rev2 = $this->setting_model->getaktivasi('revisikrss')->result();
						/** aktifkan untuk mode live **/
      					if ($aktif == TRUE || $aktif2 ==  TRUE) { ?>
						<a href="<?php echo base_url('akademik/bimbingan/comein/'.$sts->id_verifikasi)?>" onclick="return confirm('Verifikasi KRS Mahasiswa?');" class="btn btn-primary">Verifikasi</a>
						<a href="<?php echo base_url('akademik/bimbingan/comeback/'.$sts->id_verifikasi)?>" onclick="return confirm('Revisi KRS Mahasiswa?');" class="btn btn-default">Revisi</a>
					<?php } ?>
					<a href="<?php echo base_url(); ?>akademik/bimbingan/dp_view/<?php echo $this->session->userdata('npm'); ?>" target="blank" class="btn btn-warning">KHS Mahasiswa</a>
					<hr>
					<table>
						<tr>
							<td>Status</td>
								<?php if ($sts->status_verifikasi == 1) {
									$stats = '<b style="background:#ADFF2F;padding:2px;border-radius:10px;">TERIMA</b>';
								} elseif ($sts->status_verifikasi == 2) {
									$stats = '<b style="background:#FFA500;padding:2px;border-radius:10px;">REVISI</b>';
								} else {
									$stats = '<b style="background:#FF0000;padding:2px;border-radius:10px;color:white;">PENGAJUAN<b>';
								} ?>
							<td> : <?php echo $stats; ?></td>
							<td></td>
						</tr>
						<tr>
							<td>Pembimbing Akademik</td>
							<td> : <?php echo $pembimbing['nama']; ?></td>
							<td></td>
						</tr>
						<!-- <tr>
							<td>Catatan</td>
							<td> : <?php echo $pembimbing['keterangan_krs']; ?></td>
							<td></td>
						</tr> -->
					</table>
					<table id="tabel_krs" class="table table-bordered table-striped">
	                	<thead>
	                        <tr> 
	                        	<th>No</th>
	                        	<th>Kode MK</th>
	                        	<th>Mata Kuliah</th>
	                        	<th>SKS</th>								
	                        	<th>Dosen</th>
								<th>Hari/Waktu</th>
								<th>Ruangan</th>
	                        </tr>
	                    </thead>
	                    <tbody>
                            <?php $no = 1; foreach ($detail_krs as $row) { ?>
	                        <tr>
								<?php
									switch ($row->hari){
										case 1 :
											$hari = 'Senin';
										break;
										case 2 :
											$hari = 'Selasa';
										break;
										case 3 :
											$hari = 'Rabu';
										break;
										case 4 :
											$hari = 'Kamis';
										break;
										case 5 :
											$hari = 'Jumat';
										break;
		
										case 6 :
											$hari = 'Sabtu';
										break;
										case 7 :
											$hari = 'Minggu';
										break;
										default:
										$hari = '';
									}
								?>
	                        	<td><?php echo $no; ?></td>
	                        	<td><?php echo $row->kd_matakuliah; ?></td>
                                <td><?php echo $row->nama_matakuliah; ?></td>
                                <td><?php echo $row->sks_matakuliah; ?></td>
                                <td><?php echo $row->nama; ?></td>
                                <td><?php echo $hari.' / '.substr($row->waktu_mulai, 0,5).'-'.substr($row->waktu_selesai, 0,5); ?></td>
                                <td><?php echo $row->kode_ruangan; ?></td>
                                <?php $this->load->model('setting_model');?>
	                        </tr>
	                        <?php $no++; } ?>
	                    </tbody>
	               	</table>
	               	<!-- <hr>
	               	<h3>Riwayat Perwalian</h3>
	               	<br>
					<table id="tabel_krs" class="table table-bordered table-striped">
	                	<thead>
	                        <tr> 
	                        	<th width="40">No</th> -->
	                        	<!-- <th>Catatan</th> -->
	                        	<!-- <th>Tanggal</th>
	                        	<th>Tahun Ajaran</th>
	                        </tr>
	                    </thead>
	                    <tbody>
                            <?php $no = 1; foreach ($histori as $kew) { ?>
	                        <tr>
	                        	<td><?php echo $no; ?></td> -->
	                        	<!-- <td><?php echo $kew->note; ?></td> -->
                                <!-- <td><?php echo $kew->tgl; ?></td>
                                <td><?php echo get_thnajar(substr($kew->kd_krs, 12,5)); ?></td>
	                        </tr>
	                        <?php $no++; } ?>
	                    </tbody>
	               	</table> -->
				</div>
			</div>
		</div>
	</div>
</div>
<div class="modal fade" id="jadwal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">PILIH JADWAL MATAKULIAH</h4>
            </div>
            <form class ='form-horizontal' action="<?php echo base_url(); ?>akademik/viewkrs/save_jadwal" method="post">
                <div class="modal-body">    
					<input type="hidden" id="kd_jadwal" name="kd_jadwal" value=""/>
					<input type="hidden" id="kd_matakuliah" name="kd_matakuliah" value=""/>
					<input type="hidden" id="kd_krs" name="kd_krs" value="<?php echo $kd_krs; ?>"/>
                    <table id="tabel_jadwal" class="table table-bordered table-striped">
	                  <thead>
	                        <tr> 
	                          <th>Hari</th>
	                          <th>Kelas</th>
	                          <th>Jam</th>
	                          <th>Ruang</th>
							  <th>Dosen</th>
							  <th></th>
							  <th></th>
							  <th>Kuota</th>
							  <th>Jumlah</th>
	                        </tr>
	                    </thead>
	                    <tbody>
	                    	<tr>
	                    		<td></td>
	                    		<td></td>
	                    		<td></td>
	                    		<td></td>
	                    		<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
	                    	</tr>
	                    </tbody>
	                </table>
                </div> 
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <input type="submit" class="btn btn-primary" value="Save changes"/>
                </div>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div class="modal fade" id="verif" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Status Verifikasi</h4>
            </div>
            <form class ='form-horizontal' action="<?php echo base_url(); ?>akademik/bimbingan/comein" method="post">
                <div class="modal-body"> 

                    <div class="control-group">
                    	<input type="hidden" name="kode" value="<?php echo $detail_krs_row->kd_krs; ?>">
                    	<!-- <input type="hidden" name="id_verif" value="<?php echo $detail_krs_row->id_verifikasi; ?>"> -->
                        <label class="control-label">Status</label>

                        <div class="controls">

                          <select class="form-control span3" name="sts" id="" required>

                            <option disabled selected>--Pilih Status--</option>

                            <option value="1">Terima</option>

                            <!-- <option value="2">Revisi</option> -->

                          </select>

                        </div>

                    </div>	

                    <!-- <div class="control-group">

                        <label class="control-label">Catatan</label>

                        <div class="controls">

                          <textarea name="note" class="form-control span3" required></textarea>

                        </div>

                    </div> -->
					
                </div> 
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <input type="submit" class="btn btn-primary" value="Save changes"/>
                </div>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>