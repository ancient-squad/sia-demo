<?php // error_reporting(0); ?>

<div class="row">
	<div class="span12">      		  		
  		<div class="widget ">
  			<div class="widget-header">
  				<i class="icon-file"></i>
  				<h3>Data Kartu Hasil Studi</h3>
			</div> <!-- /widget-header -->
			
			<div class="widget-content" style="overflow:auto;">
				<div class="span11">
					<?php 
					$krs_status = $this->db->query("SELECT status_verifikasi FROM tbl_verifikasi_krs 
													WHERE kd_krs LIKE '{$mhs->NIMHSMSMHS}%' 
													AND status_verifikasi = 10")->row();

					if (isset($krs_status->status_verifikasi)) {  ?>
						<a href="<?= base_url(); ?>akademik/khs/transkrip/<?= $mhs->NIMHSMSMHS; ?>" class="btn btn-primary">
							<i class="icon icon-print"></i> Transkrip
						</a>
						<hr>
					<?php } ?>
					
					<table style="overflow-x:auto !important;">
                        <tr>
                            <td>NPM</td>
                            <td>:</td>
                            <td><?= $mhs->NIMHSMSMHS;?></td>
                        </tr>
                        <tr>
                            <td>Nama</td>
                            <td>:</td>
                            <td><?= $mhs->NMMHSMSMHS;?></td>
                        </tr>
                        <?php
                        
						$logged = $this->session->userdata('sess_login');
						$grup = get_group($logged['id_user_group']);

	                    if ((in_array(5, $grup)) || (in_array(7, $grup))) {
							$prodi = $mhs->KDPSTMSMHS;
						} else {
							$prodi = $logged['userid'];
						}

						$tskss = 0;
						$tnl   = 0;

						if (isset($detail_khs_konversi) && count($detail_khs_konversi) > 0) {
							$q 	= $this->db->query("SELECT nl.`KDKMKTRLNM`,
														IF(nl.`THSMSTRLNM`<'20151',
															(SELECT DISTINCT nama_matakuliah FROM tbl_matakuliah_copy 
															WHERE kd_matakuliah = nl.`KDKMKTRLNM` 
															AND kd_prodi = nl.`KDPSTTRLNM` 
															ORDER BY id_matakuliah DESC LIMIT 1),

															(SELECT DISTINCT nama_matakuliah FROM tbl_matakuliah 
															WHERE kd_matakuliah = nl.`KDKMKTRLNM` 
															AND kd_prodi = nl.`KDPSTTRLNM` 
															ORDER BY id_matakuliah DESC LIMIT 1) ) AS nama_matakuliah,

														IF(nl.`THSMSTRLNM`<'20151',
															(SELECT DISTINCT sks_matakuliah FROM tbl_matakuliah_copy 
															WHERE kd_matakuliah = nl.`KDKMKTRLNM` 
															AND kd_prodi = nl.`KDPSTTRLNM` 
															ORDER BY id_matakuliah DESC LIMIT 1),

															(SELECT DISTINCT sks_matakuliah FROM tbl_matakuliah 
															WHERE kd_matakuliah = nl.`KDKMKTRLNM` 
															AND kd_prodi = nl.`KDPSTTRLNM` 
															ORDER BY id_matakuliah DESC LIMIT 1) ) AS sks_matakuliah,
														MIN(nl.`NLAKHTRLNM`) AS NLAKHTRLNM,
														nl.`THSMSTRLNM`,
														MAX(nl.`BOBOTTRLNM`) AS BOBOTTRLNM 
													FROM tbl_transaksi_nilai_konversi nl 
													WHERE nl.`NIMHSTRLNM` = '{$mhs->NIMHSMSMHS}' 
													AND NLAKHTRLNM != 'T'
													AND nl.deletedAt IS NULL
													GROUP BY nl.`KDKMKTRLNM` ORDER BY nl.`THSMSTRLNM` ASC")->result();
							$tskss = 0;
							$tnl   = 0;
							foreach ($q as $row) {
								$nk    = ($row->sks_matakuliah*$row->BOBOTTRLNM);
								$tskss = $tskss+$row->sks_matakuliah;
								$tnl   = $tnl+$nk;
							}
						}
						
						$hitung_ips = $this->db->query("SELECT nl.`KDKMKTRLNM`,
															IF(nl.`THSMSTRLNM`<'20151',
																(SELECT DISTINCT nama_matakuliah FROM tbl_matakuliah_copy 
																WHERE kd_matakuliah = nl.`KDKMKTRLNM` 
																AND kd_prodi = nl.`KDPSTTRLNM` 
																AND tahunakademik = nl.`THSMSTRLNM` 
																ORDER BY id_matakuliah DESC LIMIT 1),

																(SELECT DISTINCT nama_matakuliah FROM tbl_matakuliah 
																WHERE kd_matakuliah = nl.`KDKMKTRLNM` 
																AND kd_prodi = nl.`KDPSTTRLNM` 
																ORDER BY id_matakuliah DESC LIMIT 1) ) AS nama_matakuliah,

															IF(nl.`THSMSTRLNM`<'20151',
																(SELECT DISTINCT sks_matakuliah FROM tbl_matakuliah_copy 
																WHERE kd_matakuliah = nl.`KDKMKTRLNM` 
																AND kd_prodi = nl.`KDPSTTRLNM` 
																AND tahunakademik = nl.`THSMSTRLNM` 
																ORDER BY id_matakuliah DESC LIMIT 1),

																(SELECT DISTINCT sks_matakuliah FROM tbl_matakuliah 
																WHERE kd_matakuliah = nl.`KDKMKTRLNM` 
																AND kd_prodi = nl.`KDPSTTRLNM` 
																ORDER BY id_matakuliah DESC LIMIT 1) ) AS sks_matakuliah,

															MIN(nl.`NLAKHTRLNM`) AS NLAKHTRLNM,
															MAX(nl.`THSMSTRLNM`) AS THSMSTRLNM,
															MAX(nl.`BOBOTTRLNM`) AS BOBOTTRLNM 
														FROM tbl_transaksi_nilai nl 
														WHERE nl.`NIMHSTRLNM` = '{$mhs->NIMHSMSMHS}' 
														AND NLAKHTRLNM != 'T'
														AND nl.deletedAt IS NULL
														GROUP BY nl.`KDKMKTRLNM` ORDER BY THSMSTRLNM ASC")->result();
						// get ips
						// $ips = $this->krs_model->countips($prodi, $mhs->NIMHSMSMHS, $year);

						$st = 0;
						$ht = 0;
						foreach ($hitung_ips as $iso) {
							$h = 0;
							$h = $iso->BOBOTTRLNM * $iso->sks_matakuliah;
							$ht=  $ht + $h;
							$st=$st+$iso->sks_matakuliah;
						}

						$ipk = $ht/$st;
						$ipk = number_format($ipk,2);
						
						$totalsks = $tskss+$st;
						$jkon     = $tnl+$ht;
						$ipk_kon  = $jkon/$totalsks;
						$ipk_kon  = number_format($ipk_kon,2);
                        ?>
                        <tr>
                            <td>IPK</td>
                            <td>:</td>
							<td><?= (isset($detail_khs_konversi) && count($detail_khs_konversi) > 0) ? $ipk_kon : $ipk ?></td>
                        </tr>
                        <tr>
                            <td>SKS</td>
                            <td>:</td>
                            <td><?= (isset($detail_khs_konversi) && count($detail_khs_konversi) > 0) ? $totalsks : $st ?></td>
                        </tr>
                    </table>
                    <br>
					<table id="example1" class="table table-bordered table-striped">
	                	<thead>
	                        <tr> 
	                        	<th>No</th>
	                        	<th>Semester</th>
	                        	<th>Total SKS</th>
	                        	<th>IPS</th>
	                            <th width="40">Aksi</th>
	                        </tr>
	                    </thead>
	                    <tbody>
							<?php 
							$no = 1; 
							if (isset($detail_khs_konversi) && count($detail_khs_konversi) > 0) { 
								foreach ($detail_khs_konversi as $row2) { ?>
		                        <tr>
		                        	<td><?= $no; ?></td>
		                        	<td>KONVERSI</td>
		                        	<?php
		                        		$q = $this->db->query("SELECT DISTINCT KDKMKTRLNM 
		                        								FROM tbl_transaksi_nilai_konversi 
		                        								WHERE NIMHSTRLNM = '{$mhs->NIMHSMSMHS}' 
		                        								AND THSMSTRLNM = '{$row2->THSMSTRLNM}'")->result();
										$sumsks = 0;
										$kdpst  = trim($row2->KDPSTTRLNM);
			                        	if ($row->THSMSTRLNM < '20151') {
			                        		foreach ($q as $value1) {
				                        		$sksmk = $this->db->query("SELECT DISTINCT sks_matakuliah 
				                        									FROM tbl_matakuliah_copy 
				                        									WHERE kd_matakuliah = '{$value1->KDKMKTRLNM}' 
				                        									AND kd_prodi = '{$kdpst}' 
				                        									AND tahunakademik = '{$row2->THSMSTRLNM}'")->row()->sks_matakuliah; 
				                        		$sumsks = $sumsks + $sksmk;
				                        	}
			                        	} else {
			                        		foreach ($q as $value1) {
				                        		$sksmk = $this->db->query("SELECT DISTINCT sks_matakuliah 
				                        									FROM tbl_matakuliah 
				                        									WHERE kd_matakuliah = '{$value1->KDKMKTRLNM}' 
			                        										AND kd_prodi = '$kdpst'")->row()->sks_matakuliah; 
				                        		$sumsks = $sumsks + $sksmk;
				                        	}
			                        	}
		                        	?>
	                                <td><?= $sumsks; ?></td>
	                                <td>-</td>
		                        	<td class="td-actions">
		                        		<?php if (in_array(7, $grup) || in_array(6, $grup)) { ?>
											<a class="btn btn-success btn-small" href="<?= base_url('akademik/bimbingan/detailKhs_konversi/'.$mhs->NIMHSMSMHS.'/'.$row2->THSMSTRLNM); ?>">
												<i class="btn-icon-only icon-eye-open"></i>
											</a>
										<?php } else { ?>
											<a class="btn btn-success btn-small" href="<?= base_url('akademik/khs/detailKhs_konversi/'.$mhs->NIMHSMSMHS.'/'.$row2->THSMSTRLNM); ?>">
												<i class="btn-icon-only icon-eye-open"></i>
											</a>
										<?php } ?>
									</td>
		                        </tr>
							<?php $no++; } } ?>

							<?php foreach ($detail_khs as $row) { ?>
	                        <tr>
	                        	<td><?= $no; ?></td>
	                        	<?php $semester = $this->app_model->get_semester_khs($mhs->SMAWLMSMHS,$row->THSMSTRLNM); ?>
	                        	<td><?= $semester; ?></td>
	                        	<?php
	                        	$kdpst = trim($row->KDPSTTRLNM);
	                        	if ($row->THSMSTRLNM < '20151') {
	                        		$kode_mk = $this->db->query("SELECT DISTINCT KDKMKTRLNM 
	                        									FROM tbl_transaksi_nilai 
	                        									WHERE NIMHSTRLNM = '{$mhs->NIMHSMSMHS}' 
	                        									AND THSMSTRLNM = '{$row->THSMSTRLNM}")->result();
		                        	$sumsks = 0;
		                        	foreach ($kode_mk as $value1) {
		                        		$sksmk = $this->db->query("SELECT DISTINCT sks_matakuliah 
		                        									FROM tbl_matakuliah_copy 
		                        									WHERE kd_matakuliah = '{$value1->KDKMKTRLNM}' 
		                        									AND kd_prodi = '{$kdpst}'")->row()->sks_matakuliah; 
		                        		$sumsks = $sumsks + $sksmk;
		                        	}
	                        	} else {
	                        		$kode_mk = $this->db->query("SELECT DISTINCT kd_matakuliah 
	                        									FROM tbl_krs 
	                        									WHERE kd_krs LIKE CONCAT('{$mhs->NIMHSMSMHS}','".$row->THSMSTRLNM."%')")->result();
		                        	$sumsks = 0;
		                        	foreach ($kode_mk as $value1) {
		                        		$sksmk = $this->db->query("SELECT DISTINCT sks_matakuliah 
		                        									FROM tbl_matakuliah 
		                        									WHERE kd_matakuliah = '{$value1->kd_matakuliah}' 
		                        									AND kd_prodi = '{$kdpst}'")->row()->sks_matakuliah; 
		                        		$sumsks = $sumsks + $sksmk;
		                        	}
	                        	}
	                        	?>
                                <td><?= $sumsks; ?></td>
                                <?php 
                                $logged = $this->session->userdata('sess_login');
			                    $grup = get_group($logged['id_user_group']);

			                    if ((in_array(5, $grup)) || (in_array(7, $grup))) {
									$prodi = $mhs->KDPSTMSMHS;
								} elseif (in_array(10, $grup)) {
									$prodi = $mhs->KDPSTMSMHS;
								} else {
									$prodi = $logged['userid'];
								}

			                    $ips = $this->krs_model->countips($prodi, $mhs->NIMHSMSMHS, $row->THSMSTRLNM); ?>

                                <?php $statusVerifikasi = $this->db->query("SELECT status_verifikasi FROM tbl_verifikasi_krs WHERE kd_krs 
                                											LIKE CONCAT('{$mhs->NIMHSMSMHS}','{$row->THSMSTRLNM}%')")->row()->status_verifikasi; 

                                $flag_dispensation = $statusVerifikasi == 10 ? '-' : number_format($ips, 2); ?>

                                <td><?= $flag_dispensation; ?></td>
	                        	<td class="td-actions">
	                        		<?php if (in_array(7, $grup) || in_array(6, $grup)) { ?>
										<a 
											class="btn btn-success btn-small" 
											href="<?= base_url('akademik/bimbingan/detailKhs/'.$mhs->NIMHSMSMHS.'/'.$row->THSMSTRLNM); ?>"><i class="btn-icon-only icon-ok"> </i>
										</a>

									<?php } else { ?>
										<a 
											class="btn btn-success btn-small" 
											href="<?= base_url('akademik/khs/detailKhs/'.$mhs->NIMHSMSMHS.'/'.$row->THSMSTRLNM); ?>">
											<i class="btn-icon-only icon-eye-open"> </i>
										</a>
									<?php } ?>
								</td>
	                        </tr>
							<?php $no++; } ?>
	                    </tbody>
	               	</table>
				</div>
			</div>
		</div>
	</div>
</div>