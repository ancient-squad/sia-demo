<script type="text/javascript">
$(function () {
    // Create the chart
    $('#kelas2').highcharts({
        chart: {
            type: 'pie'
        },
        title: {
            text: 'Jumlah Kelas Mahasiswa per <?php echo $tahunkelas; ?>'
        },
        subtitle: {
            text: 'Sumber: <a href="http://sia.<?= $this->URL ?>">SIA <?= $this->ORG_NAME ?></a>'
        },
        plotOptions: {
            series: {
                dataLabels: {
                    enabled: true,
                    format: '{point.name}: {point.y:.f} kelas'
                }
            }
        },

        tooltip: {
            headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
            pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.f}jml</b> of total<br/>'
        },
        series: [{
            name: 'Brands',
            // events: {
            //         click : function() {
            //             $('#TEKNIK').load('<?php echo base_url() ?>akademik/chart/jml/$ds->kd_fakultas');
            //         }
            //     }, 
            colorByPoint: true,
            data: [{
                <?php 
                    $baris = count($kelas);
                    $no = 0;
                    foreach ($kelas as $row) {
                        $no++;
                        if ($no == $baris) {
                            echo "name: '".$row->wkt_kls."', y:".$row->jumjum.", drilldown:'".$row->wkt_kls."'}]";
                        } else {
                            echo "name: '".$row->wkt_kls."', y:".$row->jumjum.", drilldown:'".$row->wkt_kls."'}, {";
                        };
                    }
                ?>
        }],
        drilldown: {
            series: [{
                name: 'teknik',
                id: 'TEKNIK',
                data: [
                        <?php
                            $q = $this->db->query('SELECT distinct b.`prodi`,COUNT(NIMHSMSMHS) as jum from tbl_mahasiswa a join tbl_jurusan_prodi b
                                        on b.`kd_prodi`=a.`KDPSTMSMHS` join  tbl_fakultas c
                                        on c.`kd_fakultas`=b.`kd_fakultas` join tbl_verifikasi_krs d
                                        on d.`npm_mahasiswa`=a.`NIMHSMSMHS`
                                        where c.`kd_fakultas`= "2" group by b.`kd_prodi`')->result();
                            $idih = count($q);
                            $no = 0;
                            foreach ($q as $raw) {
                                $no++;
                                if ($no == $idih) {
                                    echo "['".$raw->prodi."',".$raw->jum."]]";
                                } else {
                                    echo "['".$raw->prodi."',".$raw->jum."],";
                                };
                            }
                        ?>
            }, {
                name: 'ekonomi',
                id: 'EKONOMI',
                data: [
                    <?php
                            $q = $this->db->query('SELECT distinct b.`prodi`,COUNT(NIMHSMSMHS) as jum from tbl_mahasiswa a join tbl_jurusan_prodi b
                                        on b.`kd_prodi`=a.`KDPSTMSMHS` join  tbl_fakultas c
                                        on c.`kd_fakultas`=b.`kd_fakultas` join tbl_verifikasi_krs d
                                        on d.`npm_mahasiswa`=a.`NIMHSMSMHS` where c.`kd_fakultas`= "1" group by b.`kd_prodi`')->result();
                            $idih = count($q);
                            $no = 0;
                            foreach ($q as $raw) {
                                $no++;
                                if ($no == $idih) {
                                    echo "['".$raw->prodi."',".$raw->jum."]]";
                                } else {
                                    echo "['".$raw->prodi."',".$raw->jum."],";
                                };
                            }
                        ?>
            }, {
                name: 'hukum',
                id: 'HUKUM',
                data: [
                    <?php
                            $q = $this->db->query('SELECT distinct b.`prodi`,COUNT(NIMHSMSMHS) as jum from tbl_mahasiswa a join tbl_jurusan_prodi b
                                        on b.`kd_prodi`=a.`KDPSTMSMHS` join  tbl_fakultas c
                                        on c.`kd_fakultas`=b.`kd_fakultas` join tbl_verifikasi_krs d
                                        on d.`npm_mahasiswa`=a.`NIMHSMSMHS` where c.`kd_fakultas`= "3" group by b.`kd_prodi`')->result();
                            $idih = count($q);
                            $no = 0;
                            foreach ($q as $raw) {
                                $no++;
                                if ($no == $idih) {
                                    echo "['".$raw->prodi."',".$raw->jum."]]";
                                } else {
                                    echo "['".$raw->prodi."',".$raw->jum."],";
                                };
                            }
                        ?>
            }, {
                name: 'fikom',
                id: 'ILMU KOMUNIKASI',
                data: [
                    <?php
                            $q = $this->db->query('SELECT distinct b.`prodi`,COUNT(NIMHSMSMHS) as jum from tbl_mahasiswa a join tbl_jurusan_prodi b
                                        on b.`kd_prodi`=a.`KDPSTMSMHS` join  tbl_fakultas c
                                        on c.`kd_fakultas`=b.`kd_fakultas` join tbl_verifikasi_krs d
                                        on d.`npm_mahasiswa`=a.`NIMHSMSMHS` where c.`kd_fakultas`= "4" group by b.`kd_prodi`')->result();
                            $idih = count($q);
                            $no = 0;
                            foreach ($q as $raw) {
                                $no++;
                                if ($no == $idih) {
                                    echo "['".$raw->prodi."',".$raw->jum."]]";
                                } else {
                                    echo "['".$raw->prodi."',".$raw->jum."],";
                                };
                            }
                        ?>
            }, {
                name: 'psikologi',
                id: 'PSIKOLOGI',
                data: [
                    <?php
                            $q = $this->db->query('SELECT distinct b.`prodi`,COUNT(NIMHSMSMHS) as jum from tbl_mahasiswa a join tbl_jurusan_prodi b
                                        on b.`kd_prodi`=a.`KDPSTMSMHS` join  tbl_fakultas c
                                        on c.`kd_fakultas`=b.`kd_fakultas` join tbl_verifikasi_krs d
                                        on d.`npm_mahasiswa`=a.`NIMHSMSMHS` where c.`kd_fakultas`= "5" group by b.`kd_prodi`')->result();
                            $idih = count($q);
                            $no = 0;
                            foreach ($q as $raw) {
                                $no++;
                                if ($no == $idih) {
                                    echo "['".$raw->prodi."',".$raw->jum."]]";
                                } else {
                                    echo "['".$raw->prodi."',".$raw->jum."],";
                                };
                            }
                        ?>
            }, {
                name: 'pasca',
                id: 'PASCASARJANA',
                data: [
                    <?php
                            $q = $this->db->query('SELECT distinct b.`prodi`,COUNT(NIMHSMSMHS) as jum from tbl_mahasiswa a join tbl_jurusan_prodi b
                                        on b.`kd_prodi`=a.`KDPSTMSMHS` join  tbl_fakultas c
                                        on c.`kd_fakultas`=b.`kd_fakultas` join tbl_verifikasi_krs d
                                        on d.`npm_mahasiswa`=a.`NIMHSMSMHS` where c.`kd_fakultas`= "6" group by b.`kd_prodi`')->result();
                            $idih = count($q);
                            $no = 0;
                            foreach ($q as $raw) {
                                $no++;
                                if ($no == $idih) {
                                    echo "['".$raw->prodi."',".$raw->jum."]]";
                                } else {
                                    echo "['".$raw->prodi."',".$raw->jum."],";
                                };
                            }
                        ?>
            }]
        }
    });
});
</script>

<div id="kelas2" style="min-width: 310px; height: 400px; max-width: 600px; margin: 0 auto"></div>