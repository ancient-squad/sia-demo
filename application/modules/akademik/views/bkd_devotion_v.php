<fieldset>
	<h3>
		<i class="icon-group"></i> Tambah Data Pengabdian
		<a href="<?=  base_url('akademik/beban_kinerja_dosen/devotion_list') ?>" data-toggle="tooltip" title="lihat rincian pengabdian">
    		<i class="icon-eye-open"></i>
    	</a>
	</h3>
	<br>
    <form class="form-horizontal" id="temporaryform2" method="post">
    	<div class="control-group">
			<label class="control-label">Tahun Akademik</label>
			<div class="controls">
				<select class="form-control span6" name="tahunakademik" id="devYear" required>
					<option disabled selected value="">-- Pilih Tahun Ajaran --</option>
					<?php foreach ($yearAcademic as $key) {
						echo '<option value="'.$key->kode.'">'.$key->tahun_akademik.'</option>  ';
					} ?>
				</select>
			</div>
		</div>

		<div class="control-group">
			<label class="control-label">Jenis Pengabdian</label>
			<div class="controls">
				<select class="form-control span6" name="devotion" id="devotion" required>
					<option disabled selected value="">-- Pilih Jenis --</option>
					<?php foreach ($devotionType as $devotion) {
						echo '<option value="'.$devotion->kode.'-'.$devotion->group_param.'">'.$devotion->nama.'</option>  ';
					} ?>
				</select>
			</div>
		</div>

		<div class="control-group">
			<label class="control-label">Program Pengabdian</label>
			<div class="controls">
				<select class="form-control span6" name="devProgram" id="devProgram" required>
					<option disabled selected value="">-- Pilih Program --</option>
					
				</select>
			</div>
		</div>

		<div class="control-group" id="param-types">
			<label class="control-label">Peran</label>
			<div class="controls">
				<select class="form-control span6" name="devParam" id="devParam" required>
					<option disabled="" selected="" value="">-- Pilih Peran --</option>
					
				</select>
			</div>
		</div>

		<div class="control-group">
			<label class="control-label">Beban SKS</label>
			<div class="controls">
				<input 
					type="text" 
					value="" 
					id="devCredit" 
					readonly="" 
					name="devCredit" 
					class="form-control span6" />
			</div>
		</div>
		
		<center>
			<button class="btn btn-success" id="addDev"><i class="icon-plus"></i> Tambah Pengabdian</button>
		</center>
    </form>

    <hr>

    <form class="form-horizontal" action="<?= base_url(); ?>akademik/beban_kinerja_dosen/add_devotion" method="post">
      <h3><i class="icon icon-list"></i> Daftar Pengabdian</h3>
      <br>
      <table id="example99" class="table table-bordered table-striped">
        <thead>
              <tr> 
                <th>No</th>
                <th>Jenis Pengabdian</th>
                <th>Program Pengabdian</th>
                <th>Tahun Akademik</th>
                <th>Peran / Kategori</th>
                <th>Beban SKS</th>
                <th width="80">Aksi</th>
              </tr>
          </thead>
          <tbody id="appearHeres">
              <tr id="toRemoves">
                <td colspan="7"><i>No data available</i></td>
              </tr>
          </tbody>
      </table>
      <div class="form-actions">
        <input class="btn btn-default btn-primary" type="submit" id="submitBtn" value="Submit" disabled="">
      </div>
    </form>
</fieldset>

<script>
	$(document).ready(function(){
		$('[data-toggle="tooltip"]').tooltip();
		// get kegiatn program
		$('#devotion').change(function(){
			$.post('<?= base_url() ?>akademik/beban_kinerja_dosen/get_devotion_program/'+$(this).val().split('-')[0],{},function(get){
				$('#devProgram').html(get);
			});

			$.post('<?= base_url() ?>akademik/beban_kinerja_dosen/get_devotion_param/'+$(this).val().split('-')[1],{},function(res){
				$('#devParam').html(res);
			});

			if ($(this).val().split('-')[1] === '') {
				$('#param-types').hide();
			} else {
				$('#param-types').show();
			}

			// reset credit column every devotion type changed
			$('#devCredit').val('');
		});

		// get parameter program
		$('#devProgram').change(function(){
			if ($(this).val().split('x')[1] === 'NIL') {
				$('#devCredit').val($(this).val().split('x')[2]);
			} else {
				$('#devCredit').val('');
			}
		});

		$('#devParam,#devProgram').change(function() {
			$.post('<?= base_url() ?>akademik/beban_kinerja_dosen/set_devotion_credit/'+$('#devProgram').val().split('x')[0]+'/'+$('#devParam').val(),{},function(res){
				$('#devCredit').val(res);
			});
		});

	})

	function getTable() {
		$.post('<?= base_url(); ?>akademik/beban_kinerja_dosen/load_devotion_temp_table/', function(data) {
		  	$('#appearHeres').html(data);
		});
	}

	$(function () {
		$('#addDev').click(function (e) {
			if ($('#devProgram,#devotion,#devYear').val() === '') {
				alert('Tidak boleh ada kolom yang dikosongkan!');
				return;
			}

			$('#submitBtn').removeAttr('disabled')
			if ($('.additionalRows').length === 3) {
            	$('#submitBtn').attr("disabled","");
            }

			$.ajax({
				type: 'POST',
				url: '<?= base_url('akademik/beban_kinerja_dosen/temporaryDevotion'); ?>',
				data: $('#temporaryform2').serialize(),
				error: function (xhr, ajaxOption, thrownError) {
					return false;
				},
				success: function () {
					getTable();
					$('#toRemoves').hide();
					$('#devotion,#devProgram,#devParam,#devCredit').val('');
				}
			});
			e.preventDefault();
		});
	});

	function rmData(id) {
	    $.ajax({
	        type: 'POST',
	        url: '<?= base_url('akademik/beban_kinerja_dosen/delete_devotion/');?>'+id,
	        error: function (xhr, ajaxOptions, thrownError) {
	            return false;           
	        },
	        success: function () {
	            getTable();
	            if ($('.additionalRows').length === 1) {
	            	$('#submitBtn').attr("disabled","");
	            } else {
	            	$('#submitBtn').removeAttr("disabled");
	            }
	        }
	    });
	}
</script>