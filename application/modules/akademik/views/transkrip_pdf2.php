<?php 
ob_start();

//var_dump($q);die();

$pdf = new FPDF("P","mm", "A4");
$pdf->AliasNbPages();

$pdf->AddPage();

$pdf->SetMargins(3, 5 ,0);

//$pdf->SetFont('Arial','',18);


$pdf->Ln(0);

$pdf->Cell(200,5,'',0,0,'L');

//$pdf->Ln(1);

$pdf->image(base_url().'/assets/logo.gif',27,7,18);

$pdf->SetFont('Arial','B','',9); 
$pdf->Ln(1);
$pdf->Cell(200,5,'KARTU HASIL STUDI',0,0,'C');
$pdf->Ln(5);
$pdf->Cell(200,5,'FAKULTAS '.$head->fakultas,0,0,'C');
$pdf->Ln(5);
$pdf->Cell(200,5,$this->ORG_NAME,0,0,'C');

$pdf->Ln(3);



$pdf->Ln(5);

$pdf->SetFont('Arial','B',6);

//$pdf->Cell(140,3,'NAMA PERGURUAN TINGGI : '.$this->ORG_NAME,0,0,'L');

//$pdf->Ln(5);
$pdf->SetLeftMargin(8);

$pdf->SetFont('Arial','',6);

$pdf->Cell(20,3,'NAMA',0,0,'L');

$pdf->Cell(3,3,' : ',0,0,'L');

$pdf->Cell(140,3,$head->NMMHSMSMHS,0,0,'L');


$pdf->Ln(4);

$pdf->SetFont('Arial','',6);

$pdf->Cell(20,3,'NPM',0,0,'L');

$pdf->Cell(3,3,' : ',0,0,'L');

$pdf->Cell(140,3,$head->NIMHSMSMHS,0,0,'L');


$pdf->Ln(4);

$pdf->SetFont('Arial','',6);

$pdf->Cell(20,3,'PROGRAM STUDI',0,0,'L');

$pdf->Cell(3,3,' : ',0,0,'L');

$pdf->Cell(140,3,$head->prodi,0,0,'L');

$pdf->Ln(4);

$pdf->SetFont('Arial','',6);

$pdf->Cell(20,3,'JENJANG',0,0,'L');

$pdf->Cell(3,3,' : ',0,0,'L');

$pdf->Cell(140,3,$head->jenjang,0,0,'L');


$pdf->Ln(2);

//$pdf->Cell(290,0,'',1,0,'C');


// $pdf->ln(5);

// $pdf->SetLeftMargin(8);

// $pdf->SetFont('Arial','',12);

// $pdf->Cell(200,8,'DAFTAR HADIR PESERTA KULIAH',1,0,'C');

$pdf->ln(4);

$pdf->SetFont('Arial','B',6);

$pdf->Cell(5,10,'NO','L,T,R,B',0,'C');

$pdf->Cell(12,10,'KODE','L,T,R,B',0,'C');

$pdf->Cell(42,10,'MATA KULIAH','L,T,R,B',0,'C');

$pdf->Cell(8,10,'SEM','L,T,R,B',0,'C');

$pdf->MultiCell(8,5,'SKS (K)','L,T,R,B','C',FALSE);
$pdf->SetXY(83,47);

$pdf->Cell(7,10,'NILAI','L,T,R,B',0,'C');

$pdf->MultiCell(8,5,'BBT (N)','L,T,R,B','C',FALSE);
$pdf->SetXY(98,47);

$pdf->Cell(7,10,'NxK','L,T,R,B',0,'C');


$pdf->Cell(1,10,'','C');


$pdf->Cell(5,10,'NO','L,T,R,B',0,'C');

$pdf->Cell(12,10,'KODE','L,T,R,B',0,'C');

$pdf->Cell(42,10,'MATA KULIAH','L,T,R,B',0,'C');

$pdf->Cell(8,10,'SEM','L,T,R,B',0,'C');

$pdf->MultiCell(8,5,'SKS (K)','L,T,R,B','C',FALSE);
$pdf->SetXY(181,47);

$pdf->Cell(7,10,'NILAI','L,T,R,B',0,'C');

$pdf->MultiCell(8,5,'BBT (N)','L,T,R,B','C',FALSE);
$pdf->SetXY(196,47);

$pdf->Cell(7,10,'NxK','L,T,R,B',0,'C');
$aa = 57;
$pdf->SetY($aa);
$nomor = 1;
foreach ($q as $row) {
	$pdf->SetFont('Arial','',5.5);
	if ($nomor > 35) {
		$pdf->SetY($aa);
		$pdf->Cell(98,6,'','C');
		$aa = $aa+5;
	}
	
	//$pdf->Cell(98,5,'','C');
	$pdf->Cell(5,5,$nomor,'L,T,R,B',0,'C');
	$pdf->Cell(12,5,$row->KDKMKTRLNM,'L,T,R,B',0,'C');
	
	if (strlen ($row->nama_matakuliah) > 32) {
		$dapetX = $pdf->GetX();
		$dapetY = $pdf->GetY();
		$pdf->SetFont('Arial','',5.5);
		$pdf->MultiCell(42,2.5,strtoupper($row->nama_matakuliah),'L,T,R','L',FALSE);
		$pdf->SetXY($dapetX+42,$dapetY);
	} else {
		$pdf->Cell(42,5,strtoupper($row->nama_matakuliah),'L,T,R,B',0,'L');
	}

	$smtr_mk=$this->app_model->get_semester_khs($head->SMAWLMSMHS,$row->THSMSTRLNM);
	$pdf->SetFont('Arial','',6);
	$pdf->Cell(8,5,$smtr_mk,'L,T,R,B',0,'C');
	$pdf->Cell(8,5,number_format($row->sks_matakuliah),'L,T,R,B',0,'C');
	$pdf->Cell(7,5,$row->NLAKHTRLNM,'L,T,R,B',0,'C');
	$pdf->Cell(8,5,$row->BOBOTTRLNM,'L,T,R,B',0,'C');
	$nk = ($row->sks_matakuliah*$row->BOBOTTRLNM);
	$pdf->Cell(7,5,$nk,'L,T,R,B',0,'C');

	if ($row->NLAKHTRLNM == 'T') {
		$tskss = $tskss;
		$tnl = $tnl;
	} else {
		$tskss = $tskss + $row->sks_matakuliah;
		$tnl = $tnl + $nk;
	}
	
	// $tskss=$tskss+$row->sks_matakuliah;
	// $tnl = $tnl+$nk;
	
	$pdf->Ln();
	$nomor++;

	if ($nomor > 70) {
		$pdf->AliasNbPages();

		$pdf->AddPage();

		$pdf->SetMargins(3, 5 ,0);

		$pdf->Ln(0);

		$pdf->Cell(200,5,'',0,0,'L');

		$pdf->image(base_url().'/assets/logo.gif',27,7,18);

		$pdf->SetFont('Arial','B','',9); 
		$pdf->Ln(1);
		$pdf->Cell(200,5,'KARTU HASIL STUDI',0,0,'C');
		$pdf->Ln(5);
		$pdf->Cell(200,5,'FAKULTAS '.$head->fakultas,0,0,'C');
		$pdf->Ln(5);
		$pdf->Cell(200,5,$this->ORG_NAME,0,0,'C');

		$pdf->Ln(3);

		$pdf->Ln(5);

		$pdf->SetFont('Arial','B',6);

		$pdf->SetLeftMargin(8);

		$pdf->SetFont('Arial','',6);

		$pdf->Cell(20,3,'NAMA',0,0,'L');

		$pdf->Cell(3,3,' : ',0,0,'L');

		$pdf->Cell(140,3,$head->NMMHSMSMHS,0,0,'L');

		$pdf->Ln(4);

		$pdf->SetFont('Arial','',6);

		$pdf->Cell(20,3,'NPM',0,0,'L');

		$pdf->Cell(3,3,' : ',0,0,'L');

		$pdf->Cell(140,3,$head->NIMHSMSMHS,0,0,'L');


		$pdf->Ln(4);

		$pdf->SetFont('Arial','',6);

		$pdf->Cell(20,3,'PROGRAM STUDI',0,0,'L');

		$pdf->Cell(3,3,' : ',0,0,'L');

		$pdf->Cell(140,3,$head->prodi,0,0,'L');

		$pdf->Ln(4);

		$pdf->SetFont('Arial','',6);

		$pdf->Cell(20,3,'JENJANG',0,0,'L');

		$pdf->Cell(3,3,' : ',0,0,'L');

		$pdf->Cell(140,3,$head->jenjang,0,0,'L');


		$pdf->Ln(2);


		$pdf->ln(4);

		$pdf->SetFont('Arial','B',6);

		$pdf->Cell(5,10,'NO','L,T,R,B',0,'C');

		$pdf->Cell(12,10,'KODE','L,T,R,B',0,'C');

		$pdf->Cell(42,10,'MATA KULIAH','L,T,R,B',0,'C');

		$pdf->Cell(8,10,'SEM','L,T,R,B',0,'C');

		$pdf->MultiCell(8,5,'SKS (K)','L,T,R,B','C',FALSE);
		$pdf->SetXY(83,47);

		$pdf->Cell(7,10,'NILAI','L,T,R,B',0,'C');

		$pdf->MultiCell(8,5,'BBT (N)','L,T,R,B','C',FALSE);
		$pdf->SetXY(98,47);

		$pdf->Cell(7,10,'NxK','L,T,R,B',0,'C');


		$pdf->Cell(1,10,'','C');


		$pdf->Cell(5,10,'NO','L,T,R,B',0,'C');

		$pdf->Cell(12,10,'KODE','L,T,R,B',0,'C');

		$pdf->Cell(42,10,'MATA KULIAH','L,T,R,B',0,'C');

		$pdf->Cell(8,10,'SEM','L,T,R,B',0,'C');

		$pdf->MultiCell(8,5,'SKS (K)','L,T,R,B','C',FALSE);
		$pdf->SetXY(181,47);

		$pdf->Cell(7,10,'NILAI','L,T,R,B',0,'C');

		$pdf->MultiCell(8,5,'BBT (N)','L,T,R,B','C',FALSE);
		$pdf->SetXY(196,47);

		$pdf->Cell(7,10,'NxK','L,T,R,B',0,'C');
		$aa = 57;
		$pdf->SetY($aa);
		$nomor = 1;
	}
}

//$no=1;
// $tskss = 0;
// $tnl = 0
// $noo=0;
// foreach ($q as $row) {
// 	$noo++;
// 	if ($noo > 35) {
// 		//$noo =0;
// 		$pdf->SetFont('Arial','',5);
// 		$pdf->SetY($aa);
// 		$pdf->Cell(98,5,'','C');
// 		$pdf->Cell(5,5,$noo,'L,T,R,B',0,'C');
// 		$pdf->Cell(12,5,$row->KDKMKTRLNM,'L,T,R,B',0,'C');
// 		$pdf->Cell(42,5,$row->nama_matakuliah,'L,T,R,B',0,'L');

// 		//$smtr_mk=$this->app_model->get_semester_khs($head->SMAWLMSMHS,$row->THSMSTRLNM);

// 		$pdf->Cell(8,5,$smtr_mk,'L,T,R,B',0,'C');
// 		$pdf->Cell(8,5,$row->sks_matakuliah,'L,T,R,B',0,'C');
// 		$pdf->Cell(7,5,$row->NLAKHTRLNM,'L,T,R,B',0,'C');
// 		$pdf->Cell(8,5,$row->BOBOTTRLNM,'L,T,R,B',0,'C');
// 		$nk = ($row->sks_matakuliah*$row->BOBOTTRLNM);
// 		$pdf->Cell(7,5,$nk,'L,T,R,B',0,'C');

// 		$tskss=$tskss+$row->sks_matakuliah;
// 		$tnl = $tnl+$nk;
		
// 		$pdf->ln();
// 		$aa = $aa + 5;
// 	} else {
// 		$pdf->ln();
// 		$pdf->SetFont('Arial','',5);
// 		$pdf->Cell(5,5,$noo,'L,T,R,B',0,'C');
// 		$pdf->Cell(12,5,$row->KDKMKTRLNM,'L,T,R,B',0,'C');
// 		$pdf->Cell(42,5,$row->nama_matakuliah,'L,T,R,B',0,'L');
		
// 		//$smtr_mk=$this->app_model->get_semester_khs($head->SMAWLMSMHS,$row->THSMSTRLNM);

// 		$pdf->Cell(8,5,$smtr_mk,'L,T,R,B',0,'C');
// 		$pdf->Cell(8,5,$row->sks_matakuliah,'L,T,R,B',0,'C');
// 		$pdf->Cell(7,5,$row->NLAKHTRLNM,'L,T,R,B',0,'C');
// 		$pdf->Cell(8,5,$row->BOBOTTRLNM,'L,T,R,B',0,'C');
// 		$nk = ($row->sks_matakuliah*$row->BOBOTTRLNM);
// 		$pdf->Cell(7,5,$nk,'L,T,R,B',0,'C');
		
// 		$tskss=$tskss+$row->sks_matakuliah;
// 		$tnl = $tnl+$nk;
// 	}

// } 

	$pdf->SetY(54+(35*5));
	$pdf->ln(14);
	$pdf->SetFont('Arial','',6);
	$pdf->Cell(8,5,'*BBT','',0,'L');
	$pdf->Cell(3,5,'=','',0,'C');
	$pdf->Cell(42,5,'Bobot Nilai','',0,'L');

	$pdf->ln(3);
	$pdf->SetFont('Arial','',6);
	$pdf->Cell(8,5,'*NxK','',0,'L');
	$pdf->Cell(3,5,'=','',0,'C');
	$pdf->Cell(42,5,'Bobot Nilai x Kredit','',0,'L');

	$pdf->ln(5);
	$pdf->SetFont('Arial','',6);
	$pdf->Cell(25,5,'SKS yang telah di tempuh','',0,'L');
	$pdf->Cell(3,5,':','',0,'C');
	$pdf->Cell(42,5,$tskss,'',0,'L');
	$pdf->ln(3);
	$pdf->SetFont('Arial','',6);
	$pdf->Cell(25,5,'Indeks Prestasi Kumulatif','',0,'L');
	$pdf->Cell(3,5,':','',0,'C');
	$pdf->Cell(42,5,number_format($tnl/$tskss,2),'',0,'L');
	

	$ttd = $this->db->query('SELECT prd.`prodi`,f.`fakultas` FROM tbl_mahasiswa a 
							JOIN tbl_jurusan_prodi prd ON a.`KDPSTMSMHS` = prd.`kd_prodi`
							JOIN tbl_fakultas f ON prd.`kd_fakultas` = f.`kd_fakultas`
							WHERE a.`NIMHSMSMHS` = "'.$npm.'"')->row();

	$pdf->ln(14);
	$pdf->SetXY(155,237);
	$pdf->SetFont('Arial','',6);
	$pdf->Cell(8,5,'Bekasi,'. date('d-m-Y'),'',0,'C');

	// $pdf->ln(2);
	// $pdf->SetXY(155,240);
	// $pdf->SetFont('Arial','',6);
	// $pdf->Cell(8,5,'a.n DEKAN FAKULTAS '.$head->fakultas.'','',0,'C');

	$pdf->ln(2);
	$pdf->SetXY(155,241);
	$pdf->SetFont('Arial','',6);
	$pdf->Cell(8,5,'KETUA PROGRAM STUDI '.$head->prodi.'','',0,'C');

	$pdf->ln(15);
	$pdf->SetXY(155,255);
	$pdf->SetFont('Arial','',6);
	$pdf->Cell(8,5,'........................................','',0,'C');


$pdf->Output('TRANSKRIP.PDF','I');

?>