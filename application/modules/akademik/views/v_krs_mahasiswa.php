<script type="text/javascript">
$(document).ready(function(){
	var table = $('#tabel_krs');
	var oTable = table.dataTable({
		"bLengthChange": false,
        "bFilter": false, 
		"bInfo": false,
		"bPaginate": false,
		"bSort": false,
	});
		
	var hari = [];
		hari[1] = 'Senin'; 
		hari[2] = 'Selasa';
		hari[3] = 'Rabu';
		hari[4] = 'Kamis';
		hari[5] = 'Jumat';
		hari[6] = 'Sabtu';
		hari[7] = 'Minggu';
	
		
	var table_jadwal = $('#tabel_jadwal');	
	var oTable_jadwal = table_jadwal.dataTable({
		"aoColumnDefs": [{ "bVisible": false, "aTargets": [5,6] }],
		"bLengthChange": false,
        "bFilter": false, 
		"bInfo": false,
		"bPaginate": false,
		"bSort": false,
	});
		oTable.on('click', 'tbody tr .edit', function() {
			var nRow = $(this).parents('tr')[0];
			var aData = oTable.fnGetData(nRow);
			var kd_matakuliah = aData[1];
			$.ajax({
				url: "<?= base_url('akademik/viewkrs/get_jadwal'); ?>",
				type: "post",
				data: {kd_matakuliah: kd_matakuliah, npm: '<?= $npm ?>'},
				success: function(d) {
					var parsed = JSON.parse(d);
                    var arr = [];
                    for (var prop in parsed) {
                        arr.push(parsed[prop]);
                    }
                    oTable_jadwal.fnClearTable();
                    oTable_jadwal.fnDeleteRow(0);
                    for (var i = 0; i < arr.length; i++) {
                        oTable_jadwal.fnAddData([hari[arr[i]['hari']],arr[i]['kelas'],arr[i]['waktu_mulai'].substring(0, 5)+'/'+arr[i]['waktu_selesai'].substring(0, 5), arr[i]['kode_ruangan'],arr[i]['nama'],arr[i]['kd_jadwal'],arr[i]['kd_matakuliah'],arr[i]['kuota'],arr[i]['jumlah']]);
                    }
					$('#jadwal').modal('show');
				}
			});	
		});
		
		oTable_jadwal.on( 'click', 'tr', function () {
        if ( $(this).hasClass('active') ) {
            $(this).removeClass('active');
        }
        else {
            table_jadwal.$('tr.active').removeClass('active');
            $(this).addClass('active');
			var aData = oTable_jadwal.fnGetData(this);
			$('#kd_jadwal').val(aData[5]);
			$('#kd_matakuliah').val(aData[6]);
        }
    } );
	});
</script>
<div class="row">
	<div class="span12">      		  		
  		<div class="widget ">
  			<div class="widget-header">
  				<i class="icon-user"></i>
  				<h3>Data Kartu Rencana Studi <b><?= $nama['NMMHSMSMHS'].' ('.$nama['NIMHSMSMHS'].')'; ?> </b></h3>
			</div> <!-- /widget-header -->
			
			<div class="widget-content" style="overflow:auto;">
				<div class="span11">				<table>
						<tr>
							<td>Pembimbing Akademik</td>
							<td> : <?= $pembimbing['nama']; ?></td>
							<td></td>
						</tr>
						<tr>
							<td>Semester</td>
							<td> : <?= $smt['semester_krs']; ?></td>
							<td></td>
						</tr>
						<tr>
							<td>Tahun akademik</td>
							<td> : <?= $akad; ?></td>
							<td></td>
						</tr>
					</table>
					<hr>
					<table id="tabel_krs" class="table table-bordered table-striped">
	                	<thead>
	                        <tr> 
	                        	<th>No</th>
	                        	<th>Kode MK</th>
	                        	<th>Mata Kuliah</th>
	                        	<th>SKS</th>								
	                        	<th>Dosen</th>
								<th>Hari/Waktu</th>
								<th>Ruangan</th>
								<th width="40">Pilih Jadwal</th>
								<th width="40">Kosongkan Jadwal</th>

								<?php if ($usergroup == 19) { ?>
									<th>Hapus Matakuliah</th>
								<?php } ?>

	                        </tr>
	                    </thead>
	                    <tbody>
                            <?php $no = 1; foreach ($detail_krs as $row) { ?>
	                        <tr>
								<?php
									switch ($row->hari){
										case 1 :
											$hari = 'Senin';
										break;
										case 2 :
											$hari = 'Selasa';
										break;
										case 3 :
											$hari = 'Rabu';
										break;
										case 4 :
											$hari = 'Kamis';
										break;
										case 5 :
											$hari = 'Jumat';
										break;
										case 6 :
											$hari = 'Sabtu';
										break;
										case 7 :
											$hari = 'Minggu';
										break;
										default:
										$hari = '';
									}
								?>
	                        	<td><?= $no; ?></td>
	                        	<td><?= $row->kd_matakuliah; ?></td>
                                <td><?= $row->nama_matakuliah; ?></td>
                                <td><?= $row->sks_matakuliah; ?></td>
                                <td><?= $row->nama; ?></td>
                                <td><?= $hari.' / '.substr($row->waktu_mulai, 0,5).'-'.substr($row->waktu_selesai, 0,5); ?></td>
                                <td><?= $row->kode_ruangan; ?></td>
                                <td class="td-actions">
                                	<a data-toggle="modal" class="btn btn-success btn-small edit">
                                		<i class="btn-icon-only icon-ok"></i>
                                	</a>
                                </td>
                                <td style="text-align: center;">
                                	<a 
                                		onclick="return confirm('Yakin akan menghapus Jadwal ini?')" 
                                		href="<?= base_url('akademik/viewkrs/kosongkan_jadwal/'.$row->id_krs.'/prd'); ?>" 
                                		class="btn btn-danger btn-small">
                                		<i class="btn-icon-only icon-remove"> </i>
                                	</a>
                                </td>

                                <?php if ($usergroup == 19) { ?>
									<td style="text-align: center;">
										<a 
											onclick="return confirm('Yakin akan menghapus data ini?')"
	                                		href="<?= base_url('akademik/viewkrs/removeCourse/'.$row->kd_krs.'/'.$row->kd_matakuliah) ?>" 
	                                		class="btn btn-danger btn-small">
	                                		<i class="btn-icon-only icon-remove"> </i>
	                                	</a>
									</td>
								<?php } ?>

	                        </tr>
	                        <?php $no++; } ?>
	                    </tbody>
	               	</table>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="modal fade" id="jadwal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">PILIH JADWAL MATAKULIAH</h4>
            </div>
            <form class ='form-horizontal' action="<?= base_url(); ?>akademik/viewkrs/save_jadwal" method="post">
                <div class="modal-body">    
					<input type="hidden" id="kd_jadwal" name="kd_jadwal" value=""/>
					<input type="hidden" id="kd_matakuliah" name="kd_matakuliah" value=""/>
					<input type="hidden" id="kd_krs" name="kd_krs" value="<?= $kd_krs; ?>"/>
                    <table id="tabel_jadwal" class="table table-bordered table-striped">
	                  <thead>
	                        <tr> 
	                          <th>Hari</th>
	                          <th>Kelas</th>
	                          <th>Jam</th>
	                          <th>Ruang</th>
							  <th>Dosen</th>
							  <th></th>
							  <th></th>
							  <th>Kuota</th>
							  <th>Jumlah</th>
	                        </tr>
	                    </thead>
	                    <tbody>
	                    	<tr>
	                    		<td></td>
	                    		<td></td>
	                    		<td></td>
	                    		<td></td>
	                    		<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
	                    	</tr>
	                    </tbody>
	                </table>
                </div> 
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <input type="submit" class="btn btn-primary" value="Save changes"/>
                </div>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->