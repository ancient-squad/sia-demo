<!DOCTYPE html>
<html>
<head>
	<title></title>

    <link href="<?php echo base_url();?>assets/css/bootstrap.min.css" rel="stylesheet">

    <link href="<?php echo base_url();?>assets/css/bootstrap-responsive.min.css" rel="stylesheet">

    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet"> 

    <style type="text/css">

    	body {
    		margin: 0;
    	}
    	img.logo {
			width: 18%;
			height: 20%;
    	}
    	
    	.martop {
    		margin-top: 5px;
    	}
    	.table th, .table td {
    		padding: 0;
    	}
    	.font-h1 {
    		font-size: 12px;
    	}

    	table.data tr th,  {
    		border: 1px solid black;
    		vertical-align: middle !important;
    		text-align: center !important;
    		font-size: 10px;
    		font-weight: bold;
    	} 

    	table.data tr td,  { 
    		border: 1px solid #000;
    		font-size: 10px;
    	}
    </style>
</head>
<body>
	<!-- Header -->
	<table class="table table-bordered">
		<tr>
			<td rowspan="8">
				<img class="logo" src="http://172.16.1.5:801/assets/logo.png" alt="" >
			</td>
			<td colspan="6" style="font-size: 22px;text-align: center;"><?= $this->ORG_NAME ?></td>
		<tr>
			<td class="font-h1" align="center" colspan="6" style="font-size: 12px;">KEPUTUSAN KAPOLRI SELAKU KETUA UMUM YAYASAN BRATA BHAKTI POLRI</td>
		</tr>
		<tr>
			<td class="font-h1" align="center" colspan="6">NO.POL : KEP/05/IX/1995/YBB, TANGGAL 18 SEPTEMBER 1995</td>
		</tr>
		<tr>
			<td class="font-h1" align="center" colspan="6">Kampus I : Jl. Darmawangsa I No.1 Kebayoran Baru, Jakarta Selatan</td>
		</tr>
		<tr>
			<td class="font-h1" align="center" colspan="6">Tlp. (021) 72657655, 7231948 Fax: (021) 7267657</td>
		</tr>
		<tr>
			<td class="font-h1" align="center" colspan="6"> : Jl. Maju Terus I No.1 Kebayoran Baru, Jakarta Selatan</td>
		</tr>
		<tr>
			<td class="font-h1" align="center" colspan="6">Tlp. (021) 72657655, 7231948 Fax: (021) 7267657</td>
		</tr>
		<tr>
			<td class="font-h1" align="center" colspan="6" style="margin-top: 0px">website : www.<?= $this->URL ?></td>
		</tr>
	</table>
	<!-- Header -->

	<!-- Header 2 -->
	<table class="table table-bordered">
		<tr>
			<td align="center" style="font-size: 18px;"><u><b>TRANSKRIP AKADEMIK / <i>ACADEMIC TRANSCRIPT</i></b></u></td>
		</tr>
		<tr>
			<td align="center">NOMOR / <i>NUMBER</i>&nbsp;:&nbsp;53646834538</td>
		</tr>
		<tr>
			<td align="center">Nomor Induk Ijazah / <i>National Certificate</i>&nbsp;:&nbsp;32145687983685</td>
		</tr>
	</table>
	<!-- Header 2 -->

	<!-- Header 3 -->
	<?php
		$h1 = [
			'Program Pendidikan Tinggi',
			'Program Studi',
			'Nama',
			'Tempat dan Tanggal Lahir',
			'Nomor Pokok Mahasiswa',
			'Tanggal Kelulusan',
			'Indeks Prestasi Kumulatif',
			'Predikat Kelulusan',
		];
	?>
	<table class="table table-bordered">
		<?php foreach($h1 as $key => $row):?>
		<tr>
			<td width="49%"><?php echo $row?></td>
			<td width="2%">:</td>
			<td width="49%"></td>
		</tr>
		<?php endforeach?>
	</table>
	<!-- Header 3 -->
	
	<!-- Data Table -->
	<table class="data ">
		<thead>
			<tr  style="text-align: center;">
				<th width="5%">No</th>
				<th width="10%">KODE / <i>CODE</i></th>
				<th>MATA KULIAH / <i>SUBJECT</i></th>
				<th width="8%">SKS/ <i>CREDIT</i></th>
				<th width="8%">NILAI/ <i>GRADE</i></th>
			</tr>
		</thead>
		<tbody>
			<?php $i=1; foreach ($matkul as $key => $value): ?>
			<tr>
				<td align="center"><?php echo $i; ?></td>
				<td align="center"><?php echo $value->KDKMKTRLNM?></td>
				<td><?php echo $value->nama_matakuliah?></td>
				<td align="center"><?php echo $value->sks_matakuliah?></td>
				<td align="center"><?php echo $value->NLAKHTRLNM?></td>
			</tr>
			<?php $i++; endforeach?>
		</tbody>
	</table>
	<!-- Data Table -->
</body>
</html>