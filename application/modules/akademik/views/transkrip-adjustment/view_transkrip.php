<link href="https://gitcdn.github.io/bootstrap-toggle/2.2.0/css/bootstrap-toggle.min.css" rel="stylesheet">
<script src="https://gitcdn.github.io/bootstrap-toggle/2.2.0/js/bootstrap-toggle.min.js"></script>

<div class="row">
	<div class="span12">      		  		
  		<div class="widget ">
  			<div class="widget-header">
  				<i class="icon-user"></i>
  				<h3><?php echo $title ?></h3>
			</div> <!-- /widget-header -->
			
			<div class="widget-content">

				<form class="form-horizontal" id="temporaryform" method="post">
	              <div class="control-group">
	                <label class="control-label">Cari Mahasiswa</label>
	                <div class="controls">
	                  <input class="form-control span8" type="text" id="npm" placeholder="Isi NPM Mahasiswa" name="npm" required>
	                  <button class="btn btn-success" id="throw">
	                    <i class="icon icon-search"></i> Lihat Transkrip
	                  </button>
	                </div>
	              </div>
	              <hr>
	            </form>

	            <div class="col">
	            	<div id="load_table"></div>
	            </div>
			</div>
		</div>
	</div>
</div>


<script type="text/javascript">
  jQuery(document).ready(function($) {
    $('input[name^=npm]').autocomplete({
      source: '<?php echo base_url('akademik/transkrip-adjustment/findNpm');?>',
      minLength: 1,
      select: function (evt, ui) {
        this.form.npm.value = ui.item.value;
      }
    });


    // load table
    $("#throw").on('click', function (e) {
    	e.preventDefault();

    	var data =  $('#npm').val().split("-");
    	var npm = data[0]; 

    	$("#load_table").load('<?php echo base_url() ?>' + 'akademik/transkrip-adjustment/viewTranskrip?npm=' + npm.trim())
    })
  });
</script>
