<script type="text/javascript">
$(document).ready(function() {
  $('#mulai').datepicker({dateFormat: 'yy-mm-dd',
                  yearRange: "2015:2025",
                  changeMonth: true,
                  changeYear: true
  });

  $('#ahir').datepicker({dateFormat: 'yy-mm-dd',
                  yearRange: "2015:2025",
                  changeMonth: true,
                  changeYear: true
  });

});
</script>

<script>
    function edit(idk){
        $('#edit').load('<?php echo base_url();?>akademik/kalender/edit_isi_kalender/'+idk);
    }
</script>

<div class="row">
    <div class="span12">                    
        <div class="widget ">
            <div class="widget-header">
                <i class="icon-list"></i>
                <h3>Data Kalender Akademik - <?php echo $ta ?></h3>
            </div> <!-- /widget-header -->
            <div class="widget-content">
                <div class="span11">
                    <?php 
            
                    $logged = $this->session->userdata('sess_login');

                    $pecah = explode(',', $logged['id_user_group']);
                    $jmlh = count($pecah);
                    for ($i=0; $i < $jmlh; $i++) { 
                        $grup[] = $pecah[$i];
                    }?>

                    <a href="<?php echo base_url()?>akademik/kalender" class="btn btn-warning"></i>Kembali</a>
                    <?php if ((in_array(10, $grup))){ //BAA ?>
                        <a data-toggle="modal" href="#myModal" class="btn btn-success"><i class="icon icon-plus"></i>&nbsp; Jadwalkan Kegiatan</a>
                    <?php }elseif ((in_array(1, $grup))){ // admin?>
                        <a href="<?php echo base_url()?>akademik/kalender/approved/<?php echo $kode ?>" onclick="return confirm('Yakin untuk Menyetujui ?');" class="btn btn-info">&nbsp; Menyetujui</a>
                    <?php } ?>

                    
                    <a target="_blank" href="<?php echo base_url()?>akademik/kalender/cetak_isi_kalender/<?php echo $kode ?>" class="btn btn-default"><i class="icon icon-print"></i>&nbsp; Cetak</a>
                    <center><h3>Kalender Akademik Semester <?php if(substr($ta, -1) == 1){echo "Ganjil - ".$ta;}else{echo "Genap- ".$ta;}  ?>  </h3></center><br>
                    <h3>KEGIATAN PRA SEMESTER</h3>
                    <table id="example1" class="table table-bordered table-striped">
                        <thead>
                            <tr> 
                                <th>NO</th>
                                <th>KEGIATAN</th>
                                <th>WAKTU PELAKSANAAN</th>
                                <th>KETERANGAN</th>
                                <th>AKSI</th>
                            </tr>
                        </thead>
                        <tbody>
                           <?php $no=1; foreach ($pra as $isi) { 
                            if ($isi->mulai == $isi->ahir) {
                                $waktu = TanggalIndo($isi->mulai);
                            }else{
                                $waktu = TanggalIndoRange($isi->mulai,$isi->ahir);
                            }

                            ?>
                            <tr>
                                <td><?php echo $no ?></td>
                                <td><?php echo $isi->kegiatan ?></td>
                                <td><?php echo $waktu ?></td>
                                <td><?php echo $isi->keterangan ?></td>
                                <td>
                                    <a class="btn btn-primary btn-small" onclick="edit(<?php echo $isi->id?>)" data-toggle="modal" href="#editModal1" ><i class="btn-icon-only icon-pencil"></i></a>
                                    <a onclick="return confirm('Yakin ingin menghapus kegiatan ini ?');" href="<?php echo base_url(); ?>akademik/kalender/rem_isi_kalender/<?php echo $isi->id; ?>" class="btn btn-danger btn-small edit"><i class="btn-icon-only icon-remove"> </i></a>
                                </td>
                            </tr>
                            <?php $no++; } ?>
                        </tbody>
                    </table>
                    <h3>KEGIATAN SEMESTER</h3>
                    <table id="example2" class="table table-bordered table-striped">
                        <thead>
                            <tr> 
                                <th>NO</th>
                                <th>KEGIATAN</th>
                                <th>WAKTU PELAKSANAAN</th>
                                <th>KETERANGAN</th>
                                <th>AKSI</th>
                            </tr>
                        </thead>
                        <tbody>
                           <?php $no=1; foreach ($acara as $isi) { 
                            if ($isi->mulai == $isi->ahir) {
                                $waktu = TanggalIndo($isi->mulai);
                            }else{
                                $waktu = TanggalIndoRange($isi->mulai,$isi->ahir);
                            }

                            ?>
                            <tr>
                                <td><?php echo $no ?></td>
                                <td><?php echo $isi->kegiatan ?></td>
                                <td><?php echo $waktu ?></td>
                                <td><?php echo $isi->keterangan ?></td>
                                <td>
                                    <a class="btn btn-primary btn-small" onclick="edit(<?php echo $isi->id?>)" data-toggle="modal" href="#editModal1" ><i class="btn-icon-only icon-pencil"></i></a>
                                    <a onclick="return confirm('Yakin ingin menghapus kegiatan ini ?');" href="<?php echo base_url(); ?>akademik/kalender/rem_isi_kalender/<?php echo $isi->id; ?>" class="btn btn-danger btn-small edit"><i class="btn-icon-only icon-remove"> </i></a>
                                </td>
                            </tr>
                            <?php $no++; } ?>
                        </tbody>
                    </table>
                    <h3>KEGIATAN PEMBAYARAN KULIAH SEMESTER</h3>
                    <table id="example3" class="table table-bordered table-striped">
                        <thead>
                            <tr> 
                                <th>NO</th>
                                <th>KEGIATAN</th>
                                <th>WAKTU PELAKSANAAN</th>
                                <th>KETERANGAN</th>
                                <th>AKSI</th>
                            </tr>
                        </thead>
                        <tbody>
                           <?php $no=1; foreach ($biaya as $isi) { 
                            if ($isi->mulai == $isi->ahir) {
                                $waktu = TanggalIndo($isi->mulai);
                            }else{
                                $waktu = TanggalIndoRange($isi->mulai,$isi->ahir);
                            }

                            ?>
                            <tr>
                                <td><?php echo $no ?></td>
                                <td><?php echo $isi->kegiatan ?></td>
                                <td><?php echo $waktu ?></td>
                                <td><?php echo $isi->keterangan ?></td>
                                <td>
                                    <a class="btn btn-primary btn-small" onclick="edit(<?php echo $isi->id?>)" data-toggle="modal" href="#editModal1" ><i class="btn-icon-only icon-pencil"></i></a>
                                    <a onclick="return confirm('Yakin ingin menghapus kegiatan ini ?');" href="<?php echo base_url(); ?>akademik/kalender/rem_isi_kalender/<?php echo $isi->id; ?>" class="btn btn-danger btn-small edit"><i class="btn-icon-only icon-remove"> </i></a>
                                </td>
                            </tr>
                            <?php $no++; } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>


<!-- Mpdal -->
<div class="modal modal-large fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title"> <i class="icon icon-edit"></i> Buat Kalender Akademik</h4>
            </div>
            <div class="modal-body">
                <form class ='form-horizontal' action="<?php echo base_url();?>akademik/kalender/add_isi_kalender" method="post">
                    <div class="control-group">
                  <label class="control-label">Kelompok Kegiatan </label>
                  <div class="controls">
                    <select class="form-control span4" name="kel" required>
                        <option>-- Pilih Kelompok Kegiatan --</option>
                        <option value="1">KEGIATAN PRA SEMESTER</option>
                        <option value="2">KEGIATAN SEMESTER</option>
                        <option value="3">KEGIATAN PEMBAYARAN KULIAH SEMESTER</option>
                    </select>
                  </div>
                </div>
                <div class="control-group">
                  <label class="control-label">Nama Kegiatan </label>
                  <div class="controls">
                    <textarea class="form-control span4" name="kegiatan" placeholder="Masukan Nama Kegiatan" required></textarea>
                  </div>
                </div>
                <div class="control-group">
                  <label class="control-label">Waktu Pelaksanaan </label>
                  <div class="controls">
                    <input class="form-control span2" type="text" placeholder="Waktu Mulai"    name="mulai" id="mulai" required>
                    <input class="form-control span2" type="text" placeholder="Waktu Selesai"  name="ahir" id="ahir" required>
                    <input  type="hidden" value="<?php echo $kode ?>"  name="kode" required>
                  </div>
                </div>
                <div class="control-group">
                  <label class="control-label">Keterangan </label>
                  <div class="controls">
                    <textarea class="form-control span4" name="ket" placeholder="(OPSIONAL)" ></textarea>
                  </div>
                </div>
            </div>
             <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Keluar</button>
                <input type="submit" class="btn btn-primary" value="Simpan"/>
            </div>
            </form>
            </div><!-- /.modal-body -->    
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<!-- Modal -->

<div class="modal fade" id="editModal1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content" id="edit">
            
        </div>
    </div>
</div>