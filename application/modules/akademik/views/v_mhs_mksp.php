<script type="text/javascript">
  function loadjdl(kd) {
    $("#kontenmodal").load('<?php echo base_url() ?>akademik/perbaikan/get_jdl/' + kd);
  }

  function SetSel(elem) {
    var elems = document.getElementsByClassName("chkclass");
    var currentState = elem.checked;
    var elemsLength = elems.length;

    for (i = 0; i < elemsLength; i++) {
      if (elems[i].type === "checkbox") {
        elems[i].checked = false;
      }
    }

    elem.checked = currentState;
  }​
</script>
<script type="text/javascript">
  // jQuery(document).ready(function($) {

  //     $(function(){
  //         $("input:checkbox.chkclass").click(function(){
  //           $("input:checkbox.chkclass").not($(this)).removeAttr("checked");
  //           $(this).attr("checked", $(this).attr("checked"));    
  //         });
  //     });​
  // });
</script>
<div class="row">
  <div class="span12">
    <div class="widget ">
      <div class="widget-header">
        <i class="icon-list-ul"></i>
        <h3>Daftar Matakuliah Perbaikan</h3>
      </div> <!-- /widget-header -->

      <div class="widget-content">
        <form action="<?php echo base_url('akademik/perbaikan/addkrs'); ?>" method="post" accept-charset="utf-8">
          <div class="span11">
            <a href="#info" class="btn btn-warning" data-toggle="modal" title=""><i class="icon icon-info"></i> Informasi</a>
            <br><br>
            <table id="" class="table table-bordered table-striped">
              <thead>
                <tr style="text-align:center;">
                  <th rowspan="2" style="text-align:center;vertical-align:middle">No</th>
                  <th rowspan="2" style="text-align:center;vertical-align:middle">Kode Matakuliah</th>
                  <th rowspan="2" style="text-align:center;vertical-align:middle">Nama Matakuliah</th>
                  <th rowspan="2" style="text-align:center;vertical-align:middle">SKS</th>
                  <th rowspan="2" style="text-align:center;vertical-align:middle">Nilai</th>
                  <th width="80" colspan="5" style="text-align:center;">Jadwal</th>
                  <th rowspan="2" width="50" style="vertical-align:middle">Pilih MK</th>
                </tr>
                <tr>
                  <th style="text-align:center;">Hari</th>
                  <th style="text-align:center;">Kelas</th>
                  <th style="text-align:center;">Jam</th>
                  <th style="text-align:center;">Ruang</th>
                  <th style="text-align:center;">Dosen</th>
                </tr>
              </thead>
              <tbody>
                <?php // echo $nim; 
                ?>
                <?php $no = 1;
                foreach ($filter as $row) { ?>
                  <tr>
                    <td><?php echo number_format($no); ?></td>
                    <td><?php echo $row->kd_matakuliah; ?></td>
                    <td><?php echo $row->nama_matakuliah; ?></td>
                    <td><?php echo $row->sks_matakuliah; ?></td>
                    <td><?php echo $row->NLAKHTRLNM; ?></td>
                    <td align="center"><?php echo notohari($row->hari); ?></td>
                    <td><?php echo $row->kelas; ?></td>
                    <td><?php echo $row->waktu_mulai; ?></td>
                    <?php $rg = $this->temph_model->get_ruang($row->kd_ruangan)->row(); ?>
                    <td><?php echo $rg->ruangan; ?></td>
                    <td><?php echo $row->nama; ?></td>
                    <td style="text-align:center;"><input type="checkbox" class="chkclass" onclick="SetSel(this)" name="kdjdl[]" value="<?php echo $row->kd_jadwal; ?>"></td>
                  </tr>
                <?php $no++;
                } ?>
              </tbody>
            </table>
            <hr>
            <?php
            $aktif = $this->setting_model->getaktivasi('kls_per')->result();
            if (count($aktif) != 0) { ?>
              <button type="submit" class="btn btn-primary" title=""><i class="icon icon-ok"></i> Submit Formulir</button>
            <?php } ?>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="info" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content" id="kontenmodal">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title">Informasi Kelas Perbaikan</h4>
      </div>
      <div class="modal-body">
        <p>
          Daftar yang ditampilkan adalah data matakuliah anda yang <strong style="color:red;">belum memenuhi syarat minimal nilai kelulusan</strong>. <br>
          Nilai yang belum memenuhi syarat minimal kelulusan adalah <strong style="color:red;">C, D, dan E</strong>.
          <br>
          Nilai maksimal hasil dari kelas perbaikan adalah B
          Jika jumlah data matakuliah yang ditampilkan <strong style="color:red;">tidak sesuai</strong> dengan jumlah data matakuliah anda yang belum memenuhi syarat minimal nilai kelulusan,
          maka dapat menghubungi <strong style="color:red;">Program Studi terkait</strong> untuk mengkonfirmasi. <br>
          Berikut alur pendaftaran kelas perbaikan : <br>
          <ul>
            <li>Pilih matakuliah yang akan diajukan untuk kelas perbaikan (di halaman ini)</li>
            <li>Submit Formulir</li>
            <li>Cetak Formulir</li>
            <li>Konfirmasi ke Biro Perencanaan Keuangan (RENKEU)</li>
            <li>Cetak KRS</li>
          </ul>
        </p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Keluar</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->