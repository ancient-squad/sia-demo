<div class="row">

    <div class="span12">

        <div class="widget ">

            <div class="widget-header">

                <i class="icon-user"></i>

                <h3>Semester Pendek</h3>

            </div> <!-- /widget-header -->



            <div class="widget-content">

                <div class="span11">

                    <form method="post" class="form-horizontal" action="<?php echo base_url(); ?>akademik/perbaikan/session_create">

                        <fieldset>

                            <div class="control-group">

                                <label class="control-label">Tahun Akademik</label>

                                <div class="controls">

                                    <select class="form-control span6" name="tahunajaran" id="tahunajaran" required>

                                        <option disabled selected>--Pilih Tahun Akademik--</option>

                                        <?php
                                        $last = yearBefore();
                                        $yr = $last + 2;
                                        $nameta = $this->app_model->getdetail('tbl_tahunakademik', 'kode', $last, 'kode', 'asc')->row()->tahun_akademik;
                                        $category = explode(' - ', $nameta);
                                        ?>
                                        <option value="<?= $yr ?>">
                                            <?= substr($nameta, 0, 9) ?> Kelas Perbaikan - <?= $category[1] ?>
                                        </option>

                                        <?php foreach ($select as $key) {
                                            $ta = substr($key->kode, -1);

                                            if ($ta == '1') {
                                                $ta = substr($key->kode, 0, -1);
                                                $ta_sp = $ta . '3';
                                                echo '<option value="' . $ta_sp . '">' . substr($key->tahun_akademik, 0, 9) . ' Kelas Perbaikan - Ganjil </option>';
                                            } elseif ($ta == '2') {
                                                $ta = substr($key->kode, 0, -1);
                                                $ta_sp = $ta . '4';
                                                echo '<option value="' . $ta_sp . '">' . substr($key->tahun_akademik, 0, 9) . ' Kelas Perbaikan - Genap </option>';
                                            }
                                            //$ta_sp_old = $ta_sp;
                                        }

                                        ?>



                                    </select>
                                </div>

                            </div>


                            <div class="form-actions">

                                <input type="submit" class="btn btn-large btn-success" value="Cari" />

                            </div> <!-- /form-actions -->

                        </fieldset>

                    </form>
                </div>

            </div>

        </div>

    </div>

</div>