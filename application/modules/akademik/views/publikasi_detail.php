<?php // var_dump($rows);die(); ?>
<script>
function uts(id){
//alert('kaskus UTS');
$('#absen').load('<?php echo base_url();?>akademik/publikasi/show_nilai_uts/'+id);
}

function uas(id){
//alert('kaskus');
	$('#absen').load('<?php echo base_url();?>akademik/publikasi/show_nilai_uas/'+id);
}

function all(id){
	$('#absen').load('<?php echo base_url();?>akademik/publikasi/pub_all/'+id);
}

function jumlahpeserta(id)
{
	$.post('<?= base_url('akademik/publikasi/jumlahpeserta/') ?>'+id, function(respon){
		$('#amount').modal('show');
		var parse = JSON.parse(respon);
		$('#showhere').text(parse.jum+' peserta');
		$('#titles').text(parse.kd_matakuliah);
	});
}

</script>

<div class="row">

	<div class="span12">      		  		

  		<div class="widget">

  			<div class="widget-header">

  				<i class="icon-share"></i>

  				<h3>Data Publikasi Nilai</h3>

			</div> <!-- /widget-header -->

			

			<div class="widget-content">

				<div class="span11">

					<a href="<?php echo base_url(); ?>akademik/publikasi" class="btn btn-warning pull-left">
						<i class="icon-chevron-left"></i> Kembali
					</a>

					<table id="example8" class="table table-bordered table-striped">

	                	<thead>

	                        <tr> 

	                        	<th>No</th>

	                        	<th>Kelas</th>

	                        	<th>Kode Matakuliah</th>

	                        	<th>Mata Kuliah</th>

	                        	<th>SKS</th>

	                        	<th>Dosen</th>

	                        	<th width="40">Publikasi</th> 

	                        </tr>

	                    </thead>

	                    <tbody>

	                    	<?php  $no=1;
	                    	foreach ($rows as $isi) { ?>
	                    		<script type="text/javascript">
	                    			$.post('<?= base_url('akademik/publikasi/statuspublish/'.$isi->id_jadwal) ?>', function(res){
	                    				if (res) {
	                    					$('#stst-<?= $isi->id_jadwal ?>').empty();
	                    					$('#stst-<?= $isi->id_jadwal ?>').append(res);
	                    				}
	                    			});
	                    		</script>
	                    	<tr>
	                    		<td><?= $no; ?></td>
	                    		<td><?= $isi->kelas; ?></td>
	                    		<td><?= $isi->kd_matakuliah; ?></td>
	                    		<td><?= $isi->nama_matakuliah; ?></td>
	                    		<td><?= $isi->sks_matakuliah; ?></td>
	                    		<td><?= $isi->nama; ?></td>
	                    		                   		
	                    		<?php                				
                				
                				$logged = $this->session->userdata('sess_login');
								$grup = get_group($logged['id_user_group']);

								if ( (in_array(8, $grup) || in_array(19, $grup)) && (count($aktif) != 0 ) && ($crud->create > 0)) { ?>
                					<td class="action" id="stst-<?= $isi->id_jadwal ?>">
                						<img src="<?= base_url('assets/img/loader.gif') ?>" style="width: 50%">
                					</td>
                				<?php } else { ?>
                					<td class="action">
		                    			-
		                    		</td>
                				<?php } ?>
                				
	                    	</tr>	

	                    	<?php $no++; } ?>
	                    	
	                    </tbody>

	               	</table>

				</div>

			</div>

		</div>

	</div>

</div>

<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">

    <div class="modal-dialog">

        <div class="modal-content" id="absen">

            

        </div><!-- /.modal-content -->

    </div><!-- /.modal-dialog -->

</div><!-- /.modal -->

<div class="modal fade" id="amount" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">

    <div class="modal-dialog">

        <div class="modal-content" id="">

            <div class="modal-header">
		        <button type="button" class="close" data-dismiss="modal">&times;</button>
		        <h4 class="modal-title" id="titles"></h4>
		    </div>
		    <div class="modal-body">
		        <h1 id="showhere"></h1>
		    </div>
		    <div class="modal-footer">
		        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
		    </div>

        </div><!-- /.modal-content -->

    </div><!-- /.modal-dialog -->

</div><!-- /.modal -->