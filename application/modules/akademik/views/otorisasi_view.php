

<div class="row">
  <div class="span12">                
      <div class="widget ">
        <div class="widget-header">
          <i class="icon-user"></i>
          <h3>Otorisasi Kartu Ujian Biro Administrasi Akademik</h3>
      </div> <!-- /widget-header -->
      
      <div class="widget-content">
         <div class="tabbable">
            <ul class="nav nav-tabs">
              <li class="active"><a href="#banyak" data-toggle="tab">Cetak Banyak</a></li>
               
              <li><a href="#satuan" data-toggle="tab">Cetak Satuan</a></li>
             
            </ul>           
            <br>

                        
              <div class="tab-content">
               
                  <div class="tab-pane active" id="banyak">
                    <div class="span11">
                      <form method="post" class="form-horizontal" name="formcek" action="<?php echo base_url(); ?>akademik/otorisasi/simpan_sesi">
                      <fieldset>
                      <script>
                              $(document).ready(function(){
                                $('#faks').change(function(){
                                  $.post('<?php echo base_url()?>akademik/otorisasi/get_jurusan/'+$(this).val(),{},function(get){
                                    $('#jurs').html(get);
                                  });
                                });
                              });
                              </script>
                              <div class="control-group">
                                <label class="control-label">Fakultas</label>
                                <div class="controls">
                                  <select class="form-control span6" name="fakultas" id="faks">
                                    <option>--Pilih Fakultas--</option>
                                    <?php foreach ($fakultas as $row) { ?>
                                    <option value="<?php echo $row->kd_fakultas;?>"><?php echo $row->fakultas;?></option>
                                    <?php } ?>
                                  </select>
                                </div>
                              </div>
                              

                              <div class="control-group">
                                <label class="control-label">Program Studi</label>
                                <div class="controls">
                                  <select class="form-control span6" name="jurusan" id="jurs">
                                    <option>--Pilih Program Studi--</option>
                                  </select>
                                </div>
                              </div>

                              <div class="control-group">
                                <label class="control-label">Angkatan</label>
                                <div class="controls">
                                  <select class="form-control span6" name="angkatan" id="tahun">
                                    <option>--Pilih Angkatan--</option>
                                    <?php $tahun= date('Y'); $awal = $tahun-9; for ($i=$awal; $i <= $tahun; $i++) { 
                                      echo "<option value='".$i."'>".$i."</option>";
                                    }; ?>
                                  </select>
                                </div>
                              </div>

                              <div class="control-group">
                                <label class="control-label">Tahun Akademik</label>
                                <div class="controls">
                                  <select class="form-control span6" name="tahunajaran" id="tahunajaran">
                                    <option>--Pilih Tahun Akademik--</option>
                                    <?php foreach ($tahunajar as $row) { ?>
                                    <option value="<?php echo $row->kode;?>"><?php echo $row->tahun_akademik;?></option>
                                    <?php } ?>
                                  </select>
                                </div>
                              </div>  
                            <br/>
                              
                            <div class="form-actions">
                                <input type="submit" class="btn btn-large btn-success" 
                                value="Cari" onClick="validFaks();validJurs();validTahun();validTa();"/> 
                            </div> <!-- /form-actions -->
                        </fieldset>
                    </form>
                </div>
              </div>
              <div class="tab-pane" id="satuan">
                <div class="span11">
                  <form class="form-horizontal" action="<?php echo base_url(); ?>akademik/otorisasi/cetak_satuan_tab" method="post">
                    <fieldset>
                      <div class="control-group">
                        <label class="control-label">NPM Mahasiswa </label>
                        <div class="controls">
                          <input class="form-control span3" placeholder="Isi dengan NPM Mahasiswa"  type="text"  name="npm" required="">
                        </div>
                      </div>

                      <div class="control-group">
                        <label class="control-label">Jenis Kartu</label>
                        <div class="controls">
                          <select class="form-control span3"  name="jenis">
                            <option disabled="" selected="">--Pilih Form--</option>
                            <option value="1"  >UTS</option>
                            <option value="2" >UAS</option>
                          </select>
                        </div>
                      </div>

                      <div class="control-group">
                        <label class="control-label">Tahun Ajaran</label>
                        <div class="controls">
                          <select class="form-control span4" name="tahun" id="">
                            <option>--Pilih Tahun Akademik--</option>
                            <?php foreach ($tahunajar as $row) { ?>
                            <option value="<?php echo $row->kode;?>"><?php echo $row->tahun_akademik;?></option>
                            <?php } ?>
                          </select>
                        </div>
                      </div>
                      
                      
                      <div class="form-actions">
                        <input class="btn btn-large btn-primary" type="submit" value="Submit">
                        <input class="btn btn-large btn-default" type="reset" value="Clear">
                      </div>
                    </fieldset>
                  </form>
                </div>
              </div>
            </div>
        
      </div>
        
          
        </div>
      </div>
    </div>
  </div>
</div>
 <script type="text/javascript">

// function validFaks()
// {
//   if (document.formcek.faks.value == "") 
//   {
//     alert("Fakultas masih kosong");
//   }
// }

// function validJurs()
// {
//     if (document.formcek.jurs.value == "") 
//     {
//       alert("Jurusan masih kosong");
//     }
// }

// function validTahun()
// {
//     if (document.formcek.tahun.value == "") 
//     {
//       alert("Jurusan masih kosong");
//     }
// }

// function validTa()
// {
//     if (document.formcek.tahunajaran.value == "") 
//     {
//       alert("Jurusan masih kosong");
//     }
// }

 </script>