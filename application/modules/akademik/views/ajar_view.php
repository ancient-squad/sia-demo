<script>
    function edit(id){
        $('#absen').load('<?= base_url();?>akademik/ajar/view_absen/'+id);
    }
</script>

<div class="row">
    <div class="span12">                    
        <div class="widget ">
            <div class="widget-header">
                <a 
                    href="<?= $back_url ?>" 
                    class="btn btn-default"
                    data-toggle="tooltip"
                    title="kembali"
                    style="margin-left: 10px">
                    <i class="icon-chevron-left" style="margin-left: 0"></i>
                </a>
                <h3>Data Kegiatan Mengajar <?= nama_dsn($nid).' Tahun ajaran '.get_thnajar($tahunakademik) ?></h3>
            </div> <!-- /widget-header -->
            
            <div class="widget-content">
                <div class="span11">
                    <table id="example1" class="table table-bordered table-striped">
                        <thead>
                            <tr> 
                                <th>Kode MK</th>
                                <th>Mata Kuliah</th>
                                <th>SKS</th>
                                <th>Hari/Ruang</th>
                                <th>Waktu</th>
                                <th>Kelas</th>
                                <th width="80">Jumlah Peserta</th>
                                <?php $wdt = (in_array(7, $group) || in_array(6, $group)) ? 111 : 80; ?>
                                <th width="<?= $wdt ?>">Berita Acara</th>
                                <?php $width = in_array(19, $group) || in_array(8, $group) ? 80 : 40 ?>
                                <th width="<?= $width ?>">Absensi</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $no=1; foreach ($rows as $isi): ?>
                                <tr>
                                    <td><?= $isi->kd_matakuliah; ?></td>
                                    <td><?= $isi->nama_matakuliah; ?></td>
                                    <td><?= $isi->sks_matakuliah; ?></td>
                                    <td><?= notohari($isi->hari).' / '.get_room($isi->kd_ruangan); ?></td>
                                    <td><?= del_ms($isi->waktu_mulai).' - '.del_ms($isi->waktu_selesai); ?></td>
                                    <td><?= $isi->kelas;?></td>
                                    <td><?= $isi->jumlah_peserta; ?></td>
                                    <td>
                                        <?php if ((in_array(7, $group) || in_array(6, $group)) && is_null($isi->mk_ta)) : ?>
                                            <a 
                                                href="<?= base_url('akademik/ajar/berita-acara-ujian/'.$isi->kd_matakuliah.'/'.rawurlencode($isi->kelas) ); ?>" 
                                                target="_blank" data-toggle="tooltip" class="btn btn-info btn-small" 
                                                title="Buat Berita Acara"><i class="icon icon-plus"></i>
                                            </a>
                                        <?php endif; ?>
                                        <a 
                                            href="<?= base_url('akademik/absenDosen/event/'.$isi->id_jadwal); ?>" 
                                            target="_blank" class="btn btn-info btn-small" data-toggle="tooltip"
                                            title="cetak berita acara mengajar"><i class="btn-icon-only icon-print"></i>
                                        </a>
                                        <!-- Cetak BA -->
                                        <a 
                                            href="<?= base_url();?>akademik/ajar/cetak_ba/<?= $isi->id_jadwal; ?>" 
                                            target='_blank' data-toggle="tooltip" title="cetak berita acara ujian"
                                            class="btn btn-default btn-small"><i class="btn-icon-only icon-print"></i>
                                        </a>
                                    </td>
                                    <td class="td-actions">
                                        <a 
                                            href="<?= base_url();?>akademik/ajar/cetak_absensi/<?= $isi->id_jadwal; ?>" data-toggle="tooltip" title="Cetak Absensi"
                                            target='_blank' 
                                            class="btn btn-primary btn-small"><i class="btn-icon-only icon-print"></i>
                                        </a>

                                        <?php if (in_array(8, $group) || in_array(19, $group)) : ?>
                                            <a 
                                                href="<?= base_url('akademik/ajar/cetak_absensi_polos/').$isi->id_jadwal;?>" data-toggle="tooltip" title="Cetak Absensi Polos"
                                                target='_blank' 
                                                class="btn btn-warning btn-small"><i class="btn-icon-only icon-print"></i>
                                            </a>
                                        <?php endif; ?>
                                    </td>
                                </tr>    
                            <?php $no++; endforeach ?>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<p class="footer">Page rendered in <strong>{elapsed_time}</strong> seconds. <?php echo  (ENVIRONMENT === 'development') ?  'CodeIgniter Version <strong>' . CI_VERSION . '</strong>' : '' ?></p>



<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content" id="absen">

        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script type="text/javascript">
    $(function () {
      $('[data-toggle="tooltip"]').tooltip()
    })
</script>