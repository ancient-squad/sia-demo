<script type="text/javascript">

jQuery(document).ready(function($) {

    $('input[name^=nid]').autocomplete({

        source: '<?php echo base_url('akademik/ubah_kaprodi/load_dosen_autocomplete');?>',

        minLength: 1,

        select: function (evt, ui) {

            this.form.nid0.value = ui.item.value;

        }

    });

});

</script>


<div class="row">
	<div class="span12">      		  		
  		<div class="widget ">
  			<div class="widget-header">
  				<i class="icon-user"></i>
  				<h3>Ganti Kaprodi</h3>
			</div> <!-- /widget-header -->
			<div class="widget-content">
				<div class="span11">
							<b><center>Ganti Kaprodi</center></b><br>
							<form id="edit-profile" class="form-horizontal" method="post" action="<?php echo base_url(); ?>akademik/ubah_kaprodi/ubah">
								<fieldset>
									<div class="control-group">											
										<label class="control-label">Kaprodi saat ini : </label>
										<div class="controls">
											<input type="text" class="span3" id="nid0" name="nid" placeholder="<?php echo $kaprodi ?> - <?php echo nama_dsn($kaprodi) ?>" required>
										</div> <!-- /controls -->				
									</div> <!-- /control-group -->
									
									<div class="form-actions">
										<input type="submit" class="btn btn-primary" id="save" value="Ubah"/>
									</div> <!-- /form-actions -->
								</fieldset>
							</form>
			    </div>
			</div>

		</div>
	</div>
</div>