<link href="<?= base_url();?>assets/css/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />

<table id="example8" class="table table-striped">
    <thead>
        <tr> 
            <th>No</th>
            <th>NPM</th>
            <th>Mahasiswa</th>
            <th style="text-align: center">Aksi</th>
        </tr>
    </thead>
    <tbody>
        <?php $no=1; foreach ($data->result() as $isi): ?>
            <tr>
                <td><?= $no; ?></td>
                <td><?= $isi->NIMHSMSMHS ?></td>
                <td><?= $isi->NMMHSMSMHS ?></td>
                <td  style="text-align: center">
                    <a href="" class="btn btn-danger"><i class="icon-remove"></i></a>
                </td>
            </tr>    
        <?php $no++; endforeach ?>
    </tbody>
</table>

<script src="<?= base_url();?>assets/js/datatables/jquery.dataTables.js" type="text/javascript"></script>

<script src="<?= base_url();?>assets/js/datatables/dataTables.bootstrap.js" type="text/javascript"></script>

<script type="text/javascript">
    $(function() {
        $("#example8").dataTable();
    });
</script>