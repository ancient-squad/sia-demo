<link href="https://gitcdn.github.io/bootstrap-toggle/2.2.0/css/bootstrap-toggle.min.css" rel="stylesheet">
<script src="https://gitcdn.github.io/bootstrap-toggle/2.2.0/js/bootstrap-toggle.min.js"></script>

<div class="row">
	<div class="span12">      		  		
  		<div class="widget ">
  			<div class="widget-header">
  				<i class="icon-file-alt"></i>
  				<h3>Data Kartu Rencana Studi</h3>
			</div> <!-- /widget-header -->
			
			<div class="widget-content" style="overflow:auto;">
				<div class="span11">
					<table id="example1" class="table table-bordered table-striped">
	                	<thead>
	                        <tr> 
	                        	<th>No</th>
	                        	<th>Semester</th>
	                        	<th>Total SKS</th>
	                            <th width="40">Aksi</th>
	                        </tr>
	                    </thead>
	                    <tbody>
                            <?php $no = 1; 
                            foreach ($krs as $row) { 
                            	$statusVerifikasi = $this->app_model->getdetail(
			                            								'tbl_verifikasi_krs',
			                            								'kd_krs',
			                            								$row->kd_krs,
			                            								'npm_mahasiswa',
			                            								'asc'
			                            							)->row()->status_verifikasi;

								if (is_null($statusVerifikasi) || $statusVerifikasi != 0 ) { 
                            	  	$warna = ''; 
                            	  	$cekform3 = $this->app_model->getcekform3smt($row->kd_krs)->row();
		            				if (isset($cekform3) && $cekform3->status_verifikasi == 10) {
		            					$warna = 'style="background:#DC143C;"';
		            				} ?>

			                        <tr <?= $warna; ?> >
			                        	<td <?= $warna; ?> ><?= $no;?></td>
			                        	
			                        	<?php $sms = $this->app_model->getdetail(
						                        						'tbl_mahasiswa',
						                        						'NIMHSMSMHS',
						                        						$row->npm_mahasiswa,
						                        						'NIMHSMSMHS',
						                        						'asc'
						                        					)->row(); ?>

			                        	<td <?= $warna; ?> > 
			                        		<?php $smtr = $this->db->query("SELECT distinct semester_krs 
			                        										from tbl_krs 
			                        										where kd_krs = '".$row->kd_krs."'
			                        										")->row()->semester_krs; 
			                        										echo $smtr; ?>
			                        	</td>

			                        	<?php
			                        	$kode_mk 	= $this->db->query("SELECT distinct kd_matakuliah 
			                        									from tbl_krs 
			                        									where kd_krs = '".$row->kd_krs."'
			                        									")->result();
			                        	$sumsks = 0;
			                        	foreach ($kode_mk as $value) {
			                        		$sksmk 	= $this->db->query("SELECT distinct sks_matakuliah 
			                        									from tbl_matakuliah 
			                        									where kd_matakuliah = '".$value->kd_matakuliah."' 
			                        									AND kd_prodi = ".trim($sms->KDPSTMSMHS)."
			                        									")->row()->sks_matakuliah; 
			                        		$sumsks = $sumsks + $sksmk;
			                        	} ?>
		                                
		                                <td <?= $warna; ?> ><?= $sumsks; ?></td>
			                        	<td class="td-actions" <?= $warna; ?> >
			                        	<?php 
			                        	 	$loginSession 	= $this->session->userdata('sess_login');
			                        	 	$idUserGroup 	= explode(',', $loginSession['id_user_group']);
											$numberOfGroup 	= count($idUserGroup);

											for ($i=0; $i < $numberOfGroup; $i++) { 
												$grup[] = $idUserGroup[$i];
											}

											if ((in_array(8, $grup)) || 
												(in_array(10, $grup)) || 
												(in_array(9, $grup)) || 
												(in_array(1, $grup))) { ?>
											<a 
												class="btn btn-success btn-small" 
												href="<?=base_url('akademik/viewkrs/viewview/'.$row->kd_krs);?>"><i class="btn-icon-only icon-ok"> </i>
											</a>

										   	<?php } else { ?>
											<a 
												class="btn btn-success btn-small" 
												href="<?= base_url('akademik/viewkrs/view/'.$row->kd_krs); ?>">
												<i class="btn-icon-only icon-ok"> </i>
											</a>
										   <?php } ?>
										</td>
			                        </tr>
	                        <?php $no++; }} ?>
	                    </tbody>
	               	</table>
				</div>
			</div>
		</div>
	</div>
</div>