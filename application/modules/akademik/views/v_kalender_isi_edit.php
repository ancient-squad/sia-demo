
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title"> <i class="icon icon-edit"></i> Buat Kalender Akademik</h4>
            </div>
            <!-- enctype="multipart/form-data" -->
            <div class="modal-body">
                <form class ='form-horizontal' action="<?php echo base_url();?>akademik/kalender/exe_edit_isi_kalender" method="post" >
                    <div class="control-group">
                  <label class="control-label">Kelompok Kegiatan </label>
                  <div class="controls">
                    <select class="form-control span4" name="kel">
                        <option>-- Pilih Kelompok Kegiatan --</option>
                        <option value="1" <?php if($row->kelompok == 1){echo "selected";} ?> >KEGIATAN PRA SEMESTER</option>
                        <option value="2" <?php if($row->kelompok == 2){echo "selected";} ?> >KEGIATAN SEMESTER</option>
                        <option value="3" <?php if($row->kelompok == 3){echo "selected";} ?> >KEGIATAN PEMBAYARAN KULIAH SEMESTER</option>
                    </select>
                  </div>
                </div>
                <div class="control-group">
                  <label class="control-label">Nama Kegiatan </label>
                  <div class="controls">
                    <textarea class="form-control span4" name="kegiatan" placeholder="Masukan Nama Kegiatan" required><?php echo $row->kegiatan ?></textarea>
                  </div>
                </div>
                <div class="control-group">
                  <label class="control-label">Waktu Pelaksanaan </label>
                  <div class="controls">
                    <input class="form-control span2" value="<?php echo $row->mulai ?>" type="text" placeholder="Waktu Mulai"    name="mulai" id="mulai" required>
                    <input class="form-control span2" value="<?php echo $row->ahir ?>" type="text" placeholder="Waktu Selesai"  name="ahir" id="ahir" required>
                    <input value="<?php echo $row->id ?>" type="hidden" name="id" id="ahir" required>
                  </div>
                </div>
                <div class="control-group">
                  <label class="control-label">Keterangan </label>
                  <div class="controls">
                    <textarea class="form-control span4" name="ket" placeholder="(OPSIONAL)" ><?php echo $row->keterangan ?></textarea>
                  </div>
                </div>
                <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Keluar</button>
                <input type="submit" class="btn btn-primary" value="Simpan"/>
                </div>
            </form>
            </div>
             