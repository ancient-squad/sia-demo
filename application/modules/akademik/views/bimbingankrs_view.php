<link href="https://gitcdn.github.io/bootstrap-toggle/2.2.0/css/bootstrap-toggle.min.css" rel="stylesheet">
<script src="https://gitcdn.github.io/bootstrap-toggle/2.2.0/js/bootstrap-toggle.min.js"></script>

<div class="row">
	<div class="span12">      		  		
  		<div class="widget ">
  			<div class="widget-header">
  				<i class="icon-user"></i>
  				<h3>Data Kartu Rencana Studi</h3>
			</div> <!-- /widget-header -->
			
			<div class="widget-content">
				<div class="span11">
					<table id="example1" class="table table-bordered table-striped">
	                	<thead>
	                        <tr> 
	                        	<th>No</th>
	                        	<th>Semester</th>
	                        	<th>Total SKS</th>
	                            <th width="40">Aksi</th>
	                        </tr>
	                    </thead>
	                    <tbody>
                            <?php $no = 1; foreach ($krs as $row) { ?>
	                        <tr>
	                        	<td><?php echo $no;?></td>
	                        	<td><?php echo $row->semester_krs;?></td>
                                <td><?php echo $row->jum_sks; ?></td>
	                        	<td class="td-actions">
									<a class="btn btn-success btn-small" href="<?php echo base_url(); ?>akademik/bimbingan/view/<?php echo $row->kd_krs; ?>"><i class="btn-icon-only icon-ok"> </i></a>
								</td>
	                        </tr>
	                        <?php $no++; } ?>
	                    </tbody>
	               	</table>
				</div>
			</div>
		</div>
	</div>
</div>