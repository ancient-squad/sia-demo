<script type="text/javascript">
$(function () {
    // Create the chart
    $('#contain2').highcharts({
        chart: {
            type: 'pie'
        },
        title: {
            text: 'Jumlah Mahasiswa Cuti/Non Aktif, dan Aktif Kembali per Prodi per <?php echo $thu ?>'
        },
        subtitle: {
            text: 'Klik grafik untuk melihat jumlah per prodi'
        },
        plotOptions: {
            series: {
                dataLabels: {
                    enabled: true,
                    format: '{point.name}: {point.y:.f} mhs'
                }
            }
        },

        tooltip: {
            headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
            pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.f}jml</b> of total<br/>'
        },
        series: [{
            name: 'Brands',
            colorByPoint: true,
            data: [
                <?php 
                    $baris = count($status);
                    //echo $baris;
                    $no = 0;
                    foreach ($status as $row) {
                        $no++;
                        if ($no == $baris) {
                            echo "{name: '".$row->prodi."', y:".$row->jml.", drilldown:'".$row->prodi."'}]";
                        } else {
                            echo "{name: '".$row->prodi."', y:".$row->jml.", drilldown:'".$row->prodi."'},";
                        }
                    }
                ?>
        }],
        drilldown: {
            series: [{
                <?php 
                $culun = count($status);
                $num = 0;
                
                    foreach ($status as $ohh) {
                        $q = $this->db->query('SELECT distinct a.`status`,COUNT(npm) as jum from tbl_status_mahasiswa a join tbl_mahasiswa f
                                                on a.`npm`=f.`NIMHSMSMHS` join tbl_jurusan_prodi b
                                                on b.`kd_prodi`=f.`KDPSTMSMHS`
                                                where b.`kd_prodi`= "'.$ohh->kd_prodi.'" and a.`tahunajaran` = "'.$thu.'" group by a.`status`')->result();
                        $idih = count($q);
                        $no = 0;
                        $num++;
                        if ($num == $culun) {
                            echo "
                                    name: '".$ohh->prodi."',
                                    id: '".$ohh->prodi."',
                                    data: [";
                            foreach ($q as $raw) {
                                        $no++;
                                        if ($no == $idih) {
                                            echo "['".$raw->status."',".$raw->jum."]]";
                                        } else {
                                            echo "['".$raw->status."',".$raw->jum."],";
                                        };
                                    }
                                echo "}]";
                        } else {
                            echo "
                                    name: '".$ohh->prodi."',
                                    id: '".$ohh->prodi."',
                                    data: [";
                            foreach ($q as $raw) {
                                        $no++;
                                        if ($no == $idih) {
                                            echo "['".$raw->status."',".$raw->jum."]]";
                                        } else {
                                            echo "['".$raw->status."',".$raw->jum."],";
                                        };
                                    }
                                echo "}, {";
                        }
                    } 
                
                ?>
            
        }
        
    });
});
</script>

<div id="contain2" style="min-width: 310px; height: 400px; max-width: 600px; margin: 0 auto"></div>