<?php

ob_start();

$pdf = new FPDF("P","mm", "A4");

$pdf->AliasNbPages();

$pdf->AddPage();


$pdf->SetMargins(3, 5 ,0);

$pdf->SetFont('Arial','',22); 


$pdf->image(FCPATH.'/assets/logo.gif',10,10,20);

$pdf->Ln(0);

$pdf->Cell(200,5,'SURAT TUGAS MENGAJAR',0,3,'C');

$pdf->Ln(2);

$pdf->Cell(200,10,'UNIVERSITAS BHAYANGKARA',0,5,'C');

$pdf->Ln(0);

$pdf->Cell(200,10,'JAKARTA RAYA',0,1,'C');

$pdf->Ln(3);

$pdf->Cell(250,0,'',1,1,'C');


#deskripsi surat tugas
$pdf->ln(5);

$pdf->SetLeftMargin(7);

$pdf->SetFont('Arial','',12); 

$pdf->MultiCell(195, 5,'1. Dasar : Kalender Akademik  Tahun Akademik '.substr(getDescYearbyCode(getactyear()),0,9).', mengenai perkuliahan Semester '.substr(getDescYearbyCode(getactyear()),12,14).' yang dimulai pada tanggal '.TanggalIndo(getFirstStudy(getactyear())).'.', 0,'L');

$pdf->ln();

$pdf->MultiCell(195, 5,'2. Sehubungan dengan hal tersebut diatas, maka Dekan Fakultas '.strtoupper($fakultas->fakultas).'  menugaskan :', 0,'L');

$pdf->ln();

$pdf->SetFont('Arial','BU',12); 

$pdf->Cell(190,8,$karyawan->nama,0,0,'C');

$pdf->ln();

$pdf->SetFont('Arial','',12); 

$pdf->ln(10);

$pdf->SetLeftMargin(10);


#header
$pdf->SetFont('Arial','B',10); 

$pdf->Cell(10,8,'NO',1,0,'C');

$pdf->Cell(65,8,'MATAKULIAH',1,0,'C');

$pdf->Cell(10,8,'SKS',1,0,'C');

$pdf->Cell(15,8,'HARI',1,0,'C');

$pdf->Cell(25,8,'WAKTU',1,0,'C');

$pdf->Cell(10,8,'SMT',1,0,'C');

$pdf->Cell(15,8,'KELAS',1,0,'C');

$pdf->Cell(40,8,'FAKULTAS/PRODI',1,1,'C');


$pdf->SetFont('Arial','',10); 
#query
$total=0;$no=1;foreach ($rows as $value) {

	$pdf->Cell(10,8,number_format($no),1,0,'C');

	$fontsize = strlen($value->nama_matakuliah) > 29 ? 8 : 10;
	$pdf->SetFont('Arial','',$fontsize); 
	$pdf->Cell(65,8,$value->nama_matakuliah,1,0,'C');

	$pdf->SetFont('Arial','',10); 

	$pdf->Cell(10,8,$value->sks_matakuliah,1,0,'C');

	$pdf->Cell(15,8,notohari($value->hari),1,0,'C');

	$pdf->Cell(25,8,substr($value->waktu_mulai,0,5).' - '.substr($value->waktu_selesai,0,5),1,0,'C');

	$pdf->Cell(10,8,$value->semester_matakuliah,1,0,'C');

	$pdf->Cell(15,8,$value->kelas,1,0,'C');

	$prodifak = strlen($value->kd_jadwal) == 16 ? get_jur(substr($value->kd_jadwal, 0,5)) : 'F. '.get_fak(substr($value->kd_jadwal, 0,1));

	$pdf->MultiCell(40,8,$prodifak,1,1);

	$total = $total + intval($value->sks_matakuliah);
$no++;}


#footer
$pdf->Cell(75,8,'Total SKS',1,0,'C');

$pdf->Cell(10,8,$total,1,0,'C');

$pdf->Cell(15,8,'',1,0,'C');

$pdf->Cell(25,8,'',1,0,'C');

$pdf->Cell(10,8,'',1,0,'C');

$pdf->Cell(15,8,'',1,0,'C');

$pdf->Cell(40,8,'',1,1,'C');


$pdf->ln();

$pdf->SetLeftMargin(7);

$pdf->SetFont('Arial','',12);

$pdf->MultiCell(190, 5,'3. Demikian penugasan ini agar dapat dilaksanakan dengan penuh rasa tanggung jawab.', 0,'L');


$pdf->ln(8);

$pdf->SetMargins(3, 5 ,0);

$pdf->SetFont('Arial','',12);

date_default_timezone_set('Asia/Jakarta'); 

$pdf->Cell(170,5,'Bekasi,'.date('d-m-Y').'',0,0,'R');



$pdf->ln(8);

$pdf->SetFont('Arial','B',12);

$pdf->Cell(145,5,'',0,0,'C');

$pdf->Cell(30,5,'Dekan Fakultas '.ucwords($fakultas->fakultas).'',0,0,'C');

$pdf->Cell(60,5,'',0,0,'L');

$pdf->Cell(10,5,'',0,0,'C');



$pdf->ln(30);

$pdf->SetFont('Arial','',12); 

$pdf->Cell(145,5,'',0,0,'C');

$pdf->Cell(30,5,'........',0,0,'C');

$pdf->Cell(10,5,'',0,0,'L');

$pdf->Cell(60,5,'',0,0,'L');



//exit();
$pdf->Output('Surat_Tugas_'.$karyawan->nid.'.PDF','I');

ob_end_flush();
?>

