<?php
error_reporting(0);
//var_dump($kelas);die();

$pdf = new FPDF("L","mm", "A4");

foreach ($jadwal as $kelas) {
	


$pdf->AliasNbPages();

$pdf->AddPage();





$pdf->SetMargins(3, 3 ,0);
$pdf->SetAutoPageBreak(TRUE, 3);

$pdf->SetFont('Arial','B',10); 



//$pdf->Image(''.base_url().'assets/img/logo-stkip-bima.png',60,30,90);

$pdf->Ln(0);

$pdf->Cell(200,5,strtoupper($titles->prodi),0,0,'L');

$pdf->Ln(4);

$pdf->SetFont('Arial','',10); 

$pdf->Cell(200,5,''.strtoupper($titles->fakultas).' - '.$this->ORG_NAME,0,0,'L');

$pdf->Ln(4);



$pdf->SetFont('Arial','',6);

$pdf->Cell(13,3,'KAMPUS I',0,0,'L');

$pdf->Cell(3,3,' : ',0,0,'L');

$pdf->Cell(140,3,'Jl. piere tendean,Mpunda, Nusa Tenggara Bar., Mande, Mpunda, Bima, Nusa Tenggara Bar. 84111, Indonesia',0,0,'L');

$pdf->Ln(2);



$pdf->SetFont('Arial','',6);

$pdf->Cell(13,3,'',0,0,'L');

$pdf->Cell(3,3,' : ',0,0,'L');

$pdf->Cell(140,3,'',0,0,'L');

$pdf->Ln(4);

$pdf->Cell(290,0,'',1,0,'C');



$pdf->ln(4);

$pdf->SetFont('Arial','',8);

$pdf->Cell(15,5,'KODE MK',0,0,'L');

$pdf->Cell(2,5,':',0,0,'L');

$pdf->Cell(60,5,$kelas->kd_matakuliah ,0,0,'L');

$pdf->Cell(15,5,'Smtr/Thn',0,0,'L');

if (substr($kelas->kd_tahunajaran, 4) == 1) {
	$ta = 'Ganjil';
} else {
	$ta = 'Genap';
}

$pdf->Cell(2,5,':',0,0,'L');

$pdf->Cell(50,5,$kelas->semester_matakuliah.' / '.substr($kelas->kd_tahunajaran, 0, 4).' - '.$ta,0,0,'L');

$pdf->Cell(20,5,'NAMA DOSEN',0,0,'L');

$pdf->Cell(2,5,':',0,0,'L');

$pdf->Cell(50,5,nama_dsn($kelas->kd_dosen),0,0,'L');

$pdf->Cell(20,5,'KELAS',0,0,'L');

$pdf->Cell(2,5,':',0,0,'L');

$pdf->Cell(50,5,$kelas->kelas,0,0,'L');

$pdf->ln(4);

$pdf->SetFont('Arial','',8);

$pdf->Cell(15,5,'NAMA MK',0,0,'L');

$pdf->Cell(2,5,':',0,0,'L');

$pdf->Cell(60,5,$kelas->nama_matakuliah,0,0,'L');

$pdf->Cell(15,5,'SKS',0,0,'L');

$pdf->Cell(2,5,':',0,0,'L');

$pdf->Cell(50,5,$kelas->sks_matakuliah,0,0,'L');

$pdf->Cell(20,5,'NID',0,0,'L');

$pdf->Cell(2,5,':',0,0,'L');

$pdf->Cell(50,5,$kelas->kd_dosen,0,0,'L');

$pdf->Cell(20,5,'Kuota',0,0,'L');

$pdf->Cell(2,5,':',0,0,'L');

$pdf->Cell(50,5,$kelas->kuota,0,0,'L');

$pdf->ln(4);

$pdf->SetFont('Arial','',8);

$pdf->Cell(15,5,'RUANG',0,0,'L');

$pdf->Cell(2,5,':',0,0,'L');

$pdf->Cell(60,5,$kelas->kode_ruangan,0,0,'L');

$pdf->Cell(15,5,'WAKTU',0,0,'L');

$pdf->Cell(2,5,':',0,0,'L');

$pdf->Cell(50,5,notohari($kelas->hari).'/'.del_ms($kelas->waktu_mulai).'-'.del_ms($kelas->waktu_selesai),0,0,'L');

$pdf->Cell(20,5,'KAMPUS',0,0,'L');

$pdf->Cell(2,5,':',0,0,'L');

$pdf->Cell(50,5,'Bekasi',0,0,'L');

$pdf->Cell(20,5,'Jumlah Peserta',0,0,'L');

$pdf->Cell(2,5,':',0,0,'L');

// $jumlah = $this->db->query("SELECT count(npm_mahasiswa) as mhs from tbl_krs where kd_jadwal = '".$kelas->kd_jadwal."'")->row()->mhs;

$jumlah = $this->app_model->getdetail('tbl_krs','kd_jadwal',$kelas->kd_jadwal,'id_krs','asc');

$pdf->Cell(50,5,$jumlah->num_rows().' orang',0,0,'L');




$pdf->ln(10);

$pdf->SetLeftMargin(5);

$pdf->SetFont('Arial','',12);

$pdf->Cell(289,8,'DAFTAR HADIR PESERTA KULIAH',1,0,'C');

$pdf->ln(8);



$pdf->SetFont('Arial','',8);

$pdf->Cell(8,10,'NO','L,T,R,B',0,'C');

$pdf->Cell(25,10,'NPM','L,T,R,B',0,'C');

$pdf->Cell(80,10,'NAMA','L,T,R,B',0,'C');

$pdf->Cell(176,5,'PERTEMUAN KULIAH','L,T,R,B',0,'C');

$pdf->ln(5);

$pdf->Cell(8,0,'',0,0,'C');

$pdf->Cell(25,0,'',0,0,'C');

$pdf->Cell(80,0,'',0,0,'C');



for ($i=1; $i < 17; $i++) { 

	$pdf->Cell(11,5,$i,1,0,'C');	

}

$no=1;

$noo=1;

// $mhs = $this->db->query('SELECT * from tbl_krs where kd_jadwal = "'.$kelas->kd_jadwal.'"')->result();

foreach ($jumlah->result() as $key) {
	
	$pdf->ln(5);

	$pdf->Cell(8,5,$no,1,0,'C');

	$pdf->Cell(25,5,$key->npm_mahasiswa,1,0,'C');

	$pdf->Cell(80,5,get_nm_mhs($key->npm_mahasiswa),1,0,'L');

	for ($i=1; $i < 17; $i++) { 
	// 	$absen = $this->db->query("SELECT * from tbl_absensi_mhs_new_20171 where npm_mahasiswa = '".$key->npm_mahasiswa."' and kd_jadwal = '".$key->kd_jadwal."' and pertemuan = ".$i." ")->row();
		$pdf->Cell(11,5,'',1,0,'C');

		// ini yang dibawah diaktifin kalo mau cetak absen yg ga kosong
	// 	if (count($absen) != 0) {
	// 		if ($absen->kehadiran == 'H') {
	// 			$pdf->Cell(11,5,'v',1,0,'C');
	// 		} elseif ($absen->kehadiran == 'A') {
	// 			$pdf->Cell(11,5,'-',1,0,'C');
	// 		} else {
	// 			$pdf->Cell(11,5,$absen->kehadiran,1,0,'C');
	// 		}
	// 	} else {
	// 		$pdf->Cell(11,5,'-',1,0,'C');
	// 	}
	}

	/*$jumlahabsen = $this->db->query("select distinct pertemuan from tbl_absensi_mhs_new_20171 where kd_jadwal = '".$key->kd_jadwal."' ")->result();
	$angka = 1;
	foreach ($jumlahabsen as $value) {
	$absen = $this->db->query("select * from tbl_absensi_mhs_new_20171 where npm_mahasiswa = '".$key->NIMHSMSMHS."' and kd_jadwal = '".$key->kd_jadwal."' and pertemuan = ".$angka." ")->result();
	if ($absen == true) {
		$pdf->Cell(11,5,'x',1,0,'C');
	} else {
		$pdf->Cell(11,5,'-',1,0,'C');
	}
	
	$angka++;
}

	$sisa =  16 - $angka;

	if ($sisa <= 16) {
		for ($i=0; $i <= $sisa; $i++) { 
			$pdf->Cell(11,5,'',1,0,'C');
		}
	}*/

	$no++;
	$noo++;

	if ($noo == 21) {
		$pdf->ln(5);

		$pdf->Cell(113,5,'Tanggal Kuliah',1,0,'R');
		$pdf->SetFont('Arial','',7);
		for ($i=1; $i < 17; $i++) { 
			// $absen = $this->db->query("SELECT tanggal from tbl_absensi_mhs_new_20171 where kd_jadwal = '".$kelas->kd_jadwal."' and pertemuan = ".$i." ")->row();

			// $tanggal = $this->temph_model->moreWhere('tbl_absensi_mhs_new_20171',array('kd_jadwal' => $kelas->kd_jadwal, 'pertemuan' => $i))->row()->tanggal;
			//$pdf->Cell(11,5,'',1,0,'C');
			// if ($tanggal == true) {
			// 	$pdf->Cell(11,5,date("d/m/y", strtotime($tanggal->tanggal)),1,0,'C');
			// } else {
				$pdf->Cell(11,5,'',1,0,'C');
			// }
		}
		$pdf->SetFont('Arial','',8);

		$pdf->ln(5);

		$pdf->Cell(113,5,'Jumlah Hadir',1,0,'R');



		for ($i=1; $i < 17; $i++) { 
			// $absen = $this->db->query("SELECT count(npm_mahasiswa) as jml from tbl_absensi_mhs_new_20171 where kd_jadwal = '".$kelas->kd_jadwal."' and pertemuan = ".$i." and kehadiran != 'A' ")->row();
			// $absen = $this->temph_model->moreWhere('tbl_absensi_mhs_new_20171',array('kd_jadwal' => $kelas->kd_jadwal, 'pertemuan' => $i, 'kehadiran' => '!= A'))->num_rows();
			// if ($absen == true) {
			// 	//$pdf->Cell(11,5,'',1,0,'C');
			// 	$pdf->Cell(11,5,$absen->jml,1,0,'C');
			// } else {
				$pdf->Cell(11,5,'',1,0,'C');
			// }
		}



		$pdf->ln(5);

		$pdf->Cell(113,5,'Paraf Dosen',1,0,'R');



		for ($i=0; $i < 16; $i++) { 

			$pdf->Cell(11,5,'','L,R,B',0,'C');	

		}

		$pdf->ln(8);

		$pdf->Cell(110,5,'',0,0,'C');
		$pdf->Cell(30,5,'Biro Administrasi Akademik',0,0,'C');
		$x = $pdf->GetX();
    	$y = $pdf->GetY();
		//$pdf->image(base_url().'/assets/ttd_baa.png',($x-30),($y+2),27);
		$pdf->Cell(50,5,'',0,0,'C');
		$pdf->Cell(113,5,'Kepala Program Studi',0,0,'C');

		$pdf->ln(20);
		$pdf->Cell(110,5,'',0,0,'C');
		$pdf->Cell(30,5,'ROULY G RATNA S, ST., MM',0,0,'C');
		$pdf->Cell(50,5,'',0,0,'C');
		$pdf->Cell(113,5,'(.............................................................)',0,0,'C');

		$noo = 1;

		$pdf->AliasNbPages();

$pdf->AddPage();



$pdf->SetMargins(3, 3 ,0);

$pdf->SetFont('Arial','B',10); 



//$pdf->Image(''.base_url().'assets/img/logo-stkip-bima.png',60,30,90);

$pdf->Ln(0);

$pdf->Cell(200,5,strtoupper($titles->prodi),0,0,'L');

$pdf->Ln(4);

$pdf->SetFont('Arial','',10); 

$pdf->Cell(200,5,''.strtoupper($titles->fakultas).' - '.$this->ORG_NAME,0,0,'L');

$pdf->Ln(4);



$pdf->SetFont('Arial','',6);

$pdf->Cell(13,3,'KAMPUS I',0,0,'L');

$pdf->Cell(3,3,' : ',0,0,'L');

$pdf->Cell(140,3,'Jl. piere tendean,Mpunda, Nusa Tenggara Bar., Mande, Mpunda, Bima, Nusa Tenggara Bar. 84111, Indonesia',0,0,'L');

$pdf->Ln(2);



$pdf->SetFont('Arial','',6);

$pdf->Cell(13,3,'',0,0,'L');

$pdf->Cell(3,3,' : ',0,0,'L');

$pdf->Cell(140,3,'',0,0,'L');

$pdf->Ln(4);

$pdf->Cell(290,0,'',1,0,'C');



$pdf->ln(4);

$pdf->SetFont('Arial','',8);

$pdf->Cell(15,5,'KODE MK',0,0,'L');

$pdf->Cell(2,5,':',0,0,'L');

$pdf->Cell(60,5,$kelas->kd_matakuliah ,0,0,'L');

$pdf->Cell(15,5,'Smtr/Thn',0,0,'L');

if (substr($kelas->kd_tahunajaran, 4)) {
	$ta = 'Ganjil';
} else {
	$ta = 'Genap';
}

$pdf->Cell(2,5,':',0,0,'L');

$pdf->Cell(50,5,$kelas->semester_matakuliah.' / '.substr($kelas->kd_tahunajaran, 0, 4).' - '.$ta,0,0,'L');

$pdf->Cell(20,5,'NAMA DOSEN',0,0,'L');

$pdf->Cell(2,5,':',0,0,'L');

$pdf->Cell(50,5,nama_dsn($kelas->kd_dosen),0,0,'L');

$pdf->Cell(20,5,'KELAS',0,0,'L');

$pdf->Cell(2,5,':',0,0,'L');

$pdf->Cell(50,5,$kelas->kelas,0,0,'L');

$pdf->ln(4);

$pdf->SetFont('Arial','',8);

$pdf->Cell(15,5,'NAMA MK',0,0,'L');

$pdf->Cell(2,5,':',0,0,'L');

$pdf->Cell(60,5,$kelas->nama_matakuliah,0,0,'L');

$pdf->Cell(15,5,'SKS',0,0,'L');

$pdf->Cell(2,5,':',0,0,'L');

$pdf->Cell(50,5,$kelas->sks_matakuliah,0,0,'L');

$pdf->Cell(20,5,'NID',0,0,'L');

$pdf->Cell(2,5,':',0,0,'L');

$pdf->Cell(50,5,$kelas->kd_dosen,0,0,'L');

$pdf->Cell(20,5,'Kuota',0,0,'L');

$pdf->Cell(2,5,':',0,0,'L');

$pdf->Cell(50,5,$kelas->kuota,0,0,'L');

$pdf->ln(4);

$pdf->SetFont('Arial','',8);

$pdf->Cell(15,5,'RUANG',0,0,'L');

$pdf->Cell(2,5,':',0,0,'L');

$pdf->Cell(60,5,$kelas->kode_ruangan,0,0,'L');

$pdf->Cell(15,5,'WAKTU',0,0,'L');

$pdf->Cell(2,5,':',0,0,'L');

$pdf->Cell(50,5,notohari($kelas->hari).'/'.del_ms($kelas->waktu_mulai).'-'.del_ms($kelas->waktu_selesai),0,0,'L');

$pdf->Cell(20,5,'KAMPUS',0,0,'L');

$pdf->Cell(2,5,':',0,0,'L');

$pdf->Cell(50,5,'Bekasi',0,0,'L');

$pdf->Cell(20,5,'Jumlah Peserta',0,0,'L');

$pdf->Cell(2,5,':',0,0,'L');

// $jumlah = $this->db->query("SELECT count(npm_mahasiswa) as mhs from tbl_krs where kd_jadwal = '".$kelas->kd_jadwal."'")->row()->mhs;

$pdf->Cell(50,5,$jumlah->num_rows().' orang',0,0,'L');



$pdf->ln(10);

$pdf->SetLeftMargin(5);

$pdf->SetFont('Arial','',12);

$pdf->Cell(289,8,'DAFTAR HADIR PESERTA KULIAH',1,0,'C');

$pdf->ln(8);



$pdf->SetFont('Arial','',8);

$pdf->Cell(8,10,'NO','L,T,R,B',0,'C');

$pdf->Cell(25,10,'NPM','L,T,R,B',0,'C');

$pdf->Cell(80,10,'NAMA','L,T,R,B',0,'C');

$pdf->Cell(176,5,'PERTEMUAN KULIAH','L,T,R,B',0,'C');

$pdf->ln(5);

$pdf->Cell(8,0,'',0,0,'C');

$pdf->Cell(25,0,'',0,0,'C');

$pdf->Cell(80,0,'',0,0,'C');



for ($i=1; $i < 17; $i++) { 

	$pdf->Cell(11,5,$i,1,0,'C');	

}
	}

	//ending header

}



$pdf->ln(5);

$pdf->Cell(113,5,'Tanggal Kuliah',1,0,'R');
$pdf->SetFont('Arial','',7);
for ($i=1; $i < 17; $i++) { 
	// $absen = $this->db->query("SELECT tanggal from tbl_absensi_mhs_new_20171 where kd_jadwal = '".$kelas->kd_jadwal."' and pertemuan = ".$i." ")->row();
	//$pdf->Cell(11,5,'',1,0,'C');
	// if ($tanggal == true) {
	// 	$pdf->Cell(11,5,date("d/m/y", strtotime($tanggal->tanggal)),1,0,'C');
	// } else {
		$pdf->Cell(11,5,'',1,0,'C');
	// }
}
$pdf->SetFont('Arial','',8);

$pdf->ln(5);

$pdf->Cell(113,5,'Jumlah Hadir',1,0,'R');


for ($i=1; $i < 17; $i++) { 
	// $absen = $this->db->query("SELECT count(npm_mahasiswa) as jml from tbl_absensi_mhs_new_20171 where kd_jadwal = '".$kelas->kd_jadwal."' and pertemuan = ".$i." and kehadiran != 'A' ")->row();
	// if ($absen == true) {
	// 	//$pdf->Cell(11,5,'',1,0,'C');
	// 	$pdf->Cell(11,5,$absen->jml,1,0,'C');
	// } else {
		$pdf->Cell(11,5,'',1,0,'C');
	// }
}



$pdf->ln(5);

$pdf->Cell(113,5,'Paraf Dosen',1,0,'R');



for ($i=0; $i < 16; $i++) { 

	$pdf->Cell(11,5,'','L,R,B',0,'C');	

}

$pdf->ln(8);
		
$pdf->Cell(110,5,'',0,0,'C');
$pdf->Cell(30,5,'Biro Administrasi Akademik',0,0,'C');
$x = $pdf->GetX();
$y = $pdf->GetY();
//$pdf->image(base_url().'/assets/ttd_baa.png',($x-30),($y+2),27);
$pdf->Cell(50,5,'',0,0,'C');
$pdf->Cell(113,5,'Kepala Program Studi',0,0,'C');

$pdf->ln(20);
$pdf->Cell(110,5,'',0,0,'C');
$pdf->Cell(30,5,'ROULY G RATNA S, ST., MM',0,0,'C');
$pdf->Cell(50,5,'',0,0,'C');
$pdf->Cell(113,5,'(.............................................................)',0,0,'C');

}//end foreach

$pdf->Output('ABSENSI.PDF','I');



?>

