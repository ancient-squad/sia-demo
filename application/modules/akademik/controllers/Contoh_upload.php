<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Contoh_upload extends MY_Controller {

	public function index()
	{
		$this->load->view('contoh');		
	}

	function upup()
	{
		$this->upload_file('./upload/');
	}

	public function upload_file($path){
		if ($_FILES['userfile']) {
			$this->load->helper('inflector');
			$nama = underscore($_FILES['userfile']['name']);
	        //$config['allowed_types'] = 'pdf|doc|docx|PDF|DOC|DOCX';
			$config['allowed_types'] = 'xls|xlsx';
	        $config['max_size'] = '1000000';
	        //$config['max_width'] = '302400';
	        //$config['max_height'] = '306800';
	        $config['file_name'] = $nama;
	        $config['upload_path'] = $path;
	        $this->load->library('upload', $config);
	        if (!$this->upload->do_upload("userfile")) {
	            $error = array('error' => $this->upload->display_errors());
	            die("gagal");
	        } else {
	            //$aa = array('upload_data' => $this->upload->data());
	            $upload_data = $this->upload->data();
				 
			 $this->load->library('excel_reader');
             $this->excel_reader->setOutputEncoding('CP1251');

             $file =  $upload_data['full_path'];
             $this->excel_reader->read($file);
             error_reporting(E_ALL ^ E_NOTICE);

             // Sheet 1
             $data = $this->excel_reader->sheets[0] ;
             $dataexcel = Array();
             for ($i = 1; $i <= $data['numRows']; $i++) {
                if($data['cells'][$i][1] == '') break;

                 $dataexcel[$i-1]['no'] = $data['cells'][$i][1];
                 $dataexcel[$i-1]['kd_reg'] = $data['cells'][$i][2];
                 $dataexcel[$i-1]['nama_mhs'] = $data['cells'][$i][3];
                 $dataexcel[$i-1]['tlp_mhs'] = $data['cells'][$i][4];
                 $dataexcel[$i-1]['email_mhs'] = $data['cells'][$i][5];
                 $dataexcel[$i-1]['alamat_mhs'] = $data['cells'][$i][6];
                 $dataexcel[$i-1]['lahir_mhs'] = $data['cells'][$i][7];
                 $dataexcel[$i-1]['fakultas'] = $data['cells'][$i][8];
                 $dataexcel[$i-1]['jurusan'] = $data['cells'][$i][9];
                 $dataexcel[$i-1]['hasil'] = $data['cells'][$i][10];
             }
     //cek data
     $check= $this->Querypage->search_chapter($dataexcel);
     if (count($check) > 0)
     {
       $this->Querypage->update_chapter($dataexcel);
       // set pesan
       $this->session->set_flashdata('msg_excel', 'update data success');
     }else{
         $this->Querypage->insert_chapter($dataexcel);
         // set pesan
         $this->session->set_flashdata('msg_excel', 'inserting data success');
     }
	        }
	        $data['surat'] = $config['file_name'];
        }
        //return($data['surat']);
	}
	       //  }
	       //  $data['surat'] = $config['file_name'];
        // }
        //return($data['surat']);
	//}

}

/* End of file contoh_upload.php */
/* Location: ./application/modules/akademik/controllers/contoh_upload.php */