<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Directlogin extends MY_Controller {

	public function index()
	{
		
	}

	function saveLog($key)
	{
		$getkey = $this->app_model->getdetail('tbl_keykrs','idkey',$key,'idkey','asc')->row();

		// if login session is true
		if ($this->session->userdata('sess_login')) {

			$uid = $this->session->userdata('sess_login');

			$cod = $this->app_model->getdetail('tbl_verifikasi_krs','kd_krs',$getkey->kd_krs,'kd_krs','asc')->row();

			// if userid is not match with id_pembimbing
			if ($uid['userid'] != $cod->id_pembimbing) {
				
				echo "<script>alert('Silahkan login menggunakan akun anda!');</script>";
				redirect(base_url('auth/logout'),'refresh');

			} else {
				
				if ($uid['id_user_group'] == 5) {
					redirect('akademik/krs_mhs/jadwal_kuliah','refresh');
				} elseif ($uid['id_user_group'] == 7) {
					redirect('akademik/bimbingan/viewmhs/'.$getkey->kdkrs,'refresh');
				} else {
					echo "<script>alert('Anda tidak berhak mengakses!');history.go(-1);</script>";
				}

			}

		// dude, you've to login before
		} else {
			$arr = array(
				'idkey' => $getkey->idkey,
				'kdkrs' => $getkey->kdkrs
			);
			
			$this->session->set_userdata('sessdirectlogin', $arr);
			$this->load->view('vdirectlogin');
		}
		
	}
}

/* End of file Directlogin.php */
/* Location: .//tmp/fz3temp-1/Directlogin.php */