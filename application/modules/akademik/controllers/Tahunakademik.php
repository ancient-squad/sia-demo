<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tahunakademik extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		//error_reporting(0);
		if (!$this->session->userdata('sess_login')) {
			redirect('auth','refresh');
		}
		$this->load->model('akademik/tahunakademik_model', 'tam');
	}

	public function index()
	{
		$data['page'] = 'tahunakademik_v';
		$data['datalist'] = $this->tam->show_all()->result();
		$this->load->view('template/template', $data);
	}

	public function store()
	{
		$kode = trim($this->input->post('kode'));
		$nama = $this->input->post('nama');
		$tglmulai = $this->input->post('tglmulai');
		$tglakhir = $this->input->post('tglakhir');

		if ($this->input->post('is_update') !== '0') {
			$this->_update($kode, $nama, $tglmulai,$tglakhir,trim($this->input->post('is_update')));
		}

		$save = $this->tam->save($kode, $nama, $tglmulai,$tglakhir);
		if ($save === 1) {
			$this->session->set_flashdata('success', 'Data baru berhasil ditambahkan');
		} else {
			$this->session->set_flashdata('fail', 'Data gagal disimpan');
		}
		redirect(base_url('akademik/tahunakademik'));
	}

	/**
	 * Load academic year for edit purpose.
	 * the editable year is year that greater than active year
	 * @param <string>
	 * @return void
	 */	
	public function get_year($code)
	{
		$actyear = getactyear();
		$year = $this->tam->get_year($code)->row();
		echo json_encode([
			"editable" => ($actyear > $code || $actyear === $code ? 0 : 1),
			"kode" => $year->kode,
			"nama" => $year->tahun_akademik,
			"tglmulai" => $year->tglmulai,
			"tglakhir" => $year->tglakhir
		]);
	}

	private function _update($kode, $nama, $tglmulai,$tglakhir,$old_kode)
	{
		$update = $this->tam->update_year($kode, $nama, $tglmulai,$tglakhir,$old_kode);
		if ($update === 1) {
			$this->session->set_flashdata('success','Berhasil memperbarui data!');
		} else {
			$this->session->set_flashdata('fail','Gagal memperbarui data!');
		}		
		redirect(base_url('akademik/tahunakademik'));
	}

	public function activate($year)
	{
		$this->_is_valid_toactivate($year);
		$activate = $this->tam->activate($year);
		if ($activate === 1) {
			$this->session->set_flashdata('success', 'Tahun akademik berhasil diaktivasi');
		} else {
			$this->session->set_flashdata('fail', 'Tahun akademik gagal diaktivasi');
		}
		redirect(base_url('akademik/tahunakademik'));
	}

	private function _is_valid_toactivate($year)
	{
		$last_active_year = $this->tam->last_active_year($year);
		if ($last_active_year->tahunajaran > $year) {
			$this->session->set_flashdata('fail', 'Tidak dapat mengaktivasi tahun akademik lampau');
			redirect(base_url('akademik/tahunakademik'));
		}
		return;
	}

}

/* End of file Tahunakademik.php */
/* Location: ./application/modules/akademik/controllers/Tahunakademik.php */