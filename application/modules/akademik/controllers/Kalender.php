<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Kalender extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
        if (!$this->session->userdata('sess_login')) {
            redirect('auth','refresh');
        }        
        error_reporting(0);
	}

	public function index()
	{     
		$logged = $this->session->userdata('sess_login');
        $grup = get_group($logged['id_user_group']);

        $data['tak'] = $this->db->where('kode >=',20171)->get('tbl_tahunakademik')->result();

        if (in_array(10, $grup)){ //BAA
        	$data['rows'] = $this->db->get('tbl_kalender')->result();
        	$data['page'] = 'v_kalender_baa';
            $this->load->view('template/template', $data);

        }elseif (in_array(8, $grup) || in_array(9, $grup) || in_array(19, $grup)) {//Prodi & FAK 
            $data['rows'] = $this->db->get('tbl_kalender')->result();
            $data['page'] = 'v_kalender_prodi';
            $this->load->view('template/template', $data);

        }elseif (in_array(5, $grup)) { //Mahasiswa 
			redirect('akademik/kalender/kalender_mhs','refresh');            
        }elseif (in_array(1, $grup)) { //Wakil Rektor I
            $data['rows'] = $this->db->get('tbl_kalender')->result();
            $data['page'] = 'v_kalender_wr1';
            $this->load->view('template/template', $data);            
        }else{
            echo "<script>alert('Anda tidak berhak mengakses halaman ini !!');window.location = '".base_url()."';</script>";
        }
	}

    function kalender_mhs()
    {
        $data['kode'] = $this->db->where('flag',2)->get('tbl_kalender', 1)->row();
        $data['pra']   = $this->db->from('tbl_kalender a')
                                    ->join('tbl_kalender_dtl b','a.kd_kalender = b.kd_kalender')
                                    ->where('a.flag',2)
                                    ->where('kelompok',1)
                                    ->order_by('mulai','asc')
                                    ->get()->result();

        $data['acara'] = $this->db->from('tbl_kalender a')
                                    ->join('tbl_kalender_dtl b','a.kd_kalender = b.kd_kalender')
                                    ->where('a.flag',2)
                                    ->where('kelompok',2)
                                    ->order_by('mulai','asc')
                                    ->get()->result();

        $data['biaya'] = $this->db->from('tbl_kalender a')
                                    ->join('tbl_kalender_dtl b','a.kd_kalender = b.kd_kalender')
                                    ->where('a.flag',2)
                                    ->where('kelompok',3)
                                    ->order_by('mulai','asc')
                                    ->get()->result();

        $data['page']='v_kalender_mhs';
        $this->load->view('template/template', $data);
    }



	function add_kalender()
    {
		$ta = $this->input->post('ta');
		$cek = $this->db->query('SELECT * FROM tbl_kalender WHERE SUBSTR(kd_kalender,0,5) = '.$ta.'')->num_rows();

		if ($cek >= 1) {
			echo "<script>alert('Kalender untuk Masa tersebut sudah ada !!');history.go(-1);</script>";
		}elseif ($cek == 0) {
			$gpass = NULL;
            $n = 6; // jumlah karakter yang akan di bentuk.
            $chr = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
             
            for ($i = 0; $i < $n; $i++) {
	            $rIdx = rand(1, strlen($chr));
	            $gpass .=substr($chr, $rIdx, 1);
            }

            $kd_kalender = $ta.'.'.$gpass;

            $data = array('kd_kalender' => $kd_kalender,'flag' => 1,
            			  'create_date' => date('Y-m-d'),'create_date' => date('Y-m-d') 
            			  );

            $this->db->insert('tbl_kalender', $data);
            redirect(base_url().'akademik/kalender','refresh');
		}		
	}

    function isi_kalender_prodi($id)
    {
        $this->session->set_userdata('kode',$id);
        $kalender = $this->db->where('kd_kalender',$id)->get('tbl_kalender',1)->row();
        $data['kode'] = $id;
        $data['ta']   = substr($id, 0,5);

        $data['pra'] = $this->db->where('kd_kalender',$id)
                                ->where('kelompok',1)
                                ->order_by('mulai','asc')
                                ->get('tbl_kalender_dtl')
                                ->result();

        $data['acara'] = $this->db->where('kd_kalender',$id)
                                    ->where('kelompok',2)
                                    ->order_by('mulai','asc')
                                    ->get('tbl_kalender_dtl')
                                    ->result();

        $data['biaya'] = $this->db->where('kd_kalender',$id)
                                    ->where('kelompok',3)
                                    ->order_by('mulai','asc')
                                    ->get('tbl_kalender_dtl')
                                    ->result();

        $data['page'] = 'v_kalender_isi_oke';
        $this->load->view('template/template', $data);
    }

    function isi_kalender($id)
    {
        $this->session->set_userdata('kode',$id);
        $data['kalender'] = $this->db->where('kd_kalender',$id)->get('tbl_kalender',1)->row();
        $data['kode'] = $id;
        $data['ta']   = substr($id, 0,5);

        $data['pra'] = $this->db->where('kd_kalender',$id)
                                ->where('kelompok',1)
                                ->order_by('mulai','asc')
                                ->get('tbl_kalender_dtl')
                                ->result();

        $data['acara'] = $this->db->where('kd_kalender',$id)
                                ->where('kelompok',2)
                                ->order_by('mulai','asc')
                                ->get('tbl_kalender_dtl')
                                ->result();

        $data['biaya'] = $this->db->where('kd_kalender',$id)
                                ->where('kelompok',3)
                                ->order_by('mulai','asc')
                                ->get('tbl_kalender_dtl')
                                ->result();


        if ($data['kalender']->flag == 1) {
            $data['page']='v_kalender_isi';
        }elseif ($data['kalender']->flag == 2){
            $data['page']='v_kalender_isi_oke';
        }

        $this->load->view('template/template', $data);
    }

    function add_isi_kalender()
    {
        $kode  = $this->input->post('kode');
        $kel   = $this->input->post('kel');
        $nama  = $this->input->post('kegiatan');
        $mulai = date('Y-m-d', strtotime($this->input->post('mulai')));
        $ahir  = date('Y-m-d', strtotime($this->input->post('ahir' )));
        $ket   = $this->input->post('ket');

        if ($ahir < $mulai) {
            echo "<script>alert('Cek Kembali Masa Kegiatan !!');history.go(-1);</script>";
            exit();
        }else{
            $object = [
                'kd_kalender' => $kode,
                'kelompok' => $kel,
                'kegiatan' => $nama,
                'mulai' => $mulai,
                'ahir' => $ahir,
                'keterangan' => $ket,
                'flag' => 1,
            ];
            $q = $this->db->insert('tbl_kalender_dtl', $object);

            if ($q) {
                redirect(base_url('akademik/kalender/isi_kalender/'.$this->session->userdata('kode')),'refresh');
            }else{
                echo "<script>alert('Kesalahan Input Kegiatan !!');history.go(-1);</script>";
                exit();  
            }
        }  
    }

    function rem_isi_kalender($id)
    {
        $q = $this->db->where('id',$id)->delete('tbl_kalender_dtl');
        if ($q) {
            redirect(base_url('akademik/kalender/isi_kalender/'.$this->session->userdata('kode')),'refresh');
        }else{
            echo "<script>alert('Kesalahan Hapus Kegiatan !!');history.go(-1);</script>";
            exit();  
        }
    }

    function upload_skep($id)
    {
        $data['id'] = $id;
        $this->load->view('v_kalender_upload',$data);        
    }

    function exe_upload_skep()
    {
        $this->load->helper('inflector');
        $id = $this->input->post('id');
        $nama = date('dmY').'_'.date('H:i').'_'.underscore(str_replace('/', '', $_FILES['userfile']['name']));

        //cek keamanan
        $ng = explode('.', $nama);
        if (count($ng) > 2) {
            echo "<script>alert('FORMAT FILE MENCURIGAKAN!');history.go(-1)';</script>";exit();
        }

        $config['upload_path'] = './upload/skep_kamik/';
        $config['allowed_types'] = 'pdf|doc|docx';
        $config['file_name'] = $nama;
        $config['max_size'] = 0;

        $this->load->library('upload', $config);

        if (!$this->upload->do_upload("userfile")) {
            $data = array('error', $this->upload->display_errors());
            echo "<script>alert('Kesalahan Hapus Kegiatan !!');history.go(-1);</script>";
            exit();
        } else {
            $data = array('upload_data' => $this->upload->data());
            $object = array('file' => $nama);

            $this->db->where('id', $id);
            $this->db->update('tbl_kalender', $object);

            echo "<script>alert('Berhasil Upload SKEP.');window.location = '".base_url('akademik/kalender')."';</script>";
        }
    }

    function edit_kalender($id)
    {
        $data['tak'] = $this->db->get('tbl_tahunakademik')->result();
        $data['row'] = $this->db->where('id',$id)->get('tbl_kalender', 1)->row();
        $this->load->view('v_kalender_edit',$data);
    }

    function exe_edit_kalender()
    {
        $session=$this->session->userdata('sess_dosen');
        $user = $session['userid'];

        $no = $this->input->post('no_skep');
        $tgl = $this->input->post('tgl_skep');
        $kode = $this->input->post('kd_kalender');

        $object = array('no_skep' => $no,'tgl_skep' => $tgl);

        $q = $this->db->where('kd_kalender', $kode)->update('tbl_kalender',$object);

        if ($q) {
            redirect(base_url('akademik/kalender'),'refresh');
        }else{
            echo "<script>alert('Kesalahan Upadate Data !!');history.go(-1);</script>";
        }
    }



    function edit_isi_kalender($id)
    {    
        $data['row'] = $this->db->where('id',$id)->get('tbl_kalender_dtl', 1)->row();
        $this->load->view('v_kalender_isi_edit',$data);
    }

    function exe_edit_isi_kalender($id)
    {
        $object = [
            'kelompok' => $this->input->post('kel'),
            'kegiatan' => $this->input->post('kegiatan'),
            'mulai' => $this->input->post('mulai'),
            'ahir' => $this->input->post('ahir'),
            'keterangan' => $this->input->post('ket'),
        ];
        $q = $this->db->where('id', $this->input->post('id'))->update('tbl_kalender_dtl',$object);
       
        if ($q) {
            redirect(base_url('akademik/kalender/isi_kalender/'.$this->session->userdata('kode')),'refresh');
        }else{
            echo "<script>alert('Kesalahan Update Data !!');history.go(-1);</script>";
            exit();  
        }
    }

    function cetak_isi_kalender_mhs($id)
    {
        $this->load->library('excel');
        $data['kode'] = $id;
        $data['ta']   = substr($id, 0,5);
        $data['pra'] = $this->db->where('kd_kalender',$id)
                                ->where('kelompok',1)
                                ->order_by('mulai','asc')
                                ->get('tbl_kalender_dtl')
                                ->result();

        $data['acara'] = $this->db->where('kd_kalender',$id)
                                ->where('kelompok',2)
                                ->order_by('mulai','asc')
                                ->get('tbl_kalender_dtl')
                                ->result();

        $data['biaya'] = $this->db->where('kd_kalender',$id)
                                ->where('kelompok',3)
                                ->order_by('mulai','asc')
                                ->get('tbl_kalender_dtl')
                                ->result();

        $this->load->view('v_kalender_excel', $data);
    }



    function cetak_isi_kalender()
    {
        $this->load->library('excel');
        $id = $this->session->userdata('kode');
        $data['kode'] = $id;
        $data['ta']   = substr($id, 0,5);
        $data['pra'] = $this->db->where('kd_kalender',$id)
                                ->where('kelompok',1)
                                ->order_by('mulai','asc')
                                ->get('tbl_kalender_dtl')
                                ->result();

        $data['acara'] = $this->db->where('kd_kalender',$id)
                                ->where('kelompok',2)
                                ->order_by('mulai','asc')
                                ->get('tbl_kalender_dtl')
                                ->result();

        $data['biaya'] = $this->db->where('kd_kalender',$id)
                                ->where('kelompok',3)
                                ->order_by('mulai','asc')
                                ->get('tbl_kalender_dtl')
                                ->result();

        $this->load->view('v_kalender_excel', $data);
    }

    function approved($kode)
    {
        $object = array('flag' => 2);
        $query = $this->db->where('kd_kalender', $kode)->update('tbl_kalender', $object);

        if ($query) {
            redirect(base_url('akademik/kalender/isi_kalender/'.$this->session->userdata('kode')),'refresh');
        }else{
            echo "<script>alert('Kesalahan Update Data !!');history.go(-1);</script>";
        }
    }

    //in below all function for ajax
    public function kalender_list()
    {
        $list = $this->db->get('tbl_kalender')->result();
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $isi) {

            if ($isi->flag == 1) {
                $sts = 'menunggu persetujuan';
                $ta = substr($row->kd_kalender, 0,5);
                $no_skep = 'menunggu persetujuan';
                $tgl_skep = 'menunggu persetujuan';
            } elseif($isi->flag == 2) {
                $sts = 'Disetujui';
                $ta = $row->tahun_akademik;
                $no_skep = $row->no_skep;
                $tgl_skep = $row->tgl_skep;
            }
            
            $no++;
            $row = array();
            $row[] = $isi->ta;
            $row[] = $isi->no_skep;
            $row[] = $isi->tgl_skep;
            $row[] = $isi->sts;

            //add html for action
            $row[] = '<a class="btn btn-primary btn-small" onclick="edit(<?php echo $row->id?>)" data-toggle="modal" href="#editModal1" ><i class="btn-icon-only icon-pencil"></i></a>
                                    <a href="<?php echo base_url(); ?>akademik/kalender/isi_kalender/<?php echo $row->kd_kalender; ?>" class="btn btn-info btn-small edit"><i class="btn-icon-only icon-list"> </i></a>';
        
            $data[] = $row;
        }

        $output = [
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->person->count_all(),
            "recordsFiltered" => $this->person->count_filtered(),
            "data" => $data,
        ];
        //output to json format
        echo json_encode($output);
    }

}

/* End of file Kalender.php */
/* Location: ./application/controllers/Kalender.php */