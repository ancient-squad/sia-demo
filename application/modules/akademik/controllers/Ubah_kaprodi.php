<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ubah_kaprodi extends MY_Controller {

	function __construct()
	{
		parent::__construct();
		if (!$this->session->userdata('sess_login')) {
			redirect('auth','refresh');
		}
	} 

	function index()
	{	
		$user  = $this->session->userdata('sess_login');
		$prodi = $user['userid'];
		$grup  = get_group($user['id_user_group']);
		if (in_array(19, $grup)) {
			$data['kaprodi']=$this->app_model->getdetail(
				'tbl_jurusan_prodi',
				'kd_prodi',
				$prodi,
				'kd_prodi',
				'asc',
				1
			)->row()->kaprodi;
	        $data['page']='ganti_kaprodi';
	        $this->load->view('template/template', $data);
		} else {
			redirect(base_url());
		}
	}

	function load_dosen_autocomplete()
	{
		$bro = $this->session->userdata('sess_login');
		$prodi = $bro['userid'];
        $this->db->distinct();
        $this->db->select("nid,nama");
        $this->db->from('tbl_karyawan');
        $this->db->like('nama', $_GET['term'], 'both');
        $this->db->or_like('nid', $_GET['term'], 'both');
        $sql  = $this->db->get();
        $data = array();

        foreach ($sql->result() as $row) {
            $data[] = ['value' => $row->nid.' - '.$row->nama];
        }
        echo json_encode($data);
    }

	function ubah()
	{
		$user 	= $this->session->userdata('sess_login');
		$prodi  = $user['userid'];
		$ini 	= $this->input->post('nid');
		$pecah 	= explode(' - ', $ini);
		$cek 	= $this->db->query('SELECT * FROM tbl_karyawan WHERE nid = "'.$pecah[0].'"')->row();
		if ($cek == FALSE) {
			echo "<script>alert('Data Tidak Ditemukan! Periksa User ID Pengguna!');document.location.href='".base_url()."akademik/ubah_kaprodi';</script>";
		} else {
			$data = ['kaprodi' => $pecah[0]];
			$this->app_model->updatedata('tbl_jurusan_prodi','kd_prodi',$prodi,$data);
			echo "<script>alert('Sukses');document.location.href='".base_url()."akademik/ubah_kaprodi';</script>";
		}
    }

}

/* End of file Ajar.php */
/* Location: .//C/Users/danum246/AppData/Local/Temp/fz3temp-1/Ajar.php */