<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Krs_mhs extends MY_Controller
{
	protected $uid;

	function __construct()
	{
		error_reporting(0);
		
		parent::__construct();

		$this->load->model('setting_model');
		$this->load->model('temph_model');
		$this->load->model('krs_model');

		if ($this->session->userdata('sess_login')) {

			$user_session 	= $this->session->userdata('sess_login');
			$this->uid 		= $user_session['userid'];

			// is user have permission to access this feature?
			$cekakses 	= $this->role_model->cekakses(157)->result();

			if (!$cekakses) {
				echo "<script>alert('Anda Tidak Berhak Mengakses!');history.go(-1);</script>";
				exit();
			}

		} else {
			redirect('auth', 'refresh');
		}
	}

	public function index()
	{
		// check whether user valid to access KRS menu
		$this->_is_valid_to_access();

		// is student's KRS exist on this academic year ?
		$cek = $this->krs_model->getactvkrs($this->uid . getactyear());

		// jika sudah pernah melakukan pengisian KRS
		if ($cek->num_rows() == 1) {
			redirect(base_url('akademik/krs_mhs/save_sesi/' . $this->uid . '/' . $cek->row()->kd_krs), 'refresh');
		}

		// jika belum mengisi KRS
		$this->_prefilling_form();
	}

	function _is_valid_to_access()
	{
		// check whether student has filled out the bio
		$bio = $this->app_model->getdetail('tbl_bio_mhs', 'npm', $this->uid, 'npm', 'asc');
		if ($bio->num_rows() == 0) {
			echo "<script>alert('Mohon isi biodata Anda sebelum melakukan pengisian KRS!');history.go(-1);</script>";
			return;
		}

		// check whether student has filled out the e-mail
		if (is_null($bio->row()->email or empty($bio->row()->email))) {
			echo "<script>alert('Mohon isi e-Mail anda pada menu biodata guna pengiriman notifikasi KRS!');history.go(-1);</script>";
			return;
		}

		// check whether student has filled out the phone number
		if (is_null($bio->row()->no_hp) || $bio->row()->no_hp == '') {
			echo "<script>alert('Mohon isi nomor handphone anda terlebih dahulu!');history.go(-1);</script>";
			return;
		}

		return;
	}

	/**
	 * formulir pra pengisian KRS, memilih kelas dan dosen pembimbing
	 * @return void
	 */
	protected function _prefilling_form()
	{
		$yearBefore  = yearBefore();
		$data['mhs'] = $this->db
							->select('npm_mahasiswa,kd_dosen,kelas_mhs')
							->get_where('tbl_pa', ['npm_mahasiswa' => $this->uid])
							->row();
		
		$studentDepartment = get_mhs_jur($this->uid);

		// cek apakah jadwal untuk prodi terkait sudah aktif
		$isScheduleActive = $this->app_model->getdetail('tbl_jadwal_krs', 'kd_prodi', $studentDepartment, 'kd_prodi', 'asc')->row();

		// 1 = jadwal pengisian aktif
		if ($isScheduleActive->status == 1) {
			$data['page'] = 'akademik/krs_mhs';
			$this->load->view('template/template', $data);
		} else {
			echo "<script>alert('Akses Tidak Diizinkan!');document.location.href='" . base_url('home') . "';</script>";
			return;
		}
	}

	function create_session()
	{
		// cek apakah KRS mahasiswa teah disetujui 
		$ceks 	= $this->krs_model->statVerify($this->uid . getactyear(), 1)->result();

		// jika KRS telah disetujui
		if (count($ceks) > 0) {
			echo "<script>alert('KRS Anda telah disetujui oleh dosen pembimbing!');history.go(-1);</script>";
			exit();

		// jika KRS belum disetujui
		} else {
			$logs = $this->session->userdata('sess_login');

			// cek pembayaran tahun sebelumnya
			// $this->_is_student_have_debt($this->uid, yearBefore());

			/** cek pembayaran pada tahun ajaran aktif
			$cekrenkeu 	= $this->krs_model->krsPayment($uid, getactyear(), 1)->result();

			cek status verifikasi KRS pada tahun ajaran sebelumnya
			$krsan = $this->app_model->getdetail('tbl_verifikasi_krs','kd_krs',$uid.yearBefore(),'kd_krs','asc')->row()->status_verifikasi;

			jika pembayaran dan status verifikasi valid
			if ((count($cekrenkeu) > 0) and ($krsan != 10)) { */

			$this->session->set_userdata('kd_dosen', $this->input->post('kd_dosen'));
			$this->session->set_userdata('kelas', $this->input->post('kelas'));

			redirect(base_url() . 'akademik/krs_mhs/pengisian_krs', 'refresh');

			/** } else {
				echo "<script>alert('Mohon Penuhi Biaya Administrasi!');history.go(-1);</script>";
			} */
		}
	}

	function _is_student_have_debt($npm, $yearBefore)
	{
		$isPreviousYearPaid = $this->krs_model->last_semester_payment($npm, $yearBefore)->num_rows();
		if ($isPreviousYearPaid < 1) {
			echo "<script>alert('Anda belum melunasi pembayaran semester sebelumnya!');history.go(-1);</script>";
			exit();
		}
		return;
	}

	function printkrs($kodebaru)
	{
		$a 	= substr($kodebaru, 16, 1);
		$npm = substr($kodebaru, 0, 9);
		$ta = substr($kodebaru, 9, 5);

		if ($a == 1) {
			$b = 'Ganjil';
		} else {
			$b = 'Genap';
		}

		$cetak['kdver'] = $this->db
									->select('kd_krs_verifikasi')
									->from('tbl_verifikasi_krs')
									->where('npm_mahasiswa', $npm)
									->where('tahunajaran', $ta)
									->order_by('kd_krs_verifikasi', 'desc')
									->limit(1)
									->get()->row();

		$cetak['footer'] = $this->db
									->select('npm_mahasiswa,id_pembimbing')
									->from('tbl_verifikasi_krs')
									->like('npm_mahasiswa', $npm, 'both')
									->get()->row();

		$cetak['kd_krs'] = $kodebaru;
		$cetak['gg']     = $b;
		$cetak['npm']    = $npm;
		$cetak['ta']     = $ta;

		$this->load->view('welcome/print/krs_pdf', $cetak);
	}

	function load_dosen_autocomplete()
	{
		$this->db->distinct();
		$this->db->select("a.id_kary,a.nid,a.nama");
		$this->db->from('tbl_karyawan a');
		$this->db->like('a.nama', $_GET['term'], 'both');
		$this->db->or_like('a.nid', $_GET['term'], 'both');
		$sql  = $this->db->get();

		$data = array();

		foreach ($sql->result() as $row) {
			$data[] = array(
				'id_kary' => $row->id_kary,
				'nid'     => $row->nid,
				'value'   => $row->nid . ' - ' . $row->nama
			);
		}

		echo json_encode($data);
	}

	function revisi_krs($id)
	{
		// setting user session
		$log = $this->session->userdata('sess_login');
		
		$tglnow 	= date('Y-m-d');
		$kd_dosen 	= $this->db->where('kd_krs', $id)->get('tbl_verifikasi_krs')->row();

		$this->session->set_userdata('kd_krs', $id);
		$this->session->set_userdata('kd_dosen', $kd_dosen->id_pembimbing);
		$this->session->set_userdata('kd_kelas', $kd_dosen->kelas);

		$data['mhs'] = $this->krs_model->getPa($this->uid)->row();

		$jam = date("H:i");

		if ($jam > '00:01' and $jam < '23:59') {
			$log = get_mhs_jur($this->uid);
			$act = $this->app_model->getdetail('tbl_jadwal_krs', 'kd_prodi', $log, 'kd_prodi', 'asc')->row()->status;

			if ($act == 1) {
				redirect(base_url() . 'akademik/krs_mhs/pengisian_krs');
			} else {
				echo "<script>alert('Akses Tidak Diizinkan');document.location.href='" . base_url() . "home';</script>";
			}
		} else {
			echo "<script>alert('Akses pengisian KRS hanya diizinkan mulai pukul 00:01 s/d 23:59');document.location.href='" . base_url() . "home';</script>";
		}
	}

	function pengisian_krs()
	{
		// ganjil genap
		$gg = substr(getactyear(), -1);

 		// cek program perkuliahan S1/S2
		// $strata = substr($this->uid, 4, 1);
		// if ($strata == 1) {
			$this->_for_bachelor($gg, $this->uid);
		// } else {
		// 	$this->_for_post_graduate($this->uid);
		// }
	}

	/**
	 * untuk pengisian sarjana S1
	 * @param string $gg [ganjil genap]
	 * @param string $logmhs [npm]
	 * @return void
	 */
	function _for_bachelor($gg, $logmhs)
	{
		// pengecekan apakah mahasiswa sudah melakukan pengisian
		$loadkrs = $this->krs_model->getactvkrs($logmhs . getactyear())->result();

		// jika sudah ada KRS
		if (count($loadkrs) > 0) {
			$this->_load_krs($gg, $logmhs);

			// jika belum ada KRS
		} else {
			$this->_load_new_form($gg, $logmhs);
		}
	}

	function _for_post_graduate($logmhs)
	{
		$data['ta'] = getactyear();

		// detail mhs
		$mhs = $this->app_model->getdetail('tbl_mahasiswa', 'NIMHSMSMHS', $logmhs, 'KDPSTMSMHS', 'ASC')->row_array();

		// cek semester berdasarkan tahun masuk mahasiswa. karena mhs magister bisa masuk di genap dan ganjil
		if ($mhs['SMAWLMSMHS'] % 2 == 0) {
			$getsemester = $this->app_model->get_smt_awal_genap($mhs['SMAWLMSMHS']);
		} else {
			$getsemester = $this->app_model->get_semester($mhs['SMAWLMSMHS']);
		}

		// get ganjil genap for magister
		if ($getsemester % 2 == 1) {
			$gg = 1;
		} else {
			$gg = 2;
		}

		// get matakuliah for current semester
		$data_krs = $this->app_model->get_krs_mahasiswa($logmhs, $getsemester);

		// hitung IPS
		$ips = $this->krs_model->countips($mhs['KDPSTMSMHS'], $logmhs, yearBefore());

		$data['ipsmhs_before']  = $ips;
		$data['seekhs']         = $logmhs . yearBefore();
		$data['npm']            = $logmhs;
		$data['prodi']          = $mhs['KDPSTMSMHS'];
		$data['nama_mahasiswa'] = $mhs['NMMHSMSMHS'];
		$data['ipk']            = $this->_count_ipk($logmhs);
		$data['semester']       = $getsemester;
		$data['data_krs']       = $this->app_model->get_all_khs_mahasiswa($logmhs)->result();
		$data['data_ngulang']   = $this->app_model->get_matkul_ngulang($gg, $logmhs)->result();

		if ($data_krs->num_rows() > 0) {
			$data['data_matakuliah'] = $data_krs->result();
			$data['kode_krs']        = $data_krs->row()->kd_krs;
			$data['status_krs']      = 1;
		} else {
			$data['data_matakuliah'] = $this->app_model->get_matkul_krs($data['semester'], $data['prodi'], $data['npm'])->result();
			$data['status_krs']      = 0;
		}

		$data['dospem'] = $this->session->userdata('kd_dosen');
		$data['page'] = 'akademik/krs_view';
		$this->load->view('template', $data);
	}

	/**
	 * memuat KRS untuk mahasiswa yang sudah mengisi KRS
	 * @param string $gg [ganjil genap]
	 * @param string $logmhs [npm]
	 * @return void
	 */
	function _load_krs($gg, $logmhs)
	{
		$data['ta'] = getactyear();

		// load data mahasiswa
		$mhs = $this->app_model->getdetail('tbl_mahasiswa', 'NIMHSMSMHS', $logmhs, 'KDPSTMSMHS', 'ASC')->row_array();

		// count IPK mahasiswa
		$data['ipk'] = $this->_count_ipk($logmhs);

		// penghitungan IPS mahasiswa
		$ips = $this->krs_model->countips($mhs['KDPSTMSMHS'], $logmhs, yearBefore());

		$data['ipsmhs_before']   = $ips;
		$data['seekhs']          = $logmhs . yearBefore();
		$data['npm']             = $logmhs;
		$data['smtmhs']          = $mhs['SMAWLMSMHS'];
		$data['prodi']           = $mhs['KDPSTMSMHS'];
		$data['nama_mahasiswa']  = $mhs['NMMHSMSMHS'];
		$data['semester']        = $this->app_model->get_semester($mhs['SMAWLMSMHS']);
		$data['data_krs']        = $this->app_model->get_all_khs_mahasiswa($logmhs)->result();
		$data_krs                = $this->app_model->get_krs_mahasiswa($data['npm'], $data['semester']);
		$prekodekrs              = $logmhs . getactyear();
		$kodekrs                 = $this->db
										->like('kd_krs', $prekodekrs, 'after')
										->get('tbl_verifikasi_krs')
										->row()->kd_krs;
		$data['kode_krs']        = $kodekrs;
		$data['data_matakuliah'] = $data_krs->result();
		$data['data_ngulang']    = $this->app_model->get_matkul_ngulang($gg, $logmhs)->result();
		$data['status_krs']      = 1;
		$data['dospem']          = $this->session->userdata('kd_dosen');
		$data['page']            = 'akademik/krs_view';
		$this->load->view('template/template', $data);
	}

	/**
	 * memuat halaman baru untuk pengisian KRS
	 * @param string $gg [ganjil genap]
	 * @param string $logmhs [npm]
	 * @return void
	 */
	function _load_new_form($gg, $logmhs)
	{
		$data['ta'] = getactyear();
		$mahasiswa  = $this->app_model->getdetail('tbl_mahasiswa', 'NIMHSMSMHS', $logmhs, 'KDPSTMSMHS', 'ASC')->row_array();

		// count IPK mahasiswa
		$data['ipk'] = $this->_count_ipk($mahasiswa['NIMHSMSMHS']);

		// hitung IPS 
		$ips_nr = $this->krs_model->countips($mahasiswa['KDPSTMSMHS'], $logmhs, yearBefore());

		$data['ipsmhs_before']   = $ips_nr;
		$data['seekhs']          = $logmhs . yearBefore();
		$data['smtmhs']          = $mahasiswa['SMAWLMSMHS'];
		$data['npm']             = $logmhs;
		$data['prodi']           = $mahasiswa['KDPSTMSMHS'];
		$data['nama_mahasiswa']  = $mahasiswa['NMMHSMSMHS'];
		$data['semester']        = $this->app_model->get_semester($mahasiswa['SMAWLMSMHS']);
		$data['data_matakuliah'] = $this->app_model->get_matkul_krs($data['semester'], $data['prodi'], $logmhs)->result();
		$data['status_krs']      = 0;
		$data['data_ngulang']    = $this->app_model->get_matkul_ngulang($gg, $logmhs)->result();
		$data['dospem']          = $this->session->userdata('kd_dosen');

		$data['page']            = 'akademik/krs_view';
		$this->load->view('template/template', $data);
	}

	function _count_ipk($uid)
	{
		$getipk = $this->temph_model->get_ipk($uid)->result();
		$nulsks = 0;
		$const 	= 0;
		foreach ($getipk as $row) {
			$countloop 	= ($row->sks_matakuliah * $row->BOBOTTRLNM);
			$nulsks		= $nulsks + $row->sks_matakuliah;
			$const 	  	= $const + $countloop;
		}
		return number_format(($const / $nulsks), 2);
	}

	function cekjumlahsks()
	{
		$loggedmhs     = $this->session->userdata('sess_login');
		$npm           = $loggedmhs['userid'];
		$mhs           = $this->app_model->getdetail('tbl_mahasiswa', 'NIMHSMSMHS', $npm, 'KDPSTMSMHS', 'ASC')->row_array();
		$data['prodi'] = $mhs['KDPSTMSMHS'];
		$ips_nr        = $this->krs_model->countips($data['prodi'], $npm, yearBefore());
		$ips           = number_format($ips_nr, 2);
		$semester      = $this->app_model->get_semester($mhs['SMAWLMSMHS']);

		if (isset($_POST['sks'])) {
			$sks = $_POST['sks'];
		} else {
			$sks = 0;
		}

		if ($sks > 24) {
			echo 0;
		} else {
			$b = $this->app_model->ketentuan_sks($ips, $sks);
			if ($semester == 1) {
				echo 1;
			} else {
				echo $b;
			}
		}
	}

	function get_matkul()
	{
		$semester      = $_POST['semester'];
		$npm           = $_POST['npm'];
		$prodi         = $_POST['prodi'];
		$tahunakademik = $this->app_model->getdetail('tbl_tahunakademik', 'status', 1, 'kode', 'ASC')->row()->kode;

		if (($tahunakademik % 2) == 1 and $semester == 8) {
			$data = $this->app_model->get_matkul_krs_spesial($semester, $prodi, $npm, 1)->result();
		} elseif (($tahunakademik % 2) == 1 and $semester == 6) {
			$data = $this->app_model->get_matkul_krs_spesial($semester, $prodi, $npm, 2)->result();
		} elseif (($tahunakademik % 2) == 0 and $semester == 7) {
			$data = $this->app_model->get_matkul_krs_spesial($semester, $prodi, $npm, 2)->result();
		} else {
			$data = $this->app_model->get_matkul_krs_reguler($semester, $prodi, $npm)->result();
		}

		$js = json_encode($data);
		echo $js;
	}

	function cek_prasyarat()
	{
		$log       = $this->session->userdata('sess_login');
		$mhs       = $this->app_model->getdetail('tbl_mahasiswa', 'NIMHSMSMHS', $this->uid, 'NIMHSMSMHS', 'asc')->row();
		$sym       = array('[', ']');
		$prasyarat = array();
		$hit       = array();
		$prasyarat = explode(',', str_replace($sym, '', $_POST['prasyarat']));

		for ($i = 0; $i < count($prasyarat); $i++) {
			$kv = $this->db->where('kd_baru', $prasyarat[$i])
				->where('flag', 1)
				->where('kd_prodi', $mhs->KDPSTMSMHS)
				->get('tbl_konversi_matkul_temp');

			if ($kv->num_rows() > 0) {
				$hit[] = $kv->row()->kd_lama;

				$kv2 = $this->db->where('kd_baru', $kv->row()->kd_lama)
					->where('flag', 1)
					->where('kd_prodi', $mhs->KDPSTMSMHS)
					->get('tbl_konversi_matkul_temp');

				if ($kv2->num_rows() > 0) {
					$hit[] = $kv2->row()->kd_lama;

					$kv3 = $this->db->where('kd_baru', $kv2->row()->kd_lama)
						->where('flag', 1)
						->where('kd_prodi', $mhs->KDPSTMSMHS)
						->get('tbl_konversi_matkul_temp');

					if ($kv3->num_rows() > 0) {
						$hit[] = $kv3->row()->kd_lama;
					}
				}
			}
		}

		$gabung = array_merge($prasyarat, $hit);

		// STKIP Bima untuk mhs konversi tidak ada perbedaan format NPM
		// if (substr($this->uid, 8, 1) == '7') {
		// 	$data2 = $this->temph_model->cek_prasyarat_konversi($gabung, $this->uid)->num_rows();
		// 	$data3 = $this->temph_model->cek_prasyarat($gabung, $this->uid)->num_rows();
		// 	$data = $data2 + $data3;
		// } else {
			$data = $this->temph_model->cek_prasyarat($gabung, $this->uid)->num_rows();
		// }
		echo $data;
		// echo 1;
	}

	function cek_prasyarat_new()
	{
		$mhs       = $this->app_model->getdetail('tbl_mahasiswa', 'NIMHSMSMHS', $this->uid, 'NIMHSMSMHS', 'asc')->row();
		$prodi 	   = $mhs->KDPSTMSMHS;
		$sym       = array('[', ']');
		$prasyarat = [];
		$hit       = [];
		$prasyarat = explode(',', str_replace($sym, '', $_POST['prasyarat']));

		for ($i = 0; $i < count($prasyarat); $i++) {
			$dataMk[] = $prasyarat[$i];
		}

		$data = $this->temph_model->cek_prasyarat($dataMk, $this->uid)->result();

		// STKIP Bima untuk mhs konversi tidak ada perbedaan format NPM
		/*$isConversionStudent = substr($this->uid, 8,1);
		if ($isConversionStudent == 7 && count($data) == 0) {
			$data = $this->temph_model->cek_prasyarat_konversi($dataMk, $this->uid)->result();
		}*/

		$isEqual = count($data) == count($prasyarat);

		if (!$isEqual) {
			for ($i = 0; $i < count($prasyarat); $i++) {
				$getConversionCourse = $this->db->query("SELECT kd_matakuliah FROM tbl_matakuliah 
														WHERE konversi_matakuliah LIKE '%$prasyarat[$i]%'
														AND kd_prodi = '$prodi'")->row();
				$setConversionCourse[] = isset($getConversionCourse) ? $getConversionCourse->kd_matakuliah : NULL;
			}
			$data = $this->temph_model->cek_prasyarat($getConversionCourse, $this->uid)->result();
			$isEqual = count($data) == count($setConversionCourse);
		}

		echo $isEqual ? 1 : 0;
	}

	function get_krs()
	{
		$semester 	= $_POST['semester'];
		$npm 		= $_POST['npm'];

		$data = $this->app_model->get_matkul_krs_rekam($semester, $npm)->result();

		$js = json_encode($data);
		echo $js;
	}

	function saving()
	{
		$kode_krs 		= $this->input->post('kode_krs', TRUE);
		$kd_matakuliah 	= $this->input->post('kd_matkuliah', TRUE);
		$sumsks 		= $this->input->post('jumlah_sks', TRUE);

		if (count($kd_matakuliah) == 0) {
			echo "<script>alert('Pilih Matakuliah Terlebih Dahulu!');
			document.location.href='" . base_url('akademik/krs_mhs/pengisian_krs') . "';
			</script>";
			return;
		}

		if ($sumsks > 24 || $sumsks < 1) {
			echo "<script>alert('Jumlah SKS Tidak Diizinkan');
			document.location.href='" . base_url('akademik/krs_mhs/pengisian_krs') . "';
			</script>";
			return;
		}

		$actyear = getactyear();

		$isKrsExist = $this->db->like('kd_krs', $this->uid . $actyear, 'AFTER')->get('tbl_verifikasi_krs');

		//sudah ada krs
		if ($isKrsExist->num_rows() > 0) {
			// if (!empty($kode_krs)) {
			$this->_have_fill_krs($kd_matakuliah, $isKrsExist->row()->kd_krs, $this->uid, $sumsks, getactyear());

			// belum ada ada KRS
		} else {
			$this->_first_krs_filling($kd_matakuliah, $this->uid, $sumsks);
		}
	}

	/**
	 * untuk handle pengisian KRS bagi mahasiswa yang telah mengisi form KRS sebelumnya
	 * @param array $kodemk
	 * @param string $kodekrs
	 * @param string $nim
	 * @param string $tahunajaran
	 * @return void
	 */
	protected function _have_fill_krs($kodemk, $kodekrs, $nim, $sumsks, $tahunajaran)
	{
		$kode_krs = $kodekrs;
		$kd_matakuliah = $kodemk;

		/*=========================================================
		=            Penyesuaian penyimpanan matakuliah            =
		=========================================================*/
		/**
		 * Ketika update matakuliah, agar tidak menghapus jadwal yang telah dipilih
		 * pada matakuliah sebelumnya, maka dilakukan proses pengecekan.
		 * Bagi matakuliah yang sebelumnya telah diisikan jadwal tidak akan terhapus.
		 */

		/*----------  ambil data KRS dan kolektifkan dalam array  ----------*/
		$getkrsbefore2 = $this->app_model->getkrsbefore2($kode_krs)->result();
		foreach ($getkrsbefore2 as $vals) {
			$bulk[] = $vals->kd_matakuliah;
		}

		/*
		* bandingkan antara KRS exist dengan KRS baru
		* jika pada KRS baru tidak terdapat salah satu data pada KRS exist
		* maka hapus data yang tidak ada tersebut dari KRS exist
		*/
		foreach ($bulk as $keys) {
			if (!in_array($keys, $kd_matakuliah)) {
				$this->db->where('kd_krs', $kode_krs);
				$this->db->where('kd_matakuliah', $keys);
				$this->db->delete('tbl_krs');
			}
		}

		/*
		* bandingkan antara KRS baru dengan KRS exist
		* jika terdapat data baru yang belum ada pada KRS exist
		* maka tambahkan data tersebut kedalam KRS exist
		*/
		foreach ($kd_matakuliah as $code) {
			if (!in_array($code, $bulk)) {
				$obj['npm_mahasiswa'] = $nim;
				$obj['kd_matakuliah'] = $code;
				$obj['kd_krs'] = $kode_krs;
				$obj['semester_krs'] = $this->input->post('semester', TRUE);
				$obj['tahunakademik'] = $tahunajaran;
				$this->db->insert('tbl_krs', $obj);
			}
		}

		/*=====  End of Adjustment penyimpanan matakuliah  ======*/

		// insert to tbl_verifikasi_krs
		$this->_insert_to_verifikasi_krs($kode_krs, $nim, $sumsks, $tahunajaran, TRUE);

		// redirect to schedule filling
		redirect(base_url('akademik/krs_mhs/save_sesi/' . $nim . '/' . $kode_krs));
	}

	/**
	 * untuk handle pengisian KRS baru
	 * @param string $kd_matakuliah
	 * @param strng $nim
	 * @param int $sumsks
	 * @return void
	 */
	protected function _first_krs_filling($kd_matakuliah, $nim, $sumsks)
	{
		$tahunajaran = getactyear();
		$kodeawal    = $nim . $tahunajaran;
		$randomSufix = rand(1, 9);
		$kode_krs    = $nim . $tahunajaran . $randomSufix;

		foreach ($kd_matakuliah as $key => $value) {
			$krs[] = [
				'npm_mahasiswa' => $nim,
				'semester_krs' 	=> $this->input->post('semester', TRUE),
				'kd_matakuliah' => $value,
				'kd_krs' 		=> $kode_krs
			];
		}

		// remove duplicate data from multidimensional array
		$removeDuplicate = array_map("unserialize", array_unique(array_map("serialize", $krs)));
		$datakrs = $removeDuplicate;
		$this->db->insert_batch('tbl_krs', $datakrs);

		// insert krs transaction to tbl_verifikasi_krs
		$this->_insert_to_verifikasi_krs($kode_krs, $nim, $sumsks, $tahunajaran);

		// redirect to schedule filling
		redirect(base_url('akademik/krs_mhs/save_sesi/' . $nim . '/' . $kode_krs));
	}

	/**
	 * handle proses untuk insert ke tbl_verifikasi_krs
	 * @param string $kodeKrs
	 * @param string $nim
	 * @param string $tahunajaran
	 * @param bool $condition
	 * @param string $kelas
	 * @return void
	 */
	protected function _insert_to_verifikasi_krs($kodeKrs, $nim, $sumsks, $tahunajaran, $condition = FALSE)
	{
		/*----------  terkadang field kelas kosong, maka dibuat fungsi untuk menanganinya  ----------*/
		$sessionClass = $this->session->userdata('kelas');

		if (is_null($sessionClass) || empty($sessionClass) || !$sessionClass || !isset($sessionClass)) {
			$cls 	= $this->db->query("SELECT kelas_mhs FROM tbl_pa WHERE npm_mahasiswa = '$nim'")->row()->kelas_mhs;
			$kelas 	= $cls;
		} else {
			$kelas 	= $sessionClass;
		}

		$cekdosen  = $this->db->query("SELECT kd_dosen FROM tbl_pa WHERE npm_mahasiswa = '$nim'")->row()->kd_dosen;
		$kodedosen = $cekdosen;

		$vkrs['id_pembimbing'] 	= $kodedosen;
		$vkrs['kd_krs'] 		= $kodeKrs;
		$vkrs['npm_mahasiswa'] 	= $nim;
		$vkrs['kd_jurusan'] 	= get_mhs_jur($nim);
		$vkrs['tgl_bimbingan'] 	= date('Y-m-d H:i:s');
		$vkrs['key'] 			= $this->generateRandomString();
		$vkrs['tahunajaran'] 	= $tahunajaran;
		$vkrs['jumlah_sks'] 	= $sumsks;

		$kodver 				= $kodeKrs . $sumsks . $vkrs['key'];
		$vkrs['kd_krs_verifikasi'] = md5(md5($kodver) . key_verifikasi);

		$nama 				= get_nm_mhs($nim);
		$slug 				= url_title($nama, '_', TRUE);
		$vkrs['slug_url'] 	= $slug;
		$vkrs['kelas'] 		= $kelas;
		$vkrs 				= $this->security->xss_clean($vkrs);

		// jika insert setelah proses update
		if ($condition) {
			$this->app_model->deletedata('tbl_verifikasi_krs', 'kd_krs', $kodeKrs);
		}

		$this->app_model->insertdata('tbl_verifikasi_krs', $vkrs);

		// generate QR Code
		$this->_QRCodeGenerator($vkrs['kd_krs_verifikasi']);
	}

	protected function _QRCodeGenerator($kodeVerifikasiKrs)
	{
		$this->load->library('ciqrcode');
		$params['data']     = 'http://sia.'.$this->URL.'/welcome/welcome/cekkodeverifikasi/' . $kodeVerifikasiKrs . '';
		$params['level']    = 'H';
		$params['size']     = 10;
		$params['savename'] = FCPATH . 'QRImage/' . $kodeVerifikasiKrs . '.png';
		$this->ciqrcode->generate($params);
	}

	function save_sesi($npm, $kdkrs)
	{
		$array = array(
			'npm'    => $npm,
			'kd_krs' => $kdkrs
		);

		$this->session->set_userdata('sess_for_jdl', $array);
		redirect(base_url('akademik/krs_mhs/jadwal_kuliah'), 'refresh');
	}

	function jadwal_kuliah()
	{
		$log_jdl = $this->session->userdata('sess_for_jdl');
		$user = $this->session->userdata('sess_login');

		if ($this->session->userdata('sessdirectlogin')) {
			$logs  = $this->session->userdata('sessdirectlogin');
			$kdkrs = $logs['kdkrs'];
		} else {
			$kdkrs = $log_jdl['kd_krs'];
		}

		if (substr($kdkrs, 0, 9) != $this->uid) {
			$this->session->unset_userdata('sess_login');
			echo "<script>alert('Akses Tidak Diizinkan');document.location.href='" . base_url() . "auth';</script>";
		} else {
			$cekdata = $this->db->query("SELECT * from tbl_krs where kd_krs = '" . $kdkrs . "'")->result();
			if (count($cekdata) == 0) {
				echo "<script>alert('Data Tidak Ditemukan !');document.location.href='" . base_url('akademik/krs_mhs') . "';</script>";
			} else {
				$data['npm_mahasiswa'] 	= substr($kdkrs, 0, 9);
				$data['kd_krs']			= $kdkrs;

				$data['pembimbing'] = $this->app_model->get_pembimbing_krs($kdkrs)->row_array();
				$tahunakademik 		= getactyear();
				$data['ta']			= $tahunakademik;
				$data['detail_krs'] = $this->app_model->get_detail_print_krs_mahasiswa($kdkrs)->result();

				// buat komparasi jumlah matakuliah yang sudah dijadwalkan
				$data['comparemk'] 	= $this->db->query("SELECT count(npm_mahasiswa) as mk from tbl_krs 
														where kd_krs like '" . $this->uid . $tahunakademik . "%'")->row()->mk;

				$data['comparejd'] 	= $this->db->query("SELECT count(npm_mahasiswa) as jd from tbl_krs 
														where kd_krs like '" . $this->uid . $tahunakademik . "%' 
														and (kd_jadwal != NULL or kd_jadwal != '') ")->row()->jd;

				$data['page'] = 'akademik/krs_mhs_jdl';
				$this->load->view('template/template', $data);
			}
		}
	}

	function ajukan_krs($kd_krs)
	{
		/*$emaildosen = $this->krs_model->emaildosen($kd_krs)->result();

		if (count($emaildosen) == 0) {
			echo "<script>alert('E-Mail dosen pembimbing anda belum terdaftar! Mohon hubungi dosen anda untuk melanjutkan pengajuan KRS!');document.location.href='" . base_url() . "akademik/krs_mhs/jadwal_kuliah';</script>";
			return;
		} else {*/

			$npm_mahasiswa = substr($kd_krs, 0, 9);

			$object = array('status_verifikasi' => 4);
			$this->db->where('kd_krs', $kd_krs);
			$this->db->update('tbl_verifikasi_krs', $object);

			echo "<script>alert('Berhasil diajukan!')</script>";
			redirect('akademik/krs_mhs/jadwal_kuliah', 'refresh');
		/*}*/
	}

	function generateRandomString()
	{
		$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
		$charactersLength = strlen($characters);
		$randomString = '';
		for ($i = 0; $i < 5; $i++) {
			$randomString .= $characters[rand(0, $charactersLength - 1)];
		}
		return $randomString;
	}

	function get_jadwal()
	{
		$tahunakademik = getactyear();
		$kd_matakuliah = $_POST['kd_matakuliah'];

		$cls = $this->krs_model->getactvkrs($this->uid . $tahunakademik)->row();

		if (!$this->session->userdata('kelas')) {
			$kelas = $cls->kelas;
		} else {
			$kelas = $this->session->userdata('kelas');
		}

		$mhs  = $this->app_model->get_jurusan_mhs($this->uid)->row();
		// echo $kelas;
		$data = $this->app_model->get_pilih_jadwal_krs($kd_matakuliah, $mhs->KDPSTMSMHS, $tahunakademik, $kelas)->result();

		$js = json_encode($data);
		echo $js;
	}

	function save_jadwal()
	{
		$data['kd_jadwal'] = $this->input->post('kd_jadwal');
		$kd_matakuliah = $this->input->post('kd_matakuliah');
		$kd_krs = $this->input->post('kd_krs');

		$npm_mahasiswa = substr($kd_krs, 0, 9);
		//die($npm_mahasiswa);

		$this->app_model->updatedata_krs($kd_matakuliah, $kd_krs, $data);
		//redirect('form/formkrs/jadwal_kuliah','refresh');

		$this->jadwal_kuliah($npm_mahasiswa, $kd_krs);
	}

	function kosongkan_jadwal($id, $id2)
	{
		$data = array('kd_jadwal' => null);

		$this->db->where('kd_krs', $id);
		$this->db->where('kd_matakuliah', $id2);
		$this->db->update('tbl_krs', $data);

		$q = $this->db->select('kd_krs')
			->from('tbl_krs')
			->where('id_krs', $id)
			->get()->row();

		redirect(base_url() . 'akademik/krs_mhs/jadwal_kuliah/' . $this->uid . '/' . $id, 'refresh');
	}

	function verify()
	{
		$id 	= $this->input->post('username', TRUE);
		$cek 	= $this->input->post('g-recaptcha', TRUE);

		if ($_POST['g-recaptcha-response'] != '') {
			$secret = '6LeLLAoUAAAAAHQPTHRFKtaMGVexvRQM1_5wD4c4';
			//get verify response data
			$verifyResponse = file_get_contents('https://www.google.com/recaptcha/api/siteverify?secret=' . $secret . '&response=' . $_POST['g-recaptcha-response']);
			$responseData = json_decode($verifyResponse);
			if ($responseData->success) {
				echo 'Your contact request have submitted successfully.';;
			} else {
				echo "salah ces";
			}
		} else {
			echo "salah ces";
		}
		
		exit();
	}

	function view($npm, $id)
	{
		$data['npm'] = $npm;

		$data['ips'] = $this->app_model->get_ips_mahasiswa($npm, $id)->row()->ips;

		$data['mhs'] = $this->app_model->getdetail('tbl_mahasiswa', 'NIMHSMSMHS', $npm, 'NIMHSMSMHS', 'asc')->row();

		$data['semester'] = $id;
		$data['kode'] = $this->db->query("SELECT distinct kd_krs from tbl_krs where npm_mahasiswa = '" . $npm . "' and semester_krs = " . $id . " ")->row();

		$data['detail_khs'] = $this->krs_model->detailkhs($npm, $id, $data['mhs']->KDPSTMSMHS)->result();
		$data['page'] = 'akademik/khs_detail2';
		$this->load->view('template/template', $data);
	}

	function load_khs($kd)
	{
		$data['mhs'] = $this->app_model->getdetail('tbl_mahasiswa', 'NIMHSMSMHS', substr($kd, 0, 9), 'NIMHSMSMHS', 'asc')->row();
		$data['q'] = $this->db->query("SELECT DISTINCT a.*,b.nama_matakuliah,b.sks_matakuliah FROM tbl_krs a 
										JOIN tbl_matakuliah b ON a.kd_matakuliah = b.kd_matakuliah 
										WHERE a.kd_krs LIKE '{$kd}%' 
										AND b.kd_prodi = '{$data['mhs']->KDPSTMSMHS}'")->result();
		$sks = 0;
		foreach ($data['q'] as $key) {
			$sks = $sks + $key->sks_matakuliah;
		}
		$data['jumsks'] = $sks;

		$thn = substr($kd, 9, 5);
		$data['tahunakad'] = $thn;

		$ips_nr = $this->krs_model->countips($data['mhs']->KDPSTMSMHS, substr($kd, 0, 9), $thn);
		$data['ipsmhs_before'] = $ips_nr;

		$data['smt'] = $this->db->query("SELECT semester_krs from tbl_krs where kd_krs like '" . $kd . "%'")->row()->semester_krs;

		$this->load->view('detil_khs_modal', $data);
	}
}

/* End of file bimbingan.php */
/* Location: ./application/modules/akademik/controllers/bimbingan.php */
