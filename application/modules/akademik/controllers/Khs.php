<?php
defined('BASEPATH') or exit('No direct script access allowed');
// error_reporting(0);
class Khs extends MY_Controller
{
	public $userid, $usergroup;
	function __construct()
	{
		parent::__construct();
		$this->load->library('Cfpdf');
		$this->load->model('krs_model');
		if ($this->session->userdata('sess_login')) {
			$cekakses = $this->role_model->cekakses(61)->result();
			if (!$cekakses) {
				echo "<script>alert('Anda Tidak Berhak Mengakses !!');history.go(-1);</script>";
				exit();
			}
			$this->userid = $this->session->userdata('sess_login')['userid'];
			$this->usergroup = $this->session->userdata('sess_login')['id_user_group'];
		} else {
			redirect('auth', 'refresh');
		}
	}

	function index()
	{
		$grup = get_group($this->usergroup);
		if ((in_array(8, $grup)) or (in_array(19, $grup))) {
			$data['getData'] = $this->app_model->getdetail('tbl_mahasiswa', 'KDPSTMSMHS', $this->userid, 'NIMHSMSMHS', 'asc')->result();
			$data['page'] = 'akademik/khs_select';
		} elseif ((in_array(5, $grup))) {
			$data['mhs'] = $this->app_model->getdetail('tbl_mahasiswa', 'NIMHSMSMHS', $this->userid, 'NIMHSMSMHS', 'asc')->row();
			$data['detail_khs'] = $this->app_model->get_all_khs_mahasiswa($this->userid)->result();
			$data['page'] = 'akademik/khs_view';
		} elseif ((in_array(9, $grup))) {
			$data['jurusan'] = $this->app_model->getdetail('tbl_jurusan_prodi', 'kd_fakultas', $this->userid, 'prodi', 'asc')->result();
			$data['page'] = 'akademik/khs_fak';
		} else {
			$data['fakultas'] = $this->app_model->getdata('tbl_fakultas', 'id_fakultas', 'ASC')->result();
			$data['page'] = 'akademik/khs_fak';
		}
		$this->load->view('template/template', $data);
	}

	function dp_view($npm)
	{
		$data['mhs'] = $this->app_model->getdetail('tbl_mahasiswa', 'NIMHSMSMHS', $npm, 'NIMHSMSMHS', 'asc')->row();
		$data['detail_khs'] = $this->app_model->get_all_khs_mahasiswa($npm)->result();
		$data['page'] = 'akademik/khs_view';
		$this->load->view('template/template', $data);
	}

	function viewkhs($id)
	{
		$this->load->model('monitoring_model');
		$grup   = get_group($this->usergroup);
		$userid = in_array(5, $grup) ? $this->userid : $id;

		$data['tahunakademik'] = getactyear();
		$data['mhs'] = $this->app_model->getdetail('tbl_mahasiswa', 'NIMHSMSMHS', $userid, 'NIMHSMSMHS', 'asc')->row();
		$data['detail_khs'] = $this->app_model->get_all_khs_mahasiswa($userid)->result();
		$data['detail_khs_konversi'] = $this->monitoring_model->get_all_khs_mahasiswa_konversi($userid)->result();
		$data['page'] = 'akademik/khs_view';
		$this->load->view('template/template', $data);
	}

	function detailkhs($npm, $tahunakademik)
	{
		$logged = $this->session->userdata('sess_login');
		$grup = get_group($this->usergroup);

		if ($this->userid != $npm && in_array(5, $grup)) {
			echo "<script>alert(Access Denied!);history.go(-1);</script>";
			redirect(base_url(), 'refresh');
		}

		$userid = in_array(5, $grup) ? $this->userid : $npm;

		$data['npm'] = $userid;
		$data['mhs'] = $this->app_model->getdetail('tbl_mahasiswa', 'NIMHSMSMHS', $userid, 'NIMHSMSMHS', 'asc')->row();
		$data['semester'] = $this->app_model->get_semester_khs($data['mhs']->SMAWLMSMHS, $tahunakademik);
		$data['tahunakademik'] = $tahunakademik;

		if ($tahunakademik < '20151') {
			$data['detail_khs'] = $this->db->query("SELECT distinct 
														b.kd_matakuliah,
														b.nama_matakuliah,
														b.sks_matakuliah 
													from tbl_transaksi_nilai a 
													join tbl_matakuliah_copy b on a.KDKMKTRLNM = b.kd_matakuliah
													where a.THSMSTRLNM = '" . $tahunakademik . "' 
													and b.tahunakademik = '" . $tahunakademik . "' 
													and a.NIMHSTRLNM = '" . $userid . "' 
													and b.kd_prodi = '" . $data['mhs']->KDPSTMSMHS . "'")->result();

			$data['kode'] = $this->db->query("SELECT distinct kd_krs from tbl_krs where kd_krs like '" . $userid . $tahunakademik . "%'")->row();
		} else {

			$data['detail_khs'] = $this->db->query("SELECT distinct 
														a.npm_mahasiswa, 
														a.kd_matakuliah, 
														a.semester_krs, 
														a.kd_krs, 
														a.kd_jadwal,
														b.nama_matakuliah,
														b.sks_matakuliah 
													from tbl_krs a 
													join tbl_matakuliah b on a.kd_matakuliah = b.kd_matakuliah 
													where a.kd_krs like '" . $userid . $tahunakademik . "%' 
													and b.kd_prodi = '" . $data['mhs']->KDPSTMSMHS . "'")->result();

			$data['kode'] = $this->db->query("SELECT distinct kd_krs from tbl_krs where kd_krs like '" . $userid . $tahunakademik . "%'")->row();
		}

		$data['page'] = 'akademik/khs_detail';
		$this->load->view('template/template', $data);
	}

	function detailKhs_konversi($npm, $id)
	{
		$logged = $this->session->userdata('sess_login');
		$pecah = explode(',', $logged['id_user_group']);
		$jmlh = count($pecah);
		for ($i = 0; $i < $jmlh; $i++) {
			$grup[] = $pecah[$i];
		}

		if ((in_array(5, $grup))) {
			$nim = $logged['userid'];
		} else {
			$nim = $npm;
		}

		$data['npm'] = $nim;
		$data['mhs'] = $this->app_model->getdetail('tbl_mahasiswa', 'NIMHSMSMHS', $nim, 'NIMHSMSMHS', 'asc')->row();

		if ($id < '20151') {
			$data['detail_khs'] = $this->db->query("SELECT distinct 
														b.kd_matakuliah,
														b.nama_matakuliah,
														b.sks_matakuliah 
													from tbl_transaksi_nilai_konversi a 
													join tbl_matakuliah_copy b on a.KDKMKTRLNM = b.kd_matakuliah 
													where a.THSMSTRLNM = '" . $id . "' 
													and b.tahunakademik = '" . $id . "' 
													and a.NIMHSTRLNM = '" . $nim . "' 
													and b.kd_prodi = '" . $data['mhs']->KDPSTMSMHS . "'")->result();
		} else {
			$data['detail_khs'] = $this->db->query("SELECT distinct 
														b.kd_matakuliah,
														b.nama_matakuliah,
														b.sks_matakuliah 
													from tbl_transaksi_nilai_konversi a 
													join tbl_matakuliah b on a.KDKMKTRLNM = b.kd_matakuliah 
													where a.THSMSTRLNM = '" . $id . "'  
													and a.NIMHSTRLNM = '" . $nim . "' 
													and b.kd_prodi = '" . $data['mhs']->KDPSTMSMHS . "'")->result();
		}
		$data['tahunakademik'] = $id;
		$data['page'] = 'akademik/khs_detail_konversi';
		$this->load->view('template/template', $data);
	}

	function nilai_detail($jadwal, $nim)
	{
		$jdw = get_kd_jdl($jadwal);
		$data['avg'] = $this->app_model->getnilaix($jdw, $nim, 1);
		$data['absen'] = $this->app_model->getnilaix($jdw, $nim, 2);
		$data['uts'] = $this->app_model->getnilaix($jdw, $nim, 3);
		$data['uas'] = $this->app_model->getnilaixuas($jdw, $nim, 4);
		$data['akhir'] = $this->app_model->getnilaix($jdw, $nim, 10);
		$data['general'] = $this->app_model->getnilaixxx($jdw, $nim);
		$this->load->view('modal_nilai', $data);
	}

	function nilai_old($jadwal, $nim)
	{
		$logged = $this->session->userdata('sess_login');
		$jdw = get_kd_jdl($jadwal);
		$data['avg'] = $this->app_model->getnilaixy($jdw, $nim, 1);
		$data['absen'] = $this->app_model->getnilaixy($jdw, $nim, 2);
		$data['uts'] = $this->app_model->getnilaixy($jdw, $nim, 3);
		$data['uas'] = $this->app_model->getnilaixyuas($jdw, $nim, 4);
		$data['akhir'] = $this->app_model->getnilaixy($jdw, $nim, 10);
		$data['general'] = $this->app_model->getnilaixxxy($jdw, $nim);
		$this->load->view('modal_nilai', $data);
	}

	function viewkhsprodi()
	{
		$logged = $this->session->userdata('sess_login');
		$pecah = explode(',', $logged['id_user_group']);
		$jmlh = count($pecah);
		for ($i = 0; $i < $jmlh; $i++) {
			$grup[] = $pecah[$i];
		}
		if ((in_array(8, $grup) or in_array(19, $grup))) {
			$prodi = $logged['userid'];
		} else {
			$prodi = $this->input->post('jurusan', TRUE);
		}
		$tahun = $this->input->post('tahun', TRUE);
		$this->session->set_userdata('tahun_ak', $tahun);
		$data['getData']	= $this->db->query("SELECT * FROM tbl_mahasiswa 
												WHERE TAHUNMSMHS = '" . $tahun . "' 
												AND KDPSTMSMHS = '" . $prodi . "' ")->result();
		$data['page'] = 'akademik/khs_prodi';
		$this->load->view('template/template', $data);
	}

	function printkhs($npm, $id)
	{
		if ($this->usergroup === '5' && $npm != $this->userid) {
			echo '<script>alert("Akses tidak diizinkan! ID pengguna tidak sama dengan NPM.");history.go(-1);</script>';
			exit();
		}
		
		$grup = get_group($this->usergroup);
		$prodi = $this->userid;

		if (in_array(5, $grup)) {
			$getprodi = $this->app_model->getdetail('tbl_mahasiswa', 'NIMHSMSMHS', $this->userid, 'NIMHSMSMHS', 'asc')->row();
			$prodi = $getprodi->KDPSTMSMHS;
		}

		$data['ta'] = $this->db->query("SELECT DISTINCT kd_krs FROM tbl_krs WHERE npm_mahasiswa = '{$npm}' AND semester_krs = '{$id}'")->row();

		$tahunakademik = substr($data['ta']->kd_krs, 9, 5);
		$data['semester'] = $id;
		$data['npm'] = $npm;
		if ($tahunakademik < 20151) {
			$data['krs'] 	= $this->db->query('SELECT DISTINCT b.nama_matakuliah, b.sks_matakuliah, b.kd_matakuliah
												FROM tbl_krs a 
												JOIN tbl_matakuliah_copy b ON a.`kd_matakuliah` = b.`kd_matakuliah` 
												where a.`npm_mahasiswa` = ' . $npm . ' 
												and b.kd_prodi = ' . $prodi . ' 
												and a.semester_krs = ' . $id . '
												and b.tahunakademik = "' . $tahunakademik . '"')->result();
		} else {
			$data['krs'] 	= $this->db->query("SELECT DISTINCT b.nama_matakuliah, b.sks_matakuliah, b.kd_matakuliah
												FROM tbl_krs a 
												JOIN tbl_matakuliah b ON a.`kd_matakuliah` = b.`kd_matakuliah` 
												where a.`npm_mahasiswa` = '{$npm}' 
												and b.kd_prodi = '{$prodi}' 
												and a.semester_krs = '{$id}'")->result();
		}

		$this->load->view('welcome/print/khs_pdf', $data);
	}

	function get_jurusan($id)
	{
		$jurusan = $this->app_model->getdetail('tbl_jurusan_prodi', 'kd_fakultas', $id, 'id_prodi', 'ASC')->result();
		$out = "<select class='form-control' name='jurusan' id='jurs'><option>--Pilih Jurusan--</option>";
		foreach ($jurusan as $row) {
			$out .= "<option value='" . $row->kd_prodi . "'>" . $row->prodi . "</option>";
		}
		$out .= "</select>";
		echo $out;
	}

	function transkrip($npm)
	{
		$id_user_group = $this->session->userdata('sess_login')['id_user_group']; //user group mahasiswa => 5

		if ($id_user_group == 5 && $npm != $this->userid) {
			echo '<script>alert("Akses tidak diizinkan! ID pengguna tidak sama dengan NPM.");history.go(-1);</script>';
			exit();
		}

		$data['head'] = $this->db->query('SELECT * from tbl_mahasiswa a join tbl_jurusan_prodi b
										on a.`KDPSTMSMHS`=b.`kd_prodi` join tbl_fakultas c
										on b.`kd_fakultas`=c.`kd_fakultas`
										where a.`NIMHSMSMHS` = "' . $npm . '"')->row();
		$q = '';
		$sql = $this->_generate_transkrip_sql($npm);
		$q = $this->db->query($sql)->result();
		
		
		$arrTranskrip = [];

		$arrTranskrip = $this->_filterData($q);

		// sort array ASC by its semester
		$sortResult = usort($arrTranskrip,function($a, $b) {
			return $a->THSMSTRLNM - $b->THSMSTRLNM;
		});

		$q = $arrTranskrip;
		// is student have a conversion poin ?
		$isConversionPoinExist = $this->db->where('NIMHSTRLNM', $npm)->where('deletedAt')->get('tbl_transaksi_nilai_konversi')->num_rows();
		$data['isConversionPoinExist'] = $isConversionPoinExist;

		$q_konversi = '';
		if ($isConversionPoinExist > 0) {
			
			$sql = $this->_generate_transkrip_sql($npm, $isConversionPoinExist);
			$q_konversi = $this->db->query($sql)->result();
			
			$arrTranskrip = [];

			$arrTranskrip = $this->_filterData($q_konversi);
			$q_konversi = $arrTranskrip;
		}

		$data['q_konversi'] = $q_konversi;
		$data['q'] 	= $q;
		$data['npm'] = $npm;
		$this->load->view('transkrip_pdf3', $data);
	}

	/**
	 * Generate SQL Script untuk mengambil data nilai
	 * @param  integer  $npm                   [NPM]
	 * @param  integer  $isConversionPoinExist [konversi atau bukan]
	 * @return string   SQL script             [sql script]
	 */
	function _generate_transkrip_sql($npm,$isConversionPoinExist = 0)
	{
		$tableName = 'tbl_transaksi_nilai';

		if ($isConversionPoinExist > 0) {
			$tableName = 'tbl_transaksi_nilai_konversi';
		}

		$sql = "SELECT 
					nl.`KDKMKTRLNM`,
					IF(nl.`THSMSTRLNM` < '20151',
						(SELECT DISTINCT nama_matakuliah FROM tbl_matakuliah_copy 
						WHERE kd_matakuliah = nl.`KDKMKTRLNM`
						AND kd_prodi = nl.`KDPSTTRLNM`  
						ORDER BY id_matakuliah DESC LIMIT 1), 
						(SELECT DISTINCT nama_matakuliah FROM tbl_matakuliah 
						WHERE kd_matakuliah = nl.`KDKMKTRLNM` 
						AND kd_prodi = nl.`KDPSTTRLNM` 
						ORDER BY id_matakuliah DESC LIMIT 1) ) AS nama_matakuliah,
					IF(nl.`THSMSTRLNM` < '20151',
						(SELECT DISTINCT sks_matakuliah FROM tbl_matakuliah_copy
						WHERE kd_matakuliah = nl.`KDKMKTRLNM` 
						AND kd_prodi = nl.`KDPSTTRLNM` 
						ORDER BY id_matakuliah DESC LIMIT 1) ,
						(SELECT DISTINCT sks_matakuliah FROM tbl_matakuliah 
						WHERE kd_matakuliah = nl.`KDKMKTRLNM` 
						AND kd_prodi = nl.`KDPSTTRLNM` 
						ORDER BY id_matakuliah DESC LIMIT 1) ) AS sks_matakuliah,
					nl.`NLAKHTRLNM` AS NLAKHTRLNM,
					nl.`THSMSTRLNM` AS THSMSTRLNM,
					nl.`BOBOTTRLNM` AS BOBOTTRLNM,
					deletedAt 
				FROM $tableName nl 
				WHERE nl.`NIMHSTRLNM` = '$npm' AND deletedAt is null 
				ORDER BY KDKMKTRLNM, NLAKHTRLNM, THSMSTRLNM ASC ";
		return $sql;
	}

	/**
	 * Filter data from database
	 * Mendapatkan nilai tertinggi,
	 * Bobot, SKS, dan tahun menyesuaikan dari nilai tertinggi
	 * @param  [object] $data
	 * @return Object 
	 */
	function _filterData($data)
	{
		$arrTranskrip = [];
		$kodeMk = '';
		$tmpNilai = '';
		$tmpBobot = '';
		$tmpTahun = '';
		$tmpSks = '';

		foreach ($data as $key => $value) {
			//set row
			if (empty($kodeMk)) {
				$kodeMk = $value->KDKMKTRLNM;
				$arrTranskrip[] = $value;
				$tmpNilai = $value->NLAKHTRLNM;
				$tmpBobot = $value->BOBOTTRLNM;
				$tmpTahun = $value->THSMSTRLNM;
				$tmpSks = $value->sks_matakuliah;
			}elseif (!empty($kodeMk)) {
				if ($kodeMk == $value->KDKMKTRLNM) {
					$nilai = $this->_compare_value($tmpNilai, $value->NLAKHTRLNM); //Mencari nilai tertinggi
					if ($value->BOBOTTRLNM > $nilai[1]) {
						// mendapatkan key kode matakuliah yang akan di replace nilainya
						$keys = array_search($value->KDKMKTRLNM, array_map(function($v){return $v->KDKMKTRLNM;},$arrTranskrip));

						$arrTranskrip[$keys]->NLAKHTRLNM = $nilai[0];
						$arrTranskrip[$keys]->BOBOTTRLNM = $value->BOBOTTRLNM;
						$arrTranskrip[$keys]->THSMSTRLNM = $value->THSMSTRLNM;
						$arrTranskrip[$keys]->sks_matakuliah = $value->sks_matakuliah;
					}
					

					$kodeMk = '';
					$tmpNilai = '';
					$tmpBobot = '';
					$tmpTahun = '';
					$tmpSks = '';
				}elseif ($kodeMk !== $value->KDKMKTRLNM) {
					$kodeMk = $value->KDKMKTRLNM;
					$arrTranskrip[] = $value;
					$tmpNilai = $value->NLAKHTRLNM;
				}
			}
		}
		
		return $arrTranskrip;
	}

	/**
	 * Compare Value
	 * Membandingkan 2 Nilai dari Kode Matakuliah yang sama dan mencari nilai tertinggi
	 * @param  [string] $val1 [nilai pertama]
	 * @param  [string] $val2 [nilai kedua]
	 * @return Object       [mengembalikan nilai tertinggi beserta bobotnya]
	 */
	function _compare_value($val1, $val2)
	{
		$arrValue = [
			'A'  => 4.00,
			'A-' => 3.70,
			'B+' => 3.30,
			'B'  => 3.00,
			'B-' => 2.70,
			'C+' => 2.30,
			'C'  => 2.00,
			'D'  => 1.00,
			'E'  => 0.00,
			'T'  => 0.00,
		];

		$maxValue = max($arrValue[$val1], $arrValue[$val2]);
		$result = [array_search($maxValue, $arrValue), $maxValue];
		return $result;
	}

	function transkrip212($id)
	{
		$data['head'] = $this->db->query('SELECT * from tbl_mahasiswa a join tbl_jurusan_prodi b
										on a.`KDPSTMSMHS`=b.`kd_prodi` join tbl_fakultas c
										on b.`kd_fakultas`=c.`kd_fakultas`
										where a.`NIMHSMSMHS` = "' . $id . '"')->row();

		$this->createTempTable($id);

		$queryOld = 'SELECT nl.`KDKMKTRLNM`,
										IF(nl.`THSMSTRLNM`<"20151",(SELECT DISTINCT nama_matakuliah FROM tbl_matakuliah_copy WHERE kd_matakuliah = nl.`KDKMKTRLNM` AND kd_prodi = nl.`KDPSTTRLNM` AND tahunakademik = nl.`THSMSTRLNM` ORDER BY id_matakuliah DESC LIMIT 1) ,
											(SELECT DISTINCT nama_matakuliah FROM tbl_matakuliah WHERE kd_matakuliah = nl.`KDKMKTRLNM` AND kd_prodi = nl.`KDPSTTRLNM` ORDER BY id_matakuliah DESC LIMIT 1) ) AS nama_matakuliah,
										IF(nl.`THSMSTRLNM`<"20151",(SELECT DISTINCT sks_matakuliah FROM tbl_matakuliah_copy WHERE kd_matakuliah = nl.`KDKMKTRLNM` AND kd_prodi = nl.`KDPSTTRLNM` AND tahunakademik = nl.`THSMSTRLNM` ORDER BY id_matakuliah DESC LIMIT 1) ,
											(SELECT DISTINCT sks_matakuliah FROM tbl_matakuliah WHERE kd_matakuliah = nl.`KDKMKTRLNM` AND kd_prodi = nl.`KDPSTTRLNM` ORDER BY id_matakuliah DESC LIMIT 1) ) AS sks_matakuliah,
										MIN(nl.`NLAKHTRLNM`) AS NLAKHTRLNM,
										MAX(nl.`THSMSTRLNM`) AS THSMSTRLNM,
										MAX(nl.`BOBOTTRLNM`) AS BOBOTTRLNM,
										deletedAt 
										FROM tbl_transaksi_nilai nl 
										WHERE nl.`NIMHSTRLNM` = "' . $id . '" and deletedAt is null or deletedAt = ""
										GROUP BY nl.`KDKMKTRLNM` ORDER BY KDKMKTRLNM ASC';

		$query  = "SELECT 
					t1.NIMHSTRLNM,
				    t1.KDKMKTRLNM,
				    t1.nama_matakuliah,
				    t1.sks_matakuliah,
				    IF(strcmp(t2.NLAKHTRLNM, t1.NLAKHTRLNM) = -1, t2.NLAKHTRLNM, t1.NLAKHTRLNM) as NLAKHTRLNM,
				    IF(t1.THSMSTRLNM < t2.THSMSTRLNM, t2.THSMSTRLNM, t1.THSMSTRLNM) as THSMSTRLNM,
				    IF(t1.BOBOTTRLNM < t2.BOBOTTRLNM, t2.BOBOTTRLNM, t1.BOBOTTRLNM) as BOBOTTRLNM,
				    t1.deletedAt
				FROM
				    tmpTrans t1 
				JOIN
				    tbl_transaksi_nilai_sp t2  on t2.NIMHSTRLNM = t1.NIMHSTRLNM
				WHERE
				    t2.NIMHSTRLNM = " . $id . "
				    group by t1.KDKMKTRLNM";

		$data['q'] = $this->db->query($queryOld)->result();


		$data['q_konversi'] = $this->db->query('SELECT nl.`KDKMKTRLNM`,
										IF(nl.`THSMSTRLNM`<"20151",(SELECT DISTINCT nama_matakuliah FROM tbl_matakuliah_copy WHERE kd_matakuliah = nl.`KDKMKTRLNM` AND kd_prodi = nl.`KDPSTTRLNM`  ORDER BY id_matakuliah DESC LIMIT 1) , 
											(SELECT DISTINCT nama_matakuliah FROM tbl_matakuliah WHERE kd_matakuliah = nl.`KDKMKTRLNM` AND kd_prodi = nl.`KDPSTTRLNM` ORDER BY id_matakuliah DESC LIMIT 1) ) AS nama_matakuliah,
										IF(nl.`THSMSTRLNM`<"20151",(SELECT DISTINCT sks_matakuliah FROM tbl_matakuliah_copy WHERE kd_matakuliah = nl.`KDKMKTRLNM` AND kd_prodi = nl.`KDPSTTRLNM` ORDER BY id_matakuliah DESC LIMIT 1) ,
											(SELECT DISTINCT sks_matakuliah FROM tbl_matakuliah WHERE kd_matakuliah = nl.`KDKMKTRLNM` AND kd_prodi = nl.`KDPSTTRLNM` ORDER BY id_matakuliah DESC LIMIT 1) ) AS sks_matakuliah,
										MIN(nl.`NLAKHTRLNM`) AS NLAKHTRLNM,
										MAX(nl.`THSMSTRLNM`) AS THSMSTRLNM,
										MAX(nl.`BOBOTTRLNM`) AS BOBOTTRLNM,
										deletedAt 
										FROM tbl_transaksi_nilai_konversi nl 
										WHERE nl.`NIMHSTRLNM` = "' . $id . '"  and deletedAt is null or deletedAt = ""
										GROUP BY nl.`KDKMKTRLNM` ORDER BY THSMSTRLNM ASC
										')->result();
		$data['npm'] = $id;
		$this->load->view('transkrip_pdf3', $data);
	}

	public function createTempTable($npm = '')
	{
		$query1 = "DROP TEMPORARY TABLE IF EXISTS tmpTrans;";
		$query2 = "CREATE TEMPORARY TABLE tmpTrans engine=MEMORY 
								SELECT 
								nl.`NIMHSTRLNM` ,
								nl.`KDKMKTRLNM`,
								IF(nl.`THSMSTRLNM` < 20151,
									(SELECT DISTINCT nama_matakuliah FROM tbl_matakuliah_copy WHERE kd_matakuliah = nl.`KDKMKTRLNM` AND kd_prodi = nl.`KDPSTTRLNM` AND tahunakademik = nl.`THSMSTRLNM` ORDER BY id_matakuliah DESC LIMIT 1),
									(SELECT DISTINCT nama_matakuliah FROM tbl_matakuliah WHERE kd_matakuliah = nl.`KDKMKTRLNM` AND kd_prodi = nl.`KDPSTTRLNM` ORDER BY id_matakuliah DESC LIMIT 1)) AS nama_matakuliah,
								IF(nl.`THSMSTRLNM` < 20151,
									(SELECT DISTINCT sks_matakuliah
										FROM tbl_matakuliah_copy
										WHERE kd_matakuliah = nl.`KDKMKTRLNM` AND kd_prodi = nl.`KDPSTTRLNM` AND tahunakademik = nl.`THSMSTRLNM` ORDER BY id_matakuliah DESC LIMIT 1),
									(SELECT DISTINCT sks_matakuliah FROM tbl_matakuliah WHERE kd_matakuliah = nl.`KDKMKTRLNM` AND kd_prodi = nl.`KDPSTTRLNM` ORDER BY id_matakuliah DESC LIMIT 1)) AS sks_matakuliah,
								MIN(nl.NLAKHTRLNM) AS NLAKHTRLNM,
								MAX(nl.THSMSTRLNM) AS THSMSTRLNM,
								MAX(nl.BOBOTTRLNM) AS BOBOTTRLNM,
								deletedAt
							FROM
								tbl_transaksi_nilai nl
							WHERE
								nl.`NIMHSTRLNM` = '" . $npm . "'
									AND deletedAt IS NULL
									OR deletedAt = ''
							GROUP BY nl.KDKMKTRLNM
							ORDER BY THSMSTRLNM ASC;";
		$this->db->query($query1);
		$this->db->query($query2);
	}

	function view_edit($id)
	{
		$dataa = $this->app_model->getdetail('tbl_transaksi_nilai', 'id', $id, 'kd_transaksi_nilai', 'asc')->row();
		$data['nilai'] = $this->db->query("SELECT distinct c.`id_nilai`,c.`nilai`,c.`tipe` FROM tbl_transaksi_nilai a 
											JOIN tbl_matakuliah b ON a.`KDKMKTRLNM` = b.`kd_matakuliah`
											JOIN tbl_nilai_detail c ON a.`kd_transaksi_nilai` = c.`kd_transaksi_nilai`
											WHERE a.`kd_transaksi_nilai` = '" . $dataa->kd_transaksi_nilai . "'")->result();
		$data['mk'] = $this->db->query("SELECT b.`nama_matakuliah`,b.`kd_matakuliah` FROM tbl_transaksi_nilai a 
											JOIN tbl_matakuliah b ON a.`KDKMKTRLNM` = b.`kd_matakuliah`
											JOIN tbl_nilai_detail c ON a.`kd_transaksi_nilai` = c.`kd_transaksi_nilai`
											WHERE a.`kd_transaksi_nilai` = '" . $dataa->kd_transaksi_nilai . "'")->row();
		$this->load->view('akademik/khs_edit', $data);
	}

	function printkhsall()
	{
		$logged = $this->session->userdata('sess_login');
		$prodi = $logged['userid'];
		$data['prodi'] = $logged['userid'];
		$data['prodi'] = $prodi;
		$data['mahasiswa'] = $this->db->query('SELECT DISTINCT npm_mahasiswa as halaman FROM tbl_verifikasi_krs a
		JOIN tbl_mahasiswa b ON a.`npm_mahasiswa` = b.`NIMHSMSMHS` WHERE b.`KDPSTMSMHS` = ' . $prodi . ' and TAHUNMSMHS = ' . $this->session->userdata('tahun_ak') . ' AND a.`tahunajaran` = 20161  order by b.`NIMHSMSMHS`')->result();
		$this->load->view('welcome/print/khs_pdf_all', $data);
	}
}
/* End of file viewkrs.php */
/* Location: .//C/Users/danum246/Desktop/viewkrs.php */
