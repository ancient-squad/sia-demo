<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class AbsenDosen extends MY_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->library('Cfpdf');
	}

	public function index()
	{
		
	}

	function event($id)
	{
		$user = $this->session->userdata('sess_login');

		$nik = $user['userid'];

		$data['rows'] = $this->db->query('SELECT * FROM tbl_jadwal_matkul a
											JOIN tbl_matakuliah b ON a.`id_matakuliah` = b.`id_matakuliah` 
											JOIN tbl_karyawan c ON c.`nid` = a.`kd_dosen`
											LEFT JOIN tbl_ruangan d ON d.`id_ruangan` = a.`kd_ruangan`
											WHERE a.`id_jadwal` = '.$id.'')->row();

		if (!is_null($data['rows']) || !empty($data['rows'])) {
			$kodeprodi = substr($data['rows']->kd_jadwal, 0,5);

			$data['titles'] = $this->db->query('SELECT * FROM tbl_jurusan_prodi b
												JOIN tbl_fakultas c ON b.`kd_fakultas` = c.`kd_fakultas`
												WHERE b.`kd_prodi` = "'.$kodeprodi.'"')->row();
			$gabung = 0;

			$pos1 = stripos($data['rows']->kd_matakuliah, 'MKU');
			$pos2 = stripos($data['rows']->kd_matakuliah, 'MKDU');
			$pos3 = stripos($data['rows']->kd_matakuliah, 'MKWU');
			
			if ($pos1 !== false) 
				$gabung = 1;
			
			if ($pos2 !== false) 
				$gabung = 1;
			
			if ($pos3 !== false) 
				$gabung = 1;


			if ($data['rows']->gabung > 0) {
				$data['mhs'] = $this->db->query('SELECT distinct mhs.`NIMHSMSMHS`,mhs.`NMMHSMSMHS` FROM tbl_krs b
												JOIN tbl_mahasiswa mhs ON b.`npm_mahasiswa` = mhs.`NIMHSMSMHS`
												WHERE b.kd_jadwal = "'.$data['rows']->kd_jadwal.'" OR kd_jadwal = "'.$data['rows']->referensi.'"
												OR kd_jadwal IN (SELECT DISTINCT kd_jadwal FROM tbl_jadwal_matkul WHERE referensi = "'.$data['rows']->referensi.'")
												ORDER BY mhs.NIMHSMSMHS ASC')->result();
			} else {
				$data['mhs'] = $this->db->query('SELECT distinct mhs.`NIMHSMSMHS`,mhs.`NMMHSMSMHS` FROM tbl_krs b
												JOIN tbl_mahasiswa mhs ON b.`npm_mahasiswa` = mhs.`NIMHSMSMHS`
												WHERE b.kd_jadwal = "'.$data['rows']->kd_jadwal.'" 
												OR kd_jadwal IN (SELECT DISTINCT kd_jadwal FROM tbl_jadwal_matkul WHERE referensi = "'.$data['rows']->kd_jadwal.'")
												ORDER BY mhs.NIMHSMSMHS ASC')->result();
			}

			$data['gabung'] = $gabung;
			
			$this->load->view('beritacara', $data);
		}else{
			echo "<script>alert('Data tidak ditemukan!');window.close();</script>";
		}
	}

}

/* End of file AbsenDosen.php */
/* Location: ./application/modules/akademik/controllers/AbsenDosen.php */