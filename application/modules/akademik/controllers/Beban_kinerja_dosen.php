<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Beban_kinerja_dosen extends MY_Controller {

	protected $userid;

	public function __construct()
	{
		parent::__construct();
		if (!$this->session->userdata('sess_login')) {
			echo '<script>alert("Sesi Anda telah habis. Silahkan Log-in kembali!")</script>';
			redirect('auth','refresh');
		}
		// load model BKD
		$this->load->model('bkd_model','bkd');
		// set userid
		$this->userid = $this->session->userdata('sess_login')['userid'];
		// loac cart library
		$this->load->library('cart');
	}

	public function index()
	{
		$data['teaching']      = $this->bkd->current_teaching($this->userid);
		$data['otherTeaching'] = $this->db->get_where('tbl_pengajaran_tambahan', ['is_used' => 1])->result();
		$data['page']          = 'bkd_lecture_v';
		$this->load->view('template/template', $data);
	}

	/**
	 * See teaching detail
	 * 
	 * @return void
	 */
	public function teaching_detail()
	{
		$data['teaching'] = $this->bkd->teaching_detail($this->userid);
		$data['page']     = "teaching_detail_v";
		$this->load->view('template/template', $data);	
	}

	/**
	 * Set sks for additional teaching
	 * @param int $id
	 * @return void
	 */
	public function set_sks_teaching($id)
	{
		$sks = $this->db->select('sks')->get_where('tbl_pengajaran_tambahan', ['id' => $id])->row()->sks;
		echo $sks;
	}

	/**
	 * Set param for additonal teaching
	 * @param int $id
	 * @return void
	 */
	public function set_teaching_param($id)
	{
		$params = $this->_set_param($id);
		foreach ($params as $param) {
			$isNeedInputValidation = get_teaching_param($param)->tipe_nilai == 'NUMBER' ? "onkeypress=\"return isNumber(event)\"" : '';
			$paramType = '<div class="input-prepend input-append" style="margin-left:5px;">';
			$paramType .= '<span class="add-on">'.get_teaching_param($param)->nama.'</span>';
			$paramType .= '<input class="paramCode" type="hidden" value="'.$param.'">';
			$paramType .= '<input class="span2 params" type="text" '.$isNeedInputValidation.'>';
			$paramType .= '</div>';

			$dataParams[] = $paramType;
		}
		echo json_encode($dataParams);
	}

	/**
	 * Prepare param before serve
	 * @param int $id
	 * @return array
	 */
	protected function _set_param($id)
	{
		$params         = $this->db->select('param')->get_where('tbl_pengajaran_tambahan', ['id' => $id])->row()->param;
		$replaceBracket = str_replace(['[',']'], '', $params);
		$getUnitParam   = explode(',', $replaceBracket);
		return $getUnitParam;
	}

	/**
	 * Get list additionl teaching
	 * 
	 * @return void
	 */
	public function load_additional_teaching()
	{
		$teaches = $this->bkd->additional_teaching_list($this->userid, getactyear());
		foreach ($teaches as $teach) {
			$data[] = [
				'komponen' => $teach->komponen,
				'sks'      => $teach->sks
			];
		}
		echo json_encode($data);
	}

	/**
	 * Store additonal teaching
	 * 
	 * @return void
	 */
	public function add_additional_teaching()
	{
		extract(PopulateForm());
		$teaches = count($components);

		for ($i = 0; $i < $teaches; $i++) {
			$firstParam  = $i-$i;
			$secondParam = ($i-$i)+1;

			if (!isset($paramCode[$components[$i]][1])) {
				$paramCode2  = '';
				$paramValue2 = '';
			} else {
				$paramCode2  = $paramCode[ $components[$i] ][ $secondParam ][0];
				$paramValue2 = $paramValue[ $components[$i] ][ $secondParam ][0];
			}

			$teachingData[] = [
				'kode_pengajaran' => $components[$i],
				'param1_type'     => $paramCode[ $components[$i] ][ $firstParam ][0],
				'param1_value'    => $paramValue[ $components[$i] ][ $firstParam ][0],
				'param2_type'     => $paramCode2,
				'param2_value'    => $paramValue2,
				'sks'             => $sks[$i],
				'tahunakademik'   => getactyear(),
				'nid'             => $this->userid
			];
		}
		$this->db->insert_batch('tbl_pengajaran_tambahan_dosen', $teachingData);
		echo '<script>alert("Pengajaran berhasil disimpan");history.go(-1);</script>';
	}

	/**
	 * Load modal as view when additional teaching has to revision
	 * @param int $id
	 * @return void
	 */
	public function revision_additional_teaching($id)
	{
		$data['teach'] = $this->db->get_where('tbl_pengajaran_tambahan_dosen', ['id' => $id])->row();
		$this->load->view('modal_teaching_revision', $data);
	}

	/**
	 * Load view per tab
	 * @param string $tab
	 * @return void
	 */
	public function load_tab($tab)
	{
		switch ($tab) {
			case 'rsc':
				$this->_research_page();
				break;

			case 'dev':
				$this->_devotion_page();
				break;

			case 'oth':
				$this->_other_page();
				break;
			
			default:
				$this->_form_page();
				break;
		}
	}

	/**
	 * Show page for lecturer
	 * 
	 * @return void
	 */
	protected function _research_page()
	{
		$this->session->unset_userdata('research');
        $this->session->set_userdata('research',[]);

		$data['tahunakademik'] = $this->db->query("SELECT * FROM tbl_tahunakademik")->result();
		$data['programs']      = $this->db->query("SELECT * FROM tbl_program_penelitian")->result();
		$this->load->view('bkd_research_tab', $data);
	}

	/**
	 * Get kegiatan program by its program
	 * @param string $id
	 * @return void
	 */
	public function get_program_activity($id)
	{
		$activities = $this->db->query("SELECT * FROM tbl_kegiatan_program WHERE kode_program = '$id'")->result();

		$option = "<option value=\"\" disabled=\"\" selected=\"\"></option>";
		foreach ($activities as $activity) {
			$option .= "<option value='".$activity->kode_kegiatan."'>".$activity->kegiatan."</option>";
		}

		echo $option;
	}

	/**
	 * Get param for each program
	 * @param string $code
	 * @return void
	 */
	public function program_params($code)
	{
		$params = $this->db->query("SELECT b.* FROM tbl_program_penelitian a 
									JOIN tbl_research_params b ON a.param_code = b.group_param 
									WHERE a.kode_program = '$code'")->result();

		// set param type
		foreach ($params as $param) {
			$parameter = $param->group_param == 'PROGRES' ? 'Progres' : 'Peran';
		}

		$options = "<option value=\"\" disabled=\"\" selected=\"\"></option>";
		foreach ($params as $param) {
			$options .= "<option value='$param->kode'>$param->param</option>";
		}

		echo json_encode(['options' => $options, 'param' => $parameter]);
	}

	/**
	 * Set SKS by program, activity, and param
	 * @param string [$program,$activity,$param]
	 * @return void
	 */
	public function set_sks($program,$activity='',$param='')
	{
		if ($activity != '' && $param != '') {
			$sks = $this->db->query("SELECT sks FROM tbl_sks_penelitian 
									WHERE kode_program = '$program' 
									AND kode_kegiatan = '$activity' 
									AND parameter = '$param'");
			if ($sks->num_rows() > 0) {
				echo $sks->row()->sks;	
			} else {
				echo '';
			}
		}
		echo '';
	}

	/**
	 * Set temporary research data
	 * 
	 * @return void
	 */
	public function temporaryResearch()
	{
        extract(PopulateForm());

        $ses    = $this->session->userdata('research');
        $cp     = count($ses);

        if ($cp == 0) {
            $data = array(
                    $cp => array(
						'nid'           => $this->userid,
						'tahunakademik' => $tahunakademik, 
						'judul'         => $rscTitle,
						'program'       => $rscProgram, 
						'kegiatan'      => $rscActivity, 
						'param'         => $rscParam, 
						'sks'           => $rscCredit
                    )
                );
            $this->session->set_userdata('jml_array',$cp);     

            $arr = $ses + $data;
            $count = $cp+1;
            $this->session->unset_userdata('research');
            $this->session->set_userdata('research',$arr);
            $this->session->set_userdata('jml_array',$count);

         } else {
            $i_arr = $this->session->userdata('jml_array');
            $data = array(
                $i_arr => array(
                    'nid'           => $this->userid,
					'tahunakademik' => $tahunakademik, 
					'judul'         => $rscTitle,
					'program'       => $rscProgram, 
					'kegiatan'      => $rscActivity, 
					'param'         => $rscParam, 
					'sks'           => $rscCredit
                )
            );     

            $arr = $ses + $data;
            $count = $i_arr+1;
            $this->session->unset_userdata('research');
            $this->session->unset_userdata('jml_array');
            
            $this->session->set_userdata('research',$arr);
            $this->session->set_userdata('jml_array',$count);
         
    	}
	}

	/**
	 * Load temporary table
	 * 
	 * @return void
	 */
	public function loadTable()
    {
        $data['team'] = $this->cart->contents();   
        $this->load->view('temp_table_research',$data); 
    }

	/**
	 * Remove research from temporary data
	 * @param int $id
	 * @return void
	 */
	public function deleteList($id)
	{
        $arr = $this->session->userdata('research');

        unset($arr[$id]);
        
        $this->session->unset_userdata('research');
        $this->session->set_userdata('research',$arr);
    }

    /**
     * Add research
     * 
     * @return void
     */
    public function add()
    {
    	extract(PopulateForm());

    	for ($i = 0; $i < count($tahunakademik); $i++) {
    		$dataRsc[] = [
    			'judul' => $judul[$i],
    			'tahunakademik' => $tahunakademik[$i],
    			'program' => $program[$i],
    			'kegiatan' => $kegiatan[$i],
    			'param' => $param[$i],
    			'sks' => $sks[$i],
    			'user' => $this->userid
    		];
    	}
    	$this->db->insert_batch('tbl_lecturer_research', $dataRsc);
    	echo '<script>alert("Penelitian berhasil disimpan!");history.go(-1);</script>';
    }

    /**
     * List all research
     * 
     * @return void
     */
    public function research_list()
    {
		$data['researches'] = $this->bkd->research_list($this->userid);
		$data['page']       = 'detail_penelitian';
    	$this->load->view('template/template', $data);
    }

    /**
     * Remove data of lecturer research
     * @param int $id
     * @return void
     */
    public function remove_research($id)
    {
    	$this->db->delete('tbl_lecturer_research', ['id' => $id]);
    	echo '<script>alert("Data berhasil dihapus!");history.go(-1);</script>';
    }

    /**
     * Show "other" tab view
     * 
     * @return void
     */
    protected function _other_page()
    {
    	$data['others'] = $this->db->get('tbl_jabatan_struktural')->result();
    	$this->load->view('bkd_other_tab_v', $data);
    }

    /**
     * Store "other" data
     * 
     * @return void
     */
    public function store_other_data()
    {
    	extract(PopulateForm());

    	$this->_other_validation();

    	$jabatan = explode('-', $position)[0];

    	$data = [
    		'nid' => $this->userid,
    		'id_jabatan' => $jabatan,
    		'tahunakademik' => getactyear(),
    		'sks' => $sks,
    		'created_at' => date('Y-m-d H:i:s')
    	];
    	$this->db->insert('tbl_struktural_dosen', $data);
    	echo '<script>alert("Data berhasil disimpan!");history.go(-1);</script>';
    }

    /**
     * Validation for other data. In a semester, only 1 position allowed.
     * 
     * @return void
     */
    protected function _other_validation()
    {
    	$isValid = $this->db->get_where("tbl_struktural_dosen",['nid' => $this->userid, 'tahunakademik' => getactyear()]);
    	if ($isValid->num_rows() > 0) {
    		echo '<script>alert("Jumlah jabatan struktural sudah melebihi batas yang diijinkan!");history.go(-1);</script>';
    		exit();
    	}
    	return;
    }

    /**
     * Get structural data
     * 
     * @return void
     */
    public function load_structural_data()
    {
		$year     = getactyear();
		$position = $this->bkd->get_structural_data($this->userid,$year);
    	$data  = [
			'jabatan'       => $position->jabatan,
			'tahunakademik' => $position->tahun_akademik
    	];
    	echo json_encode($data);
    }

    /**
     * Remove data of structural position from lecturer
     * 
     * @return void
     */
    public function remove_structural()
    {
    	$this->db->delete('tbl_struktural_dosen', ['nid' => $this->userid, 'tahunakademik' => getactyear()]);
    	echo '<script>alert("Data berhasil dihapus!");history.go(-1);</script>';
    }

    /**
     * Load devotion page
     * 
     * @return void
     */
    protected function _devotion_page()
    {
    	$this->session->unset_userdata('devotions');
        $this->session->set_userdata('devotions',[]);

    	$data['yearAcademic'] = $this->db->query("SELECT * FROM tbl_tahunakademik")->result();
    	$data['devotionType'] = $this->db->get('tbl_abdimas')->result();
    	$this->load->view('bkd_devotion_v', $data);
    }

    /**
	 * Get devotion program by its devotion
	 * @param string $id
	 * @return void
	 */
	public function get_devotion_program($code)
	{
		$programs = $this->bkd->get_devotion_program($code);

		$option = "<option value=\"\" disabled=\"\" selected=\"\"></option>";
		foreach ($programs as $program) {
			$param = !is_null($program->param) ? $program->param : "NIL";
			$option .= "<option value='".$program->kode_program.'x'.$param.'x'.$program->sks."'>".$program->program."</option>";
		}

		echo $option;
	}

	/**
	 * Set devotion credit
	 * @param string $id
	 * @return void
	 */
	public function set_devotion_credit($code="",$param="")
	{
		$credit = $this->bkd->get_devotion_credit($code,$param);
		echo $credit;
	}

	/**
	 * Get devotion param
	 * @param string $id
	 * @return void
	 */
	public function get_devotion_param($code)
	{
		$params = $this->bkd->get_devotion_param($code);

		$option = "<option value=\"\" disabled=\"\" selected=\"\"></option>";
		foreach ($params as $param) {
			$option .= "<option value='".$param->kode."'>".$param->param."</option>";
		}

		echo $option;
	}

	/**
	 * Set temporary devotion data
	 * 
	 * @return void
	 */
	public function temporaryDevotion()
	{
        extract(PopulateForm());

        $ses    = $this->session->userdata('devotions');
        $cp     = count($ses);

        if ($cp == 0) {
            $data = array(
                    $cp => array(
						'nid'           => $this->userid,
						'type'          => $devotion,
						'program'       => $devProgram, 
						'tahunakademik' => $tahunakademik, 
						'param'         => $devParam, 
						'sks'           => $devCredit
                    )
                );
            $this->session->set_userdata('numberof_array',$cp);     

            $arr = $ses + $data;
            $count = $cp+1;
            $this->session->unset_userdata('devotions');
            $this->session->set_userdata('devotions',$arr);
            $this->session->set_userdata('numberof_array',$count);

         } else {
            $i_arr = $this->session->userdata('numberof_array');
            $data = array(
                $i_arr => array(
                    'nid'           => $this->userid,
					'type'          => $devotion,
					'program'       => $devProgram, 
					'tahunakademik' => $tahunakademik, 
					'param'         => $devParam, 
					'sks'           => $devCredit
                )
            );     

            $arr = $ses + $data;
            $count = $i_arr+1;
            $this->session->unset_userdata('devotions');
            $this->session->unset_userdata('numberof_array');
            
            $this->session->set_userdata('devotions',$arr);
            $this->session->set_userdata('numberof_array',$count);
         
    	}
	}

	/**
	 * Load temporary devotion table
	 * 
	 * @return void
	 */
	public function load_devotion_temp_table()
    {
        $data['team'] = $this->cart->contents();   
        $this->load->view('temp_table_devotion',$data); 
    }

    /**
	 * Remove devotion from temporary data
	 * @param int $id
	 * @return void
	 */
	public function delete_devotion($id)
	{
        $arr = $this->session->userdata('devotions');

        unset($arr[$id]);
        
        $this->session->unset_userdata('devotions');
        $this->session->set_userdata('devotions',$arr);
    }

    /**
     * Store devotion
     * 
     * @return void
     */
    public function add_devotion()
    {
    	extract(PopulateForm());

    	$this->_devotion_validation();

    	$programAmount = count($program);
    	for ($i = 0; $i < $programAmount; $i++) {
    		$devotionList[] = [
				'nid'           => $this->userid,
				'tahunakademik' => $tahunakademik[$i],
				'program'       => $program[$i],
				'param'         => $param[$i],
				'bobot_sks'     => $sks[$i],
				'created_at'    => date('Y-m-d H:i:s')
    		];
    	}
    	$this->db->insert_batch('tbl_abdimas_dosen', $devotionList);
    	echo '<script>alert("Data pengabdian berhasil disimpan!");history.go(-1);</script>';
    }

    /**
     * Validation for devotion. In a semester, only 3 devotions allowed.
     * @param string $nid
     * @return void
     */
    protected function _devotion_validation()
    {
    	$isValid = $this->db->get_where("tbl_abdimas_dosen",['nid' => $this->userid, 'tahunakademik' => getactyear()]);
    	if ($isValid->num_rows() > 3) {
    		echo '<script>alert("Jumlah pengabdian sudah melebihi batas yang diijinkan!");history.go(-1);</script>';
    		exit();
    	}
    	return;
    }

    /**
     * Show devotion list
     * 
     * @return void
     */
    public function devotion_list()
    {
		$data['devotions'] = $this->bkd->get_devotion_list($this->userid);
		$data['page']      = "devotion_list_v";
    	$this->load->view('template/template', $data);
    }

    /**
     * Remove data of lecturer devotion
     * @param int $id
     * @return void
     */
    public function remove_devotion($id)
    {
    	$this->db->delete('tbl_abdimas_dosen', ['id' => $id]);
    	echo '<script>alert("Data berhasil dihapus!");history.go(-1);</script>';
    }

    /**
     * Load BKD form page
     * 
     * @return void
     */
    protected function _form_page()
    {
    	$this->session->unset_userdata('tahunakademik');
    	$data['yearAcademic'] = $this->db->query("SELECT * FROM tbl_tahunakademik")->result();
    	$this->load->view('form_bkd_v', $data);
    }

    /**
     * Set session for form option
     * 
     * @return void
     */
    public function set_form_option()
    {
    	$tahunakademik = $this->input->post('tahunajar');
    	$this->session->set_userdata( 'tahunakademik', $tahunakademik );
    	redirect(base_url('akademik/beban_kinerja_dosen/print_bkd'));
    }
    
    /**
     * Show bkd form in PDF mode
     * 
     * @return void
     */
    public function print_bkd()
    {
    	$this->load->library('Cfpdf');
		$loginSession     = $this->userid;
		$tahunAjar        = $this->session->userdata('tahunakademik');
		$data['semester'] = $this->db->get_where('tbl_tahunakademik', ['kode' => $tahunAjar])->row()->tahun_akademik;
		
		$data['data'] = $this->db->query("SELECT 
											kry.nama,
											kry.nidn,
											bio.jabfung,
											prd.prodi,
											fak.fakultas
										FROM tbl_karyawan kry
										LEFT JOIN tbl_biodata_dosen bio ON kry.nid = bio.nid
										LEFT JOIN tbl_jurusan_prodi prd ON kry.jabatan_id = prd.kd_prodi
										JOIN tbl_fakultas fak ON prd.kd_fakultas = fak.kd_fakultas
										WHERE kry.nid = '$loginSession'")->row();

		$data['courses'] = $this->db->query("SELECT 
												mk.kd_matakuliah, 
												mk.nama_matakuliah,
												mk.sks_matakuliah,
												jdl.waktu_mulai,
												jdl.waktu_selesai,
												jdl.hari
											FROM tbl_jadwal_matkul jdl
											JOIN tbl_matakuliah mk ON jdl.id_matakuliah = mk.id_matakuliah
											WHERE jdl.kd_dosen = '$loginSession'
											AND jdl.kd_tahunajaran = '$tahunAjar'
											AND (mk.nama_matakuliah NOT LIKE 'skripsi%' 
												AND mk.nama_matakuliah NOT LIKE 'kerja praktek%' 
												AND mk.nama_matakuliah NOT LIKE 'tesis%'
												AND mk.nama_matakuliah NOT LIKE 'thesis%' 
												AND mk.nama_matakuliah NOT LIKE 'magang%'
												AND mk.nama_matakuliah NOT LIKE 'kuliah kerja%') ")->result();

		$data['aditional'] = $this->bkd->aditional_teaching($this->userid, $tahunAjar);
		$data['research']  = $this->bkd->research_list_by_year($this->userid, $tahunAjar);
		$data['devotion']  = $this->bkd->get_lecturer_devotion($this->userid, $tahunAjar);
		$data['others']    = $this->bkd->get_struktural_dosen($this->userid, $tahunAjar);
		$this->load->view('bkd_printout', $data);
    }

    /**
     * Modal Revisi
     * @param int $id [id lecture research]
     * @return HTML [modal revisi]
     */
    public function revmod($id)
    {
		$research          = $this->db->get_where('tbl_lecturer_research', ['id' => $id])->row();
		$yearAcademic      = $this->db->get_where('tbl_tahunakademik', ['kode' => $research->tahunakademik])->result();
		$programPenelitian = $this->db->get('tbl_program_penelitian')->result();
		$activity          = $this->db->get_where('tbl_kegiatan_program', ['kode_program' => $research->program])->result();
		$peran             = $this->db->get_where('tbl_research_params', ['kode' => $research->param])->row();
    	
    	$data = [
			'research'          => $research,
			'yearAcademic'      => $yearAcademic,
			'programPenelitian' => $programPenelitian,
			'activity'          => $activity,
			'peran' => $peran,
    	];

    	
    	$this->load->view('bkd_revisi_modal', $data);
    }

    /**
     * Revisi Penelitian
     * @param   $_POST [form submit]
     * @return void
     */
    public function revisi()
    {
    	extract(PopulateForm());

    	$tmp = explode("-", $id);
    	
    	$id = $tmp[0];
    	$user = $tmp[2];

		$dataUpdate = [
			'judul'         => $rscTitle,
			'tahunakademik' => $tahunakademik,
			'program'       => $rscProgram,
			'kegiatan'      => $rscActivity,
			'param'         => $rscParam,
			'sks'           => $rscCredit,
			'user'          => $user,
			'status'		=> 9
		];

		
		$affected = $this->db->update('tbl_lecturer_research', $dataUpdate, ['id' => $id]);
		if ($affected > 0) {
			echo '<script>alert("Revisi penelitian berhasil!");history.go(-1);</script>';
		}else{
			echo '<script>alert("Revisi penelitian gagal!");history.go(-1);</script>';
		}
    }

}

/* End of file Beban_kinerja_dosen.php */
/* Location: ./application/modules/akademik/controllers/Beban_kinerja_dosen.php */