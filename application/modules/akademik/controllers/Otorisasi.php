<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Otorisasi extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		// error_reporting(0);
		if ($this->session->userdata('sess_login') == TRUE) {
			$cekakses = $this->role_model->cekakses(76)->result();
			if ($cekakses != TRUE) {
				echo "<script>alert('Anda Tidak Berhak Mengakses !!');history.go(-1);</script>";
			}
		} else {
			redirect('auth','refresh');
		}
		$this->load->model('cek_model');
	}

	public function index()
	{
		$data['fakultas'] =$this->app_model->getdata('tbl_fakultas', 'id_fakultas', 'ASC')->result();
        $data['tahunajar']=$this->app_model->getdata('tbl_tahunakademik', 'kode', 'ASC')->result();
		$data['page'] = "akademik/otorisasi_view";
		$this->load->view('template/template', $data);
	}

	function get_jurusan($id)
	{
		$jrs = explode('-',$id);
        $jurusan = $this->app_model->getdetail('tbl_jurusan_prodi', 'kd_fakultas', $jrs[0], 'id_prodi', 'ASC')->result();
		$out = "<select class='form-control' name='jurusan' id='jurs'><option>--Pilih Program Studi--</option>";

        foreach ($jurusan as $row) {
            $out .= "<option value='".$row->kd_prodi."'>".$row->prodi. "</option>";
        }
        $out .= "</select>";
        echo $out;
	}

	function simpan_sesi()
	{
		$fakultas = $this->input->post('fakultas');
		$jurusan = $this->input->post('jurusan');
        $tahunajaran = $this->input->post('tahunajaran');
		$angkatan = $this->input->post('angkatan');  

        $this->session->set_userdata('tahunajaran', $tahunajaran);
		$this->session->set_userdata('jurusan', $jurusan);
		$this->session->set_userdata('fakultas', $fakultas);
		$this->session->set_userdata('angkatan', $angkatan);
      
		redirect(base_url('akademik/otorisasi/load_mhs'));
	}

	function load_mhs()
	{
		$object = array('FLAG_RENKEU' => 3);

		$this->db->select('*');
		$this->db->from('tbl_mahasiswa a');
		$this->db->join('tbl_verifikasi_krs b', 'a.NIMHSMSMHS = b.npm_mahasiswa');
		$this->db->where('FLAG_RENKEU', 2);
		$this->db->or_where('FLAG_RENKEU', 4);
		$mhs = $this->db->get()->result();

		if (count($mhs) > 0) {
			foreach ($mhs as $isi) {
				if ($isi->FLAG_RENKEU == 4) {
					$object = array('FLAG_RENKEU' => 5);
				} elseif ($isi->FLAG_RENKEU == 2) {
					$object = array('FLAG_RENKEU' => 3);
				}
				
		    	$this->db->like('NIMHSMSMHS', $isi->NIMHSMSMHS,'both');
		    	$this->db->update('tbl_mahasiswa', $object);	
			}		
		}

		$data['rows'] = $this->db->query("SELECT distinct 
										a.`NIMHSMSMHS`,a.`NMMHSMSMHS`,a.`FLAG_RENKEU`,a.`TAHUNMSMHS`,
										b.`status_verifikasi`,b.`kd_krs` 
										FROM tbl_mahasiswa a 
										JOIN tbl_verifikasi_krs b ON a.`NIMHSMSMHS` = b.`npm_mahasiswa`
										JOIN tbl_sinkronisasi_renkeu c ON c.npm_mahasiswa = b.npm_mahasiswa
										where c.tahunajaran = '{$this->session->userdata('tahunajaran')}'
										AND KDPSTMSMHS = '{$this->session->userdata('jurusan')}' 
										AND TAHUNMSMHS = '{$this->session->userdata('angkatan')}' 
										AND b.tahunajaran = '{$this->session->userdata('tahunajaran')}'
										ORDER BY a.`NIMHSMSMHS` DESC")->result();

		$data['page'] = "akademik/otorisasi_edit";
		$this->load->view('template/template', $data);
	}

	function update_flag()
	{
		$mhs = $this->input->post('mhs', TRUE);
		$jum = count($this->input->post('mhs', TRUE));
		$npm = 0;

		for ($i=0; $i < $jum ; $i++) {
			$npm = $this->input->post('mhs['.$i.']', TRUE);

			//cek data
			$cek = $this->app_model->getdetail('tbl_mahasiswa','NIMHSMSMHS',$npm,'FLAG_RENKEU','asc')->row();
			if ($cek->FLAG_RENKEU == 1) {
				$object = array('FLAG_RENKEU' => 2,'print_kartu' => date('Y-m-d'));
				$data['page'] = "akademik/otorisasi_print";
			} else {
				$object = array('FLAG_RENKEU' => 4,'print_kartu' => date('Y-m-d'));
				$data['page'] = "akademik/otorisasi_print_uas";
			}

			$this->db->like('NIMHSMSMHS',$npm);
			$q=$this->db->update('tbl_mahasiswa', $object);
		}

		$this->db->select('*');
		$this->db->from('tbl_mahasiswa a');
		$this->db->join('tbl_verifikasi_krs b', 'a.NIMHSMSMHS = b.npm_mahasiswa');
		$this->db->where_in('NIMHSMSMHS', $mhs);
		$this->db->where('tahunajaran', $this->session->userdata('tahunajaran'));
		$data['rows'] =  $this->db->get()->result();
		$this->load->view('template/template', $data);		
	}

	function cetak()
	{
		$this->db->select('*');
		$this->db->from('tbl_mahasiswa a');
		$this->db->join('tbl_verifikasi_krs b', 'a.NIMHSMSMHS = b.npm_mahasiswa');
		$this->db->where('FLAG_RENKEU', 2);
		$this->db->where('KDPSTMSMHS',$this->session->userdata('jurusan'));
		$this->db->where('TAHUNMSMHS',$this->session->userdata('angkatan'));
		$this->db->where('tahunajaran', $this->session->userdata('tahunajaran'));
		$data['rows'] =  $this->db->get()->result();
		$this->load->view('welcome/print/kartu_uts_p', $data);
	}

	function cetak_uas()
	{
		$this->db->select('*');
		$this->db->from('tbl_mahasiswa a');
		$this->db->join('tbl_verifikasi_krs b', 'a.NIMHSMSMHS = b.npm_mahasiswa');
		$this->db->where('FLAG_RENKEU', 4);
		$this->db->where('KDPSTMSMHS',$this->session->userdata('jurusan'));
		$this->db->where('TAHUNMSMHS',$this->session->userdata('angkatan'));
		$this->db->where('tahunajaran', $this->session->userdata('tahunajaran'));
		$data['rows'] =  $this->db->get()->result();
		$this->load->view('welcome/print/kartu_uas_p', $data);
	}

	function cetak_satuan($npm)
	{
		$this->db->select('*');
		$this->db->from('tbl_mahasiswa a');
		$this->db->join('tbl_verifikasi_krs b', 'a.NIMHSMSMHS = b.npm_mahasiswa');
		$this->db->where('NIMHSMSMHS', $npm);
		$this->db->where('tahunajaran', $this->session->userdata('tahunajaran'));
		$data['rows'] =  $this->db->get()->result();
		$this->load->view('welcome/print/kartu_uts_p', $data);
	}

	function cetak_satuan_tab()
	{
		$npm 	= $this->input->post('npm');
		$kartu 	= $this->input->post('jenis');
		$tahun 	= $this->input->post('tahun');

		$this->db->select('*');
		$this->db->from('tbl_mahasiswa a');
		$this->db->join('tbl_verifikasi_krs b', 'a.NIMHSMSMHS = b.npm_mahasiswa');
		$this->db->like('kd_krs', $npm.$tahun,'after');
		$this->db->where('NIMHSMSMHS', $npm);
		$this->db->where('tahunajaran', $tahun);
		$data['rows'] =  $this->db->get()->result();
		
		$cek = $this->app_model->getdetail('tbl_mahasiswa','NIMHSMSMHS',$npm,'FLAG_RENKEU','asc')->row();
		if ($cek->FLAG_RENKEU == 1) {
			$this->db->like('NIMHSMSMHS',$npm);
			$this->db->update('tbl_mahasiswa', ['FLAG_RENKEU' => 2,'print_kartu' => date('Y-m-d')]);
		} else {
			$this->db->like('NIMHSMSMHS',$npm);
			$this->db->update('tbl_mahasiswa', ['FLAG_RENKEU' => 4,'print_kartu' => date('Y-m-d')]);
		}

		$this->load->library('Cfpdf');
			
		if ($kartu == 1) {
			$sudahlunas = $this->db->query("SELECT renkeu.* FROM tbl_sinkronisasi_renkeu renkeu 
											JOIN tbl_tahunakademik thak ON renkeu.`tahunajaran` = thak.`kode`
											WHERE tahunajaran = '{$tahun}' 
											AND npm_mahasiswa = '{$npm}' 
											AND renkeu.`status` IN (2,3,4,5)")->num_rows();

			if ($sudahlunas > 0) {
	    		$this->db->select('*');
				$this->db->from('tbl_mahasiswa a');
				$this->db->join('tbl_verifikasi_krs b', 'a.NIMHSMSMHS = b.npm_mahasiswa');
				$this->db->like('kd_krs', $npm.$tahun,'after');
				$this->db->where('NIMHSMSMHS', $npm);
				$this->db->where('tahunajaran', $tahun);
				$data['rows'] =  $this->db->get()->result();
				$data['tahun'] = $tahun;
				$this->load->view('welcome/print/kartu_uts_satuan', $data);
		        
			} else {
				echo "<script>alert('Cek Pembayaran Mahasiswa');
				document.location.href='".base_url()."akademik/otorisasi';</script>";
				exit();
			}
					
		} elseif ($kartu == 2) {

			$sudahlunas = $this->db->query("SELECT renkeu.* FROM tbl_sinkronisasi_renkeu renkeu 
											JOIN tbl_tahunakademik thak ON renkeu.`tahunajaran` = thak.`kode`
											WHERE tahunajaran = '.$tahun.' AND npm_mahasiswa = '.$npm.'  
											AND renkeu.`status` IN (4,5) ")->num_rows();
	 				
			if ($sudahlunas > 0) {
	    		$this->db->select('*');
				$this->db->from('tbl_mahasiswa a');
				$this->db->join('tbl_verifikasi_krs b', 'a.NIMHSMSMHS = b.npm_mahasiswa');
				$this->db->like('kd_krs', $npm.$tahun,'after');
				$this->db->where('NIMHSMSMHS', $npm);
				$this->db->where('tahunajaran', $tahun);
				$data['rows'] =  $this->db->get()->result();
				$data['tahun'] = $tahun;
				$this->load->view('welcome/print/kartu_ujian_satuan', $data);
		        
			}else {
				echo "<script>alert('Cek Pembayaran Mahasiswa');
				document.location.href='".base_url()."akademik/otorisasi';</script>";
				exit();
			}
		}
	}

	function cetak_satuanuas($kdkrs,$stsverif)
	{
        if (($stsverif == 10) || ($z != $q)) {
            echo "<script>alert('Mahasiswa belum melunasi pembayaran!');history.go(-1);</script>";
            exit();

        } else {
        	$this->load->library('Cfpdf');
        	$npm = substr($kdkrs, 0, 12);
			$this->db->select('*');
			$this->db->from('tbl_mahasiswa a');
			$this->db->join('tbl_verifikasi_krs b', 'a.NIMHSMSMHS = b.npm_mahasiswa');
			$this->db->where('a.NIMHSMSMHS', $npm);
			$this->db->where('b.kd_krs', $kdkrs);
			$this->db->where('b.tahunajaran', $this->session->userdata('tahunajaran'));
			$data['rows'] =  $this->db->get()->result();
			$this->load->view('welcome/print/kartu_uas_p', $data);
		}
	}
}

/* End of file status_ujian.php */
/* Location: ./application/controllers/status_ujian.php */