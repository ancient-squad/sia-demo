<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Ajar extends MY_Controller
{
	private $userid, $usergroup;

	public function __construct()
	{
		parent::__construct();
		error_reporting(0);
		if ($this->session->userdata('sess_login')) {
			$cekakses = $this->role_model->cekakses(71)->result();
			if (!$cekakses) {
				echo "<script>alert('Anda Tidak Berhak Mengakses !');history.go(-1);</script>";
			}
		} else {
			redirect('auth', 'refresh');
		}
		$this->load->library('Cfpdf');
		$this->load->model('akademik/ajar_model', 'ajarmod');
		$this->userid = $this->session->userdata('sess_login')['userid'];
		$this->usergroup = get_group($this->session->userdata('sess_login')['id_user_group']);
	}

	public function index()
	{
		$prodi = $this->userid;
		$group = $this->usergroup;
		$data['tahunajar'] = $this->app_model->getdata('tbl_tahunakademik', 'kode', 'ASC')->result();
		$data['prodi'] = $this->db->get('tbl_jurusan_prodi')->result();

		if (in_array(10, $group) || in_array(1, $group)) {
			$data['page']     = 'ajar_select_baa';
		} elseif (in_array(9, $group)) {
			$data['page']  = 'ajar_select_fak';
		} elseif (in_array(8, $group) || in_array(19, $group)) {
			$data['page'] = 'ajar_select_prodi';
		} else {
			$data['page'] = 'ajar_select_ta';
		}
		$this->load->view('template/template', $data);
	}

	public function save_session_dosen()
	{
		$tahunajaran = $this->input->post('tahunajaran');
		$this->session->set_userdata('tahunajaran', $tahunajaran);
		redirect(base_url('akademik/ajar/courses_list'));	
	}

	public function courses_list()
	{
		$data['tahunakademik'] = $this->session->userdata('tahunajaran');
		$data['group']    = $this->usergroup;
		$data['back_url'] = base_url('akademik/ajar');
		$data['nid']      = $this->session->userdata('sess_login')['userid'];
		$data['rows']     = $this->ajarmod->course_list($data['nid'], $data['tahunakademik']);
		$data['nm_dosen'] = $this->db->where('nid', $data['nid'])->get('tbl_karyawan')->row();
		$data['log']      = $this->session->all_userdata()['sess_login'];
		$data['page']     = 'ajar_view';
		$this->load->view('template/template', $data);
	}

	public function save_session()
	{
		$userid      = $this->userid;
		$group       = $this->usergroup;
		$jurusan     = explode('-', $this->input->post('jurusan'));
		$tahunajaran = $this->input->post('tahunajaran');

		if (in_array(10, $group) || in_array(1, $group) || in_array(18, $group)) {
			$this->session->set_userdata('tahunajaran', $tahunajaran);
			$this->session->set_userdata('id_jurusan_prasyarat', $jurusan[0]);

		} elseif (in_array(9, $group)) {
			$this->session->set_userdata('tahunajaran', $tahunajaran);
			$this->session->set_userdata('id_fakultas_prasyarat', $userid);
			$this->session->set_userdata('id_jurusan_prasyarat', $jurusan[0]);
			$this->session->set_userdata('nama_jurusan_prasyarat', $jurusan[1]);

		} elseif (in_array(8, $group) || in_array(19, $group)) {
			$this->session->set_userdata('tahunajaran', $tahunajaran);
			$this->session->set_userdata('id_jurusan_prasyarat', $userid);
		}

		redirect(base_url('akademik/ajar/load_dosen_mengajar'));
	}

	public function save_session_baa()
	{
		$jurusan     = $this->input->post('jurusan');
		$tahunajaran = $this->input->post('tahunajaran');
		$this->session->set_userdata('tahunajaran', $tahunajaran);
		$this->session->set_userdata('id_jurusan_prasyarat', $jurusan);
		redirect(base_url('akademik/ajar/load_list_dosen'));
	}

	public function load_dosen_mengajar()
	{
		$data['tahunakademik'] = $this->session->userdata('tahunajaran');
		$prodi         = $this->session->userdata('id_jurusan_prasyarat');
		$data['dosen'] = $this->ajarmod->get_dosen_ajar($data['tahunakademik'],$prodi);
		$data['prodi'] = $this->session->userdata('id_jurusan_prasyarat');
		$data['page']  = 'akademik/ajar_listdosen_baa';
		$this->load->view('template/template', $data);
	}

	public function save_session_prodi()
	{
		$tahunajaran = $this->input->post('tahunajaran');
		$semester    = $this->input->post('semester', TRUE);
		$user        = $this->session->userdata('sess_login');
		$jurusan     = $user['userid'];

		$this->session->set_userdata('tahunajaran', $tahunajaran);
		$this->session->set_userdata('semester', $semester);
		$this->session->set_userdata('id_jurusan_prasyarat', $jurusan);

		redirect(base_url('akademik/ajar/load_list_dosen'));
	}

	public function load_list_dosen()
	{
		$usergroup = $this->session->userdata('sess_login')['id_user_group'];
		$data['back_url'] = base_url('akademik/ajar');
		$data['group'] = $this->usergroup;
		$data['ta']    = $this->session->userdata('tahunajaran');
		$data['prodi'] = $this->session->userdata('id_jurusan_prasyarat');
		$smt = $this->session->userdata('semester');

		// PRODI = 8  & STAFF = 19
		if (in_array(8, $data['group']) || in_array(19, $data['group'])) {
			$data['dosen'] = $this->ajarmod->list_dosen($this->userid, $smt, $data['ta']);
		
		// FAKULTAS = 9
		} elseif (in_array(9, $data['group'])) {
			$data['dosen'] = $this->ajarmod->list_dosen_fak($data['prodi']);
		
		// BAA = 10
		} elseif (in_array(10, $data['group'])) {
			$data['dosen'] = $this->ajarmod->list_dosen_baa($data['prodi'],$data['ta']);
		}

		$data['page'] = 'ajar_listdosen';
		$this->load->view('template/template', $data);
	}

	public function loadMeetAmount($id)
	{
		$kdjdl = get_kd_jdl($id);
		$ta = $this->session->userdata('tahunajaran');
		$data['maksper'] = $this->ajarmod->meetingAmount($ta, $kdjdl);
		$this->load->view('v_meetamount', $data);
	}

	public function show_dosen_by_prodi($nid)
	{
		$prodi = $this->session->userdata('id_jurusan_prasyarat');
		$data['back_url'] = base_url('akademik/ajar/load_dosen_mengajar');
		$tahunakademik    = $this->session->userdata('tahunajaran');
		$data['tahunakademik'] = $tahunakademik;
		$data['nid']      = $nid;
		$data['nm_dosen'] = $this->db->where('nid', $nid)->get('tbl_karyawan')->row();
		$data['group']    = $this->usergroup;
		$data['rows']     = $this->ajarmod->get_schedule_list($nid, $prodi, $tahunakademik);	
		$data['page']     = 'akademik/ajar_view';
		$this->load->view('template/template', $data);
	}

	public function cetak_absensi($id)
	{
		$user = $this->session->userdata('sess_login');

		$activeYear = getactyear();

		if ($user['userid'] == 'BAA') {
			$optyear = $this->session->userdata('ta');
		} else {
			$optyear = $this->session->userdata('tahunajaran');
		}

		$nik = $user['userid'];

		// jadwal pada tahun <  20192 masih menggunakan metode jadwal per prodi
		if ($optyear < 20192) {
			$rows 	= $this->db->query('SELECT 
											a.*,
											b.kd_matakuliah,
											b.sks_matakuliah,
											b.nama_matakuliah,
											b.semester_matakuliah,
											d.kode_ruangan,d.kuota 
										FROM tbl_jadwal_matkul a
										JOIN tbl_matakuliah b ON a.`kd_matakuliah` = b.`kd_matakuliah`
										LEFT JOIN tbl_ruangan d ON d.`id_ruangan` = a.`kd_ruangan`
										WHERE a.`id_jadwal` = ' . $id . ' AND SUBSTR(a.kd_jadwal,1,5) = b.kd_prodi')->row();
		} else {
			$rows 	= $this->db->query('SELECT 
											a.*,
											b.kd_matakuliah,
											b.sks_matakuliah,
											b.nama_matakuliah,
											b.semester_matakuliah,
											d.kode_ruangan,d.kuota 
										FROM tbl_jadwal_matkul a
										JOIN tbl_matakuliah b ON a.id_matakuliah = b.id_matakuliah
										LEFT JOIN tbl_ruangan d ON d.id_ruangan = a.kd_ruangan
										WHERE a.id_jadwal = '.$id.' 
										GROUP BY a.kd_matakuliah')->row();
		}
		
		$data['rows'] =  $rows;

		if (is_null($data['rows']->gabung)) {
			$data['kodeprodi'] = substr($data['rows']->kd_jadwal, 0, 5);
		} else {
			$data['kodeprodi'] = substr($data['rows']->kd_jadwal, 0, 1);
		}
		
		$data['years'] = $optyear;

		if ($rows->gabung > 0) {
			$data['mhs'] 	= $this->db->query('SELECT distinct b.npm_mahasiswa, kd_jadwal 
												FROM tbl_krs b
												JOIN tbl_verifikasi_krs v ON b.kd_krs = v.kd_krs
												JOIN tbl_sinkronisasi_renkeu r ON r.npm_mahasiswa = b.npm_mahasiswa
												WHERE v.status_verifikasi = 1 AND r.tahunajaran = "' . $optyear . '"
												AND b.kd_jadwal = "' . $data['rows']->kd_jadwal . ' "
												OR b.kd_jadwal = "' . $data['rows']->referensi . '"
												OR b.kd_jadwal IN 
													(SELECT DISTINCT kd_jadwal FROM tbl_jadwal_matkul 
													WHERE referensi = "' . $data['rows']->referensi . '")
												ORDER BY b.npm_mahasiswa ASC')->result();

			// ambil seluruh kode jadwal yang bersangkutan
			$subQuery = "SELECT DISTINCT kd_jadwal FROM tbl_jadwal_matkul WHERE referensi = '".$data['rows']->referensi."'";

			// menghitung total banyak mahasiswa 
			$queryJumlah = "SELECT count(distinct a.npm_mahasiswa) AS mhs FROM tbl_krs a
							JOIN tbl_verifikasi_krs v ON v.kd_krs = a.kd_krs
							JOIN tbl_sinkronisasi_renkeu b ON a.npm_mahasiswa = b.npm_mahasiswa
							WHERE v.status_verifikasi = 1
							AND a.kd_jadwal IN ($subQuery)
							AND b.tahunajaran = '".$optyear."'" ;

			$data['jumlah'] = $this->db->query($queryJumlah)->row()->mhs;
			
		} else {
			$data['mhs'] 	= $this->db->query('SELECT distinct b.npm_mahasiswa FROM tbl_krs b
												JOIN tbl_verifikasi_krs v on v.kd_krs = b.kd_krs
												JOIN tbl_sinkronisasi_renkeu r ON r.npm_mahasiswa = b.npm_mahasiswa
												WHERE v.status_verifikasi = 1
												AND r.tahunajaran = "' . $optyear . '"
												AND b.kd_jadwal = "' . $data['rows']->kd_jadwal . ' "
												ORDER BY b.npm_mahasiswa ASC')->result();

			$data['jumlah'] = $this->db->query('SELECT count(distinct a.npm_mahasiswa) AS mhs 
												FROM tbl_krs a
												JOIN tbl_verifikasi_krs v on v.kd_krs = a.kd_krs
												JOIN tbl_sinkronisasi_renkeu b ON a.npm_mahasiswa = b.npm_mahasiswa
												WHERE v.status_verifikasi = 1
												AND a.kd_jadwal = "' . $data['rows']->kd_jadwal . '"
												AND b.tahunajaran = "'.$optyear.'" ')->row()->mhs; }

		
		// OLD
		// $data['maxper'] = $this->ajarmod->meetingAmountForPrint($optyear,$data['rows']->kd_jadwal);


		// NEW
		$maxper = $this->ajarmod->meetingAmountForPrint($optyear, $data['rows']->kd_jadwal);

		for ($i = 1; $i <= $maxper; $i++) {
			$arrayPertemuan[] = $i;
		}

		$data['arrayPertemuan'] = $arrayPertemuan;
		$data['maxper'] = $maxper;
		
		$this->load->view('cetak_absensi_pdf__', $data);
	}

	public function cetak_absensi2($id)
	{
		error_reporting(E_ALL);
		ini_set('display_errors', 1);
		//ini_set('max_execution_time', 1000);
		$user = $this->session->userdata('sess_login');

		$activeYear = getactyear();

		if ($user['userid'] == 'BAA') {
			$optyear = $this->session->userdata('ta');
		} else {
			$optyear = $this->session->userdata('tahunajaran');
		}

		$nik = $user['userid'];

		$data['rows'] 	= $this->db->query('SELECT a.*,b.kd_matakuliah,b.sks_matakuliah,b.nama_matakuliah,
											b.semester_matakuliah,d.kode_ruangan,d.kuota FROM tbl_jadwal_matkul a
											JOIN tbl_matakuliah b ON a.`kd_matakuliah` = b.`kd_matakuliah`
											LEFT JOIN tbl_ruangan d ON d.`id_ruangan` = a.`kd_ruangan`
											WHERE a.`id_jadwal` = ' . $id . ' AND SUBSTR(a.kd_jadwal,1,5) = b.kd_prodi')->row();


		$data['kodeprodi'] = substr($data['rows']->kd_jadwal, 0, 5);

		$data['jumlah'] = $this->db->query('SELECT count(distinct npm_mahasiswa) as mhs from tbl_krs 
											where kd_jadwal = "' . $data['rows']->kd_jadwal . '" ')->row()->mhs;

		$data['mhs'] 	= $this->db->query('SELECT distinct b.npm_mahasiswa FROM tbl_krs b
											JOIN tbl_sinkronisasi_renkeu r ON r.npm_mahasiswa = b.npm_mahasiswa
											WHERE r.tahunajaran = "' . $optyear . '"
											AND b.kd_jadwal = "' . $data['rows']->kd_jadwal . '" 
											
											ORDER BY b.npm_mahasiswa ASC')->result();

		$maxper = $this->ajarmod->meetingAmountForPrint($optyear, $data['rows']->kd_jadwal);

		for ($i = 1; $i <= $maxper; $i++) {
			$arrayPertemuan[] = $i;
		}

		$data['arrayPertemuan'] = $arrayPertemuan;
		$data['maxper'] = $maxper;


		$this->load->view('cetak_absensi_pdf__2', $data);
	}

	public function cetak_absensi_polos($id)
	{
		$optyear 	= $this->session->userdata('tahunajaran');
		$user 		= $this->session->userdata('sess_login');

		$nik = $user['userid'];

		$data['rows'] = $this->db->query('SELECT * FROM tbl_jadwal_matkul a
											JOIN tbl_matakuliah b ON a.`kd_matakuliah` = b.`kd_matakuliah` 
											JOIN tbl_karyawan c ON c.`nid` = a.`kd_dosen`
											LEFT JOIN tbl_ruangan d ON d.`id_ruangan` = a.`kd_ruangan`
											WHERE a.`id_jadwal` = ' . $id . '
											AND b.kd_prodi = "' . $nik . '"')->row();

		$kodeprodi = substr($data['rows']->kd_jadwal, 0, 5);

		$data['titles'] = $this->db->query('SELECT * FROM tbl_jurusan_prodi b
											JOIN tbl_fakultas c ON b.`kd_fakultas` = c.`kd_fakultas`
											WHERE b.`kd_prodi` = "' . $kodeprodi . '"')->row();

		if ($data['rows']->gabung > 0) {
			$data['mhs'] = $this->db->query('SELECT distinct 
												mhs.`NIMHSMSMHS`,
												mhs.`NMMHSMSMHS` 
											FROM tbl_krs b
											JOIN tbl_verifikasi_krs v ON b.kd_krs = v.kd_krs
											JOIN tbl_mahasiswa mhs ON b.`npm_mahasiswa` = mhs.`NIMHSMSMHS`
											JOIN tbl_sinkronisasi_renkeu r ON r.npm_mahasiswa = b.npm_mahasiswa
											WHERE r.tahunajaran = "' . $optyear . '"
											AND v.status_verifikasi = 1
											AND b.kd_jadwal = "' . $data['rows']->kd_jadwal . '" 
											OR kd_jadwal = "' . $data['rows']->referensi . '"
											OR kd_jadwal IN 
												(SELECT DISTINCT kd_jadwal FROM tbl_jadwal_matkul WHERE referensi = "' . $data['rows']->referensi . '")
											ORDER BY mhs.NIMHSMSMHS ASC')->result();
		} else {
			$data['mhs'] = $this->db->query('SELECT distinct 
												mhs.`NIMHSMSMHS`,
												mhs.`NMMHSMSMHS` 
											FROM tbl_krs b
											JOIN tbl_verifikasi_krs v ON b.kd_krs = v.kd_krs
											JOIN tbl_mahasiswa mhs ON b.`npm_mahasiswa` = mhs.`NIMHSMSMHS`
											JOIN tbl_sinkronisasi_renkeu r ON r.npm_mahasiswa = b.npm_mahasiswa
											WHERE r.tahunajaran = "' . $optyear . '"
											AND v.status_verifikasi = 1
											AND b.kd_jadwal = "' . $data['rows']->kd_jadwal . '" 
											OR kd_jadwal IN 
												(SELECT DISTINCT kd_jadwal FROM tbl_jadwal_matkul WHERE referensi = "' . $data['rows']->kd_jadwal . '")
											ORDER BY mhs.NIMHSMSMHS ASC')->result();
		}
		$this->load->view('cetak_absensi_pdf', $data);
	}

	public function cetak_daftar_mahasiswa($id)
	{
		$data['www'] = $this->db->query('SELECT * from tbl_jadwal_matkul a
											join tbl_matakuliah b on a.`kd_matakuliah`=b.`kd_matakuliah`
											join tbl_krs c on c.`kd_matakuliah`=b.`kd_matakuliah`
											join tbl_mahasiswa d on d.`NIMHSMSMHS`=c.`npm_mahasiswa`
											where id_jadwal=' . $id . '')->result();

		$data['lah'] = $this->db->query('SELECT * from tbl_jadwal_matkul a
											join tbl_matakuliah b on a.`kd_matakuliah`=b.`kd_matakuliah`
											join tbl_jurusan_prodi c on b.`kd_prodi`=c.`kd_prodi`
											join tbl_fakultas d on c.`kd_fakultas`=d.`kd_fakultas`
											left join tbl_ruangan e on a.`kd_ruangan`=e.`kode_ruangan`
											left join tbl_gedung f on e.`id_gedung`=f.`id_gedung`
											where id_jadwal=' . $id . '')->row();

		$this->load->view('cetak_mahasiswa_pdf', $data);
	}

	#edit danu
	public function print_surat_tugas($nid)
	{
		$prodi            = $this->session->userdata('id_jurusan_prasyarat');
		$tahunakademik    = $this->session->userdata('tahunajaran');
		$data['rows']     = $this->ajarmod->get_schedule_list($nid, $prodi, $tahunakademik);
		$data['karyawan'] = $this->app_model->getdetail('tbl_karyawan', 'nid', $nid, 'nid', 'asc')->row();
		$data['fak']      = $this->app_model->getdetail('tbl_jurusan_prodi','kd_prodi',$prodi,'kd_prodi','asc')->row();
		$data['fakultas'] = $this->app_model->getdetail('tbl_fakultas', 'kd_fakultas', $data['fak']->kd_fakultas, 'kd_fakultas', 'asc')->row();
		$this->load->view('cetak_surat_tugas', $data);
	}

	public function view_absen($id)
	{
		// $this->output->enable_profiler(TRUE);
		$data['kelas'] = $this->ajarmod->loadAbsen($id);
		// mengambil seluruh pertemuan di database
		$pertemuan = $this->ajarmod->loadPertemuan($id);
		// array total pertemuan
		$arrPertemuan = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16];
		// membandingkan array pertemuan dengan pertemuan di database
		// mengambil pertemuan yang tidak tersedia di database
		$pertemuan_tersedia = array_diff($arrPertemuan, $pertemuan);

		$data['pertemuan_tersedia'] = $pertemuan_tersedia;
		$data['id'] = $id;

		$this->load->view('ajar_absen_harian', $data);
	}

	/**
	 * check_pertemuan : melakukan pengecekan total pertemuan sebelum input absen
	 * @param id <id jadwal>
	 * @return Object <success : boolean, message : string>
	 */
	public function check_pertemuan($id)
	{
		$success = false;
		$message = "";

		if (empty($id)) {
			$message = 'Id Pertemuan kosong';
		} else {
			// mengambil seluruh pertemuan di database
			$pertemuan = $this->ajarmod->loadPertemuan($id);
			// array total pertemuan
			$arrPertemuan = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16];
			// membandingkan array pertemuan dengan pertemuan di database
			// mengambil pertemuan yang tidak tersedia di database
			$pertemuan_tersedia = array_diff($arrPertemuan, $pertemuan);

			$countPertemuanTersedia = count($pertemuan_tersedia);

			if ($countPertemuanTersedia == 0) {
				$message = "Sudah mencapai 16 Pertemuan";
			} elseif ($countPertemuanTersedia > 0) {
				$success = true;
			}
		}

		echo json_encode([
			'success' => $success,
			'message' => $message,
		]);
	}

	public function view_absen_dosen($id)
	{
		$user = $this->session->userdata('sess_login');
		$data['kelas'] = $this->app_model->get_data_jadwal($id, $user['userid'])->row();
		$data['max'] = $this->db->query("select count(npm_mahasiswa) as mhs from tbl_krs where kd_jadwal = '" . $data['kelas']->kd_jadwal . "'")->row()->mhs;
		$data['id'] = $id;
		$this->load->view('ajar_absen_dosen', $data);
	}

	public function kelas_absen($id)
	{
		//$datenow = date('yy-mm-dd H:i:s');
		$jumlahabsen = $this->input->post('jumlah', TRUE);
		$kode = $this->app_model->getdetail('tbl_jadwal_matkul', 'id_jadwal', $id, 'id_jadwal', 'asc')->row();
		$cek = $this->db->query("select count(distinct tanggal) as jml from tbl_absensi_mhs_new_20171 where kd_jadwal = '" . $kode->kd_jadwal . "'")->row()->jml;
		$tanggal = $this->db->query("select distinct tanggal from tbl_absensi_mhs_new_20171 where kd_jadwal = '" . $kode->kd_jadwal . "' order by id_absen desc limit 1")->row();

		$masuk = 0;
		$ijin = 0;
		$alfa = 0;
		$sakit = 0;

		if ($cek == 0) {
			$a = 1;
			for ($i = 0; $i < $jumlahabsen - 1; $i++) {
				$hasil = $this->input->post('absen' . $a . '[0]', TRUE);
				//echo 'absen'.$a.'['.$i.']';
				$pecah = explode('-', $hasil);
				//var_dump($hasil);
				$datax[] = array(
					'npm_mahasiswa' => $pecah[1],
					'kd_jadwal' => $pecah[2],
					'tanggal' => $this->input->post('tgl', TRUE),
					'pertemuan' => $tanggal->pertemuan + 1,
					'kehadiran' => $pecah[0]
				);
				if ($pecah[0] == 'H') {
					$masuk++;
				} elseif ($pecah[0] == 'I') {
					$ijin++;
				} elseif ($pecah[0] == 'S') {
					$sakit++;
				} else {
					$alfa++;
				}
				$a++;
			}
			$this->db->insert_batch('tbl_absensi_mhs_new_20171', $datax);
			$datas = array(
				'kd_jadwal' => $kode->kd_jadwal,
				'tanggal' => $this->input->post('tgl', TRUE),
				'pertemuan' => 1,
				'jumlah_hadir' => $masuk,
				'jumlah_sakit' => $sakit,
				'jumlah_izin' => $ijin,
				'jumlah_alfa' => $alfa,
				//'pembahasan' => $this->input->post('pembahasan',TRUE),
				'date_input' => date('yy-mm-dd H:i:s')
			);
			$this->app_model->insertdata('tbl_absensi_dosen', $datas);
			echo "<script>alert('Sukses');
			document.location.href='" . base_url() . "akademik/ajar/load_list_dosen';</script>";
		} else if ($cek > 0 || $cek < 17) {
			if ($tanggal->tanggal == $this->input->post('tgl', TRUE)) {
				//$pertemuan =  1;
				echo "<script>alert('Absensi Sudah Dilakukan Pada Tanggal Tersebut');
				history.go(-1);</script>";
			} else {
				$a = 1;
				for ($i = 0; $i < $jumlahabsen - 1; $i++) {
					$hasil = $this->input->post('absen' . $a . '[0]', TRUE);
					//echo 'absen'.$a.'['.$i.']';
					$pecah = explode('-', $hasil);
					//var_dump($hasil);
					$datax[] = array(
						'npm_mahasiswa' => $pecah[1],
						'kd_jadwal' => $pecah[2],
						'tanggal' => $this->input->post('tgl', TRUE),
						'pertemuan' => $this->input->post('pertemuanke', TRUE),
						// 'pertemuan' => $tanggal->pertemuan + 1,
						'kehadiran' => $pecah[0]
					);

					if ($pecah[0] == 'H') {
						$masuk++;
					} elseif ($pecah[0] == 'I') {
						$ijin++;
					} elseif ($pecah[0] == 'S') {
						$sakit++;
					} else {
						$alfa++;
					}
					$a++;
				}

				$this->db->insert_batch('tbl_absensi_mhs_new_20171', $datax);
				$datas = array(
					'kd_jadwal' => $kode->kd_jadwal,
					'tanggal' => $this->input->post('tgl', TRUE),
					'pertemuan' => 1,
					'jumlah_hadir' => $masuk,
					'jumlah_sakit' => $sakit,
					'jumlah_izin' => $ijin,
					'jumlah_alfa' => $alfa,
					//'pembahasan' => $this->input->post('pembahasan',TRUE),
					'date_input' => date('yy-mm-dd H:i:s')
				);
				$this->app_model->insertdata('tbl_absensi_dosen', $datas);
				echo "<script>alert('Sukses');
				document.location.href='" . base_url() . "akademik/ajar/load_list_dosen';</script>";
			}
		} else {
			echo "<script>alert('Absen Melebihi Jumlah 16 Pertemuan');
						document.location.href='" . base_url() . "akademik/ajar/load_list_dosen';</script>";
			exit();
		}
	}


	public function dosen_absen($id)
	{
		$kode = $this->app_model->getdetail('tbl_jadwal_matkul', 'id_jadwal', $id, 'id_jadwal', 'asc')->row();
		$cek = $this->db->query("select count(distinct tanggal) as jml from tbl_absensi_dosen where kd_jadwal = '" . $kode->kd_jadwal . "'")->row()->jml;
		$tanggal = $this->db->query("select distinct tanggal,pertemuan from tbl_absensi_dosen where kd_jadwal = '" . $kode->kd_jadwal . "' order by id desc limit 1")->row();
		//var_dump($cek);exit();
		if ($cek > 0) {
			if ($tanggal->tanggal == $this->input->post('tgl', TRUE)) {
				//$pertemuan =  1;
				echo "<script>alert('Absensi Sudah Dilakukan Pada Tanggal Tersebut');
				history.go(-1);</script>";
			} else {
				$data['materi'] = $this->input->post('materi', TRUE);
				$data['jumlah_mhs'] = $this->input->post('jumlah', TRUE);
				$data['kd_jadwal'] = $kode->kd_jadwal;
				$data['tanggal'] = $this->input->post('tgl', TRUE);
				$data['pertemuan'] = 1;
				//var_dump($data);exit();
				$q = $this->app_model->insertdata('tbl_absensi_dosen', $data);
				echo "<script>alert('Sukses');
				document.location.href='" . base_url() . "akademik/ajar/load_list_dosen';</script>";
			}
		} else {
			$data['materi'] = $this->input->post('materi', TRUE);
			$data['jumlah_mhs'] = $this->input->post('jumlah', TRUE);
			$data['kd_jadwal'] = $kode->kd_jadwal;
			$data['tanggal'] = $this->input->post('tgl', TRUE);
			$data['pertemuan'] = 1;
			//var_dump($data);exit();
			$q = $this->app_model->insertdata('tbl_absensi_dosen', $data);
			echo "<script>alert('Sukses');
			document.location.href='" . base_url() . "akademik/ajar/load_list_dosen';</script>";
		}
	}

	public function edit_absen($id)
	{
		$this->session->unset_userdata('kd_jadwal');
		$q = $this->db->query('select kd_jadwal, gabung, referensi from tbl_jadwal_matkul where id_jadwal = ' . $id . '')->row();
		
		$this->session->set_userdata('kd_edit', $q->kd_jadwal);

		$data['id_jadwal'] = $id;

		$data['pertemuan'] = $this->db->query('SELECT DISTINCT pertemuan,tanggal FROM tbl_absensi_mhs_new_20171 WHERE kd_jadwal = "' . $q->kd_jadwal . '"
													GROUP BY pertemuan,tanggal')->result();

		$data['page'] = 'edit_absen';
		$this->load->view('template/template', $data);
	}

	public function open_absen($id, $akses)
	{
		$this->db->query("update tbl_jadwal_matkul set open = " . $akses . " where id_jadwal = " . $id . "");
		$lacak = $this->db->query("SELECT kd_jadwal from tbl_jadwal_matkul where id_jadwal = '" . $id . "'")->row();
		$temu = $this->db->query("SELECT max(pertemuan) as meet from tbl_absensi_mhs_new_20171 where kd_jadwal = '" . $lacak->kd_jadwal . "' order by pertemuan limit 1")->row();
		$jam = date('H:i:s');
		if ($akses == 0) {
			$this->db->query("UPDATE tbl_materi_abs set jam_keluar = '" . $jam . "' where kd_jadwal = '" . $lacak->kd_jadwal . "' and pertemuan = '" . $temu->meet . "'");
		}

		echo "<script>alert('Berhasil');
			document.location.href='" . base_url() . "akademik/ajar/load_list_dosen';</script>";
	}

	public function modal_edit_absen($id, $pertemuan)
	{
		clearstatcache();
		$actyear = getactyear();

		$q = $this->db->query('select * from tbl_jadwal_matkul where id_jadwal = ' . $id . '')->row();

		$data['pertemuan'] = $pertemuan;
		$data['kode_jadwal'] = $q->kd_jadwal;
		$data['tanggal'] = $this->db->query('SELECT DISTINCT tanggal FROM tbl_absensi_mhs_new_20171 
											WHERE pertemuan = ' . $pertemuan . ' 
											AND kd_jadwal = "' . $q->kd_jadwal . '"')->row()->tanggal;

		if ($q->gabung > 0) {
			$data['kelas'] 	= $this->db->query("SELECT
												    distinct b.`npm_mahasiswa`,
												    mhs.`NMMHSMSMHS`,
												    b.`kd_jadwal`,
												    (
												        SELECT
												            kehadiran
												        FROM
												            tbl_absensi_mhs_new_20171
												        WHERE
												           kd_jadwal IN ( select kd_jadwal from tbl_jadwal_matkul where referensi = '$q->referensi' )
												            AND pertemuan = '$pertemuan'
												            AND npm_mahasiswa = b.`npm_mahasiswa`
												    ) as kehadiran
												FROM
												    tbl_krs b
												    JOIN tbl_verifikasi_krs v ON b.kd_krs = v.kd_krs
												    JOIN tbl_mahasiswa mhs ON b.`npm_mahasiswa` = mhs.`NIMHSMSMHS`
												WHERE
												v.status_verifikasi = 1
												AND
												    b.kd_jadwal IN (
												        SELECT
												            kd_jadwal
												        FROM
												            tbl_absensi_mhs_new_20171
												        where
												             kd_jadwal IN ( select kd_jadwal from tbl_jadwal_matkul where referensi = '$q->referensi' )
												    )
												ORDER BY
												    mhs.NIMHSMSMHS ASC ")->result();
		}else{
			$data['kelas'] 	= $this->db->query('SELECT distinct 
												b.`npm_mahasiswa`,
												mhs.`NMMHSMSMHS`,
												b.`kd_jadwal`, 
												(SELECT kehadiran FROM tbl_absensi_mhs_new_20171 
												WHERE kd_jadwal = "' . $q->kd_jadwal . '" 
												AND pertemuan = ' . $pertemuan . ' 
												AND npm_mahasiswa = b.`npm_mahasiswa`) as kehadiran
											FROM tbl_krs b
											JOIN tbl_verifikasi_krs v ON b.kd_krs = v.kd_krs
											JOIN tbl_mahasiswa mhs ON b.`npm_mahasiswa` = mhs.`NIMHSMSMHS`
											WHERE v.status_verifikasi = 1 AND b.kd_jadwal = "' . $q->kd_jadwal . '"
											ORDER BY mhs.NIMHSMSMHS ASC')->result();
		}
		

		$data['id'] = $id;
		
		$this->load->view('ajar_absen_harian_edit', $data);
	}

	public function edit_kelas_absen($id)
	{

	
		$jumlahabsen = $this->input->post('jumlah', TRUE);
		$pertemuan   = $this->input->post('pertemuan', TRUE);
		$id_jadwal2  = $this->input->post('id_jadwal2', TRUE);
		$tanggal2    = $this->input->post('tanggal2', TRUE);
		$tanggal     = $this->input->post('tgl', TRUE);

		$user = $this->session->userdata('sess_login');
		$nik   = $user['userid'];

		$kode_jadwal = array_unique($this->input->post('kd_jadwal', TRUE), SORT_REGULAR);
		
		// 
		$this->db->where_in('kd_jadwal', $kode_jadwal);
		$this->db->where('pertemuan', $pertemuan);
		$res = $this->db->delete('tbl_absensi_mhs_new_20171');

		if ($res) {
			$a = 1;
			for ($i = 0; $i < $jumlahabsen - 1; $i++) {
				$hasil = $this->input->post('absen' . $a . '[0]', TRUE);

				$pecah = explode('-', $hasil);

				$data['sblm_update']   = $tanggal2;
				$data['pertemuan']     = $pertemuan;
				$data['kehadiran']     = $pecah[0];
				$data['npm_mahasiswa'] = $pecah[1];
				$data['kd_jadwal']     = $pecah[2];
				
				$data['tanggal']       = $tanggal;
				$data['sblm_update']   = $tanggal2;
				$data['user_update']   = $nik;

				$this->db->insert('tbl_absensi_mhs_new_20171', $data);

				$a++;
			}

			echo "<script>alert('Sukses');
				document.location.href='" . base_url() . "akademik/ajar/edit_absen/" . $id . "';</script>";
		}else{
			echo "<script>alert('Gagal Edit Absen');
				document.location.href='" . base_url() . "akademik/ajar/edit_absen/" . $id . "';</script>";
		}
	}

	public function export_excel()
	{
		$data['tahunakademik'] = $this->session->userdata('tahunajaran');
		$prodi          = $this->session->userdata('id_jurusan_prasyarat');
		$data['dosen']  = $this->ajarmod->get_dosen_ajar($data['tahunakademik'],$prodi);
		$data['detail'] = $this->db->query("SELECT a.prodi, b.fakultas FROM tbl_jurusan_prodi a 
											JOIN tbl_fakultas b ON a.kd_fakultas = b.kd_fakultas 
											where a.kd_prodi = '$prodi'")->row();
		$this->load->view('export_list_dosen_ajar', $data);
	}

	public function hapus_pertemuan($id, $pertemuan)
	{

		$kd_jadwal = $this->db->query('select * from tbl_jadwal_matkul where id_jadwal = ' . $id . '')->row();
		// if ($kd_jadwal->gabung > 0) {
		// 	$where = 'kd_jadwal = "'.$kd_jadwal->kd_jadwal.'" OR kd_jadwal = "'.$kd_jadwal->referensi.'"
		// 									OR kd_jadwal IN (SELECT DISTINCT kd_jadwal FROM tbl_jadwal_matkul WHERE referensi = "'.$kd_jadwal->referensi.'")';
		// } else {
		// 	$where = 'kd_jadwal = "'.$kd_jadwal->kd_jadwal.'" 
		// 									OR kd_jadwal IN (SELECT DISTINCT kd_jadwal FROM tbl_jadwal_matkul WHERE referensi = "'.$kd_jadwal->kd_jadwal.'")';
		// }
		$where = 'kd_jadwal = "' . $kd_jadwal->kd_jadwal . '"';
		$this->db->where($where);
		$this->db->where('pertemuan', $pertemuan);
		$this->db->delete('tbl_absensi_mhs_new_20171');
		//$this->db->where('kd_jadwal', $kd_jadwal);
		//$this->db->where('pertemuan', $pertemuan);
		//$this->db->delete('tbl_absensi_mhs_new_20171');

		echo "<script>alert('Berhasil Menghapus Jadwal !!');history.go(-1);</script>";
	}

	public function detail_course($id_jadwal='', $nid = '')
	{
		$sql = "SELECT * FROM tbl_materi_abs where kd_jadwal in (select kd_jadwal from tbl_jadwal_matkul where id_jadwal =  $id_jadwal) and created_by = $nid";
		$detail = $this->db->query($sql)->result();
		
		$data = [
			'detail' => $detail,
		];

		$this->load->view('absensi/v_list_detail_course', $data);
	}

	/**
	 * Berita Acara Ujian
	 */
	public function berita_acara_ujian($kodeMatkul = '',$kelas= '')
	{
		$kelas = rawurldecode($kelas);
		$tahunajaran = $this->session->userdata('tahunajaran') == "" ? getactyear() : $this->session->userdata('tahunajaran');
		$kd_dosen = $this->session->userdata('userid');

		$sql = "SELECT kd_matakuliah,kd_dosen,kelas,kd_tahunajaran,kd_jadwal, id_matakuliah,gabung,referensi,
					CASE
						WHEN waktu_kelas = 'PG' THEN 'Pagi'
						WHEN waktu_kelas = 'SR' THEN 'Sore'
						WHEN waktu_kelas = 'PK' THEN 'Karyawan'
						else '' 
					END waktu_kelas
				FROM tbl_jadwal_matkul WHERE kd_matakuliah = ? AND kd_dosen = ? AND kd_tahunajaran = ? AND kelas = ?";

		$jadwal = $this->db->query($sql, [$kodeMatkul, $kd_dosen, $tahunajaran, $kelas])->row();

		$id_matkul = $jadwal->id_matakuliah;
		$jenisKelas = $jadwal->waktu_kelas;
		$kd_jadwal = $jadwal->kd_jadwal;

		$ba = $this->db->query("select * from tbl_log_ba_ujian where kd_jadwal = ?", [$kd_jadwal]);

		$temp = $this->db->query("SELECT nama_matakuliah,semester_matakuliah, kd_prodi FROM tbl_matakuliah WHERE id_matakuliah = ?", [$id_matkul])->row();
		
		$matkulName   = $temp->nama_matakuliah;
		$semester     = $temp->semester_matakuliah;

		$kd_prodi = "-";
		$prodiName = "-";
		$fakultasName = "";

		if (is_null($jadwal->gabung)) {
			$kd_prodi     = $temp->kd_prodi;
			$fakultasName = get_fak_byprodi($kd_prodi);
			$prodiName = $this->db->query("SELECT prodi FROM tbl_jurusan_prodi where kd_prodi = ?", [$kd_prodi])->row()->prodi;
		}else{
			$fakultasName = get_fak(substr($kd_jadwal, 0, 1));
		}

		$update = false;

		if ($ba->num_rows() == 0) {
			

			$ta     = $this->db->query("SELECT tahun_akademik FROM tbl_tahunakademik WHERE kode = '{$tahunajaran}' ")->row()->tahun_akademik;
			$dosen  = $this->db->query("SELECT nama FROM tbl_karyawan WHERE nid = ?", [$kd_dosen])->row()->nama;
			$temp   = $this->db->query("SELECT prodi, start_date, end_date, start_time, kd_ruang FROM tbl_jadwaluji WHERE kd_jadwal = ? AND tahunakademik = ?", [$jadwal->kd_jadwal, $tahunajaran])->row();
			$jmlMhs = $this->db->query("SELECT npm_mahasiswa FROM tbl_krs WHERE kd_jadwal = '{$jadwal->kd_jadwal}'")->num_rows();
			$ruang  = $this->db->query("select ruangan from tbl_ruangan where id_ruangan  = ?", [$temp->kd_ruang])->row()->ruangan;
		}else{
			$ba         = $ba->row();
			$semester   = $ba->smt;	
			$ta         = $ba->tahunajaran;
			$matkulName = $ba->matkul;
			$kd_dosen   = $ba->kd_dosen;
			
			$dosen      = $this->db->query("SELECT nama FROM tbl_karyawan WHERE nid = ?", [$kd_dosen])->row()->nama;
			$jmlMhs     = $ba->total_peserta;
			$ruang      = $ba->ruang;
			$update     = true;
			$tipe_uji = $ba->tipe_uji;
		}

		$data = [
			'kelas'      => $jenisKelas,
			'prodi'      => strtoupper(trim($prodiName)),
			'fakultas'   => strtoupper(trim($fakultasName)),
			'semester'   => $semester,
			'ta'         => $ta,
			'matkulName' => $matkulName,
			'dosen'      => $dosen,
			'kd_dosen'   => $kd_dosen,
			'tipe_uji'   => @$tipe_uji ? $tipe_uji : "",
			'hari'       => notohari(date('w', strtotime($temp->start_date))),
			'start_time' => date('H:i', strtotime($temp->start_time)),
			'start_date' => date('Y-m-d',strtotime($temp->start_date)),
			'peserta'    => $jmlMhs,
			'kd_jadwal'  => $jadwal->kd_jadwal,
			'page'       => 'akademik/berita_acara/form',
			'ruangan'    => @$ruang ? $ruang : "",
			'update'     => $update,
		];

		$this->load->view('template/template', $data);		
	}

	public function store_ba()
	{	
		$tahunajaran = $this->session->userdata('tahunajaran');
		extract(PopulateForm());

		$check = $this->db->query("select * from tbl_log_ba_ujian where kd_jadwal = ? and smt = ? and kd_dosen = ?", [$kd_jadwal, $smt, $kd_dosen]);

		$exist = false;

		if ($check->num_rows() > 0) {
			$exist = true; 
		}
		if ($exist == false) {
			$dataInsert = [
				'kd_jadwal'     => $kd_jadwal,
				'fakultas'      => $fakultas,
				'prodi'         => $prodi,
				'smt'           => $smt,
				'tahunajaran'   => $tahunajaran,
				'tipe_uji'      => $tipe_uji,
				'tanggal'       => $tanggal,
				'waktu'         => $waktu,
				'ruang'         => $ruang,
				'matkul'        => $matkul,
				'hari'          => $hari,
				'kd_dosen'      => $kd_dosen,
				'total_peserta' => $jml_mhs,
				'catatan'       => $catatan,
				'kls'           => $kls,
				'pengawas1'		=> $pengawas1,
				'pengawas2'		=> $pengawas2,
				'created_at'    => date('Y-m-d H:i:s'),
				'created_by'    => $this->session->userdata('userid'),
			];

			$res = $this->db->insert('tbl_log_ba_ujian', $dataInsert);

			if ($res >= 1) {
				echo "<script>alert('Berita Acara Berhasil Di Buat!!');
				document.location.href='" . base_url() . "akademik/ajar/courses_list';</script>";
			}else{
				echo "<script>alert('Berita Acara Gagal Di Buat</script>";
			}
		}else{
			$dataUpdate = [
				'kd_jadwal'     => $kd_jadwal,
				'fakultas'      => $fakultas,
				'prodi'         => $prodi,
				'smt'           => $smt,
				'tahunajaran'   => $tahunajaran,
				'tipe_uji'      => $tipe_uji,
				'tanggal'       => $tanggal,
				'waktu'         => $waktu,
				'ruang'         => $ruang,
				'matkul'        => $matkul,
				'hari'          => $hari,
				'kd_dosen'      => $kd_dosen,
				'total_peserta' => $jml_mhs,
				'catatan'       => $catatan,
				'kls'           => $kls,
				'pengawas1'		=> $pengawas1,
				'pengawas2'		=> $pengawas2,
				'updated_at'    => date('Y-m-d H:i:s'),
				'updated_by'    => $this->session->userdata('userid'),
			];

			$condition = ['id' => $check->row()->id];
			
			$this->db->update('tbl_log_ba_ujian', $dataUpdate, $condition);

			echo "<script>alert('Berita Acara Berhasil Di Ubah');history.go(-1);</script>";
		}
	}

	public function cetak_ba($id_jadwal='')
	{
		define('IMG_ALBINO', FCPATH.'assets/img/albino.jpg');
		
		if (!empty($id_jadwal)) {
			$tahunajaran = $this->session->userdata('tahunajaran');
			$kd_jadwal = $this->db->query("select kd_jadwal from tbl_jadwal_matkul where id_jadwal = ?", [$id_jadwal])->row()->kd_jadwal;

			$jadwal = $this->db->query("select * from tbl_log_ba_ujian where kd_jadwal = ?",[$kd_jadwal]);
			
			$jmlMhs = 0;
			$data = [];

			if ($jadwal->num_rows() == 0) {
				$jadwal = new stdClass;

				$fakultasName = "………………………";
				$jadwal->fakultas = "………………………";
				$jadwal->prodi = "………………………";
				$jadwal->smt = "………..";
				$jadwal->kls         = "Pagi – Sore";
				$jadwal->kd_jadwal           = "";
				$jadwal->ta      = "………………………";
				$jadwal->tipe_uji            = "";
				$jadwal->kd_dosen            = "";
				$jadwal->tipe                = "UTS / UAS";
				$jadwal->kd_ruang            = "";
				$jadwal->tanggal          = "……………………………….";
				$jadwal->waktu          = "………………………………";
				$jadwal->total_peserta              = "……………";
				$jadwal->ruang               = "";
				$jadwal->hari                = "……………………………….";
				$jadwal->matkul                = "……………………………….";
				$jadwal->ta = "……………";
				$jadwal->catatan = "………………………………………………………………………………………………………………………\r\n………………………………………………………………………………………………………………………";
				$jadwal->pengawas1 = "……………….";
				$jadwal->pengawas2 = "……………….";
			}else{
				$jadwal = $jadwal->row();

				if ($jadwal->tipe_uji == 1) {
					$jadwal->tipe = "UTS";
				}elseif ($jadwal->tipe_uji == 2){
					$jadwal->tipe = "UAS";
				}
				
				$ta        = $this->db->query("SELECT tahun_akademik FROM tbl_tahunakademik WHERE kode = ? ", [$tahunajaran])->row()->tahun_akademik;
				$jadwal->ta = $ta;
			}
			
		    $data = [
				'jadwal' => $jadwal,
			];

			$dosen = get_detail_dosen($jadwal->kd_dosen);
			$jadwal->dosen = $dosen["nama"];
			
			$tmpPath = sys_get_temp_dir();
			$page = 'akademik/berita_acara/berita_acara_ujian';
			$html = $this->load->view($page, $data, true);

			try {
				$mpdf = new \Mpdf\Mpdf(['tempDir' => $tmpPath]);
				// satuan mm 
				$mpdf->AddPageByArray([
					'margin-left'   => 15,
					'margin-right'  => 15,
					'margin-top'    => 4,
					'margin-bottom' => 0,
				]);
				$mpdf->WriteHTML($html);

				$mpdf->Output();
			} catch (\Mpdf\MpdfException $e) {
				print "Creating an mPDF object failed with ". $e->getMessage();
			}	
		}else{
			echo "<center>Periksa Kembali ID Jadwal <h2 style=\"display:inline;\">".":☺:"."</h2></center>";
			echo "<br>";
			echo "<br>";
			echo "<marquee behavior=\"alternate\" width=\"100%\">
				    <marquee behavior=\"alternate\" direction=\"down\">
				        <marquee behavior=\"alternate\" direction=\"up\"><h1>•</h1></marquee>
				    </marquee>
				</marquee>";
			echo "<marquee behavior=\"alternate\" width=\"100%\" style=\"margin-top:-70px;\">
				    <marquee behavior=\"alternate\" direction=\"up\">
				        <marquee behavior=\"alternate\" direction=\"down\"><h1>•</h1></marquee>
				    </marquee>
				</marquee>";
		}
	}
}

/* End of file Ajar.php */
/* Location: .//C/Users/danum246/AppData/Local/Temp/fz3temp-1/Ajar.php */
