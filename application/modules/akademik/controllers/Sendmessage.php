<?php
defined('BASEPATH') or exit('No direct script access allowed');

/**
 * Controller ini hanya digunakan untuk melakukan pengiriman pesan saja.
 * Baik email maupun SMS yang dikirim ketika mas perwalian.
 */

class Sendmessage extends MY_Controller
{

	public function __construct()
	{
		parent::__construct();
		// if user dont have access
		if (!$this->session->userdata('sess_login')) {
			echo "<script>alert('Akses tidak diizinkan!');history.go(-1);</script>";
			return;
		}
	}

	function sendSms($kodekrs)
	{
		// if user dont have kode krs as param
		if (!$kodekrs || empty($kodekrs)) {
			echo "<script>alert('Akses tidak diizinkan!');history.go(-1);</script>";
			return;
		}
		// sms to mhs
		$this->_sendMessage($kodekrs);

		// sms to lecture
		$this->_sendMessageToLect($kodekrs);

		redirect(base_url('akademik/sendmessage/sendMail/' . $kodekrs));
	}

	function _sendMessage($kdkrs)
	{
		$npm = substr($kdkrs, 0, 9);

		$phone = $this->app_model->getdetail('tbl_bio_mhs', 'npm', $npm, 'npm', 'asc')->row()->no_hp;

		$subs  = substr($phone, 1, 13);

		$curl = curl_init();
		curl_setopt_array($curl, array(
			CURLOPT_RETURNTRANSFER => 1,
			CURLOPT_URL => 'http://api.nusasms.com/api/v3/sendsms/plain',
			CURLOPT_POST => true,
			CURLOPT_POSTFIELDS => array(
				'user' => 'ubharajaya2_api',
				'password' => 'U6psYjL',
				// 'user' => 'ubharajaya1_api',
				// 'password' => 'P5Mqr6R',
				'SMSText' => get_nm_mhs($npm) . ', KRS anda sudah diterima oleh sistem kami, harap hubungi dosen PA anda. -SIA-'.$this->ORG_NAME,
				'GSM' => '62' . $subs
			)
		));
		curl_exec($curl);
		curl_close($curl);
	}

	function _sendMessageToLect($kdkrs)
	{
		$npm = substr($kdkrs, 0, 9);

		$lec = $this->app_model->getdetail('tbl_verifikasi_krs', 'kd_krs', $kdkrs, 'kd_krs', 'asc')->row()->id_pembimbing;

		$bio = $this->app_model->getdetail('tbl_biodata_dosen', 'nid', $lec, 'nid', 'asc')->row();

		$sub = substr($bio->tlp, 1, 13);

		$curl = curl_init();
		curl_setopt_array($curl, array(
			CURLOPT_RETURNTRANSFER => 1,
			CURLOPT_URL => 'http://api.nusasms.com/api/v3/sendsms/plain',
			CURLOPT_POST => true,
			CURLOPT_POSTFIELDS => array(
				'user' => 'ubharajaya2_api',
				'password' => 'U6psYjL',
				// 'user' => 'ubharajaya1_api',
				// 'password' => 'P5Mqr6R',
				'SMSText' => 'Yth, Bpk/Ibu Dosen Pembimbing Akademik, mahasiswa bimbingan Anda dengan NPM ' . $npm . ', telah mengajukan KRS. mohon respon secepatnya. Terimakasih. -SIA-'.$this->ORG_NAME,
				'GSM' => '62' . $sub
			)
		));

		curl_exec($curl);
		curl_close($curl);
	}

	function sendMail($kodekrs)
	{
		// send email to lecture
		$this->_sendMail($kodekrs);

		// email to mhs
		$this->_sendMailtoMhs($kodekrs);

		redirect('akademik/krs_mhs/jadwal_kuliah', 'refresh');
	}

	function _sendMail($kodekrs)
	{
		$nim = substr($kodekrs, 0, 9);
		// get some information base on kodekrs
		$datakrs = $this->app_model->getdetail('tbl_verifikasi_krs', 'kd_krs', $kodekrs, 'id_verifikasi', 'asc')->row();

		//generate key
		$hash 	= NULL;
		$n 		= 20; // jumlah karakter yang akan di bentuk.
		$chr 	= "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvqxyz0123456789";

		for ($i = 0; $i < $n; $i++) {
			$rIdx = rand(1, strlen($chr));
			$hash .= substr($chr, $rIdx, 1);
		}
		$key = $hash . date('ymdHis');
		//end generate key

		//insert tbl_keykrs
		$data_mail 	= array(
			'idkey' 	=> $key,
			'kdkrs' 	=> $kodekrs,
			'dateinput' => date('Y-m-d H:i:s')
		);

		$this->db->insert('tbl_keykrs', $data_mail);
		//end insert tbl_keykrs

		// get email dosen
		$get_mail = $this->app_model->getdetail('tbl_biodata_dosen', 'nid', $datakrs->id_pembimbing, 'nid', 'asc')->row()->email;
		$get_nama = $this->app_model->getdetail('tbl_biodata_dosen', 'nid', $datakrs->id_pembimbing, 'nid', 'asc')->row()->nama;

		// $ses = $this->session->userdata('sess_login');
		// if ($ses['usex`rid'] == '201510415027') {
		// 	die($get_mail);
		// }

		$isi = '<p>Yang terhormat ' . $get_nama . ',</p>
				<p>Mahasiswa anda dengan nama ' . get_nm_mhs($nim) . ' (' . $nim . ') mengajukan KRS pada tanggal ' . date('Y-m-d H:i:s') . '</p>
				<p>Segera lakukan validasi KRS tersebut dengan melakukan klik tombol di bawah ini: </p>' . anchor('http://sia.<?= $this->URL ?>/akademik/directlogin/saveLog/' . $key) . '
				</br>
				<p><b>NB: Harap melakukan validasi Sebelum tanggal ' . date('Y-m-d') . ' pukul 23:59 WIB</b></p>
				<p>Untuk informasi, silahkan hubungi Direktorat Pengembangan Teknologi dan Informasi di email berikut <em>it@<?= $this->URL ?></em></p>
				
				<p>Terima Kasih</p>
				</br>
				</br>
				<p><i>UNIVERSITAS ABCD</i></p>';

		// komponen email
		$judul = 'Pengajuan KRS Mahasiswa';

		$config = array(
			'protocol' 	=> 'smtp',
			'smtp_host' => 'ssl://smtp.gmail.com',
			'smtp_port' => 465,
			'smtp_user' => 'puskom@'.$this->URL, //email id
			'smtp_pass' => 'Puskom787566',
			'mailtype'  => 'html',
			'charset'   => 'iso-8859-1'
		);

		$this->load->library('email', $config);
		$this->email->set_newline("\r\n");

		$this->email->from('it@'.$this->URL, 'UNIVERSITAS '.$this->ORG_NAME);
		$this->email->to($get_mail); // email array
		$this->email->subject($judul);
		$this->email->message($isi);

		$result = $this->email->send();
	}

	function _sendMailtoMhs($kdkrs)
	{
		$nim = substr($kdkrs, 0, 9);
		// get some information base on kodekrs
		$datakrs = $this->app_model->getdetail('tbl_bio_mhs', 'npm', $nim, 'npm', 'asc')->row();

		$isi = '<p>Salam,</p>
				<p>Terimakasih telah melakukan pengajuan KRS.</p>
				<p>Untuk informasi status pengajuan KRS anda silahkan tunggu notifikasi berikutnya yang akan dikirimkan ke e-mail anda.</p>
				</br>
				<p>Untuk informasi, silahkan hubungi Direktorat Pengembangan Teknologi dan Informasi di email berikut <em>puskom@'.$this->URL.'</em></p>
				<p>Terima Kasih</p>
				</br>
				</br>
				<p><em>UNIVERSITAS ABCD</em></p>';

		// komponen email
		$judul = 'Pengajuan KRS Mahasiswa';

		$config = array(
			'protocol' 	=> 'smtp',
			'smtp_host' => 'ssl://smtp.gmail.com',
			'smtp_port' => 465,
			'smtp_user' => 'puskom@'.$this->URL, //email id
			'smtp_pass' => 'Puskom787566',
			'mailtype'  => 'html',
			'charset'   => 'iso-8859-1'
		);

		$this->load->library('email', $config);
		$this->email->set_newline("\r\n");

		$this->email->from('puskom@'.$this->URL, 'UNIVERSITAS '.$this->ORG_NAME);
		$this->email->to($datakrs->email); // email array
		$this->email->subject($judul);
		$this->email->message($isi);

		$result = $this->email->send();
	}
}

/* End of file Sendmessage.php */
/* Location: ./application/modules/akademik/controllers/Sendmessage.php */
