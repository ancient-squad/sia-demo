<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Bkd_model extends CI_Model {

	public function current_teaching($uid)
	{
		$activeYear = getactyear();
		$teaches 	= $this->db->query("SELECT a.kd_matakuliah, b.nama_matakuliah, b.sks_matakuliah, a.kelas, a.hari, a.waktu_mulai, a.waktu_selesai FROM tbl_jadwal_matkul a
										JOIN tbl_matakuliah b ON a.id_matakuliah = b.id_matakuliah
										WHERE a.kd_dosen = '$uid' 
										AND a.kd_tahunajaran = '$activeYear'")->result();
		return $teaches;
	}

	public function research_list($uid)
	{
		$researches = $this->db->query("SELECT a.id, a.judul, a.tahunakademik, a.sks, a.status, b.program, c.kegiatan, d.param FROM tbl_lecturer_research a
										JOIN tbl_program_penelitian b ON a.program = b.kode_program
										JOIN tbl_kegiatan_program c ON c.kode_kegiatan = a.kegiatan
										JOIN tbl_research_params d ON d.kode = a.param
										WHERE user = '$uid'")->result();
		return $researches;
	}

	public function research_list_by_year($uid, $tahunakademik)
	{
		$researches = $this->db->query("SELECT a.id,a.judul, a.tahunakademik, a.sks, a.status, b.program, c.kegiatan, d.param FROM tbl_lecturer_research a
										JOIN tbl_program_penelitian b ON a.program = b.kode_program
										JOIN tbl_kegiatan_program c ON c.kode_kegiatan = a.kegiatan
										JOIN tbl_research_params d ON d.kode = a.param
										WHERE user = '$uid'
										AND a.tahunakademik = '$tahunakademik'")->result();
		return $researches;
	}

	public function additional_teaching_list($uid, $year)
	{
		$teaches 	= $this->db->query("SELECT a.id,a.sks, b.komponen, a.status FROM tbl_pengajaran_tambahan_dosen a
										JOIN tbl_pengajaran_tambahan b ON a.kode_pengajaran = b.kode_pengajaran
										WHERE a.nid = '$uid'
										AND a.tahunakademik = '$year'")->result();
		return $teaches;
	}

	public function get_structural_data($uid,$year)
	{
		$structural = $this->db->query("SELECT b.jabatan, c.tahun_akademik FROM tbl_struktural_dosen a
	    								JOIN tbl_jabatan_struktural b ON a.id_jabatan = b.id
	    								JOIN tbl_tahunakademik c ON c.kode = a.tahunakademik
	    								WHERE a.nid = '$uid'
	    								AND a.tahunakademik = '$year'")->row();
		return $structural;
	}

	public function get_devotion_program($code)
	{
		$devProgram = $this->db->query("SELECT a.kode_program, a.program, b.param, b.sks FROM tbl_abdimas_program a
										JOIN tbl_abdimas_bobot b ON a.kode_program = b.kode_program
										WHERE a.kode_abdimas = '$code'
										GROUP BY a.kode_program
										ORDER BY a.kode_program ASC")->result();
		return $devProgram;
	}

	public function get_devotion_credit($code, $param)
	{
		$credit= "";
		if (!empty($code) && !empty($param)) {
			$credit = $this->db->query("SELECT sks FROM tbl_abdimas_bobot WHERE kode_program = '$code' AND param = '$param'")->row()->sks;	
		} elseif (!empty($code) && empty($param)) {
			$credit = $this->db->query("SELECT sks FROM tbl_abdimas_bobot WHERE kode_program = '$code'")->row()->sks;
		} elseif ($code == "" && !empty($param)) {
			$credit = "";
		} else {
			$credit = "";
		}
		return $credit;
	}

	public function get_devotion_param($code)
	{
		$param = $this->db->query("SELECT * FROM tbl_research_params WHERE group_param = '$code'")->result();
		return $param;
	}

	public function get_lecturer_devotion($nid, $tahunakademik)
	{
		$devotions 	= $this->db->query("SELECT a.id,b.program, a.bobot_sks, a.param,a.status FROM tbl_abdimas_dosen a
										JOIN tbl_abdimas_program b ON a.program = b.kode_program
										WHERE a.nid = '$nid'
										AND a.tahunakademik = '$tahunakademik'")->result();
		return $devotions;
	}

	public function aditional_teaching($uid, $tahunakademik)
	{
		$aditional 	= $this->db->query("SELECT a.sks, b.komponen FROM tbl_pengajaran_tambahan_dosen a
										JOIN tbl_pengajaran_tambahan b
										ON a.kode_pengajaran = b.kode_pengajaran
										WHERE nid = '$uid'
										AND a.tahunakademik = '$tahunakademik'")->result();
		return $aditional;
	}

	public function get_struktural_dosen($nid, $tahunakademik)
	{
		$position = $this->db->query("SELECT a.id_jabatan, b.jabatan, a.sks, a.status FROM tbl_struktural_dosen a
									JOIN tbl_jabatan_struktural b
									ON a.id_jabatan = b.id
									WHERE a.nid = '$nid'
									AND a.tahunakademik = '$tahunakademik'")->result();
		return $position;
	}

	public function get_devotion_list($nid)
	{
		$devotions 	= $this->db->query("SELECT 
											a.id, 
											a.tahunakademik, 
											a.bobot_sks, 
											a.status, 
											a.param,
											b.program, 
											c.nama
										FROM tbl_abdimas_dosen a
										JOIN tbl_abdimas_program b ON a.program = b.kode_program
										JOIN tbl_abdimas c ON c.kode = b.kode_abdimas
										WHERE a.nid = '$nid'")->result();
		return $devotions;
	}

	public function teaching_detail($nid)
	{
		$teaching 	= $this->db->query("SELECT a.*, b.komponen FROM tbl_pengajaran_tambahan_dosen a
										JOIN tbl_pengajaran_tambahan b ON a.kode_pengajaran = b.kode_pengajaran
										WHERE a.nid = '$nid'")->result();
		return $teaching;
	}

}

/* End of file Bkd_model.php */
/* Location: ./application/modules/akademik/models/Bkd_model.php */