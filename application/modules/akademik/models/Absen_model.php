<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Absen_model extends CI_Model {

	public $variable;

	public function __construct()
	{
		parent::__construct();
		
	}

	function loadListJadwal($kdkrs,$day)
	{
		$this->db->select('b.*');
		$this->db->from('tbl_krs a');
		$this->db->join('tbl_jadwal_matkul b', 'a.kd_jadwal = b.kd_jadwal');
		$this->db->where('b.hari', $day);
		$this->db->like('a.kd_krs', $kdkrs, 'after');
		return $this->db->get();
	}

	function loadPresence($kdjdl)
	{
		$actyear = getactyear();

		$this->db->where('kd_jadwal', $kdjdl);
		$this->db->order_by('pertemuan', 'desc');
		$keyTransaction = $this->db->get('tbl_absensi_mhs_new_'.$actyear, 1)->row();

		$this->db->where('transaksi', $keyTransaction->transaksi);
		return $this->db->get('tbl_absensi_mhs_new_'.$actyear);
	}

	function validAbsen($trk)
	{
		$log = $this->session->userdata('sess_login');
		$act = getactyear();

		$this->db->where('transaksi', $trk);
		$this->db->where('kehadiran', 'H');
		$h = $this->db->get('tbl_absensi_mhs_new_'.$act);

		$this->db->where('transaksi', $trk);
		$this->db->where('kehadiran', 'I');
		$i = $this->db->get('tbl_absensi_mhs_new_'.$act);

		$this->db->where('transaksi', $trk);
		$this->db->where('kehadiran', 'S');
		$s = $this->db->get('tbl_absensi_mhs_new_'.$act);

		$this->db->where('transaksi', $trk);
		$this->db->where('kehadiran', 'A');
		$a = $this->db->get('tbl_absensi_mhs_new_'.$act);

		$data = [
			'kd_transaksi'	=> $trk,
			'kd_jadwal'		=> $h->row()->kd_jadwal,
			'alfa' 			=> $a->num_rows(),
			'hadir' 		=> $h->num_rows(),
			'ijin' 			=> $i->num_rows(),
			'sakit' 		=> $s->num_rows(),
			'audit_dosen'	=> '',
			'audit_mhs'		=> $log['userid'],
			'timestamp'		=> date('Y-m-d H:i:s')
		];

		$this->db->insert('tbl_transaksi_absen_'.getactyear(), $data);

		return TRUE;
	}

	public function isExist($table="",$data=[])
	{
		if (empty($table)) {
			return 'table name is missing';
		}elseif (is_array($data)) {
			foreach ($data as $key => $value) {
				$this->db->where($key, $value);
			}

			$data = $this->db->get($table)->num_rows();
		
			if ($data > 0) {
				return true;
			}
			return false;
		}else{
			return 'Invalid data type';
		}
	}

}

/* End of file Absen_model.php */
/* Location: ./application/models/Absen_model.php */