<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Publikasi_model extends CI_Model {

	public $variable;

	public function __construct()
	{
		parent::__construct();
		
	}

	public function publikasi_from_baa($prodi, $akademik, $semester)
	{
		$this->db->distinct();
		$this->db->select('
				kry.nama,
				jdl.`id_jadwal`,
				jdl.kelas,
				jdl.`kd_matakuliah`,
				mk.`kd_matakuliah` AS kd_mk,
				mk.`nama_matakuliah`,
				mk.`sks_matakuliah`,
				jdl.`kd_jadwal`,
				jdl.`kd_dosen`,
				mk.`semester_matakuliah`,
				mk.`kd_prodi`');
		$this->db->from('tbl_jadwal_matkul jdl');
		$this->db->join('tbl_matakuliah mk', 'jdl.kd_matakuliah = mk.kd_matakuliah');
		$this->db->join('tbl_karyawan kry', 'kry.nid = jdl.kd_dosen');
		$this->db->join('tbl_kurikulum_matkul_new e', 'mk.kd_matakuliah = e.kd_matakuliah');
		$this->db->like('jdl.kd_jadwal', $prodi,'after');
		$this->db->where('mk.`kd_prodi`', $prodi);
		$this->db->where('jdl.`kd_tahunajaran`', $akademik);
		$this->db->where('e.`semester_kd_matakuliah`', $semester);
		$this->db->order_by('mk.kd_matakuliah','ASC');
		return $this->db->get()->result();
	}

	public function publikasi_from_staff($prodi, $akademik, $semester)
	{
		$fakultas = get_kdfak_byprodi($prodi);
		$this->db->distinct();
		$this->db->select('
				kry.nama,
				jdl.`id_jadwal`,
				jdl.kelas,
				jdl.kd_jadwal,
				jdl.`kd_matakuliah`,
				mk.`kd_matakuliah` AS kd_mk,
				mk.`nama_matakuliah`,
				mk.`sks_matakuliah`,
				jdl.`kd_jadwal`,
				jdl.`kd_dosen`,
				mk.`semester_matakuliah`,
				mk.`kd_prodi`');
		$this->db->from('tbl_jadwal_matkul jdl');
		$this->db->join('tbl_matakuliah mk', 'jdl.id_matakuliah = mk.id_matakuliah');
		$this->db->join('tbl_karyawan kry', 'kry.nid = jdl.kd_dosen');
		$this->db->join('tbl_kurikulum_matkul_new e', 'jdl.kd_matakuliah = e.kd_matakuliah');
		$this->db->group_start();
		$this->db->like('jdl.kd_jadwal', $prodi,'after');
		$this->db->or_like('jdl.kd_jadwal', $fakultas.'/','after');
		$this->db->group_end();
		// $this->db->where('mk.`kd_prodi`', $prodi);
		$this->db->where('jdl.`kd_tahunajaran`', $akademik);
		$this->db->where('e.`semester_kd_matakuliah`', $semester);
		$this->db->order_by('mk.kd_matakuliah','ASC');
		return $this->db->get()->result();
	}

	public function publikasi_from_fakultas($prodi, $akademik, $semester)
	{
		$this->db->distinct();
		$this->db->select('kry.nama,jdl.`id_jadwal`,jdl.kelas,jdl.kd_jadwal,jdl.`kd_matakuliah`,mk.`kd_matakuliah` AS kd_mk,mk.`nama_matakuliah`,mk.`sks_matakuliah`,jdl.`kd_jadwal`,jdl.`kd_dosen`,mk.`semester_matakuliah`,mk.`kd_prodi`');
		$this->db->from('tbl_jadwal_matkul jdl');
		$this->db->join('tbl_matakuliah mk', 'jdl.kd_matakuliah = mk.kd_matakuliah');
		$this->db->join('tbl_karyawan kry', 'kry.nid = jdl.kd_dosen');
		$this->db->join('tbl_kurikulum_matkul_new e', 'mk.kd_matakuliah = e.kd_matakuliah');
		$this->db->like('jdl.kd_jadwal', $prodi,'after');
		$this->db->where('jdl.`kd_tahunajaran`', $akademik);
		$this->db->where('mk.`kd_prodi`', $prodi);
		$this->db->where('e.`semester_kd_matakuliah`', $semester);
		$this->db->order_by('mk.kd_matakuliah','ASC');
		return $this->db->get()->result();
	}
}

/* End of file Publikasi_model.php */
/* Location: ./application/models/Publikasi_model.php */