<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tahunakademik_model extends CI_Model {

	public function show_all()
	{
		return $this->db->get('tbl_tahunakademik');
	}

	public function get_year($code)
	{
		return $this->db->get_where('tbl_tahunakademik', ['kode' => $code]);
	}

	public function save($kode, $nama,$tglmulai,$tglakhir)
	{
		$this->db->insert('tbl_tahunakademik', ['kode' => $kode, 'tahun_akademik' => $nama,'tglmulai' => $tglmulai,'tglakhir' => $tglakhir, 'status' => 0]);
		return $this->db->affected_rows();
	}

	public function update_year($kode, $nama, $tglmulai,$tglakhir,$old_kode)
	{
		$this->db->update('tbl_tahunakademik', ['kode' => $kode, 'tahun_akademik' => $nama,'tglmulai' => $tglmulai,'tglakhir' => $tglakhir], ['kode' => $old_kode]);
		return $this->db->affected_rows();
	}

	public function activate($kode)
	{
		$this->db->update('tbl_tahunakademik', ['status' => 0]);
		$this->db->update('tbl_tahunakademik', ['status' => 1], ['kode' => $kode]);
		return $this->db->affected_rows();
	}

	public function last_active_year($year)
	{
		return $this->db->select('tahunajaran')->order_by('tahunajaran', 'desc')->get('tbl_verifikasi_krs', 1)->row();
	}

}

/* End of file Tahunakademik_model.php */
/* Location: ./application/modules/akademik/models/Tahunakademik_model.php */