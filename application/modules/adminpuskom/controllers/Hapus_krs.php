<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Hapus_krs extends MY_Controller
{

	public function index()
	{
		$data['page'] = 'v_hps_krs';
		$this->load->view('adminpuskom/template', $data);
	}

	function load()
	{
		$nim = $this->input->post('npm');
		$smt = $this->input->post('smt');
		$this->session->set_userdata('npm', $nim);
		$this->session->set_userdata('str', $smt);
		redirect(base_url('adminpuskom/hapus_krs/cari'));
	}

	function cari()
	{
		$data['kol'] = $this->db->query('SELECT * from tbl_krs where npm_mahasiswa = "' . $this->session->userdata('npm') . '" and semester_krs = "' . $this->session->userdata('str') . '"')->row();
		if ($data['kol'] == FALSE) {
			echo "<script>alert('Akun Tidak Ditemukan! Periksa User ID Pengguna!');document.location.href='" . base_url() . "adminpuskom/hapus_krs';</script>";
		} else {
			$data['suit'] = $this->db->query('SELECT * from tbl_krs a join tbl_mahasiswa b on a.npm_mahasiswa=b.NIMHSMSMHS where npm_mahasiswa = "' . $this->session->userdata('npm') . '" and semester_krs = "' . $this->session->userdata('str') . '"')->row();
			$data['saw'] = $this->db->query('SELECT a.npm_mahasiswa, a.kd_matakuliah, a.semester_krs, b.nama_matakuliah, a.id_krs, d.nama from tbl_krs a join tbl_matakuliah b on a.kd_matakuliah=b.kd_matakuliah
											left join tbl_jadwal_matkul c on c.kd_jadwal = a.kd_jadwal left join tbl_karyawan d on d.nid=c.kd_dosen
											where a.npm_mahasiswa = "' . $this->session->userdata('npm') . '" and a.semester_krs = "' . $this->session->userdata('str') . '"')->result();
			$data['page'] = "v_result_krs";
			$this->load->view('adminpuskom/template', $data);
		}
	}


	function delete($id)
	{
		$this->db->query('DELETE from tbl_krs where id_krs = ' . $id . ' ');
		echo "<script>alert('Berhasil !');document.location.href='" . base_url() . "adminpuskom/hapus_krs';</script>";
	}
}


/* End of file Hapus_krs.php */
/* Location: ./application/modules/adminpuskom/controllers/Hapus_krs.php */
