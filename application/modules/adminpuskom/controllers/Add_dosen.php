<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Add_dosen extends MY_Controller
{

	public function index()
	{
		$data['page'] = 'v_add_dosen';
		$this->load->view('adminpuskom/template', $data);
	}

	function add()
	{
		$sesi = $this->session->userdata('sess_mbalia');
		//$sess = $sesi['username'];

		$this->load->helper('inflector');
		$nama = underscore($_FILES['userfile']['name']);
		$size = $_FILES['userfile']['size'];

		$config['upload_path'] = './temp_upload/';
		$config['allowed_types'] = 'jpg|png|jpeg|gif';
		$config['file_name'] = $nama;
		$config['max_size']	= '100000';
		$config['max_width']  = '102400';
		$config['max_height']  = '76800';

		//var_dump($config);

		$patt = $config['upload_path'] . basename($nama);

		$this->load->library('upload', $config);

		if (!$this->upload->do_upload('userfile')) {
			$error = array('error' => $this->upload->display_errors());
			var_dump($error);
		} else {
			$data = array(
				'nidn'			=> $this->input->post('nidn'),
				'nid'			=> $this->input->post('nid'),
				'nupn'			=> $this->input->post('nupn'),
				'nik'			=> $this->input->post('nik'),
				'nama'			=> $this->input->post('nama'),
				'jns_kel'		=> $this->input->post('jenis'),
				'alamat'		=> $this->input->post('alamat'),
				'hp'			=> $this->input->post('hp'),
				'email'			=> $this->input->post('email'),
				'jabatan_id'	=> $this->input->post('jab'),
				'status'		=> $this->input->post('sts'),
				'pictures'		=> $patt,
				'nik_type'		=> $this->input->post('typ'),
				'tgl_masuk'		=> $this->input->post('tgl')
			);
			var_dump($data);
			exit();
			$this->app_model->insertdata('tbl_karyawan', $data);
			echo "<script>alert('Sukses');
				document.location.href='" . base_url() . "adminpuskom/add_dosen';</script>";
		}
		//$this->delete_upload($patt);


	}
}

/* End of file Add_dosen.php */
/* Location: ./application/modules/adminpuskom/controllers/Add_dosen.php */
