<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Konversi extends MY_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->model('temph_model');
	}

	public function index()
	{
		$sess = $this->session->userdata('sess_login');
		$data['load'] = $this->temph_model->mhs_konv($sess['userid']);
		$data['page'] = "feed_konv";
		$this->load->view('template/template', $data);
	}

	function save_session_mhs($nim){
		$this->session->set_userdata('mhs',get_nm_mhs($nim));
		$this->session->set_userdata('nim',$nim);

		redirect(base_url('sync_feed/konversi/loadDetil'),'refresh');
	}

	function loadDetil()
	{
		$user = $this->session->userdata('sess_login');
		$data['prod'] = $user['userid']; 
		$nim = $this->session->userdata('nim');
		$data['npm'] = $this->session->userdata('nim');
		$mhs = $this->app_model->getdetail('tbl_mahasiswa','NIMHSMSMHS',$nim,'NIMHSMSMHS','asc')->row();
		$data['nilai'] = $this->db->get('tbl_index_nilai')->result();

		if ($mhs->SMAWLMSMHS < '20151') {
			$data['matkul'] =  $this->db->select('*')
										->from('tbl_transaksi_nilai_konversi kv')
										->join('tbl_matakuliah_copy mk','mk.kd_matakuliah = kv.KDKMKTRLNM','left')
										->where('kv.NIMHSTRLNM',$nim)
										->where('mk.kd_prodi',$user['userid'])
										->where('tahunakademik',$mhs->SMAWLMSMHS)
										->order_by('kv.id','desc')
										->get()->result();
		} else {
			$data['matkul'] =  $this->db->select('*')
										->from('tbl_transaksi_nilai_konversi kv')
										->join('tbl_matakuliah mk','mk.kd_matakuliah = kv.KDKMKTRLNM','left')
										->where('kv.NIMHSTRLNM',$nim)
										->where('mk.kd_prodi',$user['userid'])
										->order_by('kv.id','desc')
										->get()->result();
		}
		$data['page'] = "feed_konv_detail";
		$this->load->view('template/template', $data);
	}

	function sync($nim)
	{
		$this->load->library("Nusoap_lib");
        // $url = 'http://172.16.0.58:8082/ws/sandbox.php?wsdl'; // gunakan sandbox untuk coba-coba
        $url = 'http://172.16.0.58:8082/ws/live.php?wsdl'; // gunakan live bila sudah yakin
        $client = new nusoap_client($url, true);
        $proxy = $client->getProxy();
        $result = $proxy->GetToken(userfeeder, passwordfeeder);

        $token = $result;
        $table = 'nilai_transfer';

		$user = $this->session->userdata('sess_login');
		$mhs = $this->app_model->getdetail('tbl_mahasiswa','NIMHSMSMHS',$nim,'NIMHSMSMHS','asc')->row();
		$data['nilai'] = $this->db->get('tbl_index_nilai')->result();

		if ($mhs->SMAWLMSMHS < '20151') {
			$matkul =  $this->db->select('*')
								->from('tbl_transaksi_nilai_konversi kv')
								->join('tbl_matakuliah_copy mk','mk.kd_matakuliah = kv.KDKMKTRLNM','left')
								->where('kv.NIMHSTRLNM',$nim)
								->where('mk.kd_prodi',$user['userid'])
								->where('tahunakademik',$mhs->SMAWLMSMHS)
								->order_by('kv.id','desc')
								->get()->result();
		} else {
			$matkul =  $this->db->select('*')
								->from('tbl_transaksi_nilai_konversi kv')
								->join('tbl_matakuliah mk','mk.kd_matakuliah = kv.KDKMKTRLNM','left')
								->where('kv.NIMHSTRLNM',$nim)
								->where('mk.kd_prodi',$user['userid'])
								->order_by('kv.id','desc')
								->get()->result();
		}
		
		$sp2 = $proxy->GetRecord($token, 'mahasiswa_pt', "nipd ilike '%".$nim."%'");

		foreach ($matkul as $value) {
			$record['id_reg_pd'] = $sp2['result']['id_reg_pd'];

			$table1 = 'mata_kuliah';
            $filter = "kode_mk = '".str_replace('-', '', trim($value->KDKMKTRLNM))."'";
            $limit = 100; // jumlah data yang diambill
            $result2 = $proxy->GetRecordset($token, $table1, $filter, $order, $limit,$offset);
            //var_dump($result2);exit();
            foreach ($result2['result'] as $value1) {
                if ($value1['id_sms'] == $sp2['result']['id_sms']) {
                    $id_mk = $value1['id_mk'];
                }
            }
            
            $record['id_mk'] = $id_mk;
            
            $record['kode_mk_asal'] = str_replace('-', '', trim($value->kd_mk_asal));
            $record['nm_mk_asal'] = $value->nm_mk_asal;
            $record['sks_asal'] = $value->sks_mk_asal;
            $record['sks_diakui'] = $value->sks_matakuliah;
            $record['nilai_huruf_asal'] = $value->nilai_mk_asal;
            $record['nilai_huruf_diakui'] = $value->NLAKHTRLNM;
            $record['nilai_angka_diakui'] = $value->BOBOTTRLNM;

            $result1 = $proxy->InsertRecord($token, $table, json_encode($record));

            // response if sync fail
            if ($result1['result']['error_code'] != "0") {
            	$sync_result = $result1['result'];

            // response if sync success
            } else {
            	$customResult = [
	            	'status' => 'Berhasil',
	            	'id_ekuivalensi' => $result1['result']['id_ekuivalensi']
	            ];

	            $sync_result = $customResult;
            }

            echo "<pre>";
            var_dump($sync_result);
            echo "</pre>";
            echo "<hr>";
		}
	}

}

/* End of file Konversi.php */
/* Location: ./application/modules/sync_feed/controllers/Konversi.php */