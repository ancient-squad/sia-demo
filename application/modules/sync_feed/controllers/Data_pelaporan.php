<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Data_pelaporan extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		$data['page']='v_data_pelaporan';

        $this->load->view('template/template', $data);
	}

}

/* End of file data_pelaporan.php */
/* Location: ./application/controllers/data_pelaporan.php */