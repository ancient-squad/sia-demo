<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dosen_ajar extends MY_Controller {

	public function index()
	{
		$data['akademik'] = $this->db->where('kode >', 20152)->get('tbl_tahunakademik')->result();
		$data['prodi'] = $this->app_model->getdata('tbl_jurusan_prodi', 'kd_prodi', 'asc')->result();
		$data['page'] = "v_selectyear_dosen";
        $this->load->view('template/template', $data);
	}

	function loadData()
	{
		$yearn        = $this->input->post('tahun', TRUE);
		$sesi         = $this->session->userdata('sess_login');
		$data['sess'] = $this->session->userdata('sess_login')['id_user_group'] == '26' ? $this->input->post('prodi') : $sesi['userid'];
		$data['q']    = $this->app_model->dosen_ajar_feed($data['sess'],$yearn);
		$data['page'] = "feed_dsn_ajar";
		$this->load->view('template/template', $data);
	}

	function dosen()
	{
		$this->load->library("Nusoap_lib");
		// gunakan sandbox untuk coba-coba
        // $url = 'http://172.16.2.194:8082/ws/sandbox.php?wsdl';

        // gunakan live bila sudah yakin
        $url = 'http://172.16.2.194:8082/ws/live.php?wsdl'; 

        $client = new nusoap_client($url, true);
        $proxy 	= $client->getProxy();
        $result = $proxy->GetToken(userfeeder, passwordfeeder);
        $token 	= $result;
        $filter = "";
        $order 	= "";
        $limit 	= 3000; // jumlah data yang diambill
        $offset = 0; // offset dipakai untuk paging, contoh: bila $limit=20
        $data['hasil'] = $proxy->GetListPenugasanDosen($token, $filter, $order, $limit,$offset);
        $data['page'] = "dosen_feeder";
		$this->load->view('template/template', $data);
	}

}

/* End of file Dosen_ajar.php */
/* Location: ./application/modules/sync_feed/controllers/Dosen_ajar.php */