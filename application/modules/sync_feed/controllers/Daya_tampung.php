<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Daya_tampung extends MY_Controller {

	public function index()
	{
		error_reporting(0);
		$sesi = $this->session->userdata('sess_login');
		$sess = $sesi['userid'];
		$data['row'] = $this->db->query("select * from tbl_daya_tampung where kd_prodi = '".$sess."' and tahun_ajaran = '20162'")->row();	
		$data['page'] = 'feed_tampung';
		$this->load->view('template/template', $data);
	}

	function inputdata()
	{
		$sesi = $this->session->userdata('sess_login');
		$sess = $sesi['userid'];
		$data = array(
			'kd_prodi' => $sess,
			'tahun_ajaran' => '20162',
			'jml_target' => $this->input->post('jml_target'),
			'jml_daftar' => $this->input->post('jml_daftar'),
			'jml_lulus' => $this->input->post('jml_lulus'),
			'jml_daftar_ulang' => $this->input->post('jml_daftar_ulang'),
			'jml_undur' => $this->input->post('jml_undur')
			);
		$cek = $this->db->query("select * from tbl_daya_tampung where kd_prodi = '".$sess."' and tahun_ajaran = '20162'")->result();
		if ($cek == true) {
			$this->db->where('tahun_ajaran', '20162');
			$this->db->where('kd_prodi', $sess);
			$this->db->update('tbl_daya_tampung', $data);
		} else {
			$this->db->insert('tbl_daya_tampung', $data);
		}
		echo "<script>alert('Berhasil');
		document.location.href='".base_url()."sync_feed/daya_tampung';</script>";
	}

	function sync()
	{
		$sesi = $this->session->userdata('sess_login');
		$sess = $sesi['userid'];
		$this->load->library("Nusoap_lib");
        $url = 'http://172.16.0.58:8082/ws/sandbox.php?wsdl'; // gunakan sandbox untuk coba-coba
        //$url = 'http://172.16.0.58:8082/ws/live.php?wsdl'; // gunakan live bila sudah yakin
        $client = new nusoap_client($url, true);
        $proxy = $client->getProxy();
        $result = $proxy->GetToken(userfeeder, passwordfeeder);
        $token = $result;
        $table = 'daya_tampung';
        $cek = $this->db->query("select * from tbl_daya_tampung where kd_prodi = '".$sess."' and tahun_ajaran = '20162'")->row();
        $prodi = $this->db->query("select * from tbl_jurusan_prodi where kd_prodi = '".$sess."'")->row();
        
		if ($cek == 1) {
			$key = array('id_sms'=>$prodi->id_sms,'id_smt'=>'20162');
			$data = array('target_mhs_baru'=>$cek->jml_target,'calon_ikut_seleksi' => $cek->jml_daftar,'calon_lulus_seleksi' => $cek->jml_lulus,'daftar_sbg_mhs' => $cek->jml_daftar_ulang,'pst_undur_diri' => $cek->jml_undur,'tgl_awal_kul' => '2017-02-20');
			$records[] = array('key'=>$key, 'data'=>$data);

			foreach ($records as $record) {
				$result = $proxy->UpdateRecord($token, $table, json_encode($record));
				var_dump($result);exit();
			}
		} else {
			$record['id_sms'] = $prodi->id_sms; //unique id dari prodi bhayangkara
			$record['id_smt'] = '20162';
			$record['target_mhs_baru'] = $cek->jml_target; //target mahasiswa baru
			$record['calon_ikut_seleksi'] = $cek->jml_daftar; //peserta pmb
			$record['calon_lulus_seleksi'] = $cek->jml_lulus; //peserta lulus pmb
			$record['daftar_sbg_mhs'] = $cek->jml_daftar_ulang; //daftar ulang pmb
			$record['pst_undur_diri'] = $cek->jml_undur; //mahasiswa mengundurkan diri , pengurangan ke daftar ulang dan ditulis 0 saja
			$record['tgl_awal_kul'] = '2017-02-20'; //tgl awal kuliah
			$data['status'] = 1;
			$this->db->where('tahun_ajaran', '20162');
			$this->db->where('kd_prodi', $sess);
			$this->db->update('tbl_daya_tampung', $data);
			$result1 = $proxy->InsertRecord($token, $table, json_encode($record));
			var_dump($result1);exit();
		}
		
		//var_dump($record);exit();
		

	}

}

/* End of file Daya_tampung.php */
/* Location: ./application/modules/sync_feed/controllers/Daya_tampung.php */