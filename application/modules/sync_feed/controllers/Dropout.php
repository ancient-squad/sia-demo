<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dropout extends MY_Controller {

    private $userid, $usergroup;

    function __construct()
    {
        parent::__construct();
        if (!$this->session->userdata('sess_login')) {
            redirect('auth/logout','refresh');
        }
        
        $this->load->library('pddikti');
        $this->userid = $this->session->userdata('sess_login')['userid'];
        $this->usergroup = $this->session->userdata('sess_login')['id_user_group'];
    }

	public function index()
	{
		$grup = get_group($this->usergroup);
        
        if (in_array(10, $grup)) { //baa
            $data['fakultas'] = $this->app_model->getdata('tbl_fakultas', 'id_fakultas', 'ASC')->result();
            $data['akademik'] = $this->db->get('tbl_tahunakademik')->result();
            $data['page']     = 'sync_feed/v_do_sync_baa';

        } elseif (in_array(9, $grup)) { // fakultas
            $data['jurusan'] = $this->app_model->getdetail(
                                                    'tbl_jurusan_prodi',
                                                    'kd_fakultas',
                                                    $this->userid,
                                                    'prodi',
                                                    'asc')->result();
            $data['akademik'] = $this->db->get('tbl_tahunakademik')->result();
            $data['page']     = 'sync_feed/v_do_sync_faks';

        } elseif (in_array(8, $grup) || in_array(19, $grup)) { // prodi
            $data['jurusan']  = $this->db->where('kd_prodi',$this->userid)->get('tbl_jurusan_prodi')->result();
            $data['akademik'] = $this->db->get('tbl_tahunakademik')->result();
            $data['page']     = 'sync_feed/v_do_list_prodi';

        } elseif (in_array(1, $grup)) { // admin
            $data['fakultas'] = $this->app_model->getdata('tbl_fakultas', 'id_fakultas', 'ASC')->result();
            $data['akademik'] = $this->db->get('tbl_tahunakademik')->result();
            $data['page']     = 'sync_feed/v_do_sync_baa';
        }
		$this->load->view('template/template', $data);
	}

	public function vw()
	{
		$jurusan = $this->input->post('jurusan');
        $tahun = $this->input->post('tahun');

        $this->session->set_userdata('tahun',$tahun);
        $this->session->set_userdata('jurusan',$jurusan);

        redirect(base_url().'sync_feed/Dropout/sync_data_do');
	}

	function sync_data_do()
	{
        if ($this->session->userdata('jurusan')) {
            $data['rows']   = $this->db->query("SELECT * FROM tbl_dropout do 
                                                JOIN tbl_mahasiswa mhs ON mhs.`NIMHSMSMHS` = do.`npm_mahasiswa`
                                                WHERE mhs.`KDPSTMSMHS` = '".$this->session->userdata('jurusan')."' 
                                                AND do.`tahunajaran` = ".$this->session->userdata('tahun')."")->result();

            $data['page']='v_do_list';
            $this->load->view('template/template', $data);    
        }else{
            redirect(base_url().'sync_feed/Dropout','refresh');
        }
    }

    public function syncDropOut()
    {
        ini_set('memori_limit', '512MB');
        // prepare webservice
        $this->load->library("Nusoap_lib");
        //$url = 'http://172.16.0.58:8082/ws/sandbox.php?wsdl'; // gunakan sandbox untuk coba-coba
        $url        = 'http://feeder.'.$this->URL.'/ws/live.php?wsdl'; // gunakan live bila sudah yakin
        $client     = new nusoap_client($url, true);
        $proxy      = $client->getProxy();
        $username   = userfeeder;
        $password   = passwordfeeder;
        $result     = $proxy->GetToken($username, $password);
        $token      = $result;
        $table      = 'mahasiswa_pt';

        // this session below was grab from /data/dropout/saveSessBaa
        $sessdrop = $this->session->userdata('sessforselectbaa');

        $prodi = $sessdrop['jurusan'];
        $thajr = $sessdrop['tahunajaran'];

        // get dropuot student
        $this->db->select('tgl_skep,npm_mahasiswa,alasan');
        $this->db->from('tbl_dropout');
        $this->db->where('tahunajaran', $thajr);
        $this->db->where('audit_user', $prodi);
        $mhs = $this->db->get()->result();

        foreach ($mhs as $key) {

        	// deactivated user login
        	$this->db->where('userid', $key->npm_mahasiswa);
        	$this->db->update('tbl_user_login', ['status' => 0]);

            // dikeluarkan
            if ($key->alasan == '2') {
                $jenis_keluar = '3';

            // mengundurkan diri
            } elseif ($key->alasan == '1') {
                $jenis_keluar = '4';
            }

            $data = [
                'tgl_keluar' => $key->tgl_skep,
                'id_jns_keluar' => $jenis_keluar
            ];

            // get id reg pd from feeder
            $idstudent = $proxy->GetRecord($token, $table, "nipd ilike '".$key->npm_mahasiswa."%'");
            // var_dump($idstudent['result']['id_reg_pd']); exit();
            $idregstd  = ['id_reg_pd' => $idstudent['result']['id_reg_pd']];
            $records[] = ['key' => $idregstd, 'data' => $data];
        }

        // update status to dropout
        foreach ($records as $record) {
            $result = $proxy->UpdateRecord($token, $table, json_encode($record));
            echo "<pre>";
            print_r ($result['result']);
            echo "</pre><hr>";
        }
    }

    public function sync_do()
    {
        $this->load->model('sync_feed/status_model','status');
        $token       = $this->pddikti->get_token();
        $tahunajaran = $this->session->userdata('sessforselectbaa')['tahunajaran'];
        $prodi       = $this->session->userdata('sessforselectbaa')['jurusan'];
        $list_do     = $this->status->get_do($prodi, $tahunajaran);

        foreach ($list_do as $value) {
            $jenis_keluar = ($value->alasan == '2') ? '3' : '4';
            $id_reg_mhs = $this->_get_id_reg_mhs($value->npm_mahasiswa);

            $record = [
                'id_registrasi_mahasiswa' => $id_reg_mhs,
                'id_jenis_keluar'         => $jenis_keluar,
                'nomor_sk_yudisium'       => $value->skep,
                'tanggal_sk_yudisium'     => $value->tgl_skep,
                'tanggal_keluar'          => $value->tgl_skep,
                'id_periode_keluar'       => $value->tahunajaran
            ];

            $payload = [
                "act" => "InsertMahasiswaLulusDO",
                "token" => $token,
                "record" => $record
            ];

            $exec = $this->pddikti->runWS($payload);
            $result = json_decode($exec);
            $result->npm = $value->npm_mahasiswa;
            $result->status = $result->error_code == 0 ? "Sinkronisasi berhasil" : "Sinkronisasi gagal";

            if ($result->error_code == 0) {
                $this->db->update(
                    'tbl_dropout', 
                    ['sync_status' => 1], 
                    ['npm_mahasiswa' => $value->npm_mahasiswa, 'tahunajaran' => $value->tahunajaran]
                );
            }

            //dd($result); echo '<hr>';
        }
        echo "<script>alert('Sukses');</script>";
		redirect(base_url('sync_feed/Dropout'),'refresh');
    }

    public function update_sync_do()
    {
        $this->load->model('sync_feed/status_model','status');
        $token       = $this->pddikti->get_token();
        $tahunajaran = $this->session->userdata('sessforselectbaa')['tahunajaran'];
        $prodi       = $this->session->userdata('sessforselectbaa')['jurusan'];
        $list_do     = $this->status->get_do($prodi, $tahunajaran);

        foreach ($list_do as $value) {
            $jenis_keluar = ($value->alasan == '2') ? '3' : '4';
            $id_reg_mhs = $this->_get_id_reg_mhs($value->npm_mahasiswa);

            $record = [
                'id_jenis_keluar'         => $jenis_keluar,
                'nomor_sk_yudisium'       => $value->skep,
                'tanggal_sk_yudisium'     => $value->tgl_skep,
                'tanggal_keluar'          => $value->tgl_skep,
                'id_periode_keluar'       => $value->tahunajaran
            ];

            $payload = [
                "act" => "UpdateMahasiswaLulusDO",
                "token" => $token,
                "key" => [
                    "id_registrasi_mahasiswa" => $id_reg_mhs
                ],
                "record" => $record
            ];

            $exec = $this->pddikti->runWS($payload);
            $result = json_decode($exec);
            $result->npm = $value->npm_mahasiswa;
            $result->status = $result->error_code == 0 ? "Sinkronisasi berhasil" : "Sinkronisasi gagal";

            //dd($result); echo '<hr>';
        }
        echo "<script>alert('Sukses');</script>";
		redirect(base_url('sync_feed/Dropout'),'refresh');
    }

    private function _get_id_reg_mhs($npm)
    {
        $token = $this->pddikti->get_token();

        $payload = [
            'act'    => 'GetListMahasiswa',
            'token'  => $token,
            'filter' => "nim = '$npm'" 
        ];
        $exec = $this->pddikti->runWS($payload);
        $result = json_decode($exec)->data[0]->id_registrasi_mahasiswa;
        return $result;
    }

    public function kelola_keluar()
    {
        $data['page'] = "kelola_keluar_v";
        $this->load->view('template/template', $data);
    }

    public function get_out_list($angkatan)
    {
        $this->load->model('sync_feed/status_model', 'status');
        $data['outs'] = $this->status->get_out_student($this->userid, $angkatan);
        $this->load->view('partial/table_out_list_v.php', $data);
    }

    public function set_out()
    {
        extract(PopulateForm());

        foreach ($npm as $key => $value) {
            $data[] = [
                'npm' => $value,
                'sks' => $sks[$value],
                'prodi' => $this->userid,
                'tahunakademik' => $tahunakademik,
                'tanggal' => $tgl,
                'keterangan' => 'Dikeluarkan melalui fitur kelola keluar guna keperluan feeder',
                'tipe' => $status[$value]
            ];
        }
        $this->db->insert_batch('tbl_pengunduran', $data);
        echo '<script>alert("Berhasil menyimpan data!");history.go(-1);</script>';
    }
}

/* End of file Dropout.php */
/* Location: ./application/modules/sync_feed/controllers/Dropout.php */