<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Maba extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
        ini_set('memory_limit', '512M');
        $this->db2 = $this->load->database('regis',TRUE);
        $this->load->library('pddikti');
		if ($this->session->userdata('sess_login') == TRUE) {
			$akses = $this->role_model->cekakses(128)->result();
			if ($akses != TRUE) {
				redirect('home','refresh');
			}
		} else {
			redirect('auth','refresh');
		}
	}

	public function index()
	{
		$this->session->unset_userdata('prodi_maba_sess');
		$data['prodi'] = $this->db->get('tbl_jurusan_prodi')->result();
		$data['page'] = 'select_prodi_maba_v';
		$this->load->view('template/template', $data);
	}

	public function persist_session()
	{
		$this->session->set_userdata('prodi_maba_sess', $this->input->post('prodi'));
		redirect(base_url('sync_feed/maba/show_maba'));
	}

	function show_maba()
	{
		if (!$this->session->userdata('prodi_maba_sess')) {
			redirect(base_url('sync_feed/maba'));
		}
		$prodi = $this->session->userdata('prodi_maba_sess');

        $max_angkatan   = $this->db->query("SELECT max(TAHUNMSMHS) AS max FROM tbl_mahasiswa WHERE KDPSTMSMHS = '{$prodi}' 
                                            ORDER BY TAHUNMSMHS DESC LIMIT 1 ")->row()->max;

        $new_angkatan   = $this->db->query("SELECT NIMHSMSMHS FROM tbl_mahasiswa WHERE TAHUNMSMHS = '{$max_angkatan}'
                                            AND KDPSTMSMHS = '{$prodi}'")->result();

        $data = [];
        foreach ($new_angkatan as $key) {
            $data[] = $key->NIMHSMSMHS;
        }

        $this->db2->select('*');
        $this->db2->from('tbl_form_pmb');
        $this->db2->where('prodi', $prodi);
        $this->db2->where('npm_baru IS NOT NULL', NULL, FALSE);
        $this->db2->where('status > ', 2);
        $this->db2->where_in('npm_baru', $data);
        $this->db2->like('nomor_registrasi', substr($max_angkatan, 2,4), 'after');
        $this->db2->order_by('nomor_registrasi','asc');

        $data['look'] = $this->db2->get()->result();
        $data['page'] = "feed_maba";
		
		$this->load->view('template/template', $data);
	}

	function id_maba($id)
	{
		$data['query'] = $this->db->query("SELECT * from tbl_form_camaba where nomor_registrasi = '".$id."'")->row();
		$data['page'] = "feed_idmaba";
		$this->load->view('template/template', $data);
	}

	function inputdata()
	{
		if ($this->input->post('jk') == 'L') {
			$lamin = '1';
		} else {
			$lamin = '2';
		}

		$fak = $this->db->query("SELECT * from tbl_form_camaba where npm_baru = '".$this->input->post('npm')."'")->row();
		$jur = $fak->prodi;
		$haha= $this->db->query("SELECT * from tbl_jurusan_prodi where kd_prodi = '".$jur."'")->row();
		$faks= $haha->kd_fakultas;
		
		$data = array(
			'nomor_pokok' => $this->input->post('npm'),
			'nama' => $this->input->post('nama'),
			'jenis_kelamin' => $lamin,
			'alamat' => $this->input->post('alamat'),
			'rt' => $this->input->post('rt'),
			'rw' => $this->input->post('rw'),
			'kelurahan' => $this->input->post('kel'),
			'kecamatan' => $this->input->post('kec'),
			'provinsi' => $this->input->post('prov'),
			'handphone' => $this->input->post('hp'),
			'email' => $this->input->post('email'),
			'ayah' => $this->input->post('ayah'),
			'ibu' => $this->input->post('ibu'),
			'fakultas' => $faks,
			'jurusan' => $jur
			);
		//var_dump($data);exit();
		$this->db->insert('tbl_biodata', $data);
		echo "<script>alert('Berhasil');
		document.location.href='".base_url()."sync_feed/maba';</script>";
	}

	function sync_maba_feed()
    {
    	if (!$this->session->userdata('prodi_maba_sess')) {
			redirect(base_url('sync_feed/maba'));
		}
        $logged = $this->session->userdata('prodi_maba_sess');
        $id_prodi_feeder = $this->db->get_where('tbl_jurusan_prodi', ['kd_prodi' => $logged])->row()->id_sms;
        $max_angkatan = $this->db->query("SELECT max(TAHUNMSMHS) AS max from tbl_mahasiswa order by id_mhs desc limit 1")->row()->max;
        $cari   = $this->db->query("SELECT NIMHSMSMHS, SMAWLMSMHS, biaya_semester FROM tbl_mahasiswa WHERE KDPSTMSMHS = '{$logged}' AND TAHUNMSMHS = '{$max_angkatan}' ")->result();

        $token = $this->pddikti->get_token();

        foreach ($cari as $value) {

            $maba   = $this->db2->query("SELECT 
                                            nama,
                                            kelamin,
                                            nik,
                                            agama,
                                            nm_ayah,
                                            nm_ibu,
                                            tgl_lahir,
                                            tempat_lahir,
                                            tlp,
                                            alamat,
                                            npm_baru,
                                            nisn,
                                            kd_pos,
                                            jenis_pmb,
                                            tanggal_regis
                                        FROM tbl_form_pmb 
                                        WHERE npm_baru = '{$value->NIMHSMSMHS}' 
                                        AND prodi = '{$logged}' 
                                        AND (nik IS NOT NULL OR nik != '') 
                                        AND status > 2 
                                        AND status_feeder <> 1")->row();

            //1 = islam , 2 = kristen , 3 = katolik , 4 = hindu , 5 = budha , 6 = konghucu
        	switch ($maba->agama) {
        		case 'ISL':
        			$agm = '1';
        			break;
        		case 'HND':
        			$agm = '4';
        			break;
        		case 'KTL':
        			$agm = '3';
        			break;
        		case 'PRT':
        			$agm = '2';
        			break;
        		case 'BDH':
        			$agm = '5';
        			break;
        	}
        	
            $alm  = substr($maba->alamat, 0,60);
            $almt = strval($alm);

            $bio = [
            	'nama_mahasiswa' => $maba->nama,
            	'jenis_kelamin' => $maba->kelamin,
            	'tempat_lahir' => $maba->tempat_lahir,
            	'tanggal_lahir' => $maba->tgl_lahir,
            	'id_agama' => $agm,
            	'nik' => str_replace(' ', '', $maba->nik),
            	'nisn' => empty($maba->nisn) || is_null($maba->nisn) ? '0' : $maba->nisn,
            	'kewarganegaraan' => 'ID',
            	'kelurahan' => $almt,
            	'kode_pos' => $maba->kd_pos,
            	'id_wilayah' => $this->ID_WILAYAH,
            	'handphone' => $maba->tlp,
            	'penerima_kps' => 0,
            	'nama_ayah' => $maba->nm_ayah,
            	'nama_ibu_kandung' => $maba->nm_ibu,
            	'id_kebutuhan_khusus_mahasiswa' => 0,
            	'id_kebutuhan_khusus_ayah' => 0,
            	'id_kebutuhan_khusus_ibu' => 0
            ];

            $bio_mhs = [
            	'act' => 'InsertBiodataMahasiswa',
            	'token' => $token,
            	'record' => $bio
            ];
            $exec = $this->pddikti->runWS($bio_mhs);
            $result_bio = json_decode($exec);

            if ($result_bio->error_code !== '0') {
            	$obj = (object) [
            		'NPM' => $maba->npm_baru,
            		'status sinkronisasi'=> 'GAGAL',
            		'step' => 'sinkronisasi biodata',
            		'pesan' => $result_bio->error_desc
            	];
            	dd($obj);
            	echo "<hr>";
            	continue;
            }

            $mhs = [
            	'id_mahasiswa' => $result_bio->data->id_mahasiswa,
            	'nim' => $maba->npm_baru,
            	'id_jenis_daftar' => $maba->jenis_pmb === 'MB' ? 1 : 2,
            	'id_periode_masuk' => $value->SMAWLMSMHS,
            	'tangal_daftar' => $maba->tanggal_regis,
            	'id_perguruan_tinggi' => $this->ID_PT_FEEDER,
            	'id_prodi' => $id_prodi_feeder,
            	'biaya_masuk' => $value->biaya_semester
            ];

            $insert_mhs = [
            	'act' => 'InsertRiwayatPendidikanMahasiswa',
            	'token' => $token,
            	'record' => $mhs
            ];
            $exec2 = $this->pddikti->runWS($insert_mhs);
            $result_mhs = json_decode($exec2);
            if ($result_mhs->error_code !== '0') {
            	$obj2 = (object) [
            		'NPM' => $maba->npm_baru,
            		'status sinkronisasi' => 'GAGAL',
            		'step' => 'sinkronisasi mahasiswa',
            		'pesan' => $result_mhs->error_desc
            	];
            	dd($obj2);
            	echo "<hr>";
            	continue;
            } else {
            	$obj2 = (object) [
            		'NPM' => $maba->npm_baru,
            		'status sinkronisasi' => 'BERHASIL',
            		'id registrasi mahasiswa' => $result_mhs->data->id_registrasi_mahasiswa
            	];
            	dd($obj2);
            	echo "<hr>";
            }

            $this->db2->query("UPDATE tbl_form_pmb set status_feeder = 1 where npm_baru = '{$maba->npm_baru}'");
        }
    }
}

/* End of file Maba.php */
/* Location: ./application/modules/sync_feeder/controllers/Maba.php */