<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Status_mhs extends MY_Controller {

	private $userid, $user_group;

	public function __construct()
	{
		parent::__construct();
		error_reporting(0);
		ini_set('memory_limit', '2048M');
		$this->load->model('monitoring_model');
	    if ($this->session->userdata('sess_login') == TRUE) {
            $cekakses = $this->role_model->cekakses(123)->result();
            if ($cekakses != TRUE) {
                echo "<script>alert('Anda Tidak Berhak Mengakses !!');history.go(-1);</script>";
            }
        } else {
            redirect('auth','refresh');
        }
        $this->load->library('pddikti');
        $this->load->model('feeder_perkuliahan_model','feedkuliah');
        // $this->userid = $this->session->userdata('sess_login')['userid'];
        $this->user_group = $this->session->userdata('sess_login')['id_user_group'];
	}

	public function index(){
		$group = get_group($this->usergroup);
		if (in_array(8, $group) || in_array(19, $group)) {
			redirect(base_url('sync_feed/status_mhs/getsessionprodi'),'refresh');
		} else {
			redirect(base_url('sync_feed/status_mhs/getsessionnew'),'refresh');
		}
	}

	public function getsessionprodi()
	{
		$this->session->unset_userdata('tahunajaran_stmhs');
		$data['tahunajaran'] = $this->db->get('tbl_tahunakademik')->result();
		$data['page'] = "select_year_v";
		$this->load->view('template/template', $data);
	}

	public function getsessionnew()
	{
		$this->session->unset_userdata('tahunajaran_stmhs');
		$this->session->unset_userdata('prodi');
		$data['tahunajaran'] = $this->db->get('tbl_tahunakademik')->result();
		$data['fakultas']=$this->app_model->getdata('tbl_fakultas', 'id_fakultas', 'ASC')->result();
		$data['page'] = "select_year_v_new";
		$this->load->view('template/template', $data);
	}

	public function set_session()
	{
		$group = get_group($this->usergroup);
		if (in_array(8, $group) || in_array(19, $group)) {
			$this->session->set_userdata('prodi', $this->session->userdata('sess_login')['userid']);
		} else {
			$getprodi = explode("-",$this->input->post('prodi'));
			$this->session->set_userdata('prodi',$getprodi[0]);
		}
		$this->session->set_userdata('tahunajaran_stmhs', $this->input->post('tahunajaran'));
		redirect(base_url('sync_feed/status_mhs/load_data'));
	}

	public function load_data()
	{
		$academic_year = $this->session->userdata('tahunajaran_stmhs');
		$this->userid = $this->session->userdata('prodi');
		$stryear = studyStart($academic_year);

		$data['cuti']      = $this->feedkuliah->get_cuti_list($academic_year, $this->userid);
		$data['tahunajar'] = $academic_year;
		$data['prodiuser'] = $this->userid;
		$data['page']      = "feed_sts_mhs";
		$this->load->view('template/template', $data);
	}

	public function load_tab($tab)
	{
		$academic_year = $this->session->userdata('tahunajaran_stmhs');
		$stryear = studyStart($academic_year);
		$this->userid = $this->session->userdata('prodi');
		switch ($tab) {
			case 'nonactive':
				$data['non'] = $this->feedkuliah->get_non_active($this->userid, $academic_year, $stryear);
				$this->load->view('partial/non_active_student_tab', $data);
				break;

			case 'out':
				$data['keluar'] = $this->feedkuliah->get_keluar($this->userid);
				$this->load->view('partial/out_student_tab', $data);
				break;

			case 'dropout':
				$data['dropout'] = $this->feedkuliah->get_dropout($this->userid, $academic_year);
				$this->load->view('partial/dropout_student_tab', $data);
				break;
			
			default:
				$data['lulus'] = $this->feedkuliah->get_lulus($this->userid, substr($academic_year, 0,4));
				$this->load->view('partial/graduate_student_tab', $data);
				break;
		}
	}

	function update($id)
	{
		$this->db->query("UPDATE tbl_mahasiswa set STMHSMSMHS = 'K' where NIMHSMSMHS = '".$id."'");
		echo "<script>alert('Berhasil!');
		document.location.href='".base_url()."sync_feed/status_mhs';</script>";
	}

	public function sync_sts_soap()
	{
		$actyear = $this->app_model->getdetail('tbl_tahunakademik','status',1,'kode','asc')->row()->kode;

		$logged = $this->session->userdata('sess_login');

		$cut = $this->db->select('*')
						->from('tbl_status_mahasiswa a')
						->join('tbl_mahasiswa b','a.npm=b.NIMHSMSMHS')
						->where('a.status','C')
						->where('a.tahunajaran', $actyear)
						->where('b.KDPSTMSMHS',$logged['userid'])
						->where('a.validate',1)
						->get()->result();
        
        $this->load->library("Nusoap_lib");
        //$url = 'http://172.16.0.58:8082/ws/sandbox.php?wsdl'; // gunakan sandbox untuk coba-coba
        // $url = 'http://172.16.0.58:8082/ws/live.php?wsdl'; // gunakan live bila sudah yakin
        $url = 'http://feeder.'.$this->URL.'/ws/live.php?wsdl'; // gunakan live bila sudah yakin
        $client = new nusoap_client($url, true);
        $proxy = $client->getProxy();
        $result = $proxy->GetToken(userfeeder, passwordfeeder);

        $token = $result;

        foreach ($cut as $val) {
        	$ipk = $this->monitoring_model->getskstotalmhs($val->npm,$logged['userid']);
        	$table1 = 'kuliah_mahasiswa';
	        $sp = $proxy->GetRecord($token, 'mahasiswa_pt', "nipd ilike '%".$val->npm."%'");
	        //var_dump($result2);exit();
	        $record['id_smt'] 		= $actyear;
	        $record['id_reg_pd'] 	= $sp['result']['id_reg_pd'];
			$record['id_stat_mhs'] 	= 'C';
			$record['ips'] 			= '0';
			$record['sks_smt'] 		= '0';
			$record['ipk']			= $ipk['ipk']; //1 = islam , 2 = kristen , 3 = katolik , 4 = hindu , 5 = budha , 6 = konghucu
			$record['sks_total'] 	= $ipk['sks'];
			//$record['id_sp'] = $result2['result']['id_sp'];
			//var_dump($record);exit();
	        $result1 = $proxy->InsertRecord($token, $table1, json_encode($record));
	        var_dump($result1);echo "<hr>";	
        }
    }

    public function sync_cuti()
	{
		$academic_year = $this->session->userdata('tahunajaran_stmhs');
		$token     = $this->pddikti->get_token();
		$total_mhs = count($this->input->post('mhs'));

        for ($i = 1; $i <= $total_mhs; $i++) {
	        $mhs = $this->input->post('mhs['.$i.']');
	        $ipk = $this->input->post('ipk['.$i.']');
	        $ips = $this->input->post('ips['.$i.']');
	        $fee = $this->input->post('fee['.$i.']');
	        $sks = $this->input->post('sks['.$i.']');

	        $id_reg_pd = $this->_get_mahasiswa_feeder($token, $mhs);

			$record['id_semester']             = $academic_year;
			$record['id_registrasi_mahasiswa'] = json_decode($id_reg_pd)->data[0]->id_registrasi_mahasiswa;
			$record['id_status_mahasiswa']     = 'C';
			$record['ips']                     = 0;
			$record['ipk']                     = $ipk; 
			$record['sks_semester']            = 0;
			$record['total_sks']               = $sks;
			$record['biaya_kuliah_smt']        = (int)$fee;

			$payload = [
				'act'    => 'InsertPerkuliahanMahasiswa',
				'token'  => $token,
				'record' => $record
			];

	        $result = $this->pddikti->runWS($payload);
	        // dd(json_decode($result));
	        // echo "<hr>";	
        }
		echo "<script>alert('Sukses');</script>";
        redirect(base_url('sync_feed/Status_mhs'),'refresh');
    }

    private function _get_mahasiswa_feeder($token, $npm)
    {
        $payload2 = [
            'act'    => 'GetListMahasiswa',
            'token'  => $token,
            'filter' => "nim = '$npm'" 
        ];
        $mahasiswa = $this->pddikti->runWS($payload2);
        return $mahasiswa;
    }

    function sync_sts_non_soap()
	{
		$actyear 	= getactyear();
		$logged 	= $this->session->userdata('sess_login');
		$start 		= studyStart($actyear);

		$cut = $this->db->query("SELECT DISTINCT NIMHSMSMHS FROM tbl_mahasiswa WHERE NIMHSMSMHS NOT IN (
								    SELECT npm_mahasiswa FROM tbl_verifikasi_krs WHERE tahunajaran = '".$actyear."'
								) AND NIMHSMSMHS NOT IN (
								    SELECT npm FROM tbl_status_mahasiswa WHERE tahunajaran = '".$actyear."' 
								    AND validate = 1
								) 
								AND KDPSTMSMHS = '".$logged['userid']."' 
								AND BTSTUMSMHS >= '".$actyear."' 
								AND STMHSMSMHS NOT IN ('L','D','K','C','CA')
								AND SMAWLMSMHS >= '".$start."'
								AND TAHUNMSMHS != '2019'
								")->result();
        
        $this->load->library("Nusoap_lib");

        $url 	= 'http://172.16.0.58:8082/ws/live.php?wsdl';
        $client = new nusoap_client($url, true);
        $proxy 	= $client->getProxy();
        $result = $proxy->GetToken(userfeeder, passwordfeeder);
        $token 	= $result;
        $table1 = 'kuliah_mahasiswa';
       
    	foreach ($cut as $val) {
        	$ipk 	= $this->monitoring_model->getskstotalmhs($val->NIMHSMSMHS,$logged['userid']);
        	$sp 	= $proxy->GetRecord($token, 'mahasiswa_pt', "nipd ilike '%".trim($val->NIMHSMSMHS)."%'");

	        $record['id_smt'] 		= $actyear;
	        $record['id_reg_pd'] 	= $sp['result']['id_reg_pd'];
			$record['id_stat_mhs'] 	= 'N';
			$record['ips'] 			= 0;
			$record['sks_smt'] 		= 0;
			$record['ipk']			= $ipk['ipk']; 
			$record['sks_total'] 	= $ipk['sks'];
	        $result1 = $proxy->InsertRecord($token, $table1, json_encode($record));
	        var_dump($result1);echo "<hr>";	
        }
	}

	public function sync_non_active()
	{
		$academic_year = $this->session->userdata('tahunajaran_stmhs');
		
		$token         = $this->pddikti->get_token();
		$mhs           = $this->input->post('mhs');

		foreach ($mhs as $row => $value) {
			$mhs = $this->input->post('mhs['.$value.']');
	        $ipk = $this->input->post('ipk['.$value.']');
	        $ips = $this->input->post('ips['.$value.']');
	        $fee = $this->input->post('fee['.$value.']');
	        $sks = $this->input->post('sks['.$value.']');

	        $id_reg_pd = $this->_get_mahasiswa_feeder($token, $mhs);

			$record['id_semester']             = $academic_year;
			$record['id_registrasi_mahasiswa'] = json_decode($id_reg_pd)->data[0]->id_registrasi_mahasiswa;
			$record['id_status_mahasiswa']     = 'N';
			$record['ips']                     = 0;
			$record['ipk']                     = $ipk; 
			$record['sks_semester']            = 0;
			$record['total_sks']               = $sks;
			$record['biaya_kuliah_smt']        = (int)$fee;

			$payload = [
				'act'    => 'InsertPerkuliahanMahasiswa',
				'token'  => $token,
				'record' => $record
			];
	        $result = $this->pddikti->runWS($payload);
	        // dd(json_decode($result));
	        // echo "<hr>";	
		}
		echo "<script>alert('Sukses');</script>";
        redirect(base_url('sync_feed/Status_mhs'),'refresh');
	}

	public function sync_akm_dropout()
	{
		$academic_year = $this->session->userdata('tahunajaran_stmhs');
		$token         = $this->pddikti->get_token();
		$mhs           = $this->input->post('mhs');

		foreach ($mhs as $row => $value) {
			$mhs = $this->input->post('mhs['.$value.']');
	        $ipk = $this->input->post('ipk['.$value.']');
	        $ips = $this->input->post('ips['.$value.']');
	        $fee = $this->input->post('fee['.$value.']');
	        $sks = $this->input->post('sks['.$value.']');

	        $id_reg_pd = $this->_get_mahasiswa_feeder($token, $mhs);

			$record['id_semester']             = $academic_year;
			$record['id_registrasi_mahasiswa'] = json_decode($id_reg_pd)->data[0]->id_registrasi_mahasiswa;
			$record['id_status_mahasiswa']     = 'N';
			$record['ips']                     = 0;
			$record['ipk']                     = $ipk; 
			$record['sks_semester']            = 0;
			$record['total_sks']               = $sks;
			$record['biaya_kuliah_smt']        = (int)$fee;

			$payload = [
				'act'    => 'InsertPerkuliahanMahasiswa',
				'token'  => $token,
				'record' => $record
			];
	        $exec = $this->pddikti->runWS($payload);
	        $result = json_decode($exec);
	        $result->npm = $mhs;
	        $result->status = $result->error_code == 0 ? "Sinkronisasi berhasil" : "Sinkronisasi gagal";
	        // dd($result);
	        // echo "<hr>";	
		}
		echo "<script>alert('Sukses');</script>";
        redirect(base_url('sync_feed/Status_mhs'),'refresh');
	}

	public function set_to_cuti($npm)
	{
		$this->_input_cuti($npm);
		$this->_validate_cuti($npm);
		$nama = get_nm_mhs($npm);
		echo "<script>alert('Status ".$nama.' ('.$npm.')'." berhasil diubah menjadi cuti!');
		document.location.href='".base_url()."sync_feed/status_mhs/load_data';</script>";
	}

	private function _input_cuti($npm)
	{
		$get_pembimbing = $this->db->get_where('tbl_pa', ['npm_mahasiswa' => $npm])->row()->kd_dosen;
		$bio_mahasiswa  = $this->db->get_where('tbl_bio_mhs', ['npm' => $npm])->row();
		$academic_year  = $this->session->userdata('tahunajaran_stmhs');

		$obj = [
				'npm_mhs'             => $npm,
				'tgl_permohonan'      => date('Y-m-d'),
				'tahun_akademik_cuti' => $academic_year,
				'alasan'              => 'Dikeluarkan oleh operator prodi karna sudah 3x nonaktif melalui menu feeder status mahasiswa',
				'pembimbing_akademik' => $get_pembimbing,
				'alamat'              => $bio_mahasiswa->alamat,
				'hp'                  => $bio_mahasiswa->no_hp
			];

		$this->db->insert('tbl_permohonan_cuti', $obj);
		return;
	}

	private function _validate_cuti($npm)
	{
		$academic_year = $this->session->userdata('tahunajaran_stmhs');
		$semester_awal = $this->db->get_where('tbl_mahasiswa', ['NIMHSMSMHS' => $npm])->row()->SMAWLMSMHS;
		$get_semester  = $this->app_model->get_semester($semester_awal);

		$cuti = [
			"npm" => $npm,
			"status" => "C",
			"tahunajaran" => $academic_year,
			"validate" => 1,
			"tanggal" => date('Y-m-d'),
			"semester" => $get_semester,
			"index_biaya" => "1500000"
		];
		$this->db->insert('tbl_status_mahasiswa', $cuti);
		return;
	}

	function sync_sts_out()
	{
		$actyear = $this->app_model->getdetail('tbl_tahunakademik','status',1,'kode','asc')->row()->kode;

		$logged = $this->session->userdata('sess_login');

		$out = $this->db->query("SELECT * FROM tbl_mahasiswa WHERE STMHSMSMHS = 'K' AND KDPSTMSMHS = '".$logged['userid']."'")->result();
		//var_dump($cut);exit();
        
        $this->load->library("Nusoap_lib");
        //$url = 'http://172.16.0.58:8082/ws/sandbox.php?wsdl'; // gunakan sandbox untuk coba-coba
        $url = 'http://172.16.0.58:8082/ws/sandbox.php?wsdl'; // gunakan live bila sudah yakin
        $client = new nusoap_client($url, true);
        $proxy = $client->getProxy();
        $result = $proxy->GetToken(userfeeder, passwordfeeder);

        $token = $result;

        foreach ($out as $val) {
        	$table1 = 'kuliah_mahasiswa';
	        $sp = $proxy->GetRecord($token, 'mahasiswa_pt', "nipd ilike '%".trim($val->NIMHSMSMHS)."%'");
	        //var_dump($result2);exit();
	        $record['id_smt'] 		= $actyear;
	        $record['id_reg_pd'] 	= $sp['result']['id_reg_pd'];
			$record['id_stat_mhs'] 	= 'K';
			$record['ips'] 			= '0';
			$record['sks_smt'] 		= '0';
			$record['ipk']			= '0'; //1 = islam , 2 = kristen , 3 = katolik , 4 = hindu , 5 = budha , 6 = konghucu
			$record['sks_total'] 	= '0';
			//$record['id_sp'] = $result2['result']['id_sp'];
			//var_dump($record);exit();
	        $result1 = $proxy->InsertRecord($token, $table1, json_encode($record));
	        var_dump($result1);echo "<hr>";	
        }	
        	
	}

	public function sync_out()
	{
		extract(PopulateForm());
		$this->load->model('sync_feed/status_model','status');
		$this->userid = $this->session->userdata('prodi');
		$out_list = $this->status->get_out_list($this->userid, $angkatan);
		$token    = $this->pddikti->get_token();

		foreach ($out_list as $value) {
            $get_reg_mhs = $this->_get_mahasiswa_feeder($token, $value->npm);
			$id_reg_mhs = json_decode($get_reg_mhs)->data[0]->id_registrasi_mahasiswa;
			$id_keluar = $value->tipe == 'D' ? '3' : '4';

            $record = [
                'id_jenis_keluar'         => $id_keluar,
                'tanggal_keluar'          => $value->tanggal,
                'id_periode_keluar'       => $value->tahunakademik
            ];

            $payload = [
				"act"    => "UpdateMahasiswaLulusDO",
				"token"  => $token,
				"key"    => ["id_registrasi_mahasiswa" => $id_reg_mhs],
				"record" => $record
            ];
            $exec = $this->pddikti->runWS($payload);
            $result = json_decode($exec);
            $result->npm = $value->npm;
            $result->status = $result->error_code == 0 ? 'Sinkronisasi berhasil' : 'Sinkronisasi gagal';
            //dd($result); echo '<hr>';
        }
		echo "<script>alert('Sukses');</script>";
        redirect(base_url('sync_feed/Status_mhs'),'refresh');
	}

	public function update_sync($status)
	{
		$academic_year = $this->session->userdata('tahunajaran_stmhs');
		$this->userid = $this->session->userdata('prodi');
		$token         = $this->pddikti->get_token();
		$status_mhs    = ($status == 'cuti') ? "C" : "N";

		if ($status_mhs == "C") {
			$mhs = $this->feedkuliah->get_cuti_list($academic_year, $this->userid);
		} else {
			$stryear = studyStart($academic_year);
			$mhs = $this->feedkuliah->get_non_active($this->userid, $academic_year, $stryear);
		}

		foreach ($mhs as $row) {
			$mhs = $row->NIMHSMSMHS;
	        $ipk = $row->ipk;
	        $fee = ($status_mhs == "C") ? $row->index_biaya : 0;
	        $sks = $row->sks;

	        $id_reg_pd = $this->_get_mahasiswa_feeder($token, $row->NIMHSMSMHS);

			$record['id_status_mahasiswa']     = $status_mhs;
			$record['ips']                     = 0;
			$record['ipk']                     = $ipk; 
			$record['sks_semester']            = 0;
			$record['total_sks']               = $sks;
			$record['biaya_kuliah_smt']        = (int)$fee;

			$payload = [
				'act'    => 'UpdatePerkuliahanMahasiswa',
				'token'  => $token,
				'key' => [
					'id_registrasi_mahasiswa' => json_decode($id_reg_pd)->data[0]->id_registrasi_mahasiswa,
					'id_semester' => $academic_year
				],
				'record' => $record
			];

	        $result = $this->pddikti->runWS($payload);
	        // dd(json_decode($result));
	        // echo "<hr>";	
		}
		echo "<script>alert('Sukses');</script>";
        redirect(base_url('sync_feed/Status_mhs'),'refresh');
	}

	function get_jurusan($id)
	{
		$jrs = explode('-',$id);
        $jurusan = $this->app_model->getdetail('tbl_jurusan_prodi', 'kd_fakultas', $jrs[0], 'id_prodi', 'ASC')->result();
		$out = "<select class='form-control' name='jurusan' id='jurs'><option disabled>--Pilih Program Studi--</option>";
        foreach ($jurusan as $row) {
            $out .= "<option value='".$row->kd_prodi."-".$row->prodi."'>".$row->prodi. "</option>";
        }
        $out .= "</select>";
        echo $out;

	}

}

/* End of file Status_mhs.php */
/* Location: ./application/modules/sync_feed/controllers/Status_mhs.php */