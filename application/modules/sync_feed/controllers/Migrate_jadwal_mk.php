<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Migrate_jadwal_mk extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		if (!$this->session->userdata('sess_login')) {
			redirect('auth/logout');
		}
		$this->load->model('sync_feed/migrate_jadwal_model', 'migrate');
	}

	public function index()
	{
		$data['status'] = $this->migrate->load_all();
		$data['page'] = 'migrate_jadwalmk_v';
		$this->load->view('template/template', $data);
	}

	public function transfer($tahunakademik)
	{
		$has_trf = $this->migrate->has_transfer($tahunakademik);
		$list = '';
		foreach ($has_trf as $has) {
			$list[] = $has->kd_jadwal;
		}

		$jadwal = $this->migrate->get_jadwal($tahunakademik, $list);
		$data = [];
		foreach ($jadwal as $jdl) {
			$data[] = [
				'kd_jadwal' => $jdl->kd_jadwal,
				'id_matakuliah' => $jdl->id_matakuliah,
				'kd_matakuliah' => $jdl->kd_matakuliah,
				'kd_dosen' => $jdl->kd_dosen,
				'kd_ruangan' => $jdl->kd_ruangan,
				'hari' => $jdl->hari,
				'waktu_mulai' => $jdl->waktu_mulai,
				'waktu_selesai' => $jdl->waktu_selesai,
				'kelas' => $jdl->kelas,
				'waktu_kelas' => $jdl->waktu_kelas,
				'kd_tahunajaran' => $jdl->kd_tahunajaran,
				'audit_date' => $jdl->audit_date,
				'audit_user' => $jdl->audit_user,
				'status_feeder' => $jdl->status_feeder
			];
		}

		if (count($data) > 0) {
			$insert = $this->migrate->transfer($data);
			if ($insert > 0) {
				$http_status = 200;
				$status = 1;
				$description = 'success';
				$message = 'Proses transfer data berhasil!';
			} else {
				$http_status = 500;
				$status = 88;
				$description = 'failed';
				$message = 'Terjadi kesalahan saat proses pengolahan data. Harap ulangi kembali.';
			}
		} else {
			$http_status = 200;
			$status = 23;
			$description = 'success';
			$message = 'Tidak ada jadwal yang dapat di proses. Data jadwal kosong.';
		}
		
		$this->output
			->set_content_type('application/json')
			->set_status_header($http_status)
			->set_output(
            	json_encode([
                    'status' => $status,
                    'description' => $description,
                    'message' => $message
            	])
            );
	}

}

/* End of file Migrate_jadwal_mk.php */
/* Location: ./application/modules/sync_feed/controllers/Migrate_jadwal_mk.php */