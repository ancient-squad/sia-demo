<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Research_validation extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		if (!$this->session->userdata('sess_login')) {
			redirect(base_url('auth/logout'));
		}
	}

	public function index()
	{
		$data['lectures'] = $this->db->get('tbl_karyawan')->result();
		$data['page']     = 'rsc_validation';
		$this->load->view('template', $data);
	}

	public function detail($nid)
	{
		$data['nid']      = $nid;
		$data['research'] = $this->db->query("SELECT * FROM tbl_penelitian_dosen WHERE userid = '$nid'")->result();
		$data['devotion'] = $this->db->query("SELECT * FROM tbl_pengabdian_dosen WHERE userid = '$nid'")->result();
		$data['publication'] = $this->db->query("SELECT * FROM tbl_publikasi_dosen WHERE userid = '$nid'")->result();
		$data['page']     = 'detail_lecture_activity_v';
		$this->load->view('template', $data);
	}

}

/* End of file Research_validation.php */
/* Location: ./application/modules/sync_feed/controllers/Research_validation.php */