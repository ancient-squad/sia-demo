<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Kelas_model extends CI_Model {

	public $variable;

	public function __construct()
	{
		parent::__construct();
		
	}

	function getNilaiFeeder($val)
	{
		$this->db->distinct();
		$this->db->select('npm_mahasiswa');
		$this->db->from('tbl_krs_feeder');
		$this->db->where('kd_jadwal', $val);
		return $this->db->get();
	}

	function getNilaiTransaksi($nim, $kdmk, $year)
	{
		$this->db->select('NLAKHTRLNM,BOBOTTRLNM,nilai_akhir');
		$this->db->from('tbl_transaksi_nilai');
		$this->db->where('NIMHSTRLNM', $nim);
		$this->db->where('KDKMKTRLNM', $kdmk);
		$this->db->where('THSMSTRLNM', $year);
		return $this->db->get();
	}

}

/* End of file Kelas_model.php */
/* Location: ./application/models/Kelas_model.php */