<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Migrate_jadwal_model extends CI_Model {

	public function load_all()
	{
		$data = $this->db->query("SELECT ak.*, jd.`kd_tahunajaran` FROM tbl_tahunakademik ak 
								LEFT JOIN (SELECT kd_tahunajaran FROM tbl_jadwal_matkul_feeder GROUP BY kd_tahunajaran) jd 
								ON ak.`kode` = jd.`kd_tahunajaran`")->result();
		return $data;
	}

	public function get_jadwal($tahunakademik, $has_transfer)
	{
		$jadwal = $this->db->where('kd_tahunajaran', $tahunakademik)->where_not_in('kd_jadwal', $has_transfer)->get('tbl_jadwal_matkul')->result();
		return $jadwal;
	}

	public function has_transfer($tahunakademik)
	{
		$jadwal = $this->db->get_where('tbl_jadwal_matkul_feeder', ['kd_tahunajaran' => $tahunakademik])->result();
		return $jadwal;
	}

	public function transfer($data)
	{
		$this->db->insert_batch('tbl_jadwal_matkul_feeder', $data);
		return $this->db->affected_rows();
	}

}

/* End of file Migrate_jadwal_model.php */
/* Location: ./application/modules/sync_feed/models/Migrate_jadwal_model.php */