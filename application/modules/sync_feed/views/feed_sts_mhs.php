<div class="row">
  <div class="span12">                
    <div class="widget ">
      <div class="widget-header">
        <i class="icon-group"></i>
        <h3>Data Status Mahasiswa <?= get_thnajar($tahunajar); ?></h3>
      </div> 

      <div class="widget-content">
        <a href="<?= base_url('sync_feed/status_mhs') ?>" class="btn btn-warning pull-right">
          <i class="icon-chevron-left"></i> Kembali
        </a>
        <div class="tabbable">
          <ul class="nav nav-tabs">
            <li class="active"><a href="#banyak" data-toggle="tab">Review Cuti</a></li>
            <li><a href="#satuan" onclick="load_tab('nonactive')" data-toggle="tab">Review Non Aktif</a></li>
            <li><a href="#keluar" onclick="load_tab('out')" data-toggle="tab">Review Keluar</a></li>
            <li><a href="#dropout" onclick="load_tab('dropout')" data-toggle="tab">Review Dropout</a></li>
            <li><a href="#lulus" onclick="load_tab('graduate')" data-toggle="tab">Review Lulus</a></li>
          </ul>      

          <div class="tab-content">
            <div class="tab-pane active" id="banyak">
              <div class="">
                <form method="post" action="<?= base_url('sync_feed/status_mhs/sync_cuti') ?>">
                  <fieldset>
                    <button 
                      type="submit" 
                      target="_blank" 
                      style="margin-right: 5px" 
                      class="btn btn-primary pull-left">
                      <i class="icon icon-refresh"></i> Sinkronisasi Cuti
                    </button>
                    <a 
                      href="<?= base_url('feeder/status_mahasiswa/cuti') ?>" 
                      target="_blank" class="btn btn-default pull-left"
                      data-toggle="tooltip"
                      style="margin-right: 5px" 
                      title="lihat detail data di feeder">
                      <i class="icon-eye-open"></i> Data Cuti Feeder
                    </a>
                    <a 
                      data-toggle="tooltip"
                      title="Perhatian! Gunakan fitur ini hanya ketika Anda telah melakukan sinkronisasi"
                      href="<?= base_url('sync_feed/status_mhs/update_sync/cuti') ?>" 
                      target="_blank" 
                      class="btn btn-info pull-left">
                      <i class="icon icon-refresh"></i> Update Sinkronisasi Cuti
                    </a>
                    
                    <table id="example4" class="table table-bordered table-striped">
                      <thead>
                        <tr> 
                          <th width="40">No</th>
                          <th>NPM</th>
                          <th>NAMA</th>
                          <th>IPK</th>
                          <th>SKS Total</th>
                          <th>Biaya Semester</th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php $no = 1; foreach ($cuti as $row) { ?>
                        <tr>
                          <td><?= $no; ?></td>
                          <td>
                            <?= $row->NIMHSMSMHS; ?>
                            <input type="hidden" name="mhs[<?= $no ?>]" value="<?= $row->NIMHSMSMHS; ?>">
                          </td>
                          <td><?= $row->NMMHSMSMHS; ?></td>
                          <td>
                            <input type="hidden" name="ipk[<?= $no ?>]" value="<?= $row->ipk ?>">
                            <?= !is_null($row->ipk) ? $row->ipk : '-'; ?>
                          </td>
                          <td>
                            <input type="hidden" name="sks[<?= $no ?>]" value="<?= $row->sks ?>">
                            <?= !is_null($row->sks) ? $row->sks : '-'; ?>
                          </td>
                          <td>
                            <input type="hidden" name="fee[<?= $no ?>]" value="<?= $row->index_biaya ?>">
                            <?= 'Rp. '.format_harga_indo($row->index_biaya) ?>
                          </td>
                        </tr>
                        <?php $no++; } ?>
                      </tbody>
                    </table>
                  </fieldset>
                </form>
              </div>
            </div>

            <div class="tab-pane" id="satuan">
              <div id="nonactive">
                <center>
                  <img src="<?= base_url('assets/img/cat_load.gif') ?>" style="width: 30%">
                  <h4><i>Loading . . .</i></h4>
                </center>
              </div>
            </div>

            <div class="tab-pane" id="keluar">
              <div id="out">
                <center>
                  <img src="<?= base_url('assets/img/cat_load.gif') ?>" style="width: 30%">
                  <h4><i>Loading . . .</i></h4>
                </center>
              </div>
            </div>

            <div class="tab-pane" id="dropout">
              <div id="dropout">
                <center>
                  <img src="<?= base_url('assets/img/cat_load.gif') ?>" style="width: 30%">
                  <h4><i>Loading . . .</i></h4>
                </center>
              </div>
            </div>

            <div class="tab-pane" id="lulus">
              <div id="graduate">
                <center>
                  <img src="<?= base_url('assets/img/cat_load.gif') ?>" style="width: 30%">
                  <h4><i>Loading . . .</i></h4>
                </center>
              </div>
            </div>

          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<br><br>

<script type="text/javascript">
  function load_tab (tab) {
    $('#' + tab).load('<?= base_url('sync_feed/status_mhs/load_tab/') ?>' + tab);
  }
</script>

