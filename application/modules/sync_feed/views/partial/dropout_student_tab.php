<form action="<?= base_url('sync_feed/status_mhs/sync_akm_dropout') ?>" method="post">
  <fieldset>
  <button type="submit" target="_blank" class="btn btn-primary pull-left" style="margin-right: 5px">
    <i class="icon icon-refresh"></i> Sinkronisasi AKM Dropout
  </button>
  <!-- <a 
    data-toggle="tooltip"
    title="Perhatian! Gunakan fitur ini hanya ketika Anda telah melakukan sinkronisasi"
    href="<?= base_url('sync_feed/status_mhs/update_sync/dropout') ?>" 
    target="_blank" 
    class="btn btn-info pull-left">
    <i class="icon icon-refresh"></i> Update Sinkronisasi AKM Dropout
  </a> -->
  <table id="example1" class="table table-bordered table-striped">
      <thead>
        <tr> 
          <th width="40">No</th>
          <th>NPM</th>
          <th>NAMA</th>
          <th>IPK</th>
          <th>SKS Total</th>
          <th>Biaya Semester</th>
        </tr>
      </thead>
      <tbody>
        <?php $numb = 1; foreach ($dropout as $raw) { ?>
        <tr>
          <td><?= $numb; ?></td>
          <td>
            <?= $raw->NIMHSMSMHS; ?>
            <input type="hidden" name="mhs[<?= $raw->NIMHSMSMHS ?>]" value="<?= $raw->NIMHSMSMHS ?>">
          </td>
          <td><?= $raw->NMMHSMSMHS; ?></td>
          <td>
            <?= !is_null($raw->ipk) ? $raw->ipk : '-'; ?>
            <input type="hidden" name="ipk[<?= $raw->NIMHSMSMHS ?>]" value="<?= $raw->ipk ?>">
          </td>
          <td>
            <?= !is_null($raw->sks) ? $raw->sks : '-'; ?>
            <input type="hidden" name="sks[<?= $raw->NIMHSMSMHS ?>]" value="<?= $raw->sks ?>">
          </td>
          <td>
            <?= 'Rp. 0'; ?>
            <input type="hidden" name="fee[<?= $raw->NIMHSMSMHS ?>]" value="0">
          </td>
        </tr>
        <?php $numb++; } ?>
      </tbody>
    </table>
  </fieldset>
</form>

<script>
  $("#example1").dataTable({
    "bPaginate": false,
    "bLengthChange": false,
    "bFilter": true,
    "bSort": true,
    "bInfo": false,
    "bAutoWidth": true
  });

  $(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip();
  });
</script>