<div class="modal-header">
	<h4 class="modal-title">Ubah Data Aktifitas</h4>
</div>
<form action="<?=  base_url('sync_feed/aktifitas_mahasiswa/update') ?>" method="post" class="form-horizontal">
	<input type="hidden" name="id" value="<?= $aktifitas->id ?>">
	<div class="modal-body">
		<div class="control-group">
			<label for="jenis" class="control-label">Jenis <small style="color: red">*</small></label>
			<div class="controls">
				<select name="jenis" class="span3" required="">
					<?php foreach ($jenis_aktifitas as $val) : ?>
						<option value="<?= $val->kode ?>" <?= $aktifitas->jenis == $val->kode ? 'selected=""' : ''; ?>>
							<?= $val->nama ?>
						</option>
					<?php endforeach; ?>
				</select>
			</div>
		</div>
		<div class="control-group">
			<label for="judul-edit" class="control-label">Judul <small style="color: red">*</small></label>
			<div class="controls">
				<textarea name="judul" class="span3" id="judul-edit" required="">
					<?= $aktifitas->judul ?>
				</textarea>
			</div>
		</div>
		<div class="control-group">
			<label for="lokasi-edit" class="control-label">Lokasi</label>
			<div class="controls">
				<input type="text" class="span3" name="lokasi" id="lokasi-edit" value="<?= $aktifitas->lokasi ?>">
			</div>
		</div>
		<div class="control-group">
			<label for="no_sk-edit" class="control-label">Nomor SK Tugas</label>
			<div class="controls">
				<input type="text" class="span3" name="no_sk" id="no_sk-edit" value="<?= $aktifitas->no_sk_tugas ?>">
				<input type="hidden" name="hidden_no_sk" value="<?= $aktifitas->no_sk_tugas ?>">
			</div>
		</div>
		<div class="control-group">
			<label for="tgl_sk-edit" class="control-label">Tanggal SK Tugas</label>
			<div class="controls">
				<input type="text" class="span3" name="tgl_sk" id="tgl_sk-edit" value="<?= $aktifitas->tgl_sk_tugas ?>">
			</div>
		</div>
		<div class="control-group">
			<label for="" class="control-label">Jenis Anggota</label>
			<div class="controls">
				<div class="radio">
					<label>
						<input type="radio" name="anggota" value="0" <?= $aktifitas->jenis_anggota == 0 ? 'checked=""' : '' ?>>Personal
					</label>
					<label>
						<input type="radio" name="anggota" value="1" <?= $aktifitas->jenis_anggota == 1 ? 'checked=""' : '' ?>>Kelompok
					</label>
				</div>
			</div>
		</div>
		<div class="control-group">
			<label for="note-edit" class="control-label">Keterangan</label>
			<div class="controls">
				<textarea name="note" class="span3" id="note-edit"><?= $aktifitas->note ?></textarea>
			</div>
		</div>
	</div>
	<div class="modal-footer">
		<button type="submit" class="btn btn-primary">Update</button>
		<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	</div>
</form>

<script>
	$(document).ready(function() {
		$('#tgl_sk-edit').datepicker({
			dateFormat: "yy-mm-dd",
			yearRange: "2000:<?= date('Y') ?>",
			changeMonth: true,
			changeYear: true
		});
	})
</script>