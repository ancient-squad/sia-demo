<fieldset>
  <button 
    type="button" 
    data-toggle="modal"
    data-target="#addpembimbing" 
    style="margin-right: 5px" 
    class="btn btn-success pull-left">
    <i class="icon icon-plus"></i> Tambah Pembimbing Aktifitas
  </button>
  <a 
    href="<?= base_url('sync_feed/aktifitas_mahasiswa/sync_guide/'.$id_aktifitas) ?>" 
    style="margin-right: 5px" 
    class="btn btn-primary pull-left">
    <i class="icon icon-refresh"></i> Sinkronisasi Pembimbing
  </a>
  <!-- <a 
    data-toggle="tooltip"
    title="Perhatian! Gunakan fitur ini hanya ketika Anda telah melakukan sinkronisasi"
    href="<?= base_url('sync_feed/aktifitas_mahasiswa/') ?>" 
    target="_blank" 
    class="btn btn-primary pull-left">
    <i class="icon icon-refresh"></i> Update Sinkronisasi Pembimbing
  </a> -->
  
  <table id="examples" class="table table-bordered table-striped">
    <thead>
      <tr> 
        <th>No</th>
        <th>NIDN/NUPN</th>
        <th>NAMA</th>
        <th>Pembimbing ke</th>
        <th width="400">Kategori Kegiatan</th>
        <th>Status</th>
        <th width="40">Hapus</th>
      </tr>
    </thead>
    <tbody>
      <?php $no = 1; foreach ($pembimbing as $val) { ?>
      <tr>
        <td><?= $no; ?></td>
        <td><?= empty($val->nidn) || is_null($val->nidn) ? $val->nupn : $val->nidn; ?></td>
        <td><?= $val->nama; ?></td>
        <td><?= $val->no_urut_pembimbing ?></td>
        <td><?= $val->nama_kegiatan ?></td>
        <td>
          <?php if (!is_null($val->id_bimbing_feeder)) : ?>
            <span class="label label-success">Tersinkronisasi</span>
          <?php else : ?> 
            <span class="label label-default">Belum Tersinkronisasi</span>
          <?php endif; ?>
        </td>
        <td width="40">
          <a 
            onclick="return confirm('Anda yakin ingin menghapus pembimbing ini dari daftar?')"
            href="<?= base_url('sync_feed/aktifitas_mahasiswa/remove_guide/'.$val->id) ?>"
            class="btn btn-danger"
            data-toggle="tooltip"
            title="hapus pembimbing">
            <i class="icon-trash"></i>
          </a>
        </td>
      </tr>
      <?php $no++; } ?>
    </tbody>
  </table>
</fieldset>

<div id="addpembimbing" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Tambah Pembimbing</h4>
      </div>
      <form action="<?=  base_url('sync_feed/aktifitas_mahasiswa/save_pembimbing') ?>" method="post" class="form-horizontal">
        <div class="modal-body">
          <div class="control-group">
            <label for="nidn" class="control-label">NIDN</label>
            <div class="controls">
              <input type="text" class="span3" name="nidn" id="nidn" placeholder="ketik NIDN atau nama dosen" required="">
              <input type="hidden" name="hidden_nidn">
              <input type="hidden" name="nid">
              <input type="hidden" name="id_aktifitas" value="<?= $id_aktifitas ?>">
            </div>
          </div>
          <div class="control-group">
            <label for="urut" class="control-label">Pembimbing ke</label>
            <div class="controls">
              <input type="number" maxlength="1" class="span3" name="urut" id="urut" placeholder="nomor urut pembimbing" required="">
            </div>
          </div>
          <div class="control-group">
            <label for="nidn" class="control-label">Kategori Kegiatan</label>
            <div class="controls">
              <select name="kegiatan" class="span3" id="category" required="">
                <option value="" selected="" disabled=""></option>
                <?php foreach ($kegiatan as $rows) : ?>
                  <option value="<?= $rows->id_kategori_kegiatan.'@'.$rows->nama_kategori_kegiatan ?>">
                    <?= $rows->nama_kategori_kegiatan ?>
                  </option>
                <?php endforeach; ?>
              </select>
            </div>
          </div>
          <span id="showHere">
            
          </span>
        </div>
        <div class="modal-footer">
          <button type="submit" class="btn btn-primary">Save</button>
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </form>
    </div>
  </div>
</div>

<script>
  $('input[name^=nidn]').autocomplete({
    source: '<?= base_url('sync_feed/aktifitas_mahasiswa/search_dosen');?>',
    minLength: 3,
    select: function (evt, ui) {
        this.form.nidn.value = ui.item.value;
        this.form.hidden_nidn.value = ui.item.nidn;
        this.form.nid.value = ui.item.nid;
    }
  });

  $('#category').change(function() {
    var code = $(this).val();
    var splitCode = code.split('@');
    var value = splitCode[0];
    $.get('<?= base_url('sync_feed/aktifitas_mahasiswa/sub_category/') ?>' + value, function(res) {
      $('#showHere').html(res)
    })
  })

  $("#examples").dataTable({
    "bPaginate": false,
    "bLengthChange": false,
    "bFilter": true,
    "bSort": true,
    "bInfo": false,
    "bAutoWidth": true
  });
</script>