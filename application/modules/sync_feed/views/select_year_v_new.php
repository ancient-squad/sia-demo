<div class="row">

  <div class="span12">                

      <div class="widget ">

        <div class="widget-header">

          <i class="icon-user"></i>

          <h3>Data Aktifitas Kuliah Mahasiswa</h3>

      </div> <!-- /widget-header -->      

      <div class="widget-content">

        <div class="span11">

        <form method="post" class="form-horizontal" action="<?php echo base_url(); ?>sync_feed/status_mhs/set_session">

                        <fieldset>
                          <script>

                            $(document).ready(function(){

                              $('#faks').change(function(){

                                $.post('<?php echo base_url()?>sync_feed/status_mhs/get_jurusan/'+$(this).val(),{},function(get){

                                  $('#jurs').html(get);

                                });

                              });

                            });

                            </script>

                              <div class="control-group">

                                <label class="control-label">Fakultas</label>

                                <div class="controls">

                                  <select class="form-control span6" name="fakultas" id="faks">

                                    <option>--Pilih Fakultas--</option>

                                    <?php $fakultas =$this->app_model->getdata('tbl_fakultas', 'id_fakultas', 'ASC')->result(); foreach ($fakultas as $row) { ?>

                                    <option value="<?php echo $row->kd_fakultas.'-'.$row->fakultas;?>"><?php echo $row->fakultas;?></option>

                                    <?php } ?>

                                  </select>

                                </div>

                              </div>

                              <div class="control-group">

                                <label class="control-label">Program Studi</label>

                                <div class="controls">

                                  <select class="form-control span6" name="prodi" id="jurs">

                                    <option>--Pilih Program Studi--</option>

                                  </select>

                                </div>

                              </div>
                              <div class="control-group">
                                <label class="control-label">Tahun Ajaran</label>
                                <div class="controls">
                                <select class="form-control span6" name="tahunajaran">
                                    <option disabled selected>--Pilih Tahun Akademik--</option>
                                    <?php foreach ($tahunajaran as $val) { ?>
                                    <option value="<?= $val->kode; ?>"><?= $val->tahun_akademik; ?></option>
                                    <?php } ?>
                                </select>
                                </div>
                            </div>

                            <br/>

                              

                            <div class="form-actions">

                                <input type="submit" class="btn btn-large btn-success" value="Cari"/> 

                            </div> <!-- /form-actions -->

                        </fieldset>

                    </form>

        </div>

      </div>

    </div>

  </div>

</div>

