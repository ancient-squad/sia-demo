<script type="text/javascript">
	$(document).ready(function(){
		$('#semester').change(function(){
			$.post('<?= base_url();?>sync_feed/kelas/get_detail_matakuliah_by_semester_feeder/'+$(this).val(),{},function(get){
				$('#ala_ala').html(get);
			});
		});

		$('#ala_ala').change(function() {
			var get_kode_mk = $(this + 'option:selected').text();
			var kode_mk = get_kode_mk.split(' - ');
			$('#kode_mk').val(kode_mk[0].substring(1));
		});
	});
</script>

<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
    <h4 class="modal-title">FORM EDIT DATA</h4>
</div>

<form class ='form-horizontal' action="<?= base_url();?>sync_feed/kelas/update_data_matakuliah" method="post">
    <div class="modal-body">    
        <script type="text/javascript">
        	
        </script>

        <input type='hidden' class="form-control" name="id_jadwal"  value="<?= $rows->id_jadwal; ?>">
        <input type='hidden' class="form-control" name="jurusan" value="<?= $kode_prodi; ?>" readonly >

        <div class="control-group">
          	<label class="control-label">Semester</label>
          	<div class="controls">
	            <select id="semester" class="form-control" name="semester" >
	            	<option value="<?= $rows->semester_matakuliah; ?>">
	              		<?= $rows->semester_matakuliah; ?>
	              	</option>
		            <option>--Pilih Semester--</option>
		            <?php for ($i=1; $i < 9; $i++) { ?>
		            	<option value="<?= $i ?>"><?= $i ?></option>
		            <?php } ?>
	            </select>
          	</div>
        </div>

       	<div class="control-group">
			<label class="control-label">Kelompok Kelas</label>
			<div class="controls">
				<input type="radio" name="st_kelas" value="PG" <?= ($rows->waktu_kelas == 'PG') ? 'checked=""' : ''; ?>> PAGI &nbsp;&nbsp; 
				<input type="radio" name="st_kelas" value="SR" <?= ($rows->waktu_kelas == 'SR') ? 'checked=""' : ''; ?>> SORE &nbsp;&nbsp;
				<input type="radio" name="st_kelas" value="PK" <?= ($rows->waktu_kelas == 'PK') ? 'checked=""' : ''; ?>> P2K  &nbsp;&nbsp;
			</div>
		</div>

    	<div class="control-group">											
			<label class="control-label" for="kelas">Nama Kelas</label>
			<div class="controls">
				<input type="text" class="form-control" placeholder="Input Kelas" id="kelas" name="kelas" value="<?= $rows->kelas ?>" >
			</div> <!-- /controls -->				
		</div> <!-- /control-group -->	                                    

		<div class="control-group">											
			<label class="control-label" for="firstname">Nama Matakuliah</label>
			<div class="controls">
				<select class="form-control" name="id_matakuliah" id="ala_ala" >
					<option value="<?= $rows->id_matakuliah ?>"><?= $rows->nama_matakuliah; ?> </option>
					<option>--Pilih MataKuliah--</option>
				</select>
			</div> <!-- /controls -->				
		</div> <!-- /control-group -->    

		<div class="control-group">										
			<label class="control-label" for="kode_mk">Kode MK</label>
			<div class="controls">
				<input type="text" class="form-control" name="kd_matakuliah" placeholder="Kode Matakuliah" id="kode_mk" value="<?= $rows->kd_matakuliah; ?>" readonly>
			</div> <!-- /controls -->				
		</div> <!-- /control-group -->
    </div> 

    <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <input type="submit" class="btn btn-primary" value="Save changes"/>
    </div>
</form>