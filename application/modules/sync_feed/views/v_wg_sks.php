<link href="https://gitcdn.github.io/bootstrap-toggle/2.2.0/css/bootstrap-toggle.min.css" rel="stylesheet">
<script src="https://gitcdn.github.io/bootstrap-toggle/2.2.0/js/bootstrap-toggle.min.js"></script>

<div class="row">
	<div class="span12">      		  		
  		<div class="widget ">
  			<div class="widget-header">
  				<i class="icon-user"></i>
  				<h3>Beban Mengajar</h3>
			</div> <!-- /widget-header -->
			
			<div class="widget-content">
				<div class="span11">
				<?php foreach ($quer as $key) { ?>
					<h3><?php echo $key->tahun_akademik; ?> </h3>
					
					<table id="example" class="table table-bordered table-striped">
	                	<thead>
	                        <tr> 
	                        	<th>No</th>
	                        	<th>KODE MATAKULIAH</th>
	                        	<th>MATAKULIAH</th>
	                        	<th>SKS</th>
	                        	<th>KELAS</th>
	                        	<th>TAHUN AJARAN</th>
	                        </tr>
	                    </thead>
	                    <tbody>
                            <?php 
                            $dos = $this->session->userdata('sess_login');
                            $getData = $this->app_model->get_beban_bio($dos['userid'],$key->kode)->result();
                            $no = 1; foreach ($getData as $row) { ?>
	                        <tr>
	                        	<td><?php echo number_format($no); ?></td>
	                        	<td><?php echo $row->kd_matakuliah; ?></td>
                                <td><?php echo $row->nama_matakuliah; ?></td>
                                <td><?php echo $row->sks_matakuliah; ?></td>
                                <td><?php echo $row->kelas; ?></td>
                                <td><?php echo $row->kd_tahunajaran; ?></td>
	                        </tr>
	                        <?php $no++; } ?>
	                    </tbody>
	               	</table>
	               	<hr>
				<?php } ?>
				</div>
			</div>
		</div>
	</div>
</div>