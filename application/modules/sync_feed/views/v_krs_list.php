<div class="row">
	<div class="span12">      		  		
  		<div class="widget ">
  			<div class="widget-header">
  				<i class="icon-file"></i>
  				<h3>Data Kartu Hasil Studi</h3>
			</div> <!-- /widget-header -->
			
			<div class="widget-content">
				<div class="span11">
					<a href="<?= base_url('sync_feed/krs') ?>" class="btn btn-warning pull-left" style="margin-right: 5px">
						<i class="icon-chevron-left"></i> Kembali
					</a>
					<!-- make switch for prodi who will sync -->
					<?php 
						$logged = $this->session->userdata('sess_login');
						$user = $logged['id_user_group'] == '26' ? $this->session->userdata('session_akm')['prodi'] : $logged['userid'];
						$status = $this->app_model->getdetail(
							'panel_switch',
							'kd_prodi',
							$user,
							'kd_prodi','asc'
						)->row()->feedkhs;

						if ($status == 1) { ?>

							<a class="btn btn-warning pull-right" href="<?= base_url('sync_feed/krs/change_mode/0') ?>">
								Ubah ke mode edit
							</a>

							<form 
								action="<?= base_url('sync_feed/krs/sync_akm'); ?>" 
								method="post" 
								accept-charset="utf-8" 
								target="_blank">
								<button type="submit" class="btn btn-primary">
									<i class="icon-refresh"></i> Sinkronisasi Aktifitas Kuliah
								</button>

						<?php } else { ?>
							<a class="btn btn-warning pull-right" href="<?= base_url('sync_feed/krs/change_mode/1') ?>">
								Ubah ke mode Sinkronisasi
							</a>

							<form action="<?= base_url('sync_feed/krs/in'); ?>" method="post" accept-charset="utf-8">
								<button type="submit" class="btn btn-success"><i class="icon-save"></i> Simpan Data</button>

						<?php } ?>
						<!-- end of switch panel -->

						<a href="<?= base_url('feeder/akm') ?>" class="btn btn-default">
							<i class="icon-facebook"></i> Lihat Data Feeder
						</a>

						<hr>
						<table id="example1" class="table table-bordered table-striped">
		                	<thead>
		                        <tr> 
		                        	<th>No</th>
		                        	<th>NIM</th>
		                        	<th>Nama Mahasiswa</th>
		                        	<th>Kelas</th>
		                        	<th>Tahun Masuk</th>
		                        	<th>SKS <?= $lastyear; ?></th>
		                        	<th>IPS <?= $lastyear; ?></th>
		                        	<th>SKS Total</th>
		                        	<th>IPK <?= $lastyear; ?></th>
		                        	<th>Biaya Semester</th>
		                        </tr>
		                    </thead>
		                    <tbody>
		                    	<?php $no=1; foreach ($getData as $value) { ?>
		                        <tr>
		                        	<td><?= number_format($no); ?></td>
		                        	<td><?= $value->NIMHSMSMHS; ?></td>
		                        	<td>
		                        		<?= $value->NMMHSMSMHS; ?>
		                        		<input  
											type="hidden" 
											name="nama[<?= $value->NIMHSMSMHS;?>]" 
											value="<?= $value->NMMHSMSMHS; ?>" />
		                        	</td>
		                        	<td><?= classType($value->kategori_kelas) ?></td>
	                                <td><?= $value->TAHUNMSMHS; ?></td>

	                                <td>
	                                	<a 
	                                		target="_blank" 
	                                		href="<?= base_url('sync_feed/krs/view_krs_dtl/'.$value->kd_krs); ?>">
	                                		<?= number_format($value->SKSEMTRAKM); ?>
	                                	</a>
	                                </td>
	                                
									<td><?= number_format($value->NLIPSTRAKM, 2); ?></td>
									<td>
										<input 
											style="width:70px;" 
											type="text" 
											name="sks[<?= $value->NIMHSMSMHS;?>]" 
											value="<?= (int)$value->SKSTTTRAKM; ?>" 
											required/>
									</td>
									<td>
										<input 
											style="width:70px;" 
											type="text" 
											name="ipk[<?= $value->NIMHSMSMHS;?>]" 
											value="<?= number_format($value->NLIPKTRAKM, 2); ?>" 
											required/>
									</td>
									<td>
										<input 
											style="width:90px;" 
											type="text" 
											value="<?= 'Rp. '.format_harga_indo($value->biaya_semester); ?>" 
											readonly/>
										<input 
											type="hidden" 
											name="fee[<?= $value->NIMHSMSMHS;?>]" 
											value="<?= $value->biaya_semester; ?>" 
											readonly/>
									</td>

									<input 
										type="hidden" 
										name="ips[<?= $value->NIMHSMSMHS;?>]" 
										value="<?= number_format($value->NLIPSTRAKM, 2); ?>"/>
	                                <input 
	                                	type="hidden" 
                                		name="mhs[<?= $value->NIMHSMSMHS;?>]" 
                                		value="<?= $value->NIMHSMSMHS; ?>"/>
	                                <input 
	                                	type="hidden" 
                                		name="smt[<?= $value->NIMHSMSMHS;?>]" 
                                		value="<?= (int)$value->SKSEMTRAKM; ?>"/>
									</tr>
		                        <?php $no++; } ?>
		                    </tbody>
		               	</table>
	               	</form>
				</div>
			</div>
		</div>
	</div>
</div>
<br>