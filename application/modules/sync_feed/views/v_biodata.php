<?php //var_dump($kelurahan);die(); ?>
<script type="text/javascript">
    function image_upl(id) {
        $("#upload").load('<?= base_url()?>sync_feed/biodata/upload/'+id);
    }
</script>
<style>
.kontener {
  position: relative;
  width: 100%;
  max-width: 100%;
}
/* Make the image to responsive */
.imagex {
  width: 100%;
  height: auto;
}

/* The overlay effect (full height and width) - lays on top of the container and over the image */
.overle {
  position: absolute;
  top: 0;
  bottom: 0;
  left: 0;
  right: 0;
  height: 100%;
  width: 100%;
  opacity: 0;
  transition: .3s ease;
  background-color: grey;
}

/* When you mouse over the container, fade in the overlay icon*/
.kontener:hover .overle {
  opacity: 0.5;
}

/* The icon inside the overlay is positioned in the middle vertically and horizontally */
.iconx {
  color: white;
  font-size: 80px;
  position: absolute;
  top: 50%;
  left: 50%;
  cursor: pointer;
  transform: translate(-50%, -50%);
  -ms-transform: translate(-50%, -50%);
  text-align: center;
}

/* When you move the mouse over the icon, change color */
.icon-camera:hover {
  color: #aaa;
}
</style>
<div class="container">
    <div class="row">
      	<div class="span8">      		
      		<div class="widget ">
      			<div class="widget-header">
      				<i class="icon-user"></i>
      				<h3>BIODATA DIRI</h3>
  				</div> <!-- /widget-header -->
				
				<div class="widget-content">
					
					<form 
						id="edit-profile" 
						class="form-horizontal" 
						method="post" 
						action="<?= base_url();?>sync_feed/biodata/updatedata">

						<fieldset>

							<div class="control-group">											
								<label class="control-label">NIM Mahasiswa*</label>
								<div class="controls">
									<input 
										type="text" 
										class="span6" 
										name="npm" 
										placeholder="Nomor Induk Mahasiswa" 
										value="<?= $mhs->NIMHSMSMHS; ?>" 
										required disabled>
								</div>			
							</div>

							<div class="control-group">											
								<label class="control-label">Nama Mahasiswa*</label>
								<div class="controls">
									<input 
										type="text" 
										class="span6" 
										name="nama" 
										placeholder="Nama Mahasiswa" 
										value="<?= $mhs->NMMHSMSMHS; ?>" 
										required disabled>
									<p class="help-block">Perubahan Nama Dapat Diajukan Melalui Biro Administrasi Akademik</p>
								</div>
							</div>

							<div class="control-group">											
								<label class="control-label">Jenis Kelamin*</label>
								<div class="controls">

									<?php if ($mhs->KDJEKMSMHS == 'L') {
										$jk = 'LAKI-LAKI';
									} elseif ($mhs->KDJEKMSMHS == 'P') {
										$jk = 'PEREMPUAN';
									} else {
									 	$jk = '-';
									} ?>

									<select class="form-control span6" name="jk">
										<option selected="" disabled="">-- Jenis Kelamin --</option>
										<option value="L" <?= $mhs->KDJEKMSMHS == 'L' ? 'selected=""' : NULL ?> >Laki-Laki</option>
										<option value="P" <?= $mhs->KDJEKMSMHS == 'P' ? 'selected=""' : NULL ?> >Perempuan</option>
									</select>
								</div>
							</div> 

							<div class="control-group">											
								<label class="control-label">Nama Ibu Mahasiswa*</label>
								<div class="controls">
									<input 
										type="text"
										class="span6" 
										name="ibu" 
										placeholder="Nama Ibu Mahasiswa" 
										value="<?= $mhs->NMIBUMSMHS; ?>" 
										required>
									<p class="help-block">Perubahan Nama Ibu Dapat Diajukan Melalui Biro Administrasi Akademik</p>
								</div>
							</div>

							<div class="control-group">											
								<label class="control-label">Nomor KTP*</label>
								<div class="controls">
									<input 
										type="text" 
										class="span6" 
										name="nik" 
										placeholder="Nomor Kartu Tanda Penduduk" 
										value="<?= $mhs->NIKMSMHS; ?>" 
										minlength=16 >
								</div>			
							</div>

							<div class="control-group">											
								<label class="control-label">NISN*</label>
								<div class="controls">
									<input 
										type="text" 
										class="span6" 
										name="nisn" 
										placeholder="Nomor Induk Siswa Nasional" 
										value="<?= $mhs->NISNMSMHS; ?>" >
									<p class="help-block">Nomor Induk Siswa Nasional dapat anda lihat pada ijazah terahir anda.</p>
								</div>
							</div>

							<div class="control-group">											
								<label class="control-label">Tempat Lahir*</label>
								<div class="controls">
									<input 
										type="text" 
										class="span6" 
										name="tempat" 
										placeholder="Tempat Lahir Sesuai KTP" 
										value="<?= $mhs->TPLHRMSMHS; ?>" 
										required 
										disabled/>
									<p class="help-block">Perubahan Tempat Lahir Dapat Diajukan Melalui Biro Administrasi Akademik</p>
								</div>
							</div>

							<script>
				                $(function() {
				                    $( "#tgl" ).datepicker({
				                        changeMonth: true,
				                        changeYear: true,
				                        yearRange: "-50:+0",
				                    });
				                });
				            </script>

							<div class="control-group">											
								<label class="control-label">Tanggal Lahir*</label>
								<div class="controls">
									<input 
										type="text" 
										class="span6" 
										id="tgl" 
										name="tgl" 
										placeholder="Tanggal Lahir Sesuai KTP" 
										value="<?= $mhs->TGLHRMSMHS; ?>" 
										required 
										disabled/>
									<p class="help-block">Perubahan Tanggal Lahir Dapat Diajukan Melalui Biro Administrasi Akademik</p>
								</div>
							</div>

							<hr>

							<script>
		                     $(document).ready(function(){
		                       $('#prov').change(function(){
		                         $.post('<?= base_url()?>sync_feed/biodata/get_kota/'+$(this).val(),{},function(get){
		                           $('#kota').html(get);
		                         });
		                       });
		                     });
		                    </script>

		                    <script>
		                     $(document).ready(function(){
		                       $('#kota').change(function(){
		                         $.post('<?= base_url()?>sync_feed/biodata/get_kec/'+$(this).val(),{},function(get){
		                           $('#kec').html(get);
		                         });
		                       });
		                     });
		                    </script>
							
							<script>
		                     $(document).ready(function(){
		                       $('#kec').change(function(){
		                         $.post('<?= base_url()?>sync_feed/biodata/get_kel/'+$(this).val(),{},function(get){
		                           $('#kel').html(get);
		                         });
		                       });
		                     });
		                    </script>

		                    <div class="control-group">											
								<label class="control-label">Provinsi</label>
								<div class="controls">
									<select name="prov" id="prov" class="span6">
										<?php if ($mhs2->provinsi) : ?>
											<option value="<?= $mhs2->provinsi ?>">
												<?= provinceName($mhs2->provinsi) ?>
											</option>
										<?php endif; ?>

										<option value="0000">-- pilih provinsi --<option>
										<?php foreach ($provinces as $key) { ?>                
											<option value="<?= $key->id; ?>"><?= $key->name; ?></option>
										<?php } ?>
									</select>
								</div>
							</div>

							<div class="control-group">
								<label class="control-label">Kota/Kabupaten*</label>								
								<div class="controls">
									<select name="kota" id="kota" class="span6">
										<?php if ($mhs2->kota) : ?>
											<option value="<?= $mhs2->kota ?>"><?= kokabName($mhs2->kota) ?></option>
										<?php endif; ?>
										<option value="0000">-- pilih Kota/Kabupaten --<option>
									</select>
								</div>
							</div>

							<div class="control-group">
								<label class="control-label">Kecamatan*</label>							
								<div class="controls">
									<select name="kecamatan" id="kec" class="span6">
										<?php if ($mhs2->kecamatan): ?>
											<option value="<?= $mhs2->kecamatan ?>">
												<?= kecamatanName($mhs2->kecamatan) ?>
											</option>
										<?php endif ?>
										<option value="0000">-- pilih Kecamatan --<option>
									</select>
								</div>
							</div>

							<div class="control-group">
								<label class="control-label">Kelurahan*</label>
								<div class="controls">
									<select name="kelurahan" id="kel" class="span6">
										<?php if ($mhs2->kelurahan): ?>
											<option value="<?= $mhs2->kelurahan ?>">
												<?= kelurahanName($mhs2->kelurahan) ?>
											</option>
										<?php endif ?>
										<option value="0000">-- pilih Kelurahan --<option>
									</select>
								</div>
							</div>

							<div class="control-group">											
								<label class="control-label">Alamat*</label>
								<div class="controls">
									<textarea type="text" class="span6" name="alamat" required><?= $mhs2->alamat;?>
									</textarea>
								</div>
							</div>

							<hr>

							<div class="control-group">											
								<label class="control-label">Telepon*</label>
								<div class="controls">
									<input type="text" class="span6" name="tlpn" placeholder="Masukan Nomor Telepon" value="<?= $mhs2->no_hp;?>"  required>
								</div>
							</div>

							<div class="control-group">											
								<label class="control-label">Email*</label>
								<div class="controls">
									<input type="email" class="span6" name="email" placeholder="Masukan Email Mahasiswa" value="<?= $mhs2->email;?>"  required>
									<p class="help-block">Wajib di isi untuk pemulihan password akun Sistem Informasi Akademik.</p>
								</div>
							</div> 						
							
							<div class="form-actions">
								<button type="submit" class="btn btn-primary">Save</button> 
								<button class="btn">Cancel</button>
							</div>
						</fieldset>
					</form>
				</div>
			</div>
	    </div>
      	
      	<div class="span4">  
			<div class="widget ">
      			<div class="widget-header">
      				<i class="icon-picture"></i>
      				<h3>Foto</h3>
  				</div> <!-- /widget-header -->
				<div class="widget-content">
					<div class="kontener">
					<?php if(!empty($mhs2->image)){?>
						<img src="<?= base_url()?>image/biodata/<?= $mhs->KDPSTMSMHS; ?>/<?= $mhs2->image;?>" class="imagex">
					<?php }else{ ?>
						<img src="<?= base_url()?>image/biodata/user.png" class="imagex">
					<?php } ?>
						<div class="overle">
							<a onclick="image_upl(<?= $mhs2->npm;?>)" data-toggle="modal" href="#uploadModal"  class="iconx" title="Image">
								<i class="icon-camera"></i>
							</a>
						</div>
					</div>
				</div> <!-- /widget-content -->
			</div> <!-- /widget -->
	    </div> <!-- /span4 -->
      	
      	
    </div> <!-- /row -->
	
</div> <!-- /container -->


<div class="modal fade" id="uploadModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content modal-sm" id="upload">
            
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div> 
	   
	   