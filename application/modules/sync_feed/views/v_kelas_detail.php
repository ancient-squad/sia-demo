<script type="text/javascript">
  function add_mhs(){
    $('#dodoy').load('<?php echo base_url();?>sync_feed/kelas/tambah_mhs/');
  }
</script>

<div class="row">
	<div class="span12">      		  		
		<div class="widget ">
			<div class="widget-header">
        <i class="icon-home"></i>
        <?php 
          $sess = $this->session->userdata('sess_login');
          $kd_prodi = $this->usergroup == '26' ? $this->session->userdata('tahunajaran_kelas')['prodi'] : $sess['userid'];
          $namamk = $this->db->query("SELECT nama_matakuliah from tbl_matakuliah 
                                      where kd_matakuliah = '".$this->session->userdata('kd_matakuliah')."' 
                                      and kd_prodi = '".$kd_prodi."'")->row() ?>

				<h3>
          Daftar Peserta Kuliah <?php echo $this->session->userdata('kd_matakuliah').' - '.$namamk->nama_matakuliah.' = '.$nama_kelas; ?>
        </h3>
			</div> <!-- /widget-header -->
			
			<div class="widget-content">
				<div class="span11">
          <a data-toggle="modal" onclick="add_mhs()" href="#myModal_dodoy" class="btn btn-success">
            <i class="btn-icon-only icon-plus"></i>Tambah Mahasiswa
          </a>
          <!-- <a href="#" class="btn btn-primary"><i class="btn-icon-only icon-fire"></i>Rekap Data</a> -->
          <hr>
          <table id="example1" class="table table-bordered table-striped">
            <thead>
                <tr> 
                    <th>No</th>
                    <th>NPM</th>
                    <th>Nama</th>

                    <?php if ($th == yearBefore()) { ?>
                      
                      <th>Nilai Akhir</th>
                      <th width="80">Nilai Huruf</th>

                    <?php } ?>
                    
                    <th>Aksi</th>
                </tr>
            </thead>
            <tbody>
              <?php $no=1; foreach($peserta as $row) { ?>
              <tr>
                <td><?php echo $no; ?></td>
                <td><?php echo $row->npm_mahasiswa; ?></td>
                <td><?php echo get_nm_mhs($row->npm_mahasiswa); ?></td>

                <?php 
                  if ($th == yearBefore()) {
                    $nilai_huruf = $this->db->select('*')
                                            ->from('tbl_transaksi_nilai')
                                            ->where('THSMSTRLNM',$th)
                                            ->where('KDKMKTRLNM',$this->session->userdata('kd_matakuliah'))
                                            ->where('NIMHSTRLNM',$row->npm_mahasiswa)
                                            ->get()->row();
                                            // var_dump($nilai_huruf);exit();
                    
                    if (!is_null($nilai_huruf)) {
                      $hasil_angka = $nilai_huruf->nilai_akhir;
                      $hasil_huruf = $nilai_huruf->NLAKHTRLNM;
                    } else {
                      $hasil_angka = '';
                      $hasil_huruf = '';
                    }
                    
                  }
                ?>                                   
                  
                <?php if ($th == yearBefore()) { ?>
                  
                  <td><?php echo $hasil_angka; ?></td>

                  <?php if ($nilai_huruf == FALSE) {
                    $letter = "";

                  } else {
                    $letter = $hasil_huruf;

                  } ?>

                  <td><?php echo $letter; ?></td>

                <?php } ?>
                
                <td>
                  <a class="btn btn-danger btn-small" href="<?= base_url('sync_feed/kelas/delete_jadwal/'.$row->npm_mahasiswa); ?>">
                    <i class="btn-icon-only icon-remove"></i>
                  </a>
                </td>
              </tr>
              <?php $no++; } ?>
            </tbody>
          </table>
        </div>                
      </div>
    </div>
  </div>
</div>


<div class="modal fade" id="myModal_dodoy" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">

    <div class="modal-dialog">

        <div class="modal-content" id="dodoy">

            

        </div><!-- /.modal-content -->

    </div><!-- /.modal-dialog -->

</div><!-- /.modal -->