<div class="row">
    <div class="span12">                    
        <div class="widget ">
            <div class="widget-header">
                <i class="icon-building"></i>
                <h3>Pilih Prodi</h3>
            </div> <!-- /widget-header -->
            
            <div class="widget-content">
                <div class="span11">
                    <form class ='form-horizontal' action="<?= base_url('sync_feed/maba/persist_session'); ?>" method="post">
                        <div id="jurusan" class="control-group">
                            <label class="control-label">Prodi</label>
                            <div class="controls">
                                <select class="form-control span6" name="prodi" required>
                                    <option value="">--Pilih Prodi--</option>
                                    <?php foreach ($prodi as $prodis): ?>
                                        <option value="<?= $prodis->kd_prodi ?>"><?= $prodis->prodi ?></option>
                                    <?php endforeach ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-actions">
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>