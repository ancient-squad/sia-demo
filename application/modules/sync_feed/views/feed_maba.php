<div class="row">
	<div class="span12">      		  		
  		<div class="widget ">
  			<div class="widget-header">
  				<i class="icon-list"></i>
  				<h3>Daftar Mahasiswa Baru S1</h3>
			</div> <!-- /widget-header -->
			
			<div class="widget-content">
				<div class="span11">
					<a href="<?= base_url('sync_feed/maba') ?>" class="btn btn-warning">&laquo; Kembali</a>
					<a href="<?= base_url('sync_feed/maba/sync_maba_feed'); ?>" class="btn btn-success">
						<i class="icon-refresh"></i> Sinkronisasi Mahasiswa Baru
					</a>
					<hr>
					<table id="example1" class="table table-bordered table-striped">
	                	<thead>
	                        <tr>
	                        	<th width="25">No</th>
	                        	<th>ID Registrasi</th>
                                <th>Nama</th>
                                <th>NPM</th>
                                <th>Telepon</th>
                                <th>Jenis Kelamin</th>
                                <th>Nama Ibu</th>
                                <th>Tempat lahir</th>
                                <th>Tanggal Lahir</th>
	                        </tr>
	                    </thead>
	                    <tbody>
                            <?php $no=1; foreach ($look as $row) { ?>
	                        <tr>
                                <td><?= $no;?></td>
	                        	<td><?= $row->nomor_registrasi;?></td>
	                        	<td><?= $row->nama;?></td>
	                        	<td><?= $row->npm_baru;?></td>
	                        	<td><?= $row->tlp; ?></td>
	                        	<td><?= ($row->kelamin == 'P') ? "Perempuan" : "Laki-laki"; ?></td>
	                        	<td><?= $row->nm_ibu;?></td>
	                        	<td><?= $row->tempat_lahir; ?></td>
	                        	<td><?= $row->tgl_lahir; ?></td>
	                        </tr>
                            <?php $no++; } ?>
							
	                    </tbody>
	               	</table>
				</div>
			</div>
		</div>
	</div>
</div>
