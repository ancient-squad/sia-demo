<div class="row">
	<div class="span12">      		  		
  		<div class="widget ">
  			<div class="widget-header">
  				<i class="icon-file-text-alt"></i>
  				<h3>Status Data Jadwal Mata Kuliah Pra Pelaporan</h3>
			</div>
			
			<div class="widget-content">
				<div class="span11">

					<div id="alert-response-status"></div>
					
					<table id="example1" class="table table-bordered table-striped">
	                	<thead>
	                        <tr> 
	                        	<th>No</th>
                                <th>Tahun Akademik</th>
                                <th>Status</th>
                                <th style="text-align: center;" width="90">Sinkronisasi</th>
	                        </tr>
	                    </thead>
	                    <tbody>
                            <?php $no=1; foreach ($status as $status_sync) : ?>
	                        <tr>
                                <td><?= $no; ?></td>
                                <td><?= $status_sync->tahun_akademik ?></td>
                                <td id="col-<?= $status_sync->kode ?>"><?= is_null($status_sync->kd_tahunajaran) 
                                		? '<span class="label label-warning">Belum Tersedia</span>' 
                                		: '<span class="label label-success">Sudah Tersedia</span>' ?>
                        		</td>
                                <td style="text-align: center;" id="act-col-<?= $status_sync->kode ?>">
                                	<button class="btn btn-warning" onclick="sync('<?= $status_sync->kode ?>')"><i class="icon icon-refresh"></i></button>
                                </td>
	                        </tr>
                            <?php $no++; endforeach;  ?>
	                    </tbody>
	               	</table>
				</div>
			</div>
		</div>
	</div>
</div>

<script>const baseurl = '<?= base_url() ?>';</script>
<script src="<?= base_url('assets/js/custom/migrate_jdl_mk.js') ?>"></script>