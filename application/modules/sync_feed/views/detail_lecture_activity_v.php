<div class="row">
	<div class="span12">      		  		
  		<div class="widget ">
  			<div class="widget-header">
  				<i class="icon-user"></i>
  				<h3>Detail Aktifitas Dosen - <?= nama_dsn($nid) ?> (<?= $nid ?>)</h3>
			</div> <!-- /widget-header -->
			
			<div class="widget-content">
				<div class="span11">
					<a class="btn btn-warning" href="<?= base_url('sync_feed/research_validation') ?>"><i class="icon-chevron-left"></i> Kembali</a>
					<hr>
					<h3><i class="icon-book"></i> Penelitian</h3>
					<table id="example" class="table table-bordered table-striped">
	                	<thead>
	                        <tr> 
	                        	<th>No</th>
	                        	<th>Judul Penelitian</th>
	                        	<th>Tahun</th>
	                        	<th>Biaya</th>
	                        	<th>Kategori</th>
	                        	<th>Jabatan</th>
	                        	<th width="250">Aksi</th>
	                        </tr>
	                    </thead>
	                    <tbody>
                            <?php $no = 1; foreach ($research as $row) { ?>
	                        <tr>
	                        	<td><?= $no; ?></td>
                                <td><?= $row->judul; ?></td>
                                <td><?= $row->tahun; ?></td>
                                <td><?= tridarma('rsc',$row->biaya); ?></td>
                                <td><?= tridarma('rsc',$row->jenis); ?></td>
                                <td><?= tridarma('rsc',$row->posisi); ?></td>
                                <td>
                                	<button class="btn btn-primary"><i class="icon-eye-open"></i> Lihat</button>
                                	<button class="btn btn-success"><i class="icon-check"></i>  Setujui</button>
                                	<button class="btn btn-danger"><i class="icon-remove"></i> Tolak</button>
                                </td>
	                        </tr>
	                        <?php $no++; } ?>
	                    </tbody>
	               	</table>

	               	<hr>

	               	<h3><i class="icon-group"></i> Pengabdian</h3>
					<table id="example" class="table table-bordered table-striped">
	                	<thead>
	                        <tr> 
	                        	<th>No</th>
	                        	<th>Judul Pengabdian</th>
	                        	<th>Tahun</th>
	                        	<th>Jenis</th>
	                        	<th>Posisi</th>
	                        	<th>Dana</th>
	                        	<th width="250">Aksi</th>
	                        </tr>
	                    </thead>
	                    <tbody>
                            <?php $no = 1; foreach ($devotion as $dev) { ?>
	                        <tr>
	                        	<td><?= $no; ?></td>
                                <td><?= $dev->judul; ?></td>
                                <td><?= $dev->tahun; ?></td>
                                <td><?= tridarma('rsc',$dev->jenis); ?></td>
                                <td><?= tridarma('rsc',$dev->posisi); ?></td>
                                <td><?= tridarma('rsc',$dev->dana); ?></td>
                                <td>
                                	<button class="btn btn-primary"><i class="icon-eye-open"></i> Lihat</button>
                                	<button class="btn btn-success"><i class="icon-check"></i>  Setujui</button>
                                	<button class="btn btn-danger"><i class="icon-remove"></i> Tolak</button>
                                </td>
	                        </tr>
	                        <?php $no++; } ?>
	                    </tbody>
	               	</table>

	               	<hr>

	               	<h3><i class="icon-share"></i> Publikasi</h3>
					<table id="example" class="table table-bordered table-striped">
	                	<thead>
	                        <tr> 
	                        	<th>No</th>
	                        	<th>Judul Publikasi</th>
	                        	<th>Jurnal</th>
	                        	<th>Tahun</th>
	                        	<th>Tingkat</th>
	                        	<th>Kategori</th>
	                        	<th width="250">Aksi</th>
	                        </tr>
	                    </thead>
	                    <tbody>
                            <?php $no = 1; foreach ($publication as $pub) { ?>
	                        <tr>
	                        	<td><?= $no; ?></td>
                                <td><?= $pub->judul; ?></td>
                                <td><?= $pub->jurnal; ?></td>
                                <td><?= $pub->tahun; ?></td>
                                <td><?= tridarma('pub',$pub->tingkat); ?></td>
                                <td><?= $pub->tipe == 'Y' ? 'Penelitian' : 'Non Penelitian'; ?></td>
                                <td>
                                	<button class="btn btn-primary"><i class="icon-eye-open"></i> Lihat</button>
                                	<button class="btn btn-success"><i class="icon-check"></i>  Setujui</button>
                                	<button class="btn btn-danger"><i class="icon-remove"></i> Tolak</button>
                                </td>
	                        </tr>
	                        <?php $no++; } ?>
	                    </tbody>
	               	</table>
				</div>
			</div>
		</div>
	</div>
</div>