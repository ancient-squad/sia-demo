<script type="text/javascript">
	function edit(idk){
	    $('#content').load('<?= base_url('sync_feed/aktifitas_mahasiswa/edit/') ?>'+idk);
	}
</script>
<div class="row">
    <div class="span12">                    
        <div class="widget ">
            <div class="widget-header">
            	<a 
            		href="<?= base_url('sync_feed/aktifitas_mahasiswa') ?>" 
            		class="btn btn-default" 
            		style="margin-left: 10px"
            		data-toggle="tooltip"
            		title="kembali">
                	<i class="icon-chevron-left" style="margin-left: 0"></i>
                </a>
                <h3>Data Aktifitas Mahasiswa Tahun Akademik <?= get_thajar($tahunakademik) ?></h3>
            </div>
            
            <div class="widget-content">
                <div class="span11">
	                <a href="#myModal" data-toggle="modal" class="btn btn-primary" >
	                	<i class="btn-icon-only icon-plus"> </i> Tambah Data
	                </a>
	                <a 
	                	href="<?= base_url('sync_feed/aktifitas_mahasiswa/sync_activity') ?>" 
	                	class="btn btn-warning">
	                	<i class="btn-icon-only icon-refresh"> </i> Sinkronisasi Seluruh Aktifitas
	                </a>
	                <button 
						type="button" 
						data-toggle="modal"
						data-target="#info" 
						style="margin-right: 5px" 
						class="btn btn-default pull-right">
						<i class="icon icon-question"></i> Petunjuk
	                </button>
	                <hr>
                    <table id="example1" class="table table-bordered table-striped">
                        <thead>
	                        <tr>
	                            <th>No</th>
	                            <th>Jenis</th>
								<th>Judul</th>
	                            <th>Nomor SK</th>
								<th>Tanggal SK</th>
								<th>Status</th>
	                            <th width="160">Aksi</th>
	                        </tr>
	                    </thead>
	                    <tbody>
							<?php $no=1; foreach($aktifitas as $row){?>
	                        <tr>
	                        	<td><?= $no ?></td>
	                            <td><?= $row->nama ?></td>
	                            <td><?= $row->judul ?></td>
								<td><?= $row->no_sk_tugas ?></td>
								<td><?= $row->tgl_sk_tugas ?></td>
								<td>
									<?= !is_null($row->id_aktifitas_feeder) ? 
									'<span class="label label-success"> Tersinkronisasi</span>' : 
									'<span class="label label-default">Belum Tersinkronisasi</span>' ?>
								</td>
	                        	<td>
	                        		<a 
										href="<?=  base_url('sync_feed/aktifitas_mahasiswa/detail/'.$row->id) ?>"
										class="btn btn-success btn-small edit"
										data-toggle="tooltip" 
										title="detail">
										<i class="btn-icon-only icon-list"></i>
									</a>
	                        		<span data-toggle="tooltip" title="edit">
										<a 
											onclick="edit(<?= $row->id ?>)"
											href="#editModal"
											data-toggle="modal"
											class="btn btn-primary btn-small edit">
											<i class="btn-icon-only icon-pencil"></i>
										</a>
	                        		</span>
									<a 
										data-toggle="tooltip"
										title="hapus"
										onclick="return confirm('Apakah Anda yakin ingin menghapus data ini?');" 
										class="btn btn-danger btn-small" 
										href="<?= base_url('sync_feed/aktifitas_mahasiswa/remove/'.$row->id);?>">
										<i class="btn-icon-only icon-trash"> </i>
									</a>
									<a 
										href="<?=  base_url('sync_feed/aktifitas_mahasiswa/sync_activity_unit/'.$row->id) ?>"
										class="btn btn-warning btn-small edit"
										data-toggle="tooltip" 
										title="sinkronisasi aktifitas satuan">
										<i class="btn-icon-only icon-refresh"></i>
									</a>
								</td>
	                        </tr>
							<?php $no++; } ?>
	                    </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="myModal" class="modal fade" role="dialog">
	<div class="modal-dialog">

		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">Tambah Data Aktifitas</h4>
			</div>
			<form action="<?=  base_url('sync_feed/aktifitas_mahasiswa/save') ?>" method="post" class="form-horizontal">
				<div class="modal-body">
					<div class="control-group">
						<label for="jenis" class="control-label">Jenis <small style="color: red">*</small></label>
						<div class="controls">
							<select name="jenis" id="" class="span3" required="">
								<option value="" disabled="" selected=""></option>
								<?php foreach ($jenis_aktifitas as $val) : ?>
									<option value="<?= $val->kode ?>"><?= $val->nama ?></option>
								<?php endforeach; ?>
							</select>
						</div>
					</div>
					<div class="control-group">
						<label for="judul" class="control-label">Judul <small style="color: red">*</small></label>
						<div class="controls">
							<textarea name="judul" class="span3" id="judul" required=""></textarea>
						</div>
					</div>
					<div class="control-group">
						<label for="lokasi" class="control-label">Lokasi</label>
						<div class="controls">
							<input type="text" class="span3" name="lokasi" id="lokasi">
						</div>
					</div>
					<div class="control-group">
						<label for="no_sk" class="control-label">Nomor SK Tugas</label>
						<div class="controls">
							<input type="text" class="span3" name="no_sk" id="no_sk">
						</div>
					</div>
					<div class="control-group">
						<label for="tgl_sk" class="control-label">Tanggal SK Tugas</label>
						<div class="controls">
							<input type="text" class="span3" name="tgl_sk" id="tgl_sk">
						</div>
					</div>
					<div class="control-group">
						<label for="" class="control-label">Jenis Anggota</label>
						<div class="controls">
							<div class="radio">
								<label><input type="radio" name="anggota" value="0">Personal</label>
								<label><input type="radio" name="anggota" value="1">Kelompok</label>
							</div>
						</div>
					</div>
					<div class="control-group">
						<label for="note" class="control-label">Keterangan</label>
						<div class="controls">
							<textarea name="note" class="span3" id="note"></textarea>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="submit" class="btn btn-primary">Save</button>
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				</div>
			</form>
		</div>

	</div>
</div>

<div id="editModal" class="modal fade" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content" id="content">
			
		</div>
	</div>
</div>

<div id="info" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Panduan Fitur</h4>
      </div>
      <div class="modal-body">
        <p>Buat data aktifitas baru dengan klik tombol <b>Tambah Data</b>. Kemudian lakukan sinkronisasi keseluruhan atau bisa dengan sinkronisasi satuan dengan mengklik tombol berwarna kuning pada daftar aktifitas. Tahapan sinkronisasi dimulai dengan melakukan sinkronisasi aktifitas terlebih dahulu. Kemudian klik tombol detail (warna hijau) pada daftar aktifitas untuk mengelola konten aktifitas seperti anggota, pembimbing, dan penguji. Setelah data-data konten terisi silahkan lakukan sinkronisasi sesuai dengan konten aktifitas yang telah di-<i>input</i> sebelumnya.</p>
        <p><u><b>Perhatian!</b></u> Lakukan proses sinkronisasi secara bertahap sesuai panduan ini.</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

<script>
	$(document).ready(function() {
		$('#tgl_sk').datepicker({
			dateFormat: "yy-mm-dd",
			yearRange: "2000:<?= date('Y') ?>",
			changeMonth: true,
			changeYear: true
		});
	})
</script>