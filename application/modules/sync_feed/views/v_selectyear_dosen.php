<div class="row">
	<div class="span12">      		  		
		<div class="widget ">
			<div class="widget-header">
				<i class="icon-list"></i>
				<h3>Data Dosen</h3>
		</div> <!-- /widget-header -->
			
			<div class="widget-content">
				<div class="span11">
  				<form method="post" class="form-horizontal" action="<?= base_url(); ?>sync_feed/dosen_ajar/loadData">
            <fieldset>
              <?php if ($this->session->userdata('sess_login')['id_user_group'] == '26'): ?>
                <div class="control-group">
                  <label class="control-label">Prodi</label>
                  <div class="controls">
                    <select class="form-control span6" name="prodi">
                      <option disabled selected>--Pilih Prodi--</option>
                      <?php foreach ($prodi as $value): ?>
                        <option value="<?= $value->kd_prodi ?>"><?= $value->prodi ?></option>
                      <?php endforeach ?>
                    </select>
                  </div>
                </div> 
              <?php endif ?>
              <div class="control-group">
                <label class="control-label">Tahun</label>
                <div class="controls">
                  <select class="form-control span6" name="tahun">
                    <option disabled selected>--Pilih Tahun Akademik--</option>
                    <?php foreach ($akademik as $val) { ?>
                      <option value="<?= $val->kode; ?>"><?= $val->tahun_akademik; ?></option>
                    <?php } ?>
                  </select>
                </div>
              </div>  
              <div class="form-actions">
                  <input type="submit" class="btn btn-large btn-success" value="Submit"/> 
              </div> <!-- /form-actions -->
            </fieldset>
          </form>
				</div>
			</div>
		</div>
	</div>
</div>
