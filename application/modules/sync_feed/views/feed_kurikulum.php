<link href="https://gitcdn.github.io/bootstrap-toggle/2.2.0/css/bootstrap-toggle.min.css" rel="stylesheet">
<script src="https://gitcdn.github.io/bootstrap-toggle/2.2.0/js/bootstrap-toggle.min.js"></script>

<script type="text/javascript">
$(document).ready(function(){
	var table = $('#tabel_kurikulum');
	
	var oTable = table.dataTable({
		"aoColumnDefs": [{ "bVisible": false, "aTargets": [8,9,10,11] }]
	});
	
	$('#dup').hide();
	$('#kd_kurikulum').keyup(function(){
		var teks = $('#kd_kurikulum').val();
		$.ajax({
			url: "<?php echo base_url('perkuliahan/kurikulum/cek_dup'); ?>",
            type: "post",
            data: {data:teks},
            success: function(d) {
				if(d!=0){
					$('#dup').show();
				}else{
					$('#dup').hide();
				}
            }
        });
    });
	
	$('#new').click(function () {
		$('#edit_modal').modal('show');
		$('#id_kurikulum').val('');
		$('#kd_kurikulum').val('');
		$('#kurikulum').val('');
		$('#tahun_ajaran_kurikulum').val('');
		$('#faks').val('');
		$('#jurs').val('');
    });
	
	table.on('click', '.edit', function(e) {
		$('#edit_modal').modal('show');
		var nRow = $(this).parents('tr')[0];
        var aData = oTable.fnGetData(nRow);
		
		$('#id_kurikulum').val(aData[8]);
		$('#kd_kurikulum').val(aData[1]);
		$('#kd_kurikulum').attr('readonly', true);
		$('#kurikulum').val(aData[2]);
		$('#tahun_ajaran_kurikulum').val(aData[11]);
		$('#faks').val(aData[10]);
		$.post('<?php echo base_url()?>perkuliahan/kurikulum/get_jurusan/'+$('#faks').val(),{},function(get){
			$('#jurs').html(get);
			$('#jurs').val(aData[9]);
        });
		$('#status').attr('checked', true);
	});
});
</script>
<style>
	#dup label{
		margin-bottom: 0px;
	}
</style>

<div class="row">
	<div class="span12">      		  		
  		<div class="widget ">
  			<div class="widget-header">
  				<i class="icon-user"></i>
  				<h3>Data Kurikulum</h3>
			</div> <!-- /widget-header -->
			
			<div class="widget-content">
				<div class="span11">
					<?php if ($aktif->status == 100) { ?>
						<a data-toggle="modal" id="new" class="btn btn-primary"> New Data </a> <br><hr>
					<?php } elseif ($aktif->status == 0) { ?>
						<input type="hidden">
					<?php } ?>
					<table id="tabel_kurikulum" class="table table-bordered table-striped">
	                	<thead>
	                        <tr> 
	                        	<th>No</th>
								<th>Kode Kurikulum</th>
	                        	<th>Kurikulum</th>
	                        	<th>Tahun Ajaran</th>
	                        	<th>Prodi</th>
                                <th>Fakultas</th>
                                <th>Status</th>
	                            <th width="120">Aksi</th>
								<th></th>
								<th></th>
								<th></th>
								<th></th>
	                        </tr>
	                    </thead>
	                    <tbody>
                            <?php $no = 1; foreach ($data_table as $row) { ?>
	                        <tr>
								<td><?php echo $no; ?></td>
	                        	<td><?php echo $row->kd_kurikulum; ?></td>
	                        	<td><?php echo $row->kurikulum; ?></td>
                                <td><?php echo $row->tahunajaran; ?></td>
                                <td><?php echo $row->prodi; ?></td>
                                <td><?php echo $row->fakultas; ?></td>
                                <td><?php if($row->status == 1){ echo 'Aktif'; }else{ echo 'Tidak Aktif'; } ?></td>
	                        	<td class="td-actions">
	                        		<a class="btn btn-warning btn-small" target="_blank" href="<?php echo base_url(); ?>sync_feed/kurikulum/sync_kurikulum/<?php echo $row->kd_kurikulum; ?>"><i class="btn-icon-only icon-refresh"> </i></a>
									<a class="btn btn-success btn-small" href="<?php echo base_url(); ?>sync_feed/kurikulum/detail_kurikulum/<?php echo $row->kd_kurikulum; ?>"><i class="btn-icon-only icon-ok"> </i></a>

                                    <?php if ($aktif->status == 1) { ?>
                                    	<button type="button" class="btn btn-primary btn-small edit"><i class="btn-icon-only icon-pencil"> </i></button>
										<!-- <a onclick="return confirm('Apakah Anda Yakin?');" class="btn btn-danger btn-small" href="<?php echo base_url(); ?>perkuliahan/kurikulum/delte_kurikulum/<?php echo $row->id_kurikulum; ?>"><i class="btn-icon-only icon-remove"> </i></a> -->
									<?php } elseif ($aktif->status == 0) { ?>
										<input type="hidden">
									<?php } ?>
                                    
								</td>
								<td><?php echo $row->id_kurikulum; ?></td>
								<td><?php echo $row->kd_prodi; ?></td>
								<td><?php echo $row->kd_fakultas; ?></td>
								<td><?php echo $row->tahun_ajaran_kurikulum; ?></td>
	                        </tr>
	                        <?php $no++; } ?>
	                    </tbody>
	               	</table>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="edit_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">FORM DATA</h4>
            </div>
            <form class ='form-horizontal' action="<?php echo base_url();?>perkuliahan/kurikulum/add_new" method="post">
                <div class="modal-body">    
                    <div class="control-group" id="">
                        <label class="control-label">Kode Kurikulum</label>
                        <div class="controls">
							<input type="hidden" id="id_kurikulum" name="id_kurikulum" class="span4" />
                            <input type="text" id="kd_kurikulum" name="kd_kurikulum" class="span4" placeholder="Input Kode Kurikulum" class="form-control" required/>
							<div id="dup">
								<p></p>
								<label class="alert">Kode Sudah Ada</label>
							</div>
						</div>
                    </div>
                    <div class="control-group" id="">
                        <label class="control-label">Kurikulum</label>
                        <div class="controls">
                            <input type="text" id="kurikulum" name="kurikulum" class="span4" placeholder="Input Kurikulum" class="form-control" required/>
                        </div>
                    </div>
					<script>
                              $(document).ready(function(){
                                $('#faks').change(function(){
                                  $.post('<?php echo base_url()?>perkuliahan/kurikulum/get_jurusan/'+$(this).val(),{},function(get){
                                    $('#jurs').html(get);
                                  });
                                });
                              });
                              </script>

                    <?php 	$logged = $this->session->userdata('sess_login');
							$pecah = explode(',', $logged['id_user_group']);
							$jmlh = count($pecah);
							for ($i=0; $i < $jmlh; $i++) { 
								$grup[] = $pecah[$i];
							} 
							if ((in_array(1, $grup)) or (in_array(10, $grup)) or (in_array(26, $grup))) { ?>
                    	<div class="control-group" id="">
	                        <label class="control-label">Fakultas</label>
	                        <div class="controls">
	                            <select class="form-control span4" name="kd_fakultas" id="faks">
                                    <option>--Pilih Fakultas--</option>
                                    <?php foreach ($fakultas as $row) { ?>
                                    <option value="<?php echo $row->kd_fakultas; ?>"><?php echo $row->fakultas;?></option>
                                    <?php } ?>
                                </select>
	                        </div>
	                    </div>
	                    <div class="control-group" id="">
	                        <label class="control-label">Prodi</label>
	                        <div class="controls">
	                            <select class="form-control span4" name="kd_prodi" id="jurs">
	                                <option>--Pilih Jurusan--</option>
	                            </select>
	                        </div>
	                    </div>
                    <?php } elseif ((in_array(9, $grup))) {?>
                    		<input type="hidden" value="<?php echo $kdfak; ?>" name="kd_fakultas"/>
                    		<div class="control-group" id="">
		                        <label class="control-label">Prodi</label>
		                        <div class="controls">
		                            <select class="form-control span4" name="kd_prodi" id="jurs">
		                                    <option>--Pilih Jurusan--</option>
		                                    <?php foreach ($listprodi as $key) {
		                                    	echo "<option value='".$key->kd_prodi."'> ".$key->prodi." </option>";
		                                    } ?>
		                                  </select>
		                        </div>
		                    </div>
                    <?php } else { $fakultas=$this->app_model->getdetail('tbl_jurusan_prodi','kd_prodi',$logged['userid'],'kd_prodi','asc')->row(); ?>
                    	<input type="hidden" value="<?php echo $logged['userid']; ?>" name="kd_prodi"/>
                    	<input type="hidden" value="<?php echo $fakultas->kd_fakultas; ?>" name="kd_fakultas"/>
                    	<div class="control-group" id="">
	                        <label class="control-label">Jumlah SKS</label>
	                        <div class="controls">
	                            <input value="" id="status" type="number" name="jml">
	                        </div>
	                    </div>
                    <?php } ?>

                    <div class="control-group" id="">
                        <label class="control-label">Tahun Ajaran</label>
                        <div class="controls">
                            <select class="form-control span4" name="tahun_ajaran_kurikulum" id="tahun_ajaran_kurikulum">
                                    <option>--Pilih Tahun Ajaran--</option>
                                    <?php foreach ($tahunajaran as $row) { ?>
                                    <option value="<?php echo $row->id_tahunajaran;?>"><?php echo $row->tahunajaran;?></option>
                                    <?php } ?>
                                  </select>
                        </div>
                    </div>
                    <div class="control-group" id="">
                        <label class="control-label">Aktif</label>
                        <div class="controls">
                            <input value="1" id="status" type="checkbox" name="status">
                        </div>
                    </div>

                    
                </div> 
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <input type="submit" name="save" class="btn btn-primary" value="Save changes"/>
                </div>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->