<div class="row">
  <div class="span12">                
    <div class="widget ">
      <div class="widget-header">
        <i class="icon-user"></i>
        <h3>Data Aktifitas Mahasiswa</h3>
      </div>
      
      <div class="widget-content">
        <div class="span11">
          <form method="post" class="form-horizontal" action="<?= base_url('sync_feed/aktifitas_mahasiswa/set_session'); ?>">
            <fieldset>
              <div id="tahun" class="control-group">
                <input type="hidden" name="jurusan" value="<?= $userid ?>">
                <label class="control-label">Tahun Ajaran</label>
                <div class="controls">
                  <select class="form-control span6" name="tahunakademik" required>
                    <option disabled selected value="">-- Pilih Tahun Akademik --</option>
                    <?php foreach ($tahunakademik as $key) { ?>
                      <option value="<?= $key->kode; ?>"><?= $key->tahun_akademik; ?></option>
                    <?php } ?>
                  </select>
                </div>
              </div>
              <div class="form-actions">
                <input type="submit" class="btn btn-success" value="Cari"/> 
              </div>
            </fieldset>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>


