<style type="text/css">
	.ui-autocomplete {
		z-index: 215000000 !important;
	}
</style>

<div class="container">
    <div class="row">
      	<div class="span12">      		
      		<div class="widget ">

      			<div class="widget-header">
      				<a 
	            		href="<?= base_url('sync_feed/lulusan/view_lulusan') ?>" 
	            		class="btn btn-default" 
	            		style="margin-left: 10px"
	            		data-toggle="tooltip"
	            		title="kembali">
	                	<i class="icon-chevron-left" style="margin-left: 0"></i>
	                </a>
      				<h3>Lulusan Form</h3>
  				</div>
				
				<div class="widget-content">
					
					<form 
						id="edit-profile" 
						method="POST" 
						action="<?= base_url(); ?>sync_feed/lulusan/save_edit" 
						class="form-horizontal">

						<fieldset>
							<div class="control-group">											
								<label class="control-label">Tahun Akademik Lulus</label>
								<div class="controls">
									<input 
										type="text" 
										class="span3" 
										value="<?= get_thajar($row->ta_lulus);?>" 
										readonly />
									<input type="hidden" class="span1" name="ta" id="ta" value="<?= $row->ta_lulus;?>">
									<input type="hidden" class="span1" name="id" id="ta" value="<?= $row->id_lulusan;?>">
								</div>
							</div>

							<div class="control-group">											
								<label class="control-label">NPM</label>
								<div class="controls">
									<input type="text" class="span3" name="mhs" id="mhs" value="<?= $row->npm_mahasiswa;?>"  readonly>
								</div>
							</div>

							<div class="control-group">											
								<label class="control-label">Nama Mahasiswa</label>
								<div class="controls">
									<input type="text" class="span3" value="<?= $row->NMMHSMSMHS;?>"  readonly>
								</div>
							</div>

							<div class="control-group">											
								<label class="control-label">Tanggal Lulus *</label>
								<div class="controls">
									<input type="text" class="span3" name="tgl_lulus" id="tgl_lulus"  value="<?= $row->tgl_lulus; ?>"  required>
								</div>				
							</div>

							<div class="control-group">											
								<label class="control-label">Nomor S.K. Yudisium *</label>
								<div class="controls">
									<input type="text" class="span3" name="sk_yudi"  value="<?= $row->sk_yudisium; ?>" required>
								</div>
							</div>

							<div class="control-group">											
								<label class="control-label">Tanggal S.K. *</label>
								<div class="controls">
									<input type="text" class="span3" name="tgl_yudi" id="tgl_yudi" value="<?= $row->tgl_yudisium; ?>"  required>
									<p class="help-block">*Masukan Tanggal yang Tertera Pada S.K. Yudisium</p>
								</div>
							</div>

							<div class="control-group">											
								<label class="control-label">Total SKS *</label>
								<div class="controls">
									<input type="text" class="span1" name="sks" value="<?= $row->sks;?>" onkeyup="this.value=this.value.replace(/[^0-9]/g,'');"  required>
								</div>
							</div>

							<div class="control-group">											
								<label class="control-label">IPK *</label>
								<div class="controls">
									<input type="number" step=".01" class="span1" name="ipk" value="<?= $row->ipk; ?>" onkeyup="this.value=this.value.replace(/[^0-9]/g,'');"  required>
								</div>
							</div>

							<div class="control-group">											
								<label class="control-label">Nomor Ijazah *</label>
								<div class="controls">
									<input type="text" class="span3" name="no_ijazah" value="<?= $row->no_ijazah; ?>"  required>
								</div>
							</div>

							<div class="control-group">											
								<label class="control-label">Judul Skripsi *</label>
								<div class="controls">
									<textarea  class="span6" name="jdl_skripsi" required><?= $row->jdl_skripsi; ?></textarea>
								</div>
							</div>

							<div class="control-group">											
								<label class="control-label">Dosen Pembimbing *</label>
								<div class="controls">
									<input 
										type="text" 
										class="span3" 
										name="dospem1" 
										id="dospem1" 
										placeholder="Dosen Pembimbing 1" 
										value="<?= nama_dsn_nidn($row->dospem1); ?>" 
										required />
									<input type="hidden" name="nidn1" id="nidn1" value="<?= $row->dospem1; ?>" required />

									<input 
										type="text" 
										class="span3" 
										name="dospem2" 
										id="dospem2" 
										placeholder="Dosen Pembimbing 2" 
										value="<?= nama_dsn_nidn($row->dospem2); ?>" />
									<input type="hidden" class="span3" name="nidn2" id="nidn2"  value="<?= $row->dospem2; ?>" >
								</div>
							</div>

							<div class="control-group">											
								<label class="control-label">Masa Bimbingan *</label>
								<div class="controls">
									<input 
										type="text" 
										class="span3" 
										name="mulai_bim" 
										id="mulai_bim"  
										value="<?= $row->mulai_bim; ?>" 
										required />

									<input 
										type="text" 
										class="span3" 
										name="ahir_bim" 
										id="ahir_bim" 
										value="<?= $row->ahir_bim; ?>" 
										required />
								</div>
							</div>
		
							<div class="form-actions">
								<button type="submit" class="btn btn-primary">Simpan</button> 
								<a href="<?= base_url();?>sync_feed/lulusan/view_lulusan" class="btn btn-default">Kembali</a>
							</div>

						</fieldset>
					</form>
				</div>
			</div>
	    </div>
    </div>
</div>


<script type="text/javascript">
$(document).ready(function($) {
	$('input[name^=mhs]').autocomplete({
		source: '<?= base_url('sync_feed/kelas/load_mhs');?>',
		minLength: 1,
		select: function (evt, ui) {
		    this.form.mhs.value = ui.item.value;
		}
	});

	$('input[name^=dospem1]').autocomplete({
        source: '<?= base_url('sync_feed/lulusan/load_kry_autocomplete');?>',
        minLength: 2,
        select: function (evt, ui) {
            this.form.dospem1.value = ui.item.value;
            this.form.nidn1.value = ui.item.nidn;
        }
    });

    $('input[name^=dospem2]').autocomplete({
        source: '<?= base_url('sync_feed/lulusan/load_kry_autocomplete');?>',
        minLength: 2,
        select: function (evt, ui) {
            this.form.dospem2.value = ui.item.value;
            this.form.nidn2.value = ui.item.nidn;
        }
    });

	$('#tgl_lulus').datepicker({
		dateFormat: "yy-mm-dd",
		yearRange: "2016:2020",
		changeMonth: true,
		changeYear: true
	});

	$('#tgl_yudi,#mulai_bim,#ahir_bim').datepicker({
		dateFormat: "yy-mm-dd",
		yearRange: "2000:2020",
		changeMonth: true,
		changeYear: true
	});
});
</script>