<?php

class PDF Extends FPDF 
{
	function Footer()
	{
		$pdf->SetY(-40);
		$pdf->SetFont('Arial','I',8);
		$pdf->Cell(10,10,'*Mengikuti pedoman atau ketentuan dari LPPMP',0,0,'L');
		$pdf->Ln(5); 
		$pdf->Cell(10,10,'**Ketentuan dari pedoman BKD LLDIKTI Wilayah III',0,0,'L'); 
	}
}
	
$pdf = new FPDF("L","mm", "A4");

$pdf->AliasNbPages();
$pdf->AddPage();
$pdf->SetMargins(3, 5 ,0);

$pdf->SetFont('Arial','B',10); 
$pdf->setXY(22,4);
$pdf->Cell(250,5,'Formulir Beban Kinerja Dosen Internal (BKD Internal)',0,1,'C');
$pdf->Cell(290,5,$this->ORG_NAME,0,1,'C');

$pdf->Ln(2);
$pdf->setXY(176,6);
$pdf->image(FCPATH.'/assets/logo.gif',270,2,14);
// param for image => <file>,<margin-left>,<margin-top>,<size>

$pdf->Ln(17);
$pdf->SetFont('Arial','',10); 
$pdf->Cell(33,5,'Nama',0,0,'L');
$pdf->Cell(5,5,' : ',0,0,'L');
$pdf->Cell(170,5,$data->nama,0,0,'L');
$pdf->Cell(23,5,'Fakultas',0,0,'L');
$pdf->Cell(5,5,' : ',0,0,'C');
$pdf->Cell(170,5,$data->fakultas,0,0,'L');

$pdf->Ln(5);
$pdf->SetFont('Arial','',10); 
$pdf->Cell(33,5,'NIDN',0,0,'L');
$pdf->Cell(5,5,' : ',0,0,'L');
$pdf->Cell(170,5,$data->nidn,0,0,'L');
$pdf->Cell(23,5,'Program Studi',0,0,'L');
$pdf->Cell(5,5,' : ',0,0,'C');
$pdf->Cell(170,5,$data->prodi,0,0,'L');

$pdf->Ln(5);
$pdf->SetFont('Arial','',10); 
$pdf->Cell(33,5,'Jabatan Fungsional',0,0,'L');
$pdf->Cell(5,5,' : ',0,0,'L');
$pdf->Cell(170,5,jabfung($data->jabfung),0,0,'L');
$pdf->Cell(23,5,'Semester',0,0,'L');
$pdf->Cell(5,5,' : ',0,0,'C');
$pdf->Cell(170,5,$semester,0,0,'L');

$pdf->Ln(10);
$pdf->SetFont('Arial','B',8);
$pdf->Cell(1,10,'',0,0,'C');
$pdf->Cell(7,10,'NO','L,T,R,B',0,'C');
$pdf->Cell(150,10,'BIDANG / TUGAS','L,T,R,B',0,'C');
$pdf->Cell(80,5,'WAKTU','L,T,R,B',0,'C');
$pdf->Cell(50,10,'Jumlah SKS','L,T,R,B',0,'C');

$pdf->Ln(5);
$pdf->Cell(158,10,'',0,0,'C');
$pdf->Cell(40,5,'HARI','L,T,R,B',0,'C');
$pdf->Cell(40,5,'JAM','L,T,R,B',0,'C');

$pdf->Ln(5);
$pdf->Cell(1,10,'',0,0,'C');
$pdf->SetFont('Arial','B',8);
$pdf->Cell(7,5,'1','L,T,R,B',0,'C');
$pdf->Cell(280,5,'Pendidikan dan Pengajaran','L,T,R,B',0,'L');

foreach ($courses as $course) {
	$pdf->Ln(5);
	$pdf->SetFont('Arial','',8);
	$pdf->Cell(1,10,'',0,0,'L');
	$pdf->Cell(7,5,'','L,T,R,B',0,'L');
	$pdf->Cell(150,5,$course->nama_matakuliah. '('.$course->kd_matakuliah.')','L,T,R,B',0,'L');
	$pdf->Cell(40,5,notohari($course->hari),'L,T,R,B',0,'L');
	$pdf->Cell(40,5,$course->waktu_mulai.' - '.$course->waktu_selesai,'L,T,R,B',0,'L');
	$pdf->Cell(50,5,$course->sks_matakuliah,'L,T,R,B',0,'L');
}

$pdf->Ln(5);
$pdf->Cell(1,10,'',0,0,'C');
$pdf->SetFont('Arial','B',8);
$pdf->Cell(7,5,'2','L,T,R,B',0,'C');
$pdf->Cell(280,5,'Penelitian, Pengabdian Kepada Masyarakat dan Penulisan Buku','L,T,R,B',0,'L');

foreach ($research as $rsc) {
	$pdf->Ln(5);
	$pdf->SetFont('Arial','',8);
	$pdf->Cell(1,10,'',0,0,'C');
	$pdf->Cell(7,5,'','L,T,R,B',0,'C');
	$pdf->Cell(150,5,'Penelitian: '.$rsc->judul,'L,T,R,B',0,'L');
	$pdf->Cell(40,5,'','L,T,R,B',0,'L');
	$pdf->Cell(40,5,'','L,T,R,B',0,'L');
	$pdf->Cell(50,5,'','L,T,R,B',0,'L');
}

foreach ($devotion as $dev) {
	$pdf->Ln(5);
	$pdf->SetFont('Arial','',8);
	$pdf->Cell(1,10,'',0,0,'C');
	$pdf->Cell(7,5,'','L,T,R,B',0,'C');
	$pdf->Cell(150,5,'Pengabdian: '.$dev->judul,'L,T,R,B',0,'L');
	$pdf->Cell(40,5,'','L,T,R,B',0,'L');
	$pdf->Cell(40,5,'','L,T,R,B',0,'L');
	$pdf->Cell(50,5,'','L,T,R,B',0,'L');
}

foreach ($publication as $pub) {
	$pdf->Ln(5);
	$pdf->SetFont('Arial','',8);
	$pdf->Cell(1,10,'',0,0,'C');
	$pdf->Cell(7,5,'','L,T,R,B',0,'C');
	$pdf->Cell(150,5,'Publikasi: '.$pub->judul,'L,T,R,B',0,'L');
	$pdf->Cell(40,5,'','L,T,R,B',0,'L');
	$pdf->Cell(40,5,'','L,T,R,B',0,'L');
	$pdf->Cell(50,5,'','L,T,R,B',0,'L');
}

$pdf->Ln(5);
$pdf->Cell(1,10,'',0,0,'C');
$pdf->SetFont('Arial','B',8);
$pdf->Cell(7,5,'3','L,T,R,B',0,'C');
$pdf->Cell(280,5,'Lain-lain','L,T,R,B',0,'L');

for ($i = 0; $i < 4; $i++) {
	$pdf->Ln(5);
	$pdf->SetFont('Arial','',8);
	$pdf->Cell(1,10,'',0,0,'C');
	$pdf->Cell(7,5,'','L,T,R,B',0,'C');
	$pdf->Cell(150,5,'Lain-lain '.$i,'L,T,R,B',0,'L');
	$pdf->Cell(40,5,'','L,T,R,B',0,'L');
	$pdf->Cell(40,5,'','L,T,R,B',0,'L');
	$pdf->Cell(50,5,'','L,T,R,B',0,'L');
}

$pdf->Ln(5);
$pdf->Cell(1,10,'',0,0,'C');
$pdf->SetFont('Arial','B',8);
$pdf->Cell(237,5,'Total','L,T,R,B',0,'C');
$pdf->Cell(50,5,'','L,T,R,B',0,'L');

date_default_timezone_set('Asia/Jakarta');

$pdf->Ln(5);
$pdf->SetFont('Arial','B',8);
$pdf->Cell(264,1,'',0,0,'C');
$pdf->Cell(7,15,'Bekasi, '.TanggalIndo(date('Y-m-d')),0,0,'R');

$pdf->Ln(15);
$pdf->SetFont('Arial','B',8);
$pdf->Cell(30,1,'',0,0,'C');
$pdf->Cell(110,1,'Wakil Rektor I',0,0,'L');
$pdf->Cell(100,1,'Dekan',0,0,'L');
$pdf->Cell(50,1,'Dosen',0,0,'L');

$pdf->Ln(20);
$pdf->SetFont('Arial','B',8);
$pdf->Cell(15,1,'',0,0,'C');
$pdf->Cell(105,1,'(                                                          )',0,0,'L');
$pdf->Cell(100,1,'(                                                          )',0,0,'L');
$pdf->Cell(50,1,'(                                                          )',0,0,'L');

$pdf->Output('Kartu_BKD_'.date('ymd_his').'.PDF','I');