<script type="text/javascript">
function edit(idk){
        $('#edit').load('<?= base_url();?>form/penugasandosen/load_detail/'+idk);
    }

function status(idk){
        $('#edit1').load('<?= base_url();?>form/penugasandosen/load_status/'+idk);
    }

function status2(idk){
        $('#edit1').load('<?= base_url();?>form/penugasandosen/load_status2/'+idk);
    }

</script>
<div class="row">
    <div class="span12">                    
        <div class="widget ">
            <div class="widget-header">
            	<a 
            		href="<?= base_url('sync_feed/lulusan') ?>" 
            		class="btn btn-default" 
            		style="margin-left: 10px"
            		data-toggle="tooltip"
            		title="kembali">
                	<i class="icon-chevron-left" style="margin-left: 0"></i>
                </a>
                <h3>Data Lulusan</h3>
            </div>
            
            <div class="widget-content">
            	<center>
            		<h4>
            			Data Lulusan <?= $jurusan->prodi.' - '.get_thajar($this->session->userdata('tahun')); ?>
            		</h4>
            	</center>
                <a href="<?= base_url();?>sync_feed/lulusan/form" class="btn btn-primary" >
                	<i class="btn-icon-only icon-plus"> </i> Tambah Data
                </a>
                <a href="<?= base_url();?>sync_feed/lulusan/sync_mhs" class="btn btn-warning" target="_blank">
                	<i class="btn-icon-only icon-refresh"> </i> Sinkronisasi lulusan
                </a>
                <hr>
                <div class="span11">
                    <table id="example1" class="table table-bordered table-striped">
                        <thead>
	                        <tr>
	                            <th>NPM</th>
	                            <th>Nama Mahasiswa</th>
	                            <th>Nomor Ijazah</th>
								<th>Judul Skripsi</th>
								<th>IPK</th>
	                            <th width="80">Aksi</th>
	                        </tr>
	                    </thead>
	                    <tbody>
							<?php  foreach($rows as $row){?>
	                        <tr>
	                        	<td><?= $row->npm_mahasiswa ?></td>
	                            <td><?= $row->NMMHSMSMHS ?></td>
	                            <td><?= $row->no_ijazah ?></td>
								<td><?= $row->jdl_skripsi ?></td>
								<td><?= $row->ipk ?></td>
	                        	<td>
									<?php if ($row->flag_feeder == 2){ ?>
										<button type="button" class="btn btn-warning btn-small edit">
											<i class="btn-icon-only icon-check"></i>
										</button>	
									<?php } else { ?>
										<a 
											href="<?= base_url('sync_feed/lulusan/edit/'.$row->id_lulusan);?>"
											data-toggle="tooltip"
											title="edit"
											class="btn btn-primary btn-small edit">
											<i class="btn-icon-only icon-pencil"></i>
										</a>
									<?php } ?>
									<a 
										data-toggle="tooltip"
										title="hapus"
										onclick="return confirm('Apakah Anda yakin ingin menghapus data ini?');" 
										class="btn btn-danger btn-small" 
										href="<?= base_url('sync_feed/lulusan/delete_data/'.$row->id_lulusan);?>">
										<i class="btn-icon-only icon-trash"> </i>
									</a>
								</td>
	                        </tr>
							<?php } ?>
	                    </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>