<style type="text/css">
    .ui-autocomplete {
        z-index: 215000000 !important;
    }
</style>

<script type="text/javascript">

  $(document).ready(function() {

    // alert("aaaa");
      $('input[name^=student]').autocomplete({

        source: '<?php echo base_url('sync_feed/kelas/load_mhs');?>',

        minLength: 4,

        select: function (evt, ui) {

            this.form.mhs.value = ui.item.value;

        }

      });

  });

</script>

<div class="modal-header">

    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>

    <h4 class="modal-title">Form Tambah Mahasiswa Kelas</h4>

</div>

<form class ='form-horizontal' action="<?= base_url();?>sync_feed/kelas/update_kelas" method="post">

    <div class="modal-body" style="margin-left: -60px;">

        <div class="control-group" id="">

            <label class="control-label">Nama/NPM Mahasiswa</label>

            <div class="controls">

                <input type="text" id="mhs" name="student" >

            </div>

        </div>

    </div> 

    <div class="modal-footer">

        <button type="button" class="btn btn-default" data-dismiss="modal">Keluar</button>

        <input type="submit" class="btn btn-primary" value="Simpan"/>

    </div>

</form>