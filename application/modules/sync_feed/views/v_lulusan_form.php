<style type="text/css">
	.ui-autocomplete {
	  	z-index: 215000000 !important;
	}
</style>

<div class="container">
    <div class="row">
      	<div class="span12">      		
      		<div class="widget ">
      			<div class="widget-header">
      				<a 
	            		href="<?= base_url('sync_feed/lulusan/view_lulusan') ?>" 
	            		class="btn btn-default" 
	            		style="margin-left: 10px"
	            		data-toggle="tooltip"
	            		title="kembali">
	                	<i class="icon-chevron-left" style="margin-left: 0"></i>
	                </a>
      				<h3>Lulusan Form</h3>
  				</div>
				
				<div class="widget-content">
					<form 
						id="edit-profile" 
						method="POST" 
						action="<?= base_url(); ?>sync_feed/lulusan/save" 
						class="form-horizontal">

						<fieldset>
							<div class="control-group">											
								<label class="control-label">Tahun Akademik Lulus</label>
								<div class="controls">
									<input 
										type="text" 
										class="span3"
										value="<?= get_thajar($this->session->userdata('tahun')); ?>"  
										readonly />
									<input type="hidden" name="ta" id="ta" value="<?= $this->session->userdata('tahun');?>">
								</div>
							</div>

							<div class="control-group">											
								<label class="control-label">NPM*</label>
								<div class="controls">
									<input 
										type="text" 
										class="span6" 
										name="mhs" 
										id="mhs" 
										placeholder="Nomor Induk Mahasiswa" 
										value=""  
										required="">
									<input type="hidden" class="span6" name="npm" value=""  required>
								</div>
							</div>

							<div class="control-group">											
								<label class="control-label">Tanggal Lulus*</label>
								<div class="controls">
									<input 
										type="text" 
										class="span6" 
										name="tgl_lulus" 
										id="tgl_lulus" 
										placeholder="Masukan Tanggal Lulus Mahasiswa" 
										value=""  
										required>
								</div>
							</div>

							<div class="control-group">											
								<label class="control-label">Nomor S.K. Yudisium*</label>
								<div class="controls">
									<input 
										type="text" 
										class="span6" 
										name="sk_yudi" 
										placeholder="Masukan Nomor S.k. Yudisium" 
										value=""  
										>
								</div>
							</div>

							<div class="control-group">											
								<label class="control-label">Tanggal S.K.*</label>
								<div class="controls">
									<input 
										type="text" 
										class="span6" 
										name="tgl_yudi" 
										id="tgl_yudi" 
										placeholder="Masukan Tanggal S.K. Yudisium" 
										value=""  
										>
									<p class="help-block">*Masukan Tanggal yang Tertera Pada S.K. Yudisium</p>
								</div>
							</div>

							<div class="control-group">											
								<label class="control-label">Total SKS*</label>
								<div class="controls">
									<input 
										type="text" 
										class="span6" 
										name="sks" 
										placeholder="Masukan Total SKS" 
										value=""   
										onkeyup="this.value=this.value.replace(/[^0-9]/g,'');" 
										>
								</div>
							</div>

							<div class="control-group">											
								<label class="control-label">IPK*</label>
								<div class="controls">
									<input 
										type="number" 
										step=".01" 
										class="span6" 
										name="ipk" 
										placeholder="Masukan IPK Mahasiswa" 
										>
								</div>
							</div>

							<div class="control-group">											
								<label class="control-label">Nomor Ijazah*</label>
								<div class="controls">
									<input 
										type="text" 
										class="span6" 
										name="no_ijazah" 
										placeholder="Masukan Nomor Ijazah" 
										value=""  
										>
								</div>
							</div>

							<div class="control-group">											
								<label class="control-label">Judul Skripsi*</label>
								<div class="controls">
									<textarea class="span6" name="jdl_skripsi"></textarea>
								</div>
							</div>

							<div class="control-group">											
								<label class="control-label">Dosen Pembimbing*</label>
								<div class="controls">
									<input 
										type="text" 
										class="span3" 
										name="dospem1" 
										id="dospem1" 
										placeholder="Dosen Pembimbing 1" 
										value=""  
										>
									<input type="hidden" name="nidn1" id="nidn1" value="" />

									<input 
										type="text" 
										class="span3" 
										name="dospem2" 
										id="dospem2" 
										placeholder="Dosen Pembimbing 2" 
										value=""  
										>
									<input type="hidden" class="span3" name="nidn2" id="nidn2"  value=""  >
								</div>
							</div>

							<div class="control-group">											
								<label class="control-label">Masa Bimbingan*</label>
								<div class="controls">
									<input 
										type="text" 
										class="span3" 
										name="mulai_bim" 
										id="mulai_bim" 
										placeholder="Masukan tanggal mulai bimbingan" 
										value=""  
										>

									<input 
										type="text" 
										class="span3" 
										name="ahir_bim"  
										id="ahir_bim" 
										placeholder="Masukan tanggal akhir bimbingan" 
										value=""  
										>
								</div>
							</div>
		
							<div class="form-actions">
								<button type="submit" class="btn btn-primary">Simpan</button> 
								<a href="<?= base_url();?>sync_feed/lulusan/view_lulusan" class="btn btn-default">
									Kembali
								</a>
							</div>
						</fieldset>
					</form>
				</div>
			</div>
	    </div>
    </div>
</div>

<script type="text/javascript">
	$(document).ready(function($) {
	    $('input[name^=mhs]').autocomplete({
	        source: '<?= base_url('sync_feed/lulusan/load_mhs_autocomplete');?>',
	        minLength: 4,
	        select: function (evt, ui) {
	            this.form.mhs.value = ui.item.value;
	            this.form.npm.value = ui.item.npm;
	        }
	    });

	    $('input[name^=dospem1]').autocomplete({
	        source: '<?= base_url('sync_feed/lulusan/load_kry_autocomplete');?>',
	        minLength: 2,
	        select: function (evt, ui) {
	            this.form.dospem1.value = ui.item.value;
	            this.form.nidn1.value = ui.item.nidn;
	        }
	    });

	    $('input[name^=dospem2]').autocomplete({
	        source: '<?= base_url('sync_feed/lulusan/load_kry_autocomplete');?>',
	        minLength: 2,
	        select: function (evt, ui) {
	            this.form.dospem2.value = ui.item.value;
	            this.form.nidn2.value = ui.item.nidn;
	        }
	    });

	    $('#tgl_lulus').datepicker({
			dateFormat: "yy-mm-dd",
			yearRange: "2016:2020",
			changeMonth: true,
			changeYear: true
		});

		$('#tgl_yudi,#mulai_bim,#ahir_bim').datepicker({
			dateFormat: "yy-mm-dd",
			yearRange: "2000:2020",
			changeMonth: true,
			changeYear: true
		});
	});
</script>