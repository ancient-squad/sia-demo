<?php

//ob_start();
$pdf = new FPDF("P","mm", "A4");
date_default_timezone_set('Asia/Jakarta'); 

if($que->jabfung=='TPD'){
	$fung='Tenaga Pendidik';
} elseif($que->jabfung=='SAH'){
	$fung='Asisten Ahli';
} elseif($que->jabfung=='LKT'){
	$fung='Lektor';
} elseif($que->jabfung=='LKK'){
	$fung='Lektor Kepala';
} elseif($que->jabfung=='BIG'){
	$fung='Guru Besar';
}

if($quo->struktural==1){
	$stat= 'Dosen Struktural';
} else {
	if($quo->homebase==1){
		$stat= 'Dosen Homebase';
	} else{
		if($quo->tetap==1){
			$stat= 'Dosen Tetap';
		} else {
			$stat= 'Dosen Tidak Tetap';
		}
	}
}



$pdf->AliasNbPages();
$pdf->AddPage();
$pdf->SetMargins(5, 10 ,5);

$pdf->SetFont('Arial','',8); 

$pdf->setY(10);
$pdf->Cell(50,5,'UNIVERSITAS BHAYANGKARA',0,0,'C');
$pdf->setx(150);
$pdf->Cell(50,5,'Keterangan : '.$stat,0,1,'L');
$pdf->Cell(50,5,'JAKARTA RAYA',0,0,'C');
$pdf->Line(9,20,50,20);



$pdf->setY(35);
$pdf->SetFont('Arial','B',14); 
$pdf->Cell(200,5,'BIODATA DOSEN',0,0,'C');



$pdf->ln();
$pdf->ln();
$pdf->ln();


$pdf->SetFont('Arial','',12);
$pdf->Cell(5,7,'',0,0,'L');
$pdf->Cell(8,7,'1.',0,0,'L');
$pdf->Cell(60,7,'Nama Lengkap',0,0,'L');
$pdf->Cell(3,7,':',0,0,'C');
$pdf->Cell(75.60,7,$quo->nama,0,1,'L');

if (count($que->nid) > 0) {
	$pdf->image(base_url().$que->foto,158,50,40);
} else {
	// $pdf->image(base_url().'upload/nopict.jpg',158,50,40);
}

$pdf->SetFont('Arial','',12);
$pdf->Cell(5,7,'',0,0,'L');
$pdf->Cell(8,7,'2.',0,0,'L');
$pdf->Cell(60,7,'NID',0,0,'L');
$pdf->Cell(3,7,':',0,0,'C');
$pdf->Cell(75.60,7,$quo->nid,0,1,'L');


$pdf->Cell(5,7,'',0,0,'L');
$pdf->Cell(8,7,'3.',0,0,'L');
$pdf->Cell(60,7,'NIDN',0,0,'L');
$pdf->Cell(3,7,':',0,0,'C');
$pdf->Cell(75.60,7,$quo->nidn,0,1,'L');


$pdf->Cell(5,7,'',0,0,'L');
$pdf->Cell(8,7,'4.',0,0,'L');
$pdf->Cell(60,7,'Alamat',0,0,'L');
$pdf->Cell(3,7,':',0,0,'C');
$pdf->multiCell(75.60,7,$que->alamat,0,1);


$pdf->Cell(5,7,'',0,0,'L');
$pdf->Cell(8,7,'5.',0,0,'L');
$pdf->Cell(60,7,'Telepon',0,0,'L');
$pdf->Cell(3,7,':',0,0,'C');
$pdf->Cell(75.60,7, $que->tlp,0,1);


$pdf->Cell(5,7,'',0,0,'L');
$pdf->Cell(8,7,'6.',0,0,'L');
$pdf->Cell(60,7,'E mail',0,0,'L');
$pdf->Cell(3,7,':',0,0,'C');
$pdf->Cell(75.60,7, $que->email,0,1);


$pdf->Cell(5,7,'',0,0,'L');
$pdf->Cell(8,7,'7.',0,0,'L');
$pdf->Cell(60,7,'Tempat Lahir',0,0,'L');
$pdf->Cell(3,7,':',0,0,'C');
$pdf->Cell(75.60,7, $que->tpt_lahir,0,1);


$pdf->Cell(5,7,'',0,0,'L');
$pdf->Cell(8,7,'8.',0,0,'L');
$pdf->Cell(60,7,'Tanggal Lahir',0,0,'L');
$pdf->Cell(3,7,':',0,0,'C');
$pdf->Cell(75.60,7, dateIdn($que->tgl_lahir),0,1);


$pdf->Cell(5,7,'',0,0,'L');
$pdf->Cell(8,7,'9.',0,0,'L');
$pdf->Cell(60,7,'Jabatan Fungsional',0,0,'L');
$pdf->Cell(3,7,':',0,0,'C');
$pdf->Cell(75.60,7, $que->thnj.' - '.$fung,0,1);


$pdf->Cell(5,7,'',0,0,'L');
$pdf->Cell(8,7,'10.',0,0,'L');
$pdf->Cell(60,7,'Riwayat Pendidikan',0,0,'L');
$pdf->Cell(3,7,':',0,1,'C');

foreach ($pdd as $pd) {
$pdf->Cell(5,7,'',0,0,'L');
$pdf->Cell(8,7,'',0,0,'L');
$pdf->Cell(60,7,'',0,0,'L');
$pdf->Cell(3,7,'',0,0,'C');
$pdf->Cell(3,7,'-',0,0,'C');
$pdf->Cell(119,7, $pd->tahun_masuk.' - '.$pd->tahun_keluar .', '. $pd->jenjang .$pd->instansi.'('.$pd->jurusan.')',0,1);
}


$pdf->Cell(5,7,'',0,0,'L');
$pdf->Cell(8,7,'11.',0,0,'L');
$pdf->Cell(60,7,'Riwayat Pekerjaan',0,0,'L');
$pdf->Cell(3,7,':',0,1,'C');

$arra = explode(',', $que->pekerjaan);
for ($i=0; $i < count($arra)-1; $i++) {
$pdf->Cell(5,7,'',0,0,'L');
$pdf->Cell(8,7,'',0,0,'L');
$pdf->Cell(60,7,'',0,0,'L');
$pdf->Cell(3,7,'',0,0,'C');
$pdf->Cell(3,7,'-',0,0,'C');
$pdf->Cell(75.60,7, $arra[$i],0,1);
}


$pdf->Cell(5,7,'',0,0,'L');
$pdf->Cell(8,7,'12.',0,0,'L');
$pdf->Cell(60,7,'Riwayat Penelitian',0,0,'L');
$pdf->Cell(3,7,':',0,1,'C');

foreach ($rsc as $val) { 
$pdf->Cell(5,7,'',0,0,'L');
$pdf->Cell(8,7,'',0,0,'L');
$pdf->Cell(60,7,'',0,0,'L');
$pdf->Cell(3,7,'',0,0,'C');
$pdf->Cell(3,7,'-',0,0,'C');
$pdf->Cell(75.60,7, $val->tahun.' - '.$val->judul .'(Penelitian Tingkat  '.tridarma('rsc',$val->jenis).', Sebagai '.tridarma('rsc',$val->posisi).', Sumber Dana '.tridarma('rsc',$val->biaya).')',0,1);
}


$pdf->Cell(5,7,'',0,0,'L');
$pdf->Cell(8,7,'13.',0,0,'L');
$pdf->Cell(60,7,'Riwayat Pengabdian',0,0,'L');
$pdf->Cell(3,7,':',0,1,'C');

foreach ($dev as $ops) { 
$pdf->Cell(5,7,'',0,0,'L');
$pdf->Cell(8,7,'',0,0,'L');
$pdf->Cell(60,7,'',0,0,'L');
$pdf->Cell(3,7,'',0,0,'C');
$pdf->Cell(3,7,'-',0,0,'C');
$pdf->Cell(75.60,7, $ops->tahun.' - '.$ops->judul .'(Penelitian Tingkat  '.tridarma('rsc',$ops->jenis).', Sebagai '.tridarma('rsc',$ops->posisi).', Sumber Dana '.tridarma('rsc',$ops->dana).')',0,1);
}


$pdf->Cell(5,7,'',0,0,'L');
$pdf->Cell(8,7,'14.',0,0,'L');
$pdf->Cell(60,7,'Riwayat Publikasi',0,0,'L');
$pdf->Cell(3,7,':',0,1,'C');

foreach ($pub as $ups) { 
$pdf->Cell(5,7,'',0,0,'L');
$pdf->Cell(8,7,'',0,0,'L');
$pdf->Cell(60,7,'',0,0,'L');
$pdf->Cell(3,7,'',0,0,'C');
$pdf->Cell(3,7,'-',0,0,'C');
$pdf->Cell(75.60,7, $ups->tahun.' - '.$ops->judul .'(Penelitian Tingkat  '.$ups->tingkat.', yang dipublikasikan pada jurnal '.tridarma('rsc',$ops->jurnal).')',0,1);
}


$pdf->setY(-30);
$pdf->SetFont('Arial','',6);
$pdf->Cell(210,8,'copyright - '.$this->ORG_NAME,0,0,'C');



//exit();
$pdf->Output('DRH-'.date('ymd_his').'.PDF','I');

?>

