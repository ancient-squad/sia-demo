<script type="text/javascript">
	var x = 0; /* Set Global Variable i */
	function increment(){
		x += 1; /* Function for automatic increment of field's "Name" attribute. */
	}

	function removeElement(parentDiv, childDiv){
		if (childDiv == parentDiv){
			alert("The parent div cannot be removed.");
		}
		else if (document.getElementById(parentDiv)){
			var child = document.getElementById(childDiv);
			var parent = document.getElementById(parentDiv);
			parent.removeChild(child);
		}
		else{
			alert("Child div has already been removed or does not exist.");
			return false;
		}
	}

	function addFields(){
		var kerjaan    = document.getElementById("kerja").value;
		var contain    = document.createElement("div");
		var subcontain = document.createElement("INPUT");
		var btn        = document.createElement("button");
		var icon       = document.createElement("i");

	    increment();
	    
		contain.setAttribute("class", "controls");
		contain.setAttribute("id", "work_"+x)

		subcontain.setAttribute("type", "text");
		subcontain.setAttribute("value", kerjaan);
		subcontain.setAttribute("name", "work[]");
		subcontain.setAttribute("class", "span3");
		subcontain.setAttribute("readonly", "");

		btn.setAttribute("class", "btn btn-default");
		btn.setAttribute("style", "float:left");
		btn.setAttribute("onClick", "removeElement('xx', 'work_"+x+"')");
		btn.setAttribute("type", "button");

		icon.setAttribute("class", "icon icon-remove");
		btn.appendChild(icon);

	    document.getElementById("xx").appendChild(contain);
	    document.getElementById("work_"+x).appendChild(subcontain);
	    document.getElementById("work_"+x).appendChild(btn);
	    document.getElementById("work_"+x).appendChild(document.createElement("br"));
	}

	function addFieldsdidik(){
	    var didikan = document.getElementById("didik").value;
	    var konten = document.createElement("div");
	    var subkonten = document.createElement("INPUT");
	    var btn = document.createElement("button");
	    var icon = document.createElement("i");

	    increment();

	    konten.setAttribute("class", "controls");
	    konten.setAttribute("id", "edu"+x);

	    subkonten.setAttribute("type", "text");
		subkonten.setAttribute("value", didikan);
		subkonten.setAttribute("name", "didik[]");
		subkonten.setAttribute("class", "span3");
		subkonten.setAttribute("readonly", "");

		btn.setAttribute("class", "btn btn-default");
		btn.setAttribute("onClick", "removeElement('ww','edu" + x +"')");
		btn.setAttribute("type", "button");
		btn.setAttribute("style", "float:left");

		icon.setAttribute("class", "icon icon-remove");
		btn.appendChild(icon);
		
	    document.getElementById("ww").appendChild(konten);
	    document.getElementById("edu"+x).appendChild(subkonten);
	    document.getElementById("edu"+x).appendChild(btn);
	    document.getElementById("edu"+x).appendChild(document.createElement("br"));
	    
	}
</script>

<script language="javascript">
	function numeric(e, decimal) { 
		var key;
		var keychar;
		if (window.event) {
	 		key = window.event.keyCode;
		} else if (e) {
			key = e.which;
		} else return true;
		
		keychar = String.fromCharCode(key);
		if ((key==null) || (key==0) || (key==8) ||  (key==9) || (key==13) || (key==27) ) {
			return true;
		} else 
		if ((("0123456789").indexOf(keychar) > -1)) {
			return true; 
		} else 
		if (decimal && (keychar == ".")) {
			return true; 
		} else return false; 
	}
</script> 

<script>
	$(document).ready(function() {
		$.get('<?= base_url('sync_feed/biodata/countCreditsTotal') ?>', function(response) {
			$('#creditTotal').html(response)
		})
	})
</script>


<div class="container">
	
    <div class="row">
      	
      	<div class="span12">      		
      		
      		<div class="widget ">
      			
      			<div class="widget-header">
      				<i class="icon-user"></i>
      				<h3>BIODATA DIRI</h3>
  				</div> <!-- /widget-header -->
				
				<div class="widget-content">
			  		<form 
			  			id="edit-profile" 
			  			class="form-horizontal" 
			  			method="post" 
			  			action="<?= base_url();?>sync_feed/biodata/tambah_biodosen" 
			  			enctype="multipart/form-data">
						<fieldset>

							<div class="control-group">											
								<label class="control-label">NID</label>
								<div class="controls">
									<input 
										type="text" 
										class="span6" 
										name="nid" 
										placeholder="NID" 
										value="<?= $dets->nid; ?>" 
										required 
										readonly/>
								</div>
							</div>

							<div class="control-group">											
								<label class="control-label">NIDN</label>
								<div class="controls">
									<input 
										type="text" 
										class="span6" 
										name="nidn" 
										placeholder="NIDN" 
										value="<?= $dets->nidn; ?>" 
										required 
										readonly/>
								</div>
							</div>

							<div class="control-group">											
								<label class="control-label">Nama*</label>
								<div class="controls">
									<input 
										type="text" 
										class="span6" 
										name="nama" 
										placeholder="Nama Dosen" 
										value="<?= $dets->nama; ?>" 
										minlength=16 
										required 
										readonly/>
									<p class="help-block">Perubahan Nama Dapat Diajukan Melalui Biro Administrasi Akademik</p>
								</div>
							</div>

							<div class="control-group">											
								<label class="control-label">Alamat*</label>
								<div class="controls">
									<textarea type="text" class="span6" name="alamat" required><?= $detil->alamat; ?></textarea>
								</div>
							</div>

							<div class="control-group">											
								<label class="control-label">Telepon*</label>
								<div class="controls">
									<input 
										type="text" 
										class="span6" 
										onkeypress="return numeric(event, false)" 
										name="tlp" 
										placeholder="Nomor Telepon" 
										value="<?= $detil->tlp; ?>" 
										required/>
								</div>
							</div>

							<div class="control-group">											
								<label class="control-label">E-mail*</label>
								<div class="controls">
									<input 
										type="email" 
										class="span6" 
										name="email" 
										placeholder="E-mail aktif dosen" 
										value="<?= $detil->email; ?>" 
										required/>
								</div>	
							</div>

							<div class="control-group">											
								<label class="control-label">Tempat Lahir*</label>
								<div class="controls">
									<input 
										type="text" 
										class="span6" 
										name="tempatlahir" 
										placeholder="Tempat Lahir Sesuai KTP" 
										value="<?= $detil->tpt_lahir; ?>" 
										required/>
								</div> 
							</div>

							<script>
				                $(function() {
				                    $( "#tgl" ).datepicker({
				                        changeMonth: true,
				                        changeYear: true,
				                        yearRange: "-70:+0",
				                    });
				                });
				            </script>

							<div class="control-group">											
								<label class="control-label">Tanggal Lahir*</label>
								<div class="controls">
									<input 
										type="text" 
										class="span6" 
										id="tgl" 
										name="tgl" 
										placeholder="Tanggal Lahir Sesuai KTP" 
										value="<?= $detil->tgl_lahir; ?>" 
										required/>
								</div>	
							</div>	
							
							<div class="control-group">											
								<label class="control-label">Jabatan Fungsional*</label>
			  					<div class="controls">
									<select name="jabfung" class="span6" required>
									
										<option selected disabled>--Pilih Jabatan--</option>	
										
										<option <?= $detil->jabfung=='TPD' ? 'selected="selected"' : NULL; ?> value="TPD">Tenaga Pendidik</option>
										
										<option <?= $detil->jabfung=='SAH' ? 'selected="selected"' : NULL; ?> value="SAH">Asisten Ahli</option>
										
										<option <?= $detil->jabfung=='LKT' ? 'selected="selected"' : NULL; ?> value="LKT">Lektor</option>
										
										<option <?= $detil->jabfung=='LKK' ? 'selected="selected"' : NULL; ?> value="LKK">Lektor Kepala</option>
										
										<option <?= $detil->jabfung=='BIG' ? 'selected="selected"' : NULL; ?> value="BIG">Guru Besar</option>
										
									</select>
								</div>
							</div>
							
							<div class="control-group">											
								<label class="control-label">Tahun Jabatan*</label>
								<div class="controls">
									<input 
										type="text" 
										class="span6" 
										onkeypress="return numeric(event, false)" 
										maxlength="4" 
										name="thnj" 
										placeholder="Tahun Jabatan Fungsional" 
										value="<?= $detil->thnj; ?>" 
										required/>
								</div> 
							</div>
					
<!-- ==================================================== PENDIDIKAN ==================================================== -->	
							<script type="text/javascript">
								function delRow(id) {
									if (confirm('Anda ingin menghapus data ini ?')) {
										$.ajax({
								            type: 'POST',
								            url: '<?= base_url('sync_feed/biodata/delRows/');?>'+id,
								            error: function (xhr, ajaxOptions, thrownError) {
								                return false;           
								            },
								            success: function () {
								                resetedu(id);
								            }
								        });
									}
							    }  

								function resetedu(id) {
									$("#shwdiv_"+id).remove();
							    }

								$(document).ready(function (){

									var d = new Date();
    								var n = d.getFullYear();

									// jika tahun bukan angka
									$("#stryear,#endyear").keyup(function (){
										if (isNaN($(this).val())) {
											$(this).val('');
										}
									});	

									// jika input tahun melebihi tahun aktif
									$("#endyear").blur(function (){
										if ($(this).val() > n) {
											alert('Input tahun tidak boleh melebihi tahun ini!');
											$(this).val(''); 
										}	
									});							

								});

								var count = 1;

								function addjenj() 
								{
									var inst = $('#ins').val();

									var strt = $('#stryear').val();

									var endd = $('#endyear').val();

									var cont = $('#kons').val();

									// alert jika kolom dikosongkan
									if (inst === '' || strt === '' || endd === '') {
										alert('Kolom tidak boleh dikosongkan!'); return;
									}

									count++;

									var newRows = $("#showYourDiv");

									var rows = "";
									
									rows += '<div class="controls" id="mydiv_'+count+'">';

									rows += '<input type="text" class="form-control" id="" style="margin-right:3px;" name="inst[]" value="'+inst+'" required="" readonly/>';

									rows += '<input type="text" class="form-control" id="" style="margin-right:3px;" name="konst[]" value="'+cont+'" required="" readonly/>';

									rows += '<input type="text" class="form-control span2" id="" style="margin-right:3px;" name="str[]" value="'+strt+'" required="" readonly/>';

									rows += '<input type="text" class="form-control span2" id="" style="margin-right:4px;" name="end[]" value="'+endd+'" required="" readonly/>';

									rows += '<a class="btn btn-danger" onclick="delsjenj('+count+')"><i class="icon icon-remove"></i> </a>';

									rows += '</div>';

									rows += '<br>';

									newRows.append(rows);

									$('#ins,#kons,#stryear,#endyear').val('');

								}

								function delsjenj(id)
								{
									$("#mydiv_"+id).remove();
								}

							</script>

							<div class="control-group">											
								<label class="control-label">Riwayat Pendidikan*</label>
			  					<div class="form-horizontal">
			  						<div id="showYourDiv">

			  						</div>

			  						<?php foreach ($pdd as $lue) { ?>
			  							
			  							<div class="controls" id="shwdiv_<?= $lue->id_pddkn; ?>">

					  						<input 
					  							type="text" 
					  							class="form-control" 
					  							value="<?= $lue->instansi; ?>" 
					  							disabled="">

					  						<input 
					  							type="text" 
					  							class="form-control" 
					  							value="<?= $lue->jurusan; ?>" 
					  							disabled="">

					  						<input 
					  							type="text" 
					  							class="form-control span2" 
					  							value="<?= $lue->tahun_masuk; ?>" 
					  							disabled="">

					  						<input 
					  							type="text" 
					  							class="form-control span2" 
					  							value="<?= $lue->tahun_keluar; ?>" 
					  							disabled="">

					  						<a 
					  							href="javascript:void(0);" 
					  							class="btn btn-danger" 
					  							onclick="delRow('<?= $lue->id_pddkn; ?>')">
					  							<i class="icon icon-remove"></i>
					  						</a>
				  						</div>
				  						<br>

			  						<?php } ?>

			  						<div class="controls">

				  						<input 
				  							type="text" 
				  							class="form-control" 
				  							id="ins" 
				  							placeholder="Nama Instansi" 
				  							value="" />

				  						<input 
					  						type="text" 
					  						class="form-control" 
					  						id="kons" 
					  						placeholder="Konsentrasi" 
					  						value="" />

				  						<input 
				  							type="text" 
				  							class="form-control span2"
				  							id="stryear" 
				  							maxlength="4" 
				  							minlength="4" 
				  							placeholder="Tahun Mulai" 
				  							value="" />

				  						<input 
					  						type="text" 
					  						class="form-control span2" 
					  						id="endyear" 
					  						maxlength="4" 
					  						minlength="4" 
					  						placeholder="Tahun Selesai" 
					  						value="" />

				  						<a 
				  							href="javascript:void(0);" 
				  							class="btn btn-success" 
				  							onclick="addjenj()">
				  							<i class="icon icon-plus"></i> 
				  						</a>

			  						</div>
			  					</div>
							</div>
<!-- =============================================== pendidikan end ===================================================== -->

							<div class="control-group">											
								<label class="control-label">Beban Mengajar</label>
								<div class="controls">
									<a 
										href="javascript:;" 
										class="btn btn-success"><h4 id="creditTotal">
											loading...
										</h4>
									</a>
									<a href="<?= base_url(); ?>sync_feed/biodata/detil_beban" class="">Lihat Detil</a>
								</div> 				
							</div>	

							<div class="control-group">											
								<label class="control-label">Lihat Profil</label>
								<div class="controls">
									<a href="<?= base_url(); ?>sync_feed/biodata/profil" class="btn btn-primary"><i class="icon icon-eye-open"></i> Lihat Profil</a>
								</div> 
							</div>					
							
								
							<div class="form-actions">
								<button type="submit" class="btn btn-primary">Save</button> 
								<button class="btn">Cancel</button>
							</div>

						</fieldset>
					</form>
				</div>
			</div>
	    </div>
    </div>
</div>