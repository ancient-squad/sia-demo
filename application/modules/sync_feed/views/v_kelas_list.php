<style type="text/css">
	#circle { 
		width: 20px; 
		height: 20px; 
		background: #FFB200; 
		-moz-border-radius: 50px; 
		-webkit-border-radius: 50px; 
		border-radius: 50px; 
	}

	.distance {
		margin-right: 5px;
	}
</style>

<link rel="stylesheet" href="<?= base_url();?>assets/js/jquery-ui/js/jquery.ui.autocomplete.css">
<link rel="stylesheet" href="<?= base_url();?>assets/js/timepicker/jquery.ui.timepicker.css">
<script src="<?= base_url();?>assets/js/jquery-ui/js/jquery.ui.autocomplete.js"></script>
<script src="<?= base_url();?>assets/js/timepicker/jquery.ui.timepicker.js"></script>

<script type="text/javascript">
	function member(id)
	{
		$('#members').load('<?= base_url('sync_feed/kelas/jmlPeserta/') ?>'+id);
	}

	function edit(idk){
		$('#form_edit').load('<?= base_url();?>sync_feed/kelas/edit_jadwal/'+idk);
	}

	function dosen(idk){
		$('#edit1').load('<?= base_url();?>sync_feed/kelas/load_dosen/'+idk);
	}
</script>

<script type="text/javascript">
	jQuery(document).ready(function($) {
		$('input[name^=dosen]').autocomplete({
	        source: '<?= base_url('perkuliahan/jdl_kuliah/getdosen');?>',
	        minLength: 1,
	        select: function (evt, ui) {
	            this.form.nik.value = ui.item.nik;
	            this.form.dosen.value = ui.item.value;
	        }
	    });
	});
</script>

<div class="modal fade" id="edit_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content" id="form_edit">

        </div>
    </div>
</div>

<div class="row">
	<div class="span12">      		  		
  		<div class="widget ">
  			<div class="widget-header">
  				<i class="icon-file-text-alt"></i>
  				<h3>Data Jadwal Perkuliahan Feeder <?= get_thnajar($this->session->userdata('tahunajaran_kelas')['tahunajaran']) ?></h3>
  				<a 
  					href="<?= base_url('feeder/kelas') ?>" 
  					class="btn btn-default pull-right" 
  					style="margin-top: 7px; margin-right: 10px">
  					<i class="icon-eye-open" style="margin-left: 0 !important"></i>&nbsp; Lihat Data Kelas Feeder
  				</a>
			</div>

			<div class="widget-content" style="padding:30px;">
            	<div class="tabbable">

            		<a href="<?=  base_url('sync_feed/kelas') ?>" class="btn btn-warning">
            			<i class="icon-chevron-left"></i> Kembali
            		</a>
            		
                    <?php if ($this->session->userdata('tahunajaran_kelas')['tahunajaran'] == getactyear()) { ?>

                    	<a 
                    		href="<?= base_url('sync_feed/kelas/sync_kelas') ?>" 
                    		class="btn btn-success" 
                    		target="blank"><i class="icon-refresh"></i> Sinkronisasi Kelas
                    	</a>

                    	<a 
                    		href="<?= base_url('sync_feed/kelas/sync_ajar_dosen') ?>" 
                    		class="btn btn-success" 
                    		target="blank">
                    		<i class="icon-refresh"></i> Sinkronisasi Ajar Dosen
                    	</a>

                    <?php } elseif ($this->session->userdata('tahunajaran_kelas')['tahunajaran'] == yearBefore()) { ?>

                    	<a 
                    		href="<?= base_url('sync_feed/kelas/sync_ajar_dosen') ?>" 
                    		class="btn btn-success" 
                    		target="blank">
                    		<i class="icon-refresh"></i> Sinkronisasi Ajar Dosen
                    	</a>

                    <?php } ?>
                    <!-- <a 
                    	href="<?= base_url(); ?>sync_feed/kelas/cetak_jadwal_all" 
                    	class="btn btn-success distance pull-right">
                    	<i class="icon-print"></i> Print Jadwal
                    </a> -->
                    <a data-toggle="modal" class="btn btn-success distance" href="#myModal">
                    	<i class="btn-icon-only icon-plus"></i> Tambah Data
                    </a>
					<!-- end list button -->
					<hr>

		            <ul class="nav nav-tabs">
						<!-- setup tab -->
		            	<?php foreach ($tabs as $tab) {
	            			if ($tab->semester_kd_matakuliah == 1) { ?>
		            			<li class="active"><a href="#tab<?= $tab->semester_kd_matakuliah ?>" data-toggle="tab">
		            				Semester <?= $tab->semester_kd_matakuliah ?></a>
		            			</li>
	            			<?php } else { ?>
								<li><a href="#tab<?= $tab->semester_kd_matakuliah ?>" data-toggle="tab">
		            				Semester <?= $tab->semester_kd_matakuliah ?></a>
		            			</li>
	            			<?php }
	            		} ?>
						<!-- /setup tab -->
		            </ul>
      
              		<div class="tab-content">

		            <?php foreach ($tabs as $cotab) {
		            	// setup tab content
		            	$mk_by_smt = $this->feedkuliah->jdl_matkul_semester($cotab->semester_kd_matakuliah);
		            	
	            		if ($cotab->semester_kd_matakuliah == 1) { ?>
	            			<div class="tab-pane active" id="tab<?= $cotab->semester_kd_matakuliah ?>">
	            		<?php } else { ?>
	            			<div class="tab-pane" id="tab<?= $cotab->semester_kd_matakuliah ?>">
	            		<?php } ?>

							<table id="<?= 'example'.$cotab->semester_kd_matakuliah; ?>" class="table table-striped">
			                	<thead>
			                        <tr> 
			                        	<th>No</th>
										<th>Kelas</th>
			                            <th>Kode MK</th>
			                            <th>Matakuliah</th>
										<th>SKS</th>
										<th>Peserta</th>
										<th>Dosen</th>
										<th>NID</th>
										<th>NIDN</th>
										<th>NUPN</th>
			                            <th width="160">Aksi</th>
			                        </tr>
			                    </thead>
			                    <tbody>
		                    	<?php $no=1; foreach ($mk_by_smt as $cotab)  { 

	                    			if (($cotab->nidn == '' || is_null($cotab->nidn)) && ($cotab->nupn == '' || is_null($cotab->nupn))) {
	                    				$warna ='style="background:#FF6347;"';
	                    			} elseif ($cotab->status_feeder == 1) {
	                    				$warna = 'style="background:#90EE90;"';
	                    			} else {
	                    				$warna ='';
	                    			} ?>
			                    		
			                    	<tr >
		                           		<td <?= $warna; ?> ><?= $no; ?></td>
			                        	<td <?= $warna; ?>><?= $cotab->kelas; ?></td>
			                        	<td <?= $warna; ?>><?= $cotab->kd_matakuliah; ?></td>
			                        	<td <?= $warna; ?>><?= $cotab->nama_matakuliah; ?></td>
			                        	<td <?= $warna; ?>><?= $cotab->sks_matakuliah; ?></td>
			                        	<td <?= $warna; ?>>
			                        		<span data-toggle="tooltip" title="lihat jumlah peserta">
				                        		<a 
				                        			href="#pesertaModal" 
				                        			data-toggle="modal" 
				                        			class="btn btn-success" 
				                        			onclick="member(<?= $cotab->id_jadwal; ?>)">
				                        			<i class="icon icon-eye-open"></i>
				                        		</a>
			                        		</span>
			                        	</td>
			                        	<td <?= $warna; ?>><?= $cotab->nama; ?></td>
			                        	<td <?= $warna; ?>><?= $cotab->nid; ?></td>
			                        	<td <?= $warna; ?>><?= $cotab->nidn; ?></td>
			                        	<td <?= $warna; ?>><?= $cotab->nupn; ?></td>
			                        	<td <?= $warna; ?>>
			                        		<span data-toggle="tooltip" title="ubah dosen">
				                        		<a 
				                        			data-toggle="modal" 
				                        			onclick="dosen(<?= $cotab->id_jadwal;?>)" 
				                        			href="#editModal1"
				                        			class="btn btn-success btn-small" >
				                        			<i class="btn-icon-only icon-user"></i>
				                        		</a>
			                        		</span>
											
											<span data-toggle="tooltip" title="edit kelas">
			                                    <a 
			                                    	class="btn btn-primary btn-small"
			                                    	onclick="edit(<?= $cotab->id_jadwal;?>)" 
			                                    	data-toggle="modal" 
			                                    	href="#edit_modal" >
			                                    	<i class="btn-icon-only icon-pencil"></i>
			                                    </a>
											</span>

		                                    <a 
		                                    	target="_blank" 
		                                    	class="btn btn-warning btn-small" 
		                                    	data-toggle="tooltip" 
			                        			title="lihat detail kelas"
		                                    	href="<?= base_url(); ?>sync_feed/kelas/detail_mhs/<?= $cotab->id_jadwal; ?>" >
		                                    	<i class="btn-icon-only icon-ok"></i>
		                                    </a>

		                                <?php if ($this->session->userdata('tahunajaran_kelas')['tahunajaran'] == yearBefore()) { ?>

	                                    	<a 
	                                    		target="_blank" 
	                                    		class="btn btn-primary btn-small" 
	                                    		data-toggle="tooltip" 
			                        			title="sinkronisasi nilai perkuliahan"
	                                    		href="<?= base_url();?>sync_feed/kelas/sync_kelas_nilai/<?= $cotab->id_jadwal; ?>" >
	                                    		<i class="btn-icon-only icon-refresh"></i>
	                                    	</a>

	                                    <?php } elseif ($this->session->userdata('tahunajaran_kelas')['tahunajaran'] == getactyear()) { ?>

	                                    	<a 
	                                    		target="_blank" 
	                                    		data-toggle="tooltip" 
			                        			title="sinkronisasi peserta kelas"
	                                    		class="btn btn-danger btn-small" 
	                                    		href="<?= base_url();?>sync_feed/kelas/sync_kelas_krs/<?= $cotab->id_jadwal; ?>" >
	                                    		<i class="btn-icon-only icon-refresh"></i>
	                                    	</a>

	                                    <?php } ?>

										</td >
									</tr>
								<?php $no++; } ?>

			                    </tbody>
			               	</table>
            			</div>
            		<?php } ?>
              		</div>
				</div>
          	</div> <!-- /widget-content -->
        </div>
    </div>
</div>


<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">FORM TAMBAH DATA</h4>
            </div>

            <form class ='form-horizontal' action="<?= base_url();?>sync_feed/kelas/save_data" method="post">
                <div class="modal-body">

	                <script type="text/javascript">
	                	$(document).ready(function(){
							$('#semester').change(function(){
								$.post('<?= base_url();?>sync_feed/kelas/get_detail_matakuliah_by_semester_feeder/'+$(this).val(),{},function(get){
									$('#ala_ala').html(get);
								});
							});
						});
	                </script>
                
                </div>

                <input 
                	type='hidden' 
                	class="form-control" 
                	name="jurusan" 
                	value="<?= $this->session->userdata('id_jurusan_prasyarat'); ?>" 
                	readonly>

                <div class="control-group">
                  <label class="control-label">Semester</label>
                  <div class="controls">
                    <select id="semester" class="form-control" name="semester" required="">
                      <option>--Pilih Semester--</option>
                      <option value="1">1</option>
                      <option value="2">2</option>
                      <option value="3">3</option>
                      <option value="4">4</option>
                      <option value="5">5</option>
                      <option value="6">6</option>
                      <option value="7">7</option>
                      <option value="8">8</option>
                    </select>
                  </div>
                </div>

                <div class="control-group">
					<label class="control-label">Kelompok Kelas</label>
					<div class="controls">
						<input type="radio" name="st_kelas" value="PG" required=""> A &nbsp;&nbsp; 
						<input type="radio" name="st_kelas" value="SR" required=""> B &nbsp;&nbsp;
						<input type="radio" name="st_kelas" value="PK" required=""> C  &nbsp;&nbsp;
					</div>
				</div>

            	<div class="control-group">											
					<label class="control-label" for="kelas">Nama Kelas</label>
					<div class="controls">
						<input type="text" class="form-control" placeholder="Input Kelas" id="kelas" name="kelas" required>
					</div>
				</div>

				<script type="text/javascript">
					$(document).ready(function() {
						$('#ala_ala').change(function() {
							$.post('<?= base_url();?>perkuliahan/jdl_kuliah/get_kode_mk/'+$(this).val(),{},function(get){
								$('#kode_mk').val(get);
							});
						});
					});
				</script>

				<div class="control-group">											
					<label class="control-label" for="firstname">Nama Matakuliah</label>
					<div class="controls">
						<select class="form-control" name="id_matakuliah" id="ala_ala">
							<option>--Pilih MataKuliah--</option>
						</select>
					</div>
				</div>

				<div class="control-group">										
					<label class="control-label" for="kode_mk">Kode MK</label>
					<div class="controls">
						<input 
							type="text" 
							class="form-control" 
							name="kd_matakuliah" 
							readonly="" 
							placeholder="Kode Matakuliah" 
							id="kode_mk">
					</div>
				</div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <input type="submit" class="btn btn-primary" value="Save"/>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade" id="editModal1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content" id="edit1">
            
        </div>
    </div>
</div>

<div class="modal fade" id="pesertaModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content" id="members">         
        	
        </div>
    </div>
</div>