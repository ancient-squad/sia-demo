<div class="row">
	<div class="span12">      		  		
  		<div class="widget ">
  			<div class="widget-header">
  				<i class="icon-file-alt"></i>
  				<h3>Data AKM Mahasiswa</h3>
			</div>			
			<div class="widget-content">
				<div class="span11">
  				<form method="post" class="form-horizontal" action="<?= base_url(); ?>sync_feed/krs/set_filter">
            <fieldset>
              <?php if ($this->session->userdata('sess_login')['id_user_group'] == '26'): ?>
                <div class="control-group">
                  <label class="control-label">Prodi</label>
                  <div class="controls">
                    <select class="form-control span6" name="prodi">
                      <option disabled selected>--Pilih Prodi--</option>
                      <?php foreach ($prodi as $value): ?>
                        <option value="<?= $value->kd_prodi ?>"><?= $value->prodi ?></option>
                      <?php endforeach ?>
                    </select>
                  </div>
                </div> 
              <?php endif ?>
              <?php $init_year = date('Y') - 8; ?>
              <div class="control-group">
                <label class="control-label">Angkatan</label>
                <div class="controls">
                  <select class="form-control span6" name="angkatan">
                    <option disabled selected>--Pilih Angkatan--</option>
                    <?php for ($i=$init_year; $i <= date('Y'); $i++) { ?>
                      <option value="<?= $i; ?>"><?= $i; ?></option>
                    <?php } ?>
                  </select>
                </div>
              </div> 
              <div class="control-group">
                <label class="control-label">Tahun Ajaran</label>
                <div class="controls">
                  <select class="form-control span6" name="tahunakademik">
                    <option disabled selected>--Pilih Tahun Ajaran--</option>
                    <?php foreach ($tahunakademik as $key) { ?>
                      <option value="<?= $key->kode ?>"><?= $key->tahun_akademik ?></option>
                    <?php } ?>
                  </select>
                </div>
              </div>               
              <div class="form-actions">
                <input type="submit" class="btn btn-large btn-success" value="Submit"/> 
              </div> <!-- /form-actions -->
            </fieldset>
          </form>
				</div>
			</div>
		</div>
	</div>
</div>
