<div class="row">
	<div class="span12">      		  		
  		<div class="widget ">
  			<div class="widget-header">
  				<i class="icon-file"></i>
  				<h3>Data Mahasiswa Aktif <?= get_thnajar($chooseyear); ?></h3>
			</div>
			
			<div class="widget-content">
				<form action="<?= base_url('sync_feed/krs/sync_status_aktif') ?>" method="post">
					<div class="span11">
						<a href="<?= base_url('sync_feed/krs') ?>" class="btn btn-warning">
							<i class="icon-chevron-left"></i> Kembali
						</a>
						<button type="submit" class="btn btn-primary">
							<i class="icon-refresh"></i> Sinkronisasi Mahasiswa Aktif
						</button>
						<button type="button" data-toggle="modal" data-target="#myModal" class="btn btn-default">
							<i class="icon-question-sign"></i> Petunjuk
						</button>
						<hr>
						<table id="example1" class="table table-bordered table-striped">
		                	<thead>
		                        <tr> 
		                        	<th>No</th>
		                        	<th>NIM</th>
		                        	<th>Nama Mahasiswa</th>
		                        	<th>Tahun Masuk</th>
		                        	<th width="80">SKS Semester</th>
		                        	<th width="100">Biaya Semester</th>
		                        </tr>
		                    </thead>
		                    <tbody>
		                    	<?php $no=1; foreach ($getData as $value) { ?>
		                        <tr>
		                        	<td><?= $no; ?></td>
		                        	<td>
		                        		<?= $value->NIMHSMSMHS; ?>
		                        		<input 
		                        			type="hidden" 
		                        			name="npm[<?= $value->NIMHSMSMHS; ?>]" 
		                        			value="<?= $value->NIMHSMSMHS; ?>" />
		                        	</td>
		                        	<td>
		                        		<?= $value->NMMHSMSMHS; ?>
		                        		<input 
		                        			type="hidden" 
		                        			name="nama[<?= $value->NIMHSMSMHS; ?>]" 
		                        			value="<?= $value->NMMHSMSMHS; ?>" />
		                        	</td>
	                                <td><?= $value->TAHUNMSMHS; ?></td>
	                                <td>
	                                	<a 
	                                		target="_blank" 
	                                		href="<?= base_url('sync_feed/krs/view_krs_dtl/'.$value->kd_krs); ?>">
	                                		<?= $value->sks; ?>
	                                	</a>
	                                	<input 
		                        			type="hidden" 
		                        			name="sks[<?= $value->NIMHSMSMHS; ?>]" 
		                        			value="<?= $value->sks; ?>" />
	                                </td>
	                                <td>
	                                	<?= 'Rp. '.format_harga_indo($value->biaya_semester) ?>
	                                	<input 
		                        			type="hidden" 
		                        			name="fee[<?= $value->NIMHSMSMHS; ?>]" 
		                        			value="<?= $value->biaya_semester; ?>" />
	                                </td>
		                        </tr>
		                        <?php $no++; } ?>
		                    </tbody>
		               	</table>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
<br><br>

<div id="myModal" class="modal fade" role="dialog">
	<div class="modal-dialog">

	<div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal">&times;</button>
			<h4 class="modal-title">Petunjuk Sinkronisasi</h4>
		</div>
		<div class="modal-body">
			<p>Sinkronisasi dilakukan secara parsial untuk mengurangi beban kinerja server. Untuk melakukan sinkronisasi silahkan munculkan data mahasiswa aktif 100 daftar mahasiswa per halaman. Data yang akan disinkron hanya data yang tampil pada halaman saat ini. Jika jumlah mahasiswa per prodi melebihi 100, maka lakukan sinkronisasi kembali setelah sinkronisasi pada halaman selanjutnya dengan tetap menampilkan data 100 daftar mahasiswa per halaman.</p>
		</div>
		<div class="modal-footer">
			<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
		</div>
	</div>

  </div>
</div>