<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Camaba extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		error_reporting(0);
		$this->load->model('setting_model');
		//$id_menu = 55 (database); cek apakah user memiliki akses
		if ($this->session->userdata('sess_login') == TRUE) {
			$akses = $this->role_model->cekakses(58)->result();
			$aktif = $this->setting_model->getaktivasi('nilai')->result();
			if (count($aktif) != 0) {
				if ($akses != TRUE) {
					echo "<script>alert('Akses Tidak Diizinkan');document.location.href='".base_url()."home';</script>";
					//echo "gabisa";
				}
			} else {
				echo "<script>alert('Akses Tidak Diizinkan');document.location.href='".base_url()."home';</script>";
				//echo "gabisa";
			}
		} else {
			redirect('auth','refresh');
		}
		//$this->load->library('excel');
	}

	public function index()
	{
		$id_reg = 123456789; 

		$data['id_reg'] = $id_reg;

		$data['page']='pmb/camaba_view';
		$this->load->view('template/template', $data);
	}

	function save_bio(){

		$id_reg = $this->input->post('id_reg');
		$nama = $this->input->post('nama');
		$tlp = $this->input->post('telp');
		$email = $this->input->post('email');
		//die($id_reg);
		$username = 'ini username';
		$password = 'ini password';

		$data = array(
			'ID_registrasi' => $id_reg,
			'username' => $username,
			'password' => $password,
			'nama' => $nama,
			'tlp' => $tlp,
			'email' => $email 
			);

		$data2 = array(
			'ID_registrasi' => $id_reg,
			'nama' => $nama,
			'tlp' => $tlp,
			'email' => $email 
			);


		$this->db->insert('tbl_calon_mhs', $data2);
		$this->db->insert('tbl_biodata_camaba', $data);

		$data['page']='pmb/camaba_print';
		$this->load->view('template/template', $data);
	}


	function print_kartu($kode){
		$this->db->select('*');
		$this->db->from('tbl_mahasiswa a');
		$this->db->join('tbl_verifikasi_krs b', 'a.NIMHSMSMHS = b.npm_mahasiswa');
		$this->db->where('NIMHSMSMHS', '201210225045');
		$data['rows'] =  $this->db->get()->result();

		$data['kode'] = $kode;

		$this->load->view('welcome/print/kartu_regist', $data);
	}

}

/* End of file camaba.php */
/* Location: ./application/controllers/camaba.php */