<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Rekap_jml extends MY_Controller {

	public function index()
	{
		$mari = $this->session->userdata('sess_login');
		$pecah = explode(',', $mari['id_user_group']);
		$jmlh = count($pecah);
		for ($i=0; $i < $jmlh; $i++) { 
			$grup[] = $pecah[$i];
		}

		if ((in_array(8, $grup))) {
			$data['pr'] = $this->db->query("SELECT * from tbl_jurusan_prodi where kd_prodi = '".$mari['userid']."'")->row();
		} elseif ((in_array(9, $grup))) {
			$data['pr'] = $this->db->query("SELECT * from tbl_jurusan_prodi where kd_fakultas = '".$mari['userid']."'")->result();
		} elseif ((in_array(10, $grup))|| (in_array(14, $grup)) || (in_array(1, $grup))) {
			$data['pr'] = $this->db->query("SELECT * from tbl_jurusan_prodi")->result();
		}
		$data['page'] = "v_rekjml";
		$this->load->view('template/template', $data);
	}

	function load()
	{
		$program 	= $this->input->post('program');
		$gelombang 	= $this->input->post('gelombang');
		$sts 		= $this->input->post('status');
		$prodd		= $this->input->post('pro');
		$jenis 		= $this->input->post('jns');
		$kategori 	= $this->input->post('ktg');

		$data['prodss'] = $this->db->query("SELECT * from tbl_jurusan_prodi where kd_prodi = '".$prodd."'")->row();

		// var_dump($prodd);exit();

		$this->session->set_userdata('kate',$kategori);

		if ($this->input->post('tahun') == '2016') {

			if ($program == 'S1') {

				$x1 = $this->db->query("SELECT workdad,workmom,didik_ayah,didik_ibu,jenis_sch_maba,asal_sch_maba,lulus_maba from tbl_form_camaba_2016")->result();

				foreach ($x1 as $key) {

					if ($gelombang == 'ALL') {

						if ($sts == 'ALL') {

							if ($prodd == 'ALL') {

								if ($jenis == 'AL') {

									// $data['wow'] = $this->db->query('SELECT * from tbl_form_camaba_2016 group by prodi,gelombang,jenis_pmb,status')->result();

									if ($kategori == 'edu0') {
										$data['wow'] =$this->db->query("SELECT prodi,gelombang,jenis_pmb,status,didik_ayah,count(didik_ayah) as hitung from tbl_form_camaba_2016 where workdad is not null group by didik_ayah,prodi,gelombang,jenis_pmb,status")->result();
									} elseif ($kategori == 'edu1') {
										$data['wow'] =$this->db->query("SELECT prodi,gelombang,jenis_pmb,status,didik_ibu,count(didik_ibu) as hitung from tbl_form_camaba_2016 where workdad is not null group by didik_ibu,prodi,gelombang,jenis_pmb,status")->result();
									} elseif ($kategori == 'wrk0') {
										$data['wow'] =$this->db->query("SELECT prodi,gelombang,jenis_pmb,status,workdad,count(workdad) as hitung from tbl_form_camaba_2016 where workdad is not null group by workdad,prodi,gelombang,jenis_pmb,status")->result();
									} elseif ($kategori == 'wrk1') {	
										$data['wow'] =$this->db->query("SELECT prodi,gelombang,jenis_pmb,status,workmom,count(workmom) as hitung from tbl_form_camaba_2016 where workmom is not null group by workmom,prodi,gelombang,jenis_pmb,status")->result();
									} elseif ($kategori == 'sch') {
										$data['wow'] =$this->db->query("SELECT prodi,gelombang,jenis_pmb,status,jenis_sch_maba,count(jenis_sch_maba) as hitung from tbl_form_camaba_2016 a where jenis_sch_maba is not null group by jenis_sch_maba,prodi,gelombang,jenis_pmb,status")->result();
									} elseif ($kategori == 'cty') {
										$data['wow'] =$this->db->query("SELECT prodi,gelombang,jenis_pmb,status,kota_sch_maba,count(kota_sch_maba) as hitung from tbl_form_camaba_2016 a where kota_sch_maba is not null group by kota_sch_maba,prodi,gelombang,jenis_pmb,status")->result();
									} elseif ($kategori == 'grd') {
										$data['wow'] =$this->db->query("SELECT prodi,gelombang,jenis_pmb,status,lulus_maba,count(lulus_maba) as hitung from tbl_form_camaba_2016 a where lulus_maba is not null group by lulus_maba,prodi,gelombang,jenis_pmb,status")->result();
									}									

								} else {

									// $data['wow'] = $this->db->query('SELECT * from tbl_form_camaba_2016 a where jenis_pmb = "'.$jenis.'"  group by prodi,gelombang,status')->result();

									if ($kategori == 'edu0') {
										$data['wow'] =$this->db->query("SELECT prodi,gelombang,jenis_pmb,status,didik_ayah,count(didik_ayah) as hitung from tbl_form_camaba_2016 a where jenis_pmb = '".$jenis."' and didik_ayah is not null group by prodi,gelombang,status,didik_ayah")->result();
									} elseif($kategori == 'edu1') {	
										$data['wow'] =$this->db->query("SELECT prodi,gelombang,jenis_pmb,status,didik_ibu,count(didik_ibu) as hitung from tbl_form_camaba_2016 a where jenis_pmb = '".$jenis."' and didik_ibu is not null group by prodi,gelombang,status,didik_ibu")->result();
									} elseif ($kategori == 'wrk0') {
										$data['wow'] =$this->db->query("SELECT prodi,gelombang,jenis_pmb,status,workdad,count(workdad) as hitung from tbl_form_camaba_2016 a where jenis_pmb = '".$jenis."' and workdad is not null group by prodi,gelombang,status,workdad")->result();
									} elseif ($kategori == 'wrk1') {
										$data['wow'] =$this->db->query("SELECT prodi,gelombang,jenis_pmb,status,workmom,count(workmom) as hitung from tbl_form_camaba_2016 a where jenis_pmb = '".$jenis."' and workmom is not null group by prodi,gelombang,status,workmom")->result();
									
									} elseif ($kategori == 'sch') {
										$data['wow'] =$this->db->query("SELECT prodi,gelombang,jenis_pmb,status,jenis_sch_maba,count(jenis_sch_maba) as hitung from tbl_form_camaba_2016 a where jenis_pmb = '".$jenis."' and jenis_sch_maba is not null group by prodi,gelombang,status,jenis_sch_maba")->result();
									} elseif ($kategori == 'cty') {
										$data['wow'] =$this->db->query("SELECT prodi,gelombang,jenis_pmb,status,kota_sch_maba,count(kota_sch_maba) as hitung from tbl_form_camaba_2016 a where jenis_pmb = '".$jenis."' and kota_sch_maba is not null group by prodi,gelombang,status,kota_sch_maba")->result();
									} elseif ($kategori == 'grd') {
										$data['wow'] =$this->db->query("SELECT prodi,gelombang,jenis_pmb,status,lulus_maba,count(lulus_maba) as hitung from tbl_form_camaba_2016 a where jenis_pmb = '".$jenis."' and lulus_maba is not null group by prodi,gelombang,status,lulus_maba")->result();
									}

								}	

							} else {

								if ($jenis == 'AL') {

									// $data['wow'] = $this->db->query('SELECT * from tbl_form_camaba_2016 where prodi = "'.$prodd.'"  group by gelombang,jenis_pmb,status')->result();

									if ($kategori == 'edu0') {
										$data['wow'] =$this->db->query("SELECT prodi,gelombang,jenis_pmb,status,didik_ayah,count(didik_ayah) as hitung from tbl_form_camaba_2016 where prodi = '".$prodd."' and didik_ayah is not null group by gelombang,jenis_pmb,status,didik_ayah")->result();
									} elseif ($kategori == 'edu1') {
										$data['wow'] =$this->db->query("SELECT prodi,gelombang,jenis_pmb,status,didik_ibu,count(didik_ibu) as hitung from tbl_form_camaba_2016 where prodi = '".$prodd."' and didik_ibu is not null group by gelombang,jenis_pmb,status,didik_ibu")->result();
									
									} elseif ($kategori == 'wrk0') {
										$data['wow'] =$this->db->query("SELECT prodi,gelombang,jenis_pmb,status,workdad,count(workdad) as hitung from tbl_form_camaba_2016 where prodi = '".$prodd."' and workdad is not null group by gelombang,jenis_pmb,status,workdad")->result();
									} elseif ($kategori == 'wrk1') {
										$data['wow'] =$this->db->query("SELECT prodi,gelombang,jenis_pmb,status,workmom,count(workmom) as hitung from tbl_form_camaba_2016 where prodi = '".$prodd."' and workmom is not null group by gelombang,jenis_pmb,status,workmom")->result();
									
									} elseif ($kategori == 'sch') {
										$data['wow'] =$this->db->query("SELECT prodi,gelombang,jenis_pmb,status,jenis_sch_maba,count(jenis_sch_maba) as hitung from tbl_form_camaba_2016 where prodi = '".$prodd."' and jenis_sch_maba is not null group by gelombang,jenis_pmb,status,jenis_sch_maba")->result();
									} elseif ($kategori == 'cty') {
										$data['wow'] =$this->db->query("SELECT prodi,gelombang,jenis_pmb,status,kota_sch_maba,count(kota_sch_maba) as hitung from tbl_form_camaba_2016 where prodi = '".$prodd."' and kota_sch_maba is not null group by gelombang,jenis_pmb,status,kota_sch_maba")->result();
									} elseif ($kategori == 'grd') {
										$data['wow'] =$this->db->query("SELECT prodi,gelombang,jenis_pmb,status,lulus_maba,count(lulus_maba) as hitung from tbl_form_camaba_2016 where prodi = '".$prodd."' and lulus_maba is not null group by gelombang,jenis_pmb,status,lulus_maba")->result();
									}

									

								} else {

									// $data['wow'] = $this->db->query('SELECT * from tbl_form_camaba_2016 where prodi = "'.$prodd.'" and jenis_pmb = "'.$jenis.'"  group by gelombang,status')->result();

									if ($kategori == 'edu0') {
										$data['wow'] =$this->db->query("SELECT prodi,gelombang,jenis_pmb,status,didik_ayah,count(didik_ayah) as hitung from tbl_form_camaba_2016 where prodi = '".$prodd."' and jenis_pmb = '".$jenis."' and didik_ayah is not null group by gelombang,status,didik_ayah ")->result();
									} elseif ($kategori == 'edu1') {	
										$data['wow'] =$this->db->query("SELECT prodi,gelombang,jenis_pmb,status,didik_ibu,count(didik_ibu) as hitung from tbl_form_camaba_2016 where prodi = '".$prodd."' and jenis_pmb = '".$jenis."' and didik_ibu is not null group by gelombang,status,didik_ibu ")->result();
									} elseif ($kategori == 'wrk0') {
										$data['wow'] =$this->db->query("SELECT prodi,gelombang,jenis_pmb,status,workdad,count(workdad) as hitung from tbl_form_camaba_2016 where prodi = '".$prodd."' and jenis_pmb = '".$jenis."' and workdad is not null group by gelombang,status,workdad ")->result();
									} elseif ($kategori == 'wrk1') {	
										$data['wow'] =$this->db->query("SELECT prodi,gelombang,jenis_pmb,status,workmom,count(workmom) as hitung from tbl_form_camaba_2016 where prodi = '".$prodd."' and jenis_pmb = '".$jenis."' and workmom is not null group by gelombang,status,workmom ")->result();
									} elseif ($kategori == 'sch') {
										$data['wow'] =$this->db->query("SELECT prodi,gelombang,jenis_pmb,status,jenis_sch_maba,count(jenis_sch_maba) as hitung from tbl_form_camaba_2016 where prodi = '".$prodd."' and jenis_pmb = '".$jenis."' and jenis_sch_maba is not null group by gelombang,status,jenis_sch_maba ")->result();
									} elseif ($kategori == 'cty') {
										$data['wow'] =$this->db->query("SELECT prodi,gelombang,jenis_pmb,status,kota_sch_maba,count(kota_sch_maba) as hitung from tbl_form_camaba_2016 where prodi = '".$prodd."' and jenis_pmb = '".$jenis."' and kota_sch_maba is not null group by gelombang,status,kota_sch_maba ")->result();
									} elseif ($kategori == 'grd') {
										$data['wow'] =$this->db->query("SELECT prodi,gelombang,jenis_pmb,status,lulus_maba,count(lulus_maba) as hitung from tbl_form_camaba_2016 where prodi = '".$prodd."' and jenis_pmb = '".$jenis."' and lulus_maba is not null group by gelombang,status,lulus_maba ")->result();
									}

								}

							}

						} else {

							if ($prodd == 'ALL') {

								if ($jenis == 'AL') {

									// $data['wow'] = $this->db->query('SELECT * from tbl_form_camaba_2016 where status '.$sts.'  group by prodi,gelombang,jenis_pmb')->result();

									if ($kategori == 'edu0') {
										$data['wow'] =$this->db->query("SELECT prodi,gelombang,jenis_pmb,status,didik_ayah,count(didik_ayah) as hitung from tbl_form_camaba_2016 where status ".$sts."  and didik_ayah is not null group by prodi,gelombang,jenis_pmb,didik_ayah")->result();
									} elseif ($kategori == 'edu1') {	
										$data['wow'] =$this->db->query("SELECT prodi,gelombang,jenis_pmb,status,didik_ibu,count(didik_ibu) as hitung from tbl_form_camaba_2016 where status ".$sts."  and didik_ibu is not null group by prodi,gelombang,jenis_pmb,didik_ibu")->result();
									} elseif ($kategori == 'wrk0') {
										$data['wow'] =$this->db->query("SELECT prodi,gelombang,jenis_pmb,status,workdad,count(workdad) as hitung from tbl_form_camaba_2016 where status ".$sts."  and workdad is not null group by prodi,gelombang,jenis_pmb,workdad")->result();
									} elseif ($kategori == 'wrk1') {	
										$data['wow'] =$this->db->query("SELECT prodi,gelombang,jenis_pmb,status,workmom,count(workmom) as hitung from tbl_form_camaba_2016 where status ".$sts."  and workmom is not null group by prodi,gelombang,jenis_pmb,workmom")->result();
									} elseif ($kategori == 'sch') {
										$data['wow'] =$this->db->query("SELECT prodi,gelombang,jenis_pmb,status,jenis_sch_maba,count(jenis_sch_maba) as hitung from tbl_form_camaba_2016 where status ".$sts."  and jenis_sch_maba is not null group by prodi,gelombang,jenis_pmb,jenis_sch_maba")->result();
									} elseif ($kategori == 'cty') {
										$data['wow'] =$this->db->query("SELECT prodi,gelombang,jenis_pmb,status,kota_sch_maba,count(kota_sch_maba) as hitung from tbl_form_camaba_2016 where status ".$sts."  and kota_sch_maba is not null group by prodi,gelombang,jenis_pmb,kota_sch_maba")->result();
									} elseif ($kategori == 'grd') {
										$data['wow'] =$this->db->query("SELECT prodi,gelombang,jenis_pmb,status,lulus_maba,count(lulus_maba) as hitung from tbl_form_camaba_2016 where status ".$sts."  and lulus_maba is not null group by prodi,gelombang,jenis_pmb,lulus_maba")->result();
									}

								} else {

									// $data['wow'] = $this->db->query('SELECT * from tbl_form_camaba_2016 where jenis_pmb = "'.$jenis.'" and status '.$sts.'  group by prodi,gelombang')->result();

									if ($kategori == 'edu0') {
										$data['wow'] =$this->db->query("SELECT prodi,gelombang,jenis_pmb,status,didik_ayah,count(didik_ayah) as hitung from tbl_form_camaba_2016 where jenis_pmb = '".$jenis."' and status ".$sts."  and didik_ayah is not null group by prodi,gelombang,didik_ayah")->result();
									} elseif ($kategori == 'edu1') {	
										$data['wow'] =$this->db->query("SELECT prodi,gelombang,jenis_pmb,status,didik_ibu,count(didik_ibu) as hitung from tbl_form_camaba_2016 where jenis_pmb = '".$jenis."' and status ".$sts."  and didik_ibu is not null group by prodi,gelombang,didik_ibu")->result();
									} elseif ($kategori == 'wrk0') {
										$data['wow'] =$this->db->query("SELECT prodi,gelombang,jenis_pmb,status,workdad,count(workdad) as hitung from tbl_form_camaba_2016 where jenis_pmb = '".$jenis."' and status ".$sts."  and workdad is not null group by prodi,gelombang,workdad")->result();
									} elseif ($kategori == 'wrk1') {	
										$data['wow'] =$this->db->query("SELECT prodi,gelombang,jenis_pmb,status,workmom,count(workmom) as hitung from tbl_form_camaba_2016 where jenis_pmb = '".$jenis."' and status ".$sts."  and workmom is not null group by prodi,gelombang,workmom")->result();
									} elseif ($kategori == 'sch') {
										$data['wow'] =$this->db->query("SELECT prodi,gelombang,jenis_pmb,status,jenis_sch_maba,count(jenis_sch_maba) as hitung from tbl_form_camaba_2016 where jenis_pmb = '".$jenis."' and status ".$sts."  and jenis_sch_maba is not null group by prodi,gelombang,jenis_sch_maba")->result();
									} elseif ($kategori == 'cty') {
										$data['wow'] =$this->db->query("SELECT prodi,gelombang,jenis_pmb,status,kota_sch_maba,count(kota_sch_maba) as hitung from tbl_form_camaba_2016 where jenis_pmb = '".$jenis."' and status ".$sts."  and kota_sch_maba is not null group by prodi,gelombang,kota_sch_maba")->result();
									} elseif ($kategori == 'grd') {
										$data['wow'] =$this->db->query("SELECT prodi,gelombang,jenis_pmb,status,lulus_maba,count(lulus_maba) as hitung from tbl_form_camaba_2016 where jenis_pmb = '".$jenis."' and status ".$sts."  and lulus_maba is not null group by prodi,gelombang,lulus_maba")->result();
									}

								}	

							} else {

								if ($jenis == 'AL') {

									// $data['wow'] = $this->db->query('SELECT * from tbl_form_camaba_2016 where prodi = "'.$prodd.'" and status '.$sts.'  group by gelombang,jenis_pmb')->result();

									if ($kategori == 'edu0') {
										$data['wow'] =$this->db->query("SELECT prodi,gelombang,jenis_pmb,status,didik_ayah,count(didik_ayah) as hitung from tbl_form_camaba_2016 where status ".$sts." and prodi = '".$prodd."'  and didik_ayah is not null group by gelombang,jenis_pmb,didik_ayah")->result();
									} elseif ($kategori == 'edu1') {	
										$data['wow'] =$this->db->query("SELECT prodi,gelombang,jenis_pmb,status,didik_ibu,count(didik_ibu) as hitung from tbl_form_camaba_2016 where status ".$sts." and prodi = '".$prodd."'  and didik_ibu is not null group by gelombang,jenis_pmb,didik_ibu")->result();
									} elseif ($kategori == 'wrk0') {
										$data['wow'] =$this->db->query("SELECT prodi,gelombang,jenis_pmb,status,workdad,count(workdad) as hitung from tbl_form_camaba_2016 where status ".$sts." and prodi = '".$prodd."'  and workdad is not null group by gelombang,jenis_pmb,workdad")->result();
									} elseif ($kategori == 'wrk1') {	
										$data['wow'] =$this->db->query("SELECT prodi,gelombang,jenis_pmb,status,workmom,count(workmom) as hitung from tbl_form_camaba_2016 where status ".$sts." and prodi = '".$prodd."'  and workmom is not null group by gelombang,jenis_pmb,workmom")->result();
									} elseif ($kategori == 'sch') {
										$data['wow'] =$this->db->query("SELECT prodi,gelombang,jenis_pmb,status,jenis_sch_maba,count(jenis_sch_maba) as hitung from tbl_form_camaba_2016 where status ".$sts." and prodi = '".$prodd."'  and jenis_sch_maba is not null group by gelombang,jenis_pmb,jenis_sch_maba")->result();
									} elseif ($kategori == 'cty') {
										$data['wow'] =$this->db->query("SELECT prodi,gelombang,jenis_pmb,status,kota_sch_maba,count(kota_sch_maba) as hitung from tbl_form_camaba_2016 where status ".$sts." and prodi = '".$prodd."'  and kota_sch_maba is not null group by gelombang,jenis_pmb,kota_sch_maba")->result();
									} elseif ($kategori == 'grd') {
										$data['wow'] =$this->db->query("SELECT prodi,gelombang,jenis_pmb,status,lulus_maba,count(lulus_maba) as hitung from tbl_form_camaba_2016 where status ".$sts." and prodi = '".$prodd."'  and lulus_maba is not null group by gelombang,jenis_pmb,lulus_maba")->result();
									}

								} else {

									// $data['wow'] = $this->db->query('SELECT * from tbl_form_camaba_2016 where prodi = "'.$prodd.'" and jenis_pmb = "'.$jenis.'" and status '.$sts.'  group by gelombang')->result();

									if ($kategori == 'edu0') {
										$data['wow'] =$this->db->query("SELECT prodi,gelombang,jenis_pmb,status,didik_ayah,count(didik_ayah) as hitung from tbl_form_camaba_2016 where status ".$sts." and prodi = '".$prodd."' and jenis_pmb = '".$jenis."'  and didik_ayah is not null group by gelombang,didik_ayah")->result();
									} elseif ($kategori == 'edu1') {	
										$data['wow'] =$this->db->query("SELECT prodi,gelombang,jenis_pmb,status,didik_ibu,count(didik_ibu) as hitung from tbl_form_camaba_2016 where status ".$sts." and prodi = '".$prodd."' and jenis_pmb = '".$jenis."'  and didik_ibu is not null group by gelombang,didik_ibu")->result();
									} elseif ($kategori == 'wrk0') {
										$data['wow'] =$this->db->query("SELECT prodi,gelombang,jenis_pmb,status,workdad,count(workdad) as hitung from tbl_form_camaba_2016 where status ".$sts." and prodi = '".$prodd."' and jenis_pmb = '".$jenis."'  and workdad is not null group by gelombang,workdad")->result();
									} elseif ($kategori == 'wrk1') {	
										$data['wow'] =$this->db->query("SELECT prodi,gelombang,jenis_pmb,status,workmom,count(workmom) as hitung from tbl_form_camaba_2016 where status ".$sts." and prodi = '".$prodd."' and jenis_pmb = '".$jenis."'  and workmom is not null group by gelombang,workmom")->result();
									} elseif ($kategori == 'sch') {
										$data['wow'] =$this->db->query("SELECT prodi,gelombang,jenis_pmb,status,jenis_sch_maba,count(jenis_sch_maba) as hitung from tbl_form_camaba_2016 where status ".$sts." and prodi = '".$prodd."' and jenis_pmb = '".$jenis."'  and jenis_sch_maba is not null group by gelombang,jenis_sch_maba")->result();
									} elseif ($kategori == 'cty') {
										$data['wow'] =$this->db->query("SELECT prodi,gelombang,jenis_pmb,status,kota_sch_maba,count(kota_sch_maba) as hitung from tbl_form_camaba_2016 where status ".$sts." and prodi = '".$prodd."' and jenis_pmb = '".$jenis."'  and kota_sch_maba is not null group by gelombang,kota_sch_maba")->result();
									} elseif ($kategori == 'grd') {
										$data['wow'] =$this->db->query("SELECT prodi,gelombang,jenis_pmb,status,lulus_maba,count(lulus_maba) as hitung from tbl_form_camaba_2016 where status ".$sts." and prodi = '".$prodd."' and jenis_pmb = '".$jenis."'  and lulus_maba is not null group by gelombang,lulus_maba")->result();
									}
									
								}

							}
							
						}
							
					} else {

						if ($sts == 'ALL') {

							if ($prodd == 'ALL') {

								if ($jenis == 'AL') {

									// $data['wow'] = $this->db->query('SELECT * from tbl_form_camaba_2016 where gelombang = "'.$gelombang.'"  group by prodi,jenis_pmb,status')->result();

									if ($kategori == 'edu0') {
										$data['wow'] = $this->db->query('SELECT prodi,gelombang,jenis_pmb,status,didik_ayah,count(didik_ayah) as hitung from tbl_form_camaba_2016 where gelombang = "'.$gelombang.'" and didik_ayah is not null group by prodi,jenis_pmb,status,didik_ayah')->result();
									} elseif ($kategori == 'edu1') {	
										$data['wow'] = $this->db->query('SELECT prodi,gelombang,jenis_pmb,status,didik_ibu,count(didik_ibu) as hitung from tbl_form_camaba_2016 where gelombang = "'.$gelombang.'" and didik_ibu is not null group by prodi,jenis_pmb,status,didik_ibu')->result();
									} elseif ($kategori == 'wrk0') {
										$data['wow'] = $this->db->query('SELECT prodi,gelombang,jenis_pmb,status,workdad,count(workdad) as hitung from tbl_form_camaba_2016 where gelombang = "'.$gelombang.'" and workdad is not null group by prodi,jenis_pmb,status,workdad')->result();
									} elseif ($kategori == 'wrk1') {	
										$data['wow'] = $this->db->query('SELECT prodi,gelombang,jenis_pmb,status,workmom,count(workmom) as hitung from tbl_form_camaba_2016 where gelombang = "'.$gelombang.'" and workmom is not null group by prodi,jenis_pmb,status,workmom')->result();
									} elseif ($kategori == 'sch') {
										$data['wow'] = $this->db->query('SELECT prodi,gelombang,jenis_pmb,status,jenis_sch_maba,count(jenis_sch_maba) as hitung from tbl_form_camaba_2016 where gelombang = "'.$gelombang.'" and jenis_sch_maba is not null group by prodi,jenis_pmb,status,jenis_sch_maba')->result();
									} elseif ($kategori == 'cty') {
										$data['wow'] = $this->db->query('SELECT prodi,gelombang,jenis_pmb,status,kota_sch_maba,count(kota_sch_maba) as hitung from tbl_form_camaba_2016 where gelombang = "'.$gelombang.'" and kota_sch_maba is not null group by prodi,jenis_pmb,status,kota_sch_maba')->result();
									} elseif ($kategori == 'grd') {
										$data['wow'] = $this->db->query('SELECT prodi,gelombang,jenis_pmb,status,lulus_maba,count(lulus_maba) as hitung from tbl_form_camaba_2016 where gelombang = "'.$gelombang.'" and lulus_maba is not null group by prodi,jenis_pmb,status,lulus_maba')->result();
									}

								} else {

									// $data['wow'] = $this->db->query('SELECT * from tbl_form_camaba_2016 where jenis_pmb = "'.$jenis.'" and gelombang = "'.$gelombang.'"  group by prodi,status')->result();

									if ($kategori == 'edu0') {
										$data['wow'] = $this->db->query('SELECT prodi,gelombang,jenis_pmb,status,didik_ayah,count(didik_ayah) as hitung from tbl_form_camaba_2016 where jenis_pmb = "'.$jenis.'" and gelombang = "'.$gelombang.'" and didik_ayah is not null group by prodi,status,didik_ayah')->result();
									} elseif ($kategori == 'edu1') {	
										$data['wow'] = $this->db->query('SELECT prodi,gelombang,jenis_pmb,status,didik_ibu,count(didik_ibu) as hitung from tbl_form_camaba_2016 where jenis_pmb = "'.$jenis.'" and gelombang = "'.$gelombang.'" and didik_ibu is not null group by prodi,status,didik_ibu')->result();
									} elseif ($kategori == 'wrk0') {
										$data['wow'] = $this->db->query('SELECT prodi,gelombang,jenis_pmb,status,workdad,count(workdad) as hitung from tbl_form_camaba_2016 where jenis_pmb = "'.$jenis.'" and gelombang = "'.$gelombang.'" and workdad is not null group by prodi,status,workdad')->result();
									} elseif ($kategori == 'wrk1') {	
										$data['wow'] = $this->db->query('SELECT prodi,gelombang,jenis_pmb,status,workmom,count(workmom) as hitung from tbl_form_camaba_2016 where jenis_pmb = "'.$jenis.'" and gelombang = "'.$gelombang.'" and workmom is not null group by prodi,status,workmom')->result();
									} elseif ($kategori == 'sch') {
										$data['wow'] = $this->db->query('SELECT prodi,gelombang,jenis_pmb,status,jenis_sch_maba,count(jenis_sch_maba) as hitung from tbl_form_camaba_2016 where jenis_pmb = "'.$jenis.'" and gelombang = "'.$gelombang.'" and jenis_sch_maba is not null group by prodi,status,jenis_sch_maba')->result();
									} elseif ($kategori == 'cty') {
										$data['wow'] = $this->db->query('SELECT prodi,gelombang,jenis_pmb,status,kota_sch_maba,count(kota_sch_maba) as hitung from tbl_form_camaba_2016 where jenis_pmb = "'.$jenis.'" and gelombang = "'.$gelombang.'" and kota_sch_maba is not null group by prodi,status,kota_sch_maba')->result();
									} elseif ($kategori == 'grd') {
										$data['wow'] = $this->db->query('SELECT prodi,gelombang,jenis_pmb,status,lulus_maba,count(lulus_maba) as hitung from tbl_form_camaba_2016 where jenis_pmb = "'.$jenis.'" and gelombang = "'.$gelombang.'" and lulus_maba is not null group by prodi,status,lulus_maba')->result();
									}

								}	

							} else {

								if ($jenis == 'AL') {

									// $data['wow'] = $this->db->query('SELECT * from tbl_form_camaba_2016 where prodi = "'.$prodd.'" and gelombang = "'.$gelombang.'"  group by jenis_pmb,status')->result();

									if ($kategori == 'edu0') {
										$data['wow'] = $this->db->query('SELECT prodi,gelombang,jenis_pmb,status,didik_ayah,count(didik_ayah) as hitung from tbl_form_camaba_2016 where prodi = "'.$prodd.'" and gelombang = "'.$gelombang.'" and didik_ayah is not null group by jenis_pmb,status,didik_ayah')->result();
									} elseif ($kategori == 'edu1') {	
										$data['wow'] = $this->db->query('SELECT prodi,gelombang,jenis_pmb,status,didik_ibu,count(didik_ibu) as hitung from tbl_form_camaba_2016 where prodi = "'.$prodd.'" and gelombang = "'.$gelombang.'" and didik_ibu is not null group by jenis_pmb,status,didik_ibu')->result();
									} elseif ($kategori == 'wrk0') {
										$data['wow'] = $this->db->query('SELECT prodi,gelombang,jenis_pmb,status,workdad,count(workdad) as hitung from tbl_form_camaba_2016 where prodi = "'.$prodd.'" and gelombang = "'.$gelombang.'" and workdad is not null group by jenis_pmb,status,workdad')->result();
									} elseif ($kategori == 'wrk1') {	
										$data['wow'] = $this->db->query('SELECT prodi,gelombang,jenis_pmb,status,workmom,count(workmom) as hitung from tbl_form_camaba_2016 where prodi = "'.$prodd.'" and gelombang = "'.$gelombang.'" and workmom is not null group by jenis_pmb,status,workmom')->result();
									} elseif ($kategori == 'sch') {
										$data['wow'] = $this->db->query('SELECT prodi,gelombang,jenis_pmb,status,jenis_sch_maba,count(jenis_sch_maba) as hitung from tbl_form_camaba_2016 where prodi = "'.$prodd.'" and gelombang = "'.$gelombang.'" and jenis_sch_maba is not null group by jenis_pmb,status,jenis_sch_maba')->result();
									} elseif ($kategori == 'cty') {
										$data['wow'] = $this->db->query('SELECT prodi,gelombang,jenis_pmb,status,kota_sch_maba,count(kota_sch_maba) as hitung from tbl_form_camaba_2016 where prodi = "'.$prodd.'" and gelombang = "'.$gelombang.'" and kota_sch_maba is not null group by jenis_pmb,status,kota_sch_maba')->result();
									} elseif ($kategori == 'grd') {
										$data['wow'] = $this->db->query('SELECT prodi,gelombang,jenis_pmb,status,lulus_maba,count(lulus_maba) as hitung from tbl_form_camaba_2016 where prodi = "'.$prodd.'" and gelombang = "'.$gelombang.'" and lulus_maba is not null group by jenis_pmb,status,lulus_maba')->result();
									}

								} else {

									// $data['wow'] = $this->db->query('SELECT * from tbl_form_camaba_2016 where prodi = "'.$prodd.'" and jenis_pmb = "'.$jenis.'" and gelombang = "'.$gelombang.'"  group by status')->result();

									if ($kategori == 'edu0') {
										$data['wow'] = $this->db->query('SELECT prodi,gelombang,jenis_pmb,status,didik_ayah,count(didik_ayah) as hitung from tbl_form_camaba_2016 where prodi = "'.$prodd.'" and jenis_pmb = "'.$jenis.'" and gelombang = "'.$gelombang.'" and didik_ayah is not null group by status,didik_ayah')->result();
									} elseif ($kategori == 'edu1') {	
										$data['wow'] = $this->db->query('SELECT prodi,gelombang,jenis_pmb,status,didik_ibu,count(didik_ibu) as hitung from tbl_form_camaba_2016 where prodi = "'.$prodd.'" and jenis_pmb = "'.$jenis.'" and gelombang = "'.$gelombang.'" and didik_ibu is not null group by status,didik_ibu')->result();
									} elseif ($kategori == 'wrk0') {
										$data['wow'] = $this->db->query('SELECT prodi,gelombang,jenis_pmb,status,workdad,count(workdad) as hitung from tbl_form_camaba_2016 where prodi = "'.$prodd.'" and jenis_pmb = "'.$jenis.'" and gelombang = "'.$gelombang.'" and workdad is not null group by status,workdad')->result();
									} elseif ($kategori == 'wrk1') {	
										$data['wow'] = $this->db->query('SELECT prodi,gelombang,jenis_pmb,status,workmom,count(workmom) as hitung from tbl_form_camaba_2016 where prodi = "'.$prodd.'" and jenis_pmb = "'.$jenis.'" and gelombang = "'.$gelombang.'" and workmom is not null group by status,workmom')->result();
									} elseif ($kategori == 'sch') {
										$data['wow'] = $this->db->query('SELECT prodi,gelombang,jenis_pmb,status,jenis_sch_maba,count(jenis_sch_maba) as hitung from tbl_form_camaba_2016 where prodi = "'.$prodd.'" and jenis_pmb = "'.$jenis.'" and gelombang = "'.$gelombang.'" and jenis_sch_maba is not null group by status,jenis_sch_maba')->result();
									} elseif ($kategori == 'cty') {
										$data['wow'] = $this->db->query('SELECT prodi,gelombang,jenis_pmb,status,kota_sch_maba,count(kota_sch_maba) as hitung from tbl_form_camaba_2016 where prodi = "'.$prodd.'" and jenis_pmb = "'.$jenis.'" and gelombang = "'.$gelombang.'" and kota_sch_maba is not null group by status,kota_sch_maba')->result();
									} elseif ($kategori == 'grd') {
										$data['wow'] = $this->db->query('SELECT prodi,gelombang,jenis_pmb,status,lulus_maba,count(lulus_maba) as hitung from tbl_form_camaba_2016 where prodi = "'.$prodd.'" and jenis_pmb = "'.$jenis.'" and gelombang = "'.$gelombang.'" and lulus_maba is not null group by status,lulus_maba')->result();
									}

									// $data['hits1'] = $this->db->query('SELECT workdad,count(workdad) as hitung from tbl_form_camaba_2016 where prodi = "'.$prodd.'" and jenis_pmb = "'.$jenis.'" and gelombang = "'.$gelombang.'" and workdad is not null group by workdad')->result();

								}

							}

						} else {

							if ($prodd == 'ALL') {

								if ($jenis == 'AL') {

									// $data['wow'] = $this->db->query('SELECT * from tbl_form_camaba_2016 where status '.$sts.' and gelombang = "'.$gelombang.'"  group by prodi,jenis_pmb')->result();

									if ($kategori == 'edu0') {
										$data['wow'] = $this->db->query('SELECT prodi,gelombang,jenis_pmb,status,didik_ayah,count(didik_ayah) as hitung from tbl_form_camaba_2016 where status '.$sts.' and gelombang = "'.$gelombang.'" and gelombang = "'.$gelombang.'" and didik_ayah is not null group by prodi,jenis_pmb,didik_ayah')->result();
									} elseif ($kategori == 'edu1') {	
										$data['wow'] = $this->db->query('SELECT prodi,gelombang,jenis_pmb,status,didik_ibu,count(didik_ibu) as hitung from tbl_form_camaba_2016 where status '.$sts.' and gelombang = "'.$gelombang.'" and gelombang = "'.$gelombang.'" and didik_ibu is not null group by prodi,jenis_pmb,didik_ibu')->result();
									} elseif ($kategori == 'wrk0') {
										$data['wow'] = $this->db->query('SELECT prodi,gelombang,jenis_pmb,status,workdad,count(workdad) as hitung from tbl_form_camaba_2016 where status '.$sts.' and gelombang = "'.$gelombang.'" and gelombang = "'.$gelombang.'" and workdad is not null group by prodi,jenis_pmb,workdad')->result();
									} elseif ($kategori == 'wrk1') {	
										$data['wow'] = $this->db->query('SELECT prodi,gelombang,jenis_pmb,status,workmom,count(workmom) as hitung from tbl_form_camaba_2016 where status '.$sts.' and gelombang = "'.$gelombang.'" and gelombang = "'.$gelombang.'" and workmom is not null group by prodi,jenis_pmb,workmom')->result();
									} elseif ($kategori == 'sch') {
										$data['wow'] = $this->db->query('SELECT prodi,gelombang,jenis_pmb,status,jenis_sch_maba,count(jenis_sch_maba) as hitung from tbl_form_camaba_2016 where status '.$sts.' and gelombang = "'.$gelombang.'" and gelombang = "'.$gelombang.'" and jenis_sch_maba is not null group by prodi,jenis_pmb,jenis_sch_maba')->result();
									} elseif ($kategori == 'cty') {
										$data['wow'] = $this->db->query('SELECT prodi,gelombang,jenis_pmb,status,kota_sch_maba,count(kota_sch_maba) as hitung from tbl_form_camaba_2016 where status '.$sts.' and gelombang = "'.$gelombang.'" and gelombang = "'.$gelombang.'" and kota_sch_maba is not null group by prodi,jenis_pmb,kota_sch_maba')->result();
									} elseif ($kategori == 'grd') {
										$data['wow'] = $this->db->query('SELECT prodi,gelombang,jenis_pmb,status,lulus_maba,count(lulus_maba) as hitung from tbl_form_camaba_2016 where status '.$sts.' and gelombang = "'.$gelombang.'" and gelombang = "'.$gelombang.'" and lulus_maba is not null group by prodi,jenis_pmb,lulus_maba')->result();
									}

									// $data['hits1'] = $this->db->query('SELECT workdad,count(workdad) as hitung from tbl_form_camaba_2016 where status '.$sts.' and gelombang = "'.$gelombang.'" and workdad is not null group by workdad')->result();

								} else {

									// $data['wow'] = $this->db->query('SELECT * from tbl_form_camaba_2016 where jenis_pmb = "'.$jenis.'" and status '.$sts.' and gelombang = "'.$gelombang.'"  group by prodi')->result();

									if ($kategori == 'edu0') {
										$data['wow'] = $this->db->query('SELECT prodi,gelombang,jenis_pmb,status,didik_ayah,count(didik_ayah) as hitung from tbl_form_camaba_2016 where jenis_pmb = "'.$jenis.'" and status '.$sts.' and gelombang = "'.$gelombang.'" and didik_ayah is not null group by prodi,didik_ayah')->result();
									} elseif ($kategori == 'edu1') {	
										$data['wow'] = $this->db->query('SELECT prodi,gelombang,jenis_pmb,status,didik_ibu,count(didik_ibu) as hitung from tbl_form_camaba_2016 where jenis_pmb = "'.$jenis.'" and status '.$sts.' and gelombang = "'.$gelombang.'" and didik_ibu is not null group by prodi,didik_ibu')->result();
									} elseif ($kategori == 'wrk0') {
										$data['wow'] = $this->db->query('SELECT prodi,gelombang,jenis_pmb,status,workdad,count(workdad) as hitung from tbl_form_camaba_2016 where jenis_pmb = "'.$jenis.'" and status '.$sts.' and gelombang = "'.$gelombang.'" and workdad is not null group by prodi,workdad')->result();
									} elseif ($kategori == 'wrk1') {	
										$data['wow'] = $this->db->query('SELECT prodi,gelombang,jenis_pmb,status,workmom,count(workmom) as hitung from tbl_form_camaba_2016 where jenis_pmb = "'.$jenis.'" and status '.$sts.' and gelombang = "'.$gelombang.'" and workmom is not null group by prodi,workmom')->result();
									} elseif ($kategori == 'sch') {
										$data['wow'] = $this->db->query('SELECT prodi,gelombang,jenis_pmb,status,jenis_sch_maba,count(jenis_sch_maba) as hitung from tbl_form_camaba_2016 where jenis_pmb = "'.$jenis.'" and status '.$sts.' and gelombang = "'.$gelombang.'" and jenis_sch_maba is not null group by prodi,jenis_sch_maba')->result();
									} elseif ($kategori == 'cty') {
										$data['wow'] = $this->db->query('SELECT prodi,gelombang,jenis_pmb,status,kota_sch_maba,count(kota_sch_maba) as hitung from tbl_form_camaba_2016 where jenis_pmb = "'.$jenis.'" and status '.$sts.' and gelombang = "'.$gelombang.'" and kota_sch_maba is not null group by prodi,kota_sch_maba')->result();
									} elseif ($kategori == 'grd') {
										$data['wow'] = $this->db->query('SELECT prodi,gelombang,jenis_pmb,status,lulus_maba,count(lulus_maba) as hitung from tbl_form_camaba_2016 where jenis_pmb = "'.$jenis.'" and status '.$sts.' and gelombang = "'.$gelombang.'" and lulus_maba is not null group by prodi,lulus_maba')->result();
									}

									// $data['hits1'] = $this->db->query('SELECT workdad,count(workdad) as hitung from tbl_form_camaba_2016 where jenis_pmb = "'.$jenis.'" and status '.$sts.' and gelombang = "'.$gelombang.'" and workdad is not null group by workdad')->result();

								}	

							} else {

								if ($jenis == 'AL') {

									// $data['wow'] = $this->db->query('SELECT * from tbl_form_camaba_2016 where prodi = "'.$prodd.'" and status '.$sts.' and gelombang = "'.$gelombang.'"  group by jenis_pmb')->result();

									if ($kategori == 'edu0') {
										$data['wow'] = $this->db->query('SELECT prodi,gelombang,jenis_pmb,status,didik_ayah,count(didik_ayah) as hitung from tbl_form_camaba_2016 where prodi = "'.$prodd.'" and status '.$sts.' and gelombang = "'.$gelombang.'" and didik_ayah is not null group by jenis_pmb,didik_ayah')->result();
									} elseif ($kategori == 'edu1') {	
										$data['wow'] = $this->db->query('SELECT prodi,gelombang,jenis_pmb,status,didik_ibu,count(didik_ibu) as hitung from tbl_form_camaba_2016 where prodi = "'.$prodd.'" and status '.$sts.' and gelombang = "'.$gelombang.'" and didik_ibu is not null group by jenis_pmb,didik_ibu')->result();
									} elseif ($kategori == 'wrk0') {
										$data['wow'] = $this->db->query('SELECT prodi,gelombang,jenis_pmb,status,workdad,count(workdad) as hitung from tbl_form_camaba_2016 where prodi = "'.$prodd.'" and status '.$sts.' and gelombang = "'.$gelombang.'" and workdad is not null group by jenis_pmb,workdad')->result();
									} elseif ($kategori == 'wrk1') {	
										$data['wow'] = $this->db->query('SELECT prodi,gelombang,jenis_pmb,status,workmom,count(workmom) as hitung from tbl_form_camaba_2016 where prodi = "'.$prodd.'" and status '.$sts.' and gelombang = "'.$gelombang.'" and workmom is not null group by jenis_pmb,workmom')->result();
									} elseif ($kategori == 'sch') {
										$data['wow'] = $this->db->query('SELECT prodi,gelombang,jenis_pmb,status,jenis_sch_maba,count(jenis_sch_maba) as hitung from tbl_form_camaba_2016 where prodi = "'.$prodd.'" and status '.$sts.' and gelombang = "'.$gelombang.'" and jenis_sch_maba is not null group by jenis_pmb,jenis_sch_maba')->result();
									} elseif ($kategori == 'cty') {
										$data['wow'] = $this->db->query('SELECT prodi,gelombang,jenis_pmb,status,kota_sch_maba,count(kota_sch_maba) as hitung from tbl_form_camaba_2016 where prodi = "'.$prodd.'" and status '.$sts.' and gelombang = "'.$gelombang.'" and kota_sch_maba is not null group by jenis_pmb,kota_sch_maba')->result();
									} elseif ($kategori == 'grd') {
										$data['wow'] = $this->db->query('SELECT prodi,gelombang,jenis_pmb,status,lulus_maba,count(lulus_maba) as hitung from tbl_form_camaba_2016 where prodi = "'.$prodd.'" and status '.$sts.' and gelombang = "'.$gelombang.'" and lulus_maba is not null group by jenis_pmb,lulus_maba')->result();
									}

									// $data['hits1'] = $this->db->query('SELECT workdad,count(workdad) as hitung from tbl_form_camaba_2016 where prodi = "'.$prodd.'" and status '.$sts.' and gelombang = "'.$gelombang.'" and workdad is not null group by workdad')->result();

								} else {

									// $data['wow'] = $this->db->query('SELECT * from tbl_form_camaba_2016 where prodi = "'.$prodd.'" and jenis_pmb = "'.$jenis.'" and status '.$sts.' and gelombang = "'.$gelombang.'"')->result();

									if ($kategori == 'edu0') {
										$data['wow'] = $this->db->query('SELECT prodi,gelombang,jenis_pmb,status,didik_ayah,count(didik_ayah) as hitung from tbl_form_camaba_2016 where prodi = "'.$prodd.'" and jenis_pmb = "'.$jenis.'" and status '.$sts.' and gelombang = "'.$gelombang.'" and didik_ayah is not null group by didik_ayah')->result();
									} elseif ($kategori == 'edu1') {	
										$data['wow'] = $this->db->query('SELECT prodi,gelombang,jenis_pmb,status,didik_ibu,count(didik_ibu) as hitung from tbl_form_camaba_2016 where prodi = "'.$prodd.'" and jenis_pmb = "'.$jenis.'" and status '.$sts.' and gelombang = "'.$gelombang.'" and didik_ibu is not null group by didik_ibu')->result();
									} elseif ($kategori == 'wrk0') {
										$data['wow'] = $this->db->query('SELECT prodi,gelombang,jenis_pmb,status,workdad,count(workdad) as hitung from tbl_form_camaba_2016 where prodi = "'.$prodd.'" and jenis_pmb = "'.$jenis.'" and status '.$sts.' and gelombang = "'.$gelombang.'" and workdad is not null group by workdad')->result();
									} elseif ($kategori == 'wrk1') {	
										$data['wow'] = $this->db->query('SELECT prodi,gelombang,jenis_pmb,status,workmom,count(workmom) as hitung from tbl_form_camaba_2016 where prodi = "'.$prodd.'" and jenis_pmb = "'.$jenis.'" and status '.$sts.' and gelombang = "'.$gelombang.'" and workmom is not null group by workmom')->result();
									} elseif ($kategori == 'sch') {
										$data['wow'] = $this->db->query('SELECT prodi,gelombang,jenis_pmb,status,jenis_sch_maba,count(jenis_sch_maba) as hitung from tbl_form_camaba_2016 where prodi = "'.$prodd.'" and jenis_pmb = "'.$jenis.'" and status '.$sts.' and gelombang = "'.$gelombang.'" and jenis_sch_maba is not null group by jenis_sch_maba')->result();
									} elseif ($kategori == 'cty') {
										$data['wow'] = $this->db->query('SELECT prodi,gelombang,jenis_pmb,status,kota_sch_maba,count(kota_sch_maba) as hitung from tbl_form_camaba_2016 where prodi = "'.$prodd.'" and jenis_pmb = "'.$jenis.'" and status '.$sts.' and gelombang = "'.$gelombang.'" and kota_sch_maba is not null group by asal_sch_maba')->result();
									} elseif ($kategori == 'grd') {
										$data['wow'] = $this->db->query('SELECT prodi,gelombang,jenis_pmb,status,lulus_maba,count(lulus_maba) as hitung from tbl_form_camaba_2016 where prodi = "'.$prodd.'" and jenis_pmb = "'.$jenis.'" and status '.$sts.' and gelombang = "'.$gelombang.'" and lulus_maba is not null group by lulus_maba')->result();
									}

									// $data['hits1'] = $this->db->query('SELECT workdad,count(workdad) as kerjadad from tbl_form_camaba_2016 where prodi = "'.$prodd.'" and jenis_pmb = "'.$jenis.'" and status '.$sts.' and gelombang = "'.$gelombang.'" and workdad is not null group by workdad')->result();
									
								}

							}
							
						}
							
					}
				}
				
				// if ($kategori == 'edu0' || $kategori == 'wrk0' || $kategori == 'edu1' || $kategori == 'wrk1') {
					// die('aaa');
					$this->load->view('excel_report_jml_edu', $data);
				// } else {
					// die('xxx');
					// $this->load->view('excel_report_jml', $data);
				// }

			} elseif ($program == 'S2') {

				if ($gelombang == 'ALL') {

					if ($sts == 'ALL') {

						if ($prodd == 'ALL') {

							if ($jenis == 'AL') {

								if ($kategori == 'wrk') {
									$data['wew'] = $this->db->query('SELECT opsi_prodi_s2,gelombang,status,jenis_pmb,pekerjaan,count(pekerjaan) as angka from tbl_pmb_s2_2016 group by jenis_pmb,opsi_prodi_s2,status,gelombang,pekerjaan')->result();
								} elseif ($kategori == 'cty') {
									$data['wew'] = $this->db->query('SELECT opsi_prodi_s2,gelombang,status,jenis_pmb,kota_asl_univ,count(kota_asl_univ) as angka from tbl_pmb_s2_2016 group by jenis_pmb,opsi_prodi_s2,status,gelombang,kota_asl_univ')->result();
								} elseif ($kategori == 'grd') {
									$data['wew'] = $this->db->query('SELECT opsi_prodi_s2,gelombang,status,jenis_pmb,th_lulus,count(th_lulus) as angka from tbl_pmb_s2_2016 group by jenis_pmb,opsi_prodi_s2,status,gelombang,th_lulus')->result();
								}

							} else {

								// $data['wew'] = $this->db->query('SELECT * from tbl_pmb_s2_2016 where jenis_pmb = "'.$jenis.'"')->result();

								if ($kategori == 'wrk') {
									$data['wew'] = $this->db->query('SELECT opsi_prodi_s2,gelombang,status,jenis_pmb,pekerjaan,count(pekerjaan) as angka from tbl_pmb_s2_2016 where jenis_pmb = "'.$jenis.'" group by opsi_prodi_s2,status,gelombang,pekerjaan')->result();
								} elseif ($kategori == 'cty') {
									$data['wew'] = $this->db->query('SELECT opsi_prodi_s2,gelombang,status,jenis_pmb,kota_asl_univ,count(kota_asl_univ) as angka from tbl_pmb_s2_2016 where jenis_pmb = "'.$jenis.'" group by opsi_prodi_s2,status,gelombang,kota_asl_univ')->result();
								} elseif ($kategori == 'grd') {
									$data['wew'] = $this->db->query('SELECT opsi_prodi_s2,gelombang,status,jenis_pmb,th_lulus,count(th_lulus) as angka from tbl_pmb_s2_2016 where jenis_pmb = "'.$jenis.'" group by opsi_prodi_s2,status,gelombang,th_lulus')->result();
								}

							}	

						} else {

							if ($jenis == 'AL') {

								// $data['wew'] = $this->db->query('SELECT * from tbl_pmb_s2_2016 where opsi_prodi_s2 = "'.$prodd.'"')->result();

								if ($kategori == 'wrk') {
									$data['wew'] = $this->db->query('SELECT opsi_prodi_s2,gelombang,status,jenis_pmb,pekerjaan,count(pekerjaan) as angka from tbl_pmb_s2_2016 where opsi_prodi_s2 = "'.$prodd.'" group by jenis_pmb,status,gelombang,pekerjaan')->result();
								} elseif ($kategori == 'cty') {
									$data['wew'] = $this->db->query('SELECT opsi_prodi_s2,gelombang,status,jenis_pmb,kota_asl_univ,count(kota_asl_univ) as angka from tbl_pmb_s2_2016 where opsi_prodi_s2 = "'.$prodd.'" group by jenis_pmb,status,gelombang,kota_asl_univ')->result();
								} elseif ($kategori == 'grd') {
									$data['wew'] = $this->db->query('SELECT opsi_prodi_s2,gelombang,status,jenis_pmb,th_lulus,count(th_lulus) as angka from tbl_pmb_s2_2016 where opsi_prodi_s2 = "'.$prodd.'" group by jenis_pmb,status,gelombang,th_lulus')->result();
								}

							} else {

								// $data['wew'] = $this->db->query('SELECT * from tbl_pmb_s2_2016 where opsi_prodi_s2 = "'.$prodd.'" and jenis_pmb = "'.$jenis.'"')->result();

								if ($kategori == 'wrk') {
									$data['wew'] = $this->db->query('SELECT opsi_prodi_s2,gelombang,status,jenis_pmb,pekerjaan,count(pekerjaan) as angka from tbl_pmb_s2_2016 where opsi_prodi_s2 = "'.$prodd.'" and jenis_pmb = "'.$jenis.'" group by status,gelombang,pekerjaan')->result();
								} elseif ($kategori == 'cty') {
									$data['wew'] = $this->db->query('SELECT opsi_prodi_s2,gelombang,status,jenis_pmb,kota_asl_univ,count(kota_asl_univ) as angka from tbl_pmb_s2_2016 where opsi_prodi_s2 = "'.$prodd.'" and jenis_pmb = "'.$jenis.'" group by status,gelombang,kota_asl_univ')->result();
								} elseif ($kategori == 'grd') {
									$data['wew'] = $this->db->query('SELECT opsi_prodi_s2,gelombang,status,jenis_pmb,th_lulus,count(th_lulus) as angka from tbl_pmb_s2_2016 where opsi_prodi_s2 = "'.$prodd.'" and jenis_pmb = "'.$jenis.'" group by status,gelombang,th_lulus')->result();
								}

							}

						}

					} else {

						if ($prodd == 'ALL') {

							if ($jenis == 'AL') {

								// $data['wew'] = $this->db->query('SELECT * from tbl_pmb_s2_2016 where status '.$sts.'')->result();

								if ($kategori == 'wrk') {
									$data['wew'] = $this->db->query('SELECT opsi_prodi_s2,gelombang,status,jenis_pmb,pekerjaan,count(pekerjaan) as angka from tbl_pmb_s2_2016 where status '.$sts.' group by jenis_pmb,opsi_prodi_s2,gelombang,pekerjaan')->result();
								} elseif ($kategori == 'cty') {
									$data['wew'] = $this->db->query('SELECT opsi_prodi_s2,gelombang,status,jenis_pmb,kota_asl_univ,count(kota_asl_univ) as angka from tbl_pmb_s2_2016 where status '.$sts.' group by jenis_pmb,opsi_prodi_s2,gelombang,kota_asl_univ')->result();
								} elseif ($kategori == 'grd') {
									$data['wew'] = $this->db->query('SELECT opsi_prodi_s2,gelombang,status,jenis_pmb,th_lulus,count(th_lulus) as angka from tbl_pmb_s2_2016 where status '.$sts.' group by jenis_pmb,opsi_prodi_s2,gelombang,th_lulus')->result();
								}

							} else {

								// $data['wew'] = $this->db->query('SELECT * from tbl_pmb_s2_2016 where jenis_pmb = "'.$jenis.'" and status '.$sts.'')->result();

								if ($kategori == 'wrk') {
									$data['wew'] = $this->db->query('SELECT opsi_prodi_s2,gelombang,status,jenis_pmb,pekerjaan,count(pekerjaan) as angka from tbl_pmb_s2_2016 where jenis_pmb = "'.$jenis.'" and status '.$sts.' group by opsi_prodi_s2,gelombang,pekerjaan')->result();
								} elseif ($kategori == 'cty') {
									$data['wew'] = $this->db->query('SELECT opsi_prodi_s2,gelombang,status,jenis_pmb,kota_asl_univ,count(kota_asl_univ) as angka from tbl_pmb_s2_2016 where jenis_pmb = "'.$jenis.'" and status '.$sts.' group by opsi_prodi_s2,gelombang,kota_asl_univ')->result();
								} elseif ($kategori == 'grd') {
									$data['wew'] = $this->db->query('SELECT opsi_prodi_s2,gelombang,status,jenis_pmb,th_lulus,count(th_lulus) as angka from tbl_pmb_s2_2016 where jenis_pmb = "'.$jenis.'" and status '.$sts.' group by opsi_prodi_s2,gelombang,th_lulus')->result();
								}

							}	

						} else {

							if ($jenis == 'AL') {

								// $data['wew'] = $this->db->query('SELECT * from tbl_pmb_s2_2016 where opsi_prodi_s2 = "'.$prodd.'" and status '.$sts.'')->result();

								if ($kategori == 'wrk') {
									$data['wew'] = $this->db->query('SELECT opsi_prodi_s2,gelombang,status,jenis_pmb,pekerjaan,count(pekerjaan) as angka from tbl_pmb_s2_2016 where opsi_prodi_s2 = "'.$prodd.'" and status '.$sts.' group by jenis_pmb,gelombang,pekerjaan')->result();
								} elseif ($kategori == 'cty') {
									$data['wew'] = $this->db->query('SELECT opsi_prodi_s2,gelombang,status,jenis_pmb,kota_asl_univ,count(kota_asl_univ) as angka from tbl_pmb_s2_2016 where opsi_prodi_s2 = "'.$prodd.'" and status '.$sts.' group by jenis_pmb,gelombang,kota_asl_univ')->result();
								} elseif ($kategori == 'grd') {
									$data['wew'] = $this->db->query('SELECT opsi_prodi_s2,gelombang,status,jenis_pmb,th_lulus,count(th_lulus) as angka from tbl_pmb_s2_2016 where opsi_prodi_s2 = "'.$prodd.'" and status '.$sts.' group by jenis_pmb,gelombang,th_lulus')->result();
								}

							} else {

								// $data['wew'] = $this->db->query('SELECT * from tbl_pmb_s2_2016 where opsi_prodi_s2 = "'.$prodd.'" and jenis_pmb = "'.$jenis.'" and status '.$sts.'')->result();

								if ($kategori == 'wrk') {
									$data['wew'] = $this->db->query('SELECT opsi_prodi_s2,gelombang,status,jenis_pmb,pekerjaan,count(pekerjaan) as angka from tbl_pmb_s2_2016 where opsi_prodi_s2 = "'.$prodd.'" and jenis_pmb = "'.$jenis.'" and status '.$sts.' group by gelombang,pekerjaan')->result();
								} elseif ($kategori == 'cty') {
									$data['wew'] = $this->db->query('SELECT opsi_prodi_s2,gelombang,status,jenis_pmb,kota_asl_univ,count(kota_asl_univ) as angka from tbl_pmb_s2_2016 where opsi_prodi_s2 = "'.$prodd.'" and jenis_pmb = "'.$jenis.'" and status '.$sts.' group by gelombang,kota_asl_univ')->result();
								} elseif ($kategori == 'grd') {
									$data['wew'] = $this->db->query('SELECT opsi_prodi_s2,gelombang,status,jenis_pmb,th_lulus,count(th_lulus) as angka from tbl_pmb_s2_2016 where opsi_prodi_s2 = "'.$prodd.'" and jenis_pmb = "'.$jenis.'" and status '.$sts.' group by gelombang,th_lulus')->result();
								}
								
							}

						}
						
					}
						
				} else {

					if ($sts == 'ALL') {

						if ($prodd == 'ALL') {

							if ($jenis == 'AL') {

								// $data['wew'] = $this->db->query('SELECT * from tbl_pmb_s2_2016 where gelombang = "'.$gelombang.'"')->result();

								if ($kategori == 'wrk') {
									$data['wew'] = $this->db->query('SELECT opsi_prodi_s2,gelombang,status,jenis_pmb,pekerjaan,count(pekerjaan) as angka from tbl_pmb_s2_2016 where gelombang = "'.$gelombang.'" group by opsi_prodi_s2,jenis_pmb,status,pekerjaan')->result();
								} elseif ($kategori == 'cty') {
									$data['wew'] = $this->db->query('SELECT opsi_prodi_s2,gelombang,status,jenis_pmb,kota_asl_univ,count(kota_asl_univ) as angka from tbl_pmb_s2_2016 where gelombang = "'.$gelombang.'" group by opsi_prodi_s2,jenis_pmb,status,kota_asl_univ')->result();
								} elseif ($kategori == 'grd') {
									$data['wew'] = $this->db->query('SELECT opsi_prodi_s2,gelombang,status,jenis_pmb,th_lulus,count(th_lulus) as angka from tbl_pmb_s2_2016 where gelombang = "'.$gelombang.'" group by opsi_prodi_s2,jenis_pmb,status,th_lulus')->result();
								}

							} else {

								// $data['wew'] = $this->db->query('SELECT * from tbl_pmb_s2_2016 where jenis_pmb = "'.$jenis.'" and gelombang = "'.$gelombang.'"')->result();

								if ($kategori == 'wrk') {
									$data['wew'] = $this->db->query('SELECT opsi_prodi_s2,gelombang,status,jenis_pmb,pekerjaan,count(pekerjaan) as angka from tbl_pmb_s2_2016 where jenis_pmb = "'.$jenis.'" and gelombang = "'.$gelombang.'" group by opsi_prodi_s2,status,pekerjaan')->result();
								} elseif ($kategori == 'cty') {
									$data['wew'] = $this->db->query('SELECT opsi_prodi_s2,gelombang,status,jenis_pmb,kota_asl_univ,count(kota_asl_univ) as angka from tbl_pmb_s2_2016 where jenis_pmb = "'.$jenis.'" and gelombang = "'.$gelombang.'" group by opsi_prodi_s2,status,kota_asl_univ')->result();
								} elseif ($kategori == 'grd') {
									$data['wew'] = $this->db->query('SELECT opsi_prodi_s2,gelombang,status,jenis_pmb,th_lulus,count(th_lulus) as angka from tbl_pmb_s2_2016 where jenis_pmb = "'.$jenis.'" and gelombang = "'.$gelombang.'" group by opsi_prodi_s2,status,th_lulus')->result();
								}

							}	

						} else {

							if ($jenis == 'AL') {

								// $data['wew'] = $this->db->query('SELECT * from tbl_pmb_s2_2016 where opsi_prodi_s2 = "'.$prodd.'" and gelombang = "'.$gelombang.'"')->result();

								if ($kategori == 'wrk') {
									$data['wew'] = $this->db->query('SELECT opsi_prodi_s2,gelombang,status,jenis_pmb,pekerjaan,count(pekerjaan) as angka from tbl_pmb_s2_2016 where opsi_prodi_s2 = "'.$prodd.'" and gelombang = "'.$gelombang.'" group by jenis_pmb,status,pekerjaan')->result();
								} elseif ($kategori == 'cty') {
									$data['wew'] = $this->db->query('SELECT opsi_prodi_s2,gelombang,status,jenis_pmb,kota_asl_univ,count(kota_asl_univ) as angka from tbl_pmb_s2_2016 where opsi_prodi_s2 = "'.$prodd.'" and gelombang = "'.$gelombang.'" group by jenis_pmb,status,kota_asl_univ')->result();
								} elseif ($kategori == 'grd') {
									$data['wew'] = $this->db->query('SELECT opsi_prodi_s2,gelombang,status,jenis_pmb,th_lulus,count(th_lulus) as angka from tbl_pmb_s2_2016 where opsi_prodi_s2 = "'.$prodd.'" and gelombang = "'.$gelombang.'" group by jenis_pmb,status,th_lulus')->result();
								}

							} else {

								// $data['wew'] = $this->db->query('SELECT * from tbl_pmb_s2_2016 where opsi_prodi_s2 = "'.$prodd.'" and jenis_pmb = "'.$jenis.'" and gelombang = "'.$gelombang.'"')->result();

								if ($kategori == 'wrk') {
									$data['wew'] = $this->db->query('SELECT opsi_prodi_s2,gelombang,status,jenis_pmb,pekerjaan,count(pekerjaan) as angka from tbl_pmb_s2_2016 where opsi_prodi_s2 = "'.$prodd.'" and jenis_pmb = "'.$jenis.'" and gelombang = "'.$gelombang.'" group by status,pekerjaan')->result();
								} elseif ($kategori == 'cty') {
									$data['wew'] = $this->db->query('SELECT opsi_prodi_s2,gelombang,status,jenis_pmb,kota_asl_univ,count(kota_asl_univ) as angka from tbl_pmb_s2_2016 where opsi_prodi_s2 = "'.$prodd.'" and jenis_pmb = "'.$jenis.'" and gelombang = "'.$gelombang.'" group by status,kota_asl_univ')->result();
								} elseif ($kategori == 'grd') {
									$data['wew'] = $this->db->query('SELECT opsi_prodi_s2,gelombang,status,jenis_pmb,th_lulus,count(th_lulus) as angka from tbl_pmb_s2_2016 where opsi_prodi_s2 = "'.$prodd.'" and jenis_pmb = "'.$jenis.'" and gelombang = "'.$gelombang.'" group by status,th_lulus')->result();
								}

							}

						}

					} else {

						if ($prodd == 'ALL') {

							if ($jenis == 'AL') {

								// $data['wew'] = $this->db->query('SELECT * from tbl_pmb_s2_2016 where status  '.$sts.' and gelombang = "'.$gelombang.'"')->result();

								if ($kategori == 'wrk') {
									$data['wew'] = $this->db->query('SELECT opsi_prodi_s2,gelombang,status,jenis_pmb,pekerjaan,count(pekerjaan) as angka from tbl_pmb_s2_2016 where status  '.$sts.' and gelombang = "'.$gelombang.'" group by opsi_prodi_s2,jenis_pmb,pekerjaan')->result();
								} elseif ($kategori == 'cty') {
									$data['wew'] = $this->db->query('SELECT opsi_prodi_s2,gelombang,status,jenis_pmb,kota_asl_univ,count(kota_asl_univ) as angka from tbl_pmb_s2_2016 where status  '.$sts.' and gelombang = "'.$gelombang.'" group by opsi_prodi_s2,jenis_pmb,kota_asl_univ')->result();
								} elseif ($kategori == 'grd') {
									$data['wew'] = $this->db->query('SELECT opsi_prodi_s2,gelombang,status,jenis_pmb,th_lulus,count(th_lulus) as angka from tbl_pmb_s2_2016 where status  '.$sts.' and gelombang = "'.$gelombang.'" group by opsi_prodi_s2,jenis_pmb,th_lulus')->result();
								}

							} else {

								// $data['wew'] = $this->db->query('SELECT * from tbl_pmb_s2_2016 where jenis_pmb = "'.$jenis.'" and status '.$sts.' and gelombang = "'.$gelombang.'"')->result();

								if ($kategori == 'wrk') {
									$data['wew'] = $this->db->query('SELECT opsi_prodi_s2,gelombang,status,jenis_pmb,pekerjaan,count(pekerjaan) as angka from tbl_pmb_s2_2016 where jenis_pmb = "'.$jenis.'" and status '.$sts.' and gelombang = "'.$gelombang.'" group by opsi_prodi_s2,pekerjaan')->result();
								} elseif ($kategori == 'cty') {
									$data['wew'] = $this->db->query('SELECT opsi_prodi_s2,gelombang,status,jenis_pmb,kota_asl_univ,count(kota_asl_univ) as angka from tbl_pmb_s2_2016 where jenis_pmb = "'.$jenis.'" and status '.$sts.' and gelombang = "'.$gelombang.'" group by opsi_prodi_s2,kota_asl_univ')->result();
								} elseif ($kategori == 'grd') {
									$data['wew'] = $this->db->query('SELECT opsi_prodi_s2,gelombang,status,jenis_pmb,th_lulus,count(th_lulus) as angka from tbl_pmb_s2_2016 where jenis_pmb = "'.$jenis.'" and status '.$sts.' and gelombang = "'.$gelombang.'" group by opsi_prodi_s2,th_lulus')->result();
								}

							}	

						} else {

							if ($jenis == 'AL') {

								// $data['wew'] = $this->db->query('SELECT * from tbl_pmb_s2_2016 where opsi_prodi_s2 = "'.$prodd.'" and status '.$sts.' and gelombang = "'.$gelombang.'"')->result();

								if ($kategori == 'wrk') {
									$data['wew'] = $this->db->query('SELECT opsi_prodi_s2,gelombang,status,jenis_pmb,pekerjaan,count(pekerjaan) as angka from tbl_pmb_s2_2016 where opsi_prodi_s2 = "'.$prodd.'" and status '.$sts.' and gelombang = "'.$gelombang.'" group by jenis_pmb,pekerjaan')->result();
								} elseif ($kategori == 'cty') {
									$data['wew'] = $this->db->query('SELECT opsi_prodi_s2,gelombang,status,jenis_pmb,kota_asl_univ,count(kota_asl_univ) as angka from tbl_pmb_s2_2016 where opsi_prodi_s2 = "'.$prodd.'" and status '.$sts.' and gelombang = "'.$gelombang.'" group by jenis_pmb,kota_asl_univ')->result();
								} elseif ($kategori == 'grd') {
									$data['wew'] = $this->db->query('SELECT opsi_prodi_s2,gelombang,status,jenis_pmb,th_lulus,count(th_lulus) as angka from tbl_pmb_s2_2016 where opsi_prodi_s2 = "'.$prodd.'" and status '.$sts.' and gelombang = "'.$gelombang.'" group by jenis_pmb,th_lulus')->result();
								}

							} else {

								// $data['wew'] = $this->db->query('SELECT * from tbl_pmb_s2_2016 where opsi_prodi_s2 = "'.$prodd.'" and jenis_pmb = "'.$jenis.'" and status '.$sts.' and gelombang = "'.$gelombang.'"')->result();

								if ($kategori == 'wrk') {
									$data['wew'] = $this->db->query('SELECT opsi_prodi_s2,gelombang,status,jenis_pmb,pekerjaan,count(pekerjaan) as angka from tbl_pmb_s2_2016 where opsi_prodi_s2 = "'.$prodd.'" and jenis_pmb = "'.$jenis.'" and status '.$sts.' and gelombang = "'.$gelombang.'" group by pekerjaan')->result();
								} elseif ($kategori == 'cty') {
									$data['wew'] = $this->db->query('SELECT opsi_prodi_s2,gelombang,status,jenis_pmb,kota_asl_univ,count(kota_asl_univ) as angka from tbl_pmb_s2_2016 where opsi_prodi_s2 = "'.$prodd.'" and jenis_pmb = "'.$jenis.'" and status '.$sts.' and gelombang = "'.$gelombang.'" group by kota_asl_univ')->result();
								} elseif ($kategori == 'grd') {
									$data['wew'] = $this->db->query('SELECT opsi_prodi_s2,gelombang,status,jenis_pmb,th_lulus,count(th_lulus) as angka from tbl_pmb_s2_2016 where opsi_prodi_s2 = "'.$prodd.'" and jenis_pmb = "'.$jenis.'" and status '.$sts.' and gelombang = "'.$gelombang.'" group by th_lulus')->result();
								}
								
							}

						}
						
					}
						
				}	
				
				$this->load->view('excel_report_jml_s2', $data);

			} 

		} else {

			if ($program == 'S1') {

				$x16 = $this->db->query("SELECT workdad,workmom,didik_ayah,didik_ibu,jenis_sch_maba,asal_sch_maba,lulus_maba from tbl_form_camaba")->result();

				if ($gelombang == 'ALL') {

						if ($sts == 'ALL') {

							if ($prodd == 'ALL') {

								if ($jenis == 'AL') {

									// $data['wow'] = $this->db->query('SELECT * from tbl_form_camaba_2016 group by prodi,gelombang,jenis_pmb,status')->result();

									if ($kategori == 'edu0') {
										$data['wow'] =$this->db->query("SELECT prodi,gelombang,jenis_pmb,status,didik_ayah,count(didik_ayah) as hitung from tbl_form_camaba where workdad is not null group by didik_ayah,prodi,gelombang,jenis_pmb,status")->result();
									} elseif ($kategori == 'edu1') {
										$data['wow'] =$this->db->query("SELECT prodi,gelombang,jenis_pmb,status,didik_ibu,count(didik_ibu) as hitung from tbl_form_camaba where workdad is not null group by didik_ibu,prodi,gelombang,jenis_pmb,status")->result();
									} elseif ($kategori == 'wrk0') {
										$data['wow'] =$this->db->query("SELECT prodi,gelombang,jenis_pmb,status,workdad,count(workdad) as hitung from tbl_form_camaba where workdad is not null group by workdad,prodi,gelombang,jenis_pmb,status")->result();
									} elseif ($kategori == 'wrk1') {	
										$data['wow'] =$this->db->query("SELECT prodi,gelombang,jenis_pmb,status,workmom,count(workmom) as hitung from tbl_form_camaba where workmom is not null group by workmom,prodi,gelombang,jenis_pmb,status")->result();
									} elseif ($kategori == 'sch') {
										$data['wow'] =$this->db->query("SELECT prodi,gelombang,jenis_pmb,status,jenis_sch_maba,count(jenis_sch_maba) as hitung from tbl_form_camaba a where jenis_sch_maba is not null group by jenis_sch_maba,prodi,gelombang,jenis_pmb,status")->result();
									} elseif ($kategori == 'cty') {
										$data['wow'] =$this->db->query("SELECT prodi,gelombang,jenis_pmb,status,kota_sch_maba,count(kota_sch_maba) as hitung from tbl_form_camaba a where kota_sch_maba is not null group by kota_sch_maba,prodi,gelombang,jenis_pmb,status")->result();
									} elseif ($kategori == 'grd') {
										$data['wow'] =$this->db->query("SELECT prodi,gelombang,jenis_pmb,status,lulus_maba,count(lulus_maba) as hitung from tbl_form_camaba a where lulus_maba is not null group by lulus_maba,prodi,gelombang,jenis_pmb,status")->result();
									}									

								} else {

									// $data['wow'] = $this->db->query('SELECT * from tbl_form_camaba a where jenis_pmb = "'.$jenis.'"  group by prodi,gelombang,status')->result();

									if ($kategori == 'edu0') {
										$data['wow'] =$this->db->query("SELECT prodi,gelombang,jenis_pmb,status,didik_ayah,count(didik_ayah) as hitung from tbl_form_camaba a where jenis_pmb = '".$jenis."' and didik_ayah is not null group by prodi,gelombang,status,didik_ayah")->result();
									} elseif($kategori == 'edu1') {	
										$data['wow'] =$this->db->query("SELECT prodi,gelombang,jenis_pmb,status,didik_ibu,count(didik_ibu) as hitung from tbl_form_camaba a where jenis_pmb = '".$jenis."' and didik_ibu is not null group by prodi,gelombang,status,didik_ibu")->result();
									} elseif ($kategori == 'wrk0') {
										$data['wow'] =$this->db->query("SELECT prodi,gelombang,jenis_pmb,status,workdad,count(workdad) as hitung from tbl_form_camaba a where jenis_pmb = '".$jenis."' and workdad is not null group by prodi,gelombang,status,workdad")->result();
									} elseif ($kategori == 'wrk1') {
										$data['wow'] =$this->db->query("SELECT prodi,gelombang,jenis_pmb,status,workmom,count(workmom) as hitung from tbl_form_camaba a where jenis_pmb = '".$jenis."' and workmom is not null group by prodi,gelombang,status,workmom")->result();
									
									} elseif ($kategori == 'sch') {
										$data['wow'] =$this->db->query("SELECT prodi,gelombang,jenis_pmb,status,jenis_sch_maba,count(jenis_sch_maba) as hitung from tbl_form_camaba a where jenis_pmb = '".$jenis."' and jenis_sch_maba is not null group by prodi,gelombang,status,jenis_sch_maba")->result();
									} elseif ($kategori == 'cty') {
										$data['wow'] =$this->db->query("SELECT prodi,gelombang,jenis_pmb,status,kota_sch_maba,count(kota_sch_maba) as hitung from tbl_form_camaba a where jenis_pmb = '".$jenis."' and kota_sch_maba is not null group by prodi,gelombang,status,kota_sch_maba")->result();
									} elseif ($kategori == 'grd') {
										$data['wow'] =$this->db->query("SELECT prodi,gelombang,jenis_pmb,status,lulus_maba,count(lulus_maba) as hitung from tbl_form_camaba a where jenis_pmb = '".$jenis."' and lulus_maba is not null group by prodi,gelombang,status,lulus_maba")->result();
									}

								}	

							} else {

								if ($jenis == 'AL') {

									// $data['wow'] = $this->db->query('SELECT * from tbl_form_camaba where prodi = "'.$prodd.'"  group by gelombang,jenis_pmb,status')->result();

									if ($kategori == 'edu0') {
										$data['wow'] =$this->db->query("SELECT prodi,gelombang,jenis_pmb,status,didik_ayah,count(didik_ayah) as hitung from tbl_form_camaba where prodi = '".$prodd."' and didik_ayah is not null group by gelombang,jenis_pmb,status,didik_ayah")->result();
									} elseif ($kategori == 'edu1') {
										$data['wow'] =$this->db->query("SELECT prodi,gelombang,jenis_pmb,status,didik_ibu,count(didik_ibu) as hitung from tbl_form_camaba where prodi = '".$prodd."' and didik_ibu is not null group by gelombang,jenis_pmb,status,didik_ibu")->result();
									
									} elseif ($kategori == 'wrk0') {
										$data['wow'] =$this->db->query("SELECT prodi,gelombang,jenis_pmb,status,workdad,count(workdad) as hitung from tbl_form_camaba where prodi = '".$prodd."' and workdad is not null group by gelombang,jenis_pmb,status,workdad")->result();
									} elseif ($kategori == 'wrk1') {
										$data['wow'] =$this->db->query("SELECT prodi,gelombang,jenis_pmb,status,workmom,count(workmom) as hitung from tbl_form_camaba where prodi = '".$prodd."' and workmom is not null group by gelombang,jenis_pmb,status,workmom")->result();
									
									} elseif ($kategori == 'sch') {
										$data['wow'] =$this->db->query("SELECT prodi,gelombang,jenis_pmb,status,jenis_sch_maba,count(jenis_sch_maba) as hitung from tbl_form_camaba where prodi = '".$prodd."' and jenis_sch_maba is not null group by gelombang,jenis_pmb,status,jenis_sch_maba")->result();
									} elseif ($kategori == 'cty') {
										$data['wow'] =$this->db->query("SELECT prodi,gelombang,jenis_pmb,status,kota_sch_maba,count(kota_sch_maba) as hitung from tbl_form_camaba where prodi = '".$prodd."' and kota_sch_maba is not null group by gelombang,jenis_pmb,status,kota_sch_maba")->result();
									} elseif ($kategori == 'grd') {
										$data['wow'] =$this->db->query("SELECT prodi,gelombang,jenis_pmb,status,lulus_maba,count(lulus_maba) as hitung from tbl_form_camaba where prodi = '".$prodd."' and lulus_maba is not null group by gelombang,jenis_pmb,status,lulus_maba")->result();
									}

									

								} else {

									// $data['wow'] = $this->db->query('SELECT * from tbl_form_camaba where prodi = "'.$prodd.'" and jenis_pmb = "'.$jenis.'"  group by gelombang,status')->result();

									if ($kategori == 'edu0') {
										$data['wow'] =$this->db->query("SELECT prodi,gelombang,jenis_pmb,status,didik_ayah,count(didik_ayah) as hitung from tbl_form_camaba where prodi = '".$prodd."' and jenis_pmb = '".$jenis."' and didik_ayah is not null group by gelombang,status,didik_ayah ")->result();
									} elseif ($kategori == 'edu1') {	
										$data['wow'] =$this->db->query("SELECT prodi,gelombang,jenis_pmb,status,didik_ibu,count(didik_ibu) as hitung from tbl_form_camaba where prodi = '".$prodd."' and jenis_pmb = '".$jenis."' and didik_ibu is not null group by gelombang,status,didik_ibu ")->result();
									} elseif ($kategori == 'wrk0') {
										$data['wow'] =$this->db->query("SELECT prodi,gelombang,jenis_pmb,status,workdad,count(workdad) as hitung from tbl_form_camaba where prodi = '".$prodd."' and jenis_pmb = '".$jenis."' and workdad is not null group by gelombang,status,workdad ")->result();
									} elseif ($kategori == 'wrk1') {	
										$data['wow'] =$this->db->query("SELECT prodi,gelombang,jenis_pmb,status,workmom,count(workmom) as hitung from tbl_form_camaba where prodi = '".$prodd."' and jenis_pmb = '".$jenis."' and workmom is not null group by gelombang,status,workmom ")->result();
									} elseif ($kategori == 'sch') {
										$data['wow'] =$this->db->query("SELECT prodi,gelombang,jenis_pmb,status,jenis_sch_maba,count(jenis_sch_maba) as hitung from tbl_form_camaba where prodi = '".$prodd."' and jenis_pmb = '".$jenis."' and jenis_sch_maba is not null group by gelombang,status,jenis_sch_maba ")->result();
									} elseif ($kategori == 'cty') {
										$data['wow'] =$this->db->query("SELECT prodi,gelombang,jenis_pmb,status,kota_sch_maba,count(kota_sch_maba) as hitung from tbl_form_camaba where prodi = '".$prodd."' and jenis_pmb = '".$jenis."' and kota_sch_maba is not null group by gelombang,status,kota_sch_maba ")->result();
									} elseif ($kategori == 'grd') {
										$data['wow'] =$this->db->query("SELECT prodi,gelombang,jenis_pmb,status,lulus_maba,count(lulus_maba) as hitung from tbl_form_camaba where prodi = '".$prodd."' and jenis_pmb = '".$jenis."' and lulus_maba is not null group by gelombang,status,lulus_maba ")->result();
									}

								}

							}

						} else {

							if ($prodd == 'ALL') {

								if ($jenis == 'AL') {

									// $data['wow'] = $this->db->query('SELECT * from tbl_form_camaba where status '.$sts.'  group by prodi,gelombang,jenis_pmb')->result();

									if ($kategori == 'edu0') {
										$data['wow'] =$this->db->query("SELECT prodi,gelombang,jenis_pmb,status,didik_ayah,count(didik_ayah) as hitung from tbl_form_camaba where status ".$sts."  and didik_ayah is not null group by prodi,gelombang,jenis_pmb,didik_ayah")->result();
									} elseif ($kategori == 'edu1') {	
										$data['wow'] =$this->db->query("SELECT prodi,gelombang,jenis_pmb,status,didik_ibu,count(didik_ibu) as hitung from tbl_form_camaba where status ".$sts."  and didik_ibu is not null group by prodi,gelombang,jenis_pmb,didik_ibu")->result();
									} elseif ($kategori == 'wrk0') {
										$data['wow'] =$this->db->query("SELECT prodi,gelombang,jenis_pmb,status,workdad,count(workdad) as hitung from tbl_form_camaba where status ".$sts."  and workdad is not null group by prodi,gelombang,jenis_pmb,workdad")->result();
									} elseif ($kategori == 'wrk1') {	
										$data['wow'] =$this->db->query("SELECT prodi,gelombang,jenis_pmb,status,workmom,count(workmom) as hitung from tbl_form_camaba where status ".$sts."  and workmom is not null group by prodi,gelombang,jenis_pmb,workmom")->result();
									} elseif ($kategori == 'sch') {
										$data['wow'] =$this->db->query("SELECT prodi,gelombang,jenis_pmb,status,jenis_sch_maba,count(jenis_sch_maba) as hitung from tbl_form_camaba where status ".$sts."  and jenis_sch_maba is not null group by prodi,gelombang,jenis_pmb,jenis_sch_maba")->result();
									} elseif ($kategori == 'cty') {
										$data['wow'] =$this->db->query("SELECT prodi,gelombang,jenis_pmb,status,kota_sch_maba,count(kota_sch_maba) as hitung from tbl_form_camaba where status ".$sts."  and kota_sch_maba is not null group by prodi,gelombang,jenis_pmb,kota_sch_maba")->result();
									} elseif ($kategori == 'grd') {
										$data['wow'] =$this->db->query("SELECT prodi,gelombang,jenis_pmb,status,lulus_maba,count(lulus_maba) as hitung from tbl_form_camaba where status ".$sts."  and lulus_maba is not null group by prodi,gelombang,jenis_pmb,lulus_maba")->result();
									}

								} else {

									// $data['wow'] = $this->db->query('SELECT * from tbl_form_camaba where jenis_pmb = "'.$jenis.'" and status '.$sts.'  group by prodi,gelombang')->result();

									if ($kategori == 'edu0') {
										$data['wow'] =$this->db->query("SELECT prodi,gelombang,jenis_pmb,status,didik_ayah,count(didik_ayah) as hitung from tbl_form_camaba where jenis_pmb = '".$jenis."' and status ".$sts."  and didik_ayah is not null group by prodi,gelombang,didik_ayah")->result();
									} elseif ($kategori == 'edu1') {	
										$data['wow'] =$this->db->query("SELECT prodi,gelombang,jenis_pmb,status,didik_ibu,count(didik_ibu) as hitung from tbl_form_camaba where jenis_pmb = '".$jenis."' and status ".$sts."  and didik_ibu is not null group by prodi,gelombang,didik_ibu")->result();
									} elseif ($kategori == 'wrk0') {
										$data['wow'] =$this->db->query("SELECT prodi,gelombang,jenis_pmb,status,workdad,count(workdad) as hitung from tbl_form_camaba where jenis_pmb = '".$jenis."' and status ".$sts."  and workdad is not null group by prodi,gelombang,workdad")->result();
									} elseif ($kategori == 'wrk1') {	
										$data['wow'] =$this->db->query("SELECT prodi,gelombang,jenis_pmb,status,workmom,count(workmom) as hitung from tbl_form_camaba where jenis_pmb = '".$jenis."' and status ".$sts."  and workmom is not null group by prodi,gelombang,workmom")->result();
									} elseif ($kategori == 'sch') {
										$data['wow'] =$this->db->query("SELECT prodi,gelombang,jenis_pmb,status,jenis_sch_maba,count(jenis_sch_maba) as hitung from tbl_form_camaba where jenis_pmb = '".$jenis."' and status ".$sts."  and jenis_sch_maba is not null group by prodi,gelombang,jenis_sch_maba")->result();
									} elseif ($kategori == 'cty') {
										$data['wow'] =$this->db->query("SELECT prodi,gelombang,jenis_pmb,status,kota_sch_maba,count(kota_sch_maba) as hitung from tbl_form_camaba where jenis_pmb = '".$jenis."' and status ".$sts."  and kota_sch_maba is not null group by prodi,gelombang,kota_sch_maba")->result();
									} elseif ($kategori == 'grd') {
										$data['wow'] =$this->db->query("SELECT prodi,gelombang,jenis_pmb,status,lulus_maba,count(lulus_maba) as hitung from tbl_form_camaba where jenis_pmb = '".$jenis."' and status ".$sts."  and lulus_maba is not null group by prodi,gelombang,lulus_maba")->result();
									}

								}	

							} else {

								if ($jenis == 'AL') {

									// $data['wow'] = $this->db->query('SELECT * from tbl_form_camaba where prodi = "'.$prodd.'" and status '.$sts.'  group by gelombang,jenis_pmb')->result();

									if ($kategori == 'edu0') {
										$data['wow'] =$this->db->query("SELECT prodi,gelombang,jenis_pmb,status,didik_ayah,count(didik_ayah) as hitung from tbl_form_camaba where status ".$sts." and prodi = '".$prodd."'  and didik_ayah is not null group by gelombang,jenis_pmb,didik_ayah")->result();
									} elseif ($kategori == 'edu1') {	
										$data['wow'] =$this->db->query("SELECT prodi,gelombang,jenis_pmb,status,didik_ibu,count(didik_ibu) as hitung from tbl_form_camaba where status ".$sts." and prodi = '".$prodd."'  and didik_ibu is not null group by gelombang,jenis_pmb,didik_ibu")->result();
									} elseif ($kategori == 'wrk0') {
										$data['wow'] =$this->db->query("SELECT prodi,gelombang,jenis_pmb,status,workdad,count(workdad) as hitung from tbl_form_camaba where status ".$sts." and prodi = '".$prodd."'  and workdad is not null group by gelombang,jenis_pmb,workdad")->result();
									} elseif ($kategori == 'wrk1') {	
										$data['wow'] =$this->db->query("SELECT prodi,gelombang,jenis_pmb,status,workmom,count(workmom) as hitung from tbl_form_camaba where status ".$sts." and prodi = '".$prodd."'  and workmom is not null group by gelombang,jenis_pmb,workmom")->result();
									} elseif ($kategori == 'sch') {
										$data['wow'] =$this->db->query("SELECT prodi,gelombang,jenis_pmb,status,jenis_sch_maba,count(jenis_sch_maba) as hitung from tbl_form_camaba where status ".$sts." and prodi = '".$prodd."'  and jenis_sch_maba is not null group by gelombang,jenis_pmb,jenis_sch_maba")->result();
									} elseif ($kategori == 'cty') {
										$data['wow'] =$this->db->query("SELECT prodi,gelombang,jenis_pmb,status,kota_sch_maba,count(kota_sch_maba) as hitung from tbl_form_camaba where status ".$sts." and prodi = '".$prodd."'  and kota_sch_maba is not null group by gelombang,jenis_pmb,kota_sch_maba")->result();
									} elseif ($kategori == 'grd') {
										$data['wow'] =$this->db->query("SELECT prodi,gelombang,jenis_pmb,status,lulus_maba,count(lulus_maba) as hitung from tbl_form_camaba where status ".$sts." and prodi = '".$prodd."'  and lulus_maba is not null group by gelombang,jenis_pmb,lulus_maba")->result();
									}

								} else {

									// $data['wow'] = $this->db->query('SELECT * from tbl_form_camaba where prodi = "'.$prodd.'" and jenis_pmb = "'.$jenis.'" and status '.$sts.'  group by gelombang')->result();

									if ($kategori == 'edu0') {
										$data['wow'] =$this->db->query("SELECT prodi,gelombang,jenis_pmb,status,didik_ayah,count(didik_ayah) as hitung from tbl_form_camaba where status ".$sts." and prodi = '".$prodd."' and jenis_pmb = '".$jenis."'  and didik_ayah is not null group by gelombang,didik_ayah")->result();
									} elseif ($kategori == 'edu1') {	
										$data['wow'] =$this->db->query("SELECT prodi,gelombang,jenis_pmb,status,didik_ibu,count(didik_ibu) as hitung from tbl_form_camaba where status ".$sts." and prodi = '".$prodd."' and jenis_pmb = '".$jenis."'  and didik_ibu is not null group by gelombang,didik_ibu")->result();
									} elseif ($kategori == 'wrk0') {
										$data['wow'] =$this->db->query("SELECT prodi,gelombang,jenis_pmb,status,workdad,count(workdad) as hitung from tbl_form_camaba where status ".$sts." and prodi = '".$prodd."' and jenis_pmb = '".$jenis."'  and workdad is not null group by gelombang,workdad")->result();
									} elseif ($kategori == 'wrk1') {	
										$data['wow'] =$this->db->query("SELECT prodi,gelombang,jenis_pmb,status,workmom,count(workmom) as hitung from tbl_form_camaba where status ".$sts." and prodi = '".$prodd."' and jenis_pmb = '".$jenis."'  and workmom is not null group by gelombang,workmom")->result();
									} elseif ($kategori == 'sch') {
										$data['wow'] =$this->db->query("SELECT prodi,gelombang,jenis_pmb,status,jenis_sch_maba,count(jenis_sch_maba) as hitung from tbl_form_camaba where status ".$sts." and prodi = '".$prodd."' and jenis_pmb = '".$jenis."'  and jenis_sch_maba is not null group by gelombang,jenis_sch_maba")->result();
									} elseif ($kategori == 'cty') {
										$data['wow'] =$this->db->query("SELECT prodi,gelombang,jenis_pmb,status,kota_sch_maba,count(kota_sch_maba) as hitung from tbl_form_camaba where status ".$sts." and prodi = '".$prodd."' and jenis_pmb = '".$jenis."'  and kota_sch_maba is not null group by gelombang,kota_sch_maba")->result();
									} elseif ($kategori == 'grd') {
										$data['wow'] =$this->db->query("SELECT prodi,gelombang,jenis_pmb,status,lulus_maba,count(lulus_maba) as hitung from tbl_form_camaba where status ".$sts." and prodi = '".$prodd."' and jenis_pmb = '".$jenis."'  and lulus_maba is not null group by gelombang,lulus_maba")->result();
									}
									
								}

							}
							
						}
							
					} else {

						if ($sts == 'ALL') {

							if ($prodd == 'ALL') {

								if ($jenis == 'AL') {

									// $data['wow'] = $this->db->query('SELECT * from tbl_form_camaba where gelombang = "'.$gelombang.'"  group by prodi,jenis_pmb,status')->result();

									if ($kategori == 'edu0') {
										$data['wow'] = $this->db->query('SELECT prodi,gelombang,jenis_pmb,status,didik_ayah,count(didik_ayah) as hitung from tbl_form_camaba where gelombang = "'.$gelombang.'" and didik_ayah is not null group by prodi,jenis_pmb,status,didik_ayah')->result();
									} elseif ($kategori == 'edu1') {	
										$data['wow'] = $this->db->query('SELECT prodi,gelombang,jenis_pmb,status,didik_ibu,count(didik_ibu) as hitung from tbl_form_camaba where gelombang = "'.$gelombang.'" and didik_ibu is not null group by prodi,jenis_pmb,status,didik_ibu')->result();
									} elseif ($kategori == 'wrk0') {
										$data['wow'] = $this->db->query('SELECT prodi,gelombang,jenis_pmb,status,workdad,count(workdad) as hitung from tbl_form_camaba where gelombang = "'.$gelombang.'" and workdad is not null group by prodi,jenis_pmb,status,workdad')->result();
									} elseif ($kategori == 'wrk1') {	
										$data['wow'] = $this->db->query('SELECT prodi,gelombang,jenis_pmb,status,workmom,count(workmom) as hitung from tbl_form_camaba where gelombang = "'.$gelombang.'" and workmom is not null group by prodi,jenis_pmb,status,workmom')->result();
									} elseif ($kategori == 'sch') {
										$data['wow'] = $this->db->query('SELECT prodi,gelombang,jenis_pmb,status,jenis_sch_maba,count(jenis_sch_maba) as hitung from tbl_form_camaba where gelombang = "'.$gelombang.'" and jenis_sch_maba is not null group by prodi,jenis_pmb,status,jenis_sch_maba')->result();
									} elseif ($kategori == 'cty') {
										$data['wow'] = $this->db->query('SELECT prodi,gelombang,jenis_pmb,status,kota_sch_maba,count(kota_sch_maba) as hitung from tbl_form_camaba where gelombang = "'.$gelombang.'" and kota_sch_maba is not null group by prodi,jenis_pmb,status,kota_sch_maba')->result();
									} elseif ($kategori == 'grd') {
										$data['wow'] = $this->db->query('SELECT prodi,gelombang,jenis_pmb,status,lulus_maba,count(lulus_maba) as hitung from tbl_form_camaba where gelombang = "'.$gelombang.'" and lulus_maba is not null group by prodi,jenis_pmb,status,lulus_maba')->result();
									}

								} else {

									// $data['wow'] = $this->db->query('SELECT * from tbl_form_camaba where jenis_pmb = "'.$jenis.'" and gelombang = "'.$gelombang.'"  group by prodi,status')->result();

									if ($kategori == 'edu0') {
										$data['wow'] = $this->db->query('SELECT prodi,gelombang,jenis_pmb,status,didik_ayah,count(didik_ayah) as hitung from tbl_form_camaba where jenis_pmb = "'.$jenis.'" and gelombang = "'.$gelombang.'" and didik_ayah is not null group by prodi,status,didik_ayah')->result();
									} elseif ($kategori == 'edu1') {	
										$data['wow'] = $this->db->query('SELECT prodi,gelombang,jenis_pmb,status,didik_ibu,count(didik_ibu) as hitung from tbl_form_camaba where jenis_pmb = "'.$jenis.'" and gelombang = "'.$gelombang.'" and didik_ibu is not null group by prodi,status,didik_ibu')->result();
									} elseif ($kategori == 'wrk0') {
										$data['wow'] = $this->db->query('SELECT prodi,gelombang,jenis_pmb,status,workdad,count(workdad) as hitung from tbl_form_camaba where jenis_pmb = "'.$jenis.'" and gelombang = "'.$gelombang.'" and workdad is not null group by prodi,status,workdad')->result();
									} elseif ($kategori == 'wrk1') {	
										$data['wow'] = $this->db->query('SELECT prodi,gelombang,jenis_pmb,status,workmom,count(workmom) as hitung from tbl_form_camaba where jenis_pmb = "'.$jenis.'" and gelombang = "'.$gelombang.'" and workmom is not null group by prodi,status,workmom')->result();
									} elseif ($kategori == 'sch') {
										$data['wow'] = $this->db->query('SELECT prodi,gelombang,jenis_pmb,status,jenis_sch_maba,count(jenis_sch_maba) as hitung from tbl_form_camaba where jenis_pmb = "'.$jenis.'" and gelombang = "'.$gelombang.'" and jenis_sch_maba is not null group by prodi,status,jenis_sch_maba')->result();
									} elseif ($kategori == 'cty') {
										$data['wow'] = $this->db->query('SELECT prodi,gelombang,jenis_pmb,status,kota_sch_maba,count(kota_sch_maba) as hitung from tbl_form_camaba where jenis_pmb = "'.$jenis.'" and gelombang = "'.$gelombang.'" and kota_sch_maba is not null group by prodi,status,kota_sch_maba')->result();
									} elseif ($kategori == 'grd') {
										$data['wow'] = $this->db->query('SELECT prodi,gelombang,jenis_pmb,status,lulus_maba,count(lulus_maba) as hitung from tbl_form_camaba where jenis_pmb = "'.$jenis.'" and gelombang = "'.$gelombang.'" and lulus_maba is not null group by prodi,status,lulus_maba')->result();
									}

								}	

							} else {

								if ($jenis == 'AL') {

									// $data['wow'] = $this->db->query('SELECT * from tbl_form_camaba where prodi = "'.$prodd.'" and gelombang = "'.$gelombang.'"  group by jenis_pmb,status')->result();

									if ($kategori == 'edu0') {
										$data['wow'] = $this->db->query('SELECT prodi,gelombang,jenis_pmb,status,didik_ayah,count(didik_ayah) as hitung from tbl_form_camaba where prodi = "'.$prodd.'" and gelombang = "'.$gelombang.'" and didik_ayah is not null group by jenis_pmb,status,didik_ayah')->result();
									} elseif ($kategori == 'edu1') {	
										$data['wow'] = $this->db->query('SELECT prodi,gelombang,jenis_pmb,status,didik_ibu,count(didik_ibu) as hitung from tbl_form_camaba where prodi = "'.$prodd.'" and gelombang = "'.$gelombang.'" and didik_ibu is not null group by jenis_pmb,status,didik_ibu')->result();
									} elseif ($kategori == 'wrk0') {
										$data['wow'] = $this->db->query('SELECT prodi,gelombang,jenis_pmb,status,workdad,count(workdad) as hitung from tbl_form_camaba where prodi = "'.$prodd.'" and gelombang = "'.$gelombang.'" and workdad is not null group by jenis_pmb,status,workdad')->result();
									} elseif ($kategori == 'wrk1') {	
										$data['wow'] = $this->db->query('SELECT prodi,gelombang,jenis_pmb,status,workmom,count(workmom) as hitung from tbl_form_camaba where prodi = "'.$prodd.'" and gelombang = "'.$gelombang.'" and workmom is not null group by jenis_pmb,status,workmom')->result();
									} elseif ($kategori == 'sch') {
										$data['wow'] = $this->db->query('SELECT prodi,gelombang,jenis_pmb,status,jenis_sch_maba,count(jenis_sch_maba) as hitung from tbl_form_camaba where prodi = "'.$prodd.'" and gelombang = "'.$gelombang.'" and jenis_sch_maba is not null group by jenis_pmb,status,jenis_sch_maba')->result();
									} elseif ($kategori == 'cty') {
										$data['wow'] = $this->db->query('SELECT prodi,gelombang,jenis_pmb,status,kota_sch_maba,count(kota_sch_maba) as hitung from tbl_form_camaba where prodi = "'.$prodd.'" and gelombang = "'.$gelombang.'" and kota_sch_maba is not null group by jenis_pmb,status,kota_sch_maba')->result();
									} elseif ($kategori == 'grd') {
										$data['wow'] = $this->db->query('SELECT prodi,gelombang,jenis_pmb,status,lulus_maba,count(lulus_maba) as hitung from tbl_form_camaba where prodi = "'.$prodd.'" and gelombang = "'.$gelombang.'" and lulus_maba is not null group by jenis_pmb,status,lulus_maba')->result();
									}

								} else {

									// $data['wow'] = $this->db->query('SELECT * from tbl_form_camaba where prodi = "'.$prodd.'" and jenis_pmb = "'.$jenis.'" and gelombang = "'.$gelombang.'"  group by status')->result();

									if ($kategori == 'edu0') {
										$data['wow'] = $this->db->query('SELECT prodi,gelombang,jenis_pmb,status,didik_ayah,count(didik_ayah) as hitung from tbl_form_camaba where prodi = "'.$prodd.'" and jenis_pmb = "'.$jenis.'" and gelombang = "'.$gelombang.'" and didik_ayah is not null group by status,didik_ayah')->result();
									} elseif ($kategori == 'edu1') {	
										$data['wow'] = $this->db->query('SELECT prodi,gelombang,jenis_pmb,status,didik_ibu,count(didik_ibu) as hitung from tbl_form_camaba where prodi = "'.$prodd.'" and jenis_pmb = "'.$jenis.'" and gelombang = "'.$gelombang.'" and didik_ibu is not null group by status,didik_ibu')->result();
									} elseif ($kategori == 'wrk0') {
										$data['wow'] = $this->db->query('SELECT prodi,gelombang,jenis_pmb,status,workdad,count(workdad) as hitung from tbl_form_camaba where prodi = "'.$prodd.'" and jenis_pmb = "'.$jenis.'" and gelombang = "'.$gelombang.'" and workdad is not null group by status,workdad')->result();
									} elseif ($kategori == 'wrk1') {	
										$data['wow'] = $this->db->query('SELECT prodi,gelombang,jenis_pmb,status,workmom,count(workmom) as hitung from tbl_form_camaba where prodi = "'.$prodd.'" and jenis_pmb = "'.$jenis.'" and gelombang = "'.$gelombang.'" and workmom is not null group by status,workmom')->result();
									} elseif ($kategori == 'sch') {
										$data['wow'] = $this->db->query('SELECT prodi,gelombang,jenis_pmb,status,jenis_sch_maba,count(jenis_sch_maba) as hitung from tbl_form_camaba where prodi = "'.$prodd.'" and jenis_pmb = "'.$jenis.'" and gelombang = "'.$gelombang.'" and jenis_sch_maba is not null group by status,jenis_sch_maba')->result();
									} elseif ($kategori == 'cty') {
										$data['wow'] = $this->db->query('SELECT prodi,gelombang,jenis_pmb,status,kota_sch_maba,count(kota_sch_maba) as hitung from tbl_form_camaba where prodi = "'.$prodd.'" and jenis_pmb = "'.$jenis.'" and gelombang = "'.$gelombang.'" and kota_sch_maba is not null group by status,kota_sch_maba')->result();
									} elseif ($kategori == 'grd') {
										$data['wow'] = $this->db->query('SELECT prodi,gelombang,jenis_pmb,status,lulus_maba,count(lulus_maba) as hitung from tbl_form_camaba where prodi = "'.$prodd.'" and jenis_pmb = "'.$jenis.'" and gelombang = "'.$gelombang.'" and lulus_maba is not null group by status,lulus_maba')->result();
									}

									// $data['hits1'] = $this->db->query('SELECT workdad,count(workdad) as hitung from tbl_form_camaba where prodi = "'.$prodd.'" and jenis_pmb = "'.$jenis.'" and gelombang = "'.$gelombang.'" and workdad is not null group by workdad')->result();

								}

							}

						} else {

							if ($prodd == 'ALL') {

								if ($jenis == 'AL') {

									// $data['wow'] = $this->db->query('SELECT * from tbl_form_camaba where status '.$sts.' and gelombang = "'.$gelombang.'"  group by prodi,jenis_pmb')->result();

									if ($kategori == 'edu0') {
										$data['wow'] = $this->db->query('SELECT prodi,gelombang,jenis_pmb,status,didik_ayah,count(didik_ayah) as hitung from tbl_form_camaba where status '.$sts.' and gelombang = "'.$gelombang.'" and gelombang = "'.$gelombang.'" and didik_ayah is not null group by prodi,jenis_pmb,didik_ayah')->result();
									} elseif ($kategori == 'edu1') {	
										$data['wow'] = $this->db->query('SELECT prodi,gelombang,jenis_pmb,status,didik_ibu,count(didik_ibu) as hitung from tbl_form_camaba where status '.$sts.' and gelombang = "'.$gelombang.'" and gelombang = "'.$gelombang.'" and didik_ibu is not null group by prodi,jenis_pmb,didik_ibu')->result();
									} elseif ($kategori == 'wrk0') {
										$data['wow'] = $this->db->query('SELECT prodi,gelombang,jenis_pmb,status,workdad,count(workdad) as hitung from tbl_form_camaba where status '.$sts.' and gelombang = "'.$gelombang.'" and gelombang = "'.$gelombang.'" and workdad is not null group by prodi,jenis_pmb,workdad')->result();
									} elseif ($kategori == 'wrk1') {	
										$data['wow'] = $this->db->query('SELECT prodi,gelombang,jenis_pmb,status,workmom,count(workmom) as hitung from tbl_form_camaba where status '.$sts.' and gelombang = "'.$gelombang.'" and gelombang = "'.$gelombang.'" and workmom is not null group by prodi,jenis_pmb,workmom')->result();
									} elseif ($kategori == 'sch') {
										$data['wow'] = $this->db->query('SELECT prodi,gelombang,jenis_pmb,status,jenis_sch_maba,count(jenis_sch_maba) as hitung from tbl_form_camaba where status '.$sts.' and gelombang = "'.$gelombang.'" and gelombang = "'.$gelombang.'" and jenis_sch_maba is not null group by prodi,jenis_pmb,jenis_sch_maba')->result();
									} elseif ($kategori == 'cty') {
										$data['wow'] = $this->db->query('SELECT prodi,gelombang,jenis_pmb,status,kota_sch_maba,count(kota_sch_maba) as hitung from tbl_form_camaba where status '.$sts.' and gelombang = "'.$gelombang.'" and gelombang = "'.$gelombang.'" and kota_sch_maba is not null group by prodi,jenis_pmb,kota_sch_maba')->result();
									} elseif ($kategori == 'grd') {
										$data['wow'] = $this->db->query('SELECT prodi,gelombang,jenis_pmb,status,lulus_maba,count(lulus_maba) as hitung from tbl_form_camaba where status '.$sts.' and gelombang = "'.$gelombang.'" and gelombang = "'.$gelombang.'" and lulus_maba is not null group by prodi,jenis_pmb,lulus_maba')->result();
									}

									// $data['hits1'] = $this->db->query('SELECT workdad,count(workdad) as hitung from tbl_form_camaba where status '.$sts.' and gelombang = "'.$gelombang.'" and workdad is not null group by workdad')->result();

								} else {

									// $data['wow'] = $this->db->query('SELECT * from tbl_form_camaba where jenis_pmb = "'.$jenis.'" and status '.$sts.' and gelombang = "'.$gelombang.'"  group by prodi')->result();

									if ($kategori == 'edu0') {
										$data['wow'] = $this->db->query('SELECT prodi,gelombang,jenis_pmb,status,didik_ayah,count(didik_ayah) as hitung from tbl_form_camaba where jenis_pmb = "'.$jenis.'" and status '.$sts.' and gelombang = "'.$gelombang.'" and didik_ayah is not null group by prodi,didik_ayah')->result();
									} elseif ($kategori == 'edu1') {	
										$data['wow'] = $this->db->query('SELECT prodi,gelombang,jenis_pmb,status,didik_ibu,count(didik_ibu) as hitung from tbl_form_camaba where jenis_pmb = "'.$jenis.'" and status '.$sts.' and gelombang = "'.$gelombang.'" and didik_ibu is not null group by prodi,didik_ibu')->result();
									} elseif ($kategori == 'wrk0') {
										$data['wow'] = $this->db->query('SELECT prodi,gelombang,jenis_pmb,status,workdad,count(workdad) as hitung from tbl_form_camaba where jenis_pmb = "'.$jenis.'" and status '.$sts.' and gelombang = "'.$gelombang.'" and workdad is not null group by prodi,workdad')->result();
									} elseif ($kategori == 'wrk1') {	
										$data['wow'] = $this->db->query('SELECT prodi,gelombang,jenis_pmb,status,workmom,count(workmom) as hitung from tbl_form_camaba where jenis_pmb = "'.$jenis.'" and status '.$sts.' and gelombang = "'.$gelombang.'" and workmom is not null group by prodi,workmom')->result();
									} elseif ($kategori == 'sch') {
										$data['wow'] = $this->db->query('SELECT prodi,gelombang,jenis_pmb,status,jenis_sch_maba,count(jenis_sch_maba) as hitung from tbl_form_camaba where jenis_pmb = "'.$jenis.'" and status '.$sts.' and gelombang = "'.$gelombang.'" and jenis_sch_maba is not null group by prodi,jenis_sch_maba')->result();
									} elseif ($kategori == 'cty') {
										$data['wow'] = $this->db->query('SELECT prodi,gelombang,jenis_pmb,status,kota_sch_maba,count(kota_sch_maba) as hitung from tbl_form_camaba where jenis_pmb = "'.$jenis.'" and status '.$sts.' and gelombang = "'.$gelombang.'" and kota_sch_maba is not null group by prodi,kota_sch_maba')->result();
									} elseif ($kategori == 'grd') {
										$data['wow'] = $this->db->query('SELECT prodi,gelombang,jenis_pmb,status,lulus_maba,count(lulus_maba) as hitung from tbl_form_camaba where jenis_pmb = "'.$jenis.'" and status '.$sts.' and gelombang = "'.$gelombang.'" and lulus_maba is not null group by prodi,lulus_maba')->result();
									}

									// $data['hits1'] = $this->db->query('SELECT workdad,count(workdad) as hitung from tbl_form_camaba where jenis_pmb = "'.$jenis.'" and status '.$sts.' and gelombang = "'.$gelombang.'" and workdad is not null group by workdad')->result();

								}	

							} else {

								if ($jenis == 'AL') {

									// $data['wow'] = $this->db->query('SELECT * from tbl_form_camaba where prodi = "'.$prodd.'" and status '.$sts.' and gelombang = "'.$gelombang.'"  group by jenis_pmb')->result();

									if ($kategori == 'edu0') {
										$data['wow'] = $this->db->query('SELECT prodi,gelombang,jenis_pmb,status,didik_ayah,count(didik_ayah) as hitung from tbl_form_camaba where prodi = "'.$prodd.'" and status '.$sts.' and gelombang = "'.$gelombang.'" and didik_ayah is not null group by jenis_pmb,didik_ayah')->result();
									} elseif ($kategori == 'edu1') {	
										$data['wow'] = $this->db->query('SELECT prodi,gelombang,jenis_pmb,status,didik_ibu,count(didik_ibu) as hitung from tbl_form_camaba where prodi = "'.$prodd.'" and status '.$sts.' and gelombang = "'.$gelombang.'" and didik_ibu is not null group by jenis_pmb,didik_ibu')->result();
									} elseif ($kategori == 'wrk0') {
										$data['wow'] = $this->db->query('SELECT prodi,gelombang,jenis_pmb,status,workdad,count(workdad) as hitung from tbl_form_camaba where prodi = "'.$prodd.'" and status '.$sts.' and gelombang = "'.$gelombang.'" and workdad is not null group by jenis_pmb,workdad')->result();
									} elseif ($kategori == 'wrk1') {	
										$data['wow'] = $this->db->query('SELECT prodi,gelombang,jenis_pmb,status,workmom,count(workmom) as hitung from tbl_form_camaba where prodi = "'.$prodd.'" and status '.$sts.' and gelombang = "'.$gelombang.'" and workmom is not null group by jenis_pmb,workmom')->result();
									} elseif ($kategori == 'sch') {
										$data['wow'] = $this->db->query('SELECT prodi,gelombang,jenis_pmb,status,jenis_sch_maba,count(jenis_sch_maba) as hitung from tbl_form_camaba where prodi = "'.$prodd.'" and status '.$sts.' and gelombang = "'.$gelombang.'" and jenis_sch_maba is not null group by jenis_pmb,jenis_sch_maba')->result();
									} elseif ($kategori == 'cty') {
										$data['wow'] = $this->db->query('SELECT prodi,gelombang,jenis_pmb,status,kota_sch_maba,count(kota_sch_maba) as hitung from tbl_form_camaba where prodi = "'.$prodd.'" and status '.$sts.' and gelombang = "'.$gelombang.'" and kota_sch_maba is not null group by jenis_pmb,kota_sch_maba')->result();
									} elseif ($kategori == 'grd') {
										$data['wow'] = $this->db->query('SELECT prodi,gelombang,jenis_pmb,status,lulus_maba,count(lulus_maba) as hitung from tbl_form_camaba where prodi = "'.$prodd.'" and status '.$sts.' and gelombang = "'.$gelombang.'" and lulus_maba is not null group by jenis_pmb,lulus_maba')->result();
									}

									// $data['hits1'] = $this->db->query('SELECT workdad,count(workdad) as hitung from tbl_form_camaba where prodi = "'.$prodd.'" and status '.$sts.' and gelombang = "'.$gelombang.'" and workdad is not null group by workdad')->result();

								} else {

									// $data['wow'] = $this->db->query('SELECT * from tbl_form_camaba where prodi = "'.$prodd.'" and jenis_pmb = "'.$jenis.'" and status '.$sts.' and gelombang = "'.$gelombang.'"')->result();

									if ($kategori == 'edu0') {
										$data['wow'] = $this->db->query('SELECT prodi,gelombang,jenis_pmb,status,didik_ayah,count(didik_ayah) as hitung from tbl_form_camaba where prodi = "'.$prodd.'" and jenis_pmb = "'.$jenis.'" and status '.$sts.' and gelombang = "'.$gelombang.'" and didik_ayah is not null group by didik_ayah')->result();
									} elseif ($kategori == 'edu1') {	
										$data['wow'] = $this->db->query('SELECT prodi,gelombang,jenis_pmb,status,didik_ibu,count(didik_ibu) as hitung from tbl_form_camaba where prodi = "'.$prodd.'" and jenis_pmb = "'.$jenis.'" and status '.$sts.' and gelombang = "'.$gelombang.'" and didik_ibu is not null group by didik_ibu')->result();
									} elseif ($kategori == 'wrk0') {
										$data['wow'] = $this->db->query('SELECT prodi,gelombang,jenis_pmb,status,workdad,count(workdad) as hitung from tbl_form_camaba where prodi = "'.$prodd.'" and jenis_pmb = "'.$jenis.'" and status '.$sts.' and gelombang = "'.$gelombang.'" and workdad is not null group by workdad')->result();
									} elseif ($kategori == 'wrk1') {	
										$data['wow'] = $this->db->query('SELECT prodi,gelombang,jenis_pmb,status,workmom,count(workmom) as hitung from tbl_form_camaba where prodi = "'.$prodd.'" and jenis_pmb = "'.$jenis.'" and status '.$sts.' and gelombang = "'.$gelombang.'" and workmom is not null group by workmom')->result();
									} elseif ($kategori == 'sch') {
										$data['wow'] = $this->db->query('SELECT prodi,gelombang,jenis_pmb,status,jenis_sch_maba,count(jenis_sch_maba) as hitung from tbl_form_camaba where prodi = "'.$prodd.'" and jenis_pmb = "'.$jenis.'" and status '.$sts.' and gelombang = "'.$gelombang.'" and jenis_sch_maba is not null group by jenis_sch_maba')->result();
									} elseif ($kategori == 'cty') {
										$data['wow'] = $this->db->query('SELECT prodi,gelombang,jenis_pmb,status,kota_sch_maba,count(kota_sch_maba) as hitung from tbl_form_camaba where prodi = "'.$prodd.'" and jenis_pmb = "'.$jenis.'" and status '.$sts.' and gelombang = "'.$gelombang.'" and kota_sch_maba is not null group by asal_sch_maba')->result();
									} elseif ($kategori == 'grd') {
										$data['wow'] = $this->db->query('SELECT prodi,gelombang,jenis_pmb,status,lulus_maba,count(lulus_maba) as hitung from tbl_form_camaba where prodi = "'.$prodd.'" and jenis_pmb = "'.$jenis.'" and status '.$sts.' and gelombang = "'.$gelombang.'" and lulus_maba is not null group by lulus_maba')->result();
									}

									// $data['hits1'] = $this->db->query('SELECT workdad,count(workdad) as kerjadad from tbl_form_camaba_2016 where prodi = "'.$prodd.'" and jenis_pmb = "'.$jenis.'" and status '.$sts.' and gelombang = "'.$gelombang.'" and workdad is not null group by workdad')->result();
									
								}

							}
							
						}
							
					}

				$this->load->view('excel_report_jml_edu', $data);

			} elseif ($program == 'S2') {

				if ($gelombang == 'ALL') {

					if ($sts == 'ALL') {

						if ($prodd == 'ALL') {

							if ($jenis == 'AL') {

								if ($kategori == 'wrk') {
									$data['wew'] = $this->db->query('SELECT opsi_prodi_s2,gelombang,status,jenis_pmb,pekerjaan,count(pekerjaan) as angka from tbl_pmb_s2 group by jenis_pmb,opsi_prodi_s2,status,gelombang,pekerjaan')->result();
								} elseif ($kategori == 'cty') {
									$data['wew'] = $this->db->query('SELECT opsi_prodi_s2,gelombang,status,jenis_pmb,kota_asl_univ,count(kota_asl_univ) as angka from tbl_pmb_s2 group by jenis_pmb,opsi_prodi_s2,status,gelombang,kota_asl_univ')->result();
								} elseif ($kategori == 'grd') {
									$data['wew'] = $this->db->query('SELECT opsi_prodi_s2,gelombang,status,jenis_pmb,th_lulus,count(th_lulus) as angka from tbl_pmb_s2 group by jenis_pmb,opsi_prodi_s2,status,gelombang,th_lulus')->result();
								}

							} else {

								// $data['wew'] = $this->db->query('SELECT * from tbl_pmb_s2 where jenis_pmb = "'.$jenis.'"')->result();

								if ($kategori == 'wrk') {
									$data['wew'] = $this->db->query('SELECT opsi_prodi_s2,gelombang,status,jenis_pmb,pekerjaan,count(pekerjaan) as angka from tbl_pmb_s2 where jenis_pmb = "'.$jenis.'" group by opsi_prodi_s2,status,gelombang,pekerjaan')->result();
								} elseif ($kategori == 'cty') {
									$data['wew'] = $this->db->query('SELECT opsi_prodi_s2,gelombang,status,jenis_pmb,kota_asl_univ,count(kota_asl_univ) as angka from tbl_pmb_s2 where jenis_pmb = "'.$jenis.'" group by opsi_prodi_s2,status,gelombang,kota_asl_univ')->result();
								} elseif ($kategori == 'grd') {
									$data['wew'] = $this->db->query('SELECT opsi_prodi_s2,gelombang,status,jenis_pmb,th_lulus,count(th_lulus) as angka from tbl_pmb_s2 where jenis_pmb = "'.$jenis.'" group by opsi_prodi_s2,status,gelombang,th_lulus')->result();
								}

							}	

						} else {

							if ($jenis == 'AL') {

								// $data['wew'] = $this->db->query('SELECT * from tbl_pmb_s2 where opsi_prodi_s2 = "'.$prodd.'"')->result();

								if ($kategori == 'wrk') {
									$data['wew'] = $this->db->query('SELECT opsi_prodi_s2,gelombang,status,jenis_pmb,pekerjaan,count(pekerjaan) as angka from tbl_pmb_s2 where opsi_prodi_s2 = "'.$prodd.'" group by jenis_pmb,status,gelombang,pekerjaan')->result();
								} elseif ($kategori == 'cty') {
									$data['wew'] = $this->db->query('SELECT opsi_prodi_s2,gelombang,status,jenis_pmb,kota_asl_univ,count(kota_asl_univ) as angka from tbl_pmb_s2 where opsi_prodi_s2 = "'.$prodd.'" group by jenis_pmb,status,gelombang,kota_asl_univ')->result();
								} elseif ($kategori == 'grd') {
									$data['wew'] = $this->db->query('SELECT opsi_prodi_s2,gelombang,status,jenis_pmb,th_lulus,count(th_lulus) as angka from tbl_pmb_s2 where opsi_prodi_s2 = "'.$prodd.'" group by jenis_pmb,status,gelombang,th_lulus')->result();
								}

							} else {

								// $data['wew'] = $this->db->query('SELECT * from tbl_pmb_s2 where opsi_prodi_s2 = "'.$prodd.'" and jenis_pmb = "'.$jenis.'"')->result();

								if ($kategori == 'wrk') {
									$data['wew'] = $this->db->query('SELECT opsi_prodi_s2,gelombang,status,jenis_pmb,pekerjaan,count(pekerjaan) as angka from tbl_pmb_s2 where opsi_prodi_s2 = "'.$prodd.'" and jenis_pmb = "'.$jenis.'" group by status,gelombang,pekerjaan')->result();
								} elseif ($kategori == 'cty') {
									$data['wew'] = $this->db->query('SELECT opsi_prodi_s2,gelombang,status,jenis_pmb,kota_asl_univ,count(kota_asl_univ) as angka from tbl_pmb_s2 where opsi_prodi_s2 = "'.$prodd.'" and jenis_pmb = "'.$jenis.'" group by status,gelombang,kota_asl_univ')->result();
								} elseif ($kategori == 'grd') {
									$data['wew'] = $this->db->query('SELECT opsi_prodi_s2,gelombang,status,jenis_pmb,th_lulus,count(th_lulus) as angka from tbl_pmb_s2 where opsi_prodi_s2 = "'.$prodd.'" and jenis_pmb = "'.$jenis.'" group by status,gelombang,th_lulus')->result();
								}

							}

						}

					} else {

						if ($prodd == 'ALL') {

							if ($jenis == 'AL') {

								// $data['wew'] = $this->db->query('SELECT * from tbl_pmb_s2 where status '.$sts.'')->result();

								if ($kategori == 'wrk') {
									$data['wew'] = $this->db->query('SELECT opsi_prodi_s2,gelombang,status,jenis_pmb,pekerjaan,count(pekerjaan) as angka from tbl_pmb_s2 where status '.$sts.' group by jenis_pmb,opsi_prodi_s2,gelombang,pekerjaan')->result();
								} elseif ($kategori == 'cty') {
									$data['wew'] = $this->db->query('SELECT opsi_prodi_s2,gelombang,status,jenis_pmb,kota_asl_univ,count(kota_asl_univ) as angka from tbl_pmb_s2 where status '.$sts.' group by jenis_pmb,opsi_prodi_s2,gelombang,kota_asl_univ')->result();
								} elseif ($kategori == 'grd') {
									$data['wew'] = $this->db->query('SELECT opsi_prodi_s2,gelombang,status,jenis_pmb,th_lulus,count(th_lulus) as angka from tbl_pmb_s2 where status '.$sts.' group by jenis_pmb,opsi_prodi_s2,gelombang,th_lulus')->result();
								}

							} else {

								// $data['wew'] = $this->db->query('SELECT * from tbl_pmb_s2 where jenis_pmb = "'.$jenis.'" and status '.$sts.'')->result();

								if ($kategori == 'wrk') {
									$data['wew'] = $this->db->query('SELECT opsi_prodi_s2,gelombang,status,jenis_pmb,pekerjaan,count(pekerjaan) as angka from tbl_pmb_s2 where jenis_pmb = "'.$jenis.'" and status '.$sts.' group by opsi_prodi_s2,gelombang,pekerjaan')->result();
								} elseif ($kategori == 'cty') {
									$data['wew'] = $this->db->query('SELECT opsi_prodi_s2,gelombang,status,jenis_pmb,kota_asl_univ,count(kota_asl_univ) as angka from tbl_pmb_s2 where jenis_pmb = "'.$jenis.'" and status '.$sts.' group by opsi_prodi_s2,gelombang,kota_asl_univ')->result();
								} elseif ($kategori == 'grd') {
									$data['wew'] = $this->db->query('SELECT opsi_prodi_s2,gelombang,status,jenis_pmb,th_lulus,count(th_lulus) as angka from tbl_pmb_s2 where jenis_pmb = "'.$jenis.'" and status '.$sts.' group by opsi_prodi_s2,gelombang,th_lulus')->result();
								}

							}	

						} else {

							if ($jenis == 'AL') {

								// $data['wew'] = $this->db->query('SELECT * from tbl_pmb_s2 where opsi_prodi_s2 = "'.$prodd.'" and status '.$sts.'')->result();

								if ($kategori == 'wrk') {
									$data['wew'] = $this->db->query('SELECT opsi_prodi_s2,gelombang,status,jenis_pmb,pekerjaan,count(pekerjaan) as angka from tbl_pmb_s2 where opsi_prodi_s2 = "'.$prodd.'" and status '.$sts.' group by jenis_pmb,gelombang,pekerjaan')->result();
								} elseif ($kategori == 'cty') {
									$data['wew'] = $this->db->query('SELECT opsi_prodi_s2,gelombang,status,jenis_pmb,kota_asl_univ,count(kota_asl_univ) as angka from tbl_pmb_s2 where opsi_prodi_s2 = "'.$prodd.'" and status '.$sts.' group by jenis_pmb,gelombang,kota_asl_univ')->result();
								} elseif ($kategori == 'grd') {
									$data['wew'] = $this->db->query('SELECT opsi_prodi_s2,gelombang,status,jenis_pmb,th_lulus,count(th_lulus) as angka from tbl_pmb_s2 where opsi_prodi_s2 = "'.$prodd.'" and status '.$sts.' group by jenis_pmb,gelombang,th_lulus')->result();
								}

							} else {

								// $data['wew'] = $this->db->query('SELECT * from tbl_pmb_s2 where opsi_prodi_s2 = "'.$prodd.'" and jenis_pmb = "'.$jenis.'" and status '.$sts.'')->result();

								if ($kategori == 'wrk') {
									$data['wew'] = $this->db->query('SELECT opsi_prodi_s2,gelombang,status,jenis_pmb,pekerjaan,count(pekerjaan) as angka from tbl_pmb_s2 where opsi_prodi_s2 = "'.$prodd.'" and jenis_pmb = "'.$jenis.'" and status '.$sts.' group by gelombang,pekerjaan')->result();
								} elseif ($kategori == 'cty') {
									$data['wew'] = $this->db->query('SELECT opsi_prodi_s2,gelombang,status,jenis_pmb,kota_asl_univ,count(kota_asl_univ) as angka from tbl_pmb_s2 where opsi_prodi_s2 = "'.$prodd.'" and jenis_pmb = "'.$jenis.'" and status '.$sts.' group by gelombang,kota_asl_univ')->result();
								} elseif ($kategori == 'grd') {
									$data['wew'] = $this->db->query('SELECT opsi_prodi_s2,gelombang,status,jenis_pmb,th_lulus,count(th_lulus) as angka from tbl_pmb_s2 where opsi_prodi_s2 = "'.$prodd.'" and jenis_pmb = "'.$jenis.'" and status '.$sts.' group by gelombang,th_lulus')->result();
								}
								
							}

						}
						
					}
						
				} else {

					if ($sts == 'ALL') {

						if ($prodd == 'ALL') {

							if ($jenis == 'AL') {

								// $data['wew'] = $this->db->query('SELECT * from tbl_pmb_s2 where gelombang = "'.$gelombang.'"')->result();

								if ($kategori == 'wrk') {
									$data['wew'] = $this->db->query('SELECT opsi_prodi_s2,gelombang,status,jenis_pmb,pekerjaan,count(pekerjaan) as angka from tbl_pmb_s2 where gelombang = "'.$gelombang.'" group by opsi_prodi_s2,jenis_pmb,status,pekerjaan')->result();
								} elseif ($kategori == 'cty') {
									$data['wew'] = $this->db->query('SELECT opsi_prodi_s2,gelombang,status,jenis_pmb,kota_asl_univ,count(kota_asl_univ) as angka from tbl_pmb_s2 where gelombang = "'.$gelombang.'" group by opsi_prodi_s2,jenis_pmb,status,kota_asl_univ')->result();
								} elseif ($kategori == 'grd') {
									$data['wew'] = $this->db->query('SELECT opsi_prodi_s2,gelombang,status,jenis_pmb,th_lulus,count(th_lulus) as angka from tbl_pmb_s2 where gelombang = "'.$gelombang.'" group by opsi_prodi_s2,jenis_pmb,status,th_lulus')->result();
								}

							} else {

								// $data['wew'] = $this->db->query('SELECT * from tbl_pmb_s2 where jenis_pmb = "'.$jenis.'" and gelombang = "'.$gelombang.'"')->result();

								if ($kategori == 'wrk') {
									$data['wew'] = $this->db->query('SELECT opsi_prodi_s2,gelombang,status,jenis_pmb,pekerjaan,count(pekerjaan) as angka from tbl_pmb_s2 where jenis_pmb = "'.$jenis.'" and gelombang = "'.$gelombang.'" group by opsi_prodi_s2,status,pekerjaan')->result();
								} elseif ($kategori == 'cty') {
									$data['wew'] = $this->db->query('SELECT opsi_prodi_s2,gelombang,status,jenis_pmb,kota_asl_univ,count(kota_asl_univ) as angka from tbl_pmb_s2 where jenis_pmb = "'.$jenis.'" and gelombang = "'.$gelombang.'" group by opsi_prodi_s2,status,kota_asl_univ')->result();
								} elseif ($kategori == 'grd') {
									$data['wew'] = $this->db->query('SELECT opsi_prodi_s2,gelombang,status,jenis_pmb,th_lulus,count(th_lulus) as angka from tbl_pmb_s2 where jenis_pmb = "'.$jenis.'" and gelombang = "'.$gelombang.'" group by opsi_prodi_s2,status,th_lulus')->result();
								}

							}	

						} else {

							if ($jenis == 'AL') {

								// $data['wew'] = $this->db->query('SELECT * from tbl_pmb_s2 where opsi_prodi_s2 = "'.$prodd.'" and gelombang = "'.$gelombang.'"')->result();

								if ($kategori == 'wrk') {
									$data['wew'] = $this->db->query('SELECT opsi_prodi_s2,gelombang,status,jenis_pmb,pekerjaan,count(pekerjaan) as angka from tbl_pmb_s2 where opsi_prodi_s2 = "'.$prodd.'" and gelombang = "'.$gelombang.'" group by jenis_pmb,status,pekerjaan')->result();
								} elseif ($kategori == 'cty') {
									$data['wew'] = $this->db->query('SELECT opsi_prodi_s2,gelombang,status,jenis_pmb,kota_asl_univ,count(kota_asl_univ) as angka from tbl_pmb_s2 where opsi_prodi_s2 = "'.$prodd.'" and gelombang = "'.$gelombang.'" group by jenis_pmb,status,kota_asl_univ')->result();
								} elseif ($kategori == 'grd') {
									$data['wew'] = $this->db->query('SELECT opsi_prodi_s2,gelombang,status,jenis_pmb,th_lulus,count(th_lulus) as angka from tbl_pmb_s2 where opsi_prodi_s2 = "'.$prodd.'" and gelombang = "'.$gelombang.'" group by jenis_pmb,status,th_lulus')->result();
								}

							} else {

								// $data['wew'] = $this->db->query('SELECT * from tbl_pmb_s2 where opsi_prodi_s2 = "'.$prodd.'" and jenis_pmb = "'.$jenis.'" and gelombang = "'.$gelombang.'"')->result();

								if ($kategori == 'wrk') {
									$data['wew'] = $this->db->query('SELECT opsi_prodi_s2,gelombang,status,jenis_pmb,pekerjaan,count(pekerjaan) as angka from tbl_pmb_s2 where opsi_prodi_s2 = "'.$prodd.'" and jenis_pmb = "'.$jenis.'" and gelombang = "'.$gelombang.'" group by status,pekerjaan')->result();
								} elseif ($kategori == 'cty') {
									$data['wew'] = $this->db->query('SELECT opsi_prodi_s2,gelombang,status,jenis_pmb,kota_asl_univ,count(kota_asl_univ) as angka from tbl_pmb_s2 where opsi_prodi_s2 = "'.$prodd.'" and jenis_pmb = "'.$jenis.'" and gelombang = "'.$gelombang.'" group by status,kota_asl_univ')->result();
								} elseif ($kategori == 'grd') {
									$data['wew'] = $this->db->query('SELECT opsi_prodi_s2,gelombang,status,jenis_pmb,th_lulus,count(th_lulus) as angka from tbl_pmb_s2 where opsi_prodi_s2 = "'.$prodd.'" and jenis_pmb = "'.$jenis.'" and gelombang = "'.$gelombang.'" group by status,th_lulus')->result();
								}

							}

						}

					} else {

						if ($prodd == 'ALL') {

							if ($jenis == 'AL') {

								// $data['wew'] = $this->db->query('SELECT * from tbl_pmb_s2 where status  '.$sts.' and gelombang = "'.$gelombang.'"')->result();

								if ($kategori == 'wrk') {
									$data['wew'] = $this->db->query('SELECT opsi_prodi_s2,gelombang,status,jenis_pmb,pekerjaan,count(pekerjaan) as angka from tbl_pmb_s2 where status  '.$sts.' and gelombang = "'.$gelombang.'" group by opsi_prodi_s2,jenis_pmb,pekerjaan')->result();
								} elseif ($kategori == 'cty') {
									$data['wew'] = $this->db->query('SELECT opsi_prodi_s2,gelombang,status,jenis_pmb,kota_asl_univ,count(kota_asl_univ) as angka from tbl_pmb_s2 where status  '.$sts.' and gelombang = "'.$gelombang.'" group by opsi_prodi_s2,jenis_pmb,kota_asl_univ')->result();
								} elseif ($kategori == 'grd') {
									$data['wew'] = $this->db->query('SELECT opsi_prodi_s2,gelombang,status,jenis_pmb,th_lulus,count(th_lulus) as angka from tbl_pmb_s2 where status  '.$sts.' and gelombang = "'.$gelombang.'" group by opsi_prodi_s2,jenis_pmb,th_lulus')->result();
								}

							} else {

								// $data['wew'] = $this->db->query('SELECT * from tbl_pmb_s2 where jenis_pmb = "'.$jenis.'" and status '.$sts.' and gelombang = "'.$gelombang.'"')->result();

								if ($kategori == 'wrk') {
									$data['wew'] = $this->db->query('SELECT opsi_prodi_s2,gelombang,status,jenis_pmb,pekerjaan,count(pekerjaan) as angka from tbl_pmb_s2 where jenis_pmb = "'.$jenis.'" and status '.$sts.' and gelombang = "'.$gelombang.'" group by opsi_prodi_s2,pekerjaan')->result();
								} elseif ($kategori == 'cty') {
									$data['wew'] = $this->db->query('SELECT opsi_prodi_s2,gelombang,status,jenis_pmb,kota_asl_univ,count(kota_asl_univ) as angka from tbl_pmb_s2 where jenis_pmb = "'.$jenis.'" and status '.$sts.' and gelombang = "'.$gelombang.'" group by opsi_prodi_s2,kota_asl_univ')->result();
								} elseif ($kategori == 'grd') {
									$data['wew'] = $this->db->query('SELECT opsi_prodi_s2,gelombang,status,jenis_pmb,th_lulus,count(th_lulus) as angka from tbl_pmb_s2 where jenis_pmb = "'.$jenis.'" and status '.$sts.' and gelombang = "'.$gelombang.'" group by opsi_prodi_s2,th_lulus')->result();
								}

							}	

						} else {

							if ($jenis == 'AL') {

								// $data['wew'] = $this->db->query('SELECT * from tbl_pmb_s2 where opsi_prodi_s2 = "'.$prodd.'" and status '.$sts.' and gelombang = "'.$gelombang.'"')->result();

								if ($kategori == 'wrk') {
									$data['wew'] = $this->db->query('SELECT opsi_prodi_s2,gelombang,status,jenis_pmb,pekerjaan,count(pekerjaan) as angka from tbl_pmb_s2 where opsi_prodi_s2 = "'.$prodd.'" and status '.$sts.' and gelombang = "'.$gelombang.'" group by jenis_pmb,pekerjaan')->result();
								} elseif ($kategori == 'cty') {
									$data['wew'] = $this->db->query('SELECT opsi_prodi_s2,gelombang,status,jenis_pmb,kota_asl_univ,count(kota_asl_univ) as angka from tbl_pmb_s2 where opsi_prodi_s2 = "'.$prodd.'" and status '.$sts.' and gelombang = "'.$gelombang.'" group by jenis_pmb,kota_asl_univ')->result();
								} elseif ($kategori == 'grd') {
									$data['wew'] = $this->db->query('SELECT opsi_prodi_s2,gelombang,status,jenis_pmb,th_lulus,count(th_lulus) as angka from tbl_pmb_s2 where opsi_prodi_s2 = "'.$prodd.'" and status '.$sts.' and gelombang = "'.$gelombang.'" group by jenis_pmb,th_lulus')->result();
								}

							} else {

								// $data['wew'] = $this->db->query('SELECT * from tbl_pmb_s2 where opsi_prodi_s2 = "'.$prodd.'" and jenis_pmb = "'.$jenis.'" and status '.$sts.' and gelombang = "'.$gelombang.'"')->result();

								if ($kategori == 'wrk') {
									$data['wew'] = $this->db->query('SELECT opsi_prodi_s2,gelombang,status,jenis_pmb,pekerjaan,count(pekerjaan) as angka from tbl_pmb_s2 where opsi_prodi_s2 = "'.$prodd.'" and jenis_pmb = "'.$jenis.'" and status '.$sts.' and gelombang = "'.$gelombang.'" group by pekerjaan')->result();
								} elseif ($kategori == 'cty') {
									$data['wew'] = $this->db->query('SELECT opsi_prodi_s2,gelombang,status,jenis_pmb,kota_asl_univ,count(kota_asl_univ) as angka from tbl_pmb_s2 where opsi_prodi_s2 = "'.$prodd.'" and jenis_pmb = "'.$jenis.'" and status '.$sts.' and gelombang = "'.$gelombang.'" group by kota_asl_univ')->result();
								} elseif ($kategori == 'grd') {
									$data['wew'] = $this->db->query('SELECT opsi_prodi_s2,gelombang,status,jenis_pmb,th_lulus,count(th_lulus) as angka from tbl_pmb_s2 where opsi_prodi_s2 = "'.$prodd.'" and jenis_pmb = "'.$jenis.'" and status '.$sts.' and gelombang = "'.$gelombang.'" group by th_lulus')->result();
								}
								
							}

						}
						
					}
						
				}		

				$this->load->view('excel_report_jml_s2', $data);

			}
		}
	}

}

/* End of file Rekap_jml.php */
/* Location: ./application/modules/pmb/controllers/Rekap_jml.php */