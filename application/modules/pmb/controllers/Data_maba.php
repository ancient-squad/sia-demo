<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Data_maba extends MY_Controller
{

	function __construct()
	{
		parent::__construct();
		$this->load->library('Cfpdf');
		$this->db2 = $this->load->database('regis', TRUE);
		//error_reporting(0);
		if ($this->session->userdata('sess_login') == TRUE) {
			$cekakses = $this->role_model->cekakses(118)->result();
			if ($cekakses != TRUE) {
				echo "<script>alert('Anda Tidak Berhak Mengakses !!');history.go(-1);</script>";
			}
		} else {
			redirect('auth', 'refresh');
		}
	}

	public function index()
	{
		$data['prods'] = $this->db->query('SELECT * from tbl_jurusan_prodi')->result();
		$data['page'] = "v_data_maba";
		$this->load->view('template/template', $data);
	}

	function load()
	{
		$kampus 	= $this->input->post('kampus');
		$program 	= $this->input->post('program');
		$gelombang 	= $this->input->post('gelombang');
		$sts 		= $this->input->post('status');
		$prodd		= $this->input->post('prodi');
		$thn = substr($this->input->post('tahun'), 2, 4);

		$data['tahun'] = $this->input->post('tahun');
		$data['prog'] = $this->input->post('program');

		if ($this->input->post('tahun') == '2016') {
			if ($program == 'S1') {
				if ($kampus == 'ALL') {
					if ($gelombang == 'ALL') {
						if ($sts == 'ALL') {
							if ($prodd == 'ALL') {
								$data['wow'] = $this->db->query('SELECT * from tbl_form_camaba_2016')->result();
							} else {
								$data['wow'] = $this->db->query('SELECT * from tbl_form_camaba_2016 where prodi = "' . $prodd . '"')->result();
							}
						} else {
							if ($prodd == 'ALL') {
								$data['wow'] = $this->db->query('SELECT * from tbl_form_camaba_2016 where status ' . $sts . '')->result();
							} else {
								$data['wow'] = $this->db->query('SELECT * from tbl_form_camaba_2016 where status ' . $sts . ' and prodi = "' . $prodd . '"')->result();
							}
						}
					} else {
						if ($prodd == 'ALL') {
							$data['wow'] = $this->db->query('SELECT * from tbl_form_camaba_2016 where status ' . $sts . ' and gelombang = "' . $gelombang . '"')->result();
						} else {
							$data['wow'] = $this->db->query('SELECT * from tbl_form_camaba_2016 where status ' . $sts . ' and prodi = "' . $prodd . '" and gelombang = "' . $gelombang . '"')->result();
						}
					}
				} else {
					if ($gelombang == 'ALL') {
						if ($sts == 'ALL') {
							if ($prodd == 'ALL') {
								$data['wow'] = $this->db->query('SELECT * from tbl_form_camaba_2016 where kampus = "' . $kampus . '"')->result();
							} else {
								$data['wow'] = $this->db->query('SELECT * from tbl_form_camaba_2016 where prodi = "' . $prodd . '" and kampus = "' . $kampus . '"')->result();
							}
						} else {
							if ($prodd == 'ALL') {
								$data['wow'] = $this->db->query('SELECT * from tbl_form_camaba_2016 where status ' . $sts . ' and kampus = "' . $kampus . '"')->result();
							} else {
								$data['wow'] = $this->db->query('SELECT * from tbl_form_camaba_2016 where status ' . $sts . ' and prodi = "' . $prodd . '" and kampus = "' . $kampus . '"')->result();
							}
						}
					} else {
						if ($prodd == 'ALL') {
							$data['wow'] = $this->db->query('SELECT * from tbl_form_camaba_2016 where status ' . $sts . ' and gelombang = "' . $gelombang . '" and kampus = "' . $kampus . '"')->result();
						} else {
							$data['wow'] = $this->db->query('SELECT * from tbl_form_camaba_2016 where status ' . $sts . ' and prodi = "' . $prodd . '" and gelombang = "' . $gelombang . '" and kampus = "' . $kampus . '"')->result();
						}
					}
				}
				$this->load->view('v_status_maba', $data);
			} elseif ($program == 'S2') {
				if ($kampus == 'ALL') {
					if ($gelombang == 'ALL') {
						if ($sts == 'ALL') {
							if ($prodd == 'ALL') {
								$data['wew'] = $this->db->query('SELECT * from tbl_pmb_s2_2016')->result();
							} else {
								$data['wew'] = $this->db->query('SELECT * from tbl_pmb_s2_2016 where opsi_prodi_s2 = "' . $prodd . '"')->result();
							}
						} else {
							if ($prodd == 'ALL') {
								$data['wew'] = $this->db->query('SELECT * from tbl_pmb_s2_2016 where status ' . $sts . '')->result();
							} else {
								$data['wew'] = $this->db->query('SELECT * from tbl_pmb_s2_2016 where opsi_prodi_s2 = "' . $prodd . '" and status ' . $sts . '')->result();
							}
						}
					} else {
						if ($prodd == 'ALL') {
							$data['wew'] = $this->db->query('SELECT * from tbl_pmb_s2_2016 where status ' . $sts . ' and gelombang = "' . $gelombang . '"')->result();
						} else {
							$data['wew'] = $this->db->query('SELECT * from tbl_pmb_s2_2016 where opsi_prodi_s2 = "' . $prodd . '" and status ' . $sts . ' and gelombang = "' . $gelombang . '"')->result();
						}
					}
				} else {
					if ($gelombang == 'ALL') {
						if ($sts == 'ALL') {
							if ($prodd == 'ALL') {
								$data['wew'] = $this->db->query('SELECT * from tbl_pmb_s2_2016 where kampus = "' . $kampus . '"')->result();
							} else {
								$data['wew'] = $this->db->query('SELECT * from tbl_pmb_s2_2016 where opsi_prodi_s2 = "' . $prodd . '" and kampus = "' . $kampus . '"')->result();
							}
						} else {
							if ($prodd == 'ALL') {
								$data['wew'] = $this->db->query('SELECT * from tbl_pmb_s2_2016 where status ' . $sts . ' and kampus = "' . $kampus . '"')->result();
							} else {
								$data['wew'] = $this->db->query('SELECT * from tbl_pmb_s2_2016 where opsi_prodi_s2 = "' . $prodd . '" and status ' . $sts . ' and kampus = "' . $kampus . '"')->result();
							}
						}
					} else {
						if ($prodd == 'ALL') {
							$data['wew'] = $this->db->query('SELECT * from tbl_pmb_s2_2016 where status ' . $sts . ' and gelombang = "' . $gelombang . '" and kampus = "' . $kampus . '"')->result();
						} else {
							$data['wew'] = $this->db->query('SELECT * from tbl_pmb_s2_2016 where opsi_prodi_s2 = "' . $prodd . '" and status ' . $sts . ' and gelombang = "' . $gelombang . '" and kampus = "' . $kampus . '"')->result();
						}
					}
				}
				$this->load->view('v_status_maba_s2', $data);
			} elseif ($program == 'ALL') {
				if ($kampus == 'ALL') {
					if ($gelombang == 'ALL') {
						if ($sts == 'ALL') {
							$data['dua'] = $this->db->query('SELECT * from tbl_pmb_s2_2016')->result();
							$data['satu'] = $this->db->query('SELECT * from tbl_form_camaba_2016')->result();
						} else {
							$data['dua'] = $this->db->query('SELECT * from tbl_pmb_s2_2016 where status ' . $sts . '')->result();
							$data['satu'] = $this->db->query('SELECT * from tbl_form_camaba_2016 where status ' . $sts . '')->result();
						}
					} else {
						$data['dua'] = $this->db->query('SELECT * from tbl_pmb_s2_2016 where status ' . $sts . ' and gelombang = "' . $gelombang . '"')->result();
						$data['satu'] = $this->db->query('SELECT * from tbl_form_camaba_2016 where status ' . $sts . ' and gelombang = "' . $gelombang . '"')->result();
					}
				} else {
					$data['dua'] = $this->db->query('SELECT * from tbl_pmb_s2_2016 where status ' . $sts . ' and gelombang = "' . $gelombang . '" and kampus = "' . $kampus . '"')->result();
					$data['satu'] = $this->db->query('SELECT * from tbl_form_camaba_2016 where status ' . $sts . ' and gelombang = "' . $gelombang . '" and kampus = "' . $kampus . '"')->result();
				}
				$this->load->view('v_status_maba_all', $data);
			}
		} elseif ($this->input->post('tahun') == '2017') {
			if ($program == 'S1') {
				if ($kampus == 'ALL') {
					if ($gelombang == 'ALL') {
						if ($sts == 'ALL') {
							if ($prodd == 'ALL') {
								$data['wow'] = $this->db->query('SELECT * from tbl_form_camaba')->result();
							} else {
								$data['wow'] = $this->db->query('SELECT * from tbl_form_camaba where prodi = "' . $prodd . '"')->result();
							}
						} else {
							if ($prodd == 'ALL') {
								$data['wow'] = $this->db->query('SELECT * from tbl_form_camaba where status ' . $sts . '')->result();
							} else {
								$data['wow'] = $this->db->query('SELECT * from tbl_form_camaba where status ' . $sts . ' and prodi = "' . $prodd . '"')->result();
							}
						}
					} else {
						if ($prodd == 'ALL') {
							$data['wow'] = $this->db->query('SELECT * from tbl_form_camaba where status ' . $sts . ' and gelombang = "' . $gelombang . '"')->result();
						} else {
							$data['wow'] = $this->db->query('SELECT * from tbl_form_camaba where status ' . $sts . ' and prodi = "' . $prodd . '" and gelombang = "' . $gelombang . '"')->result();
						}
					}
				} else {
					if ($gelombang == 'ALL') {
						if ($sts == 'ALL') {
							if ($prodd == 'ALL') {
								$data['wow'] = $this->db->query('SELECT * from tbl_form_camaba where kampus = "' . $kampus . '"')->result();
							} else {
								$data['wow'] = $this->db->query('SELECT * from tbl_form_camaba where prodi = "' . $prodd . '" and kampus = "' . $kampus . '"')->result();
							}
						} else {
							if ($prodd == 'ALL') {
								$data['wow'] = $this->db->query('SELECT * from tbl_form_camaba where status ' . $sts . ' and kampus = "' . $kampus . '"')->result();
							} else {
								$data['wow'] = $this->db->query('SELECT * from tbl_form_camaba where status ' . $sts . ' and prodi = "' . $prodd . '" and kampus = "' . $kampus . '"')->result();
							}
						}
					} else {
						if ($prodd == 'ALL') {
							$data['wow'] = $this->db->query('SELECT * from tbl_form_camaba where status ' . $sts . ' and gelombang = "' . $gelombang . '" and kampus = "' . $kampus . '"')->result();
						} else {
							$data['wow'] = $this->db->query('SELECT * from tbl_form_camaba where status ' . $sts . ' and prodi = "' . $prodd . '" and gelombang = "' . $gelombang . '" and kampus = "' . $kampus . '"')->result();
						}
					}
				}
				$this->load->view('v_status_maba', $data);
			} elseif ($program == 'S2') {
				if ($kampus == 'ALL') {
					if ($gelombang == 'ALL') {
						if ($sts == 'ALL') {
							if ($prodd == 'ALL') {
								$data['wew'] = $this->db->query('SELECT * from tbl_pmb_s2')->result();
							} else {
								$data['wew'] = $this->db->query('SELECT * from tbl_pmb_s2 where opsi_prodi_s2 = "' . $prodd . '"')->result();
							}
						} else {
							if ($prodd == 'ALL') {
								$data['wew'] = $this->db->query('SELECT * from tbl_pmb_s2 where status ' . $sts . '')->result();
							} else {
								$data['wew'] = $this->db->query('SELECT * from tbl_pmb_s2 where opsi_prodi_s2 = "' . $prodd . '" and status ' . $sts . '')->result();
							}
						}
					} else {
						if ($prodd == 'ALL') {
							$data['wew'] = $this->db->query('SELECT * from tbl_pmb_s2 where status ' . $sts . ' and gelombang = "' . $gelombang . '"')->result();
						} else {
							$data['wew'] = $this->db->query('SELECT * from tbl_pmb_s2 where opsi_prodi_s2 = "' . $prodd . '" and status ' . $sts . ' and gelombang = "' . $gelombang . '"')->result();
						}
					}
				} else {
					if ($gelombang == 'ALL') {
						if ($sts == 'ALL') {
							if ($prodd == 'ALL') {
								$data['wew'] = $this->db->query('SELECT * from tbl_pmb_s2 where kampus = "' . $kampus . '"')->result();
							} else {
								$data['wew'] = $this->db->query('SELECT * from tbl_pmb_s2 where opsi_prodi_s2 = "' . $prodd . '" and kampus = "' . $kampus . '"')->result();
							}
						} else {
							if ($prodd == 'ALL') {
								$data['wew'] = $this->db->query('SELECT * from tbl_pmb_s2 where status ' . $sts . ' and kampus = "' . $kampus . '"')->result();
							} else {
								$data['wew'] = $this->db->query('SELECT * from tbl_pmb_s2 where opsi_prodi_s2 = "' . $prodd . '" and status ' . $sts . ' and kampus = "' . $kampus . '"')->result();
							}
						}
					} else {
						if ($prodd == 'ALL') {
							$data['wew'] = $this->db->query('SELECT * from tbl_pmb_s2 where status ' . $sts . ' and gelombang = "' . $gelombang . '" and kampus = "' . $kampus . '"')->result();
						} else {
							$data['wew'] = $this->db->query('SELECT * from tbl_pmb_s2 where opsi_prodi_s2 = "' . $prodd . '" and status ' . $sts . ' and gelombang = "' . $gelombang . '" and kampus = "' . $kampus . '"')->result();
						}
					}
				}
				$this->load->view('v_status_maba_s2', $data);
			} elseif ($program == 'ALL') {
				if ($kampus == 'ALL') {
					if ($gelombang == 'ALL') {
						if ($sts == 'ALL') {
							$data['dua'] = $this->db->query('SELECT * from tbl_pmb_s2')->result();
							$data['satu'] = $this->db->query('SELECT * from tbl_form_camaba')->result();
						} else {
							$data['dua'] = $this->db->query('SELECT * from tbl_pmb_s2 where status ' . $sts . '')->result();
							$data['satu'] = $this->db->query('SELECT * from tbl_form_camaba where status ' . $sts . '')->result();
						}
					} else {
						$data['dua'] = $this->db->query('SELECT * from tbl_pmb_s2 where status ' . $sts . ' and gelombang = "' . $gelombang . '"')->result();
						$data['satu'] = $this->db->query('SELECT * from tbl_form_camaba where status ' . $sts . ' and gelombang = "' . $gelombang . '"')->result();
					}
				} else {
					$data['dua'] = $this->db->query('SELECT * from tbl_pmb_s2 where status ' . $sts . ' and gelombang = "' . $gelombang . '" and kampus = "' . $kampus . '"')->result();
					$data['satu'] = $this->db->query('SELECT * from tbl_form_camaba where status ' . $sts . ' and gelombang = "' . $gelombang . '" and kampus = "' . $kampus . '"')->result();
				}
				$this->load->view('v_status_maba_all', $data);
			}
		} else {
			if ($program != 'ALL') {
				if ($program == 'S1') {
					$p = 1;
				} else {
					$p = 2;
				}

				$program = 'where program = "' . $p . '"';
			} else {
				$program = 'where program IN (1,2)';
			}
			if ($prodd != 'ALL') {
				$prodi = 'and prodi = "' . $prodd . '"';
			} else {
				$prodi = '';
			}
			if ($kampus != 'ALL') {
				$kps = 'and kampus = "' . $kampus . '"';
			} else {
				$kps = '';
			}
			if ($gelombang > 0) {
				$gelbang = 'and gelombang = "' . $gelombang . '"';
			} else {
				$gelbang = '';
			}
			if ($sts != 'ALL') {
				if ($sts == 'IS NULL') {
					$status = 'and status = 0';
				} else {
					$status = 'and status  ' . $sts . '';
				}
			} else {
				$status = '';
			}

			$query = 'SELECT * from tbl_form_pmb ' . $program . ' ' . $kps . ' ' . $gelbang . ' ' . $prodi . ' ' . $status . ' and nomor_registrasi like "' . $thn . '%" and status_form = 1 order by nomor_registrasi ASC';

			dd($query);
			$data['wew'] = $this->db2->query($query)->result();
			//var_dump('SELECT * from tbl_form_pmb '.$program.' '.$kps.' '.$gelbang.' '.$prodi.' '.$status.' and status_form = 1 order by nomor_registrasi ASC');exit();
			/* echo "<pre>";
			print_r($data['wew']);
			exit;   */
			$this->load->view('v_status_maba_new', $data);
		}
	}
}

/* End of file Data_maba.php */
/* Location: ./application/modules/pmb/controllers/Data_maba.php */
