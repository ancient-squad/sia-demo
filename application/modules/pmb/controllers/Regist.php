<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Regist extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->db_regis = $this->load->database('regis', TRUE);
		if ($this->session->userdata('sess_login_pmb') != TRUE) {
			echo "<script>alert('Akses Tidak Diizinkan!');</script>";
			redirect('auth','refresh');
		}
		
	}

	public function index()
	{
		$data['page'] = "v_regis_select";
		$this->load->view('template/template',$data);
	}

	function save_sess()
	{
		$date1 = $this->input->post('tgl1');
		$date2 = $this->input->post('tgl2');

		$this->session->set_userdata('tgl_mulai', $date1);
		$this->session->set_userdata('tgl_ahir', $date2);

		redirect(base_url('pmb/regist/list_regist'));
	}

	function list_regist()
	{
		$mulai = $this->session->userdata('tgl_mulai');
		$ahir  = $this->session->userdata('tgl_ahir');
		
		$data['qry'] = $this->db_regis->query("SELECT * from tbl_regist where regis_date between '".$mulai." 00:00:00' and '".$ahir." 23:59:00' ")->result();
		
		$data['page'] = "v_list_regis";
		$this->load->view('template/template', $data);
	}

	function update_stat()
	{
		$log = $this->session->userdata('sess_login_pmb');
		$nik = $this->input->post('id');
		$cnt = count($nik);
		for ($i=0; $i < $cnt; $i++) { 
			$data = array(
				'status'		=> 1,
				'audit_user'	=> $log['userid'],
				'audit_time'	=> date('Y-m-d H:i:s')
				);
			$this->db_regis->where('nik',$nik[$i]);
			$this->db_regis->update('tbl_regist', $data);

			$usr['usr'] = $this->db_regis->query("SELECT * from tbl_regist where nik = '".$nik[$i]."'")->row();
			$dats[] = array(
				'email'		=> $usr['usr']->email,
				'password'	=> crypt(sha1($usr['usr']->userid), keyreg),
				'userid'	=> $usr['usr']->userid
				);

			// send e-mail	
			$this->load->library('email');
			$isi = $this->load->view('v_email', $usr, TRUE);
			$judul=$this->ORG_NAME;
			$config = array(
	            'protocol' 	=> 'smtp',
	            'smtp_host' => 'ssl://smtp.gmail.com',
	            'smtp_port' => 465,
	            'smtp_user' => 'it@'.$this->URL,    	//email id
	            'smtp_pass' => '192735**',            			// password
	            'mailtype'  => 'html',
	            'charset'   => 'iso-8859-1',
	            'validation'=> TRUE
	        );
	       	$this->email->initialize($config);
	        $this->email->set_newline("\r\n");
	        $this->email->from('it@'.$this->URL,'emailtest');
	        $this->email->to($usr['usr']->email); // email array
	        $this->email->subject($judul);  
	        $this->email->message($isi);
	        $result = $this->email->send();
		}
		$this->db_regis->insert_batch('tbl_user_login',$dats);

		echo "<script>alert('Berhasil!');document.location.href='".base_url('pmb/regist/list_regist')."';</script>";
	}

	function destroy_date()
	{
		$this->session->unset_userdata('sess_date');
		redirect(base_url('pmb/regist'));
	}

	function rekapSurat()
	{
		$data['page'] = "v_rekapsurat";
		$this->load->view('template/template', $data);
	}

	function byPass()
	{
		$a = $this->input->post('wave');
		$this->session->set_userdata('wavy', $a);
		redirect(base_url('pmb/regist/loadDataRekap'),'refresh');
	}

	function loadDataRekap()
	{
		$b = $this->session->userdata('wavy');
		$data['load'] = $this->db_regis->query("SELECT * from tbl_form_pmb where SUBSTRING(user_input,3,1) = '".$b."'")->result();
		$data['page'] = "v_datarekap";
		$this->load->view('template/template', $data);
	}

	function destByPass()
	{
		$this->session->unset_userdata('wavy');
		redirect('pmb/regist/rekapSurat','refresh');
	}

	function kelengkapan($id)
	{
		$data['mhspmb'] = $this->db_regis->query("SELECT * from tbl_form_pmb where nomor_registrasi = '".$id."'")->row();
		$this->load->view('v_load_kelengkapan',$data);
	}

}

/* End of file Regist.php */
/* Location: ./application/modules/pmb/controllers/Regist.php */