<?php
header("Content-Type: application/xls");    
if ($this->session->userdata('kate') == 'edu0') {
    $o = 'Pendidikan_Ayah';
} elseif ($this->session->userdata('kate') == 'wrk0') {
    $o = 'Pekerjaan_Ayah';
} elseif ($this->session->userdata('kate') == 'edu1') {
    $o = 'Pendidikan_Ibu';
} elseif ($this->session->userdata('kate') == 'wrk1') {
    $o = 'Pekerjaan_Ibu';
} elseif ($this->session->userdata('kate') == 'sch') {
    $o = 'Asal_Sekolah';
} elseif ($this->session->userdata('kate') == 'cty') {
    $o = 'Asal_Kota';
} elseif ($this->session->userdata('kate') == 'grd') {
    $o = 'Tahun_Lulus';
} 
header("Content-Disposition: attachment; filename=DATA_".$o."_MHS_".$prodss->prodi.".xls"); 
header("Pragma: no-cache"); 
header("Expires: 0");
?>

<style>
table, td, th {
    border: 1px solid black;
}

th {
    background-color: blue;
    color: black;
}
</style>
                    
<table>
	<thead>
        <tr> 
        	<th rowspan="3">No</th>
            <th rowspan="3">Prodi</th>
            <th rowspan="3">Gelombang</th>
            <th rowspan="3">Jenis Pendaftar</th>
            <th rowspan="3">Status</th>
            <th style="text-align:center" colspan="2">Kategori</th>
        </tr>
        <tr>
            <?php if ($this->session->userdata('kate') == 'edu0') {
                    $q = 'Pendidikan Ayah';
                } elseif ($this->session->userdata('kate') == 'wrk0') {
                    $q = 'Pekerjaan Ayah';
                } elseif ($this->session->userdata('kate') == 'edu1') {
                    $q = 'Pendidikan Ibu';
                } elseif ($this->session->userdata('kate') == 'wrk1') {
                    $q = 'Pekerjaan Ibu';
                } elseif ($this->session->userdata('kate') == 'sch') {
                    $q = 'Asal Sekolah';
                } elseif ($this->session->userdata('kate') == 'cty') {
                    $q = 'Asal Kota';
                } elseif ($this->session->userdata('kate') == 'grd') {
                    $q = 'Tahun Lulus';
                }
            ?>
            <th colspan="2"><?php echo $q; ?></th>
        </tr>
        <tr>
            <th>Kategori</th>
            <th>Jumlah</th>
        </tr>
    </thead>
    <tbody>
        <?php $no = 1;
        foreach ($wow as $key) { ?>
            <tr>
                <td><?php echo $no; ?></td>
                <td rowspan="<?php count($key->prodi) ?>"><?php echo $key->prodi; ?></td>
                <td><?php echo $key->gelombang; ?></td>
                <td><?php echo $key->jenis_pmb; ?></td>
                    <?php 
                        if ($key->status < 1) {
                            $kadal =  "Registrasi";
                        } elseif($key->status == 1) {
                            $kadal =  "Lulus Tes";
                        } elseif ($key->status > 1) {
                            $kadal =  "Daftar Ulang";
                    }?>
                <td><?php echo $kadal; ?></td>
                <?php 
                    if ($this->session->userdata('kate') == 'edu0') {
                        if ($key->didik_ayah == 'TSD') {
                            $abon = "Tidak Tamat SD";
                        } elseif($key->didik_ayah == 'SD') {
                            $abon = "Tamat SD";
                        } elseif ($key->didik_ayah == 'SLTP') {
                            $abon = "Tamata SLTP";
                        } elseif ($key->didik_ayah == 'SLTA') {
                            $abon = "Tamat SLTA";
                        } elseif ($key->didik_ayah == 'D') {
                            $abon = "Diploma";
                        } elseif ($key->didik_ayah == 'SM') {
                            $abon = "Sarjana Muda";
                        } elseif ($key->didik_ayah == 'S') {
                            $abon = "Sarjana";
                        } elseif ($key->didik_ayah == 'PSC') {
                            $abon = "Pascasarjana";
                        } elseif ($key->didik_ayah == 'DTR') {
                            $abon = "Doktor";
                        }
                    } elseif ($this->session->userdata('kate') == 'wrk0') {
                        if ($key->workdad == 'PN') {
                            $abon = "Pegawai Negeri";
                        } elseif($key->workdad == 'TP') {
                            $abon = "TNI / POLRI";
                        } elseif ($key->workdad == 'PS') {
                            $abon = "Pegawai Swasta";
                        } elseif ($key->workdad == 'WU') {
                            $abon = "Wirausaha";
                        } elseif ($key->workdad == 'PSN') {
                            $abon = "Pensiun";
                        } elseif ($key->workdad == 'TK') {
                            $abon = "Tidak Bekerja";
                        } elseif ($key->workdad == 'LL') {
                            $abon = "Lain-lain";
                        }
                    } elseif ($this->session->userdata('kate') == 'edu1') {
                        if ($key->didik_ibu == 'TSD') {
                            $abon = "Tidak Tamat SD";
                        } elseif($key->didik_ibu == 'SD') {
                            $abon = "Tamat SD";
                        } elseif ($key->didik_ibu == 'SLTP') {
                            $abon = "Tamata SLTP";
                        } elseif ($key->didik_ibu == 'SLTA') {
                            $abon = "Tamat SLTA";
                        } elseif ($key->didik_ibu == 'D') {
                            $abon = "Diploma";
                        } elseif ($key->didik_ibu == 'SM') {
                            $abon = "Sarjana Muda";
                        } elseif ($key->didik_ibu == 'S') {
                            $abon = "Sarjana";
                        } elseif ($key->didik_ibu == 'PSC') {
                            $abon = "Pascasarjana";
                        } elseif ($key->didik_ibu == 'DTR') {
                            $abon = "Doktor";
                        }
                    } elseif ($this->session->userdata('kate') == 'wrk1') {
                        if ($key->workmom == 'PN') {
                            $abon = "Pegawai Negeri";
                        } elseif($key->workmom == 'TP') {
                            $abon = "TNI / POLRI";
                        } elseif ($key->workmom == 'PS') {
                            $abon = "Pegawai Swasta";
                        } elseif ($key->workmom == 'WU') {
                            $abon = "Wirausaha";
                        } elseif ($key->workmom == 'PSN') {
                            $abon = "Pensiun";
                        } elseif ($key->workmom == 'TK') {
                            $abon = "Tidak Bekerja";
                        } elseif ($key->workmom == 'LL') {
                            $abon = "Lain-lain";
                        }
                    } elseif ($this->session->userdata('kate') == 'sch') {
                        if ($key->jenis_sch_maba == 'MA') {
                            $abon = "MA";
                        } elseif($key->jenis_sch_maba == 'SMA') {
                            $abon = "SMA";
                        } elseif ($key->jenis_sch_maba == 'SMK') {
                            $abon = "SMK";
                        } elseif ($key->jenis_sch_maba == 'LAIN') {
                            $abon = "LAIN-LAIN";
                        } elseif ($key->jenis_sch_maba == 'SMTB') {
                            $abon = "SMTB";
                        }
                    } elseif ($this->session->userdata('kate') == 'cty') {
                        $abon = $key->kota_sch_maba;
                    } elseif ($this->session->userdata('kate') == 'grd') {
                        $abon = $key->lulus_maba;
                    }
                ?>
                <td><?php echo $abon; ?></td>
                <td><?php echo $key->hitung; ?></td>
            </tr>
        <?php $no++; }  ?>
    </tbody>
</table>