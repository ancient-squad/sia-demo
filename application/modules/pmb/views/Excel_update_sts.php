<?php
header("Content-Type: application/xls");    
header("Content-Disposition: attachment; filename=DATA_CALON_MHS.xls");
header("Pragma: no-cache"); 
header("Expires: 0");
?>

<style>
table, td, th {
    border: 1px solid black;
}

th {
    background-color: blue;
    color: black;
}
</style>
<center><b>Daftar Calon Mahasiswa <?= $this->ORG_NAME ?></b></center>
<p style="background-color:#7FFFD4">KET : Untuk kolom status diisi dengan angka 1 (jika peserta dinyatakan lulus) atau 0 (jika peseta dinyatakan tidak lulus)</p>        
<table>
	<thead>
        <tr> 
        	<th>No</th>
            <th>ID Registrasi</th>
            <th>Nama</th>
            <?php if ($progr == 'S1') { ?>
                <th width="130">Asal Sekolah</th>
            <?php } ?>
            <th>Prodi</th>
            <th>Kampus</th>
            <th width="120">Gelombang</th>
            <th>Status</th>
        </tr>
    </thead>
    <tbody>
		<?php $no = 1; foreach($query->result() as $row) { ?>
        <tr>
            <td><?php echo $no;?></td>

            <?php if ($progr == 'S1') { ?>
                <td><?php echo $row->nomor_registrasi;?></td>
            <?php } else { ?>
                <td><?php echo $row->ID_registrasi;?></td>
            <?php } ?>

            <td><?php echo $row->nama;?></td>

            <?php if ($progr == 'S1') { ?>
                <td><?php echo $row->asal_sch_maba; ?></td>
            <?php } ?>

            <?php if ($progr == 'S1') { ?>
                <td><?php echo get_jur($row->prodi);?></td>
            <?php } else { ?>
                <td><?php echo get_jur($row->opsi_prodi_s2);?></td>
            <?php } ?>

            <td><?php if ($row->kampus == 'bks') {
                $camp = 'Bekasi';
            } else {
                $camp = 'Jakarta';
            }
             echo $camp; ?></td>
            
            <td><?php echo $row->gelombang; ?></td>
            <td></td>
        </tr>
		<?php $no++; } ?>
    </tbody>
</table>