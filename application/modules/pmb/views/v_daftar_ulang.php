<?php 
	$tgl1 = '2016';
	$tgl2 = date('Y')+1;
 ?>

<div class="row">
	<div class="span12">      		  		
  		<div class="widget ">
  			<div class="widget-header">
                <i class="icon-home"></i>
  				<h3>Data Daftar Ulang Mahasiswa Baru</h3>
			</div>
			<div class="widget-content">
				<form class="form-horizontal" action="<?php echo base_url(); ?>pmb/daftar_ulang/load_data" method="post" enctype="multipart/form-data">
					<fieldset>
						<div class="control-group">
							<label class="control-label">Pilih Tahun</label>
							<div class="controls">
								<select class="form-control span4" name="tahun" required/>
									<option disabled="" selected="">--Pilih Tahun--</option>
									<?php for ($i = $tgl1; $i <= $tgl2 ; $i++) { ?>
										<option value="<?php echo $i; ?>"><?php echo $i; ?></option>
									<?php } ?>
								</select>
							</div>
						</div>

						<div class="control-group">
							<label class="control-label">Pilih Kampus</label>
							<div class="controls">
								<select class="form-control span4" name="kampus" required/>
									<option disabled="" selected="">--Pilih Kampus--</option>
									<option value="ALL">Semua Kampus</option>
									<option value="bima">Kota Bima</option>
								</select>
							</div>
						</div>

						<div class="control-group">
							<label class="control-label">Pilih Program</label>
							<div class="controls">
								<select class="form-control span4" name="program" required/>
									<option disabled="" selected="">--Pilih Program--</option>
									<option value="ALL">Semua Program</option>
									<option value="S1">S1</option>
								</select>
							</div>
						</div>

						<div class="control-group">
							<label class="control-label">Pilih Gelombang</label>
							<div class="controls">
								<select class="form-control span4" name="gelombang" required/>
									<option disabled="" selected="">--Pilih Gelombang--</option>
									<option value="ALL">Semua Gelombang</option>
									<option value="1">1</option>
									<option value="2">2</option>
									<option value="3">3</option>
									<option value="4">4</option>
									<option value="5">5</option>
								</select>
							</div>
						</div>
						<div class="form-actions">
							<input class="btn btn-large btn-success" type="submit" value="Download Excel">
						</div>
					</fieldset>
				</form>
			</div>
		</div>
	</div>
</div>

