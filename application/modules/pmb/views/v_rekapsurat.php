<div class="row">
	<div class="span12">      		  		
  		<div class="widget ">
  			<div class="widget-header">
                <i class="icon-file"></i>
  				<h3>Kelengkapan Data Calon Mahasiswa</h3>
			</div>
			<div class="widget-content">
				<form class="form-horizontal" action="<?php echo base_url(); ?>pmb/regist/byPass" method="post">
					<fieldset>
						<div class="control-group">
							<label class="control-label">Gelombang</label>
							<div class="controls">
								<select class="form-control span6" name="wave">
                                    <option selected="" disabled="">-- Pilih Gelombang --</option>
                                    <?php for ($i = 1; $i < 6; $i++) { ?>
                                        <option value="<?php echo $i ?>"><?php echo $i; ?></option>
                                    <?php } ?>
                                </select>
							</div>
						</div>
						<div class="form-actions">
							<input class="btn btn-large btn-success" type="submit" value="Submit">
						</div>

					</fieldset>
				</form>
			</div>
		</div>
	</div>
</div>

