<?php
header("Content-Type: application/xls");    
header("Content-Disposition: attachment; filename=hasil_tes.xls");  
header("Pragma: no-cache"); 
header("Expires: 0");
?>

<style>
table, td, th {
    border: 1px solid black;
}

th {
    background-color: blue;
    color: black;
}
</style>

<table border="1">
	<thead>
		<tr>
			<th>No</th>
			<th>ID Registrasi</th>
			<th>Nama</th>
			<th>Status</th>
		</tr>
	</thead>
	<tbody>
		<?php $no = 1; foreach($qr as $row) { ?>
		<tr>
			<td><?php echo number_format($no); ?></td>
			<td><?php echo $row->ID_registrasi; ?></td>
			<td><?php echo $row->nama; ?></td>	
			<td></td>		
		</tr>
		<?php $no++; } ?>
	</tbody>
</table>