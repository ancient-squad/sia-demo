<?php
header("Content-Type: application/xls");  
if ($this->session->userdata('kate') == 'wrk') {
    $o = 'Pekerjaan';
}  elseif ($this->session->userdata('kate') == 'cty') {
    $o = 'Asal Kota';
} elseif ($this->session->userdata('kate') == 'grd') {
    $o = 'Tahun Lulus';
}  
header("Content-Disposition: attachment; filename=DATA_".$o."_MHS_S2_".$prodss->prodi.".xls");
header("Pragma: no-cache"); 
header("Expires: 0");
?>

<style>
table, td, th {
    border: 1px solid black;
}

th {
    background-color: blue;
    color: black;
}
</style>
                    
<table>
	<thead>
        <tr> 
        	<th rowspan="3">No</th>
            <th rowspan="3">Prodi</th>
            <th rowspan="3">Gelombang</th>
            <th rowspan="3">Jenis Pendaftar</th>
            <th rowspan="3">Status</th>
            <th style="text-align:center" colspan="<?php echo $col; ?>">Kategori</th>
        </tr>
        <tr>
            <?php if ($this->session->userdata('kate') == 'wrk') {
                    $q = 'Pekerjaan';
                }  elseif ($this->session->userdata('kate') == 'cty') {
                    $q = 'Asal Kota';
                } elseif ($this->session->userdata('kate') == 'grd') {
                    $q = 'Tahun Lulus';
                }
            ?>
            <th colspan="2"><?php echo $q; ?></th>
        </tr>
        <tr>
            <th>Kategori</th>
            <th>Jumlah</th>
        </tr>
    </thead>
    <tbody>
        <?php $no = 1;
        foreach ($wew as $key) {  ?>
            <tr>
                <td><?php echo $no; ?></td>
                <td rowspan="<?php count($key->prodi) ?>"><?php echo $key->prodi; ?></td>
                <td><?php echo $key->gelombang; ?></td>
                <td><?php echo $key->jenis_pmb; ?></td>
                    <?php 
                        if ($key->status < 1) {
                            $kadal =  "Registrasi";
                        } elseif($key->status == 1) {
                            $kadal =  "Lulus Tes";
                        } elseif ($key->status > 1) {
                            $kadal =  "Daftar Ulang";
                    }?>
                <td><?php echo $kadal; ?></td>
                <td>
                    <?php 
                        if ($this->session->userdata('kate') == 'wrk') {
                            echo $this->pekerjaan;
                        } elseif ($this->session->userdata('kate') == 'cty') {
                            echo $key->kota_asl_univ;
                        } elseif ($this->session->userdata('kate') == 'grd') {
                            echo $key->th_lulus;
                        }
                    ?>
                </td>
                <td><?php echo $key->hitung; ?></td>
            </tr>
        <?php $no++; } ?>
    </tbody>
</table>