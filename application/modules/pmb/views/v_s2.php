<script type="text/javascript">
	$(document).ready(function () {
		$('#read').hide();
		$('#konv').hide();
		$('#maba').hide();

		$('#rm').click(function () {
            $('#konv').hide('fast');
            $('#maba').hide();
            $('#read').show('fast');
        });
        $('#kv').click(function () {
        	$('#read').hide('fast');
        	$('#maba').hide();
            $('#konv').show('fast');
        });
        $('#mhs').click(function () {
        	$('#read').hide('fast');
            $('#konv').hide('fast');
            $('#maba').show();
        });

        //status kerja
        $('#ttbkr').hide('fast');
        $('#bkr').click(function () {
            $('#ttbkr').hide('fast');
        });
        $('#tbkr').click(function () {
            $('#ttbkr').show('fast');
        });
	})
</script>

<div class="row">
	<div class="span12">      		  		
  		<div class="widget ">
  			<div class="widget-header">
                <i class="icon-home"></i>
  				<h3>Data PMB Program Pasca Sarjana (S.2)</h3>
			</div>
			<div class="widget-content">
				<form class="form-horizontal" action="<?php echo base_url(); ?>pmb/forms2/inputdata" method="post" enctype="multipart/form-data">
					<fieldset>
						<div class="control-group">
							<label class="control-label">Jenis PMB</label>
							<div class="controls">
								<select class="form-control span4" name="jenis" required/>
									<option disabled="" selected="">--Pilih Jenis PMB--</option>
									<!-- <option value="RM" id="rm">Readmisi</option> -->
									<option value="MB" id="mhs">Mahasiswa Baru</option>
									<option value="KV" id="kv">Mahasiswa Konversi</option>
								</select>
							</div>
						</div>
						<div class="control-group">
							<label class="control-label">Pilihan Kampus</label>
							<div class="controls">
								<select class="form-control span4" name="kampus" required/>
									<option disabled="" selected="">--Pilih Kampus--</option>
									<option value="jkt">Jakarta</option>
									<option value="bks">Bekasi</option>
								</select>
							</div>
						</div>
						<div class="control-group">
							<label class="control-label">Keterangan</label>
							<div class="controls">
								<select class="form-control span2"  name="ket" required>
									<option value="1">POLISI / PNS POLRI</option>
									<option value="2">KELUARGA POLISI</option>
									<option value="0">UMUM</option>
								</select>
							</div>
						</div>
						<div class="control-group">
							<label class="control-label">NIK</label>
							<div class="controls">
								<input type="text" class="form-control span4" placeholder="Nomor Induk Kependudukan" name="nik" maxlength=16 minlength=16><br>
								<small>*sesuai ktp / kartu keluarga</small>
							</div>
						</div>
						<div class="control-group">
							<label class="control-label">Nama</label>
							<div class="controls">
								<input type="text" class="form-control span4" name="nama" required/><br><small>*sesuai ijazah/akte kelahiran</small>
							</div>
						</div>
						<div class="control-group">
							<label class="control-label">Jenis Kelamin</label>
							<div class="controls">
								<input type="radio" name="jk" value="L" required/> Laki - Laki <br>
								<input type="radio" name="jk" value="P"/> Perempuan
							</div>
						</div>
						<script type="text/javascript" charset="utf-8" async defer>
							/** kewarganegaraan-start **/
							$(document).ready(function () {
								$('#wno').hide();
							    $('#wni').click(function () {
							        $('#wno').hide();
							    });
							    $('#wna').click(function () {
							        $('#wno').show();
							    });
							});
						    /** kewarganegaraan-end **/
						</script>
						<div class="control-group">
							<label class="control-label">Kewarganegaraan</label>
							<div class="controls">
								<input type="radio" id="wni" name="wn" value="WNI" required> WNI <br>
								<input type="radio" id="wna" name="wn" value="WNA"> WNA
								<input type="text" id="wno" name="jns_wn" class="form-control span4" placeholder="Isi Kewarganegaraan (jika WNA)">
							</div>
						</div>
						<div class="control-group">
							<label class="control-label">Tempat Lahir</label>
							<div class="controls">
								<input class="form-control span4" name="tpt" required/>
							</div>
						</div>
						<div class="control-group">
							<label class="control-label">Tanggal Lahir</label>
							<script>
							  	$(function() {
							    	$( "#tgllhr" ).datepicker({
								      	changeMonth: true,
								      	changeYear: true,
								      	dateFormat: "yy-mm-dd",
								      	yearRange: "1950:2030"
							    	});
							  	});
							</script>
							<div class="controls">
								<input class="form-control span4" type="text" name="tgl" id="tgllhr" required/>
							</div>
						</div>
						<div class="control-group">
							<label class="control-label">Agama</label>
							<div class="controls">
								<select class="form-control span4" name="agama" required/>
									<option disabled="" selected="">-- Pilih Agama --</option>
									<option value="ISL">Islam</option>
									<option value="KTL">Katolik</option>
									<option value="PRT">Protestan</option>
									<option value="BDH">Budha</option>
									<option value="HND">Hindu</option>
									<option value="OTH">Lainnya</option>
								</select>
							</div>
						</div>
						<div class="control-group">
							<label class="control-label">Status Pernikahan</label>
							<div class="controls">
								<table>
									<tbody>
										<tr>
											<td>
												<input type="radio" name="stsm" value="Y" required/> Menikah &nbsp;&nbsp;&nbsp;&nbsp;
											</td>
											<td>
												<input type="radio" name="stsm" value="N"> Belum Menikah &nbsp;&nbsp;&nbsp;&nbsp;
											</td>
											<td>
												<input type="radio" name="stsm" value="D"> Duda/Janda
											</td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>
						<div class="control-group">
							<label class="control-label">Status Pekerjaan</label>
							<div class="controls">
								<table>
									<tbody>
										<tr>
											<td>
												<input type="radio" id="bkr" name="stsb" value="N"> Belum Bekerja &nbsp;&nbsp;&nbsp;
											</td>
											<td>
												<input type="radio" id="tbkr" name="stsb" value="Y"> Bekerja
												<input class="form-control span4" id="ttbkr" type="text" name="tpt_kerja" placeholder="Profesi pekerjaan anda">
											</td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>
						<div class="control-group">
							<label class="control-label">Alamat</label>
							<div class="controls">
								<!-- <textarea class="form-control span4" type="text" name="alamat" required/></textarea> -->
								<input class="form-control span3" placeholder="Jalan" type="text"  name="jalan" required>
								<input class="form-control span2" placeholder="RT" type="text"  name="rt" required>
								<input class="form-control span2" placeholder="RW" type="text"  name="rw" required>
								<input class="form-control span3" placeholder="Perumahan" type="text"  name="perum" required>
							</div>
						</div>
						<div class="control-group">
							<label class="control-label"></label>
							<div class="controls">
								<!-- <textarea class="form-control span4" type="text"  name="alamat" required></textarea> -->
								<input class="form-control span4" placeholder="Kelurahan" type="text"  name="lurah" required>
								<input class="form-control span4" placeholder="Kecamatan" type="text"  name="camat" required>
							</div>
						</div>
						<div class="control-group">
							<label class="control-label">Kode Pos</label>
							<div class="controls">
								<input class="form-control span4" type="number" name="kdpos" required/>
							</div>
						</div>
						<div class="control-group">
							<label class="control-label">No. Telpon / HP</label>
							<div class="controls">
								<input class="form-control span4" type="number" name="tlp" required/>
							</div>
						</div>

						<div class="control-group">
							<label class="control-label">E-Mail</label>
							<div class="controls">
								<input class="form-control span3" placeholder="Isi dengan E-Mail aktif" type="email"  name="email" required="">
							</div>
						</div>

						<!-- maba start -->
						<div id="maba">
							<div class="control-group">
								<label class="control-label">Asal Universitas</label>
								<div class="controls">
									<input class="form-control span4" type="text" name="nm_pt">
								</div>
							</div>
						</div>
						<!-- maba end -->

						<!-- readmisi start -->
						<div id="read">
							<div class="control-group">
								<label class="control-label">NPM Lama</label>
								<div class="controls">
									<input class="form-control span4" type="number" name="npm">
								</div>
							</div>
							<div class="control-group">
								<label class="control-label">Tahun Masuk di <?= $this->ORG_NAME ?></label>
								<div class="controls">
									<input class="form-control span4" type="number" name="thmasuk">
								</div>
							</div>
						</div>
						<!-- readmisi end -->

						<!-- konversi start -->
						<div id="konv">
							<div class="control-group">
								<label class="control-label">Jenis Perguruan Tinggi</label>
								<div class="controls">
									<input type="radio" name="jenispt" value="ngr" > NEGERI  <br>
									<input type="radio" name="jenispt" value="swt"> SWASTA
								</div>
							</div>
							<div class="control-group">
								<label class="control-label">Nama Perguruan Tinggi</label>
								<div class="controls">
									<input class="form-control span4" type="text" name="nm_pt_konv">
								</div>
							</div>
							<div class="control-group">
								<label class="control-label">Program Studi</label>
								<div class="controls">
									<input class="form-control span4" type="text" name="prodi_lm">
								</div>
							</div>
							<div class="control-group">
								<label class="control-label">Tahun lulus/Semester</label>
								<div class="controls">
									<input class="form-control span2" type="text" placeholder="Tahun Lulus" name="thlulus">
									<input class="form-control span2" type="text" placeholder="Semester" name="smtlulus">
								</div>
							</div>
							<div class="control-group">
								<label class="control-label">Kota Asal PTS/PTN</label>
								<div class="controls">
									<input class="form-control span4" type="text" name="kota_asal">
								</div>
							</div>
							<div class="control-group">
								<label class="control-label">NPM/NIM</label>
								<div class="controls">
									<input class="form-control span4" type="text" name="npmnim">
								</div>
							</div>
						</div>
						<!-- konversi end -->


						<div class="control-group">
							<label class="control-label">Program Pasca Sarjana(S.2) Pilihan I</label>
							<div class="controls">
								<select class="form-control span4" name="prodi" required/>
									<option disabled="" selected="">-- Pilih Program Studi--</option>
									<option value="61101">Magister Manajemen</option>
									<option value="74101">Magister Hukum</option>
								</select>
							</div>
						</div>

						<div class="control-group">
							<label class="control-label">Program Pasca Sarjana(S.2) Pilihan II</label>
							<div class="controls">
								<select class="form-control span4" name="prodi2" required/>
									<option disabled="" selected="">-- Pilih Program Studi--</option>
									<option value="61101">Magister Manajemen</option>
									<option value="74101">Magister Hukum</option>
								</select>
							</div>
						</div>

						<div class="control-group">
							<label class="control-label">Pekerjaan</label>
							<div class="controls">
								<select class="form-control span4" name="pekerjaan" required/>
									<option disabled="" selected="">-- Pilih Pekerjaan --</option>
									<option value="PN">Pegawai Negeri</option>
									<option value="TP">TNI / POLRI</option>
									<option value="PS">Pegawai Swasta</option>
									<option value="WU">Wirausaha</option>
									<option value="PE">Pensiun</option>
									<option value="TK">Tidak Bekerja</option>
									<option value="LL">Lain-lain</option>
								</select>
							</div>
						</div>
						<div class="control-group">
							<label class="control-label">Nama Ayah</label>
							<div class="controls">
								<input class="form-control span4" type="text" name="nm_ayah" required/>
							</div>
						</div>
						<div class="control-group">
							<label class="control-label">Nama Ibu</label>
							<div class="controls">
								<input class="form-control span4" type="text" name="nm_ibu" required/>
							</div>
						</div>
						<div class="control-group">
							<label class="control-label">Penghasilan Pribadi</label>
							<div class="controls">
								<table>
									<tbody>
										<tr>
											<td>
												<input type="radio" value="1" name="gaji" required/>
												Rp 1,000,000 - 2,000,000 &nbsp&nbsp&nbsp&nbsp
											</td>
											<td>
												<input type="radio" value="2" name="gaji"/>
												Rp 2,100,000 - 4,000,000
											</td>
										</tr>
										<tr>
											<td>
												<input type="radio" value="3" name="gaji"/>
												Rp 4,100,000 - 6,000,000
											</td>
											<td>
												<input type="radio" value="4" name="gaji"/>
												> Rp 6,000,000
											</td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>
						<div class="control-group">
							<label class="control-label">Upload Foto</label>
							<div class="controls">
								<input id="wizard-picture" type="file" name="img">
							</div>
						</div>
						<script type="text/javascript">
							/** bpjs-start **/
							$(document).ready(function () {
								$('#bpjs-yes').hide();
								$('#bpjs-y').click(function () {
									$('#bpjs-yes').show();
								});	

								$('#bpjs-n').click(function () {
									$('#bpjs-yes').hide();
								});
							});
							/** bpjs-end **/
						</script>
						<div class="control-group" id="">
							<label class="control-label">Pengguna BPJS </label>
							<div class="controls">
								<input type="radio" id="bpjs-y" name="bpjs" value="y" required> Ya &nbsp;&nbsp;
								<input type="radio" id="bpjs-n" name="bpjs" value="n" > Tidak &nbsp;&nbsp; 
							</div>
						</div>
						<div class="control-group" id="bpjs-yes">
							<label class="control-label">NO. BPJS</label>
							<div class="controls">
								<input class="form-control span3" placeholder="No. BPJS Calon Mahasiswa"  type="text"  name="nobpjs">
							</div>
						</div>
						<div class="control-group" id="bpjs-yes">
							<label class="control-label">Transportasi</label>
							<div class="controls">
								<select class="form-control span2"  name="transport">
									<option disabled="" selected="">-- Alat Transportasi --</option>
									<option value="MBL">Mobil</option>
									<option value="MTR">Motor</option>
									<option value="AKT">Angkutan Umum</option>
									<option value="SPD">Sepeda</option>
									<option value="JKK">Jalan Kaki</option>
									<option value="ADG">Andong</option>
									<option value="KRT">Kereta</option>
								</select>
							</div>
						</div>
						<div class="control-group">
							<label class="control-label">Referensi</label>
							<div class="controls">
								<input class="form-control span4" placeholder="informasi mengenai ubhara diperoleh dari" type="text" name="infrom" required/>
							</div>
						</div>
						<div class="control-group" id="new4">
							<label class="control-label">Ukuran Almamater</label>
							<div class="controls">
								<select class="form-control span2"  name="sizealmet">
									<option disabled="" selected="">-- Pilih Ukuran --</option>
									<option value="S">S</option>
                                    <option value="M">M</option>
                                    <option value="L">L</option>
                                    <option value="X">XL</option>
                                    <option value="2">XXL</option>
                                    <option value="3">XXXL</option>
								</select> 
							</div>
						</div>
						<div class="control-group" id="new4">
							<script type="text/javascript">
								$(document).ready(function () {
									$('#lba').hide();
									$('#ktr').hide();
									$('#spd').hide();
									$('#stf').hide();

									$('#rm').click(function () {
										$('#lba').show();
										$('#ktr').show();
										$('#ijz').hide();
										$('#skl').hide();
										$('#spd').hide();
										$('#stf').hide();
									});

									$('#kv').click(function () {
										$('#spd').show();
										$('#stf').show();
										$('#skl').hide();
										$('#lba').hide();
										$('#ktr').hide();
									});

									$('#mhs').click(function () {
										$('.base').show();
										$('#lba').hide();
										$('#ktr').hide();
										$('#spd').hide();
										$('#stf').hide();
									});
								});
							</script>
							<label class="control-label">Kelengkapan Berkas</label>
							<div class="controls">
								<div class="base" id="akt"><input type="checkbox" name="lengkap[]" value="AKT"> Akte Kelahiran &nbsp;&nbsp;</div>
								<div class="base" id="kk"><input type="checkbox" name="lengkap[]" value="KK"> Kartu Keluarga (KK) &nbsp;&nbsp; </div>
								<div class="base" id="ktp"><input type="checkbox" name="lengkap[]" value="KTP"> Kartu Tanda Penduduk (KTP)  &nbsp;&nbsp;</div>
								<div class="base" id="ft"><input type="checkbox" name="lengkap[]" value="FT"> Foto (3x4 dan 4x6) &nbsp;&nbsp;</div>
								<div class="base" id="ijz"><input type="checkbox" name="lengkap[]" value="IJZ"> Ijazah &nbsp;&nbsp; </div>
								<div class="base" id="skl"><input type="checkbox" name="lengkap[]" value="SKL"> Surat Kelulusan  &nbsp;&nbsp;<br></div>
								<div class="base" id="tkr"><input type="checkbox" name="lengkap[]" value="TKR"> Transkrip  &nbsp;&nbsp;</div>

								<div id="lba"><input type="checkbox" name="lengkap[]" value="LBAA"> Laporan BAA  &nbsp;&nbsp;</div>
								<div id="ktr"><input type="checkbox" name="lengkap[]" value="KETREN"> Keterangan RENKEU  &nbsp;&nbsp;</div>

								<div id="spd"><input type="checkbox" name="lengkap[]" value="SPD"> Surat Pindah  &nbsp;&nbsp;</div>
								<div id="stf"><input type="checkbox" name="lengkap[]" value="STFK"> Sertifikat Akreditasi  &nbsp;&nbsp;</div>
								
								<div class="base" id="lkp"><input type="checkbox" name="lengkap[]" value="LLKP"> Lengkap Administratif  &nbsp;&nbsp;</div>
							</div>
						</div>
						<div class="form-actions">
							<input class="btn btn-large btn-primary" type="submit" value="Submit">
							<input class="btn btn-large btn-default" type="reset" value="Clear">
						</div>
					</fieldset>
				</form>
			</div>
		</div>
	</div>
</div>
