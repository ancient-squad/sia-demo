<div class="row">
	<div class="span12">      		  		
  		<div class="widget ">
  			<div class="widget-header">
                <i class="icon-home"></i>
  				<h3>Data PMB</h3>
			</div>
			<div class="widget-content">
				<form class="form-horizontal" action="<?php echo base_url(); ?>pmb/hasil_tes/simpan_sesi" method="post" enctype="multipart/form-data">
					<fieldset>
						<div class="control-group">
							<label class="control-label">Pilih Pendaftaran</label>
							<div class="controls">
								<select class="form-control span4" name="jenjang" required/>
									<option disabled selected>--Pilih Pendaftaran--</option>
									<option value="1">S1</option>
								</select>
							</div>
						</div>
						<div class="control-group">
							<label class="control-label">Pilih Jenis</label>
							<div class="controls">
								<select class="form-control span4" name="jenis" required/>
									<option disabled selected>--Pilih Jenis--</option>
									<option value="ALL">Semua Jenis</option>
									<option value="KV">Konversi</option>
									<option value="MB">Mahasiswa Baru</option>
								</select>
							</div>
						</div>
						<div class="control-group">
							<label class="control-label">Pilih Gelombang</label>
							<div class="controls">
								<select class="form-control span4" name="gel" required/>
									<option disabled selected>--Pilih Gelombang--</option>
									<option value="0">Semua Gelombang</option>
									<?php
									foreach($gelombang as $value){
										echo "<option value='".$value->gelombang."-".$value->tahun."'>".$value->gelombang."</option>";
									}
									?>
								</select>
							</div>
						</div>
						<div class="form-actions">
							<input class="btn btn-large btn-primary" type="submit" value="Submit">
							<input class="btn btn-large btn-default" type="reset" value="Clear">
						</div>
					</fieldset>
				</form>
			</div>
		</div>
	</div>
</div>

