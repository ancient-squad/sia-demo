<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
    <h4 class="modal-title">Form Edit Data</h4>
</div>
<form class ='form-horizontal' action="<?php echo base_url(); ?>pmb/gelombangpmb/edit_gel/<?php echo $pos->id_gel; ?>" method="post" enctype="multipart/form-data">
    <div class="modal-body" style="margin-left: -60px;">  
		<div class="control-group" id="">
            <label class="control-label">Gelombang</label>
            <div class="controls">
                <input type="text" class="span4" name="plak" placeholder="Input Gelombang" class="form-control" value="<?php echo $pos->gelombang; ?>" required/>
                <input type="hidden" name="id" value="<?php echo $pos->id_gel; ?>">
            </div>
        </div>
        <script>
            $(function() {
                $( "#ding" ).datepicker({
                    changeMonth: true,
                    numberOfMonths: 1
                });
                $( "#dong" ).datepicker({
                    changeMonth: true,
                    numberOfMonths: 1
                });
            });
        </script>				
        <div class="control-group" id="">
            <label class="control-label">Waktu Mulai</label>
            <div class="controls">
                <input type="text" class="span3" id="ding" name="plik" placeholder="Input Waktu Mulai" class="form-control" value="<?php echo $pos->str_date; ?>" required/>
            </div>
        </div>
        <div class="control-group" id="">
            <label class="control-label">Waktu Selesai</label>
            <div class="controls">
                <input type="text" class="span3" id="dong" name="pluk" placeholder="Input Waktu Selesai" class="form-control" value="<?php echo $pos->end_date; ?>" required/>
            </div>
        </div>
    </div> 
    <div class="modal-footer">
        <input type="reset" class="btn btn-default" value="Clear"/>
        <input type="submit" class="btn btn-primary" value="Simpan"/>
    </div>
</form>