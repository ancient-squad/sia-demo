<?php
header("Content-Type: application/xls");    
header("Content-Disposition: attachment; filename=jumlah_maba.xls");  
header("Pragma: no-cache"); 
header("Expires: 0");
?>


<style>
table, td, th {
    border: 1px solid black;
}



th {
    background-color: blue;
    color: black;
}
</style>

<table border="3">
    <thead>
        <tr> 
            <th rowspan="2">Fakultas</th>
            <th colspan="3">Gelombang I</th>
            <th colspan="3">Gelombang II</th>
            <th colspan="3">Gelombang III</th>
            <th colspan="3">Gelombang IV</th>
            <th colspan="3">Gelombang Ekstra</th>
            <th colspan="3">Jumlah</th>
            <th rowspan="2">Jumlah Total</th>
        </tr>
       
        <tr>
            <th>Pagi</th>
            <th>Sore</th>
            <th>KK</th>
            <th>Pagi</th>
            <th>Sore</th>
            <th>KK</th>
            <th>Pagi</th>
            <th>Sore</th>
            <th>KK</th>
            <th>Pagi</th>
            <th>Sore</th>
            <th>KK</th>
            <th>Pagi</th>
            <th>Sore</th>
            <th>KK</th>
            <th>Pagi</th>
            <th>Sore</th>
            <th>KK</th>
        </tr>
    </thead>
    <tbody>
        
         <?php
            $t_pg1 = 0;
            $t_sr1 = 0;
            $t_ky1 = 0;

            $jml_by_prodi_all= 0;

        foreach ($fakultas as $isi) { ?>
            
        <tr>
            <td style="color:#000000; background-color:#ffff00"><b><?php echo $isi->fakultas; ?></b></td>
            <td colspan="19" style="background-color:#ffff00"></td>
        </tr>

        <?php $q = $this->db->query('select * from tbl_jurusan_prodi where kd_fakultas = '.$isi->kd_fakultas.'')->result(); ?>
        
            <?php 
           
            foreach ($q as $rows) { ?>
                <tr>
                    <td><b><?php echo $rows->prodi; ?></b></td>
                    <?php if ($ss == 'S1') { ?>
                        <?php if ($kampus == 'bks'){ ?>
                            <?php if ($gel == 'ALL') { $b = 0;
                                    for ($i=1; $i <= $jumlahgel; $i++) { 
                                        if ($jenis == 'ALL') {
                                            $pg[$b] = $this->db->query('SELECT count(nomor_registrasi) AS jml FROM '.$table.' WHERE kampus = "bks" AND prodi = "'.$rows->kd_prodi.'" AND kelas = "pg" AND gelombang = "'.$i.'"')->row();
                                            $sr[$b] = $this->db->query('SELECT count(nomor_registrasi) AS jml FROM '.$table.' WHERE kampus = "bks" AND prodi = "'.$rows->kd_prodi.'" AND kelas = "sr" AND gelombang = "'.$i.'"')->row();
                                            $ky[$b] = $this->db->query('SELECT count(nomor_registrasi) AS jml FROM '.$table.' WHERE kampus = "bks" AND prodi = "'.$rows->kd_prodi.'" AND kelas = "ky" AND gelombang = "'.$i.'"')->row();
                                        } else {
                                            $pg[$b] = $this->db->query('SELECT count(nomor_registrasi) AS jml FROM '.$table.' WHERE kampus = "bks" AND prodi = "'.$rows->kd_prodi.'" AND kelas = "pg" and jenis_pmb = "'.$jenis.'" AND gelombang = "'.$i.'"')->row();
                                            $sr[$b] = $this->db->query('SELECT count(nomor_registrasi) AS jml FROM '.$table.' WHERE kampus = "bks" AND prodi = "'.$rows->kd_prodi.'" AND kelas = "sr" and jenis_pmb = "'.$jenis.'" AND gelombang = "'.$i.'"')->row();
                                            $ky[$b] = $this->db->query('SELECT count(nomor_registrasi) AS jml FROM '.$table.' WHERE kampus = "bks" AND prodi = "'.$rows->kd_prodi.'" AND kelas = "ky" and jenis_pmb = "'.$jenis.'" AND gelombang = "'.$i.'"')->row();
                                        }
                                    $b++; }
                                
                            } else { ?>
                            
                                    <?php if ($jenis == 'ALL') { ?>
                                    <?php $pg1 = $this->db->query('SELECT count(nomor_registrasi) AS jml FROM '.$table.' WHERE kampus = "bks" AND prodi = '.$rows->kd_prodi.' AND kelas = "pg" AND gelombang = "'.$gel.'" ')->row();?>
                                    <?php $sr1 = $this->db->query('SELECT count(nomor_registrasi) AS jml FROM '.$table.' WHERE kampus = "bks" AND prodi = '.$rows->kd_prodi.' AND kelas = "sr" AND gelombang = "'.$gel.'" ')->row();?>
                                    <?php $ky1 = $this->db->query('SELECT count(nomor_registrasi) AS jml FROM '.$table.' WHERE kampus = "bks" AND prodi = '.$rows->kd_prodi.' AND kelas = "ky" AND gelombang = "'.$gel.'" ')->row();
                                } else { ?>
                                    <?php $pg1 = $this->db->query('SELECT count(nomor_registrasi) AS jml FROM '.$table.' WHERE kampus = "bks" AND prodi = '.$rows->kd_prodi.' AND kelas = "pg" AND gelombang = "'.$gel.'" and jenis_pmb = "'.$jenis.'" ')->row();?>
                                    <?php $sr1 = $this->db->query('SELECT count(nomor_registrasi) AS jml FROM '.$table.' WHERE kampus = "bks" AND prodi = '.$rows->kd_prodi.' AND kelas = "sr" AND gelombang = "'.$gel.'" and jenis_pmb = "'.$jenis.'" ')->row();?>
                                    <?php $ky1 = $this->db->query('SELECT count(nomor_registrasi) AS jml FROM '.$table.' WHERE kampus = "bks" AND prodi = '.$rows->kd_prodi.' AND kelas = "ky" AND gelombang = "'.$gel.'" and jenis_pmb = "'.$jenis.'" ')->row();
                                }
                            } 
                        
                        } elseif ($kampus == 'jkt') { ?>
                            <?php if ($gel == 'ALL') { $b = 0;
                                    for ($i=1; $i <= $jumlahgel; $i++) { 
                                        if ($jenis == 'ALL') {
                                            $pg[$b] = $this->db->query('SELECT count(nomor_registrasi) AS jml FROM '.$table.' WHERE kampus = "jkt" AND prodi = "'.$rows->kd_prodi.'" AND kelas = "pg" AND gelombang = "'.$i.'"')->row();
                                            $sr[$b] = $this->db->query('SELECT count(nomor_registrasi) AS jml FROM '.$table.' WHERE kampus = "jkt" AND prodi = "'.$rows->kd_prodi.'" AND kelas = "sr" AND gelombang = "'.$i.'"')->row();
                                            $ky[$b] = $this->db->query('SELECT count(nomor_registrasi) AS jml FROM '.$table.' WHERE kampus = "jkt" AND prodi = "'.$rows->kd_prodi.'" AND kelas = "ky" AND gelombang = "'.$i.'"')->row();
                                        } else {
                                            $pg[$b] = $this->db->query('SELECT count(nomor_registrasi) AS jml FROM '.$table.' WHERE kampus = "jkt" AND prodi = "'.$rows->kd_prodi.'" AND kelas = "pg" and jenis_pmb = "'.$jenis.'" AND gelombang = "'.$i.'"')->row();
                                            $sr[$b] = $this->db->query('SELECT count(nomor_registrasi) AS jml FROM '.$table.' WHERE kampus = "jkt" AND prodi = "'.$rows->kd_prodi.'" AND kelas = "sr" and jenis_pmb = "'.$jenis.'" AND gelombang = "'.$i.'"')->row();
                                            $ky[$b] = $this->db->query('SELECT count(nomor_registrasi) AS jml FROM '.$table.' WHERE kampus = "jkt" AND prodi = "'.$rows->kd_prodi.'" AND kelas = "ky" and jenis_pmb = "'.$jenis.'" AND gelombang = "'.$i.'"')->row();
                                        }
                                    $b++; }
                                
                            } else { ?>
                                <?php if ($jenis == 'ALL') { ?>
                                    <?php $pg1 = $this->db->query('SELECT count(nomor_registrasi) AS jml FROM '.$table.' WHERE kampus = "jkt" AND prodi = '.$rows->kd_prodi.' AND kelas = "pg" AND gelombang = "'.$gel.'" ')->row();?>
                                    <?php $sr1 = $this->db->query('SELECT count(nomor_registrasi) AS jml FROM '.$table.' WHERE kampus = "jkt" AND prodi = '.$rows->kd_prodi.' AND kelas = "sr" AND gelombang = "'.$gel.'" ')->row();?>
                                    <?php $ky1 = $this->db->query('SELECT count(nomor_registrasi) AS jml FROM '.$table.' WHERE kampus = "jkt" AND prodi = '.$rows->kd_prodi.' AND kelas = "ky" AND gelombang = "'.$gel.'" ')->row();
                                } else { ?>
                                    <?php $pg1 = $this->db->query('SELECT count(nomor_registrasi) AS jml FROM '.$table.' WHERE kampus = "jkt" AND prodi = '.$rows->kd_prodi.' AND kelas = "pg" AND gelombang = "'.$gel.'" and jenis_pmb = "'.$jenis.'" ')->row();?>
                                    <?php $sr1 = $this->db->query('SELECT count(nomor_registrasi) AS jml FROM '.$table.' WHERE kampus = "jkt" AND prodi = '.$rows->kd_prodi.' AND kelas = "sr" AND gelombang = "'.$gel.'" and jenis_pmb = "'.$jenis.'" ')->row();?>
                                    <?php $ky1 = $this->db->query('SELECT count(nomor_registrasi) AS jml FROM '.$table.' WHERE kampus = "jkt" AND prodi = '.$rows->kd_prodi.' AND kelas = "ky" AND gelombang = "'.$gel.'" and jenis_pmb = "'.$jenis.'" ')->row();
                                }
                                
                            } 
                            
                        } elseif ($kampus == 'ALL') { ?>
                            <?php if ($gel == 'ALL') { $b = 0;
                                    for ($i=1; $i <= $jumlahgel; $i++) { 
                                        if ($jenis == 'ALL') {
                                            $pg[$b] = $this->db->query('SELECT count(nomor_registrasi) AS jml FROM '.$table.' WHERE prodi = "'.$rows->kd_prodi.'" AND kelas = "pg" AND gelombang = "'.$i.'"')->row();
                                            $sr[$b] = $this->db->query('SELECT count(nomor_registrasi) AS jml FROM '.$table.' WHERE prodi = "'.$rows->kd_prodi.'" AND kelas = "sr" AND gelombang = "'.$i.'"')->row();
                                            $ky[$b] = $this->db->query('SELECT count(nomor_registrasi) AS jml FROM '.$table.' WHERE prodi = "'.$rows->kd_prodi.'" AND kelas = "ky" AND gelombang = "'.$i.'"')->row();
                                        } else {
                                            $pg[$b] = $this->db->query('SELECT count(nomor_registrasi) AS jml FROM '.$table.' WHERE prodi = "'.$rows->kd_prodi.'" AND kelas = "pg" and jenis_pmb = "'.$jenis.'" AND gelombang = "'.$i.'"')->row();
                                            $sr[$b] = $this->db->query('SELECT count(nomor_registrasi) AS jml FROM '.$table.' WHERE prodi = "'.$rows->kd_prodi.'" AND kelas = "sr" and jenis_pmb = "'.$jenis.'" AND gelombang = "'.$i.'"')->row();
                                            $ky[$b] = $this->db->query('SELECT count(nomor_registrasi) AS jml FROM '.$table.' WHERE prodi = "'.$rows->kd_prodi.'" AND kelas = "ky" and jenis_pmb = "'.$jenis.'" AND gelombang = "'.$i.'"')->row();
                                        }
                                    $b++; }
                                
                            } else { ?>
                                <?php if ($jenis == 'ALL') { ?>
                                    <?php $pg1 = $this->db->query('SELECT count(nomor_registrasi) AS jml FROM '.$table.' WHERE prodi = "'.$rows->kd_prodi.'" AND kelas = "pg" AND gelombang = "'.$gel.'" ')->row();?>
                                    <?php $sr1 = $this->db->query('SELECT count(nomor_registrasi) AS jml FROM '.$table.' WHERE prodi = "'.$rows->kd_prodi.'" AND kelas = "sr" AND gelombang = "'.$gel.'" ')->row();?>
                                    <?php $ky1 = $this->db->query('SELECT count(nomor_registrasi) AS jml FROM '.$table.' WHERE prodi = "'.$rows->kd_prodi.'" AND kelas = "ky" AND gelombang = "'.$gel.'" ')->row();
                                } else { ?>
                                    <?php $pg1 = $this->db->query('SELECT count(nomor_registrasi) AS jml FROM '.$table.' WHERE prodi = "'.$rows->kd_prodi.'" AND kelas = "pg" AND gelombang = "'.$gel.'" and jenis_pmb = "'.$jenis.'" ')->row();?>
                                    <?php $sr1 = $this->db->query('SELECT count(nomor_registrasi) AS jml FROM '.$table.' WHERE prodi = "'.$rows->kd_prodi.'" AND kelas = "sr" AND gelombang = "'.$gel.'" and jenis_pmb = "'.$jenis.'" ')->row();?>
                                    <?php $ky1 = $this->db->query('SELECT count(nomor_registrasi) AS jml FROM '.$table.' WHERE prodi = "'.$rows->kd_prodi.'" AND kelas = "ky" AND gelombang = "'.$gel.'" and jenis_pmb = "'.$jenis.'" ')->row();
                                }
                            }
                        } 
                        
                    } elseif($ss == 'S2') { ?>
                        <?php if ($kampus == 'bks'){ ?>
                            <?php if ($gel == 'ALL') { $b = 0;
                                    for ($i=1; $i <= $jumlahgel; $i++) { 
                                        if ($jenis == 'ALL') {
                                            $pg[$b] = $this->db->query('SELECT count(ID_registrasi) AS jml FROM '.$tables2.' WHERE kampus = "bks" AND opsi_prodi_s2 = '.$rows->kd_prodi.' AND kelas = "pg" AND gelombang = "'.$i.'"')->row();
                                            $sr[$b] = $this->db->query('SELECT count(ID_registrasi) AS jml FROM '.$tables2.' WHERE kampus = "bks" AND opsi_prodi_s2 = '.$rows->kd_prodi.' AND kelas = "sr" AND gelombang = "'.$i.'"')->row();
                                            $ky[$b] = $this->db->query('SELECT count(ID_registrasi) AS jml FROM '.$tables2.' WHERE kampus = "bks" AND opsi_prodi_s2 = '.$rows->kd_prodi.' AND kelas = "KR" AND gelombang = "'.$i.'"')->row();
                                        } else {
                                            $pg[$b] = $this->db->query('SELECT count(ID_registrasi) AS jml FROM '.$tables2.' WHERE kampus = "bks" AND opsi_prodi_s2 = '.$rows->kd_prodi.' AND kelas = "pg"and jenis_pmb = "'.$jenis.'" AND gelombang = "'.$i.'"')->row();
                                            $sr[$b] = $this->db->query('SELECT count(ID_registrasi) AS jml FROM '.$tables2.' WHERE kampus = "bks" AND opsi_prodi_s2 = '.$rows->kd_prodi.' AND kelas = "sr" and jenis_pmb = "'.$jenis.'" AND gelombang = "'.$i.'"')->row();
                                            $ky[$b] = $this->db->query('SELECT count(ID_registrasi) AS jml FROM '.$tables2.' WHERE kampus = "bks" AND opsi_prodi_s2 = '.$rows->kd_prodi.' AND kelas = "KR" and jenis_pmb = "'.$jenis.'" AND gelombang = "'.$i.'"')->row();
                                        }
                                    $b++; }
                                
                            } else { ?>
                            
                                <?php if ($jenis == 'ALL') { ?>
                                    <?php $pg1 = $this->db->query('SELECT count(ID_registrasi) AS jml FROM '.$tables2.' WHERE kampus = "bks" AND opsi_prodi_s2 = '.$rows->kd_prodi.' AND kelas = "pg" AND gelombang = "'.$gel.'" ')->row();?>
                                    <?php $sr1 = $this->db->query('SELECT count(ID_registrasi) AS jml FROM '.$tables2.' WHERE kampus = "bks" AND opsi_prodi_s2 = '.$rows->kd_prodi.' AND kelas = "sr" AND gelombang = "'.$gel.'" ')->row();?>
                                    <?php $ky1 = $this->db->query('SELECT count(ID_registrasi) AS jml FROM '.$tables2.' WHERE kampus = "bks" AND opsi_prodi_s2 = '.$rows->kd_prodi.' AND kelas = "KR" AND gelombang = "'.$gel.'" ')->row();
                                } else { ?>
                                    <?php $pg1 = $this->db->query('SELECT count(ID_registrasi) AS jml FROM '.$tables2.' WHERE kampus = "bks" AND opsi_prodi_s2 = '.$rows->kd_prodi.' AND kelas = "pg" AND gelombang = "'.$gel.'" and jenis_pmb = "'.$jenis.'" ')->row();?>
                                    <?php $sr1 = $this->db->query('SELECT count(ID_registrasi) AS jml FROM '.$tables2.' WHERE kampus = "bks" AND opsi_prodi_s2 = '.$rows->kd_prodi.' AND kelas = "sr" AND gelombang = "'.$gel.'" and jenis_pmb = "'.$jenis.'" ')->row();?>
                                    <?php $ky1 = $this->db->query('SELECT count(ID_registrasi) AS jml FROM '.$tables2.' WHERE kampus = "bks" AND opsi_prodi_s2 = '.$rows->kd_prodi.' AND kelas = "KR" AND gelombang = "'.$gel.'" and jenis_pmb = "'.$jenis.'" ')->row();
                                }
                            }
                        } elseif ($kampus == 'jkt') { ?>
                            <?php if ($gel == 'ALL') { $b = 0;
                                    for ($i=1; $i <= $jumlahgel; $i++) { 
                                        if ($jenis == 'ALL') {
                                            $pg[$b] = $this->db->query('SELECT count(ID_registrasi) AS jml FROM '.$tables2.' WHERE kampus = "jkt" AND opsi_prodi_s2 = '.$rows->kd_prodi.' AND kelas = "pg" AND gelombang = "'.$i.'"')->row();
                                            $sr[$b] = $this->db->query('SELECT count(ID_registrasi) AS jml FROM '.$tables2.' WHERE kampus = "jkt" AND opsi_prodi_s2 = '.$rows->kd_prodi.' AND kelas = "sr" AND gelombang = "'.$i.'"')->row();
                                            $ky[$b] = $this->db->query('SELECT count(ID_registrasi) AS jml FROM '.$tables2.' WHERE kampus = "jkt" AND opsi_prodi_s2 = '.$rows->kd_prodi.' AND kelas = "KR" AND gelombang = "'.$i.'"')->row();
                                        } else {
                                            $pg[$b] = $this->db->query('SELECT count(ID_registrasi) AS jml FROM '.$tables2.' WHERE kampus = "jkt" AND opsi_prodi_s2 = '.$rows->kd_prodi.' AND kelas = "pg"and jenis_pmb = "'.$jenis.'" AND gelombang = "'.$i.'"')->row();
                                            $sr[$b] = $this->db->query('SELECT count(ID_registrasi) AS jml FROM '.$tables2.' WHERE kampus = "jkt" AND opsi_prodi_s2 = '.$rows->kd_prodi.' AND kelas = "sr" and jenis_pmb = "'.$jenis.'" AND gelombang = "'.$i.'"')->row();
                                            $ky[$b] = $this->db->query('SELECT count(ID_registrasi) AS jml FROM '.$tables2.' WHERE kampus = "jkt" AND opsi_prodi_s2 = '.$rows->kd_prodi.' AND kelas = "KR" and jenis_pmb = "'.$jenis.'" AND gelombang = "'.$i.'"')->row();
                                        }
                                    $b++; }
                                
                            } else { ?>
                                <?php if ($jenis == 'ALL') { ?>
                                    <?php $pg1 = $this->db->query('SELECT count(ID_registrasi) AS jml FROM '.$tables2.' WHERE kampus = "jkt" AND opsi_prodi_s2 = '.$rows->kd_prodi.' AND kelas = "pg" AND gelombang = "'.$gel.'" ')->row();?>
                                    <?php $sr1 = $this->db->query('SELECT count(ID_registrasi) AS jml FROM '.$tables2.' WHERE kampus = "jkt" AND opsi_prodi_s2 = '.$rows->kd_prodi.' AND kelas = "sr" AND gelombang = "'.$gel.'" ')->row();?>
                                    <?php $ky1 = $this->db->query('SELECT count(ID_registrasi) AS jml FROM '.$tables2.' WHERE kampus = "jkt" AND opsi_prodi_s2 = '.$rows->kd_prodi.' AND kelas = "KR" AND gelombang = "'.$gel.'" ')->row();
                                } else { ?>
                                    <?php $pg1 = $this->db->query('SELECT count(ID_registrasi) AS jml FROM '.$tables2.' WHERE kampus = "jkt" AND opsi_prodi_s2 = '.$rows->kd_prodi.' AND kelas = "pg" AND gelombang = "'.$gel.'" and jenis_pmb = "'.$jenis.'" ')->row();?>
                                    <?php $sr1 = $this->db->query('SELECT count(ID_registrasi) AS jml FROM '.$tables2.' WHERE kampus = "jkt" AND opsi_prodi_s2 = '.$rows->kd_prodi.' AND kelas = "sr" AND gelombang = "'.$gel.'" and jenis_pmb = "'.$jenis.'" ')->row();?>
                                    <?php $ky1 = $this->db->query('SELECT count(ID_registrasi) AS jml FROM '.$tables2.' WHERE kampus = "jkt" AND opsi_prodi_s2 = '.$rows->kd_prodi.' AND kelas = "KR" AND gelombang = "'.$gel.'" and jenis_pmb = "'.$jenis.'" ')->row();
                                }
                                
                            }
                            
                        } elseif ($kampus == 'ALL') { ?>
                            <?php if ($gel == 'ALL') { $b = 0;
                                    for ($i=1; $i <= $jumlahgel; $i++) { 
                                        if ($jenis == 'ALL') {
                                            $pg[$b] = $this->db->query('SELECT count(ID_registrasi) AS jml FROM '.$tables2.' WHERE opsi_prodi_s2 = '.$rows->kd_prodi.' AND kelas = "pg" AND gelombang = "'.$i.'"')->row();
                                            $sr[$b] = $this->db->query('SELECT count(ID_registrasi) AS jml FROM '.$tables2.' WHERE opsi_prodi_s2 = '.$rows->kd_prodi.' AND kelas = "sr" AND gelombang = "'.$i.'"')->row();
                                            $ky[$b] = $this->db->query('SELECT count(ID_registrasi) AS jml FROM '.$tables2.' WHERE opsi_prodi_s2 = '.$rows->kd_prodi.' AND kelas = "KR" AND gelombang = "'.$i.'"')->row();
                                        } else {
                                            $pg[$b] = $this->db->query('SELECT count(ID_registrasi) AS jml FROM '.$tables2.' WHERE opsi_prodi_s2 = '.$rows->kd_prodi.' AND kelas = "pg"and jenis_pmb = "'.$jenis.'" AND gelombang = "'.$i.'"')->row();
                                            $sr[$b] = $this->db->query('SELECT count(ID_registrasi) AS jml FROM '.$tables2.' WHERE opsi_prodi_s2 = '.$rows->kd_prodi.' AND kelas = "sr" and jenis_pmb = "'.$jenis.'" AND gelombang = "'.$i.'"')->row();
                                            $ky[$b] = $this->db->query('SELECT count(ID_registrasi) AS jml FROM '.$tables2.' WHERE opsi_prodi_s2 = '.$rows->kd_prodi.' AND kelas = "KR" and jenis_pmb = "'.$jenis.'" AND gelombang = "'.$i.'"')->row();
                                        }
                                    $b++; }
                                
                            } else { ?>
                                <?php if ($jenis == 'ALL') { ?>
                                    <?php $pg1 = $this->db->query('SELECT count(ID_registrasi) AS jml FROM '.$tables2.' WHERE opsi_prodi_s2 = '.$rows->kd_prodi.' AND kelas = "pg" AND gelombang = "'.$gel.'" ')->row();?>
                                    <?php $sr1 = $this->db->query('SELECT count(ID_registrasi) AS jml FROM '.$tables2.' WHERE opsi_prodi_s2 = '.$rows->kd_prodi.' AND kelas = "sr" AND gelombang = "'.$gel.'" ')->row();?>
                                    <?php $ky1 = $this->db->query('SELECT count(ID_registrasi) AS jml FROM '.$tables2.' WHERE opsi_prodi_s2 = '.$rows->kd_prodi.' AND kelas = "KR" AND gelombang = "'.$gel.'" ')->row();
                                } else { ?>
                                    <?php $pg1 = $this->db->query('SELECT count(ID_registrasi) AS jml FROM '.$tables2.' WHERE opsi_prodi_s2 = '.$rows->kd_prodi.' AND kelas = "pg" AND gelombang = "'.$gel.'" and jenis_pmb = "'.$jenis.'"')->row();?>
                                    <?php $sr1 = $this->db->query('SELECT count(ID_registrasi) AS jml FROM '.$tables2.' WHERE opsi_prodi_s2 = '.$rows->kd_prodi.' AND kelas = "sr" AND gelombang = "'.$gel.'" and jenis_pmb = "'.$jenis.'"')->row();?>
                                    <?php $ky1 = $this->db->query('SELECT count(ID_registrasi) AS jml FROM '.$tables2.' WHERE opsi_prodi_s2 = '.$rows->kd_prodi.' AND kelas = "KR" AND gelombang = "'.$gel.'" and jenis_pmb = "'.$jenis.'"')->row();
                                }
                            }
                        }

                    } elseif($ss == 'ALL') { ?>
                            <?php if ($isi->kd_fakultas == 6){ ?>
                                <?php if ($kampus == 'bks'){ ?>
                                    <?php if ($gel == 'ALL') { $b = 0;
                                            for ($i=1; $i <= $jumlahgel; $i++) { 
                                                if ($jenis == 'ALL') {
                                                    $pg[$b] = $this->db->query('SELECT count(ID_registrasi) AS jml FROM '.$tables2.' WHERE kampus = "bks" AND opsi_prodi_s2 = '.$rows->kd_prodi.' AND kelas = "pg" AND gelombang = "'.$i.'"')->row();
                                                    $sr[$b] = $this->db->query('SELECT count(ID_registrasi) AS jml FROM '.$tables2.' WHERE kampus = "bks" AND opsi_prodi_s2 = '.$rows->kd_prodi.' AND kelas = "sr" AND gelombang = "'.$i.'"')->row();
                                                    $ky[$b] = $this->db->query('SELECT count(ID_registrasi) AS jml FROM '.$tables2.' WHERE kampus = "bks" AND opsi_prodi_s2 = '.$rows->kd_prodi.' AND kelas = "KR" AND gelombang = "'.$i.'"')->row();
                                                } else {
                                                    $pg[$b] = $this->db->query('SELECT count(ID_registrasi) AS jml FROM '.$tables2.' WHERE kampus = "bks" AND opsi_prodi_s2 = '.$rows->kd_prodi.' AND kelas = "pg"and jenis_pmb = "'.$jenis.'" AND gelombang = "'.$i.'"')->row();
                                                    $sr[$b] = $this->db->query('SELECT count(ID_registrasi) AS jml FROM '.$tables2.' WHERE kampus = "bks" AND opsi_prodi_s2 = '.$rows->kd_prodi.' AND kelas = "sr" and jenis_pmb = "'.$jenis.'" AND gelombang = "'.$i.'"')->row();
                                                    $ky[$b] = $this->db->query('SELECT count(ID_registrasi) AS jml FROM '.$tables2.' WHERE kampus = "bks" AND opsi_prodi_s2 = '.$rows->kd_prodi.' AND kelas = "KR" and jenis_pmb = "'.$jenis.'" AND gelombang = "'.$i.'"')->row();
                                                }
                                            $b++; }
                                        
                                    } else { ?>
                            
                                <?php if ($jenis == 'ALL') { ?>
                                    <?php $pg1 = $this->db->query('SELECT count(ID_registrasi) AS jml FROM '.$tables2.' WHERE kampus = "bks" AND opsi_prodi_s2 = '.$rows->kd_prodi.' AND kelas = "pg" AND gelombang = "'.$gel.'" ')->row();?>
                                    <?php $sr1 = $this->db->query('SELECT count(ID_registrasi) AS jml FROM '.$tables2.' WHERE kampus = "bks" AND opsi_prodi_s2 = '.$rows->kd_prodi.' AND kelas = "sr" AND gelombang = "'.$gel.'" ')->row();?>
                                    <?php $ky1 = $this->db->query('SELECT count(ID_registrasi) AS jml FROM '.$tables2.' WHERE kampus = "bks" AND opsi_prodi_s2 = '.$rows->kd_prodi.' AND kelas = "KR" AND gelombang = "'.$gel.'" ')->row();
                                } else { ?>
                                    <?php $pg1 = $this->db->query('SELECT count(ID_registrasi) AS jml FROM '.$tables2.' WHERE kampus = "bks" AND opsi_prodi_s2 = '.$rows->kd_prodi.' AND kelas = "pg" AND gelombang = "'.$gel.'" and jenis_pmb = "'.$jenis.'" ')->row();?>
                                    <?php $sr1 = $this->db->query('SELECT count(ID_registrasi) AS jml FROM '.$tables2.' WHERE kampus = "bks" AND opsi_prodi_s2 = '.$rows->kd_prodi.' AND kelas = "sr" AND gelombang = "'.$gel.'" and jenis_pmb = "'.$jenis.'" ')->row();?>
                                    <?php $ky1 = $this->db->query('SELECT count(ID_registrasi) AS jml FROM '.$tables2.' WHERE kampus = "bks" AND opsi_prodi_s2 = '.$rows->kd_prodi.' AND kelas = "KR" AND gelombang = "'.$gel.'" and jenis_pmb = "'.$jenis.'" ')->row();
                                }
                            }
                        } elseif ($kampus == 'jkt') { ?>
                            <?php if ($gel == 'ALL') { $b = 0;
                                    for ($i=1; $i <= $jumlahgel; $i++) { 
                                        if ($jenis == 'ALL') {
                                            $pg[$b] = $this->db->query('SELECT count(ID_registrasi) AS jml FROM '.$tables2.' WHERE kampus = "jkt" AND opsi_prodi_s2 = '.$rows->kd_prodi.' AND kelas = "pg" AND gelombang = "'.$i.'"')->row();
                                            $sr[$b] = $this->db->query('SELECT count(ID_registrasi) AS jml FROM '.$tables2.' WHERE kampus = "jkt" AND opsi_prodi_s2 = '.$rows->kd_prodi.' AND kelas = "sr" AND gelombang = "'.$i.'"')->row();
                                            $ky[$b] = $this->db->query('SELECT count(ID_registrasi) AS jml FROM '.$tables2.' WHERE kampus = "jkt" AND opsi_prodi_s2 = '.$rows->kd_prodi.' AND kelas = "KR" AND gelombang = "'.$i.'"')->row();
                                        } else {
                                            $pg[$b] = $this->db->query('SELECT count(ID_registrasi) AS jml FROM '.$tables2.' WHERE kampus = "jkt" AND opsi_prodi_s2 = '.$rows->kd_prodi.' AND kelas = "pg"and jenis_pmb = "'.$jenis.'" AND gelombang = "'.$i.'"')->row();
                                            $sr[$b] = $this->db->query('SELECT count(ID_registrasi) AS jml FROM '.$tables2.' WHERE kampus = "jkt" AND opsi_prodi_s2 = '.$rows->kd_prodi.' AND kelas = "sr" and jenis_pmb = "'.$jenis.'" AND gelombang = "'.$i.'"')->row();
                                            $ky[$b] = $this->db->query('SELECT count(ID_registrasi) AS jml FROM '.$tables2.' WHERE kampus = "jkt" AND opsi_prodi_s2 = '.$rows->kd_prodi.' AND kelas = "KR" and jenis_pmb = "'.$jenis.'" AND gelombang = "'.$i.'"')->row();
                                        }
                                    $b++; }
                                
                            } else { ?>
                                <?php if ($jenis == 'ALL') { ?>
                                    <?php $pg1 = $this->db->query('SELECT count(ID_registrasi) AS jml FROM '.$tables2.' WHERE kampus = "jkt" AND opsi_prodi_s2 = '.$rows->kd_prodi.' AND kelas = "pg" AND gelombang = "'.$gel.'" ')->row();?>
                                    <?php $sr1 = $this->db->query('SELECT count(ID_registrasi) AS jml FROM '.$tables2.' WHERE kampus = "jkt" AND opsi_prodi_s2 = '.$rows->kd_prodi.' AND kelas = "sr" AND gelombang = "'.$gel.'" ')->row();?>
                                    <?php $ky1 = $this->db->query('SELECT count(ID_registrasi) AS jml FROM '.$tables2.' WHERE kampus = "jkt" AND opsi_prodi_s2 = '.$rows->kd_prodi.' AND kelas = "KR" AND gelombang = "'.$gel.'" ')->row();
                                } else { ?>
                                    <?php $pg1 = $this->db->query('SELECT count(ID_registrasi) AS jml FROM '.$tables2.' WHERE kampus = "jkt" AND opsi_prodi_s2 = '.$rows->kd_prodi.' AND kelas = "pg" AND gelombang = "'.$gel.'" and jenis_pmb = "'.$jenis.'" ')->row();?>
                                    <?php $sr1 = $this->db->query('SELECT count(ID_registrasi) AS jml FROM '.$tables2.' WHERE kampus = "jkt" AND opsi_prodi_s2 = '.$rows->kd_prodi.' AND kelas = "sr" AND gelombang = "'.$gel.'" and jenis_pmb = "'.$jenis.'" ')->row();?>
                                    <?php $ky1 = $this->db->query('SELECT count(ID_registrasi) AS jml FROM '.$tables2.' WHERE kampus = "jkt" AND opsi_prodi_s2 = '.$rows->kd_prodi.' AND kelas = "KR" AND gelombang = "'.$gel.'" and jenis_pmb = "'.$jenis.'" ')->row();
                                }
                                
                            }
                            
                        } elseif ($kampus == 'ALL') { ?>
                            <?php if ($gel == 'ALL') { $b = 0;
                                    for ($i=1; $i <= $jumlahgel; $i++) { 
                                        if ($jenis == 'ALL') {
                                            $pg[$b] = $this->db->query('SELECT count(ID_registrasi) AS jml FROM '.$tables2.' WHERE opsi_prodi_s2 = '.$rows->kd_prodi.' AND kelas = "pg" AND gelombang = "'.$i.'"')->row();
                                            $sr[$b] = $this->db->query('SELECT count(ID_registrasi) AS jml FROM '.$tables2.' WHERE opsi_prodi_s2 = '.$rows->kd_prodi.' AND kelas = "sr" AND gelombang = "'.$i.'"')->row();
                                            $ky[$b] = $this->db->query('SELECT count(ID_registrasi) AS jml FROM '.$tables2.' WHERE opsi_prodi_s2 = '.$rows->kd_prodi.' AND kelas = "KR" AND gelombang = "'.$i.'"')->row();
                                        } else {
                                            $pg[$b] = $this->db->query('SELECT count(ID_registrasi) AS jml FROM '.$tables2.' WHERE opsi_prodi_s2 = '.$rows->kd_prodi.' AND kelas = "pg"and jenis_pmb = "'.$jenis.'" AND gelombang = "'.$i.'"')->row();
                                            $sr[$b] = $this->db->query('SELECT count(ID_registrasi) AS jml FROM '.$tables2.' WHERE opsi_prodi_s2 = '.$rows->kd_prodi.' AND kelas = "sr" and jenis_pmb = "'.$jenis.'" AND gelombang = "'.$i.'"')->row();
                                            $ky[$b] = $this->db->query('SELECT count(ID_registrasi) AS jml FROM '.$tables2.' WHERE opsi_prodi_s2 = '.$rows->kd_prodi.' AND kelas = "KR" and jenis_pmb = "'.$jenis.'" AND gelombang = "'.$i.'"')->row();
                                        }
                                    $b++; }
                                
                            } else { ?>
                                <?php if ($jenis == 'ALL') { ?>
                                    <?php $pg1 = $this->db->query('SELECT count(ID_registrasi) AS jml FROM '.$tables2.' WHERE opsi_prodi_s2 = '.$rows->kd_prodi.' AND kelas = "pg" AND gelombang = "'.$gel.'" ')->row();?>
                                    <?php $sr1 = $this->db->query('SELECT count(ID_registrasi) AS jml FROM '.$tables2.' WHERE opsi_prodi_s2 = '.$rows->kd_prodi.' AND kelas = "sr" AND gelombang = "'.$gel.'" ')->row();?>
                                    <?php $ky1 = $this->db->query('SELECT count(ID_registrasi) AS jml FROM '.$tables2.' WHERE opsi_prodi_s2 = '.$rows->kd_prodi.' AND kelas = "KR" AND gelombang = "'.$gel.'" ')->row();
                                } else { ?>
                                    <?php $pg1 = $this->db->query('SELECT count(ID_registrasi) AS jml FROM '.$tables2.' WHERE opsi_prodi_s2 = '.$rows->kd_prodi.' AND kelas = "pg" AND gelombang = "'.$gel.'" and jenis_pmb = "'.$jenis.'"')->row();?>
                                    <?php $sr1 = $this->db->query('SELECT count(ID_registrasi) AS jml FROM '.$tables2.' WHERE opsi_prodi_s2 = '.$rows->kd_prodi.' AND kelas = "sr" AND gelombang = "'.$gel.'" and jenis_pmb = "'.$jenis.'"')->row();?>
                                    <?php $ky1 = $this->db->query('SELECT count(ID_registrasi) AS jml FROM '.$tables2.' WHERE opsi_prodi_s2 = '.$rows->kd_prodi.' AND kelas = "KR" AND gelombang = "'.$gel.'" and jenis_pmb = "'.$jenis.'"')->row();
                                }
                            }
                        }
                                
                            } else { ?>
                                <?php if ($kampus == 'bks'){ ?>
                            <?php if ($gel == 'ALL') { $b = 0;
                                    for ($i=1; $i <= $jumlahgel; $i++) { 
                                        if ($jenis == 'ALL') {
                                            $pg[$b] = $this->db->query('SELECT count(nomor_registrasi) AS jml FROM '.$table.' WHERE kampus = "bks" AND prodi = "'.$rows->kd_prodi.'" AND kelas = "pg" AND gelombang = "'.$i.'"')->row();
                                            $sr[$b] = $this->db->query('SELECT count(nomor_registrasi) AS jml FROM '.$table.' WHERE kampus = "bks" AND prodi = "'.$rows->kd_prodi.'" AND kelas = "sr" AND gelombang = "'.$i.'"')->row();
                                            $ky[$b] = $this->db->query('SELECT count(nomor_registrasi) AS jml FROM '.$table.' WHERE kampus = "bks" AND prodi = "'.$rows->kd_prodi.'" AND kelas = "ky" AND gelombang = "'.$i.'"')->row();
                                        } else {
                                            $pg[$b] = $this->db->query('SELECT count(nomor_registrasi) AS jml FROM '.$table.' WHERE kampus = "bks" AND prodi = "'.$rows->kd_prodi.'" AND kelas = "pg" and jenis_pmb = "'.$jenis.'" AND gelombang = "'.$i.'"')->row();
                                            $sr[$b] = $this->db->query('SELECT count(nomor_registrasi) AS jml FROM '.$table.' WHERE kampus = "bks" AND prodi = "'.$rows->kd_prodi.'" AND kelas = "sr" and jenis_pmb = "'.$jenis.'" AND gelombang = "'.$i.'"')->row();
                                            $ky[$b] = $this->db->query('SELECT count(nomor_registrasi) AS jml FROM '.$table.' WHERE kampus = "bks" AND prodi = "'.$rows->kd_prodi.'" AND kelas = "ky" and jenis_pmb = "'.$jenis.'" AND gelombang = "'.$i.'"')->row();
                                        }
                                    $b++; }
                                
                            } else { ?>
                            
                                    <?php if ($jenis == 'ALL') { ?>
                                    <?php $pg1 = $this->db->query('SELECT count(nomor_registrasi) AS jml FROM '.$table.' WHERE kampus = "bks" AND prodi = '.$rows->kd_prodi.' AND kelas = "pg" AND gelombang = "'.$gel.'" ')->row();?>
                                    <?php $sr1 = $this->db->query('SELECT count(nomor_registrasi) AS jml FROM '.$table.' WHERE kampus = "bks" AND prodi = '.$rows->kd_prodi.' AND kelas = "sr" AND gelombang = "'.$gel.'" ')->row();?>
                                    <?php $ky1 = $this->db->query('SELECT count(nomor_registrasi) AS jml FROM '.$table.' WHERE kampus = "bks" AND prodi = '.$rows->kd_prodi.' AND kelas = "ky" AND gelombang = "'.$gel.'" ')->row();
                                } else { ?>
                                    <?php $pg1 = $this->db->query('SELECT count(nomor_registrasi) AS jml FROM '.$table.' WHERE kampus = "bks" AND prodi = '.$rows->kd_prodi.' AND kelas = "pg" AND gelombang = "'.$gel.'" and jenis_pmb = "'.$jenis.'" ')->row();?>
                                    <?php $sr1 = $this->db->query('SELECT count(nomor_registrasi) AS jml FROM '.$table.' WHERE kampus = "bks" AND prodi = '.$rows->kd_prodi.' AND kelas = "sr" AND gelombang = "'.$gel.'" and jenis_pmb = "'.$jenis.'" ')->row();?>
                                    <?php $ky1 = $this->db->query('SELECT count(nomor_registrasi) AS jml FROM '.$table.' WHERE kampus = "bks" AND prodi = '.$rows->kd_prodi.' AND kelas = "ky" AND gelombang = "'.$gel.'" and jenis_pmb = "'.$jenis.'" ')->row();
                                }
                            } 
                        
                        } elseif ($kampus == 'jkt') { ?>
                            <?php if ($gel == 'ALL') { $b = 0;
                                    for ($i=1; $i <= $jumlahgel; $i++) { 
                                        if ($jenis == 'ALL') {
                                            $pg[$b] = $this->db->query('SELECT count(nomor_registrasi) AS jml FROM '.$table.' WHERE kampus = "jkt" AND prodi = "'.$rows->kd_prodi.'" AND kelas = "pg" AND gelombang = "'.$i.'"')->row();
                                            $sr[$b] = $this->db->query('SELECT count(nomor_registrasi) AS jml FROM '.$table.' WHERE kampus = "jkt" AND prodi = "'.$rows->kd_prodi.'" AND kelas = "sr" AND gelombang = "'.$i.'"')->row();
                                            $ky[$b] = $this->db->query('SELECT count(nomor_registrasi) AS jml FROM '.$table.' WHERE kampus = "jkt" AND prodi = "'.$rows->kd_prodi.'" AND kelas = "ky" AND gelombang = "'.$i.'"')->row();
                                        } else {
                                            $pg[$b] = $this->db->query('SELECT count(nomor_registrasi) AS jml FROM '.$table.' WHERE kampus = "jkt" AND prodi = "'.$rows->kd_prodi.'" AND kelas = "pg" and jenis_pmb = "'.$jenis.'" AND gelombang = "'.$i.'"')->row();
                                            $sr[$b] = $this->db->query('SELECT count(nomor_registrasi) AS jml FROM '.$table.' WHERE kampus = "jkt" AND prodi = "'.$rows->kd_prodi.'" AND kelas = "sr" and jenis_pmb = "'.$jenis.'" AND gelombang = "'.$i.'"')->row();
                                            $ky[$b] = $this->db->query('SELECT count(nomor_registrasi) AS jml FROM '.$table.' WHERE kampus = "jkt" AND prodi = "'.$rows->kd_prodi.'" AND kelas = "ky" and jenis_pmb = "'.$jenis.'" AND gelombang = "'.$i.'"')->row();
                                        }
                                    $b++; }
                                
                            } else { ?>
                                <?php if ($jenis == 'ALL') { ?>
                                    <?php $pg1 = $this->db->query('SELECT count(nomor_registrasi) AS jml FROM '.$table.' WHERE kampus = "jkt" AND prodi = '.$rows->kd_prodi.' AND kelas = "pg" AND gelombang = "'.$gel.'" ')->row();?>
                                    <?php $sr1 = $this->db->query('SELECT count(nomor_registrasi) AS jml FROM '.$table.' WHERE kampus = "jkt" AND prodi = '.$rows->kd_prodi.' AND kelas = "sr" AND gelombang = "'.$gel.'" ')->row();?>
                                    <?php $ky1 = $this->db->query('SELECT count(nomor_registrasi) AS jml FROM '.$table.' WHERE kampus = "jkt" AND prodi = '.$rows->kd_prodi.' AND kelas = "ky" AND gelombang = "'.$gel.'" ')->row();
                                } else { ?>
                                    <?php $pg1 = $this->db->query('SELECT count(nomor_registrasi) AS jml FROM '.$table.' WHERE kampus = "jkt" AND prodi = '.$rows->kd_prodi.' AND kelas = "pg" AND gelombang = "'.$gel.'" and jenis_pmb = "'.$jenis.'" ')->row();?>
                                    <?php $sr1 = $this->db->query('SELECT count(nomor_registrasi) AS jml FROM '.$table.' WHERE kampus = "jkt" AND prodi = '.$rows->kd_prodi.' AND kelas = "sr" AND gelombang = "'.$gel.'" and jenis_pmb = "'.$jenis.'" ')->row();?>
                                    <?php $ky1 = $this->db->query('SELECT count(nomor_registrasi) AS jml FROM '.$table.' WHERE kampus = "jkt" AND prodi = '.$rows->kd_prodi.' AND kelas = "ky" AND gelombang = "'.$gel.'" and jenis_pmb = "'.$jenis.'" ')->row();
                                }
                                
                            } 
                            
                        } elseif ($kampus == 'ALL') { ?>
                            <?php if ($gel == 'ALL') { $b = 0;
                                    for ($i=1; $i <= $jumlahgel; $i++) { 
                                        if ($jenis == 'ALL') {
                                            $pg[$b] = $this->db->query('SELECT count(nomor_registrasi) AS jml FROM '.$table.' WHERE prodi = "'.$rows->kd_prodi.'" AND kelas = "pg" AND gelombang = "'.$i.'"')->row();
                                            $sr[$b] = $this->db->query('SELECT count(nomor_registrasi) AS jml FROM '.$table.' WHERE prodi = "'.$rows->kd_prodi.'" AND kelas = "sr" AND gelombang = "'.$i.'"')->row();
                                            $ky[$b] = $this->db->query('SELECT count(nomor_registrasi) AS jml FROM '.$table.' WHERE prodi = "'.$rows->kd_prodi.'" AND kelas = "ky" AND gelombang = "'.$i.'"')->row();
                                        } else {
                                            $pg[$b] = $this->db->query('SELECT count(nomor_registrasi) AS jml FROM '.$table.' WHERE prodi = "'.$rows->kd_prodi.'" AND kelas = "pg" and jenis_pmb = "'.$jenis.'" AND gelombang = "'.$i.'"')->row();
                                            $sr[$b] = $this->db->query('SELECT count(nomor_registrasi) AS jml FROM '.$table.' WHERE prodi = "'.$rows->kd_prodi.'" AND kelas = "sr" and jenis_pmb = "'.$jenis.'" AND gelombang = "'.$i.'"')->row();
                                            $ky[$b] = $this->db->query('SELECT count(nomor_registrasi) AS jml FROM '.$table.' WHERE prodi = "'.$rows->kd_prodi.'" AND kelas = "ky" and jenis_pmb = "'.$jenis.'" AND gelombang = "'.$i.'"')->row();
                                        }
                                    $b++; }
                                
                            } else { ?>
                                <?php if ($jenis == 'ALL') { ?>
                                    <?php $pg1 = $this->db->query('SELECT count(nomor_registrasi) AS jml FROM '.$table.' WHERE prodi = "'.$rows->kd_prodi.'" AND kelas = "pg" AND gelombang = "'.$gel.'" ')->row();?>
                                    <?php $sr1 = $this->db->query('SELECT count(nomor_registrasi) AS jml FROM '.$table.' WHERE prodi = "'.$rows->kd_prodi.'" AND kelas = "sr" AND gelombang = "'.$gel.'" ')->row();?>
                                    <?php $ky1 = $this->db->query('SELECT count(nomor_registrasi) AS jml FROM '.$table.' WHERE prodi = "'.$rows->kd_prodi.'" AND kelas = "ky" AND gelombang = "'.$gel.'" ')->row();
                                } else { ?>
                                    <?php $pg1 = $this->db->query('SELECT count(nomor_registrasi) AS jml FROM '.$table.' WHERE prodi = "'.$rows->kd_prodi.'" AND kelas = "pg" AND gelombang = "'.$gel.'" and jenis_pmb = "'.$jenis.'" ')->row();?>
                                    <?php $sr1 = $this->db->query('SELECT count(nomor_registrasi) AS jml FROM '.$table.' WHERE prodi = "'.$rows->kd_prodi.'" AND kelas = "sr" AND gelombang = "'.$gel.'" and jenis_pmb = "'.$jenis.'" ')->row();?>
                                    <?php $ky1 = $this->db->query('SELECT count(nomor_registrasi) AS jml FROM '.$table.' WHERE prodi = "'.$rows->kd_prodi.'" AND kelas = "ky" AND gelombang = "'.$gel.'" and jenis_pmb = "'.$jenis.'" ')->row();
                                }
                            }
                        } 
                            }
                    } ?>
                    

                        
                        
                        <?php if ($gel == '1') { ?>
                            <?php $jml_by_prodi = $pg1->jml + $sr1->jml + $ky1->jml; ?>
                            <?php $jml_by_prodi_all = $jml_by_prodi_all+$jml_by_prodi; ?>
                            <td><?php echo $pg1->jml; ?></td>
                            <td><?php echo $sr1->jml; ?></td>
                            <td><?php echo $ky1->jml; ?></td>
                            <td>-</td>
                            <td>-</td>
                            <td>-</td>
                            <td>-</td>
                            <td>-</td>
                            <td>-</td>
                            <td>-</td>
                            <td>-</td>
                            <td>-</td>
                            <td>-</td>
                            <td>-</td>
                            <td>-</td>
                            <td><?php echo $pg1->jml; $t_pg1=$t_pg1+$pg1->jml; ?></td>
                            <td><?php echo $sr1->jml; $t_sr1=$t_sr1+$sr1->jml; ?></td>
                            <td><?php echo $ky1->jml; $t_ky1=$t_ky1+$ky1->jml; ?></td>
                            <td><?php echo $jml_by_prodi; ?></td>
                        <?php } elseif($gel == '2') { ?>
                            <?php $jml_by_prodi = $pg1->jml + $sr1->jml + $ky1->jml; ?>
                            <?php $jml_by_prodi_all = $jml_by_prodi_all+$jml_by_prodi; ?>
                            <td>-</td>
                            <td>-</td>
                            <td>-</td>
                            <td><?php echo $pg1->jml; ?></td>
                            <td><?php echo $sr1->jml; ?></td>
                            <td><?php echo $ky1->jml; ?></td>
                            <td>-</td>
                            <td>-</td>
                            <td>-</td>
                            <td>-</td>
                            <td>-</td>
                            <td>-</td>
                            <td>-</td>
                            <td>-</td>
                            <td>-</td>
                            <td><?php echo $pg1->jml; $t_pg1=$t_pg1+$pg1->jml; ?></td>
                            <td><?php echo $sr1->jml; $t_sr1=$t_sr1+$sr1->jml; ?></td>
                            <td><?php echo $ky1->jml; $t_ky1=$t_ky1+$ky1->jml; ?></td>
                            <td><?php echo $jml_by_prodi; ?></td>
                        <?php } elseif($gel == '3') { ?>
                            <?php $jml_by_prodi = $pg1->jml + $sr1->jml + $ky1->jml; ?>
                            <?php $jml_by_prodi_all = $jml_by_prodi_all+$jml_by_prodi; ?>
                            <td>-</td>
                            <td>-</td>
                            <td>-</td>
                            <td>-</td>
                            <td>-</td>
                            <td>-</td>
                            <td><?php echo $pg1->jml;?></td>
                            <td><?php echo $sr1->jml;?></td>
                            <td><?php echo $ky1->jml;?></td>
                            <td>-</td>
                            <td>-</td>
                            <td>-</td>
                            <td>-</td>
                            <td>-</td>
                            <td>-</td>
                            <td><?php echo $pg1->jml; $t_pg1=$t_pg1+$pg1->jml; ?></td>
                            <td><?php echo $sr1->jml; $t_sr1=$t_sr1+$sr1->jml; ?></td>
                            <td><?php echo $ky1->jml; $t_ky1=$t_ky1+$ky1->jml; ?></td>
                            <td><?php echo $jml_by_prodi; ?></td>
                        <?php } elseif($gel == '4') { ?>
                            <?php $jml_by_prodi = $pg1->jml + $sr1->jml + $ky1->jml; ?>
                            <?php $jml_by_prodi_all = $jml_by_prodi_all+$jml_by_prodi; ?>
                            <td>-</td>
                            <td>-</td>
                            <td>-</td>
                            <td>-</td>
                            <td>-</td>
                            <td>-</td>
                            <td>-</td>
                            <td>-</td>
                            <td>-</td>
                            <td><?php echo $pg1->jml; ?></td>
                            <td><?php echo $sr1->jml; ?></td>
                            <td><?php echo $ky1->jml; ?></td>
                            <td>-</td>
                            <td>-</td>
                            <td>-</td>
                            <td><?php echo $pg1->jml; $t_pg1=$t_pg1+$pg1->jml; ?></td>
                            <td><?php echo $sr1->jml; $t_sr1=$t_sr1+$sr1->jml; ?></td>
                            <td><?php echo $ky1->jml; $t_ky1=$t_ky1+$ky1->jml; ?></td>
                            <td><?php echo $jml_by_prodi; ?></td>
                        <?php } elseif($gel == '5') { ?>
                            <?php $jml_by_prodi = $pg1->jml + $sr1->jml + $ky1->jml; ?>
                            <?php $jml_by_prodi_all = $jml_by_prodi_all+$jml_by_prodi; ?>
                            <td>-</td>
                            <td>-</td>
                            <td>-</td>
                            <td>-</td>
                            <td>-</td>
                            <td>-</td>
                            <td>-</td>
                            <td>-</td>
                            <td>-</td>
                            <td>-</td>
                            <td>-</td>
                            <td>-</td>
                            <td><?php echo $pg1->jml; ?></td>
                            <td><?php echo $sr1->jml; ?></td>
                            <td><?php echo $ky1->jml; ?></td>
                            <td><?php echo $pg1->jml; $t_pg1=$t_pg1+$pg1->jml; ?></td>
                            <td><?php echo $sr1->jml; $t_sr1=$t_sr1+$sr1->jml; ?></td>
                            <td><?php echo $ky1->jml; $t_ky1=$t_ky1+$ky1->jml; ?></td>
                            <td><?php echo $jml_by_prodi; ?></td>
                        <?php } elseif ($gel == 'ALL') { ?>
                            <?php $jml_by_prodi = 0; $z=0; $b = 0; $pgall = 0; $srall = 0; $kyall = 0; for ($i=1; $i <= $jumlahgel; $i++) { ?>
                                <td><?php echo $pg[$b]->jml; $t_pg[$b]=$t_pg[$b]+$pg[$b]->jml; ?></td>
                                <td><?php echo $sr[$b]->jml; $t_sr[$b]=$t_sr[$b]+$sr[$b]->jml; ?></td>
                                <td><?php echo $ky[$b]->jml; $t_ky[$b]=$t_ky[$b]+$ky[$b]->jml; ?></td>
                            <?php $z = $pg[$b]->jml + $sr[$b]->jml + $ky[$b]->jml; $jml_by_prodi = $jml_by_prodi+$z; $pgall = $pgall + $pg[$b]->jml; $srall = $srall + $sr[$b]->jml; $kyall = $kyall + $ky[$b]->jml; $b++; } ?>
                            <td>-</td>
                            <td>-</td>
                            <td>-</td>
                            <td>-</td>
                            <td>-</td>
                            <td>-</td>
                            <td>-</td>
                            <td>-</td>
                            <td>-</td>
                            <td><?php echo $pgall; ?></td>
                            <td><?php echo $srall; ?></td>
                            <td><?php echo $kyall; ?></td>
                            <td><?php echo $jml_by_prodi; ?></td>
                        <?php }?>
                    
                </tr>

               
            <?php } ?>
            
            
            
        <?php } ?> 
        <tr>
            <td><b>JUMLAH</b></td>
                <?php if ($gel == '1') { ?>
                    <td><?php echo $t_pg1; ?></td>
                    <td><?php echo $t_sr1; ?></td>
                    <td><?php echo $t_ky1; ?></td>
                    <td>-</td>
                    <td>-</td>
                    <td>-</td>
                    <td>-</td>
                    <td>-</td>
                    <td>-</td>
                    <td>-</td>
                    <td>-</td>
                    <td>-</td>
                    <td>-</td>
                    <td>-</td>
                    <td>-</td>
                    <td><?php echo $t_pg1; ?></td>
                    <td><?php echo $t_sr1; ?></td>
                    <td><?php echo $t_ky1; ?></td>
                    <td><?php echo $jml_by_prodi_all; ?></td>
                <?php } elseif($gel == '2') { ?>
                    <td>-</td>
                    <td>-</td>
                    <td>-</td>
                    <td><?php echo $t_pg1; ?></td>
                    <td><?php echo $t_sr1; ?></td>
                    <td><?php echo $t_ky1; ?></td>
                    <td>-</td>
                    <td>-</td>
                    <td>-</td>
                    <td>-</td>
                    <td>-</td>
                    <td>-</td>
                    <td>-</td>
                    <td>-</td>
                    <td>-</td>
                    <td><?php echo $t_pg1; ?></td>
                    <td><?php echo $t_sr1; ?></td>
                    <td><?php echo $t_ky1; ?></td>
                    <td><?php echo $jml_by_prodi_all; ?></td>
                <?php } elseif($gel == '3') { ?>
                    <td>-</td>
                    <td>-</td>
                    <td>-</td>
                    <td>-</td>
                    <td>-</td>
                    <td>-</td>
                    <td><?php echo $t_pg1; ?></td>
                    <td><?php echo $t_sr1; ?></td>
                    <td><?php echo $t_ky1; ?></td>
                    <td>-</td>
                    <td>-</td>
                    <td>-</td>
                    <td>-</td>
                    <td>-</td>
                    <td>-</td>
                    <td>-</td>
                    <td>-</td>
                    <td>-</td>
                    <td><?php echo $t_pg1; ?></td>
                    <td><?php echo $t_sr1; ?></td>
                    <td><?php echo $t_ky1; ?></td>
                    <td><?php echo $jml_by_prodi_all; ?></td>
                <?php } elseif($gel == '4') { ?>
                    <td>-</td>
                    <td>-</td>
                    <td>-</td>
                    <td>-</td>
                    <td>-</td>
                    <td>-</td>
                    <td>-</td>
                    <td>-</td>
                    <td>-</td>
                    <td><?php echo $t_pg1; ?></td>
                    <td><?php echo $t_sr1; ?></td>
                    <td><?php echo $t_ky1; ?></td>
                    <td>-</td>
                    <td>-</td>
                    <td>-</td>
                    <td><?php echo $t_pg1; ?></td>
                    <td><?php echo $t_sr1; ?></td>
                    <td><?php echo $t_ky1; ?></td>
                    <td><?php echo $jml_by_prodi_all; ?></td>
                <?php } elseif($gel == '5') { ?>
                    <td>-</td>
                    <td>-</td>
                    <td>-</td>
                    <td>-</td>
                    <td>-</td>
                    <td>-</td>
                    <td>-</td>
                    <td>-</td>
                    <td>-</td>
                    <td>-</td>
                    <td>-</td>
                    <td>-</td>
                    <td><?php echo $t_pg1; ?></td>
                    <td><?php echo $t_sr1; ?></td>
                    <td><?php echo $t_ky1; ?></td>
                    <td><?php echo $t_pg1; ?></td>
                    <td><?php echo $t_sr1; ?></td>
                    <td><?php echo $t_ky1; ?></td>
                    <td><?php echo $jml_by_prodi_all; ?></td>
                <?php } elseif($gel == 'ALL') { ?>
                    <?php $jml_by_prodi = 0; $z=0; $b = 0; $pgall = 0; $srall = 0; $kyall = 0; for ($i=1; $i <= $jumlahgel; $i++) { ?>
                        <td><?php echo $t_pg[$b]; ?></td>
                        <td><?php echo $t_sr[$b]; ?></td>
                        <td><?php echo $t_ky[$b]; ?></td>
                    <?php $z = $t_pg[$b] + $t_sr[$b] + $t_ky[$b]; $jml_by_prodi = $jml_by_prodi+$z; $pgall = $pgall + $t_pg[$b]; $srall = $srall + $t_sr[$b]; $kyall = $kyall + $t_ky[$b]; $b++; } ?>
                    <td>-</td>
                    <td>-</td>
                    <td>-</td>
                    <td>-</td>
                    <td>-</td>
                    <td>-</td>
                    <td>-</td>
                    <td>-</td>
                    <td>-</td>
                    <td><?php echo $pgall; ?></td>
                    <td><?php echo $srall; ?></td>
                    <td><?php echo $kyall; ?></td>
                    <td><?php echo $jml_by_prodi; ?></td>
                <?php } ?>
                        
        </tr> 
    </tbody>
</table>