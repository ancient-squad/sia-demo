<script type="text/javascript">
	$(function () {
	    $('#maba').highcharts({
	        chart: {
	            type: 'column'
	        },
	        title: {
	        	<?php  
	        				$nm = $this->app_model->getdetail('tbl_jurusan_prodi','kd_prodi',$maba,'kd_prodi','asc')->row();
	        				$text = "'Prodi ".$nm->prodi."'"; ?>
	            text: <?php echo $text; ?> 
	        },
	        subtitle: {
	            text: 'Source: SIA <?= $this->ORG_NAME ?>'
	        },
	        xAxis: {
	            categories: [
	            <?php $tahun = date('Y'); for ($i=2016; $i <= $tahun; $i++) { 
	            	echo $i.",";
	            } ?>
	            ],
	            crosshair: true
	        },
	        yAxis: {
	            min: 0,
	            title: {
	                text: 'Total'
	            }
	        },
	        tooltip: {
	            headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
	            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
	                '<td style="padding:0"><b>{point.y:f}</b></td></tr>',
	            footerFormat: '</table>',
	            shared: true,
	            useHTML: true
	        },
	        plotOptions: {
	            column: {
	                pointPadding: 0.2,
	                borderWidth: 0
	            }
	        },
	        series: [{
		        name: 'Total pendaftar',
		        data: [
		        	<?php $tahun = date('Y'); for ($i=2016; $i <= $tahun; $i++) { 
			        	$daftar = $this->ig_model->getAmountRegProdi($i,$maba); echo $daftar.","; 
			        	}
		        	?>
		         ]
		    }, {
		        name: 'Peserta tes',
		        data: [
		        	<?php $tahun = date('Y'); for ($i=2016; $i <= $tahun; $i++) { 
			        		$daftar = $this->ig_model->pesertates($i,$maba); echo $daftar.","; 
			        	}
		        	?>
		         ]
		    }, {
		        name: 'Lulus Tes',
		        data: [
		        	<?php $tahun = date('Y'); for ($i=2016; $i <= $tahun; $i++) { 
			        	$daftar = $this->ig_model->getLulusTes($i,$maba); echo $daftar.","; 
			        	}
		        	?>
		         ]
		    }, {
		        name: 'Daftar ulang',
		        data: [
		        	<?php $tahun = date('Y'); for ($i=2016; $i <= $tahun; $i++) { 
			        	$daftar = $this->ig_model->getReRegist($i,$maba); echo $daftar.","; 
			        	}
		        	?>
		         ]
		    }],
	    });
	});
</script>


<script src="<?php echo base_url(); ?>assets/hc/js/highcharts.js"></script>
<script src="<?php echo base_url(); ?>assets/hc/js/modules/exporting.js"></script>
<script src="<?php echo base_url(); ?>assets/hc/js/modules/data.js"></script>
<script src="<?php echo base_url(); ?>assets/hc/js/modules/drilldown.js"></script>

<div id='maba' style='min-width: 300px; height: 400px; max-width: 1000px; margin: 0 auto'></div>