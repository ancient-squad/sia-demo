<script type="text/javascript">
	$(function () {
		$('#ipk_on').highcharts({

		    title: {
		    	<?php $x = $this->db->query("SELECT prodi from tbl_jurusan_prodi where kd_prodi = '".$ipkon."'")->row()->prodi; ?>
		        text: 'Prodi <?php echo $x; ?>'
		    },

		    subtitle: {
		        text: 'Source: SIA <?= $this->ORG_NAME ?>'
		    },

		    yAxis: {
		        title: {
		            text: 'IPK'
		        }
		    },
		    legend: {
		        layout: 'vertical',
		        align: 'right',
		        verticalAlign: 'middle'
		    },

		   xAxis: {
	            categories: [
	                <?php 
	                	$tah = date('Y')-1;
	                	for ($i=2014; $i <= $tah; $i++) { 
	                		for ($n=1; $n < 3; $n++) { 
	                			// if ($i.$n == 20162) {
	                			// 	# code...
	                			// } else {
	                				echo $i.$n.',';
	                			// }
	                			
	                			
	                		}
	                	}
	                ?>
	            ],
	            crosshair: true
	        },

		    series: [{
		        name: 'IPK Rata-rata',
		        data: [
				        <?php 
					        $tah = date('Y')-1;
		                	for ($i=2014; $i <= $tah; $i++) { 
		                		for ($n=1; $n < 3; $n++) { 
		                			//echo $i.$n.',';
		                			// if ($i.$n == 20162) {
		                			// 	# code...
		                			// } else {
		                				$qx = $this->ig_model->gotipkon( $i.$n,$ipkon); 
		                             	echo number_format($qx,2).',';
		                			// }
		                			
		                			
		                		}
		                	}
		                ?>
                    ],
                dataLabels: {
		            enabled: true,
		            rotation: 0,
		            color: '#FFFFFF',
		            align: 'right',
		            format: '{point.y:.f}', // one decimal
		            y: 10, // 10 pixels down from the top
		            style: {
		                fontSize: '13px',
		                fontFamily: 'Verdana, sans-serif'
		            }
		        }
		    }]
		});
	});
</script>


<script src="<?php echo base_url(); ?>assets/hc/js/highcharts.js"></script>
<script src="<?php echo base_url(); ?>assets/hc/js/modules/exporting.js"></script>
<script src="<?php echo base_url(); ?>assets/hc/js/modules/data.js"></script>
<script src="<?php echo base_url(); ?>assets/hc/js/modules/drilldown.js"></script>

<div id='ipk_on' style='min-width: 300px; height: 400px; max-width: 1000px; margin: 0 auto'></div>