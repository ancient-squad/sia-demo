<?php $log = $this->session->userdata('sess_login'); ?>

<h1>Akreditasi <?= $this->ORG_NAME ?></h1><br>
<h3>668/SK/BAN-PT/Akred/PT/VII/2015</h3>
<hr>
<table class="table table-bordered table-striped">
	<thead>
        <tr> 
        	<th>No</th>
            <th>Prodi</th>
            <th>Fakultas</th>
            <th>Akreditasi</th>
            <th>No. SK</th>
            <th>Tahun SK</th>
            <th>Tanggal Kadaluwarsa</th>
            <th>Status</th>
        </tr>
    </thead>
    <tbody>
    	<?php $no = 1; foreach ($akre as $key) { 
    		if ($key->kd_prodi == $log['userid']) {
    			$warna = 'style="background:#FFB200;"';
    		} else {
    			$warna = '';
    		}
    		
    		?>
    		<tr <?php echo $warna; ?>>
	        	<td <?php echo $warna; ?>><?php echo $no; ?></td>
	        	<td <?php echo $warna; ?>><?php echo $key->prodi; ?></td>
	        	<td <?php echo $warna; ?>><?php echo $key->fakultas; ?></td>
	        	<td <?php echo $warna; ?>><?php echo $key->akreditasi; ?></td>
	        	<td <?php echo $warna; ?>><?php echo $key->sk; ?></td>
	        	<td <?php echo $warna; ?>><?php echo $key->tahun_akreditasi; ?></td>
	        	<td <?php echo $warna; ?>><?php echo $key->tgl_kadaluarsa; ?></td>
	        	<td <?php echo $warna; ?>>
	        		<?php switch ($key->status_prodi) {
	        			case '1':
	        				echo "Masih Berlaku";
	        				break;
	        			case '2':
	        				echo "Reakreditasi";
	        				break;
	        		} ?>
	        	</td>
	        </tr>
    	<?php $no++; } ?>
    </tbody>
</table>	