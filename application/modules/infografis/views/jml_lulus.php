<script type="text/javascript">
	$(function () {
	    $('#jml_lulus1').highcharts({
	    	colors: ['#7cb5ec', '#55BF3B'],
	        chart: {
	            type: 'column'
	        },
	         title: {
	        	<?php  
    				$nm = $this->app_model->getdetail('tbl_jurusan_prodi','kd_prodi',$lulus,'kd_prodi','asc')->row();
    				$text = "'Prodi ".$nm->prodi."'";
	        	?>
	            text: <?php echo $text; ?> 
	        },
	        subtitle: {
	            text: 'Source: SIA <?= $this->ORG_NAME ?>'
	        },
	        xAxis: {
	            categories: [
	                <?php 
	                	$lulusan = $this->ig_model->getlulusan1($lulus);
	                	foreach ($lulusan as $oz) {
	            			echo $oz->tahun.',';
	            		} ?>
	            ],
	            crosshair: true
	        },
	        yAxis: {
	            min: 0,
	            title: {
	                text: 'Total'
	            }
	        },
	        tooltip: {
	            headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
	            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
	                '<td style="padding:0"><b>{point.y:f}</b></td></tr>',
	            footerFormat: '</table>',
	            shared: true,
	            useHTML: true
	        },
	        plotOptions: {
	            column: {
	                pointPadding: 0.2,
	                borderWidth: 0
	            }
	        },
	        series: [{
	            name: 'Jumlah Lulusan',
	            data: [
	            	<?php 
	                	$lulusan = $this->ig_model->getlulusan1($lulus);
		            	foreach ($lulusan as $oz) {
		            		echo $oz->jml.',';
		            	} ?>
	            ],
	            dataLabels: {
		            enabled: true,
		            rotation: 0,
		            color: '#FFFFFF',
		            align: 'right',
		            format: '{point.y:.f}', // one decimal
		            y: 10, // 10 pixels down from the top
		            style: {
		                fontSize: '13px',
		                fontFamily: 'Verdana, sans-serif'
		            }
		        }

	        },
	        {
	            name: 'Rata-Rata Masa Studi Lulusan',
	            data: [
	            	<?php 
	            		$masas = $this->ig_model->getmasalulusan1($lulus);
	                	foreach ($masas as $oz) {
	            			echo number_format($oz->rata,1).',';
	            	} ?>
	            ],
	            dataLabels: {
		            enabled: true,
		            rotation: 0,
		            color: '#FFFFFF',
		            align: 'right',
		            format: '{point.y:.f}', // one decimal
		            y: 10, // 10 pixels down from the top
		            style: {
		                fontSize: '13px',
		                fontFamily: 'Verdana, sans-serif'
		            }
		        }

	        }
	        ]
	    });
	});
</script>

<script src="<?php echo base_url(); ?>assets/hc/js/highcharts.js"></script>
<script src="<?php echo base_url(); ?>assets/hc/js/modules/exporting.js"></script>
<script src="<?php echo base_url(); ?>assets/hc/js/modules/data.js"></script>
<script src="<?php echo base_url(); ?>assets/hc/js/modules/drilldown.js"></script>

<div id="jml_lulus1" style="min-width: 310px; height: 400px; max-width: 600px; margin: 0 auto"></div>