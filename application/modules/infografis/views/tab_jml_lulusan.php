<?php $log = $this->session->userdata('sess_login'); ?>

<!-- jumlah lulusan -->
<script type="text/javascript">
	$(function () {
	    $('#jml_lulus').highcharts({
	    	colors: ['#7cb5ec', '#55BF3B'],
	        chart: {
	            type: 'column'
	        },
	         title: {
	        	text: <?= $textjmlulus; ?> 
	        },
	        subtitle: {
	            text: 'Source: SIA <?= $this->ORG_NAME ?>'
	        },
	        xAxis: {
	            categories: [
	                <?php 
	                	foreach ($lulusan as $oz) {
	            			echo $oz->tahun.',';
	            		} ?>
	            ],
	            crosshair: true
	        },
	        yAxis: {
	            min: 0,
	            title: {
	                text: 'Total'
	            }
	        },
	        tooltip: {
	            headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
	            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
	                '<td style="padding:0"><b>{point.y:f}</b></td></tr>',
	            footerFormat: '</table>',
	            shared: true,
	            useHTML: true
	        },
	        plotOptions: {
	            column: {
	                pointPadding: 0.2,
	                borderWidth: 0
	            }
	        },
	        series: [{
	            name: 'Jumlah Lulusan',
	            data: [
	            	<?php foreach ($lulusan as $oz) {
		            		echo $oz->jml.',';
		            	} ?>
	            ],
	            dataLabels: {
		            enabled: true,
		            rotation: 0,
		            color: '#FFFFFF',
		            align: 'right',
		            format: '{point.y:.f}', // one decimal
		            y: 10, // 10 pixels down from the top
		            style: {
		                fontSize: '13px',
		                fontFamily: 'Verdana, sans-serif'
		            }
		        }

	        },
	        {
	            name: 'Rata-Rata Masa Studi Lulusan',
	            data: [
	            	<?php 
	            	foreach ($masas as $oz) {
	            		echo number_format($oz->rata,1).',';
	            	} ?>
	            ],
	            dataLabels: {
		            enabled: true,
		            rotation: 0,
		            color: '#FFFFFF',
		            align: 'right',
		            format: '{point.y:.f}', // one decimal
		            y: 10, // 10 pixels down from the top
		            style: {
		                fontSize: '13px',
		                fontFamily: 'Verdana, sans-serif'
		            }
		        }

	        }
	        ]
	    });
	});
</script>

<script type="text/javascript">
	$(function () {
	    $('#avg_lulus').highcharts({
	        chart: {
	            type: 'column'
	        },

	        title: {
	            text: <?= $textavglulus; ?> 
	        },

	        subtitle: {
	            text: 'Source: SIA <?= $this->ORG_NAME ?>'
	        },

	        xAxis: {
	            categories: [
	                <?php
	                	foreach ($data as $oz) {
	            			echo $oz->thn.',';
	            		}
	                ?>
	            ],
	            crosshair: true
	        },
	        yAxis: {
	            min: 0,
	            title: {
	                text: 'Total'
	            }
	        },
	        tooltip: {
	            headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
	            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
	                '<td style="padding:0"><b>{point.y:f}</b></td></tr>',
	            footerFormat: '</table>',
	            shared: true,
	            useHTML: true
	        },
	        plotOptions: {
	            column: {
	                pointPadding: 0.2,
	                borderWidth: 0
	            }
	        },
	        series: [{
	            name: 'Rata-Rata Masa Studi Lulusan',
	            data: [
	            	<?php
	                	foreach ($data as $oz) {
	            			echo number_format($oz->rata,1).',';
	            		}
	                ?>
	            ],
	            dataLabels: {
		            enabled: true,
		            rotation: 0,
		            color: '#FFFFFF',
		            align: 'right',
		            format: '{point.y:.f}', // one decimal
		            y: 10, // 10 pixels down from the top
		            style: {
		                fontSize: '13px',
		                fontFamily: 'Verdana, sans-serif'
		            }
		        }

	        }]
	    });
	});
</script>

<script src="<?php echo base_url(); ?>assets/hc/js/highcharts.js"></script>
<script src="<?php echo base_url(); ?>assets/hc/js/modules/exporting.js"></script>
<script src="<?php echo base_url(); ?>assets/hc/js/modules/data.js"></script>
<script src="<?php echo base_url(); ?>assets/hc/js/modules/drilldown.js"></script>

<div id="jml_lulus" style="min-width: 1000px; height: 400px; max-width: 1000px; margin: 0 auto"></div>
<?php 
	if ($log['id_user_group'] != 8 && $log['id_user_group'] != 9) { ?>
		<hr>
    	<center>
    		<h3>Jumlah Lulusan Mahasiswa Per Prodi</h3>
    		<small>*pilih prodi pada <i>select box</i> untuk menampilkan statistik</small>
    	</center>
    	<br>
    	<script>
            $(document).ready(function(){
                $('#fk2').change(function(){
                    $.post('<?php echo base_url()?>infografis/ig02/jmlulus/'+$(this).val(),{},function(get){
                        $('#showlulusan').html(get);
                    });
                });
            }); 
        </script>
    	<div class="pull-right">
            <div class="form-group" style="float:left;margin-right">
                <select class="form-control" name="prod" id="fk2">
                    <option disabled="" selected="">-- Pilih Prodi --</option>
                    <?php foreach ($pro as $key) { ?>
                    <option value="<?php echo $key->kd_prodi; ?>"><?php echo $key->prodi; ?></option>
                    <?php } ?>
                </select>
            </div>
        </div>
        <br>
        <div id="showlulusan" style="min-width: 310px; height: 400px; max-width: 600px; margin: 0 auto"></div>
<?php } ?>
<hr>
<div id="avg_lulus" style="min-width: 1000px; height: 400px; max-width: 1000px; margin: 0 auto"></div>	
<?php 
	if ($log['id_user_group'] != 8 && $log['id_user_group'] != 9) { ?>
		<hr>
    	<center>
    		<h3>Rata-rata Masa Studi Mahasiswa Per Prodi</h3>
    		<small>*pilih prodi pada <i>select box</i> untuk menampilkan statistik</small>
    	</center>
    	<br>
    	<script>
            $(document).ready(function(){
                $('#fk3').change(function(){
                    $.post('<?php echo base_url()?>infografis/ig02/masastd/'+$(this).val(),{},function(get){
                        $('#showavgstudi').html(get);
                    });
                });
            }); 
        </script>
    	<div class="pull-right">
            <div class="form-group" style="float:left;margin-right">
                <select class="form-control" name="prod" id="fk3">
                    <option disabled="" selected="">-- Pilih Prodi --</option>
                    <?php foreach ($pro as $key) { ?>
                    <option value="<?php echo $key->kd_prodi; ?>"><?php echo $key->prodi; ?></option>
                    <?php } ?>
                </select>
            </div>
        </div>
        <br>
        <div id="showavgstudi" style="min-width: 310px; height: 400px; max-width: 600px; margin: 0 auto"></div>
<?php } ?>