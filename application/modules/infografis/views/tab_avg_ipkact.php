<?php $log = $this->session->userdata('sess_login'); ?>

<!-- ipk mhs aktif -->
<script type="text/javascript">
	$(function () {
		$('#avgipkaktif').highcharts({

		    title: {
		        text: 'IPK Rata-rata Mahasiswa Aktif'
		    },

		    subtitle: {
		        text: 'Source: SIA <?= $this->ORG_NAME ?>'
		    },

		    yAxis: {
		        title: {
		            text: 'IPK'
		        }
		    },
		    legend: {
		        layout: 'vertical',
		        align: 'right',
		        verticalAlign: 'middle'
		    },

		   xAxis: {
	            categories: [
	                <?php 
	                	$tah = date('Y');
	                	for ($i=2014; $i <= $tah; $i++) { 
	                		for ($n=1; $n < 3; $n++) { 
	                			echo $i.$n.',';
	                		}
	                	}
	                ?>
	            ],
	            crosshair: true
	        },

		    series: [{
		        name: 'IPK Rata-rata',
		        data: [
				        <?= $actstudentipk; ?>
                    ],
                dataLabels: {
		            enabled: true,
		            rotation: 0,
		            color: '#FFFFFF',
		            align: 'right',
		            format: '{point.y:.f}', // one decimal
		            y: 10, // 10 pixels down from the top
		            style: {
		                fontSize: '13px',
		                fontFamily: 'Verdana, sans-serif'
		            }
		        }
		    }]
		});
	});
</script>

<script src="<?php echo base_url(); ?>assets/hc/js/highcharts.js"></script>
<script src="<?php echo base_url(); ?>assets/hc/js/modules/exporting.js"></script>
<script src="<?php echo base_url(); ?>assets/hc/js/modules/data.js"></script>
<script src="<?php echo base_url(); ?>assets/hc/js/modules/drilldown.js"></script>

<div id="avgipkaktif" style="min-width: 1000px; height: 400px; max-width: 1000px; margin: 0 auto"></div>
<?php 
	if ($log['id_user_group'] != 8 && $log['id_user_group'] != 9) { ?>
		<hr>
    	<center>
    		<h3>Rata-rata IPK Mahasiswa Aktif Per Prodi</h3>
    		<small>*pilih prodi pada <i>select box</i> untuk menampilkan statistik</small>
    	</center>
    	<br>
    	<script>
            $(document).ready(function(){
                $('#fk5').change(function(){
                    $.post('<?php echo base_url()?>infografis/ig02/ipkaktifprodi/'+$(this).val(),{},function(get){
                        $('#showavgon').html(get);
                    });
                });
            }); 
        </script>
    	<div class="pull-right">
            <div class="form-group" style="float:left;margin-right">
                <select class="form-control" name="prod" id="fk5">
                    <option disabled="" selected="">-- Pilih Prodi --</option>
                    <?php foreach ($pro as $key) { ?>
                    <option value="<?php echo $key->kd_prodi; ?>"><?php echo $key->prodi; ?></option>
                    <?php } ?>
                </select>
            </div>
        </div>
        <br>
        <div id="showavgon" style="min-width: 310px; height: 400px; max-width: 600px; margin: 0 auto"></div>
<?php } ?>	