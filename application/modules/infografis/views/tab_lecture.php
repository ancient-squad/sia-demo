<script type="text/javascript">
$(function () {
    $('#dosen').highcharts({
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false,
            type: 'pie'
        },
        title: {
            text: 'Status Dosen per 2017/2018'
        },
        tooltip: {
            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: true,
                    format: '<b>{point.name}</b>: {point.y:.f}',
                    style: {
                        color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                    }
                }
            }
        },
        series: [{
            name: 'Brands',
            colorByPoint: true,
            data: [
            <?= $dosen; ?>
            ]
        }]
    });
});
</script>

<script type="text/javascript">
	$(function () {
		var chart = $('#det_dosen').highcharts({
			colors: ['#7cb5ec', '#55BF3B'],
		    chart: {
		        type: 'column'
		    },

		    title: {
		        text: 'Detail Status Dosen Per Prodi'
		    },

		    subtitle: {
		        text: 'Tahun Akademik 2017/2018'
		    },

		    legend: {
		        align: 'right',
		        verticalAlign: 'middle',
		        layout: 'vertical'
		    },

		    xAxis: {
		        categories: [
		        	<?php 
	            	$dsn = $this->info_model->getdosenprodi();
	            	foreach ($dsn as $oz) {
	            		echo "'".$oz->prodi."',";
	            	} ?>
		        ],
		        labels: {
		            x: -10
		        }
		    },

		    yAxis: {
		        allowDecimals: false,
		        title: {
		            text: 'Amount'
		        }
		    },

		    series: [{
		        name: 'Jumlah Dosen Tetap',
		        data: [
		        <?= $lecture ?>
		        ],
		        dataLabels: {
		            enabled: true,
		            rotation: 0,
		            color: '#FFFFFF',
		            align: 'right',
		            format: '{point.y:.f}', // one decimal
		            y: 10, // 10 pixels down from the top
		            style: {
		                fontSize: '13px',
		                fontFamily: 'Verdana, sans-serif'
		            }
		        }
		    },
		    {
		        name: 'Jumlah Dosen NIDN',
		        data: [
		        	<?= $lecturenidn; ?>
		        ],
		        dataLabels: {
		            enabled: true,
		            rotation: 0,
		            color: '#FFFFFF',
		            align: 'right',
		            format: '{point.y:.f}', // one decimal
		            y: 10, // 10 pixels down from the top
		            style: {
		                fontSize: '13px',
		                fontFamily: 'Verdana, sans-serif'
		            }
		        }
		    }
		    ],
		});
	});
</script>

<script src="<?php echo base_url(); ?>assets/hc/js/highcharts.js"></script>
<script src="<?php echo base_url(); ?>assets/hc/js/modules/exporting.js"></script>
<script src="<?php echo base_url(); ?>assets/hc/js/modules/data.js"></script>
<script src="<?php echo base_url(); ?>assets/hc/js/modules/drilldown.js"></script>

<div id="dosen" style="min-width: 1000px; height: 400px; max-width: 1000px; margin: 0 auto"></div>
<hr>
<div id="det_dosen" style="min-width: 1000px; height: 400px; max-width: 1000px; margin: 0 auto"></div>