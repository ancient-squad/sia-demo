<?php
	$logged = $this->session->userdata('sess_login');
	$pecah = explode(',', $logged['id_user_group']);
	$jmlh = count($pecah);
	for ($i=0; $i < $jmlh; $i++) { 
		$grup[] = $pecah[$i];
	}
?>

<script type="text/javascript">
$(function () {
    $('#dosen').highcharts({
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false,
            type: 'pie'
        },
        title: {
            text: 'Status Dosen per 2016/2017'
        },
        tooltip: {
            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: true,
                    format: '<b>{point.name}</b>: {point.y:.f}',
                    style: {
                        color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                    }
                },
                enabled: false
            }
        },
        series: [{
            name: 'Brands',
            colorByPoint: true,
            data: [
            <?php foreach ($getdosen as $value) {
            	if ($value->tetap == 1) {
            		echo "{name: 'Dosen Tetap',";
            	} else {
            		echo "{name: 'Dosen Tidak Tetap',";
            	}
            	echo "y: ".$value->jml."},";
            } ?>
            ]
        }]
    });
});
</script>



<script src="<?php echo base_url(); ?>assets/hc/js/highcharts.js"></script>
<script src="<?php echo base_url(); ?>assets/hc/js/modules/exporting.js"></script>
<script src="<?php echo base_url(); ?>assets/hc/js/modules/data.js"></script>
<script src="<?php echo base_url(); ?>assets/hc/js/modules/drilldown.js"></script>

<div class="row">
	<div class="span12">      		  		
  		<div class="widget ">
  			<div class="widget-header">
  				<i class="icon-graph"></i>
  				<h3>INFOGRAFIS</h3>
			</div> <!-- /widget-header -->
			
			<div class="widget-content">
				<div class="span11">
					<ul class="nav nav-tabs">
					    <li class="active"><a data-toggle="tab" href="#home1">Data Mahasiswa Baru</a></li>
					    <li><a data-toggle="tab" href="#home6">Asal Sekolah</a></li>
					</ul>
					<div class="tab-content">
					    <div id="home1" class="tab-pane fade in active">
					    	<div id="dosen" style="min-width: 1000px; height: 400px; max-width: 1000px; margin: 0 auto"></div>
						</div>
						<div id="home6" class="tab-pane fade in">
							
						</div>
					</div>
			    </div>
			</div>

		</div>
	</div>
</div>