<script type="text/javascript">
	$(function () {
		var chart = $('#gaji').highcharts({

		    chart: {
		        type: 'column'
		    },

		    title: {
		    	<?php $nm = $this->app_model->getdetail('tbl_jurusan_prodi','kd_prodi',$gaji,'kd_prodi','asc')->row();
	        				$text = "'Prodi ".$nm->prodi."'"; ?>
		        text: <?php echo $text; ?>
		    },

		    subtitle: {
		        text: 'Source: SIA <?= $this->ORG_NAME ?>'
		    },

		    legend: {
		        align: 'right',
		        verticalAlign: 'middle',
		        layout: 'vertical'
		    },

		    xAxis: {
		        categories: [
	            <?php $tahun = date('Y'); for ($i=2016; $i <= $tahun; $i++) { 
	            	echo $i.",";
	            } ?>
	            ],
		        labels: {
		            x: -10
		        }
		    },

		    yAxis: {
		        allowDecimals: false,
		        title: {
		            text: 'Amount'
		        }
		    },

		    series: [
		    <?php for ($i=1; $i < 5; $i++) {
		    	switch ($i) {
		    		case '1':
		    			$hasilan = 'Rp 1,000,000 - 2,000,000';
		    			break;

		    		case '2':
		    			$hasilan = 'Rp 2,100,000 - 4,000,000';
		    			break;

		    		case '3':
		    			$hasilan = 'Rp 4,100,000 - 5,999,000';
		    			break;

		    		case '4':
		    			$hasilan = '>= Rp 6,000,000';
		    			break;
		    	}
		    	echo "{name: '".$hasilan."',";
		    	echo "data: [";
		    	$tahun = date('Y'); for ($a=2016; $a <= $tahun; $a++) {
		    		
		        		$daftar = $this->ig_model->getjumlahmabapenghasilanprodi($a,$i,$gaji); echo $daftar.","; 
		        	
		    	}
		    	echo "]},";
		    } ?>
		    ],
		});
	});
</script>


<script src="<?php echo base_url(); ?>assets/hc/js/highcharts.js"></script>
<script src="<?php echo base_url(); ?>assets/hc/js/modules/exporting.js"></script>
<script src="<?php echo base_url(); ?>assets/hc/js/modules/data.js"></script>
<script src="<?php echo base_url(); ?>assets/hc/js/modules/drilldown.js"></script>

<div id='gaji' style='min-width: 300px; height: 400px; max-width: 1000px; margin: 0 auto'></div>