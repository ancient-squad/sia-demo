<script type="text/javascript">
	$(function () {
	    $('#avg_lulus1').highcharts({
	        chart: {
	            type: 'column'
	        },
	         title: {
	        	<?php  
	        		$nm = $this->app_model->getdetail('tbl_jurusan_prodi','kd_prodi',$std,'kd_prodi','asc')->row();
	        		$text = "'Prodi ".$nm->prodi."'";?>
	            text: <?php echo $text; ?> 
	        },
	        subtitle: {
	            text: 'Source: SIA <?= $this->ORG_NAME ?>'
	        },
	        xAxis: {
	            categories: [
	                <?php 
	                	$lulusan = $this->ig_model->getmasalulusan1($std);
	                	foreach ($lulusan as $oz) {
	            			echo $oz->thn.',';
	            		} ?>
	            ],
	            crosshair: true
	        },
	        yAxis: {
	            min: 0,
	            title: {
	                text: 'Total'
	            }
	        },
	        tooltip: {
	            headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
	            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
	                '<td style="padding:0"><b>{point.y:f}</b></td></tr>',
	            footerFormat: '</table>',
	            shared: true,
	            useHTML: true
	        },
	        plotOptions: {
	            column: {
	                pointPadding: 0.2,
	                borderWidth: 0
	            }
	        },
	        series: [{
	            name: 'Rata-Rata Masa Studi Lulusan',
	            data: [
	            	<?php 
	            	$lulusan = $this->info_model->getmasalulusan($std);
	            	foreach ($lulusan as $oz) {
	            		echo number_format($oz->rata,1).',';
	            	} ?>
	            ],
	            dataLabels: {
		            enabled: true,
		            rotation: 0,
		            color: '#FFFFFF',
		            align: 'right',
		            format: '{point.y:.f}', // one decimal
		            y: 10, // 10 pixels down from the top
		            style: {
		                fontSize: '13px',
		                fontFamily: 'Verdana, sans-serif'
		            }
		        }

	        }]
	    });
	});
</script>

<script src="<?php echo base_url(); ?>assets/hc/js/highcharts.js"></script>
<script src="<?php echo base_url(); ?>assets/hc/js/modules/exporting.js"></script>
<script src="<?php echo base_url(); ?>assets/hc/js/modules/data.js"></script>
<script src="<?php echo base_url(); ?>assets/hc/js/modules/drilldown.js"></script>

<div id='avg_lulus1' style='min-width: 300px; height: 400px; max-width: 1000px; margin: 0 auto'></div>