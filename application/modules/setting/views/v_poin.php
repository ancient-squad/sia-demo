<script>
	/**
	 * Load edit modal
	 * @param int id_menu
	 */
	function loadEdit(id_menu) {
		$('#contentEdit').load('<?= base_url('setting/poin/loadEdit/') ?>' + id_menu);
	}
</script>

<div class="row">
	<div class="span12">      		  		
  		<div class="widget ">
  			<div class="widget-header">
                <i class="icon-gear"></i>
  				<h3>Kelola Mutu</h3>
			</div>
			<div class="widget-content">
				
				<fieldset>
			    	<button type="button" id="btnAdd" class="btn btn-info btn-md" data-toggle="modal" data-target="#myModal">
			    		<i class="icon icon-plus"></i> Tambah Data
			    	</button>
			    	<button type="button" class="btn btn-warning btn-md" data-toggle="modal" data-target="#infoModal">
			    		<i class="icon icon-info"></i> Informasi
			    	</button>
			    	<hr>
			    	<table class="table table-stripped" id="example1">
			    		<thead>
			    			<tr>
			    				<th>No</th>
			    				<th>Nilai Huruf</th>
			    				<th>Nilai Mutu</th>
			    				<th>Rentang Nilai</th>
			    				<th>Deskripsi</th>
			    				<th>Description (english)</th>
			    				<th style="text-align: center">Aksi</th>
			    			</tr>
			    		</thead>
			    		<tbody id="tblBody">
			    			<?php $no = 1; foreach ($poin as $val) { ?>
								<tr>
									<td><?= $no; ?></td>
									<td><?= $val->nilai_huruf ?></td>
									<td><?= $val->nilai_mutu ?></td>
									<td><?= $val->nilai_bawah.' - '.$val->nilai_atas ?></td>
									<td><?= $val->deskripsi ?></td>
									<td><?= $val->description ?></td>
									<td style="text-align: center">
										<button 
											data-toggle="modal" 
											data-target="#editModal" 
											class="btn btn-primary" 
											title="Edit data" 
											onclick="loadEdit(<?= $val->id ?>)">
											<i class="icon icon-pencil"></i>
										</button>
										<a 
											class="btn btn-danger" 
											onclick="return confirm('Data akan dihapus. Lanjutkan?')"
											href="<?= base_url('setting/poin/removeMutu/'.$val->id) ?>" 
											title="Hapus data">
											<i class="icon icon-remove"></i>
										</a>
									</td>
								</tr>
							<?php $no++; } ?>
			    		</tbody>
			    	</table>
				</fieldset>
			</div>
		</div>
	</div>
</div>

<style>
	.modal-body {
		overflow-y: none !important;
	}
</style>


<!-- modal add start -->
<div id="myModal" class="modal fade" role="dialog">
  	<div class="modal-dialog">

    	<!-- Modal content-->
    	<div class="modal-content">
	      	<div class="modal-header">
	        	<h4 class="modal-title">Tambah Data Mutu</h4>
	      	</div>
	      	<div class="modal-body">
	      		<div class="alert alert-danger">
					*Untuk <b>Bobot</b>, <b>Nilai bawah</b>, <b>Nilai atas</b> Gunakan titik sebagai pemisah. Misal: <b>4.00 (empat titik nol nol)</b>
	      		</div>
	    		<form class="form-horizontal" action="<?= base_url('setting/poin/saveMutu') ?>" method="post">
			    	<div class="control-group">
						<label class="control-label">Mutu</label>
						<div class="controls">
							<input type="text" class="span3" name="mutu" placeholder="(A, A-, B, ...)" required="">
						</div>
					</div>
					
					<div class="control-group">
						<label class="control-label">Bobot</label>
						<div class="controls">
							<input type="text" class="span3" id="bobot" name="bobot" placeholder="Bobot mutu (angka desimal)" required="">
						</div>
					</div>

					<div class="control-group">
						<label class="control-label">Nilai Bawah</label>
						<div class="controls">
							<input type="text" class="span3" id="bawah" name="bawah" placeholder="Batas bawah nilai" required="">
						</div>
					</div>
					
					<div class="control-group">
						<label class="control-label">Nilai Atas</label>
						<div class="controls">
							<input type="text" class="span3" id="atas" name="atas" placeholder="Batas atas nilai" required="">
						</div>
					</div>
					
					<div class="control-group">
						<label class="control-label">Deskripsi</label>
						<div class="controls">
							<textarea name="deskripsi" class="span3" required=""></textarea>
						</div>
					</div>
					
					<div class="control-group">
						<label class="control-label">Description (english)</label>
						<div class="controls">
							<textarea name="description" class="span3" required=""></textarea>
						</div>
					</div>
				
		      	</div>
		      	<div class="modal-footer">
		      		<input class="btn btn-success" id="sbm" type="submit" value="Save">
		        	<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
		      	</div>
	    	</div>
    	</form>
  	</div>
</div>
<!-- modal end -->

<div id="infoModal" class="modal fade" role="dialog">
  	<div class="modal-dialog">

    	<!-- Modal content-->
    	<div class="modal-content">
	      	<div class="modal-header">
	        	<h4 class="modal-title">Informasi</h4>
	      	</div>
	      	<div class="modal-body">
	    		<p>Angka yang digunakan untuk <b>Nilai Mutu</b>, <b>Nilai Bawah</b>, dan <b>Nilai Atas</b> (untuk rentang nilai) adalah angka desimal. Untuk menggunakan format angka desimal gunakan <b>TITIK</b> sebagai pemisah angka. Contoh <b>4.00 (empat titik nol nol).</b></p>
	      	</div>
	      	<div class="modal-footer">
	        	<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	      	</div>
    	</div>
  	</div>
</div>
<!-- modal end -->

<!-- modal edit -->
<div id="editModal" class="modal fade" role="dialog">
  	<div class="modal-dialog">

    	<!-- Modal content-->
    	<div class="modal-content" id="contentEdit">
	      	
    	</div>

  	</div>
</div>
<!-- end modal edit -->