<div class="row">
	<div class="span12">
		<div class="widget ">
			<div class="widget-header">
				<i class="icon-user"></i>
				<h3>Version Sistem Informasi Akademik</h3>
			</div> <!-- /widget-header -->

			<div class="widget-content">
				<div class="span11">
					<a class="btn btn-primary" href="<?php echo base_url(); ?>setting/version/tambah "><i class="btn-icon-only icon-plus"> Tambah </i></a>
					<hr>
					<table id="example1" class="table table-bordered table-striped">
						<thead>
							<tr>
								<th>No</th>
								<th>Versi</th>
								<th>Keterangan</th>
								<th>Waktu</th>
								<th>Aksi</th>
							</tr>
						</thead>
						<tbody>
							<?php $i = 1;
							foreach ($versi as $row) { ?>
								<tr>
									<td><?php echo $i ?></td>
									<td><?php echo $row->versi ?></td>
									<td><?php echo $row->keterangan ?></td>
									<td><?php echo $row->created_at ?></td>
									<td>
										<a class="btn btn-success" onclick="detail(<?php echo $row->id_versi ?>)" href="#detailModal" data-toggle="modal" title="Detail"><i class="btn-icon-only icon-eye-open"></i></a>
										<a class="btn btn-warning" href="<?php echo base_url(); ?>setting/version/edit/<?php echo $row->id_versi; ?>" data-toggle="tooltip" title="Ubah"><i class="btn-icon-only icon-edit"></i></a>
									</td>
								</tr>
							<?php $i++;
							} ?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>

<!-- edit modal -->
<div class="modal fade" id="detailModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content" id="detail">
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script>
	function detail(id) {
		$("#detail").load('<?php echo base_url() ?>setting/version/detail_version/' + id);
	}
</script>