<form class ='form-horizontal' action="<?php echo base_url(); ?>setting/cpanel/update" method="post">
	<input type="hidden" name="id" value="<?php echo $getEdit->id; ?>"/>
    <div class="modal-body" style="margin-left: -30px;">    
        <div class="control-group" id="">
            <label class="control-label">Panel</label>
            <div class="controls">
                <input type="text" class="span3 form-control" value="<?php echo $getEdit->deskripsi; ?>" readonly required/>
            </div>
        </div>
        <div class="control-group" id="">
            <label class="control-label">Status</label>
            <div class="controls">
                <select name="status" class="span2 form-control">
                    <option value="1">On</option>
                    <option value="0">Off</option>
                </select>
            </div>
        </div>
    </div> 
    <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
        <input type="submit" class="btn btn-primary" value="Submit"/>
    </div>
</form>