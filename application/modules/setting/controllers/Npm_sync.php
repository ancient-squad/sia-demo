<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Npm_sync extends MY_Controller {

	function __construct()
	{
		parent::__construct();
		$this->dbreg = $this->load->database('regis', TRUE);
	}

	public function index()
	{
		$data['list'] = $this->db->query('SELECT * from tbl_jurusan_prodi b join tbl_fakultas c on c.`kd_fakultas`=b.`kd_fakultas` ORDER BY c.`fakultas` asc')->result();
		$data['page'] = "v_sinc_table";
		$this->load->view('template/template', $data);
	}

	function print_npm($id)
	{
		/*
		if (($id == '61101') or ($id == '74101')) {
			$data['koll'] = $this->db->query('SELECT * from tbl_pmb_s2 a join tbl_jurusan_prodi b
											on a.`opsi_prodi_s2`=b.`kd_prodi` join tbl_fakultas c
											on c.`kd_fakultas`=b.`kd_fakultas`
											where a.`status` > 1 and a.`opsi_prodi_s2` = "'.$id.'" and npm_baru is not null')->result();
			$this->load->view('excel_maba_s2', $data);	
		} else {
			$data['koll'] = $this->db->query('SELECT * from tbl_form_camaba a join tbl_jurusan_prodi b
											on a.`prodi`=b.`kd_prodi` join tbl_fakultas c
											on c.`kd_fakultas`=b.`kd_fakultas`
											where a.`status` > 1 and a.`prodi` = "'.$id.'" and npm_baru is not null')->result();
			$this->load->view('excel_maba', $data);	
		}
		*/

		$data['koll'] = $this->dbreg->query('SELECT * from tbl_form_pmb
											where status > 2 
											and prodi = "'.$id.'" 
											and npm_baru is not null')->result();
		
		if ($id == '61101' || $id == '74101') {
			
			$this->load->view('excel_maba_s2', $data);	

		} else {
			
			$this->load->view('excel_maba', $data);	

		}
			
	}

	function print_npm2016($id)
	{
		if (($id == '61101') or ($id == '74101')) {
			$data['koll'] = $this->db->query('SELECT * from tbl_pmb_s2_2016 a join tbl_jurusan_prodi b
											on a.`opsi_prodi_s2`=b.`kd_prodi` join tbl_fakultas c
											on c.`kd_fakultas`=b.`kd_fakultas`
											where a.`status` > 1 and a.`opsi_prodi_s2` = "'.$id.'" and npm_baru is not null')->result();
			$this->load->view('excel_maba_s2', $data);	
		} else {
			$data['koll'] = $this->db->query('SELECT * from tbl_form_camaba_2016 a join tbl_jurusan_prodi b
											on a.`prodi`=b.`kd_prodi` join tbl_fakultas c
											on c.`kd_fakultas`=b.`kd_fakultas`
											where a.`status` > 1 and a.`prodi` = "'.$id.'" and npm_baru is not null')->result();
			$this->load->view('excel_maba', $data);	
		}
		
			
	}

}

/* End of file Npm_sinc.php */
/* Location: ./application/modules/setting/controllers/Npm_sinc.php */