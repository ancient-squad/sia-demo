<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Role extends MY_Controller {

	function __construct()
	{
		parent::__construct();
		error_reporting(0);
		if ($this->session->userdata('sess_login') == TRUE) {
			$cekakses = $this->role_model->cekakses(5)->result();
			if ($cekakses != TRUE) {
				echo "<script>alert('Anda Tidak Berhak Mengakses !!');history.go(-1);</script>";
			}
		} else {
			redirect('auth','refresh');
		}
	}

	function index()
	{
		$data['parent'] = $this->db->query("select * from tbl_menu where parent_menu = 0")->result();
		$data['usergrup'] = $this->app_model->getdata('tbl_user_group','user_group','asc')->result();
		$data['page'] = 'setting/role_view';
		$this->load->view('template/template',$data);
	}
	
	function getdataedit($id_user_group){
		$data['parent'] = $this->db->query("select * from tbl_menu where parent_menu = 0")->result();
		$data['usergrup'] = $id_user_group;
		$this->load->view('setting/edit_role_view',$data);
	}

	function list_menu($id)
	{
		$tmp = '';
		$list = $this->app_model->getlistmenu($id)->result();
		
		if (!empty($list)) {
            $tmp .= "<option value=''> -- Pilih -- </option>";
            foreach ($list as $row) {
            	if ($row->parent_menu != 0) {
            		$tmp .= "<option value='" . $row->id_menu . "'>" . $row->menu . "</option>";
            	} else {
            		$tmp .= "<option value='" . $row->id_menu . "'> -- <b>" . $row->menu . "* -- </b> </option>";
            	}       
            }
        } else {
            $tmp .= "<option value=''> -- Pilih -- </option>";
        }
        die($tmp);
	}

	function save()
	{
		$data['user_group_id'] = $this->input->post('user_group', TRUE);
		$data['menu_id'] = $this->input->post('menu', TRUE);
		$data['create'] = $this->input->post('create', TRUE);
		$data['edit'] = $this->input->post('edit', TRUE);
		$data['delete'] = $this->input->post('delete', TRUE);
		$insert = $this->app_model->insertdata('tbl_role_access',$data);
		if ($insert == TRUE) {
			echo "<script>alert('Berhasil');document.location.href='".base_url()."setting/role';</script>";
		} else {
			echo "<script>alert('Gagal Simpan Data');history.go(-1);</script>";
		}
	}
	
	function edit_role(){
		$trows = $this->input->post('trows');
		$prows = $this->input->post('prows');
		$user_group = $this->input->post('id_user_group');
		//die($trows." ".$user_grup);
		$this->db->query("delete from tbl_role_access where user_group_id = '$user_group'");
		for($no = 1; $no <= $trows; $no++){
			$menu = $this->input->post('menu'.$no);
			if($menu == 1){
			$data = array(
			'menu_id'	=> $this->input->post('kode'.$no),
			'user_group_id'=> $user_group,
			'create' => $this->input->post('create'.$no),
			'edit' => $this->input->post('edit'.$no),
			'delete' => $this->input->post('delete'.$no)
			);
			$this->db->insert('tbl_role_access',$data);
			}
		}
		for($no = 1; $no <= $prows; $no++){
			$menu = $this->input->post('parent'.$no);
			if($menu == 1){
			$data = array(
			'menu_id'	=> $this->input->post('parmen'.$no),
			'user_group_id'=> $user_group,
			'create' => $this->input->post('create'.$no),
			'edit' => $this->input->post('edit'.$no),
			'delete' => $this->input->post('delete'.$no)
			);
			$this->db->insert('tbl_role_access',$data);
			}
		}
		redirect('setting/role');
	}

}

/* End of file role.php */
/* Location: ./application/controllers/role.php */