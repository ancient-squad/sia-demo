<div class="row">
	<div class="span12">      		  		
  		<div class="widget ">
  			<div class="widget-header">
                <i class="icon-archive"></i>
  				<h3>Jumlah Status Mahasiswa <?= $tahunajar ?></h3>
			</div>
			
			<div class="widget-content">
				<div class="span11">
                    <a class="btn btn-warning" href="<?= base_url(); ?>keuangan/report_jml/">
                        <i class="icon-chevron-left"></i> Kembali
                    </a>
                    <a class="btn btn-success" href="<?= base_url(); ?>keuangan/report_jml/export_total_status">
                        <i class="icon-print"></i> Print Excel
                    </a>
                    <hr>
                    <table id="example1" class="table table-bordered table-striped">
                        <thead>
                            <tr> 
                                <th>No</th>
                                <th>Prodi</th>
                                <th>Fakultas</th>
                                <th>Aktif</th>
                                <th>Cuti</th>
                                <th>Non Aktif</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $no=1; foreach($query as $row) { ?>
                            <tr>
                                <td><?= $no; ?></td>
                                <td><?= $row->prodi; ?></td>
                                <td><?= $row->fakultas; ?></td>
                                <td>
                                    <a href="<?= base_url(); ?>keuangan/report_jml/detil_jml_aktv/<?= $row->kd_prodi; ?>">
                                        <?= $row->aktif; ?>
                                    </a>
                                </td>
                                <td>
                                    <a href="<?= base_url('keuangan/report_jml/detil_jml_cuti/'.$row->kd_prodi); ?>">
                                        <?= $row->cuti; ?>
                                    </a>
                                </td>
                                <td>
                                    <a href="<?= base_url('keuangan/report_jml/detil_jml_non/'.$row->kd_prodi); ?>"> 
                                        <?= $row->nonaktif; ?>
                                    </a>
                                </td>
                            </tr>
                            <?php $no++; } ?>
                        </tbody>
                    </table>
                </div>                
            </div>
        </div>
    </div>
</div>