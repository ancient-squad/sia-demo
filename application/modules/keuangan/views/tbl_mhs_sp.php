<script type="text/javascript">
    function edit(edc) {
        $("#cuti").load('<?= base_url()?>form/formcuti/looad/'+edc);
    }
</script>

<div class="row">
	<div class="span12">      		  		
  		<div class="widget ">
  			<div class="widget-header">
                <i class="icon-home"></i>
  				<h3>Data Validasi Semester Perbaikan/Khusus Mahasiswa</h3>
			</div> <!-- /widget-header -->
			
			<div class="widget-content">
				<div class="span11">
                    <form action="<?= base_url(); ?>keuangan/status_sp/input_data" method="post">
                        <!-- <a class="btn btn-success btn-large" href="<?php //echo base_url(); ?>keuangan/status_sp/rekap_data"><i class="btn-icon-only icon-print"> Print</i></a> -->
                        <input type="submit" class="btn btn-primary btn-large" value="Submit">
                        <hr>
                        <table id="example1" class="table table-bordered table-striped">
                            <thead>
                                <tr> 
                                    <th>No</th>
                                    <th>NIM</th>
                                    <th>Nama</th>
                                    <th>Angkatan</th>
                                    <th>Total SKS</th>
                                    <th>Validasi</th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php $no = 1; foreach ($getData as $value) { ?>
                                <tr> 
                                    <td><?= $no; ?></td>
                                    <td><?= $value->NIMHSMSMHS; ?></td>
                                    <td><?= $value->NMMHSMSMHS; ?></td>
                                    <td><?= $value->TAHUNMSMHS; ?></td>
                                    <?php
                                    $q = $this->db->query("SELECT DISTINCT a.kd_matakuliah FROM tbl_krs_sp a
                                                            JOIN tbl_jadwal_matkul_sp b ON a.`kd_jadwal` = b.`kd_jadwal`
                                                            WHERE a.kd_krs = '{$value->kd_krs}' 
                                                            AND b.`open` = 1")->result();
                                    $sumsks = 0;
                                    foreach ($q as $row) {
                                        $sksmk = $this->db->query("SELECT distinct sks_matakuliah FROM tbl_matakuliah 
                                                                    WHERE kd_matakuliah = '{$row->kd_matakuliah}' 
                                                                    AND kd_prodi = '{$this->session->userdata('jurusan')}'"
                                                                )->row()->sks_matakuliah; 
                                        $sumsks += $sksmk;
                                    }
                                    ?>
                                    <td><?= $sumsks; ?></td>
									
                                    <?php $q = $this->db->query("SELECT status FROM tbl_sinkronisasi_renkeu 
                                                                WHERE npm_mahasiswa = '{$value->NIMHSMSMHS}' 
                                                                and tahunajaran = '{$this->session->userdata('semester')}' "
                                                            )->row();
                                    if (is_null($q->status)) { ?>
                                        <td>
                                            <input 
                                                type="checkbox" 
                                                class="checkbox" 
                                                id="cek2" 
                                                name="lunas[]" 
                                                value="1911<?= $value->NIMHSMSMHS.'911'.$this->session->userdata('semester'); ?>"/>
                                        </td>
                                    <?php } else { ?>
                                        <td>
                                            <input 
                                                type="checkbox" 
                                                class="checkbox" 
                                                id="cek2" 
                                                name="lunas[]" 
                                                value="1911<?= $value->NIMHSMSMHS.'911'.$this->session->userdata('semester'); ?>" 
                                                checked disabled/>
                                        </td>
                                    <?php } ?>
									
                                </tr>
								
                            <?php $no++; } ?>
                            </tbody>
                        </table>
                    </form>
                </div>                
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content" id="cuti">
            
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>