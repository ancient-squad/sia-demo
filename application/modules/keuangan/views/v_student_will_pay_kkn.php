<script>
    // handle onCLick to load list students whose has pay for KKN
    function getStudentHasPay(angkatan, prodi, tahunajaran) {
        $('#showHere').load('<?= base_url('keuangan/kkn_payment/students_has_pay/') ?>' + angkatan + '/' + prodi + '/' + tahunajaran);
    }
</script>

<div class="row">
    <div class="span12">                    
        <div class="widget ">
            <div class="widget-header">
                <h3><i class="icon-list"></i> Daftar Mahasiswa Pengaju KKN</h3>
            </div> <!-- /widget-header -->
            
            <div class="widget-content">
                <div class="tabbable">
                    <ul class="nav nav-tabs">
                        <li class="active">
                            <a href="#form1" data-toggle="tab">Pembayaran KKN</a>
                        </li>
                        <li>
                            <a 
                                href="#form2" 
                                onclick="getStudentHasPay('<?= $angkatan ?>','<?= $prodi ?>','<?= $tahunajaran ?>')" 
                                data-toggle="tab">Mahasiswa Lunas KKN
                            </a>
                        </li>
                    </ul>           
                    <br>
                    <div class="tab-content">
                        <div class="tab-pane active" id="form1">
                            <div class="span11">
                                <form class="form-horizontal" action="<?= base_url('keuangan/kkn_payment/save_payment'); ?>" method="post">
                                    <fieldset>
                                        <a class="btn btn-warning" href="<?= base_url('keuangan/kkn_payment'); ?>">
                                            <i class="icon-arrow-left"></i> Kembali
                                        </a>
                                        <button type="submit" class="btn btn-success pull-right"><i class="icon-upload"></i> Submit</button>
                                        <hr>

                                        <input type="hidden" name="tahunajaran" value="<?= $tahunajaran ?>">
                                        
                                        <table id="example1" class="table table-bordered table-striped">
                                            <thead>
                                                <tr> 
                                                    <th>No</th>
                                                    <th>NPM</th>
                                                    <th>Nama</th>
                                                    <th>Prodi</th>
                                                    <th>Verifikasi</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php $no=1; foreach($studentWhoHaventPay as $students) { ?>
                                                <tr>
                                                    <td><?= $no; ?></td>
                                                    <td><?= $students->NIMHSMSMHS; ?></td>
                                                    <td><?= $students->NMMHSMSMHS; ?></td>
                                                    <td><?= get_jur($students->KDPSTMSMHS) ?></td>
                                                    <td>
                                                        <center>
                                                            <input 
                                                                type="checkbox" 
                                                                name="npm[]" 
                                                                value="<?= $students->NIMHSMSMHS ?>" 
                                                                class="form-control">
                                                        </center>
                                                    </td>
                                                </tr>
                                                <?php $no++; } ?>
                                            </tbody>
                                        </table>
                                    </fieldset>
                                </form>
                                <p class="footer">Page rendered in <strong>{elapsed_time}</strong> seconds.</p>
                            </div>
                        </div>
                        <div class="tab-pane" id="form2">
                            <div class="span11" id="showHere">
                                <center>
                                    <img src="<?= base_url('assets/img/loader.gif') ?>" style="width: 20%;">
                                </center>
                            </div>
                        </div>
    
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>