            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Edit Data Pembayaran</h4>
            </div>
            <form class ='form-horizontal' action="<?php echo base_url();?>keuangan/Jenispembyrn/update_pembayaran" method="post">
                <div class="modal-body" style="margin-left: -60px;"> 
                    <script type="text/javascript">
                        $(document).ready(function(){
                            $.post('<?php echo base_url()?>keuangan/pembayaran/get_list_pembyrn'+$(this).val(),{},function(get){
                                $("#kd_jenis").html(get);
                            });
                        });
                    </script>
                    <div class="control-group" id="">
                        <label class="control-label">Kode</label>
                        <div class="controls">
                            <input type="hidden" name="id_jenis" value="<?php echo $list->id_jenis;?>">
                            <input type="text" id="kd_jenis" class="span4" name="kode" placeholder="Input Kode" class="form-control" value="<?php echo $list->kd_jenis;?>"required/>
                        </div>
                    </div>              
                    <div class="control-group" id="">
                        <label class="control-label">pembayaran</label>
                        <div class="controls">
                            <input type="text" id="id_jenis" class="span4" name="pembayaran" placeholder="Input Pembayaran" class="form-control" value="<?php echo $list->jenis_pembayaran?>" required/>
                        </div>
                    </div>
                </div> 
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Keluar</button>
                    <input type="submit" class="btn btn-primary" value="Simpan"/>
                </div>
            </form>