<link href="<?php echo base_url();?>assets/css/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />

<fieldset>
    <a class="btn btn-warning" href="<?= base_url('keuangan/kkn_payment'); ?>">
        <i class="icon-arrow-left"></i> Kembali
    </a>
    <hr>
    
    <table id="examples" class="table table-bordered table-striped">
        <thead>
            <tr> 
                <th>No</th>
                <th>NPM</th>
                <th>Nama</th>
                <th>Prodi</th>
                <th>Hapus</th>
            </tr>
        </thead>
        <tbody>
            <?php $no=1; foreach($studentHavePay as $students) { ?>
            <tr>
                <td><?= $no; ?></td>
                <td><?= $students->npm; ?></td>
                <td><?= $students->NMMHSMSMHS; ?></td>
                <td><?= get_jur($students->KDPSTMSMHS) ?></td>
                <td>
                    <a 
                        class="btn btn-danger" 
                        onclick="return confirm('Yakin ingin menghapus data ini?')" 
                        href="<?= base_url('keuangan/kkn_payment/void_payment/'.$students->npm) ?>">
                        <i class="icon-remove"></i>
                    </a>
                </td>
            </tr>
            <?php $no++; } ?>
        </tbody>
    </table>
</fieldset>

<script src="<?php echo base_url();?>assets/js/datatables/jquery.dataTables.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/js/datatables/dataTables.bootstrap.js" type="text/javascript"></script>

<script type="text/javascript">
    $(function() {
        $("#examples").dataTable();
    });
</script>