<?php $tgl = date('Y'); $increase = $tgl + 1;?>
<div class="row">
	<div class="span12">      		  		
  		<div class="widget ">
  			<div class="widget-header">
                <i class="icon-file"></i>
  				<h3>Pilih Gelombang Registrasi Calon Mahasiswa</h3>
			</div> <!-- /widget-header -->
			
			<div class="widget-content">
				<div class="span11">
                    <form action="<?php echo base_url(); ?>keuangan/valid_regis/sess_reg" method="post" class="form-horizontal">
                        <fieldset>
                            <div class="control-group">
                                <label class="control-label">Tahun</label>
                                <div class="controls">
                                  <select class="form-control span4" name="year">
                                    <option selected="" disabled="">--Pilih Tahun--</option>
                                    <?php for ($x = 2017; $x <= $increase; $x++) { ?>
                                    <option value="<?php echo $x;?>"><?php echo $x;?></option>
                                    <?php } ?>
                                  </select>
                                </div>
                            </div>                         
                            <div class="control-group">
                                <label class="control-label">Gelombang</label>
                                <div class="controls">
                                  <select class="form-control span4" name="gel">
                                    <option selected="" disabled="">--Pilih Gelombang--</option>
                                    <?php for ($i = 1; $i < 6; $i++) { ?>
                                    <option value="<?php echo $i;?>"><?php echo $i;?></option>
                                    <?php } ?>
                                  </select>
                                </div>
                            </div>  

                            <div class="form-actions">
                                <input type="submit" class="btn btn-large btn-success" value="Submit"/> 
                            </div> <!-- /form-actions -->
                        </fieldset>
                    </form>
                </div>                
            </div>
        </div>
    </div>
</div>