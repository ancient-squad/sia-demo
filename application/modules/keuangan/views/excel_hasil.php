<?php
header("Content-Type: application/xls");    
header("Content-Disposition: attachment; filename=Data_Mahasiswa_Aktif_".str_replace(' ', '_', get_jur($prodi))."_".$tahunajar.".xls");  
header("Pragma: no-cache"); 
header("Expires: 0");
?>

<style>
table, td, th {
    border: 1px solid black;
}

th {
    background-color: blue;
    color: black;
}
</style>

<table id="example1" class="table table-bordered table-striped" border="1">
    <thead>
        <tr> 
            <th>No</th>
            <th>NPM</th>
            <th>Nama</th>
            <th>Total SKS</th>
        </tr>
    </thead>
    <tbody>
        <?php $no=1; foreach($hasil as $row) { ?>
        <tr>
            <td><?= $no; ?></td>
            <td><?= $row->NIMHSMSMHS; ?></td>
            <td><?= $row->NMMHSMSMHS; ?></td>
            <td><?= $row->tot; ?></td>
        </tr>
        <?php $no++; } ?>
    </tbody>
</table>