<!DOCTYPE html>

<html lang="en">

<head>

    <meta charset="utf-8">

              <?php $user = $this->session->userdata('sess_login');

              if ($user['user_type'] == 1) {

                $nama = $this->app_model->getdetail('tbl_karyawan','nid',$user['userid'],'nik','asc')->row(); $name = $nama->nama;

              } elseif($user['user_type'] == 3) {

                $nama = $this->app_model->getdetail('tbl_divisi','kd_divisi',$user['userid'],'kd_divisi','asc')->row(); $name = $nama->divisi;

              } else {

                $nama = $this->app_model->getdetail('tbl_karyawan_2','nip',$user['userid'],'nip','asc')->row(); $name = $nama->nama;

              }

               ?>
    <title>Siakad | <?php echo $name?></title>
    <link rel="shortcut icon" href="<?php echo base_url(); ?>assets/logo.ico"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

    <meta name="apple-mobile-web-app-capable" content="yes">

    <link href="<?php echo base_url();?>assets/css/bootstrap.min.css" rel="stylesheet">

    <link href="<?php echo base_url();?>assets/css/bootstrap-responsive.min.css" rel="stylesheet">

    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">

    <link href="<?php echo base_url();?>assets/css/font-awesome.css" rel="stylesheet">

    <link href="<?php echo base_url();?>assets/css/style.css" rel="stylesheet">

    <link href="<?php echo base_url();?>assets/css/pages/dashboard.css" rel="stylesheet">

    <link href="<?php echo base_url();?>assets/css/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />

    <link href="<?php echo base_url();?>assets/css/pages/reports.css" rel="stylesheet">

    <link href="<?php echo base_url();?>assets/js/jquery-ui/css/ui-lightness/jquery-ui-1.9.2.custom.css" rel="stylesheet">

    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->

    <!--[if lt IE 9]>

          <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>

    <![endif]-->

    <script src="<?php echo base_url();?>assets/js/jquery-1.7.2.min.js"></script> 
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery-ui/js/jquery-ui.js"></script>

</head>

<body style="background: rgba(0, 0, 0, 0) url('https://www.toptal.com/designers/subtlepatterns/patterns/cloth.png') repeat scroll 0% 0%;">

<div class="navbar navbar-fixed-top">

  <div class="navbar-inner">

    <div class="container"> <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse"><span

                    class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span> </a><a class="brand" href="#"><img src="<?php echo base_url();?>assets/logo.png" style="width:40px;float:left;margin-top:-8px;margin-bottom:-13px">

            <span style="margin-left:10px;line-height:-15px">SISTEM INFORMASI AKADEMIK </span></a>

      <div class="nav-collapse">

        <ul class="nav pull-right">

          <li class="dropdown">

            <a href="#" class="dropdown-toggle" data-toggle="dropdown" style="font-size:16px">


              <i class="icon-user"></i> <?php echo $name; ?> <b class="caret"></b></a>

            <ul class="dropdown-menu">

              <li><a href="<?php echo base_url();?>extra/account">Ganti Password</a></li>

              <li><a href="<?php echo base_url();?>auth/logout">Logout</a></li>

            </ul>

          </li>

        </ul>

      </div>

      <!--/.nav-collapse --> 

    </div>

    <!-- /container --> 

  </div>

  <!-- /navbar-inner --> 

</div>

<!-- /navbar -->

<div class="subnavbar">

  <div class="subnavbar-inner">

    <div class="container">

      <ul class="mainnav">

        <li class="active" ><a href="<?php echo base_url();?>home"><i class="icon-home"></i><span >Dashboard</span> </a>

        <?php $q = $this->role_model->getparentmenu()->result(); foreach ($q as $menu) { if (($menu->url) == '-') { ?>

            <li class="dropdown active"><a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown">

        <?php } elseif (($menu->url) == '#') { ?>

            <li class="active" ><a href="<?php echo $menu->url; ?>">

        <?php } else { ?>

            <li class="active" ><a href="<?php echo base_url();?><?php echo $menu->url; ?>">

        <?php } ?>

            <i class="<?php echo $menu->icon; ?>"></i><span ><?php echo $menu->menu; ?></span> </a>



        <?php if (($menu->url) == '-') { ?>

                <ul class="dropdown-menu">

                 <?php $qd = $this->role_model->getmenu($menu->id_menu)->result(); foreach ($qd as $row) { ?>

                     <?php if (($row->url) == '#') { ?>

                        <li><a href="<?php echo $row->url; ?>">

                    <?php } else { ?>

                        <li><a href="<?php echo base_url();?><?php echo $row->url; ?>">

                    <?php } ?>

                    <?php echo $row->menu; ?></a></li>

                    <?php } ?>

                  </ul>

                <?php } ?>

            </li>

        <?php } ?>

      </ul>

    </div>

    <!-- /container --> 

  </div>

  <!-- /subnavbar-inner --> 

</div>

<!-- /subnavbar -->

<div class="main">

  <div class="main-inner">

    <div class="container">

      <!-- load page -->

      <?php $this->load->view($page); ?>

      <!-- /row --> 

    </div>

    <!-- /container --> 

  </div>

  <!-- /main-inner --> 

</div>

<!-- /main -->

<div class="footer">

  <div class="footer-inner">

    <div class="container">

      <div class="row">

        <div class="span12"> &copy; <?php date_default_timezone_set('Asia/Jakarta');echo date('Y'); ?> - <a target="_blank" href="http://<?= $this->URL ?>/">UNIVERSITAS <?= $this->ORG_NAME ?></a>. <b><i>Last Login : <?php $cek = $this->app_model->getdetail('tbl_user_login','userid',$user['userid'],'userid','asc')->row()->last_login; echo $cek; ?></i></b></div>

        <!-- /span12 --> 

      </div>

      <!-- /row --> 

    </div>

    <!-- /container --> 

  </div>

  <!-- /footer-inner --> 

</div>

<!-- /footer --> 

<!-- Le javascript

================================================== --> 

<!-- Placed at the end of the document so the pages load faster -->  

<script src="<?php echo base_url();?>assets/js/bootstrap.js"></script>

<script language="javascript" type="text/javascript" src="<?php echo base_url();?>assets/js/full-calendar/fullcalendar.min.js"></script>

<script type="text/javascript">

$(document).ready(function() {

        var date = new Date();

        var d = date.getDate();

        var m = date.getMonth();

        var y = date.getFullYear();

        var calendar = $('#calendar').fullCalendar({

          header: {

            left: 'prev,next today',

            center: 'title',

            right: 'month,agendaWeek,agendaDay'

          },

          selectable: true,

          selectHelper: true,

          select: function(start, end, allDay) {

            var title = prompt('Event Title:');

            if (title) {

              calendar.fullCalendar('renderEvent',

                {

                  title: title,

                  start: start,

                  end: end,

                  allDay: allDay

                },

                true // make the event "stick"

              );

            }

            calendar.fullCalendar('unselect');

          },

          editable: true,

          events: [

            {

              title: 'All Day Event',

              start: new Date(y, m, 1)

            },

            {

              title: 'Long Event',

              start: new Date(y, m, d+5),

              end: new Date(y, m, d+7)

            },

            {

              id: 999,

              title: 'Repeating Event',

              start: new Date(y, m, d-3, 16, 0),

              allDay: false

            },

            {

              id: 999,

              title: 'Repeating Event',

              start: new Date(y, m, d+4, 16, 0),

              allDay: false

            },

            {

              title: 'Meeting',

              start: new Date(y, m, d, 10, 30),

              allDay: false

            },

            {

              title: 'Lunch',

              start: new Date(y, m, d, 12, 0),

              end: new Date(y, m, d, 14, 0),

              allDay: false

            },

            {

              title: 'Birthday Party',

              start: new Date(y, m, d+1, 19, 0),

              end: new Date(y, m, d+1, 22, 30),

              allDay: false

            },

            {

              title: 'EGrappler.com',

              start: new Date(y, m, 28),

              end: new Date(y, m, 29),

              url: 'http://EGrappler.com/'

            }

          ]

        });

      });

</script>

<script src="<?php echo base_url();?>assets/js/datatables/jquery.dataTables.js" type="text/javascript"></script>

<script src="<?php echo base_url();?>assets/js/datatables/dataTables.bootstrap.js" type="text/javascript"></script>

<script type="text/javascript">

    $(function() {

        $("#example1").dataTable();

        $("#example3").dataTable();

        $("#example4").dataTable();

        $("#example5").dataTable();

        $('#example2').dataTable({

            "bPaginate": false,

            "bLengthChange": false,

            "bFilter": false,

            "bSort": true,

            "bInfo": false,

            "bAutoWidth": true

        });

    });

</script>

<!--script type="text/javascript">

    $(function() {

        $("#analisa1").dataTable({

          "bPaginate": false,

            "bLengthChange": false,

            "bFilter": false,

            "bSort": true,

            "bInfo": false,

            "bAutoWidth": true

        });

        $("#analisa2").dataTable({

          "bPaginate": false,

            "bLengthChange": false,

            "bFilter": false,

            "bSort": true,

            "bInfo": false,

            "bAutoWidth": true

        });

        $("#analisa3").dataTable({

          "bPaginate": false,

            "bLengthChange": false,

            "bFilter": false,

            "bSort": true,

            "bInfo": false,

            "bAutoWidth": true

        });

        $("#analisa4").dataTable({

          "bPaginate": false,

            "bLengthChange": false,

            "bFilter": false,

            "bSort": true,

            "bInfo": false,

            "bAutoWidth": true

        });

    });

</script-->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-93675871-1', 'auto');
  ga('send', 'pageview');

</script>


</body>

</html>

