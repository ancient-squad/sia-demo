<div class="row">
	<div class="span12">      		  		
  		<div class="widget ">
  			<div class="widget-header">
                <i class="icon-file-alt"></i>
  				<h3>Detail Kartu Rencana Studi - <?= $nama.' ('.$nim.')'?></h3>
			</div> <!-- /widget-header -->
			
			<div class="widget-content">
				<div class="span11">
                    <a href="<?= base_url('keuangan/report_jml/detil_jml_aktv/'.$prodi) ?>" class="btn btn-warning">
                        <i class="icon-chevron-left"></i> Kembali
                    </a> <br><br>
                    <table>
                        <tr>
                            <td>Prodi</td>
                            <td> &emsp;:&emsp; </td>
                            <td><?= get_jur($prodi); ?></td>
                            <td class="span6"></td>
                            <td>Mahasiswa</td>
                            <td> &emsp;:&emsp; </td>
                            <td><?= $nama ?></td>
                        </tr>
                        <tr>
                            <td>Tahun Akademik</td>
                            <td>&emsp;:&emsp;</td>
                            <td><?= get_thnajar($this->session->userdata('tahunajaran_report_sts')); ?></td>
                            <td class="span6"></td>
                            <td>Nomor Pokok</td>
                            <td>&emsp;:&emsp;</td>
                            <td><?= $nim ?></td>
                        </tr>
                    </table>
                    <hr>
                    <table id="example1" class="table table-bordered table-striped">
                        <thead>
                            <tr> 
                                <th>No</th>
                                <th>Kode Matakuliah</th>
                                <th>Nama Matakuliah</th>
                                <th>SKS</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $sks=0; $no=1; foreach($rows as $row) { ?>
                            <tr>
                                <td><?= $no; ?></td>
                                <td><?= $row->kd_matakuliah; ?></td>
                                <td><?= $row->nama_matakuliah; ?></td>
                                <td><?= $row->sks_matakuliah; ?></td>
                            </tr>
                            <?php $sks=$sks+$row->sks_matakuliah; $no++; } ?>
                        </tbody>
                        <tfoot>
                            <tr> 
                                <td colspan="3">Total SKS</td>
                                <td ><?= $sks; ?></td>
                            </tr>
                        </tfoot>
                    </table>
                </div>                
            </div>
        </div>
    </div>
</div>