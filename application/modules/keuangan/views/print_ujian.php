<?php
header("Content-Type: application/xls");    
header("Content-Disposition: attachment; filename=data_statusujian_mhs.xls");  
header("Pragma: no-cache"); 
header("Expires: 0");
?>

<style>
table, td, th {
    border: 1px solid black;
}

th {
    background-color: blue;
    color: black;
}
</style>

<table border="1">
	<thead>
		<tr>
			<th colspan="6">Daftar Status Pembayaran Mahasiswa</th>
		</tr>
		<tr>
			<th>No</th>
			<th>NPM</th>
			<th>NAMA</th>
			<th>JURUSAN</th>
			<th>FAKULTAS</th>
			<th>STATUS</th>
		</tr>
	</thead>
	<tbody>
		<?php $no = 1; foreach($pong as $row) { ?>
		<tr>
			<td><?php echo number_format($no); ?></td>
			<td><?php echo $row->NIMHSMSMHS; ?></td>
			<td><?php echo $row->NMMHSMSMHS; ?></td>
			<td><?php echo $row->prodi; ?></td>
			<td><?php echo $row->fakultas; ?></td>
			<?php if ($row->status == 1) {
				$c = "DAFTAR ULANG";
			} elseif($row->status == 2) {
				$c = "UTS";
			} elseif($row->status == 3) {
				$c = "UTS SUSULAN";
			} elseif ($row->status == 5) {
				$c = "UAS / (UTS SUSULAN)";
			} else {
				$c = "UAS";
			}
			?>
			<td><?php echo $c ?></td>
		</tr>
		<?php $no++; } ?>
	</tbody>
</table>