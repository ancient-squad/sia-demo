<script type="text/javascript">
    function edit(idf) {
        $("#edit_pembayaran").load('<?php echo base_url()?>keuangan/jenispembyrn/get_list_pembyrn/'+idf);
    }

</script>


<div class="row">
    <div class="span12">                    
        <div class="widget ">
            <div class="widget-header">
                <i class="icon-user"></i>
                <h3>Data Pembayaran</h3>
            </div> <!-- /widget-header -->
            
            <div class="widget-content">
                <div class="span11">
                    <a data-toggle="modal" href="#myModal" class="btn btn-primary"> New Data </a><br><hr>
                    <table id="example1" class="table table-bordered table-striped">
                        <thead>
                            <tr> 
                                <th>No</th>
                                <th>Kode</th>
                                <th>Pembayaran</th>
                                <th width="120">Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $no = 1; foreach($pembayaran->result() as $row){?>
                            <tr>
                                <td><?php echo $no;?></td>
                                <td><?php echo $row->kd_jenis;?></td>
                                <td><?php echo $row->jenis_pembayaran;?></td>
                                <td class="td-actions">
                                    <a onclick="edit(<?php echo $row->id_jenis;?>)" class="btn btn-primary btn-small" href="#editModal" data-toggle="modal"><i class="btn-icon-only icon-pencil"> </i></a>
                                    <a onclick="return confirm('Apakah Anda Yakin?');" class="btn btn-danger btn-small" href="<?php echo base_url();?>keuangan/jenispembyrn/del_pembayaran/<?php echo $row->id_jenis;?>"><i class="btn-icon-only icon-remove"> </i></a>
                                </td>
                            </tr>
                            <?php $no++; } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Form Data</h4>
            </div>
            <form class ='form-horizontal' action="<?php echo base_url();?>keuangan/jenispembyrn/save_pembayaran" method="post" enctype="multipart/form-data">
                <div class="modal-body" style="margin-left: -30px;">  
                    <div class="control-group" id="">
                        <label class="control-label">Kode Pembayaran</label>
                        <div class="controls">
                            <input type="text" class="span4" name="kode" id='kode' placeholder="Input Kode" class="form-control" value="" style="text-transform:uppercase" required/>
                        </div>
                    </div>              
                    <div class="control-group" id="">
                        <label class="control-label">Pembayaran</label>
                        <div class="controls">
                            <input type="text" class="span4" name="pembayaran" placeholder="Input Pembayaran" class="form-control" value="" required/>
                        </div>
                    </div>
                </div> 
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Keluar</button>
                    <input type="submit" class="btn btn-primary" value="Simpan"/>
                </div>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<!-- modal edit pembayaran-->
<div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content" id="edit_pembayaran">
            
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->