<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Status_sp extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		error_reporting(0);
		if (!$this->session->userdata('sess_login')) {
			redirect('auth/logout','refresh');
		}
		date_default_timezone_set('Asia/Jakarta'); 
	}

	public function index()
	{
		$data['fakultas'] =$this->app_model->getdata('tbl_fakultas', 'id_fakultas', 'ASC')->result();
		$data['ta'] =$this->app_model->getdata('tbl_tahunakademik', 'id', 'ASC')->result();
		$data['page'] = "sp_view";
		$this->load->view('template/template', $data);
	}

	function simpan_sesi()
	{
		$fakultas = $this->input->post('fakultas');
		$jurusan = $this->input->post('jurusan');
		$semester = $this->input->post('semester');
		$this->session->set_userdata('jurusan', $jurusan);
		$this->session->set_userdata('fakultas', $fakultas);
		$this->session->set_userdata('semester', $semester);
		redirect(base_url('keuangan/status_sp/load_uji'));
	}

	function get_jurusan($id)
	{
		$jrs = explode('-',$id);
        $jurusan = $this->app_model->getdetail('tbl_jurusan_prodi', 'kd_fakultas', $jrs[0], 'id_prodi', 'ASC')->result();
		$out = "<select class='form-control' name='jurusan' id='jurs'><option>--Pilih Jurusan--</option>";

        foreach ($jurusan as $row) {
            $out .= "<option value='".$row->kd_prodi."'>".$row->prodi. "</option>";
        }

        $out .= "</select>";
        echo $out;
	}

	function load_uji()
	{
		$data['getData'] = $this->db->query("SELECT DISTINCT 
												a.NIMHSMSMHS,
												a.NMMHSMSMHS,
												a.TAHUNMSMHS,
												c.kd_krs 
											FROM tbl_mahasiswa a 
											JOIN tbl_jurusan_prodi b ON a.`KDPSTMSMHS`=b.`kd_prodi` 
											JOIN tbl_verifikasi_krs_sp c ON a.NIMHSMSMHS = c.npm_mahasiswa
											WHERE b.`kd_prodi`= '{$this->session->userdata('jurusan')}' 
											AND c.tahunajaran = '{$this->session->userdata('semester')}'
											ORDER BY a.`NIMHSMSMHS` ASC")->result();
		$data['page'] = "keuangan/tbl_mhs_sp";
		$this->load->view('template/template', $data);
	}

	function input_data()
	{
		$a = $this->session->userdata('sess_login');
		$b = $a['userid'];

		$jum = count($this->input->post('lunas', TRUE));
		
		for ($i=0; $i < $jum ; $i++) { 
			$v = explode('911', $this->input->post('lunas['.$i.']', TRUE));
			
			$mhs = $this->app_model->getdetail('tbl_mahasiswa','NIMHSMSMHS',$v[1],'NIMHSMSMHS','asc')->row();
			$k = $mhs->KDPSTMSMHS; 
			$t = $mhs->SMAWLMSMHS;
			$o = $mhs->NIMHSMSMHS;

			$y = $this->app_model->get_semester($t);

			$data['briva_mhs'] = '70306'.substr($v[1], 2);
			$data['npm_mahasiswa'] = $v[1];
			$data['tahunajaran'] = $v[2];
			
			if ($v[0] == 'z') {
			 	$data['status'] = NULL;
			 } elseif($v[0] == 8) {
			 	$data2['status'] = 'C';
			 	$data2['tanggal'] = date('Y-m-d');
				$data2['npm'] = $v[1];
				$data2['tahunajaran'] = $v[2];
				$data2['semester'] = $y;
			 	$this->app_model->insertdata('tbl_status_mahasiswa',$data2);
			 	$data['status'] = $v[0];
			 } else {
			 	$data['status'] = $v[0];
			 }
			$data['transaksi_terakhir'] = date('Y-m-d');
			$data['userid'] = $b;
			$data['semester'] = $y;
				
			//$tahunakademik = $this->app_model->getdetail('tbl_tahunakademik', 'status', 1, 'kode', 'ASC')->row();
			$dx = $this->db->query('SELECT * from tbl_sinkronisasi_renkeu where npm_mahasiswa = "'.$o.'" and tahunajaran = "'.$this->session->userdata('semester').'"')->row();
				
			if ($dx == TRUE) {
				$mn = $dx->id_sink;
				$l   = $this->app_model->getdetail('tbl_sinkronisasi_renkeu','id_sink',$mn,'id_sink','asc')->row();
				$gb  = $l->npm_mahasiswa;
				$this->app_model->updatedata_by_smt('tbl_sinkronisasi_renkeu','npm_mahasiswa',$gb,$data,$this->session->userdata('semester'));
			} else {
				//$data['kode_sink'] = $doc_no;
				$this->app_model->insertdata('tbl_sinkronisasi_renkeu',$data);
			}
		}
		echo "<script>alert('Sukses');
		document.location.href='".base_url()."keuangan/status_sp/load_uji';</script>";
	}

	function rekap_data()
	{
		$data['pong'] = $this->db->query('SELECT DISTINCT b.`NIMHSMSMHS`,b.`NMMHSMSMHS`,d.`fakultas`,c.`prodi`,a.`status`
		    from tbl_sinkronisasi_renkeu a join tbl_mahasiswa b
			on a.`npm_mahasiswa`=b.`NIMHSMSMHS` join tbl_jurusan_prodi c
			on c.`kd_prodi`=b.`KDPSTMSMHS` join tbl_fakultas d
			on d.`kd_fakultas`=c.`kd_fakultas` join tbl_verifikasi_krs e
			on e.`npm_mahasiswa`=b.`NIMHSMSMHS`
			where c.`kd_prodi`="'.$this->session->userdata('jurusan').'"
			AND a.`tahunajaran`="'.$this->session->userdata('tahunajaran').'"
			AND b.`TAHUNMSMHS`="'.$this->session->userdata('angkatan').'"
			GROUP BY b.`NIMHSMSMHS`')->result();
		$this->load->view('print_ujian', $data);

	}
}

/* End of file status_ujian.php */
/* Location: ./application/controllers/status_ujian.php */