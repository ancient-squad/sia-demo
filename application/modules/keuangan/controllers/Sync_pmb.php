<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sync_pmb extends MY_Controller {

	// function __construct()
	// {
	// 	parent::__construct();
	// 	if ($this->session->userdata('sess_login') != TRUE) {
	// 		//$cekakses = $this->role_model->cekakses(57)->result();
	// 		//if ($cekakses != TRUE) {
	// 			//echo "<script>alert('Anda Tidak Berhak Mengakses !!');history.go(-1);</script>";
	// 		//}
	// 	//} else {
	// 		redirect('auth','refresh');
	// 	}
	// }

	function index()
	{
		error_reporting(0);
		$query = $this->db->query("SELECT a.`nomor_registrasi`,a.`npm_baru` FROM tbl_form_camaba a
									LEFT JOIN tbl_sinkronisasi_renkeu c ON a.`npm_baru` = c.`npm_mahasiswa`
									WHERE c.`briva_mhs` IS NULL AND a.`status` > 1")->result();		
		foreach ($query as $key) {
			$data[] = array(
	       		'briva_mhs' => '70306'.substr($key->npm_baru, 2),
				'npm_mahasiswa' => substr($key->npm_baru, 0,12),
				'tahunajaran' => '20161',
				
				'transaksi_terakhir' => date('Y-m-d'),
				'userid' => 'admin',
				'semester' => 1,
				'status' => 1
	       	);
		}

		$this->db->insert_batch('tbl_sinkronisasi_renkeu',$data);

		$query2 = $this->db->query("SELECT a.`ID_registrasi`,a.`npm_baru` FROM tbl_pmb_s2 a
									LEFT JOIN tbl_sinkronisasi_renkeu c ON a.`npm_baru` = c.`npm_mahasiswa`
									WHERE c.`briva_mhs` IS NULL AND a.`status` > 1")->result();		
		foreach ($query2 as $key1) {
			$data1[] = array(
	       		'briva_mhs' => '70306'.substr($key1->npm_baru, 2),
				'npm_mahasiswa' => substr($key1->npm_baru, 0,12),
				'tahunajaran' => '20161',
				
				'transaksi_terakhir' => date('Y-m-d'),
				'userid' => 'admin',
				'semester' => 1,
				'status' => 1
	       	);
		}

		$this->db->insert_batch('tbl_sinkronisasi_renkeu',$data1);
		echo "<script>alert('Berhasil');
		document.location.href='".base_url()."home';</script>";
		//$this->app_model->insertdata('tbl_sinkronisasi_renkeu',$data);
		
	}

}

/* End of file Sync_pmb.php */
/* Location: ./application/modules/keuangan/controllers/Sync_pmb.php */