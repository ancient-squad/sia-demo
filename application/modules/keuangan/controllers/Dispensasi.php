<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dispensasi extends MY_Controller {

	function __construct()
	{
		parent::__construct();
		if (!$this->session->userdata('sess_login')) {
			echo "<script>alert('Silahkan log-in kembali untuk memulai sesi!');</script>";
			redirect(base_url('auth/logout'),'refresh');
		} else {
			$this->sess = $this->session->userdata('sess_login');
		}
	}

	public function index()
	{
		$data['users'] = $this->app_model->getdetail('tbl_verifikasi_krs','status_verifikasi',10,'npm_mahasiswa','asc')->result();
		$data['page'] = 'v_dispensasi';
		$this->load->view('template/template', $data);
	}

	public function load_npm_autocomplete()
	{
		$this->db->distinct();
        $this->db->select("NIMHSMSMHS,NMMHSMSMHS");
        $this->db->from('tbl_mahasiswa');
        $this->db->like('NIMHSMSMHS', $_GET['term'], 'both');
        $this->db->or_like('NMMHSMSMHS', $_GET['term'], 'both');
        $sql  = $this->db->get();

        $data = array();

        foreach ($sql->result() as $row) {
            $data[] = array(
                    'value'         => $row->NIMHSMSMHS.' - '.$row->NMMHSMSMHS
                    );
        }
        echo json_encode($data);
		
		
	}
	
	public function add_dispensasi()
	{
		$tahunajaran = getactyear();
		$ini 	= $this->input->post('npm');
		$pecah 	= explode(' - ', $ini);
		$cek 	= $this->db->query('SELECT * from tbl_verifikasi_krs where npm_mahasiswa = "'.$pecah[0].'"')->row();
		if ($cek == FALSE) {
			echo "<script>alert('Data Tidak Ditemukan! Periksa User ID Pengguna!');document.location.href='".base_url()."keuangan/dispensasi';</script>";
		} else {
			$data = array(
					'status_verifikasi'			=> 10
				);
			// echo "<pre>";
			// print_r($prodi);
			// exit;
				$this->app_model->updatedatamk('tbl_verifikasi_krs','npm_mahasiswa',$pecah[0],'tahunajaran',$tahunajaran,$data);
				
				echo "<script>alert('Sukses');document.location.href='".base_url()."keuangan/dispensasi';</script>";
		}
	}
	public function pelunasan($npm,$ta)
	{
		$data = array(
				'status_verifikasi'			=> 1
			);
		// echo "<pre>";
		// print_r($prodi);
		// exit;
			$this->app_model->updatedatamk('tbl_verifikasi_krs','npm_mahasiswa',$npm,'tahunajaran',$ta,$data);
			
			echo "<script>alert('Sukses');document.location.href='".base_url()."keuangan/dispensasi';</script>";
	
	}

}

/* End of file Debt.php */
/* Location: .//tmp/fz3temp-1/Debt.php */