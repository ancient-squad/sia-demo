<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Upload_sinc extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		// if ($this->session->userdata('sess_login') == TRUE) {
		// 	 $akses = $this->role_model->cekakses(1)->result();
		// 	 if ($akses != TRUE) {
		// 	 	redirect('home','refresh');
		// 	 }
		// } else {
		// 	redirect('auth','refresh');
		// }
		//$this->load->library('csvimport');
		date_default_timezone_set('Asia/Jakarta');
	}

	public function index()
	{
		
		$data['p'] = $this->app_model->getdata('tbl_sync_detail','id_sync_detail','asc')->result();
		//$data['p'] = $this->app_model->get_sync();
		$data['page'] = "upload_sinc_view";
		$this->load->view('template/template', $data);
	}

	function naik()
	{
		$this->upload('./temp_upload/');
		redirect('keuangan/upload_sinc/sukses');
	}

	function sukses()
	{
		echo "sukses";
		// $data['q'] = $this->app_model->getdata('tbl_sync','id_sync','asc')->result();
		// $data['q'] = $this->app_model->get_sync();
		// $data['page'] = "keuangan/upload_sinc_view";
		// $this->load->view('template/template', $data);
	}

	function count_row($id)
	{
		$q = $this->db->query("select count(id_file) as total from tbl_sync where id_file = '".$id."'");
			return $q;
	}

	function upload()
	{
			// $this->load->helper('inflector');
			// $nama = underscore($_FILES['userfile']['name']);
		// $a=$this->input->post('tgl');
		// $b=$this->input->post('desk');
		// $c=$_FILES['userfile']['name'];

		//die($a.'-'.$b.'-'.$c);
			//$config['encrypt_name'] = TRUE;
			//$kode=$config;

			$config['upload_path'] = './temp_upload/';
			$config['allowed_types'] = 'text/plain|text/anytext|csv|text/x-comma-separated-values|text/comma-separated-values|application/octet-stream|application/vnd.ms-excel|application/x-csv|text/x-csv|text/csv|application/csv|application/excel|application/vnd.msexcel';
			$config['max_size']	= '100000';
			// $config['max_width']  = '302400';
			// $config['max_height']  = '306800';
			$config['file_name']  = date('His').$_FILES['userfile']['name'];
			$filecoba = $config['file_name'];

			$this->load->library('upload', $config);
			$this->upload->initialize($config);

		if (!$this->upload->do_upload()) {
			$data['error'] = $this->upload->display_errors();
			$data['q'] = $this->app_model->getdata('tbl_sync','id_sync','asc')->result();
		 	$data['q'] = $this->app_model->get_sync();
			$this->load->view('upload_sinc_view', $data);
			die($data['error']);
		} else {
			$file_data = $this->upload->data();
            $file_path = './temp_upload/'.$filecoba;
            $kode='FILE'.rand(0,1000000);
            $k = 1;
            if (($fp = fopen($file_path,'r')) !== FALSE) {
            	while(($csv_line = fgetcsv($fp,1024,",")) !== FALSE)
				{
					if($k == 1){ $k++; continue; }
				 	for ($i = 0, $j = 2; $i < $j; $i++) 
				   	{
				   		$a = explode(';', implode($csv_line));

				   		$d = substr($a[0],0,-3);
				   		$o = substr($a[5],0,-3);
						//var_dump($a); exit();
					    $insert_csv = array();      
						$insert_csv['total'] = $d;
						$insert_csv['no'] = $a[1];
						$insert_csv['tanggal_transaksi'] = $a[2];
						$insert_csv['deskripsi'] = $a[3];
						$insert_csv['keterangan'] = $a[4];
						$insert_csv['amount'] = $o;
						$insert_csv['user_pembuku_transaksi'] = $a[6];
					}

					// str_replace(',', '', $a[0]);

				   	$qw = $insert_csv['tanggal_transaksi'];
				   	$er = explode(' ', $qw);

				   	$cv = explode('/', $er[0]);
				   	$th = $cv[2];
				   	$bl = $cv[1];
				   	$tg = $cv[0];
				   	$ex = ''.$th.'-'.$bl.'-'.$tg.'';

				   	$hy = ''.$ex.' '.$er[1].'';
				   	//$gh = implode(' ', $hy);
				   	//$config['encrypt_name'] = TRUE;
					
					$data1 = array(
					    'total' => $insert_csv['total'] ,
					    'no' => $insert_csv['no'],
					    'tgl_transaksi' => $hy,
					    'deskripsi' => $insert_csv['deskripsi'] ,
					    'keterangan' => $insert_csv['keterangan'],
					    'amount' => $insert_csv['amount'],
					    'usr_pembuku_transaksi' => $insert_csv['user_pembuku_transaksi'] ,
					    'id_file' => $kode,
					    'tanggal_sinkron' => date('Y-m-d H:i:s'),
					    'deskrip_input' => $this->input->post('desk')
					    );
					//var_dump($data); die();
					$this->db->insert('tbl_sync', $data1);				
				}

     		$sess = $this->session->userdata('sess_login');
     		$result = $sess['userid'];
     		
     		$g = $this->count_row($kode)->row();

    		$isi = array(
             	'date_sync' 		=> date('Y-m-d H:i:s'),
             	'kode_file' 		=> $file_path,//$_FILES['userfile']['name'],
             	'user'				=> $result,
             	'jumlah_transaksi'	=> $g->total,
             	'id_file'			=> $kode
             	);
    			//var_dump($isi); die();
             	$this->db->insert('tbl_sync_detail', $isi);

			
            }
            $this->iuran($kode);
			
			fclose($fp) or die("can't close file");
			redirect('keuangan/Upload_sinc');
        }
	}

	function iuran($kode){
		$hasil_upload=$this->db->query('SELECT * FROM tbl_sync WHERE id_file = "'.$kode.'"')->result();

		foreach ($hasil_upload as $key) {
			$r=explode(' ', $key->deskripsi);
			$briva=$r[0];


			$noo=1;
			$debit = $key->amount;

			do {
				$outs=$this->db->query('SELECT * FROM tbl_rekap_pembayaran_mhs WHERE briva = "'.$briva.'" AND flag = 1 ')->row();
				//uang_masuk - tagihan
				$debit=($debit-($outs->credit));
				

				if ($debit >= 1 ) {
					$arr = array('debit' => $outs->credit,'balance' =>  0, 'flag' => 4);

					$this->db->where('briva',$briva)
							 ->where('flag',1)
							 ->update('tbl_rekap_pembayaran_mhs', $arr);

					$ci=$outs->idx+1;
					$this->db->query('UPDATE tbl_rekap_pembayaran_mhs SET flag = 1 WHERE briva = "'.$briva.'" AND idx = '.$ci.'');

				} elseif ($debit == 0) {
					$arr = array('debit' => $outs->credit,'balance' => 0 , 'flag' => 4);

					$this->db->where('briva',$briva)
							 ->where('flag',1)
							 ->update('tbl_rekap_pembayaran_mhs', $arr);

					$ci=$outs+1;
					$this->db->query('UPDATE tbl_rekap_pembayaran_mhs SET flag = 1 WHERE briva = "'.$briva.'" AND idx = '.$ci.'');
				//}
				}elseif ($debit < 0) {
					$arr = array('debit' => $debit,'balance' => (0-($outs->credit+$debit)) );

					$this->db->where('briva',$briva)
							 ->where('flag',1)
							 ->update('tbl_rekap_pembayaran_mhs', $arr);

					$ci=$outs+1;
					$this->db->query('UPDATE tbl_rekap_pembayaran_mhs SET flag = 1 WHERE briva = "'.$briva.'" AND idx = '.$ci.'');
				}
				

					
				$noo++;
					
				
				//var_dump($outstanding);die();
			} while ($debit > 0);

			$cek_bayar = $this->db->query('select * from tbl_rekap_pembayaran_mhs where briva = '.$briva.' AND flag = 4')->result();
			$cek_count = count($cek_bayar);

			$aa = substr($briva, 5,10);
			$nim = '20'.$aa;

			//var_dump($cek_count);die();

			if ($cek_count >= 3) {
				$this->db->query('update tbl_mahasiswa set FLAG_RENKEU = 1 where NIMHSMSMHS = '.$nim.'');
			}
			
		}

		//die($debit);
	}
}




// if ($_FILES['file']) {
// 			$this->load->helper('inflector');
// 			$nama = underscore($_FILES['file']['name']);

// 			$config['upload_path'] = '../../temp_upload';
// 			$config['allowed_types'] = 'xls|xlsx|csv';
// 			$config['max_size']	= '10000';
// 			$config['max_width']  = '10240';
// 			$config['max_height']  = '7680';
// 			$config['file_name']  = $nama;

// 			$this->upload->initialize($config);

// 				if (!$this->upload->do_upload("file")) {
// 		            $error = array('error' => $this->upload->display_errors());
// 		            echo $error['error'];
// 		        } else {
// 		      		$upload_data = $this->upload->data();
		      				
// 		      		// $this->load->library('excel');
		      			
// 		      		$objPHPExcel = PHPExcel_IOFactory::load('../../temp_upload'.$upload_data['file_name']);
// 		      		$cell_collection = $objPHPExcel->getActiveSheet()->getCellCollection(); 
		      		
// 		      		foreach ($cell_collection as $cell) {
// 		      			$column = $objPHPExcel->getActiveSheet()->getCell($cell)->getColumn();
// 		      			$row = $objPHPExcel->getActiveSheet()->getCell($cell)->getRow();
// 		      			$data_value = $objPHPExcel->getActiveSheet()->getCell($cell)->getValue();
// 		      			//header will/should be in row 1 only. of course this can be modified to suit your need.
// 		      			if ($row == 1) {
// 		      				$header[$row][$column] = $data_value;
// 		      			} else {
// 		      				$arr_data[$row][$column] = $data_value;
// 		      			}				
// 					}

// 					foreach($arr_data as $rows){
					
// 						$data['total'] = $rows["A"];
// 						$data['no'] = $rows["B"];
// 						$data['tanggal_transaksi'] = $rows["C"];
// 						$data['deskripsi'] = $rows["D"];
// 						$data['keterangan'] = $rows["E"];
// 						$data['amount'] = $rows["F"];
// 						$data['user_pembuku_transaksi'] = $rows["G"];
						
// 						$this->app_model->insertdata('tbl_sync', $data);
// 				}		
// 					// $datas['flag_cmahasiswa'] = 1;
					
// 					// $this->app_model->updatedata('tbl_calonmahasiswa','kd_registrasi', $data['kd_registrasi']);
// 			}
// 		}

/* End of file Upload_sinc.php */
/* Location: ./application/controllers/Upload_sinc.php */