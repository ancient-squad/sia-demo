<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kkn_payment extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		if (!$this->session->userdata('sess_login')) {
			redirect('auth/logout','refresh');
			exit();
		}
	}

	public function index()
	{
		// unset tahun ajaran kkn session
		$this->session->unset_userdata('tahun_ajaran_kkn');

		$data['tahunajaran'] = $this->db->get('tbl_tahunakademik')->result();
		$data['prodi'] = $this->db->get('tbl_jurusan_prodi')->result();
		$data['page'] = "v_kkn_payment";
		$this->load->view('template/template', $data);
	}

	/**
	 * Save session of tahunajaran choosed
	 * 
	 * @return void
	 */
	public function save_tahun_ajaran()
	{
		$kknSession = [
			'angkatan' => $this->input->post('angkatan'),
			'prodi' => $this->input->post('prodi'),
			'tahunajaran' => $this->input->post('tahunajaran')
		];
		$this->session->set_userdata('tahun_ajaran_kkn', $kknSession);
		redirect(base_url('keuangan/kkn_payment/student_will_pay'));
	}

	/**
	 * show all students whose would pay their KKN in choosed academic year
	 * 
	 * @return view
	 */
	public function student_will_pay()
	{
		$kknSession = $this->session->userdata('tahun_ajaran_kkn');

		// get active students
		$tahunaktif = date('Y')-7;
		$data['studentWhoHaventPay'] = $this->db->query("SELECT NMMHSMSMHS, NIMHSMSMHS, KDPSTMSMHS FROM tbl_mahasiswa
														WHERE TAHUNMSMHS = '".$kknSession['angkatan']."'
														AND KDPSTMSMHS = '".$kknSession['prodi']."'
														AND NIMHSMSMHS NOT IN (
															SELECT npm from tbl_kkn_payment 
															where tahunajaran = '".$kknSession['tahunajaran']."'
															and deleted_at IS NULL
														) ")->result();

		$data['tahunajaran'] 	= $kknSession['tahunajaran'];
		$data['angkatan'] 		= $kknSession['angkatan'];
		$data['prodi'] 			= $kknSession['prodi'];

		$data['page'] = "v_student_will_pay_kkn";
		$this->load->view('template/template', $data);
	}

	/**
	 * Save student(s) who/whose have/has pay
	 * 
	 * @return void
	 */
	public function save_payment()
	{
		// count student who has pay
		$numberofStudentWhoPay = count($this->input->post('npm'));

		for ($i=0; $i < $numberofStudentWhoPay; $i++) { 
			$npm[] = [
				'npm' => $this->input->post('npm['.$i.']'),
				'tahunajaran' => $this->input->post('tahunajaran'),
				'created_by' => $this->session->userdata('sess_login')['userid'],
				'created_at' => date('Y-m-d H:i:s')
			];
		}

		$this->db->insert_batch('tbl_kkn_payment', $npm);

		echo "<script>alert('Berhasil menyimpan!');history.go(-1);</script>";
		return;
	}

	/**
	 * Show student who has pay KKN
	 * @param string $angkatan
	 * @param string $prodi
	 * @param string $tahunajaran
	 * @return view
	 */
	public function students_has_pay($angkatan, $prodi, $tahunajaran)
	{
		$data['studentHavePay'] = $this->db->select('a.*, b.NMMHSMSMHS, b.KDPSTMSMHS')
											->from('tbl_kkn_payment a')
											->join('tbl_mahasiswa b','a.npm = b.NIMHSMSMHS')
											->where('a.tahunajaran', $tahunajaran)
											->where('b.KDPSTMSMHS', $prodi)
											->where('b.TAHUNMSMHS', $angkatan)
											->where('a.deleted_at')
											->get()->result();

		$this->load->view('v_student_has_pay', $data);
	}
	
	/**
	 * Void KKN payment for student
	 * @param string $npm
	 * @return void
	 */
	public function void_payment($npm)
	{
		$this->db->where('npm', $npm);
		$this->db->update('tbl_kkn_payment', ['deleted_at' => date('Y-m-d H:i:s')]);

		echo "<script>alert('Berhasil menghapus!');history.go(-1);</script>";
		return;
	}
	
}

/* End of file Kkn_payment.php */
/* Location: ./application/modules/keuangan/controllers/Kkn_payment.php */