<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Print_hasil extends MY_Controller {


    public function __construct()
    {
        parent::__construct();
        $this->db2 = $this->load->database('regis', TRUE);
        error_reporting(0);
        $this->load->library('Cfpdf');
    }

	function index()
	{
		$data['page'] = "v_idx_print";
		$this->load->view('tenplate', $data);
	}

	function print_hsl($id)
	{
		$data['kampret'] = $this->db2->query('SELECT * from tbl_form_pmb where nomor_registrasi = "'.$id.'"')->row();
		$this->load->view('pdf_hasil_tes',$data);
	}

	function print_hsls2($id)
	{
		$data['pret'] = $this->db2->query('SELECT * from tbl_form_pmb where nomor_registrasi = "'.$id.'"')->row();
		$this->load->view('pdf_hasil_tess2', $data);
	}

}

/* End of file Print_hasil.php */
/* Location: ./application/modules/master/controllers/Print_hasil.php */