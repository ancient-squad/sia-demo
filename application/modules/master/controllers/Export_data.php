<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Export_data extends MY_Controller {

	/*function __construct()
	{
		parent::__construct();
		if ($this->session->userdata('sess_login') != TRUE) {
		 	redirect('auth','refresh');
		}
	}*/

	function index()
	{
		//$data['mhs'] = $this->app_model->getdetail('tbl_mahasiswa','TAHUNMSMHS','2015','NIMHSMSMHS','ASC')->result();
		$this->db->select('a.*,b.prodi,c.fakultas');
		$this->db->from('tbl_mahasiswa a');
		$this->db->join('tbl_jurusan_prodi b','a.KDPSTMSMHS = b.kd_prodi');
		$this->db->join('tbl_fakultas c','c.kd_fakultas = b.kd_fakultas');
		$this->db->join('tbl_user_login d', 'a.NIMHSMSMHS = d.userid');
		$this->db->where('a.TAHUNMSMHS', '2015');
		$data['mhs'] = $this->db->get()->result();
		$this->load->view('print_excel_mhs',$data);
	}

	function mhs_fakultas($id)
	{
		$this->db->select('a.*,b.prodi,c.fakultas');
		$this->db->from('tbl_mahasiswa a');
		$this->db->join('tbl_jurusan_prodi b','a.KDPSTMSMHS = b.kd_prodi');
		$this->db->join('tbl_fakultas c','c.kd_fakultas = b.kd_fakultas');
		$this->db->join('tbl_user_login d', 'a.NIMHSMSMHS = d.userid');
		//$this->db->where('a.TAHUNMSMHS', '2015');
		$this->db->where('c.kd_fakultas', $id);
		$this->db->order_by('a.NIMHSMSMHS', 'asc');
		$data['mhs'] = $this->db->get()->result();
		$this->load->view('print_excel_mhs',$data);
	}

	function mhs_jurusan($id)
	{
		$this->db->distinct();
		$this->db->select('a.*,b.prodi,c.fakultas');
		$this->db->from('tbl_mahasiswa a');
		$this->db->join('tbl_jurusan_prodi b','a.KDPSTMSMHS = b.kd_prodi');
		$this->db->join('tbl_fakultas c','c.kd_fakultas = b.kd_fakultas');
		$this->db->join('tbl_user_login d', 'a.NIMHSMSMHS = d.userid');
		//$this->db->join('tbl_verifikasi_krs e', 'a.NIMHSMSMHS = e.npm_mahasiswa');
		//$this->db->where('a.TAHUNMSMHS', '2015');
		$this->db->where('b.kd_prodi', $id);
		$this->db->order_by('a.NIMHSMSMHS', 'asc');
		$data['mhs'] = $this->db->get()->result();
		$this->load->view('print_excel_mhs',$data);
	}

	function krs_jurusan($id)
	{
		//die('bego');exit();
		//$this->db->distinct();
		$this->db->select('a.*,g.nama_matakuliah,g.sks_matakuliah,c.NIMHSMSMHS,c.NMMHSMSMHS');
		$this->db->from('tbl_krs a');
		$this->db->join('tbl_verifikasi_krs b','a.kd_krs = b.kd_krs');
		$this->db->join('tbl_mahasiswa c','a.npm_mahasiswa = c.NIMHSMSMHS');
		$this->db->join('tbl_user_login d', 'c.NIMHSMSMHS = d.userid');
		//$this->db->join('tbl_jurusan_prodi e','c.KDPSTMSMHS = e.kd_prodi');
		//$this->db->join('tbl_fakultas f','f.kd_fakultas = e.kd_fakultas');
		$this->db->join('tbl_matakuliah g','a.kd_matakuliah = g.kd_matakuliah');
		$this->db->where('c.KDPSTMSMHS', $id);
		$this->db->where('c.TAHUNMSMHS', '2015');
		//$this->db->where('c.NIMHSMSMHS', '201210225052');
		$this->db->order_by('c.NIMHSMSMHS', 'asc');
		$data['mhs'] = $this->db->get()->result();
		//var_dump($data['mhs']);exit();
		$this->load->view('print_excel_krs_mhs',$data);
	}

	function resetpasswordnid()
	{
		/*$this->db->distinct();
		$this->db->select('a.nid');
		$this->db->from('tbl_karyawan a');
		$this->db->join('tbl_user_login b', 'a.nid = b.userid');
		$q = $this->db->get()->result();
		foreach ($q as $value) {*/
			$nid = '0011509034';
			$newpassword = '50901014300';
			$data['password_plain'] = $newpassword;
			$data['password'] = sha1(md5($newpassword).key);
			$this->app_model->updatedata('tbl_user_login','username',$nid,$data);
			//exit();
		//}
		//var_dump($q);

	}



	function randomDigits($nid){
	    $shuffled = str_shuffle($nid);
	    return $shuffled;
	}

}

/* End of file Export_data.php */
/* Location: ./application/modules/master/controllers/Export_data.php */