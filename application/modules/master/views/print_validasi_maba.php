<?php
header("Content-Type: application/xls");    
header("Content-Disposition: attachment; filename=jumlah_maba.xls");  
header("Pragma: no-cache"); 
header("Expires: 0");
?>



<table border="2">
    <thead>       
        <tr>
            <th>NO</th>
            <th>Gelombang</th>
            <th>Nomor Registrasi</th>
            <th>NPM Sementara</th>
            <th>Nama Mahasiswa</th>
            <th>Prodi</th>
            <th>Kelas</th>
            <th>Status</th>
        </tr>
    </thead>
    <tbody>
    <?php $no =1;
     foreach ($rows as $key) { ?>
        <tr>
            <td><?php echo $no; ?></td>
            <td style="text-align:center;"><?php echo $key->gelombang; ?></td>
            <td><?php echo $key->nomor_registrasi; ?></td>
            <td><?php echo $key->npm_baru;?></td>
            <td><?php echo $key->nama; ?></td>
            <td><?php echo get_jur($key->prodi); ?></td>

            <?php if ($key->kelas == 'PG'){ ?>
                <td>Pagi</td>
            <?php }elseif($key->kelas == 'SR' ){?>
                <td>Sore</td>
            <?php }elseif($key->kelas == 'KY' ){?>
                <td>Karyawan</td>
            <?php }else{?>
                <td>Tidak Ada</td>
            <?php } ?>

            <?php if ($key->status == 0){ ?>
                <td>Registrasi</td>
            <?php }elseif($key->status == 1 ){?>
                <td>Lulus</td>
            <?php }elseif($key->status == 2 ){?>
                <td>Tidak Lulus</td>
            <?php }elseif($key->status >= 3 ){?>
                <td>Daftar Ulang</td>
            <?php } ?>
            
        </tr>
    <?php $no++; } ?>
        
    </tbody>
</table>