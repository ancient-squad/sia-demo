<?php 
	$tgl1 = '2016';
	$tgl2 = date('Y');
 ?>

<div class="row">
	<div class="span12">      		  		
  		<div class="widget ">
  			<div class="widget-header">
                <i class="icon-home"></i>
  				<h3>Daftar Validasi Mahasiswa Baru</h3>
			</div>
			<div class="widget-content">
				<form class="form-horizontal" action="<?php echo base_url(); ?>master/validasi_maba/saveSessS1" method="post">
					<fieldset>
						<div class="control-group">
							<label class="control-label">Pilih Tahun</label>
							<div class="controls">
								<select class="form-control span4" name="tahun" required/>
									<option disabled="" selected="">--Pilih Tahun--</option>
									<?php for ($i = $tgl1; $i <= $tgl2 ; $i++) { ?>
										<option value="<?php echo $i; ?>"><?php echo $i; ?></option>
									<?php } ?>
								</select>
							</div>
						</div>
						<div class="form-actions">
							<input class="btn btn-large btn-success" type="submit" value="Submit">
						</div>
					</fieldset>
				</form>
			</div>
		</div>
	</div>
</div>

