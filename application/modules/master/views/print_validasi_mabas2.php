<?php
header("Content-Type: application/xls");    
header("Content-Disposition: attachment; filename=jumlah_maba.xls");  
header("Pragma: no-cache"); 
header("Expires: 0");
?>


<style>
table, td, th {
    border: 1px solid black;
}



th {
    background-color: blue;
    color: black;
}
</style>

<table border="3">
    <thead>       
        <tr>
            <th>NO</th>
            <th>Nomor Pokok Mahasiswa</th>
            <th>Nama Mahasiswa</th>
            <th>Status</th>
        </tr>
    </thead>
    <tbody>
    <?php $no =1;
     foreach ($rows as $key) { ?>
        <tr>
            <td><?php echo $no; ?></td>
            <td><?php echo $key->ID_Registrasi; ?></td>
            <td><?php echo $key->nama; ?></td>

            <?php if ($key->status == 1){ ?>
                <td>Belum Daftar Ulang</td>
            <?php }elseif($key->status >= 2 ){?>
                <td>Sudah Daftar Ulang</td>
            <?php } ?>
            <td></td>
        </tr>
    <?php $no++; } ?>
        
    </tbody>
</table>