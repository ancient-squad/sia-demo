<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Krssync extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		if (!$this->session->userdata('sess_login')) {
			redirect('auth/logout');
		}
	}

    public function index()
	{
		$data['tahunajar'] = $this->db->query("SELECT * FROM tbl_tahunakademik WHERE kode NOT IN (SELECT DISTINCT tahunakademik FROM tbl_krs_feeder) ORDER BY kode ASC")->result();
		$data['page']      = "krs_sync_v";
		$this->load->view('template/template', $data);
	}

    function transferdatatokrsfeeder(){
        //die("CALL transdatakrsfeeder('".$this->input->post('tahunajaran')."')");
		$this->db->query("CALL transdatakrsfeeder('".$this->input->post('tahunajaran')."')");
        echo '<script>alert("Sukses")</script>';
		redirect(base_url('feeder/Krssync'));
	}
}