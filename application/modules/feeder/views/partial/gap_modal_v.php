<div class="modal-header">
	<h4 class="modal-title">Selisih data <?= ucfirst($status).' '.$tahunakademik ?></h4>
</div>
<div class="modal-body">
	<table class="table table-stripped" id="example8">
		<thead>
		<tr>
			<th>No</th>
			<th>NPM</th>
			<th>Nama</th>
			<th>Sync</th>
		</tr>
		</thead>
		<tbody>
			<?php $no=1; foreach ($difference as $key => $data) : ?>
				<tr>
					<td><?= $no ?></td>
					<td><?= $data ?></td>
					<td><?= get_nm_mhs($data) ?></td>
					<td>
						<a href="<?= base_url('feeder/status_mahasiswa/unit_sync/'.$data.'/'.$status) ?>" class="btn btn-warning" data-toggle="tooltip" title="sinkron satuan">
							<i class="icon-refresh"></i>
						</a>
					</td>
				</tr>
			<?php $no++; endforeach; ?>
		</tbody>
	</table>
</div>
<div class="modal-footer">
	<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
</div>

<script>
	$("#example8").dataTable({
	    "bPaginate": false,
	    "bLengthChange": false,
	    "bFilter": true,
	    "bSort": true,
	    "bInfo": false,
	    "bAutoWidth": true
  	});
</script>

<script>
	$(document).ready(function(){
		$('[data-toggle="tooltip"]').tooltip();
	});
</script>