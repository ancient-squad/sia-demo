<table>
	<tr>
		<th>NPM</th>
		<td>:</td>
		<td><?= $akm[0]->nim ?></td>
	</tr>
	<tr>
		<th>Nama</th>
		<td>:</td>
		<td><?= $akm[0]->nama_mahasiswa ?></td>
	</tr>
</table>
<hr>

<div class="control-group">
	<label for="sks_smt" class="control-label">SKS Semester</label>
	<div class="controls">
		<input type="text" name="sks_smt" value="<?= $akm[0]->sks_semester ?>" id="sks_smt" class="" readonly="" placeholder="SKS">
	</div>
</div>

<div class="control-group">
	<label for="ips" class="control-label">IPS</label>
	<div class="controls">
		<input type="text" name="ips" value="<?= $akm[0]->ips ?>" id="ips" class="" readonly="" placeholder="IPS">
	</div>
</div>

<div class="control-group">
	<label for="sks_total" class="control-label">SKS Total</label>
	<div class="controls">
		<input type="text" name="sks_total" oninput="isValid(this)" value="<?= $akm[0]->sks_total ?>" maxlength="3" id="sks_total" class="" placeholder="SKS Total">
	</div>
</div>

<div class="control-group">
	<label for="ipk" class="control-label">IPK</label>
	<div class="controls">
		<input type="text" name="ipk" oninput="isValid(this)" value="<?= $akm[0]->ipk ?>" id="ipk" maxlength="4" class="" placeholder="IPK">
	</div>
</div>

<input type="hidden" name="id_reg_mhs" value="<?= $akm[0]->id_registrasi_mahasiswa ?>">
<input type="hidden" name="npm" value="<?= $akm[0]->nim ?>">

<script>
    // input validation
    var RegEx = new RegExp(/^\d*\.?\d*$/);

    function isValid(elem) {
        if (!RegEx.test(elem.value)) {
            elem.value = '';
        }
        if (elem.value.substr(0, 1) == '.') {
            elem.value = '';
        }
    }
</script>