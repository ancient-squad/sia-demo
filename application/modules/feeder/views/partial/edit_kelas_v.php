<form action="<?= base_url('feeder/kelas/update_kelas') ?>" method="post" class="form-horizontal">
	<input type="hidden" name="id_kelas" value="<?= $id_kelas ?>">
	<div class="modal-header">
		<h4 class="modal-title">Edit Kelas</h4>
	</div>
	<div class="modal-body">
		<div class="alert alert-warning">
          <strong>Perhatian!</strong> Setiap perubahan akan dicatat waktu dan aktor yang melakukan perubahan.
        </div>
	  	<div class="control-group">                     
		    <label class="control-label">Nama Kelas</label>
		    <div class="controls">
		      	<input type="text" value="<?= $kelas ?>" class="span3" name="kelas" maxlength="5">
		    </div>
	  	</div>    
	  </div>
	</div>
	<div class="modal-footer">
		<button type="submit" class="btn btn-warning">Update</button>
		<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	</div>
</form>