<div class="row">
  <div class="span12">                
    <div class="widget ">
      <div class="widget-header">
        <i class="icon-file"></i>
        <h3>Data Kelas Perkuliahan <?= get_thnajar($tahunajar); ?></h3>
      </div> 

      <div class="widget-content">
        <fieldset>
          <div class="alert alert-warning">
            <strong>Perhatian!</strong> Seluruh data yang ditampilkan merupakan data riil pada Feeder Dikti. Lakukan <b><u>Sinkronisasi Ulang</u></b> bila terdapat data yang belum tersinkronisasi dari SIA.
          </div>
          <a href="<?= base_url('sync_feed/kelas/view_matakuliah') ?>" class="btn btn-warning pull-left" style="margin-right: 5px">
            <i class="icon-chevron-left"></i> Kembali
          </a>
          <table id="example1" class="table table-bordered table-striped">
            <thead>
              <tr> 
                <th width="40">No</th>
                <th>Kode Mata Kuliah</th>
                <th>Nama Mata Kuliah</th>
                <th>Kelas</th>
                <th>SKS</th>
                <th>Dosen Pengajar</th>
                <th>Jumlah Peserta</th>
                <th width="90">Aksi</th>
              </tr>
            </thead>
            <tbody>
              <?php $no = 1; foreach ($data as $row) { ?>
              <tr>
                <td><?= $no; ?></td>
                <td><?= $row->kode_mata_kuliah; ?></td>
                <td><?= $row->nama_mata_kuliah; ?></td>
                <td><?= $row->nama_kelas_kuliah ?></td>
                <td><?= $row->sks ?></td>
                <td><?= $row->nama_dosen ?></td>
                <td><?= $row->jumlah_mahasiswa ?></td>
                <td>
                  <a 
                    type="button" 
                    data-toggle="tooltip" 
                    title="lihat detail kelas"
                    class="btn btn-primary" 
                    href="<?= base_url('feeder/kelas/detail/'.$row->id_kelas_kuliah) ?>">
                    <i class="icon-eye-open"></i>
                  </a>
                  <span data-toggle="tooltip" title="edit kelas">
                    <button 
                      type="button" 
                      data-toggle="modal"
                      data-target="#editModal"
                      class="btn btn-warning" 
                      onclick="get_kelas('<?= $row->id_kelas_kuliah ?>')">
                      <i class="icon-pencil"></i>
                    </button>  
                  </span>
                  </td>
              </tr>
              <?php $no++; } ?>
            </tbody>
          </table>
        </fieldset>
      </div>
    </div>
  </div>
</div>

<br><br>

<div id="editModal" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content" id="content-edit">
      <center>
        <img src="<?= base_url('assets/img/cat_load.gif') ?>" style="width: 50%;" alt="">
        <i><b>Please wait, human . . .</b></i>
      </center>
    </div>
  </div>
</div>

<script>
  function get_kelas(id_kelas) {
    $('#content-edit').load('<?= base_url('feeder/kelas/edit/') ?>' + id_kelas)
  }
</script>

