<div class="row">
  <div class="span12">                
    <div class="widget ">
      <div class="widget-header">
        <i class="icon-file"></i>
        <?php foreach ($data as $val) {
          $kelas = $val->nama_kelas_kuliah;
          $kd_matakuliah = $val->kode_mata_kuliah;
          $nama_matakuliah = $val->nama_mata_kuliah;
        } ?>
        <h3>Data Peserta Kelas <?= $kelas.' - '.$kd_matakuliah.' '.$nama_matakuliah; ?></h3>
      </div> 

      <div class="widget-content">
        <form action="<?= base_url('feeder/kelas/remove') ?>" method="post">
          <input type="hidden" name="id_kelas_feeder" value="<?= $id_kelas_feeder ?>">
          <fieldset>
            <div class="alert alert-warning">
              <strong>Perhatian!</strong> Seluruh data yang ditampilkan merupakan data riil pada Feeder Dikti. Lakukan <b><u>Sinkronisasi Ulang</u></b> bila terdapat data yang belum tersinkronisasi dari SIA.
            </div>
            <a href="<?= base_url('feeder/kelas') ?>" class="btn btn-warning" style="margin-right: 5px">
              <i class="icon-chevron-left"></i> Kembali
            </a>
            <button type="submit" class="btn btn-danger pull-right" style="margin-right: 5px">
              <i class="icon-trash"></i> Hapus Peserta
            </button>
            <table id="example3" class="table table-bordered table-striped">
              <thead>
                <tr> 
                  <th>No</th>
                  <th>NPM</th>
                  <th>Nama Mahasiswa</th>
                  <th>Angkatan</th>
                  <th>Prodi</th>
                  <th>Hapus</th>
                </tr>
              </thead>
              <tbody>
                <?php $no = 1; foreach ($data as $row) { ?>
                <tr>
                  <td><?= $no; ?></td>
                  <td><?= $row->nim; ?></td>
                  <td><?= $row->nama_mahasiswa; ?></td>
                  <td><?= $row->angkatan ?></td>
                  <td><?= $row->nama_program_studi ?></td>
                  <td style="text-align: center;">
                    <input type="checkbox" class="control" name="npm[<?= $row->nim ?>]" value="<?= $row->nim ?>">
                  </td>
                </tr>
                <?php $no++; } ?>
              </tbody>
            </table>
          </fieldset>
        </form>
      </div>
    </div>
  </div>
</div>

<br><br>

