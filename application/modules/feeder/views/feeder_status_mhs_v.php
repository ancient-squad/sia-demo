<div class="row">
  <div class="span12">                
    <div class="widget ">
      <div class="widget-header">
        <i class="icon-group"></i>
        <h3>Data Status Mahasiswa <?= ucfirst($status) ?> <?= get_thnajar($tahunajar); ?></h3>
      </div> 

      <div class="widget-content">
        <form method="post" action="<?= base_url('feeder/status_mahasiswa/get_gap/'.$status) ?>">
          <fieldset>
            <div class="alert alert-warning">
              <strong>Perhatian!</strong> Seluruh data yang ditampilkan merupakan data riil pada Feeder Dikti. Lakukan <b><u>Sinkronisasi Ulang</u></b> bila terdapat data yang belum tersinkronisasi dari SIA.
            </div>
            <a href="<?= base_url('sync_feed/status_mhs/load_data') ?>" class="btn btn-warning pull-left" style="margin-right: 5px">
              <i class="icon-chevron-left"></i> Kembali
            </a>
            <button type="submit" class="btn btn-primary pull-left"  style="margin-right: 5px">
              <i class="icon-refresh"></i> Sinkronisasi Ulang
            </button>
            <span data-toggle="tooltip" title="lihat data yang belum tersinkronisasi">
              <button 
                type="button" 
                data-toggle="modal" 
                data-target="#gapModal" 
                class="btn btn-default pull-left" 
                onclick="get_gap('<?= $status ?>')">
                <i class="icon-pause"></i> Selisih Data
              </button>
            </span>
            <table id="example4" class="table table-bordered table-striped">
              <thead>
                <tr> 
                  <th width="40">No</th>
                  <th>NPM</th>
                  <th>NAMA</th>
                  <th>IPK</th>
                  <th>SKS Total</th>
                  <th width="40">Edit</th>
                </tr>
              </thead>
              <tbody>
                <?php $no = 1; foreach ($data as $row) { ?>
                <tr>
                  <td><?= $no; ?></td>
                  <td>
                    <?= $row->nim; ?>
                    <input type="hidden" name="mhs[<?= $no ?>]" value="<?= $row->nim; ?>">
                  </td>
                  <td><?= $row->nama_mahasiswa; ?></td>
                  <td><?= !is_null($row->ipk) ? $row->ipk : '-'; ?></td>
                  <td><?= !is_null($row->sks_total) ? $row->sks_total : '-'; ?></td>
                  <td>
                    <button 
                      type="button" 
                      data-toggle="modal" 
                      data-target="#editModal" 
                      class="btn btn-warning" 
                      onclick="load_edit('<?= $row->id_registrasi_mahasiswa ?>','<?= $row->nim ?>','<?= $status ?>')">
                      <i class="icon-pencil"></i>
                    </button>
                  </td>
                </tr>
                <?php $no++; } ?>
              </tbody>
            </table>
          </fieldset>
        </form>
      </div>
    </div>
  </div>
</div>

<br><br>

<div id="gapModal" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content" id="content">
      <center>
        <img src="<?= base_url('assets/img/cat_load.gif') ?>" style="width: 50%;" alt="">
        <i><b>Please wait, human . . .</b></i>
      </center>
    </div>
  </div>
</div>

<div id="editModal" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content" id="content-edit">
      
    </div>
  </div>
</div>

<script type="text/javascript">
  function get_gap(status) {
    $.ajax({
      url: '<?= base_url('feeder/status_mahasiswa/difference/') ?>' + status,
      type: 'POST',
      success: function(res) {
        $('#content').load('<?= base_url('feeder/status_mahasiswa/difference/') ?>' + status);
      }
    })
  }

  function load_edit(id_reg_mhs, npm, status) {
    $('#content-edit').load('<?= base_url('feeder/status_mahasiswa/edit_on_feeder/') ?>'+id_reg_mhs+'/'+npm+'/'+status);
  }

  $('#form1').submit(function(e) {
    e.preventDefault();

    $(".progress-bar").attr("aria-valuenow", 0);
    $(".progress-bar").css("width", 0+"%");
    $(".progress-bar").html(0 +"%");

    $.ajax({
      type: 'POST',
      url: "<?= base_url('feeder/status_mahasiswa/get_gap') ?>",
      data: $(this).serialize(),
      xhr: function(){
        //upload Progress
        var xhr = $.ajaxSettings.xhr();
        if (xhr.upload) {
          xhr.upload.addEventListener('progress', function(event) {
            var percent = 0;
            var position = event.loaded || event.position;
            var total = event.total;
            if (event.lengthComputable) {
              percent = Math.ceil(position / total * 100);
            }
            console.log(percent)
            $(".progress-bar").attr("aria-valuenow", percent);
            $(".progress-bar").css("width", percent+"%");
            $(".progress-bar").html(percent +"%");

          }, true);
        }
        return xhr;
      }
    })
  })
</script>

