<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Lokasi extends MY_Controller {

	function __construct()
	{
		parent::__construct();
		//$id_menu = 32 (database); cek apakah user memiliki akses
		if ($this->session->userdata('sess_login') == TRUE) {
			 // $akses = $this->role_model->cekakses(51)->result();
			 // if ($akses != TRUE) {
			 // 	redirect('home','refresh');
			 // }
		} else {
			redirect('auth','refresh');
		}
	}

	function index()
	{	

		$data['isakol']=$this->app_model->getdata('tbl_lembaga','id_lembaga','asc')->result();
		$data['lokasi']=$this->app_model->get_lokasi();
		$data['page'] = 'organisasi/lokasi_view';
		$this->load->view('template/template',$data);
	}

	function save_gedung()
	{
		$data=array(
		'gedung' 		=> $this->input->post('gedung'),
		'kode_lembaga'	=> $this->input->post('loks')
		);
		$this->app_model->insertdata('tbl_gedung',$data);
		echo "<script>alert('Sukses');
		document.location.href='".base_url()."organisasi/lokasi';</script>";
	}

	function update_gedung()
	{
		$data=array(
		'gedung'		=> $this->input->post('gedung'),
		'kode_lembaga'	=> $this->input->post('loks')
		);
		$this->app_model->updatedata('tbl_gedung','id_gedung',$this->input->post('id_gedung'),$data);
		echo "<script>alert('Sukses');
		document.location.href='".base_url()."organisasi/lokasi';</script>";
	}

	function loaddata($edg)
	{
		$data['isako']=$this->app_model->getdata('tbl_lembaga','id_lembaga','asc')->result();
		$data['list'] = $this->db->query("select * from tbl_gedung where id_gedung = '$edg'")->row();
		$this->load->view('organisasi/edit_gedung_view',$data);
		// $data['lokasi'] = $this->app_model->getdata('tbl_gedung','id_gedung','asc');
		// $this->load->view('organisasi/lokasi_view', $data);
	}

	function del_lokasi($id)
	{
		$this->app_model->deletedata('tbl_gedung','id_gedung',$id);
		echo "<script>alert('Berhasil');
		document.location.href='".base_url()."organisasi/lokasi';</script>";
	}

	function detail_gd($id)
	{
		$data['gedung']=$this->app_model->getdetail('tbl_gedung','id_gedung',$id,'id_gedung','asc')->row($id);
		// $data['ge'] = $this->app_model->getdata('tbl_gedung','id_gedung','asc')->result();
		//$data['w']=$this->app_model->getdetail('tbl_ruangan','id_ruangan',$id,'id_ruangan','asc')->result();
		$data['w']=$this->app_model->get_lantai($id);
		$data['page']="lantai_view";
		$this->load->view('template/template', $data);
	}

	

}

/* End of file mahasiswa.php */
/* Location: ./application/modules/data/controllers/mahasiswa.php */