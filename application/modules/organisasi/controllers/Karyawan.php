<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Karyawan extends MY_Controller {


	// function __construct()
	// {
	// 	// parent::__construct();
	// 	// $this->load->library('Cfpdf');
	// 	// //error_reporting(0);
	// 	// if ($this->session->userdata('sess_login') == TRUE) {
	// 	// 	$cekakses = $this->role_model->cekakses(71)->result();
	// 	// 	if ($cekakses != TRUE) {
	// 	// 		echo "<script>alert('Anda Tidak Berhak Mengakses !!');history.go(-1);</script>";
	// 	// 	}
	// 	// } else {
	// 	// 	redirect('auth','refresh');
	// 	// }
	// }
	
	public function index()
	{
		$data['query'] = $this->db->query('SELECT * from tbl_karyawan_2')->result();
		$data['page']	= "v_kary";
		$this->load->view('template/template', $data);
	}

	function add()
	{
		$data = array(
			'nip'		=> $this->input->post('nip'),
			'nama'		=> $this->input->post('nama'),
			'unit'		=> $this->input->post('unit'),
			'jabatan'	=> $this->input->post('jabatan'),
			'status'	=> $this->input->post('status'),
			'no_hp'		=> $this->input->post('hp'),
			'no_ktp'	=> $this->input->post('ktp'),
			'email'		=> $this->input->post('email'),
			'alamat'	=> $this->input->post('alamat') ,
			'th_masuk'	=> $this->input->post('thms')
			);
		//var_dump($data);exit();
		$this->app_model->insertdata('tbl_karyawan_2',$data);
		echo "<script>alert('Berhasil');
		document.location.href='".base_url()."organisasi/karyawan/';</script>";
	}

	function load($edg)
	{
		$data['kue'] = $this->db->query('SELECT * from tbl_karyawan_2 where id = "'.$edg.'"')->row();
		$this->load->view('v_edit', $data);
	}

	function edit()
	{
		$data = array(
			'nip'		=> $this->input->post('nip'),
			'nama'		=> $this->input->post('nama'),
			'unit'		=> $this->input->post('unit'),
			'jabatan'	=> $this->input->post('jabatan'),
			'status'	=> $this->input->post('status'),
			'no_hp'		=> $this->input->post('hp'),
			'no_ktp'	=> $this->input->post('ktp'),
			'email'		=> $this->input->post('email'),
			'alamat'	=> $this->input->post('alamat') ,
			'th_masuk'	=> $this->input->post('thms')
			);
		//var_dump($data);exit();
		$this->app_model->updatedata('tbl_karyawan_2','id',$this->input->post('iden'),$data);
		echo "<script>alert('Berhasil');
		document.location.href='".base_url()."organisasi/karyawan/';</script>";
	}

	function delete($id)
	{
		$this->app_model->deletedata('tbl_karyawan_2','id',$id);
		echo "<script>alert('Berhasil');
		document.location.href='".base_url()."organisasi/karyawan';</script>";
	}

	function detail($yuk)
	{
		$data['wew'] = $this->db->query('SELECT * from tbl_karyawan_2 where id = "'.$yuk.'"')->row();
		$this->load->view('v_detil_kry', $data);
	}

}

/* End of file Karyawan.php */
/* Location: ./application/modules/organisasi/controllers/Karyawan.php */