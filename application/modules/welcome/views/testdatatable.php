<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">

<script type="text/javascript" src="https://code.jquery.com/jquery-1.12.4.js"></script>

<script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js" type="text/javascript"></script>

<script type="text/javascript">
	$(document).ready(function() {
	    $('#example').dataTable( {
	        processing: true,
	        serverSide: true,
	        ajax: {
	        	url: "<?= base_url(); ?>welcome/test_function/dataTableServerSide",
	        	type: "POST"
	        }
	    });
	});
</script>

<table id="example">
	<thead>
		<tr>
			<td>id krs</td>
			<td>npm mahasiswa</td>
			<td>kode krs</td>
			<td>kode mk</td>
			<td>kode jadwal</td>
			<td>Aksi</td>
		</tr>
	</thead>
</table>
