<?php
header("Content-Type: application/xls");    
header("Content-Disposition: attachment; filename=danu.xls");  
header("Pragma: no-cache"); 
header("Expires: 0");
//die('kaskus');
?>


<style>
	table, td, th {
	    border: 1px solid black;
	}

	th {
	    background-color: blue;
	    color: black;
	}
</style>

<table>
    <thead>
        <tr> 
            <th>No</th>
            <th>NIM</th>
            <th>NAMA</th>
            <th>IPS</th>
            <th>KODE MK</th>
            <th>NAMA MK</th>
            <th>BOBOT</th>
        </tr>
    </thead>
     <tbody>
        <?php  $no = 1;  foreach ($mhs as $value) { $ips_nr=0; ?>
        <?php   $hitung_ips = $this->db->query('SELECT distinct a.`NIMHSTRLNM`,a.`KDKMKTRLNM`,a.`NLAKHTRLNM`,a.`BOBOTTRLNM`,b.`sks_matakuliah` FROM tbl_transaksi_nilai a
                JOIN tbl_matakuliah b ON a.`KDKMKTRLNM` = b.`kd_matakuliah`
                WHERE a.`kd_transaksi_nilai` IS NOT NULL AND kd_prodi = "62201" AND NIMHSTRLNM = "'.$value->NIMHSTRLNM.'" and THSMSTRLNM = "20152" ')->result();

                $st=0;
                $ht=0;
                foreach ($hitung_ips as $iso) {
                    $h = 0;


                    $h = $iso->BOBOTTRLNM * $iso->sks_matakuliah;
                    $ht=  $ht + $h;

                    $st=$st+$iso->sks_matakuliah;
                }

                $ips_nr = $ht/$st; ?>
        <?php $oncom = $this->db->query('SELECT DISTINCT COUNT(KDKMKTRLNM) AS totalrow FROM tbl_transaksi_nilai where NIMHSTRLNM = "'.$value->NIMHSTRLNM.'" and THSMSTRLNM = 20152')->row()->totalrow; ?>
        <tr>
            <td rowspan="<?php echo $oncom; ?>"><?php echo $no; ?></td>
            <td rowspan="<?php echo $oncom; ?>"><?php echo $value->NIMHSTRLNM; ?></td>
            <?php $mhsnm = $this->db->query('SELECT NMMHSMSMHS FROM tbl_mahasiswa where NIMHSMSMHS = "'.$value->NIMHSTRLNM.'"')->row()->NMMHSMSMHS; ?>
            <td rowspan="<?php echo $oncom; ?>"><?php echo $mhsnm; ?></td>
            <td rowspan="<?php echo $oncom; ?>"><?php echo number_format($ips_nr,2); ?></td>
            <?php $mk = $this->db->query('SELECT KDKMKTRLNM,NLAKHTRLNM FROM tbl_transaksi_nilai where NIMHSTRLNM = "'.$value->NIMHSTRLNM.'" and THSMSTRLNM = 20152')->result(); foreach ($mk as $key) { 
                  $nama_matakuliah = $this->db->query('SELECT DISTINCT nama_matakuliah FROM tbl_matakuliah where kd_matakuliah = "'.$key->KDKMKTRLNM.'" and kd_prodi = 62201')->row()->nama_matakuliah;
                ?>
                <td><?php echo $key->KDKMKTRLNM; ?></td><td><?php echo $nama_matakuliah; ?></td><td><?php echo $key->NLAKHTRLNM; ?></td></tr>
            <?php } ?>
        </tr>
        <?php $no++; $ips_nr = 0; } ?>
    </tbody>
   
</table>