<?php
$pdf = new FPDF("P","mm", "A4");
//var_dump($mahasiswa);die();
foreach ($mahasiswa as $key) {
	//var_dump($detail_khs);exit();




$pdf->AliasNbPages();
$pdf->AddPage();


$pdf->SetMargins(3, 5 ,0);

$pdf->SetFont('Arial','',22); 



$pdf->image(base_url().'/assets/logo.gif',10,10,20);

$pdf->Ln(0);

$pdf->Cell(200,5,'KARTU HASIL STUDI MAHASISWA',0,3,'C');

$pdf->Ln(2);

$pdf->Cell(200,10,'UNIVERSITAS BHAYANGKARA',0,5,'C');

$pdf->Ln(0);

$pdf->Cell(200,10,'JAKARTA RAYA',0,1,'C');

$pdf->Ln(3);

$pdf->Cell(250,0,'',1,1,'C');


$q=$this->db->query('SELECT 
  a.`NIMHSMSMHS`,
  a.`NMMHSMSMHS`,
  a.`TAHUNMSMHS`,
  a.`SMAWLMSMHS`,
  b.`kd_prodi`,
  b.`prodi`,
  c.`fakultas`,
  e.`kd_krs`,
  e.`id_pembimbing`,
  f.`nama`,
  f.`nid`,
  b.`kaprodi` 
FROM
  tbl_mahasiswa a 
  LEFT JOIN tbl_jurusan_prodi b 
    ON a.`KDPSTMSMHS` = b.`kd_prodi` 
  LEFT JOIN tbl_fakultas c 
    ON c.`kd_fakultas` = b.`kd_fakultas` 
  JOIN tbl_verifikasi_krs e 
    ON e.`npm_mahasiswa` = a.`NIMHSMSMHS`  
  LEFT JOIN tbl_karyawan f 
    ON e.`id_pembimbing` = f.`nid` 
WHERE e.`npm_mahasiswa` = "'.$key->halaman.'" 
  AND e.tahunajaran = "20161"')->row();

$kaprodi=$this->db->query('SELECT * FROM tbl_jurusan_prodi
											WHERE kd_prodi = "' . $q->kd_prodi . '"')->row()->kaprodi;		


$pdf->ln(6);

$pdf->SetFont('Arial','',10); 

$pdf->Cell(40,5,'Fakultas',0,0);

$pdf->Cell(10,5,':',0,0,'C');

$pdf->Cell(60,5,$q->fakultas,0,0,'L');

$pdf->Cell(40,5,'Semester',0,0);

$pdf->Cell(5,5,':',0,0,'L');
$a = $this->app_model->get_semester_khs($q->SMAWLMSMHS,substr($q->kd_krs, 12,5));

	//$ips = $this->app_model->get_ips_mahasiswa($q->NIMHSMSMHS,$a )->row()->ips;

// 	$hitung_ips = $this->db->query('SELECT a.`NIMHSTRLNM`,a.`KDKMKTRLNM`,a.`NLAKHTRLNM`,a.`BOBOTTRLNM`,b.`sks_matakuliah` FROM tbl_transaksi_nilai a
// JOIN tbl_matakuliah b ON a.`KDKMKTRLNM` = b.`kd_matakuliah`
// WHERE a.`kd_transaksi_nilai` IS NOT NULL AND kd_prodi = '.$prodi.' AND NIMHSTRLNM = "'.$q->NIMHSMSMHS.'" and THSMSTRLNM = "'.substr($q->kd_krs, 12,5).'"')->result();

// $st=0;
// $ht=0;
// 		foreach ($hitung_ips as $iso) {
// 			$h = 0;


// 			$h = $iso->BOBOTTRLNM * $iso->sks_matakuliah;
// 			$ht=  $ht + $h;

// 			$st=$st+$iso->sks_matakuliah;
// 		}

// 		$ips_nr = $ht/$st;
		//$ipk = $this->app_model->get_ipk_mahasiswa($q->NIMHSMSMHS)->row()->ipk;

$pdf->Cell(60,5,$a,0,0,'L');


$pdf->ln(3);

$pdf->SetFont('Arial','',10); 

$pdf->Cell(40,10,'Program Studi',0,0);

$pdf->Cell(10,10,':',0,0,'C');

$pdf->Cell(60,10,$q->prodi,0,0,'L');

$pdf->Cell(40,10,'Tahun Akademik',0,0);

$pdf->Cell(5,5,':',0,0,'L');

$pdf->Cell(50,10,'2016/2017 - GANJIL',0,0,'L');


$pdf->ln(10);

$pdf->SetFont('Arial','B',10); 

$pdf->Cell(40,10,'Identitas Mahasiswa',0,0);

$pdf->Cell(10,10,'',0,0,'C');

$pdf->Cell(60,10,'',0,0,'C');

$pdf->Cell(40,10,'Identitas Dosen PA',0,0);

$pdf->Cell(10,10,'',0,0,'L');

$pdf->Cell(60,10,'',0,0,'L');


$pdf->ln(5);

$pdf->SetFont('Arial','',10); 

$pdf->Cell(40,10,'Nama Mahasiswa',0,0);

$pdf->Cell(10,10,':',0,0,'C');
$current_y = $pdf->GetY();
$current_x = $pdf->GetX();

$cell_width = 60;
$pdf->MultiCell($cell_width, 5, $q->NMMHSMSMHS, 0,'L',false);

$pdf->ln(5);

$x = 60;
$pdf->SetXY($current_x + $x, $current_y);
//$pdf->Cell(60,10,$q->NMMHSMSMHS,0,0,'L');

$pdf->Cell(40,10,'Nama Dosen PA',0,0);

$pdf->Cell(5,5,':',0,0,'L');

//$pdf->Cell(60,10,$q->nama,0,0,'L')MultiCell(60, 10, $q->nama, 0,'L');
$pdf->MultiCell(50, 5, $q->nama, 0,'L');


$pdf->ln(7);

$pdf->SetFont('Arial','',10); 

$pdf->Cell(40,5,'NPM',0,0);

$pdf->Cell(10,5,':',0,0,'C');

$pdf->Cell(60,5,$q->NIMHSMSMHS,0,0,'L');

$pdf->Cell(40,5,'NIDN',0,0);

$pdf->Cell(5,5,':',0,0,'L');

$pdf->Cell(60,5,$q->nid,0,0,'L');



$pdf->SetFont('Arial','',10); 

$pdf->Cell(40,5,'',0,0);

$pdf->Cell(10,5,'',0,0,'C');

$pdf->Cell(60,5,'',0,0,'C');

$pdf->SetFont('Arial','',12);

$pdf->Cell(40,5,'',0,0);

$pdf->Cell(10,5,'',0,0,'C');

$pdf->Cell(60,5,'',0,1,'L');


$pdf->SetFont('Arial','',10); 

$pdf->Cell(40,5,'IPS/IPK',0,0);

$pdf->Cell(10,5,':',0,0,'C');

$ips = $this->app_model->getipsmhs($prodi,$q->NIMHSMSMHS,substr($q->kd_krs, 12,5));
if (substr($q->NIMHSMSMHS, 0,4) > 2014) {
    $ipk = $this->app_model->getipkmhs($q->NIMHSMSMHS,$prodi);
    $pdf->Cell(60,5,$ips.'/'.$ipk,0,0,'L');
}elseif ($a > 1) {
    $pdf->Cell(60,5,$ips.'/ - ',0,0,'L');
}


$pdf->ln(10);


// $pdf->SetFont('Arial','',10); 

// $pdf->Cell(40,5,'SKS TERAMBIL',0,0);

// $pdf->Cell(10,5,':',0,0,'C');

// $pdf->Cell(60,5,'',0,0,'C');

// $pdf->Cell(40,5,'Jumlah SKS Lulus',0,0);

// $pdf->Cell(5,5,':',0,0,'C');

// $pdf->Cell(60,5,'',0,0,'L');


$pdf->ln(10);

$pdf->SetLeftMargin(10);

$pdf->SetFont('Arial','',10); 

$pdf->Cell(15,8,'NO',1,0,'C');

$pdf->Cell(45,8,'KODE MATAKULIAH',1,0,'C');

$pdf->Cell(100,8,'NAMA MATAKULIAH',1,0,'C');

$pdf->Cell(15,8,'SKS',1,0,'C');

$pdf->Cell(15,8,'NILAI',1,1,'C');

$no=1;

$total_sks=0;

$krs = $this->db->query('SELECT * FROM tbl_krs a 
		JOIN tbl_matakuliah b ON a.`kd_matakuliah` = b.`kd_matakuliah` where a.`npm_mahasiswa` = "'.$key->halaman.'" and kd_prodi = '.$prodi.' and kd_krs like "'.$key->halaman.'20161%"')->result();

$no = 1; foreach ($krs as $row) {

	$pdf->Cell(15,8,number_format($no),1,0,'C');

	$pdf->Cell(45,8,$row->kd_matakuliah,1,0,'C');

	$pdf->SetFont('Arial','',8);

	$pdf->Cell(100,8,$row->nama_matakuliah,1,0,'L');
	//$pdf->MultiCell(50, 5, $q->nama, 0,'L');

	$pdf->SetFont('Arial','',10); 
	$pdf->Cell(15,8,$row->sks_matakuliah,1,0,'C');

	

	$adanilai = $this->db->query('SELECT * FROM tbl_transaksi_nilai WHERE NIMHSTRLNM  = '.$q->NIMHSMSMHS.' AND KDKMKTRLNM = "'.$row->kd_matakuliah.'" AND THSMSTRLNM = "'.substr($q->kd_krs, 12,5).'"')->row();

	
	if (($adanilai->NLAKHTRLNM != '') OR ($adanilai->NLAKHTRLNM == null)) {
		//$que = $this->db->query('SELECT * FROM tbl_transaksi_nilai WHERE NIMHSTRLNM  = '.$q->NIMHSMSMHS.' AND KDKMKTRLNM = "'.$row->kd_matakuliah.'"')->row();
		$pdf->Cell(15,8,$adanilai->NLAKHTRLNM,1,1,'C');		
	}else {
		$pdf->Cell(15,8,'-',1,1,'C');
	}

	$total_sks = $total_sks + $row->sks_matakuliah;

	$no++;

}


$pdf->Cell(160,8,'Total',1,0,'C');

$pdf->Cell(15,8,$total_sks,1,0,'C');

$pdf->Cell(15,8,'',1,1,'C');


$pdf->ln(8);

$pdf->SetMargins(3, 5 ,0);

$pdf->SetFont('Arial','',12);

date_default_timezone_set('Asia/Jakarta'); 

$pdf->Cell(170,5,'Jakarta,'.date('d-m-Y').'',0,0,'R');


$pdf->ln(8);

$pdf->SetFont('Arial','B',12); 

$pdf->Cell(20,5,'MAHASISWA',0,0);

$pdf->Cell(75,5,'',0,0,'C');

$pdf->Cell(10,5,'',0,0,'C');

$pdf->Cell(30,5,'KETUA PROGRAM STUDI',0,0);

$pdf->Cell(60,5,'',0,0,'L');

$pdf->Cell(10,5,'',0,1,'C');



$pdf->ln(10);

$pdf->SetFont('Arial','',12); 

$pdf->Cell(20,5,'Nama',0,0);

$pdf->Cell(5,5,':',0,0,'C');

$current_y = $pdf->GetY();
$current_x = $pdf->GetX();

$cell_width = 80;
$pdf->MultiCell($cell_width, 5, $q->NMMHSMSMHS, 0,'L',false);
//$pdf->Cell(80,5,,0,0,'C');

$pdf->ln(5);

$x = 80;
$pdf->SetXY($current_x + $x, $current_y);

//$pdf->Cell(80,5,$q->NMMHSMSMHS,0,0,'C');

switch ($kd_prodi) {
    case "70201":
        $nama = 'Nurul Fauziah, S.Sos, M.I.Kom';
        $nidpd = '0041509033';
        break;
    case "73201":
        $nama = 'Yunike Balsa Rhapsodia, M.Psi., Psikolog';
        $nidpd = '051507026';
        break;
    case "26201":
        $nama = 'Denny Siregar, ST. M.Sc';
        $nidpd = '020409008';
        break;
    case "24201":
        $nama = 'Ir, Hernowo Widodo, MT';
        $nidpd = '021503036';
        break;
    case "55201":
        $nama = 'Sugiyatno, S.Kom., M.Kom';
        $nidpd = '021711100';
        break;
    case "32201":
        $nama = 'Abdullah Rizky Agusman, ST, MT';
        $nidpd = '021512057';
        break;
    case "25201":
        $nama = 'Ir. Agus Setyono, M.Si';
        $nidpd = '021503038';
        break;
    case "74201":
        $nama = 'Elfirda Ade Putri, S.H., M.H';
        $nidpd = '011512043';
        break;
    case "62201":
        $nama = 'Tutiek Yoganingsih, SE, M.Si';
        $nidpd = '039709004';
        break;
    case "61201":
        $nama = 'M. Fadhli Nursal, SE, MM';
        $nidpd = '031407021';
        break;
    case "61101":
        $nama = 'Dr. Hj. Beti Nurbaiti, STP., ME.';
        $nidpd = '031407022';
        break;
    case "74101":
        $nama = 'Dr. Awaludin Marwan, S.H., M.H., M.A.'; 
        $nidpd = '011901082';
        break;
    case "86206":
        $nama = 'Husna Farhana, SE., M.Pd';
        $nidpd = '061708004';
        break;
    case "85202":
        $nama = 'Eskar Tri Denatara, S.Pd., M.Pd';
        $nidpd = '061709051';
        break;
}

$pdf->Cell(20,5,'Nama',0,0);

$pdf->Cell(5,5,':',0,0,'L');

$pdf->MultiCell(80, 5,nama_dsn($kaprodi), 0,'L');
//$pdf->Cell(80,5,$q->nama,0,0,'L');
$pdf->ln();
$pdf->SetFont('Arial','',12); 

$pdf->Cell(20,5,'NPM',0,0);

$pdf->Cell(5,5,':',0,0,'C');

$pdf->Cell(80,5,$q->NIMHSMSMHS,0,0,'L');

$pdf->Cell(20,5,'NIDN',0,0);

$pdf->Cell(5,5,':',0,0,'L');

$pdf->Cell(80,5,$kaprodi,0,0,'L');

}

$pdf->Output('KHS.PDF','I');



?>

