<?php
header("Content-Type: application/xls");    
header("Content-Disposition: attachment; filename=nganu.xls");  
header("Pragma: no-cache"); 
header("Expires: 0");
//die('kaskus');
?>


<style>
	table, td, th {
	    border: 1px solid black;
	}

	th {
	    background-color: blue;
	    color: black;
	}

    
</style>

<table>
    <thead>
        <tr> 
            <th>No</th>
            <th>NIM</th>
            <th>NAMA</th>
            <th>KODE MK</th>
            <th>NAMA MK</th>
        </tr>
    </thead>
     <tbody>
        <?php  $no = 1; foreach ($mhs as $value) { ?>
       
        <?php $oncom = $this->db->query('SELECT DISTINCT COUNT(kd_matakuliah) AS totalrow FROM tbl_krs a join tbl_verifikasi_krs b on a.kd_krs=b.kd_krs where a.npm_mahasiswa = "'.$value->npm_mahasiswa.'" and b.tahunajaran = 20152')->row()->totalrow; ?>
        <tr>
            <td rowspan="<?php echo $oncom; ?>" style="vertical-align: middle;"><?php echo $no; ?></td>
            <td rowspan="<?php echo $oncom; ?>" style="vertical-align: middle;"><?php echo $value->npm_mahasiswa; ?></td>
            <?php $mhsnm = $this->db->query('SELECT NMMHSMSMHS FROM tbl_mahasiswa where NIMHSMSMHS = "'.$value->npm_mahasiswa.'"')->row()->NMMHSMSMHS; ?>
            <td rowspan="<?php echo $oncom; ?>" style="vertical-align: middle;"><?php echo $mhsnm; ?></td>
            <?php $mk = $this->db->query('SELECT kd_matakuliah FROM tbl_krs a join tbl_verifikasi_krs b on a.kd_krs=b.kd_krs where a.npm_mahasiswa = "'.$value->npm_mahasiswa.'" and b.tahunajaran = 20152')->result();
                foreach ($mk as $key) { 
                  $nama_matakuliah = $this->db->query('SELECT DISTINCT nama_matakuliah FROM tbl_matakuliah where kd_matakuliah = "'.$key->kd_matakuliah.'" and kd_prodi = 32201')->row()->nama_matakuliah;
                ?>
                <td><?php echo $key->kd_matakuliah; ?></td>
                <td><?php echo $nama_matakuliah; ?></td>
            </tr>
            <?php } ?>
        </tr>
        <?php $no++; } ?>
    </tbody>
   
</table>