<html>
<head>
	<title>SKEP</title>
</head>
<body  style="background: url("http://subtlepatterns2015.subtlepatterns.netdna-cdn.com/patterns/swirl_pattern.png") repeat scroll 0% 0% transparent;">
	<style type="text/css">
		.left-corner>h3 {
			font-size: 12;
			font-family: calibri;
			margin-left: 25%;
		}

		.header>h3 {
			font-size: 12;
		}

		.header>hr {
			width: 5%;
		}

		.image {
			width: 8%;
		}

		.tbl-content {
			margin: 0 auto;
		}

		.container {
			width: 50%;
			margin: 0 auto;
		}

		td {
			vertical-align: top;
		}
	</style>

	<div class="left-corner">
		<center><h3>UNIVERSITAS ABCD</h3></center>
	</div>

	<center>
		<div class="header">
			<img src="https://lh3.googleusercontent.com/MtljsGcdgKJvySdeAR6wekfH2ZMaTYBERKfIgESmXn3lVlZT2W6HZfOmuACkh5hJz4SpFko5GsQmnW0mKf2QuJWzHSqsihvTKEXgNuKEuokG-nCt3lp32CJGbyNfu6YHwFuIHUCIXG_7eOakyuWm0nuUVKaShVGnf3o1wC_PEHFUepUCPGU1qWWjItEQ-aO__O8nmDCyIuYV6HhExY1Kwlduw7ZAO2KRzd4k2B465_lcaHT4kZQLDiuURWKWWEVvL6zfC_DanBaRn_HiKgwLgOY3CIPn9aWl-z5MAp99_at3QLXmqDL17Jp2YW63caAKNHTMN4RnvZgQvV5b1SWAwXlAdq8hDJe_KEdTMG2PBL5Zk84iRjYXIUBAw6qFb1xyKmhKi_j6J2GztK66TQYYEnyG5gUyA-2TDKYzrl1GWSjczGvGEFQRzVCrKjnq4o6EcMqEZgk0_I4UZDXj8IM8Mw_O2FiK4sVbqtxf8fcyxk7QAuntBBOKLM_qqd5bPETUsy-yecTybEify4kEopNzTl3fqo5ffneHv7pSJxgHHaILAL-uuNsmSPg0uEN6a0RKl_62=s491-no" class="image" alt="">
			<h3><u><b>PENGUMUMAN</b></u></h3>
			<h3>Nomor : PENG/10/XI/2015/BAA-<?= $this->ORG_NAME ?></h3>
			<h3>TATA TERTIB UJIAN AKHIR SEMESTER</h3>
		</div>
	</center>

	<div class="container">
		<h4>Peserta Ujian Diwajibkan</h4>
		<table class="tbl-content">
			<tr>
				<td>1.</td>
				<td colspan="3">Menandatangani Daftar Hadir Ujian (DHU)</td>
			</tr>
			<tr>
				<td>2.</td>
				<td colspan="3">Memeperlihatkan kartu ujian kepada pengawas untuk ditandatangani</td>
			</tr>
			<tr>
				<td>3.</td>
				<td colspan="3">Peserta ujian dilarang untuk :</td>
			</tr>
			<tr>
				<td></td>
				<td style="text-align:center">a.</td>
				<td>Berhubungan langsung (berbicara maupun tanpa berbicara antara sesama peserta ujian) dengan alasan apapun.</td>
			</tr>
			<tr>
				<td></td>
				<td style="text-align:center">b.</td>
				<td>Pinjam meminjam peralatan yang dibutuhkan dalam ujian.</td>
			</tr>
			<tr>
				<td></td>
				<td style="text-align:center">c.</td>
				<td>Menyontek atau mencontoh catatan dalam bentuk apapun atau kertas atau pekerjaan peserta ujian lainnya.</td>
			</tr>
			<tr>
				<td></td>
				<td style="text-align:center">d.</td>
				<td>Makan, minum dan kegiatan lain selain melaksanakan uian selama ujian berlangsung.</td>
			</tr>
			<tr>
				<td></td>
				<td style="text-align:center">e.</td>
				<td>Meninggalkan ruangan selama ujian berlangsung.</td>
			</tr>
			<tr>
				<td></td>
				<td style="text-align:center">f.</td>
				<td>Peserta hanya diperkenankan ke toilet selama ujian berlangsung.</td>
			</tr>
			<tr>
				<td>4.</td>
				<td colspan="3">Peserta ujian diwajibkan:</td>
			</tr>
			<tr>
				<td></td>
				<td style="text-align:center">a)</td>
				<td>Pria : Kemeja putih lengan panjang, celana panjang hitam (bukan bahan jeans).</td>
			</tr>
			<tr>
				<td></td>
				<td style="text-align:center">b)</td>
				<td>Wanita : Kemeja putih lengan panjang, rok hiam (bukan bahan jeans) dan sepatu tertutup.</td>
			</tr>
			<tr>
				<td>5.</td>
				<td colspan="3">Peserta ujian dapat mengajukan pertanyaan kepada pengawas dengan ketentuan tidak menggangu kelancaran ujian dan hanya terbatas pada hal-hal yang berkaitan dengan redaksi atau teks ujian yang tertulis dilembar soal ujian.</td>
			</tr>
			<tr>
				<td>6.</td>
				<td colspan="3">Peserta wajib menonaktifkan semua bentuk alat komunikasi (Handphone adalah alat komunikasi, sedangkan kalkulator adalah alat bantu memghitung) dan semua tas harap diletakan didepan kelas sesuai perintah pengawas.</td>
			</tr>
			<tr>
				<td>7.</td>
				<td colspan="3">Peserta ujian dapat dipindahkan tempat duduknya oleh pengawas ujian.</td>
			</tr>
			<tr>
				<td>8.</td>
				<td colspan="3">Peserta ujian yang telah menyelesaikan ujian dapat meninggalkan ruang ujian paling cepat 30 menit setelah ujian berlangsung.</td>
			</tr>
			<tr>
				<td>9.</td>
				<td colspan="3">Dilarang membawa senjata tajam didalam kelas maupun di area kampus.</td>
			</tr>
			<tr>
				<td colspan="3"> Peserta ujian yang melanggar ketentuan seperti hal tersebut diatas, dapat dikeluarkan dari ruang ujian dan atau dikenakan sanksi dan pekerjaan ujian dinyatakan tidak sah atau batal.</td>
			</tr>
			<tr>
				<td></td>
				<td colspan="5" style="text-align:right"><img src="https://lh3.googleusercontent.com/RGgTHpFgLSZkGQP8epbS5yoVlTj6gsi1gXd1jQM5sksKje3F5LZSt6Bpbc_zaudVx3lPACpR0KnbBg_eRx2IrgwMBE5FbIgrpqBdg-BeFOfoKyJ4alpQ5pf2JVkjQ6zuU8ALqFkRFAH3tpVVErgu3WdsGxP_DwK3DF2qxPg0E4ipLldRLrl9tcl2hgtpG2mRMcczp2G3C77xL8SG8OMTH8GhR2MspErP35B8BH3fkKSIh7B4Thcl8AQiEMHuDgYXzynzkPH-sAocgMgEDD1Zlb8PF56Q_kOsYdOaqfkwhBUWTmsJ_SAa1xNwcm9IsxC83eWpMxAXtksi8IJU0IyT2r11ADyVzHKnrUd7LZZafB6zZEjq-NskgcAEhbI1hjVLHIVGjaB7bYn_9slGpPrGGWxvZWrVw1Flr7WwROxboCRB22yH71aWFAxiAW3WH_TDBcDgzcnOW-yD3jwqXgdkVHlOWKnCWC5q0fmuVJE3lnd-Dpf2Cy03anhqzZu2mT3xI_CyVVW3TUUfcJ_PoWchXrx0kPy5lZR4wcSn3bCWv3cC90Uzm70md4kjSwhBvQnFMoyO=w437-h229-no" style="width:55%"></td>
			</tr>
		</table>
	</div>
</body>
</html>