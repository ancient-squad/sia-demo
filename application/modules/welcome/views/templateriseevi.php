<!DOCTYPE html>

<html lang="en">

<head>

    <meta charset="utf-8">

    <title>SIA <?= $this->ORG_NAME ?></title>

    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

    <meta name="apple-mobile-web-app-capable" content="yes">

    <link href="<?php echo base_url();?>assets/css/bootstrap.min.css" rel="stylesheet">

    <link href="<?php echo base_url();?>assets/css/bootstrap-responsive.min.css" rel="stylesheet">

    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">

    <link href="<?php echo base_url();?>assets/css/font-awesome.css" rel="stylesheet">

    <link href="<?php echo base_url();?>assets/css/style.css" rel="stylesheet">

    <link href="<?php echo base_url();?>assets/css/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />

    <link href="<?php echo base_url();?>assets/css/pages/reports.css" rel="stylesheet">

    <link href="<?php echo base_url();?>assets/js/jquery-ui/css/ui-lightness/jquery-ui-1.9.2.custom.css" rel="stylesheet">

    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->

    <!--[if lt IE 9]>

          <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>

    <![endif]-->

    <script src="<?php echo base_url();?>assets/js/jquery-1.7.2.min.js"></script> 

</head>

<body>

<!-- /subnavbar -->
<marquee behavior="scroll" direction="up" style="width:100%; height:690px;" scrollamount="2" scroll="continuous" loop="infinte">
<div class="main">

  <div class="main-inner">
    <div class="container">

      <!-- load page -->

      <div class="row">
        <div class="span12"> 

                <table class="table table-bordered table-striped" style="font-size:17pt;font-weight: bold;">
                            <tbody>
                    <?php $no = 1; foreach($getData as $row){?>
                                <tr>
                                  <td><?php echo $no;?></td>
                                  <td><?php echo $row->NIMHSMSMHS;?></td>
                                  <td><?php echo $row->NMMHSMSMHS;?></td>
                                  <td><?php echo $row->TAHUNMSMHS;?></td>
                                  <td><?php echo $row->prodi;?></td>
                                  <td><?php echo $row->fakultas;?></td>
                                </tr>
                    <?php $no++; } ?>
                            </tbody>
                        </table>
             
        </div>
      </div>

      <!-- /row --> 

    </div>

    <!-- /container --> 

  </div>

  <!-- /main-inner --> 

</div>
</marquee>
<!-- /footer --> 

<!-- Le javascript

================================================== --> 

<!-- Placed at the end of the document so the pages load faster -->  

<script src="<?php echo base_url();?>assets/js/bootstrap.js"></script>

<script src="<?php echo base_url();?>assets/js/datatables/jquery.dataTables.js" type="text/javascript"></script>

<script src="<?php echo base_url();?>assets/js/datatables/dataTables.bootstrap.js" type="text/javascript"></script>

<script type="text/javascript">

    $(function() {

        $("#example1").dataTable();

        $("#example3").dataTable({

            "bPaginate": false,

            "bLengthChange": true,

            "bFilter": false,

            "bSort": true,

            "bInfo": true,

            "bAutoWidth": true

        });

        $("#example4").dataTable();

        $("#example5").dataTable();

        $('#example2').dataTable({

            "bPaginate": true,

            "bLengthChange": true,

            "bFilter": true,

            "bSort": true,

            "bInfo": true,

            "bAutoWidth": true

        });

    });

</script>

</body>

</html>

