<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

 class Formulir extends MY_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->library('Cfpdf');
    }

    public function index()
    {
        //die('jajaj');
        $pdf = new FPDF();
        $pdf->AddPage();
        $pdf->SetFont('Arial','',12);
        $pdf->Output();
    }

    function cetakformulir()
    {
        $user = $this->session->userdata('sess_login');
        $id= $user['userid'];


        $this->db->select('*');
        $this->db->from('tbl_calon_mhs a');
        $this->db->join('tbl_bio_cmb b', 'a.ID_registrasi = b.ID_registrasi');
        $this->db->where('a.ID_registrasi', $id);
        $data['row']=$this->db->get()->row();

        //var_dump($data['row']);die();

        $this->load->view('welcome/print/formulir_pdf2',$data);
       
    }

    function repairkrs($npm)
    {
        $nilai = $this->db->query("SELECT DISTINCT a.* FROM tbl_transaksi_nilai a 
                                    WHERE a.`THSMSTRLNM` = '20152' AND a.`NIMHSTRLNM` = '".$npm."' AND KDKMKTRLNM NOT IN (SELECT kd_matakuliah FROM tbl_krs WHERE kd_krs LIKE CONCAT('".$npm."','20152%'))")->result();
        $kodekrs = $this->db->query("select distinct kd_krs,semester_krs from tbl_krs where kd_krs like CONCAT('".$npm."','20152%')")->row();
        foreach ($nilai as $key) {
            $data['semester_krs'] = $kodekrs->semester_krs;
            $data['npm_mahasiswa'] = $key->NIMHSTRLNM;
            $pecah = explode('zzz',$key->kd_transaksi_nilai);
            $data['kd_jadwal'] = $pecah[0];
            $data['kd_matakuliah'] = $key->KDKMKTRLNM;
            $data['kd_krs'] = $kodekrs->kd_krs;        
            //var_dump($data);exit();
            $this->app_model->insertdata('tbl_krs', $data);
        }
    }


}

/* End of file pdf.php */
/* Location: ./application/controllers/testpdf.php */