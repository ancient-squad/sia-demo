<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pisang_krs extends MY_Controller {

	public function index()
	{
		$data['kolek'] = $this->db->query('SELECT a.npm_mahasiswa,c.NMMHSMSMHS,d.kd_matakuliah FROM tbl_verifikasi_krs a JOIN tbl_mahasiswa c
											ON c.NIMHSMSMHS=a.npm_mahasiswa JOIN tbl_krs d
											ON d.npm_mahasiswa=a.npm_mahasiswa 
											WHERE a.kd_jurusan = "32201" AND a.tahunajaran = "20152" ORDER BY a.npm_mahasiswa ASC')->result();

		$data['sambel'] = $this->db->query('SELECT DISTINCT a.npm_mahasiswa,c.NMMHSMSMHS,a.kd_matakuliah FROM tbl_krs a
									JOIN tbl_verifikasi_krs b ON a.`kd_krs`=b.`kd_krs`
									JOIN tbl_mahasiswa c ON a.`npm_mahasiswa`=c.`NIMHSMSMHS`
									WHERE b.kd_jurusan ="32201" AND b.tahunajaran = "20152" GROUP BY a.`kd_krs`')->result();

		

		$this->load->view('pisang', $data);
	}

	function sale()
	{
		$data['mhs'] = $this->db->query("SELECT DISTINCT a.npm_mahasiswa from tbl_krs a join tbl_verifikasi_krs b on a.kd_krs=b.kd_krs where b.tahunajaran = '20152' and b.kd_jurusan='32201'")->result();
		$this->load->view('krs_pisang', $data);	
	}

	function tblmahasiswa()
	{
		$CI = &get_instance();
		$this->db2 = $CI->load->database('msmhs', TRUE);
		$query = $this->db2->query("select * from MSMHS where TAHUNMSMHS >= 2009")->result();
		//var_dump($query);
		foreach ($query as $value) {
			//var_dump("update tbl_mahasiswa set STMHSMSMHS = '".$value->STMHSMSMHS."' where NIMHSMSMHS = '".$value->NIMHSMSMHS."'");exit();
			$this->db->query("update tbl_mahasiswa set STMHSMSMHS = '".$value->STMHSMSMHS."' where NIMHSMSMHS = '".$value->NIMHSMSMHS."'");
		}
	}

	function krsmhs($npm)
	{
		$query = $this->db->query("select * from tbl_krs where npm_mahasiswa = '".$npm."'")->result();
		//var_dump($query);
		foreach ($query as $value) {
			$data['npm_mahasiswa'] = $npm;
			$data['kd_matakuliah'] = $value->kd_matakuliah;
			$data['semester_krs'] = $value->semester_krs;
			$data['kd_krs'] = $value->kd_krs;
			$data['kd_jadwal'] = $value->kd_jadwal;
			$this->db->insert('tbl_krs_feeder', $data);
		}
	}

	function updatenamaibu()
	{
		$q = $this->db->query("SELECT b.`NIMHSMSMHS`,b.`NMMHSMSMHS`,a.`nm_ibu` FROM tbl_pmb_s2_2016 a
								JOIN tbl_mahasiswa b ON a.`npm_baru` = b.`NIMHSMSMHS`")->result();
		foreach ($q as $key) {
			$this->db->query('update tbl_mahasiswa set NMIBUMSMHS = "'.strtoupper($key->nm_ibu).'" where NIMHSMSMHS = "'.$key->NIMHSMSMHS.'"');
		}
	}

}

/* End of file Menyeh.php */
/* Location: ./application/modules/welcome/controllers/Menyeh.php */