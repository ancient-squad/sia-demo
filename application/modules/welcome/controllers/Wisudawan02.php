<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Wisudawan02 extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		$data['q'] = $this->db->query("SELECT * from tbl_wisuda where fak = 'Psikologi' order by nama, jur")->result();

		//var_dump($data['q']);die();

		$this->load->view('welcome/wisudawan02',$data);
	}


}

/* End of file Wisudawan.php */
/* Location: ./application/controllers/Wisudawan.php */