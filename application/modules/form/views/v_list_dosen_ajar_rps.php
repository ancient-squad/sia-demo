<link href="https://gitcdn.github.io/bootstrap-toggle/2.2.0/css/bootstrap-toggle.min.css" rel="stylesheet">

<script src="https://gitcdn.github.io/bootstrap-toggle/2.2.0/js/bootstrap-toggle.min.js"></script>

<script>
    function muat(kd){
        $('#konten').load('<?php echo base_url();?>form/formrps/load_modal/'+kd);
    }
</script>

<div class="row">

    <div class="span12">                    

        <div class="widget ">

            <div class="widget-header">

                <i class="icon-list"></i>

                <h3>Data Kegiatan Dosen Mengajar <?php echo $tahunajar; ?></h3>

            </div> <!-- /widget-header -->

            

            <div class="widget-content">

                <div class="span11">
                    
                    <table id="example1" class="table table-bordered table-striped">

                        <thead>

                            <tr> 

                                <th>Kode MK</th>

                                <th>Mata Kuliah</th>

                                <th>SKS</th>

                                <th>Dosen Pengampu</th>

                                <th width="110">Unduh Form RPS</th>

                            </tr>

                        </thead>

                        <tbody>

                            <?php $no=1; foreach ($load as $isi): ?>

                                <tr>

                                    <td><?php echo $isi->kd_matakuliah; ?></td>

                                    <td><?php echo $isi->nama_matakuliah; ?></td>

                                    <td><?php echo $isi->sks_matakuliah; ?></td>

                                    <td><?php echo $isi->nama; ?></td>

                                    <td class="td-actions" style="text-align:center">

                                        <?php 
                                            $cek = $this->temph_model->cek_available_rps($isi->kd_matakuliah,$isi->kd_prodi,$isi->kd_dosen);
                                            if (count($cek->result()) > 0) { ?>
                                                <a href="<?php echo base_url('temp_formrps/'.$cek->row()->filename);?>" class="btn btn-success btn-small"><i class="btn-icon-only icon-download"> </i></a>
                                            <?php } else {  ?>
                                                <a href="javascript:void(0);" onclick="alert('File Belum Diunggah !')" class="btn btn-danger btn-small"><i class="btn-icon-only icon-remove"> </i></a>
                                        <?php } ?>

                                    </td>

                                </tr>    

                            <?php $no++; endforeach ?>

                        </tbody>

                    </table>

                </div>

            </div>

        </div>

    </div>

</div>



<div class="modal fade" id="UpModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">

    <div class="modal-dialog">

        <div class="modal-content" id="konten">

            

        </div><!-- /.modal-content -->

    </div><!-- /.modal-dialog -->

</div><!-- /.modal -->