<?php
//var_dump(nama_dsn($kaprodi));die();


error_reporting(0);
//var_dump($dataJadwal);die();

$pdf = new FPDF("L","mm", "A4");

$pdf->AliasNbPages();

$pdf->AddPage();

$pdf->SetMargins(3, 3 ,0);

$pdf->SetFont('Arial','B',10);


$pdf->Ln(0);

$pdf->Cell(200,5,strtoupper($title->prodi),0,0,'L');

$pdf->Ln(4);

$pdf->SetFont('Arial','',10);

$pdf->Cell(200,5,''.strtoupper($title->fakultas).' - '.$this->ORG_NAME,0,0,'L');

$pdf->Ln(4);



$pdf->SetFont('Arial','',6);

$pdf->Cell(13,3,'KAMPUS I',0,0,'L');

$pdf->Cell(3,3,' : ',0,0,'L');

$pdf->Cell(140,3,'Jl. piere tendean,Mpunda, Nusa Tenggara Bar., Mande, Mpunda, Bima, Nusa Tenggara Bar. 84111, Indonesia',0,0,'L');

$pdf->Ln(2);



$pdf->SetFont('Arial','',6);

$pdf->Cell(13,3,'',0,0,'L');

$pdf->Cell(3,3,' : ',0,0,'L');

$pdf->Cell(140,3,'',0,0,'L');

$pdf->Ln(4);

$pdf->Cell(290,0,'',1,0,'C');


$pdf->ln(4);

$pdf->SetFont('Arial','',8);

$pdf->Cell(15,5,'KODE MK',0,0,'L');

$pdf->Cell(2,5,':',0,0,'L');

$pdf->Cell(80,5,$dataJadwal->kd_matakuliah ,0,0,'L');

$pdf->Cell(15,5,'Smtr/Thn',0,0,'L');

$pdf->Cell(2,5,':',0,0,'L');

if (substr($dataJadwal->kd_tahunajaran, 4,1) == 1) {
	$gj = 'Ganjil';
} else {
	$gj = 'Genap';
}


$pdf->Cell(50,5,$dataJadwal->semester_matakuliah.' / '.substr($dataJadwal->kd_tahunajaran, 0,4).' - '.$gj,0,0,'L');

$pdf->Cell(20,5,'NAMA DOSEN',0,0,'L');

$pdf->Cell(2,5,':',0,0,'L');

$pdf->Cell(50,5,$dataJadwal->nama,0,0,'L');

$pdf->ln(4);

$pdf->SetFont('Arial','',8);

$pdf->Cell(15,5,'NAMA MK',0,0,'L');

$pdf->Cell(2,5,':',0,0,'L');

$pdf->Cell(80,5,$dataJadwal->nama_matakuliah,0,0,'L');

$pdf->Cell(15,5,'SKS',0,0,'L');

$pdf->Cell(2,5,':',0,0,'L');

$pdf->Cell(50,5,$dataJadwal->sks_matakuliah,0,0,'L');

$pdf->Cell(20,5,'NID',0,0,'L');

$pdf->Cell(2,5,':',0,0,'L');

$pdf->Cell(50,5,$dataJadwal->kd_dosen,0,0,'L');

$pdf->ln(4);

$pdf->SetFont('Arial','',8);

$pdf->Cell(15,5,'KAMPUS',0,0,'L');

$pdf->Cell(2,5,':',0,0,'L');

$pdf->Cell(80,5,'Bekasi',0,0,'L');

$pdf->Cell(15,5,'KELAS',0,0,'L');

$pdf->Cell(2,5,':',0,0,'L');

$pdf->Cell(50,5,$dataJadwal->kelas,0,0,'L');



$pdf->ln(10);

$pdf->SetLeftMargin(3);

$pdf->SetFont('Arial','',12);
$pdf->SetLeftMargin(8);
$pdf->Cell(280,8,'DAFTAR NILAI PESERTA KULIAH '.strtoupper($dataJadwal->nama_matakuliah),1,0,'C');

$pdf->ln(8);

$pdf->SetFont('Arial','',8);

$pdf->Cell(8,10,'NO','L,T,R,B',0,'C');

$pdf->Cell(25,10,'NPM','L,T,R,B',0,'C');

$pdf->Cell(80,10,'NAMA','L,T,R,B',0,'C');

$pdf->SetFont('Arial','',6);
$pdf->Cell(29,5,'KEHADIRAN','L,T,R,B',0,'C');

$pdf->Cell(55,5,'NILAI TUGAS','L,T,R,B',0,'C');

$pdf->Cell(12,10,'ABSEN','L,T,R,B',0,'C');

$pdf->Cell(13,5,'RATA RATA','L,T,R',0,'C');

$pdf->Cell(12,10,'UTS','L,T,R,B',0,'C');

$pdf->Cell(12,10,'UAS','L,T,R,B',0,'C');


$pdf->Cell(34,5,'JENIS NILAI','L,T,R,B',0,'C');

$pdf->ln(5);

$pdf->Cell(8,0,'',0,0,'C');

$pdf->Cell(25,0,'',0,0,'C');

$pdf->Cell(80,0,'',0,0,'C');

$pdf->SetFont('Arial','',6);
$pdf->Cell(14,5,'DOSEN','1',0,'C');
$pdf->Cell(15,5,'MAHASISWA','1',0,'C');

$pdf->SetFont('Arial','',6);
$pdf->Cell(11,5,'TUGAS 1','1',0,'C');
$pdf->Cell(11,5,'TUGAS 2','1',0,'C');
$pdf->Cell(11,5,'TUGAS 3','1',0,'C');
$pdf->Cell(11,5,'TUGAS 4','1',0,'C');
$pdf->Cell(11,5,'TUGAS 5','1',0,'C');

$pdf->Cell(12,0,'','',0,'C');

$pdf->Cell(13,5,'TUGAS','L,R,B',0,'C');

$pdf->Cell(12,0,'',0,0,'C');

$pdf->Cell(12,0,'',0,0,'C');

$pdf->SetFont('Arial','',6);
$pdf->Cell(17,5,'NILAI','1',0,'C');
$pdf->Cell(17,5,'HURUF','1',0,'C');

$no=1;
$noo=1;
if ($dataJadwal->kd_tahunajaran >= '20171') {
	$tblabsen = 'tbl_absensi_mhs_new_20171';
	$tblnilai = 'tbl_nilai_detail';
} elseif ($dataJadwal->kd_tahunajaran == '20162') {
	$tblabsen = 'tbl_absensi_mhs_new';
	$tblnilai = 'tbl_nilai_detail';
} else {
	$tblabsen = 'tbl_absensi_mhs';
	$tblnilai = 'tbl_nilai_detail_20161';
}

foreach ($peserta as $key) {

	$pdf->ln(5);
	$pdf->SetFont('Arial','',8);
	$pdf->Cell(8,5,$no,1,0,'C');

	$pdf->Cell(25,5,$key->NIMHSMSMHS,1,0,'C');

	$pdf->Cell(80,5,$key->NMMHSMSMHS,1,0,'L');
	//$absendosen = $this->db->query("SELECT MAX(pertemuan) as satu FROM tbl_absensi_mhs where kd_jadwal = '".$dataJadwal->kd_jadwal."'")->row();
	$absenmhs = $this->db->query("SELECT COUNT(npm_mahasiswa) as dua FROM ".$tblabsen." where kd_jadwal = '".$dataJadwal->kd_jadwal."' and npm_mahasiswa = '".$key->NIMHSMSMHS."' and (kehadiran = 'H')")->row();
	$pdf->SetFont('Arial','',6);
	$pdf->Cell(14,5,$absendosen->satu,'1',0,'C');
	$pdf->Cell(15,5,$absenmhs->dua,'1',0,'C');

	$nilai 	= $this->db->query("SELECT DISTINCT tipe,nilai FROM ".$tblnilai."
								WHERE npm_mahasiswa = '".str_replace(' ', '', $key->NIMHSMSMHS)."'
								AND kd_jadwal = '".$dataJadwal->kd_jadwal."' ")->result();
	foreach ($nilai as $val) {
		switch ($val->tipe) {
			case '2':
				$absensi = $val->nilai;
				break;
			case '1':
				$tugas = $val->nilai;
				break;
			case '3':
				$uts = $val->nilai;
				break;
			case '4':
				$uas = $val->nilai;
				break;
			case '5':
				$tugas1 = $val->nilai;
				break;
			case '6':
				$tugas2 = $val->nilai;
				break;
			case '7':
				$tugas3 = $val->nilai;
				break;
			case '8':
				$tugas4 = $val->nilai;
				break;
			case '9':
				$tugas5 = $val->nilai;
				break;
			case '10':
				$rt = $val->nilai;
				break;
		}
	}

	$pdf->SetFont('Arial','',6);
	if (is_null($tugas1) or $tugas1 == '') {
		$tugas1 = '-';
	}
	if (is_null($tugas2) or $tugas2 == '') {
		$tugas2 = '-';
	}
	if (is_null($tugas3) or $tugas3 == '') {
		$tugas3 = '-';
	}
	if (is_null($tugas4) or $tugas4 == '') {
		$tugas4 = '-';
	}
	if (is_null($tugas5) or $tugas5 == '') {
		$tugas5 = '-';
	}
	$pdf->Cell(11,5,$tugas1,'1',0,'C');
	$pdf->Cell(11,5,$tugas2,'1',0,'C');
	$pdf->Cell(11,5,$tugas3,'1',0,'C');
	$pdf->Cell(11,5,$tugas4,'1',0,'C');
	$pdf->Cell(11,5,$tugas5,'1',0,'C');

	$pdf->Cell(12,5,$absensi,'L,R,B',0,'C');

	$pdf->SetFont('Arial','',6);
	$pdf->Cell(13,5,$tugas,'1',0,'C');
	$pdf->Cell(12,5,$uts,'1',0,'C');
	$pdf->Cell(12,5,$uas,'1',0,'C');


	$logged = $this->session->userdata('sess_login');

	$getFinalScore = $this->app_model->getnilai($dataJadwal->kd_jadwal,$key->NIMHSMSMHS,10,$dataJadwal->kd_matakuliah)->row()->nilai;
	$finalScore    = number_format($getFinalScore,2);
	# get index nilai
	$this->db->select('nilai_bawah, nilai_atas, nilai_huruf');
	$this->db->where('deleted_at');
	$data = $this->db->get('tbl_index_nilai')->result();

	# get nilai_huruf dase on poin range
	foreach ($data as $keys => $value) {
		if ($finalScore >= $value->nilai_bawah && $finalScore <= $value->nilai_atas) {
			$gradeScore = $value->nilai_huruf;
		}
	}

	$pdf->SetFont('Arial','',6);
	$pdf->Cell(17,5,$finalScore,'1',0,'C');
	$pdf->Cell(17,5,$gradeScore,'1',0,'C');


	$no++;
	$noo++;

	if ($noo == 21) {
		$noo = 1;

		$pdf->ln(8);

		$pdf->Cell(110,5,'',0,0,'C');
		$pdf->Cell(30,5,'Kepala Program Studi',0,0,'C');
		$x = $pdf->GetX();
		$y = $pdf->GetY();
		//$pdf->image(base_url().'/assets/ttd_baa.png',($x-30),($y+2),27);
		$pdf->Cell(50,5,'',0,0,'C');
		$pdf->Cell(113,5,'Dosen Pengajar',0,0,'C');

		$pdf->ln(16);
		$pdf->Cell(110,5,'',0,0,'C');
		$pdf->Cell(30,5,nama_dsn($kaprodi),0,0,'C');
		$pdf->Cell(50,5,'',0,0,'C');
		$pdf->Cell(113,5,$dataJadwal->nama,0,0,'C');

$pdf->AliasNbPages();

$pdf->AddPage();

$pdf->SetMargins(3, 3 ,0);

$pdf->SetFont('Arial','B',10);

//$pdf->Image(''.base_url().'assets/img/logo-stkip-bima.png',60,30,90);

$pdf->Ln(0);

$pdf->Cell(200,5,strtoupper($title->prodi),0,0,'L');

$pdf->Ln(4);

$pdf->SetFont('Arial','',10);

$pdf->Cell(200,5,strtoupper($title->fakultas),0,0,'L');

$pdf->Ln(4);



$pdf->SetFont('Arial','',6);

$pdf->Cell(13,3,'KAMPUS I',0,0,'L');

$pdf->Cell(3,3,' : ',0,0,'L');

$pdf->Cell(140,3,'Jl. piere tendean,Mpunda, Nusa Tenggara Bar., Mande, Mpunda, Bima, Nusa Tenggara Bar. 84111, Indonesia',0,0,'L');

$pdf->Ln(2);



$pdf->SetFont('Arial','',6);

$pdf->Cell(13,3,'',0,0,'L');

$pdf->Cell(3,3,' : ',0,0,'L');

$pdf->Cell(140,3,'',0,0,'L');

$pdf->Ln(4);

$pdf->Cell(280,0,'',1,0,'C');



$pdf->ln(4);

$pdf->SetFont('Arial','',8);

$pdf->Cell(15,5,'KODE MK',0,0,'L');

$pdf->Cell(2,5,':',0,0,'L');

$pdf->Cell(50,5,$dataJadwal->kd_matakuliah ,0,0,'L');

$pdf->Cell(15,5,'Smtr/Thn',0,0,'L');

$pdf->Cell(2,5,':',0,0,'L');

$pdf->Cell(50,5,$dataJadwal->semester_matakuliah,0,0,'L');

$pdf->Cell(20,5,'NAMA DOSEN',0,0,'L');

$pdf->Cell(2,5,':',0,0,'L');

$pdf->Cell(50,5,$dataJadwal->nama,0,0,'L');

$pdf->ln(4);

$pdf->SetFont('Arial','',8);

$pdf->Cell(15,5,'NAMA MK',0,0,'L');

$pdf->Cell(2,5,':',0,0,'L');

$pdf->Cell(50,5,$dataJadwal->nama_matakuliah,0,0,'L');

$pdf->Cell(15,5,'SKS',0,0,'L');

$pdf->Cell(2,5,':',0,0,'L');

$pdf->Cell(50,5,$dataJadwal->sks_matakuliah,0,0,'L');

$pdf->Cell(20,5,'NID',0,0,'L');

$pdf->Cell(2,5,':',0,0,'L');

$pdf->Cell(50,5,$dataJadwal->kd_dosen,0,0,'L');

$pdf->ln(4);

$pdf->SetFont('Arial','',8);

$pdf->Cell(15,5,'KAMPUS',0,0,'L');

$pdf->Cell(2,5,':',0,0,'L');

$pdf->Cell(50,5,'Bekasi',0,0,'L');

$pdf->Cell(15,5,'KELAS',0,0,'L');

$pdf->Cell(2,5,':',0,0,'L');

$pdf->Cell(50,5,$dataJadwal->kelas,0,0,'L');


$pdf->ln(10);
$pdf->SetLeftMargin(3);

$pdf->SetFont('Arial','',12);

$pdf->Cell(280,8,'DAFTAR NILAI PESERTA KULIAH '.strtoupper($dataJadwal->nama_matakuliah),1,0,'C');

$pdf->ln(8);

$pdf->SetFont('Arial','',8);

$pdf->Cell(8,10,'NO','L,T,R,B',0,'C');

$pdf->Cell(25,10,'NPM','L,T,R,B',0,'C');

$pdf->Cell(80,10,'NAMA','L,T,R,B',0,'C');

$pdf->SetFont('Arial','',6);

$pdf->Cell(29,5,'KEHADIRAN','L,T,R,B',0,'C');

$pdf->Cell(55,5,'NILAI TUGAS','L,T,R,B',0,'C');

$pdf->Cell(12,10,'ABSEN','L,T,R,B',0,'C');

$pdf->Cell(13,5,'RATA RATA','L,T,R',0,'C');

$pdf->Cell(12,10,'UTS','L,T,R,B',0,'C');

$pdf->Cell(12,10,'UAS','L,T,R,B',0,'C');


$pdf->Cell(34,5,'JENIS NILAI','L,T,R,B',0,'C');

$pdf->ln(5);

$pdf->Cell(8,0,'',0,0,'C');

$pdf->Cell(25,0,'',0,0,'C');

$pdf->Cell(80,0,'',0,0,'C');

$pdf->SetFont('Arial','',6);
$pdf->Cell(14,5,'DOSEN','1',0,'C');
$pdf->Cell(15,5,'MAHASISWA','1',0,'C');

$pdf->SetFont('Arial','',6);
$pdf->Cell(11,5,'TUGAS 1','1',0,'C');
$pdf->Cell(11,5,'TUGAS 2','1',0,'C');
$pdf->Cell(11,5,'TUGAS 3','1',0,'C');
$pdf->Cell(11,5,'TUGAS 4','1',0,'C');
$pdf->Cell(11,5,'TUGAS 5','1',0,'C');

$pdf->Cell(12,0,'','',0,'C');

$pdf->Cell(13,5,'TUGAS','L,R,B',0,'C');

$pdf->Cell(12,0,'',0,0,'C');

$pdf->Cell(12,0,'',0,0,'C');

$pdf->SetFont('Arial','',6);
$pdf->Cell(17,5,'NILAI','1',0,'C');
$pdf->Cell(17,5,'HURUF','1',0,'C');

	}

	//ending header

}

$pdf->ln(8);

$pdf->Cell(110,5,'',0,0,'C');
$pdf->Cell(30,5,'Kepala Program Studi',0,0,'C');
$x = $pdf->GetX();
$y = $pdf->GetY();
//$pdf->image(base_url().'/assets/ttd_baa.png',($x-30),($y+2),27);
$pdf->Cell(50,5,'',0,0,'C');
$pdf->Cell(113,5,'Dosen Pengajar',0,0,'C');

$pdf->ln(16);
$pdf->Cell(110,5,'',0,0,'C');
$pdf->Cell(30,5,nama_dsn($kaprodi),0,0,'C');
$pdf->Cell(50,5,'',0,0,'C');
$pdf->Cell(113,5,$dataJadwal->nama,0,0,'C');


$pdf->Output('DAFTAR_NILAI.PDF','I');



?>
