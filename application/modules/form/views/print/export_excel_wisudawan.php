<?php
//die('aa');
header("Content-Type: application/xls");    
header("Content-Disposition: attachment; filename=data_status_wisudawan.xls");  
header("Pragma: no-cache"); 
header("Expires: 0");
?>

<style>
table, td, th {
    border: 1px solid black;
}

th {
    background-color: blue;
    color: black;
}
</style>

<table border="1">
	<thead>
		<tr>
			<th colspan="12">Daftar Data Wisudawan Tahun <?= date('Y') ?></th>
		</tr>
		<tr>
			<th>No</th>
			<th>NPM</th>
			<th>NAMA</th>
			<th>JURUSAN</th>
			<th>FAKULTAS</th>
			<th>Nilai Akademik</th>
			<th>Skripsi</th>
			<th>Iuran Mahasiswa</th>
			<th>Pembayaran Skripsi & Wisuda</th>
			<th>Bebas Pinjaman Buku</th>
			<th>Sumbangan Buku</th>
			<th>Status</th>
		</tr>
	</thead>
	<tbody>
		<?php $no = 1; foreach($mhs as $row) { ?>
		<tr>
			<td><?php echo number_format($no); ?></td>
			<td><?php echo $row->NIMHSMSMHS; ?></td>
			<td><?php echo $row->NMMHSMSMHS; ?></td>
			<td><?php echo $row->prodi; ?></td>
			<td><?php echo $row->fakultas; ?></td>
			<?php if ($row->flag_wisuda_fak == 2) { ?>
				<td>Valid</td>
				<td>Valid</td>
			<?php } elseif ($row->flag_wisuda_fak == 1) { ?>
				<td>Valid</td>
				<td> - </td>
			<?php } else { ?>
				<td> - </td>
				<td> - </td>
			<?php } ?>
			
			<?php if ($row->flag_wisuda_renkeu == 2) { ?>
				<td>Valid</td>
				<td>Valid</td>
			<?php } elseif ($row->flag_wisuda_renkeu == 1) { ?>
				<td>Valid</td>
				<td> - </td>
			<?php } else { ?>
				<td> - </td>
				<td> - </td>
			<?php } ?>
			
			<?php if ($row->flag_wisuda_perpus == 2) { ?>
				<td>Valid</td>
				<td>Valid</td>
			<?php } elseif ($row->flag_wisuda_perpus == 1) { ?>
				<td>Valid</td>
				<td> - </td>
			<?php } else { ?>
				<td> - </td>
				<td> - </td>
			<?php } ?>
			
			<?php if ($row->flag_wisuda_baa == 1) { ?>
				<td>SUdah Diambil</td>
			<?php } else { ?>
				<td> - </td>
			<?php } ?>
		</tr>
		<?php $no++; } ?>
	</tbody>
</table>