<script type="text/javascript">
    function edit(edc) {
        $("#cuti").load('<?php echo base_url()?>form/formcuti/looad/'+edc);
    }
</script>

<div class="row">
    <div class="span12">                    
        <div class="widget ">
            <div class="widget-header">
                <i class="icon-home"></i>
                <h3>Daftar Permohonan Cuti</h3>
            </div> <!-- /widget-header -->
            
            <div class="widget-content">
                <div class="span11">
                    <form action="<?php echo base_url(); ?>form/formcuti/input_data" method="post">
                        <a class="btn btn-success" href="#"><i class="btn-icon-only icon-print"> Print Daftar Cuti</i></a>
                        <input type="submit" class="btn btn-primary" value="Submit">
                        <hr>
                        <table id="example1" class="table table-bordered table-striped">
                            <thead>
                                <tr> 
                                    <th>No</th>
                                    <th>NIM</th>
                                    <th>Nama</th>
                                    <th>Angkatan</th>
                                    <th>Prodi</th>
                                    <th>Print Surat</th>
                                    <th width="40">Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $no=1; foreach ($holah as $row) { ?>
                                <tr>
                                    <td><?php echo $no; ?></td>
                                    <td><?php echo $row->NIMHSMSMHS; ?></td>
                                    <td><?php echo $row->NMMHSMSMHS; ?></td>
                                    <td><?php echo $row->TAHUNMSMHS; ?></td>
                                    <td><?php echo $row->prodi; ?></td>
                                    <td><a class="btn btn-success btn-small" href="<?php echo base_url(); ?>form/formcuti/form_cuti/<?php echo $row->NIMHSMSMHS; ?>"><i class="btn-icon-only icon-print"> </i></a></td>
                                    <td class="td-actions">
                                    <?php $statusv = $this->db->query("SELECT validate from tbl_status_mahasiswa where npm = '".$row->NIMHSMSMHS."' and tahunajaran = '".$this->session->userdata('tahunajaran')."' ")->row()->validate; if ($statusv != 1) { ?>
                                        <input type="checkbox" class="checkbox" name="mhs[]" value="1911<?php echo $row->NIMHSMSMHS; ?>911<?php echo $this->session->userdata('tahunajaran'); ?>"/>
                                    <?php } else { ?>
                                        <input type="checkbox" class="checkbox" name="mhs[]" value="1911<?php echo $row->NIMHSMSMHS; ?>911<?php echo $this->session->userdata('tahunajaran'); ?>" checked disabled/>
                                    <?php }?>
                                    </td>
                                </tr>
                                <?php $no++; } ?>
                            </tbody>
                        </table>
                    </form>
                </div>                
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content" id="cuti">
            
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>