

<div class="row">
	<div class="span12" id="form_pmb">      		  		
  		<div class="widget ">
  			<div class="widget-header">
                <i class="icon-user"></i>
  				<h3>Form Pendaftaran Sidang</h3>
			</div>
			<div class="widget-content">
				<form class="form-horizontal" action="<?php echo base_url(); ?>" method="post">
					<fieldset>
						<h3>Data Ijazah</h3>
						<div class="control-group">
							<label class="control-label">NPM</label>
							<div class="controls">
								<input type="text" class="form-control span6" name="npm" required><br>
								<!-- <small>*sesuai ktp / kartu keluarga</small> -->
							</div>
						</div>
						<div class="control-group">
							<label class="control-label">Nama</label>
							<div class="controls">
								<input type="text" class="form-control span6"  name="nama" required><br>
								<small>*Pastikan nama anda sesuai dengan ijazah terahir.</small>
							</div>
						</div>
						<div class="control-group" id="new4">
							<label class="control-label">Kesesuaian Nama</label>
							<div class="controls">
								<input type="radio" name="ijazah_ubah" value="T" > Sesuai Ijazah &nbsp;&nbsp;
								<input type="radio" name="ijazah_ubah" value="F" > Tidak Sesuai Ijazah
							</div>
						</div>
						<div class="control-group">
							<label class="control-label">Upload Ijazah</label>
							<div class="controls">
								<input type="file" name="ijazah"><br>
								<small>*Upload ijazah terahir anda.</small>
							</div>
						</div>
						<br>
						<hr>
						<h3>Data Akta Kelahiran</h3>
						<div class="control-group">
							<label class="control-label">Nama Ibu</label>
							<div class="controls">
								<input type="text" class="form-control span6"  name="namaibu" required><br>
								<small>*Sesuai akta kelahiran.</small>
							</div>
						</div>
						<div class="control-group">
							<label class="control-label">Tempat/Tanggal Lahir</label>
							<div class="controls">
								<input type="text" class="form-control span4"  name="tempatlahir" required>&nbsp;/&nbsp;
								<input type="text" class="form-control span2"  name="tanggallahir" required><br>
								<small>*Sesuai akta kelahiran anda(bukan Ibu anda).</small>
							</div>
						</div>						
						
						<div class="control-group" id="new4">
							<label class="control-label">Kesesuaian Data </label>
							<div class="controls">
								<input type="radio" name="akta_ubah" value="T" > Sesuai Akta Kelahiran &nbsp;&nbsp;
								<input type="radio" name="akta_ubah" value="F" > Tidak Sesuai Akta Kelahiran&nbsp;&nbsp;
							</div>
						</div>

						<div class="control-group">
							<label class="control-label">Upload Akta Kelahiran</label>
							<div class="controls">
								<input type="file" name="akta"><br>
								<small>*Upload Akta Kelahiran anda.</small>
							</div>
						</div>
						<br>
						<hr>
						<h3>Upload Skripsi</h3>
						<div class="control-group">
							<label class="control-label">Bab I</label>
							<div class="controls">
								<input type="file" name="bab1"><br>
							</div>
						</div>
						<div class="control-group">
							<label class="control-label">Bab II</label>
							<div class="controls">
								<input type="file" name="bab2"><br>
							</div>
						</div>
						<div class="control-group">
							<label class="control-label">Bab III</label>
							<div class="controls">
								<input type="file" name="bab3"><br>
							</div>
						</div>
						<div class="control-group">
							<label class="control-label">Bab IV</label>
							<div class="controls">
								<input type="file" name="bab4"><br>
							</div>
						</div>
						<div class="control-group">
							<label class="control-label">Bab V</label>
							<div class="controls">
								<input type="file" name="bab5"><br>
							</div>
						</div>
						<br>
						<hr>
						<h3>Lain-Lain</h3>
						<div class="control-group">
							<label class="control-label">Lembar pengesahan</label>
							<div class="controls">
								<input type="file" name="lp"><br>
							</div>
						</div>
						<div class="control-group">
							<label class="control-label">Foto</label>
							<div class="controls">
								<input type="file" name="foto"><br>
							</div>
						</div>
						
						

						
						<div class="form-actions">
							<input class="btn btn-large btn-primary" type="submit" value="Submit">
							<input class="btn btn-large btn-default" type="reset" value="Clear">
						</div>
					</fieldset>
				</form>
			</div>
		</div>
	</div>
</div>
