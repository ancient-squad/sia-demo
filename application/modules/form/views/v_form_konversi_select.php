<div class="row">

  <div class="span12">                

      <div class="widget ">

        <div class="widget-header">

          <i class="icon-user"></i>

          <h3>Form Nilai Konversi</h3>

      </div> <!-- /widget-header -->      

      <div class="widget-content">

        <div class="span11">

        <form method="post" class="form-horizontal" action="<?php echo base_url(); ?>form/formnilai_konversi/save_session">

            <fieldset>

                  <div class="control-group">

                    <label class="control-label">Tahun Akademik</label>

                    <div class="controls">

                      <select class="form-control span3" name="angkatan" id="tahunajaran">

                        <option>--Pilih Angkatan Mahasiswa--</option>

                        <?php foreach ($angkatan as $row) { ?>

                        <option value="<?php echo $row->TAHUNMSMHS;?>"><?php echo $row->TAHUNMSMHS;?></option>

                        <?php } ?>

                      </select>

                    </div>

                  </div>  

                <br/>
                 

                <div class="form-actions">

                    <input type="submit" class="btn btn-large btn-success" value="Cari"/> 

                </div>

            </fieldset>

        </form>

        </div>

      </div>

    </div>

  </div>

</div>
