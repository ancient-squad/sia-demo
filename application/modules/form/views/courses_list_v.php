<div class="row">
    <div class="span12">                    
        <div class="widget ">
            <div class="widget-header">
                <i class="icon-list"></i>
                <?php $name = (!in_array(6, $usergroup) || !in_array(7, $usergroup)) ? ' - '.nama_dsn($userid) : ''; ?>
                <h3>Daftar Pengajaran <?= get_thnajar($tahunajar).$name ?></h3>
            </div>
            
            <div class="widget-content">
                <div class="span11">
                    <?php if (in_array(6, $usergroup) || in_array(7, $usergroup)) {
                        $path = base_url('form/nilai');
                    } else {
                        $path = base_url('form/nilai/lecturer_list');
                    } ?>
                    <a href="<?= $path ?>" class="btn btn-warning">
                        <i class="icon-chevron-left"></i> Kembali
                    </a>
                    <hr>
                    <table id="example1" class="table table-bordered table-striped">
                        <thead>
                            <tr> 
                                <th>No</th>
                                <th>Kode Matakuliah</th>
                                <th>Nama Matakuliah</th>
                                <th>SKS</th>
                                <th>Kelas</th>
                                <th>Waktu Perkuliahan</th>
                                <th>Jumlah Peserta</th>
                                <th width='40'>Detail</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $no=1; foreach ($courses as $course) : ?>
                                <script>
                                    $.get('<?= base_url('form/nilai/total_participant/'.$course->id_jadwal) ?>',function(res) {
                                        $('.participant<?= $no ?>').empty();
                                        $('.participant<?= $no ?>').html(res);
                                    })
                                </script>
                                <tr>
                                    <td><?= $no ?></td>
                                    <td><?= $course->kd_matakuliah ?></td>
                                    <td><?= $course->nama_matakuliah ?></td>
                                    <td><?= $course->sks_matakuliah ?></td>
                                    <td><?= $course->kelas ?></td>
                                    <td>
                                        <?= notohari($course->hari) ?>, <?= $course->waktu_mulai.' - '.$course->waktu_selesai ?>
                                    </td>
                                    <td class="participant<?= $no ?>">
                                        <img src="<?= base_url('assets/img/ajax-loader.gif') ?>" style="width: 20px">
                                    </td>
                                    <td>
                                        <a href="<?= base_url('form/nilai/participants_list/'.$course->id_jadwal) ?>" class="btn btn-primary">
                                            <i class="icon-eye-open"></i>
                                        </a>
                                    </td>
                                </tr>
                            <?php $no++; endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<br>