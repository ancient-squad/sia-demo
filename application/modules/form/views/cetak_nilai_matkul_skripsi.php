<?php
error_reporting(0);

$pdf = new FPDF("P","mm", "A4");

$pdf->AliasNbPages();
$pdf->AddPage();
$pdf->SetMargins(3, 3 ,0);
$pdf->SetFont('Arial','B',10); 

$pdf->Ln(0);
$pdf->Cell(200,5,strtoupper($title->prodi),0,0,'L');
$pdf->Ln(4);
$pdf->SetFont('Arial','',10); 
$pdf->Cell(200,5,''.strtoupper($title->fakultas).' - '.$this->ORG_NAME,0,0,'L');

$pdf->Ln(4);
$pdf->SetFont('Arial','',6);
$pdf->Cell(13,3,'KAMPUS I',0,0,'L');
$pdf->Cell(3,3,' : ',0,0,'L');
$pdf->Cell(140,3,'Jl. piere tendean,Mpunda, Nusa Tenggara Bar., Mande, Mpunda, Bima, Nusa Tenggara Bar. 84111, Indonesia',0,0,'L');

$pdf->Ln(2);
$pdf->SetFont('Arial','',6);
$pdf->Cell(13,3,'',0,0,'L');
$pdf->Cell(3,3,' : ',0,0,'L');
$pdf->Cell(140,3,'',0,0,'L');
$pdf->Ln(4);
$pdf->Cell(200,0,'',1,0,'C');

$pdf->ln(4);
$pdf->SetFont('Arial','',8);
$pdf->Cell(15,5,'KODE MK',0,0,'L');
$pdf->Cell(2,5,':',0,0,'L');
$pdf->Cell(50,5,$dataJadwal->kd_matakuliah ,0,0,'L');
$pdf->Cell(15,5,'Smtr/Thn',0,0,'L');
$pdf->Cell(2,5,':',0,0,'L');
$pdf->Cell(50,5,$dataJadwal->semester_matakuliah,0,0,'L');
$pdf->Cell(20,5,'NAMA DOSEN',0,0,'L');
$pdf->Cell(2,5,':',0,0,'L');
$pdf->Cell(50,5,$dataJadwal->nama,0,0,'L');

$pdf->ln(4);
$pdf->SetFont('Arial','',8);
$pdf->Cell(15,5,'NAMA MK',0,0,'L');
$pdf->Cell(2,5,':',0,0,'L');
$pdf->Cell(50,5,$dataJadwal->nama_matakuliah,0,0,'L');
$pdf->Cell(15,5,'SKS',0,0,'L');
$pdf->Cell(2,5,':',0,0,'L');
$pdf->Cell(50,5,$dataJadwal->sks_matakuliah,0,0,'L');
$pdf->Cell(20,5,'NIDN',0,0,'L');
$pdf->Cell(2,5,':',0,0,'L');
$pdf->Cell(50,5,$dataJadwal->kd_dosen,0,0,'L');

$pdf->ln(4);
$pdf->SetFont('Arial','',8);
$pdf->Cell(15,5,'KAMPUS',0,0,'L');
$pdf->Cell(2,5,':',0,0,'L');
$pdf->Cell(50,5,'Bekasi',0,0,'L');
$pdf->Cell(15,5,'KELAS',0,0,'L');
$pdf->Cell(2,5,':',0,0,'L');
$pdf->Cell(50,5,$dataJadwal->kelas,0,0,'L');

$pdf->ln(10);
$pdf->SetLeftMargin(35);
$pdf->SetFont('Arial','',12);
$pdf->Cell(133,8,'DAFTAR NILAI PESERTA KULIAH',1,0,'C');

$pdf->ln(8);
$pdf->SetFont('Arial','',8);
$pdf->Cell(8,10,'NO','L,T,R,B',0,'C');
$pdf->Cell(25,10,'NPM','L,T,R,B',0,'C');
$pdf->Cell(60,10,'NAMA','L,T,R,B',0,'C');
$pdf->Cell(20,10,'NILAI','1',0,'C');
$pdf->Cell(20,10,'HURUF','1',0,'C');

$no=1;
$noo=1;
$pdf->ln(5);

foreach ($peserta as $key) {
	
	$pdf->ln(5);
	$pdf->SetFont('Arial','',8);
	$pdf->Cell(8,5,$no,1,0,'C');
	$pdf->Cell(25,5,$key->NIMHSMSMHS,1,0,'C');
	$pdf->Cell(60,5,$key->NMMHSMSMHS,1,0,'L');

	$getFinalScore = $this->app_model->getnilai($dataJadwal->kd_jadwal,$key->NIMHSMSMHS,10,$dataJadwal->kd_matakuliah)->row()->nilai;
	$finalScore    = number_format($getFinalScore,2);
	# get index nilai
	$this->db->select('nilai_bawah, nilai_atas, nilai_huruf');
	$this->db->where('deleted_at');
	$data = $this->db->get('tbl_index_nilai')->result();

	# get nilai_huruf dase on poin range
	foreach ($data as $keys => $value) {
		if (in_array((float)$finalScore, range((float)$value->nilai_bawah, (float)$value->nilai_atas, 0.01))) {
			$gradeScore = $value->nilai_huruf;	
		}
	}

	$pdf->SetFont('Arial','',6);
	$pdf->Cell(20,5,$finalScore,'1',0,'C');
	$pdf->Cell(20,5,$gradeScore,'1',0,'C');

	$no++;
	$noo++;

	if ($noo == 31) {
		$noo = 1;

		$pdf->AliasNbPages();
		$pdf->AddPage();
		$pdf->SetMargins(3, 3 ,0);
		$pdf->SetFont('Arial','B',10); 

		$pdf->Ln(0);
		$pdf->Cell(200,5,strtoupper($title->prodi),0,0,'L');

		$pdf->Ln(4);
		$pdf->SetFont('Arial','',10); 
		$pdf->Cell(200,5,strtoupper($title->fakultas),0,0,'L');

		$pdf->Ln(4);
		$pdf->SetFont('Arial','',6);
		$pdf->Cell(13,3,'KAMPUS I',0,0,'L');
		$pdf->Cell(3,3,' : ',0,0,'L');
		$pdf->Cell(140,3,'Jl. piere tendean,Mpunda, Nusa Tenggara Bar., Mande, Mpunda, Bima, Nusa Tenggara Bar. 84111, Indonesia',0,0,'L');

		$pdf->Ln(2);
		$pdf->SetFont('Arial','',6);
		$pdf->Cell(13,3,'',0,0,'L');
		$pdf->Cell(3,3,' : ',0,0,'L');
		$pdf->Cell(140,3,'',0,0,'L');
		$pdf->Ln(4);
		$pdf->Cell(200,0,'',1,0,'C');

		$pdf->ln(4);
		$pdf->SetFont('Arial','',8);
		$pdf->Cell(15,5,'KODE MK',0,0,'L');
		$pdf->Cell(2,5,':',0,0,'L');
		$pdf->Cell(50,5,$dataJadwal->kd_matakuliah ,0,0,'L');
		$pdf->Cell(15,5,'Smtr/Thn',0,0,'L');
		$pdf->Cell(2,5,':',0,0,'L');
		$pdf->Cell(50,5,$dataJadwal->semester_matakuliah,0,0,'L');
		$pdf->Cell(20,5,'NAMA DOSEN',0,0,'L');
		$pdf->Cell(2,5,':',0,0,'L');
		$pdf->Cell(50,5,$dataJadwal->nama,0,0,'L');

		$pdf->ln(4);
		$pdf->SetFont('Arial','',8);
		$pdf->Cell(15,5,'NAMA MK',0,0,'L');
		$pdf->Cell(2,5,':',0,0,'L');
		$pdf->Cell(50,5,$dataJadwal->nama_matakuliah,0,0,'L');
		$pdf->Cell(15,5,'SKS',0,0,'L');
		$pdf->Cell(2,5,':',0,0,'L');
		$pdf->Cell(50,5,$dataJadwal->sks_matakuliah,0,0,'L');
		$pdf->Cell(20,5,'NIDN',0,0,'L');
		$pdf->Cell(2,5,':',0,0,'L');
		$pdf->Cell(50,5,$dataJadwal->kd_dosen,0,0,'L');

		$pdf->ln(4);
		$pdf->SetFont('Arial','',8);
		$pdf->Cell(20,5,'KAMPUS',0,0,'L');
		$pdf->Cell(2,5,':',0,0,'L');
		$pdf->Cell(50,5,'Bekasi',0,0,'L');

		$pdf->ln(10);
		$pdf->SetLeftMargin(35);
		$pdf->SetFont('Arial','',12);
		$pdf->Cell(133,8,'DAFTAR NILAI PESERTA KULIAH',1,0,'C');
		$pdf->ln(8);

		$pdf->SetFont('Arial','',8);
		$pdf->Cell(8,10,'NO','L,T,R,B',0,'C');
		$pdf->Cell(25,10,'NPM','L,T,R,B',0,'C');
		$pdf->Cell(60,10,'NAMA','L,T,R,B',0,'C');
		$pdf->Cell(20,10,'NILAI','1',0,'C');
		$pdf->Cell(20,10,'HURUF','1',0,'C');
		$pdf->ln(5);

	}
}

$pdf->Output('DAFTAR_NILAI.PDF','I');

