
<div class="row">
	<div class="span12">      		  		
  		<div class="widget ">
  			<div class="widget-header">
  				<i class="icon-user"></i>
  				<h3>Beasiswa</h3>
			</div> <!-- /widget-header -->
			<div class="widget-content">
				<div class="span11">
					<?php if (!empty($formulir)){?>
						<a href="#" class="btn btn-primary" title="Formulir Beasiswa" disabled><i class="btn-icon-only icon-file"> </i>Formulir sudah diisi</a>
					<?php }else{ ?>
						<a href="<?php echo base_url();?>form/beasiswa/isi" class="btn btn-primary" title="Formulir Beasiswa" ><i class="btn-icon-only icon-file"> </i>Formulir</a>
					<?php } ?>
					<hr>
					<?php foreach($list_formulir AS $row) {?>
					<div class="thumbnail">
						<div class="row">
							<div>
								<div class="span8">
								<table border="0" height="150px">
									<tr>
										<td width="180px"><b>NPM</b></td>
										<td width="10px"> : </td>
										<td><?php echo $row->npm?></td>
									</tr>
									<tr>
										<td><b>NAMA</b></td>
										<td> : </td>
										<td><?php echo getNameMhs($row->npm)?></td>
									</tr>
									<tr>
										<td><b>PRESTASI</b></td>
										<td> : </td>
										<td><?php echo $row->prestasi?></td>
									</tr>
									<tr>
										<td><b>BEASISWA</b></td>
										<td> : </td>
										<td><?php echo getBeasiswa($row->beasiswa) ?></td>
									</tr>
									<tr>
										<td><b>Waktu Pengajuan</b></td>
										<td> : </td>
										<td><?php echo datetimeIdn($row->createddate) ?></td>
									</tr>
									<tr>
										<td><b>STATUS</b></td>
										<td> : </td>
										<?php if($row->status == 2){
												$color="style='color:#FF0000'";
											}elseif($row->status == 1){
												$color="style='color:#00FF00'";
											}else{
												$color="";
											}?>
										<td <?php echo $color ?>><?php echo getStatusbeasiswa($row->status) ?></td>
									</tr>
								</table>
								</div>
							</div>
							<div class="span2 pull-right">
								<a href="<?php echo base_url();?>form/beasiswa/cetak/<?php echo $row->id_beasiswa ?>" class="pull-right btn btn-success" title="Cetak Formulir" ><i class="btn-icon-only icon-print"> </i>Cetak</a>
							</div>
						</div>
					</div>
					<br>
					<?php } ?>
					
				</div>
			</div>
		</div>
	</div>
</div>
