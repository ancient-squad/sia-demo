<?php 
$excel = new PHPExcel();
$BStyle = array(
  'borders' => array(
    'allborders' => array(
      'style' => PHPExcel_Style_Border::BORDER_THIN
    )
  )
);
//border
$excel->getActiveSheet()->getStyle('B3:C4')->applyFromArray($BStyle);
$excel->getActiveSheet()->getStyle('E3:F4')->applyFromArray($BStyle);
$excel->getActiveSheet()->getStyle('A6:G15')->applyFromArray($BStyle);

$excel->setActiveSheetIndex(0);
//name the worksheet
$excel->getActiveSheet()->setTitle('Mata Kuliah');
//header
$excel->getActiveSheet()->setCellValue('D1', 'RENCANA PEMBELAJARAN SEMESTER (RPS)');
$excel->getActiveSheet()->setCellValue('D2', get_jur($load->kd_prodi).' '.$thn->tahun_akademik);
$excel->getActiveSheet()->setCellValue('B3', 'Mata Kuliah');
$excel->getActiveSheet()->setCellValue('C3', strtoupper($load->nama_matakuliah));
$excel->getActiveSheet()->setCellValue('B4', 'NID / Nama');
$excel->getActiveSheet()->setCellValue('C4', $user->nid.'/'.$user->nama);
$excel->getActiveSheet()->setCellValue('E3', 'Kode Mata Kuliah');
$excel->getActiveSheet()->setCellValue('F3', $load->kd_matakuliah);
$excel->getActiveSheet()->setCellValue('E4', 'Semester / SKS');
$excel->getActiveSheet()->setCellValue('F4', $load->semester_matakuliah);

//isi mahasiswa
$excel->getActiveSheet()->setCellValue('A6', 'MATA KULIAH');
$excel->getActiveSheet()->setCellValue('C6', 'KODE MATA KULIAH');
$excel->getActiveSheet()->setCellValue('D6', 'RUMPUN MK');
$excel->getActiveSheet()->setCellValue('E6', 'BOBOT/SKS');
$excel->getActiveSheet()->setCellValue('F6', 'SEMESTER');
$excel->getActiveSheet()->setCellValue('G6', 'DIREVISI');
$excel->getActiveSheet()->setCellValue('A7', strtoupper($load->nama_matakuliah));
$excel->getActiveSheet()->setCellValue('C7', $load->kd_matakuliah);
$excel->getActiveSheet()->setCellValue('D7', 'MKK_PRODI');
$excel->getActiveSheet()->setCellValue('E7', $load->sks_matakuliah);
$excel->getActiveSheet()->setCellValue('F7', $load->semester_matakuliah);
$excel->getActiveSheet()->setCellValue('G7', 'TGL_REVISI');
$excel->getActiveSheet()->setCellValue('C8', 'DOSEN PENGEMBANG RPS');
$excel->getActiveSheet()->setCellValue('D8', 'KOORDINATOR RMK');
$excel->getActiveSheet()->setCellValue('F8', 'KA. PRODI');
$excel->getActiveSheet()->setCellValue('C9', $user->nama);
$excel->getActiveSheet()->setCellValue('D9', '-');
$excel->getActiveSheet()->setCellValue('F9', 'NAMA_KAPRODI');

$excel->getActiveSheet()->setCellValue('A10', 'CAPAIAN PEMBELAJARAN');
$excel->getActiveSheet()->setCellValue('B10', 'PROGRAM STUDI');
$excel->getActiveSheet()->setCellValue('C10', get_jur($load->kd_prodi));
$excel->getActiveSheet()->setCellValue('B11', 'MATA KULIAH');
$excel->getActiveSheet()->setCellValue('C11', strtoupper($load->nama_matakuliah));

$excel->getActiveSheet()->setCellValue('A12', 'Deskripsi Singkat Mata Kuliah');
$excel->getActiveSheet()->setCellValue('C12', '---');

$excel->getActiveSheet()->setCellValue('A13', 'Bahan Kajian Mata Kuliah');
$excel->getActiveSheet()->setCellValue('C13', '---');

$excel->getActiveSheet()->setCellValue('A14', 'Pustaka Acuan');
$excel->getActiveSheet()->setCellValue('C14', 'Utama:');
$excel->getActiveSheet()->setCellValue('D14', '');
$excel->getActiveSheet()->setCellValue('C15', '---');

 
//merge cell
$excel->getActiveSheet()->mergeCells('A6:B6');
$excel->getActiveSheet()->mergeCells('D8:E8');
$excel->getActiveSheet()->mergeCells('F8:G8');
$excel->getActiveSheet()->mergeCells('D9:E9');
$excel->getActiveSheet()->mergeCells('F8:G8');
$excel->getActiveSheet()->mergeCells('F9:G9');
$excel->getActiveSheet()->mergeCells('A7:B9');
$excel->getActiveSheet()->mergeCells('A10:A11');
$excel->getActiveSheet()->mergeCells('C10:G10');
$excel->getActiveSheet()->mergeCells('C11:G11');
$excel->getActiveSheet()->mergeCells('A12:B12');
$excel->getActiveSheet()->mergeCells('A13:B13');
$excel->getActiveSheet()->mergeCells('A14:B15');
$excel->getActiveSheet()->mergeCells('C12:G12');
$excel->getActiveSheet()->mergeCells('C13:G13');
$excel->getActiveSheet()->mergeCells('D14:G14');
$excel->getActiveSheet()->mergeCells('C15:G15');
//change the font size
$excel->getActiveSheet()->getStyle('E1:E2')->getFont()->setSize(12);
$excel->getActiveSheet()->getStyle()->getFont()->setSize(11);

//align
$style = array(
    'alignment' => array(
        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
    )
);

//bold text
$excel->getActiveSheet()->getStyle("A6:G9")->getFont()->setBold(true);

// vertical align
$vertical = array(
    'alignment' => array(
        'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
    )
);

$excel->getActiveSheet()->getStyle("E1:E2")->applyFromArray($style);
$excel->getActiveSheet()->getStyle("D1:D2")->applyFromArray($style);
$excel->getActiveSheet()->getStyle("A6:G9")->applyFromArray($style);
$excel->getActiveSheet()->getStyle("A6:G9")->applyFromArray($vertical);
$excel->getActiveSheet()->getStyle("A10:A14")->applyFromArray($vertical);
//$excel->getDefaultStyle()->applyFromArray($style);


// Second Sheet //
// Head
$excel->createSheet(1);
$excel->getSheet(1)->setTitle('Detail Pencapaian');

$excel->getSheet(1)->setCellValue('D1', 'RENCANA PEMBELAJARAN SEMESTER (RPS)');
$excel->getSheet(1)->setCellValue('D2', get_jur($load->kd_prodi).' '.$thn->tahun_akademik);

$excel->getSheet(1)->setCellValue('A3', 'Pendukung:');
$excel->getSheet(1)->setCellValue('B3', '---');
$excel->getSheet(1)->setCellValue('A6', 'Media Pembelajaran');
$excel->getSheet(1)->setCellValue('B6', 'Software:');
$excel->getSheet(1)->setCellValue('C6', 'Hardware:');
$excel->getSheet(1)->setCellValue('B7', '---');
$excel->getSheet(1)->setCellValue('C7', '---');
$excel->getSheet(1)->setCellValue('A8', 'Team Teaching');
$excel->getSheet(1)->setCellValue('B8', '---');
$excel->getSheet(1)->setCellValue('A9', 'Mata Kuliah Syarat');

if ($load->prasyarat_matakuliah != '[]') {
  $array = array('[',']');
  $rep1 = str_replace($array, '', $load->prasyarat_matakuliah);
  $rep2 = explode(',', $rep1);
  $cont = count($rep2);
  $namamk = array();
  for ($i=0; $i < $cont; $i++) { 
    $namamk = $this->app_model->getdetail('tbl_matakuliah','kd_matakuliah',$rep2[$i],'kd_matakuliah','asc')->row()->nama_matakuliah;
  }
  $excel->getSheet(1)->setCellValue('B9', $namamk.', ');
} else {
  $excel->getSheet(1)->setCellValue('B9', '-');
}

// Border style header sheet 2
$excel->getSheet(1)->getStyle('A3:C9')->applyFromArray($BStyle);

// Merge style header sheet 2
$excel->getSheet(1)->mergeCells('A3:A5');
$excel->getSheet(1)->mergeCells('A6:A7');
$excel->getSheet(1)->mergeCells('B8:C8');
$excel->getSheet(1)->mergeCells('B9:C9');

$excel->getSheet(1)->mergeCells('B3:C3');
$excel->getSheet(1)->mergeCells('B4:C4');
$excel->getSheet(1)->mergeCells('B5:C5');

// Bold style header sheet 2
$excel->getSheet(1)->getStyle("A3:A9")->getFont()->setBold(true);
$excel->getSheet(1)->getStyle("B6:C6")->getFont()->setBold(true);

// Vertical align style header sheet 2
$excel->getSheet(1)->getStyle("A3:B9")->applyFromArray($vertical);

// Horizontal align style header sheet 2
$excel->getSheet(1)->getStyle("D1:D2")->applyFromArray($style);
$excel->getSheet(1)->getStyle("D1:D2")->getFont()->setBold(true);



// Content
$excel->getSheet(1)->setCellValue('A11', 'No.');
$excel->getSheet(1)->setCellValue('B11', 'Capaian pembelajaran pertemuan');
$excel->getSheet(1)->setCellValue('C11', 'Kemampuan akhir capaian pemebelajaran (Indikator)');
$excel->getSheet(1)->setCellValue('D11', 'Materi pembelajaran');
$excel->getSheet(1)->setCellValue('E11', 'Metode Pembelajaran');
$excel->getSheet(1)->setCellValue('F11', 'Pengalaman belajar');
$excel->getSheet(1)->setCellValue('G11', 'Kriteria penilaian (Indikator, bentuk, & bobot penilaian)');
$excel->getSheet(1)->setCellValue('H11', 'Estimasi waktu');

$excel->getSheet(1)->setCellValue('A12', '(1)');
$excel->getSheet(1)->setCellValue('B12', '(2)');
$excel->getSheet(1)->setCellValue('C12', '(3)');
$excel->getSheet(1)->setCellValue('D12', '(4)');
$excel->getSheet(1)->setCellValue('E12', '(5)');
$excel->getSheet(1)->setCellValue('F12', '(6)');
$excel->getSheet(1)->setCellValue('G12', '(7)');
$excel->getSheet(1)->setCellValue('H12', '(8)');

$excel->getSheet(1)->setCellValue('A13', 'Pertemuan');
$excel->getSheet(1)->setCellValue('B13', 'Kemampuan akhir yang direncanakan pada tiap tahap pembelajaran');
$excel->getSheet(1)->setCellValue('C13', 'Penanda ketercapaian capaian pembelajaran disetiap pertemuan');
$excel->getSheet(1)->setCellValue('D13', 'Materi pembelajaran yang digunakan untuk membantu pencapaian pembelajaran perteuan dan landasan pengembangan nilai keislaman');
$excel->getSheet(1)->setCellValue('E13', 'Sistem deliveri yang digunakan dosen');
$excel->getSheet(1)->setCellValue('F13', 'Uraian spesifik tentang aktivitas atau tugas yang disiapkan oleh dosen kepada mahasiswa dengan mengacu pada metode pembelajaran yang dipilih');
$excel->getSheet(1)->setCellValue('G13', 'Teknik penilaian');
$excel->getSheet(1)->setCellValue('H13', 'Estimasi waktu yang diperlukan');

$excel->getSheet(1)->setCellValue('A14', '');
$excel->getSheet(1)->setCellValue('B14', '');
$excel->getSheet(1)->setCellValue('C14', '');
$excel->getSheet(1)->setCellValue('D14', '');
$excel->getSheet(1)->setCellValue('E14', '');
$excel->getSheet(1)->setCellValue('F14', '');
$excel->getSheet(1)->setCellValue('G14', '');
$excel->getSheet(1)->setCellValue('H14', '');

// Border style content 
$excel->getSheet(1)->getStyle('A11:H14')->applyFromArray($BStyle);

// Bold style content sheet 2
$excel->getSheet(1)->getStyle("A12:H12")->getFont()->setBold(true);

// Font size content sheet 2
$excel->getActiveSheet()->getStyle('A12:H13')->getFont()->setSize(8);

// Horizontal align style header sheet 2
$excel->getSheet(1)->getStyle("A11:H13")->applyFromArray($style);

// Vertical align style header sheet 2
$excel->getSheet(1)->getStyle("A11:H13")->applyFromArray($vertical);


$filename = 'Form_RPS.xls'; //save our workbook as this file name
header('Content-Type: application/vnd.ms-excel'); //mime type
header('Content-Disposition: attachment;filename="RPS'); //tell browser what's the file name
header('Cache-Control: max-age=0'); //no cache
$objWriter = PHPExcel_IOFactory::createWriter($excel, 'Excel5');  
//force user to download the Excel file without writing it to server's HD
$objWriter->save('php://output');
?>