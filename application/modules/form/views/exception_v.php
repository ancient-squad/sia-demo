<script type="text/javascript">
  $(document).ready(function() {
    $('#tgl_skep').datepicker({
      dateFormat: 'yy-mm-dd',
      yearRange: "1945:2015",
      changeMonth: true,
      changeYear: true
    });
  });
</script>

<script type="text/javascript">
  jQuery(document).ready(function($) {
    $('input[name^=nid]').autocomplete({
      source: '<?php echo base_url('form/excepupload/load_dosen');?>',
      minLength: 1,
      select: function (evt, ui) {
        this.form.nid.value = ui.item.value;
      }
    });
  });
</script>

<script type="text/javascript">
  function getTable() {
    $.post('<?php echo base_url(); ?>form/excepupload/loadTable/', function(data) {
      $('#appearHere').html(data);
      });
    }

  $(function () {
    $('#throw').click(function (e) {
      $.ajax({
        type: 'POST',
        url: '<?= base_url('form/excepupload/outtemporary'); ?>',
        data: $('#temporaryform').serialize(),
        error: function (xhr, ajaxOption, thrownError) {
          return false;
        },
        success: function () {
                  getTable();
                  $('#toRemove').hide();
                  $('#nid').val('');
        }
      });
      e.preventDefault();
    });
  });

  function rmData(id) {
      //alert(id);
      $.ajax({
          type: 'POST',
          url: '<?= base_url('form/excepupload/deleteList/');?>'+id,
          error: function (xhr, ajaxOptions, thrownError) {
              return false;           
          },
          success: function () {
              getTable();
          }
      });
  }
</script>

<script type="text/javascript">
   $(function () {
    
    $("#frmAdd").submit( function (e) {
      var row = $('#example99 tr').eq(1).find('td').eq(1).html()

      if (row == null) {
        alert('Add Dosen First')
        return false
      }
    })
   })
</script>

<div class="row">
  <div class="span12">                
      <div class="widget ">
        <div class="widget-header">
          <i class="icon-check"></i>
          <h3>Form akses pembukaan unggah nilai ujian</h3>
        </div> <!-- /widget-header -->
      
        <div class="widget-content">
          <fieldset>
            <a href="<?= base_url('form/excepupload/listDosen') ?>" class="btn btn-warning"><< Kembali</a>
            <hr>
            <form class="form-horizontal" id="temporaryform" method="post">
              <div class="control-group">
                <label class="control-label">Tambah Dosen</label>
                <div class="controls">
                  <input class="form-control span8" type="text" id="nid" placeholder="Isi NID/Nama Dosen" name="nid" required>
                  <button class="btn btn-success" id="throw">
                    <i class="icon icon-plus"></i> Tambah ke daftar
                  </button>
                </div>
              </div>
              <hr>
            </form>
            <form class="form-horizontal" action="<?php echo base_url(); ?>form/excepupload/add" method="post" id="frmAdd">
              <h3><i class="icon icon-list"></i> Daftar dosen</h3>
              <br>
              <table id="example99" class="table table-bordered table-striped">
                <thead>
                    <tr> 
                      <th>No</th>
                      <th>NID</th>
                      <th>Nama Dosen</th>
                      <th width="80">Aksi</th>
                    </tr>
                </thead>
                <tbody id="appearHere">
                    <tr id="toRemove">
                      <td colspan="6"><i>No data available</i></td>
                    </tr>
                </tbody>
              </table>
              <div class="form-actions">
                <input class="btn btn-default btn-primary" type="submit" value="Submit" id="sbmt">
              </div>
            </form>
          </fieldset>    
        </div>
      </div>
  </div>
</div>