<?php

ob_start();

$pdf = new FPDF("P","mm", "A4");

$pdf->AliasNbPages();

$pdf->AddPage();

$pdf->SetMargins(3, 5 ,0);

// $pdf->SetFont('Arial','B',16); 

// $pdf->image(base_url().'/assets/logo.gif',10,10,20);

// $pdf->Ln(3);
// $pdf->Cell(200,10,'UNIVERSITAS BHAYANGKARA',0,5,'C');
// $pdf->Cell(200,5,'JAKARTA RAYA',0,1,'C');

// $pdf->Ln(0);
// $pdf->SetFont('Arial','U',14);
// $pdf->Cell(200,10,'______________________________________________________________________',0,5,'C');

$pdf->Ln(10);
$pdf->Cell(200,30,'',0,5,'C');

if ($list->tipe == 'P') {
	$titl = 'Pindah';
} else {
	$titl = 'Pengunduran Diri';
} 

$pdf->ln(5);
$pdf->SetFont('Arial','BU',13); 
$pdf->Cell(200,5,'SURAT KETERANGAN '.strtoupper($titl).'',0,0,'C');

$pdf->ln(5);
$pdf->SetFont('Arial','',13);
$tanggal = date('m');
$pdf->Cell(200,5,'Nomor: KET/ .../'.romawi($tanggal).'/ '.date('Y').'/'. $this->ORG_NAME,0,1,'C');

$pdf->ln(5);
$pdf->SetFont('Arial','',12); 
$pdf->Cell(5,5,'',0,0);
$pdf->MultiCell(185,5,'Pimpinan '.$this->ORG_NAME.' dengan ini menerangkan bahwa mahasiswa dengan identitas berikut:',0,'L');

$pdf->ln(5);
$pdf->SetFont('Arial','','12');
$pdf->Cell(15,5,'',0,0);
$pdf->Cell(65,5,'Nama',0,0);
$pdf->Cell(5,5,':',0,'L');
$pdf->Cell(10,5,get_nm_mhs($list->npm),0,1);

$pdf->ln(1);
$pdf->SetFont('Arial','','12');
$pdf->Cell(15,5,'',0,0);
$pdf->Cell(65,5,'NPM / NIM',0,0);
$pdf->Cell(5,5,':',0,'L');
$pdf->Cell(10,5,$list->npm,0,1);

$pdf->ln(1);
$pdf->SetFont('Arial','','12');
$pdf->Cell(15,5,'',0,0);
$pdf->Cell(65,5,'Semester',0,0);
$pdf->Cell(5,5,':',0,'L');
$pdf->Cell(10,5,$smst,0,1);

$pdf->ln(1);
$pdf->SetFont('Arial','','12');
$pdf->Cell(15,5,'',0,0);
$pdf->Cell(65,5,'Fakultas / Prodi',0,0);
$pdf->Cell(5,5,':',0,'L');
$pdf->Cell(5,5,get_fak_byprodi($list->prodi).' / '.get_jur($list->prodi),0,1,'L');

switch ($list->kelas) {
	case 'PG':
		$kelas = 'A';
		break;

	case 'SR':
		$kelas = 'B';
		break;
		
	case 'KY':
		$kelas = 'C';
		break;
}

$pdf->ln(1);
$pdf->SetFont('Arial','','12');
$pdf->Cell(15,5,'',0,0);
$pdf->Cell(65,5,'Kelas',0,0);
$pdf->Cell(5,5,':',0,'L');
$pdf->Cell(27,5,$kelas,0,1);

$pdf->ln(1);
$pdf->SetFont('Arial','','12');
$pdf->Cell(15,5,'',0,0);
$pdf->Cell(65,5,'Jumlah SKS yang ditempuh',0,0);
$pdf->Cell(5,5,':',0,'L');
$pdf->Cell(27,5,$list->sks,0,1);

$pdf->ln(1);
$pdf->SetFont('Arial','','12');
$pdf->Cell(15,5,'',0,0);
$pdf->Cell(65,5,'No.Telp / HP',0,0);
$pdf->Cell(5,5,':',0,'L');
$pdf->Cell(10,5,$list->tlp,0,1);
//$pdf->Cell(60,10,$q->nama,0,0,'L')MultiCell(60, 10, $q->nama, 0,'L');
// $pdf->MultiCell(50, 5,0,'L');
$pdf->ln(1);
$pdf->SetFont('Arial','',11);
$pdf->Cell(15,5,'',0,0);
$pdf->Cell(65,5,'Alamat',0,0);
$pdf->Cell(5,5,':',0,'L');
$pdf->MultiCell(80,5,trim($list->alamat),0,'L');

if ($list->tahunakademik%2 == 0) {
	$smt = 'Genap';
} else {
	$smt = 'Ganjil';
}

// destinasi
if ($list->tipe == 'P') {
	$content = 'Adalah benar sebagai mahasiswa/i di Fakultas '.get_fak_byprodi($list->prodi).'. Berdasarkan surat permohonan pindah mahasiswa pada tanggal '.TanggalIndo($list->tanggal).'. Semester '.$smt.' tahunakademik '.get_thajar($list->tahunakademik).' mahasiswa/i tersebut mengajukan pindah kuliah dari Fakultas '.get_fak_byprodi($list->prodi).' '.$this->ORG_NAME.' ke '.$list->destinasi.'. Terlampir Kartu Hasil Studi mahasiswa tersebut.';
} else {
	$content = 'Adalah benar sebagai mahasiswa/i di Fakultas '.get_fak_byprodi($list->prodi).'. Berdasarkan surat permohonan pengunduran diri mahasiswa yang bersangkutan pada tanggal '.TanggalIndo($list->tanggal).'. Maka pada semester '.$smt.' tahunakademik '.get_thajar($list->tahunakademik).' mahasiswa/i tersebut tidak dapat melakukan kegiatan akademik di Fakultas '.get_fak_byprodi($list->prodi).' Program Studi '.get_jur($list->prodi).' di '.$this->ORG_NAME.'.';
}

$pdf->ln(5);
$pdf->Cell(5,5,'',0,0);
$pdf->MultiCell(0,5,$content,0,'L');

$pdf->ln(1);
$pdf->Cell(5,10,'',0,0);
$pdf->Cell(10,10,'Demikian permohonan ini saya buat untuk dapat diperhatikan.',0,1);

$pdf->ln(4);
$pdf->SetMargins(3, 5 ,0);
$pdf->SetFont('Arial','',11);
$pdf->Cell(3,10,'',0,0);

date_default_timezone_set('Asia/Jakarta'); 
$pdf->Cell(165,5,'Bekasi, '.date('d-m-Y').'',0,0,'R');

$pdf->ln();
$pdf->Cell(100,5,'',0,0,'L');
$pdf->SetFont('Arial','',11);
$pdf->Cell(100,5,'a.n. Rektor '.$this->ORG_NAME.',',0,0,'C');

$pdf->ln();
$pdf->Cell(100,5,'',0,0,'L');
$pdf->SetFont('Arial','',11);
$pdf->Cell(100,5,'ub. Wakil Rektor I',0,0,'C');

$pdf->ln();
$pdf->Cell(100,5,'',0,0,'L');
$pdf->SetFont('Arial','',11);
$pdf->Cell(100,5,'Kepala Biro Administrasi Akademik',0,0,'C');

$pdf->ln(25);
$pdf->SetFont('Arial','U',11);
$pdf->Cell(10,0,'',0,'C');
$pdf->Cell(100,5,'',0,0,'L');
$pdf->Cell(80,5,'Rouly G Ratna Silalahi,ST.,MM',0,0,'C');

$pdf->ln(7);
$pdf->SetFont('Arial','U',11);
$pdf->Cell(110,5,'',0,0,'C');
$pdf->Cell(80,5,'NIP: 0607108',0,0,'C');

$pdf->Ln(20);
$pdf->SetFont('Arial','',9);
$pdf->Cell(143,5,'Tembusan yth :',0,0,'L');
// $pdf->Cell(80,5,'Mengeluarkan,',0,0,'L');

$pdf->Ln(4);
$pdf->Cell(148,5,'1. Rektor',0,0,'L');
// $pdf->Cell(80,5,'Ka.BAA',0,0,'L');

$pdf->Ln(4);
$pdf->Cell(148,5,'2. Fakultas '.get_fak_byprodi($list->prodi).'',0,0,'L');
// $pdf->Cell(80,5,'Ka.BAA',0,0,'L');

$pdf->Ln(4);
$pdf->Cell(130,5,'3. Biro Administrasi Akademik',0,0,'L');
$pdf->Cell(80,5,'',0,0,'L');

$pdf->Ln(4);
$pdf->Cell(130,5,'4. Biro Perencanaan Anggaran dan Keuangan',0,0,'L');
$pdf->Cell(80,5,'',0,0,'L');

$pdf->ln(6);
$pdf->SetFont('Arial','',8);
date_default_timezone_set('Asia/Jakarta'); 
$pdf->Cell(110,5,'Print  : '.date('d-m-Y H:i').'',0,0,'L');






//exit();
$pdf->Output();
ob_end_flush();
?>