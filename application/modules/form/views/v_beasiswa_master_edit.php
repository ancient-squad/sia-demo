<div class="modal-header">
	<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
	<h4 class="modal-title">Ubah Beasiswa</h4>
</div>
<form class ='form-horizontal' action="<?php echo base_url();?>form/beasiswa/edit_master/" method="post">
	<div class="modal-body" style="margin-left: -30px;">    
		<div class="control-group" id="">
			<label class="control-label">Kode Beasiswa</label>
			<div class="controls">
				<input type="text" class="span4" name="kd_beasiswa" placeholder="Max 3 Karakter" class="form-control" value="<?php echo $beasiswa->kd_beasiswa?>" maxlength="3" readonly required/>
			</div>
		</div>
		<div class="control-group" id="">
			<label class="control-label">Nama Beasiswa</label>
			<div class="controls">
				<input type="text" class="span4" name="nama_beasiswa" placeholder="Nama Beasiswa" class="form-control" value="<?php echo $beasiswa->nama_beasiswa?>" required/>
			</div>
		</div>
		<?php 	
			$Startyear=date('Y');
			$endYear=$Startyear+3;
			$yearArray = range($Startyear,$endYear);
		?>
		<div class="control-group" id="">
			<label class="control-label">Periode</label>
			<div class="controls">
				<select name="periode" class="span4" class="form-control" required>
				<?php
					foreach ($yearArray as $year) {
					$selected = ($year == $beasiswa->periode) ? 'selected' : '';
				?>
					<option value="<?php echo $year?>" <?php echo $selected?>><?php echo $year?></option>
					<?php } ?>
				</select>
			</div>
		</div>
		<div class="control-group" id="">
			<label class="control-label">Status</label>
			<div class="controls"> 
				<input name="isactivated" value="1" type="radio" data-toggle="toggle" <?php if($beasiswa->isactivated == 1){echo 'checked';}?> required> Aktif
				<br>
				<input name="isactivated" value="0" type="radio" data-toggle="toggle" <?php if($beasiswa->isactivated == 0){echo 'checked';}?> required> Tidak Aktif
			</div>
		</div>
	</div>
	<div class="modal-footer">
	<button type="submit" class="btn btn-success">Simpan</button>
		<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	</div>
</form>