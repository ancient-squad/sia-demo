<div class="row">
    <div class="span12">
        <div class="widget ">
            <div class="widget-header">
                <i class="icon-group"></i>
                <h3>
                    <?= (!in_array(6, $usergroup) || !in_array(7, $usergroup)) ? nama_dsn($dosen) . ' - ' : ''; ?>
                    <?= $detail ?>
                </h3>
            </div>

            <!-- if user is not lecturer disabled all -->
            <?php (in_array(10, $usergroup)) ? $disabled_all = 'disabled=""' : $disabled_all = ''; ?>

            <div class="widget-content">
                <?php if ($this->session->flashdata('fail_storing')) { ?>
                    <div class="alert alert-danger alert-dismissible">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <?= $this->session->flashdata('fail_storing'); ?>
                    </div>
                <?php } ?>
                <form action="<?= base_url('form/nilai/store_ta/' . $id_jadwal) ?>" method="POST">
                    <div class="span11">
                        <a href="<?= base_url('form/nilai/lecturer_list') ?>" class="btn btn-warning pull-left" style="margin-right: 5px">
                            <i class="icon-chevron-left"></i> Kembali
                        </a>
                        <button type="submit" class="btn btn-success pull-left" style="margin-right: 5px" >
                            <i class="icon-save"></i> Simpan
                        </button>

                        <a 
                            href="<?= base_url(); ?>form/formnilai/cetak_nilai/<?= $id_jadwal; ?>" 
                            target="_blank" 
                            style="margin-right: 5px" 
                            class="btn btn-primary pull-left"><i class="icon-print"></i> Cetak 
                        </a>

                        <button 
                            type="button" 
                            data-toggle="modal" 
                            data-target="#info" 
                            class="btn btn-warning pull-left">
                            <i class="icon-pushpin"></i> Petunjuk
                        </button>

                        <table id="example8" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>NPM</th>
                                    <th>Nama</th>
                                    <th width="80">Nilai Akhir</th>
                                    <th width="80">Grade</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $no = 1; foreach ($participants as $row) : ?>
                                    <tr>
                                        <td><?= $no ?></td>
                                        <td><?= $row->NIMHSMSMHS ?></td>
                                        <td>
                                            <?= $row->NMMHSMSMHS ?>
                                            <input type="hidden" name="npm[<?= $row->NIMHSMSMHS ?>]" value="<?= $row->NIMHSMSMHS ?>" />
                                        </td>
                                        <td id="uas-<?= $row->NIMHSMSMHS ?>">
                                            <input 
                                                value="<?= ($row->final == 0.00) ? NULL : $row->final; ?>" 
                                                maxlength="5" 
                                                placeholder="nilai akhir" 
                                                name="final[<?= $row->NIMHSMSMHS ?>]" 
                                                type="text" class="final<?= $row->NIMHSMSMHS ?> span1" 
                                                oninput="isValid(this)"
                                                <?= $disabled_all ?>/>
                                        </td>
                                        <td id="grade-<?= $row->NIMHSMSMHS ?>"><?= getValueRange($row->final) ?></td>
                                        <!-- matakuliah -->
                                        <input type="hidden" name="kode_matakuliah" value="<?= $matakuliah ?>">
                                    </tr>
                                <?php $no++;
                                endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<br>

<div id="info" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Petunjuk</h4>
            </div>
            <div class="modal-body">
                <p>Pengisian nilai untuk satu matakuliah dapat dilakukan berkala, tidak harus sekaligus. Dengan catatan Anda telah menekan tombol <b><u>simpan</u></b> terlebih dahulu. Kemudian Anda dapat melanjutkan penilaian yang belum diselesaikan.</p>
                <p>
                    <b><u>Harap diperhatikan! Jika nilai mahasiswa adalah TUNDA, maka beri poin 101 untuk nilai akhirnya.</u></b>
                </p>
                <p>
                    * <b><u>PERHATIKAN</u></b> penggunaan kolom <i>SEARCH</i> ketika anda menyimpan data. Data yang tersimpan hanya data yang ditampilkan pada monitor. Mekanisme penyimpanan menggunakan metode <i>replace</i> sehingga data lama akan diganti oleh data terbaru. Sehingga nilai mahasiswa yang tidak muncul pada <i>screen</i> akan dianggap <b><u>0 (nol)</u></b>. Oleh karena itu pastikan kolom <i>SEARCH</i> dalam keadaan kosong ketika Anda menyimpan data.
                </p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>

    </div>
</div>

<script>
    // input validation
    var RegEx = new RegExp(/^\d*\.?\d*$/);

    function isValid(elem) {
        if (!RegEx.test(elem.value)) {
            elem.value = '';
        }
        if (elem.value.substr(0, 1) == '.') {
            elem.value = '';
        }
    }
</script>