<?php

ob_start();

$pdf = new FPDF("P","mm", "A4");

$pdf->AliasNbPages();

$pdf->AddPage();

$pdf->SetMargins(3, 5 ,0);

$pdf->SetFont('Arial','',14); 



$pdf->image(base_url().'/assets/albino.png',10,10,20);

$pdf->Ln(0);

$pdf->Cell(200,5,'KARTU HASIL STUDI',0,3,'C');

$pdf->Ln(2);

$pdf->Cell(200,10,'SEMESTER PERBAIKAN',0,5,'C');

$pdf->Ln(0);

$pdf->Cell(200,10,$this->ORG_NAME,0,1,'C');

// $pdf->Ln(3);

// $pdf->Cell(250,0,'',1,1,'L');



$pdf->ln(7);

$pdf->SetFont('Arial','',9);

$pdf->Cell(10,10,'',0,0);

$pdf->Cell(35,5,'Fakultas',0,0,'L');

$pdf->Cell(5,5,':',0,0);

$pdf->Cell(75,5,$prodi->fakultas,0,0);

$pdf->Cell(30,5,'Semester',0,0);

$pdf->Cell(5,5,':',0,0);

$pdf->Cell(5,5,$mhs->semester_krs,0,0);


$pdf->ln(3);

$pdf->Cell(10,10,'',0,0);

$pdf->Cell(35,10,'Program Studi',0,0,'L');

$pdf->Cell(5,10,':',0,0);

$pdf->Cell(75,10,$prodi->prodi,0,0);

$pdf->Cell(30,10,'Tahun Akademik',0,0);

$pdf->Cell(5,10,':',0,0);

$pdf->Cell(5,10,$mhs->tahunajaran,0,0);



$pdf->ln(6);

$pdf->SetFont('Arial','B',9);

$pdf->Cell(10,10,'',0,0);

$pdf->Cell(115,10,'Identitas Mahasiswa',0,0,'L');

$pdf->Cell(35,10,'Identitas Dosen',0,0);



$pdf->ln(6);

$pdf->SetFont('Arial','',9);

$pdf->Cell(10,10,'',0,0);

$pdf->Cell(35,10,'Nama Mahasiswa',0,0,'L');

$pdf->Cell(5,10,':',0,0);

$pdf->Cell(75,10,$mhs->NMMHSMSMHS,0,0);

$pdf->Cell(30,10,'Nama Dosen PA',0,0);

$pdf->Cell(5,10,':',0,0);

$pdf->MultiCell(40,5,$idp->nama,0,'L',0);
$pdf->SetXY(178,69);


$pdf->ln(0);

$pdf->Cell(10,10,'',0,0);

$pdf->Cell(35,10,'NPM',0,0,'L');

$pdf->Cell(5,10,':',0,0);

$pdf->Cell(75,10,$mhs->NIMHSMSMHS,0,0);

$pdf->Cell(30,10,'NID',0,0);

$pdf->Cell(5,10,':',0,0);

$pdf->Cell(5,10,$idp->nid,0,1);



$pdf->ln(3);

$pdf->SetFont('Arial','',9); 

$pdf->Cell(10,10,'',0,0);

$pdf->Cell(7,10,'No',1,0,'C');

$pdf->Cell(23,10,'Kode MK',1,0,'C');

$pdf->Cell(50,10,'MK',1,0,'C');

$pdf->Cell(10,10,'SKS',1,0,'C');

$pdf->Cell(50,10,'Nama Dosen',1,0,'C');

$pdf->MultiCell(25,5,'Nilai Sebelumnya',1,'C',0);

$pdf->SetXY(178,82);

$pdf->Cell(25,10,'Nilai Akhir',1,0,'C');

$pdf->SetXY(10,92);


$pdf->ln(0);

$pdf->SetFont('Arial','',9); 
$no = 1;
foreach ($query as $key) {
	$pdf->Cell(10,5,'',0,0);

	$pdf->Cell(7,5,$no,1,0,'C');

	$pdf->Cell(23,5,$key->kd_matakuliah,1,0,'C');

	$pdf->MultiCell(50,5,$key->nama_matakuliah,1,'C',0);
	$pdf->SetXY(93,92);

	$pdf->Cell(10,5,$key->sks_matakuliah,1,0,'C');

	$pdf->MultiCell(50,5,$key->nama,1,'L',0);
	$pdf->SetXY(153,92);

	$th = $key->THSMSTRLNM - 2;
	$bfr = $this->db->query("SELECT * from tbl_transaksi_nilai where THSMSTRLNM = '".$th."' and NIMHSTRLNM = '".$key->NIMHSTRLNM."' and KDKMKTRLNM = '".$key->kd_matakuliah."'")->row();

	$pdf->Cell(25,5,$bfr->NLAKHTRLNM,1,0,'C');

	$pdf->Cell(25,5,$key->NLAKHTRLNM.' ('.$key->nilai.')',1,1,'C');

	$no++;
}







$pdf->ln(8);

$pdf->SetMargins(3, 5 ,0);

$pdf->SetFont('Arial','',9);
$pdf->Cell(5,10,'',0,0);

date_default_timezone_set('Asia/Jakarta'); 

$pdf->Cell(170,5,'Jakarta,'.date('d-m-Y').'',0,0,'R');



$pdf->ln(8);

$pdf->SetFont('Arial','',9);

$pdf->Cell(20,5,'',0,0,'C');

$pdf->Cell(30,5,'Mengetahui',0,0,'C');

$pdf->Cell(60,5,'',0,0,'C');

$pdf->Cell(10,5,'',0,0,'C');

$pdf->Cell(10,5,'',0,0,'C');

// $pdf->Cell(80,5,'Mahasiswa',0,0,'C');

$pdf->Cell(60,5,'',0,0,'L');

$pdf->Cell(10,5,'',0,1,'C');


$pdf->ln(1);

$pdf->SetFont('Arial','',9);

$pdf->Cell(20,10,'',0,0);
$pdf->Cell(140,7,'Penasehat Akademik',0,0);
$pdf->Cell(10,5,'Mahasiswa',0,1,'C');


$pdf->Ln();
$pdf->Ln();
$pdf->Ln();
$pdf->Ln();
$pdf->SetFont('Arial','B',9);
$pdf->Cell(10,10,'',0,0);
$pdf->Cell(160,7,$idp->nama,0,0,'L');
$pdf->Cell(10,7,$mhs->NMMHSMSMHS,0,0,'R');


$pdf->ln(1);

$pdf->SetFont('Arial','B',9);

$pdf->Cell(8,10,'',0,1);


$pdf->Ln();

$pdf->SetFont('Arial','',9);
$pdf->Cell(12,10,'',0,0);
$pdf->Cell(115,10,'',0,0);
$pdf->Cell(140,10,'',0,1);


$pdf->Ln();
$pdf->SetFont('Arial','',9);
$pdf->Cell(90,10,'',0,0);
$pdf->Cell(10,7,'Disetujui',0,0);

$pdf->Ln();
$pdf->SetFont('Arial','',9);
$pdf->Cell(80,10,'',0,0);
$pdf->Cell(10,7,'Dekan/Wakil Dekan 1',0,1);
// $pdf->ln(30);

$pdf->Ln();
$pdf->Ln();
$pdf->Ln();
$pdf->SetFont('Arial','B',9);
$pdf->Cell(70,10,'',0,0);
$pdf->Cell(10,7,'(........................................................)',0,0);

$pdf->Output();


ob_end_flush();
?>

