<div class="row">
  <div class="span12">                
    <div class="widget ">
      <div class="widget-header">
        <i class="icon-calendar"></i>
        <h3>Pilih Tahun Ajaran</h3>
      </div>

      <div class="widget-content">
        <div class="span11">
          <form method="post" class="form-horizontal" action="<?= base_url(); ?>form/nilai/set_tahunajar">
            <fieldset>
              <!-- for baa -->
              <?php if (in_array(10, $usergroup)) : ?>
                <div class="control-group">
                  <label class="control-label">Prodi</label>
                  <div class="controls">
                    <select class="form-control span6" name="prodi" id="prodi" required>
                      <option selected="" value="">-- Pilih Prodi --</option>
                      <?php foreach ($prodi as $prodis) { ?>
                      <option value="<?= $prodis->kd_prodi;?>"><?= $prodis->prodi;?></option>
                      <?php } ?>
                    </select>
                  </div>
                </div>
              <?php endif; ?>
              <div class="control-group">
                <label class="control-label">Tahun Akademik</label>
                <div class="controls">
                  <select class="form-control span6" name="tahunajaran" id="tahunajaran" required>
                    <option selected="" value="">-- Pilih Tahun Akademik --</option>
                    <?php foreach ($tahunajaran as $row) { ?>
                    <option value="<?= $row->kode;?>"><?= $row->tahun_akademik;?></option>
                    <?php } ?>
                  </select>
                </div>
              </div>
              <div class="form-actions">
                <input type="submit" class="btn btn-large btn-success" value="Submit"/> 
              </div>
            </fieldset>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>