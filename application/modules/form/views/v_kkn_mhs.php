
<div class="row">
	<div class="span12">      		  		
  		<div class="widget ">
  			<div class="widget-header">
  				<i class="icon-user"></i>
  				<h3>Kuliah Kerja Nyata</h3>
			</div> <!-- /widget-header -->
			<div class="widget-content">
				<div class="span11">
					<?php if($formulir->status != 2){?>
						<a href="#" class="btn btn-primary" title="Formulir KKN" disabled><i class="btn-icon-only icon-file"> </i>Formulir sudah diisi</a>
					<?php }else{ ?>
						<a href="<?php echo base_url();?>form/kkn/isi" class="btn btn-primary" title="Formulir KKN" ><i class="btn-icon-only icon-file"> </i>Formulir</a>
					<?php } ?>
					<hr>
					<?php foreach($list_formulir AS $row) {?>
					<div class="thumbnail">
						<div class="row">
							<div>
								<div class="span8">
								<table border="0" height="150px">
									<tr>
										<td width="180px"><b>NPM</b></td>
										<td width="10px"> : </td>
										<td><?php echo $row->npm?></td>
									</tr>
									<tr>
										<td><b>NAMA</b></td>
										<td> : </td>
										<td><?php echo getNameMhs($row->npm)?></td>
									</tr>
									<tr>
										<td><b>PENYAKIT</b></td>
										<td> : </td>
										<td><?php echo $row->penyakit?></td>
									</tr>
									<tr>
										<td><b>KKN</b></td>
										<td> : </td>
										<td><?php echo getKKN($row->kkn) ?></td>
									</tr>
									<tr>
										<td><b>Waktu Pengajuan</b></td>
										<td> : </td>
										<td><?php echo datetimeIdn($row->createddate) ?></td>
									</tr>
								</table>
								</div>
							</div>
							<div class="span2 pull-right">
								<a href="<?php echo base_url();?>form/kkn/cetak/<?php echo $row->id_kkn ?>" class="pull-right btn btn-success" title="Cetak Formulir" ><i class="btn-icon-only icon-print"> </i>Cetak</a>
							</div>
						</div>
					</div>
					<br>
					<?php } ?>
					
				</div>
			</div>
		</div>
	</div>
</div>
