<script type="text/javascript">
$(document).ready(function() {
	$('#form_rubah').hide();

	 $("input[name$='data_ubah']").click(function() {
        var test = $(this).val();
        if (test == 'F') {
        	$('#form_rubah').show();
        }else{
        	$('#form_rubah').hide();
        };
    });
}); 
</script>

<div class="row">
	<div class="span12" id="form_pmb">      		  		
  		<div class="widget ">
  			<div class="widget-header">
                <i class="icon-user"></i>
  				<h3>Form Pendaftaran Sidang</h3>
			</div>
			<div class="widget-content">
				<form action="<?php echo base_url(); ?>form/formsidyud/simpan" enctype="multipart/form-data" method="post"  class="form-horizontal">
					<fieldset>
						<h3>Data Diri</h3>
						<div class="control-group">
							<label class="control-label">NPM</label>
							<div class="controls">
								<input type="hidden" class="form-control span6" name="npm_mhs" value="<?php echo $rows->nomor_pokok ?>" readonly>
								<input type="text" class="form-control span6" name="npm" value="<?php echo $rows->nomor_pokok ?>" disabled><br>
								<!-- <small>*sesuai ktp / kartu keluarga</small> -->
							</div>
						</div>
						<div class="control-group">
							<label class="control-label">Nama</label>
							<div class="controls">
								<input type="text" class="form-control span6"  name="nama" value="<?php echo $rows->nama ?>" disabled><br>
								<small>*Pastikan nama anda sesuai dengan ijazah terahir.</small>
							</div>
						</div>
						<div class="control-group">
							<label class="control-label">Nama Ibu</label>
							<div class="controls">
								<input type="text" class="form-control span6"  name="namaibu" value="<?php echo strtoupper($rows->ibu);?>" disabled><br>
								<small>*Sesuai akta kelahiran.</small>
							</div>
						</div>
						<div class="control-group">
							<label class="control-label">Tempat/Tanggal Lahir</label>
							<div class="controls">
								<input type="text" class="form-control span4"  name="tempatlahir" value="<?php echo $rows->tempat_lahir ?>" disabled>&nbsp;/&nbsp;
								<input type="text" class="form-control span2"  name="tanggallahir" value="<?php echo $rows->tanggal_lahir ?>" disabled><br>
								<small>*Sesuai akta kelahiran anda(bukan Ibu anda).</small>
							</div>
						</div>
						
						<div class="control-group" id="new4">
							<label class="control-label">Kesesuaian Data </label>
							<div class="controls">
								<input type="radio" name="data_ubah" value="T" required> Sesuai Akta Kelahiran &nbsp;&nbsp;
								<input type="radio" name="data_ubah" value="F" required> Tidak Sesuai Akta Kelahiran&nbsp;&nbsp;
							</div>
						</div>
						
						<div id="form_rubah">
						<div class="control-group">
							<label class="control-label">Nama</label>
							<div class="controls">
								<input type="text" class="form-control span6"  name="namaedit" value="<?php echo $rows->nama ?>"><br>
							</div>
						</div>
						<div class="control-group">
							<label class="control-label">Nama Ibu</label>
							<div class="controls">
								<input type="text" class="form-control span6"  name="namaibuedit" value="<?php echo strtoupper($rows->ibu);?>"><br>
							</div>
						</div>
						<div class="control-group">
							<label class="control-label">Tempat/Tanggal Lahir</label>
							<div class="controls">
								<input type="text" class="form-control span4"  name="tempatlahiredit" value="<?php echo $rows->tempat_lahir ?>">&nbsp;/&nbsp;
								<input type="text" class="form-control span2"  name="tanggallahiredit" value="<?php echo $rows->tanggal_lahir ?>"><br>
							</div>
						</div>
						
						</div>	
						<br>
						<div class="control-group">
							<label class="control-label">Upload Akta Kelahiran</label>
							<div class="controls">
								<input type="file" name="userfile[0]" required><br>
								<small>*Upload Akta Kelahiran anda.</small>
							</div>
						</div>
						<div class="control-group">
							<label class="control-label">Upload Ijazah</label>
							<div class="controls">
								<input type="file" name="userfile[1]" required><br>
								<small>*Upload ijazah terahir anda.</small>
							</div>
						</div>
						<br>
						<hr>
						<h3>Upload Skripsi</h3>
						<div class="control-group">
							<label class="control-label">Bab I</label>
							<div class="controls">
								<input type="file" name="userfile[2]" required><br>
							</div>
						</div>
						<div class="control-group">
							<label class="control-label">Bab II</label>
							<div class="controls">
								<input type="file" name="userfile[3]" required><br>
							</div>
						</div>
						<div class="control-group">
							<label class="control-label">Bab III</label>
							<div class="controls">
								<input type="file" name="userfile[4]" required><br>
							</div>
						</div>
						<div class="control-group">
							<label class="control-label">Bab IV</label>
							<div class="controls">
								<input type="file" name="userfile[5]" required><br>
							</div>
						</div>
						<div class="control-group">
							<label class="control-label">Bab V</label>
							<div class="controls">
								<input type="file" name="userfile[6]" required><br>
							</div>
						</div>

						
						<div class="form-actions">
							<input class="btn btn-large btn-primary" type="submit" value="Submit">
						</div>
					</fieldset>
				</form>
			</div>
		</div>
	</div>
</div>
