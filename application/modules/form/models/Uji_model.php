<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Uji_model extends CI_Model {

	function get_detail_jadwal($kode_jadwal)
	{
		$data = $this->db->query("SELECT * FROM tbl_jadwal_matkul jdl
								JOIN tbl_matakuliah mk ON jdl.`id_matakuliah`= mk.`id_matakuliah`
								JOIN tbl_karyawan ky ON ky.`nid` = jdl.`kd_dosen`
								WHERE jdl.`kd_jadwal`= '$kode_jadwal' ")->row();
		return $data;
	}

	function get_prodi_fakultas($prodi)
	{
		$data = $this->db->query("SELECT * FROM tbl_jurusan_prodi c
								JOIN tbl_fakultas d ON d.`kd_fakultas` = c.`kd_fakultas`
								WHERE c.`kd_prodi` = '$prodi' ")->row();
		return $data;
	}

	function last_meeting($kode_jadwal)
	{
		$data = $this->db->query("SELECT MAX(pertemuan) AS satu FROM tbl_absensi_mhs_new_20171 
								WHERE kd_jadwal = '$kode_jadwal'")->row();
		return $data;
	}

}

/* End of file Ujian_model.php */
/* Location: ./application/modules/form/models/Ujian_model.php */