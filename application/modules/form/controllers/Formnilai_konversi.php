<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Formnilai_konversi extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		error_reporting(0);
		/**
		* redirect to auth when session is false		
		*/
		!$this->session->userdata('sess_login') ? redirect('auth/logout','refresh') : ''; 
		date_default_timezone_set('Asia/Jakarta');
	}

	public function index()
	{
		$user = $this->session->userdata('sess_login');
		$data['angkatan'] = $this->app_model->angkatan_mhs($user['userid']);

		$data['page'] = 'v_form_konversi_select';
		$this->load->view('template/template', $data);
	}

	function save_session(){
		$angkatan = $this->input->post('angkatan');

		$this->session->set_userdata('angkatan',$angkatan);
		redirect(base_url('form/Formnilai_konversi/list_mhs'),'refresh');
	}

	function list_mhs()
	{
		$user = $this->session->userdata('sess_login');

		$this->db->select('mhs.*,nl.id');
		$this->db->from('tbl_mahasiswa mhs');
		$this->db->join('tbl_transaksi_nilai_konversi nl','nl.NIMHSTRLNM = mhs.NIMHSMSMHS','left');
		$this->db->where('mhs.TAHUNMSMHS',$this->session->userdata('angkatan'));
		$this->db->where('mhs.KDPSTMSMHS',$user['userid']);

		//pasca beda digit pengkodean
		if ($user['userid'] == 74101 || $user['userid'] == 61101) {
			$this->db->where('SUBSTRING(mhs.NIMHSMSMHS,8,1)',7);
		}else{
			$this->db->where('SUBSTRING(mhs.NIMHSMSMHS,9,1)',7);
		}

		$this->db->group_by('mhs.NIMHSMSMHS');
		$this->db->order_by('nl.id','desc');
		$data['mhs'] = $this->db->get()->result();

		$data['page'] = 'v_form_konversi_list';
		$this->load->view('template/template', $data);
	}

	function save_session_mhs($nim){
		$this->session->set_userdata('mhs',get_nm_mhs($nim));
		$this->session->set_userdata('nim',$nim);

		redirect(base_url('form/Formnilai_konversi/input_nilai'),'refresh');
	}

	function input_nilai(){
		$user = $this->session->userdata('sess_login');
		$nim = $this->session->userdata('nim');
		$mhs = $this->app_model->getdetail('tbl_mahasiswa','NIMHSMSMHS',$nim,'NIMHSMSMHS','asc')->row();
		$data['nilai'] = $this->db->where('deleted_at')->get('tbl_index_nilai')->result();

		if ($mhs->SMAWLMSMHS < '20151') {
			$data['slct_mk'] = $this->db->where('kd_prodi',$user['userid'])
			                        ->where('tahunakademik',$mhs->SMAWLMSMHS)
									->order_by('kd_matakuliah','asc')
									->order_by('nama_matakuliah','asc')
									->get('tbl_matakuliah_copy')
									->result();


			$data['matkul'] =  $this->db->select('*')
										->from('tbl_transaksi_nilai_konversi kv')
										->join('tbl_matakuliah_copy mk','mk.kd_matakuliah = kv.KDKMKTRLNM','left')
										->where('kv.NIMHSTRLNM',$nim)
										->where('mk.kd_prodi',$user['userid'])
										->where('tahunakademik',$mhs->SMAWLMSMHS)
										->order_by('kv.id','desc')
										->get()->result();

		} else {
			$data['slct_mk'] = $this->db->where('kd_prodi',$user['userid'])
									->order_by('kd_matakuliah','asc')
									->order_by('nama_matakuliah','asc')
									->get('tbl_matakuliah')
									->result();


			$data['matkul'] =  $this->db->select('*')
										->from('tbl_transaksi_nilai_konversi kv')
										->join('tbl_matakuliah mk','mk.kd_matakuliah = kv.KDKMKTRLNM','left')
										->where('kv.NIMHSTRLNM',$nim)
										->where('mk.kd_prodi',$user['userid'])
										->order_by('kv.id','desc')
										->get()->result();
		}


		$data['page'] = 'v_form_konversi_input';
		$this->load->view('template/template', $data);	
	}

	function add_matkul()
	{
		$user = $this->session->userdata('sess_login');

		$matkul = $this->input->post('matkul');
		$nilai2 = $this->input->post('nilai');
		$nim = $this->session->userdata('nim');

		$mhs_masuk = $this->db->select('SMAWLMSMHS')->where('NIMHSMSMHS',$nim)->get('tbl_mahasiswa',1)->row()->SMAWLMSMHS;

		$nilai = explode('/', $nilai2);

		$object = array('THSMSTRLNM' => $mhs_masuk, 
						'KDPSTTRLNM' => $user['userid'],
						'NIMHSTRLNM' => $nim,
						'KDKMKTRLNM' => $matkul,
						'kd_mk_asal' => $this->input->post('kd_mk_asal'),
						'nm_mk_asal' => $this->input->post('nm_mk_asal'),
						'sks_mk_asal' => $this->input->post('sks_mk_asal'),
						'nilai_mk_asal' => $this->input->post('nilai_mk_asal'),
						'NLAKHTRLNM' => $nilai[0],
						'BOBOTTRLNM' => $nilai[1]
						);

		$q = $this->db->insert('tbl_transaksi_nilai_konversi', $object);

		if ($q) {
			echo "<script>alert('Berhasil Tambah Matakuliah');window.location = '".base_url('form/formnilai_konversi/input_nilai')."';</script>";
		}else{
			echo "<script>alert('Gagal Tambah Matakuliah!');history.go(-1);</script>";
		}
	}

	function delete_mk($id){
		$q = $this->db->where('id',$id)->delete('tbl_transaksi_nilai_konversi');

		if ($q) {
			echo "<script>alert('Berhasil Hapus Matakuliah');window.location = '".base_url('form/formnilai_konversi/input_nilai')."';</script>";
		}else{
			echo "<script>alert('Gagal Hapus Matakuliah!');history.go(-1);</script>";
		}
	}

	function edit_mk_mhs($id){
		$user = $this->session->userdata('sess_login');
		$data['nilai'] = $this->db->get('tbl_index_nilai')->result();

		$data['slct_mk'] = $this->db->where('kd_prodi',$user['userid'])
									->order_by('nama_matakuliah','asc')
									->order_by('kd_matakuliah','asc')
									->get('tbl_matakuliah')
									->result();

		$data['mk'] = $this->db->where('id',$id)->get('tbl_transaksi_nilai_konversi',1)->row();
		$this->load->view('v_form_konversi_edit', $data);
	}

	function exe_edit_mk_mhs(){
		$id = $this->input->post('id_nilai');
		$matkul = $this->input->post('matkul_edit');
		$nilai = $this->input->post('nilai_edit');

		$nilai = explode('/', $nilai);

		$object = array(
						'KDKMKTRLNM' => $matkul,
						'NLAKHTRLNM' => $nilai[0],
						'BOBOTTRLNM' => $nilai[1]
						);

		$query = $this->db->where('id',$id)->update('tbl_transaksi_nilai_konversi', $object);

		if ($query) {
			echo "<script>alert('Berhasil Update Matakuliah');window.location = '".base_url('form/formnilai_konversi/input_nilai')."';</script>";
		}else{
			echo "<script>alert('Gagal Update Matakuliah!');history.go(-1);</script>";
		}
	}

}

/* End of file Formnilai_konversi.php */
/* Location: ./application/controllers/Formnilai_konversi.php */