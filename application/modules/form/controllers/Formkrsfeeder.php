<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Formkrsfeeder extends MY_Controller {

  function __construct()
  {
    parent::__construct();
    $this->load->model('setting_model');
    error_reporting(0);
	if ($this->session->userdata('sess_login') == TRUE) {
		$cekakses = $this->role_model->cekakses(131)->result();
		if ($cekakses != TRUE) {
			echo "<script>alert('Anda Tidak Berhak Mengakses !!');history.go(-1);</script>";
		}
	} else {
		redirect('auth','refresh');
	}
	date_default_timezone_set('Asia/Jakarta');
	
	//$this->load->library('Cfpdf');
  }

  function index()
  {
    $data['tahunajaran'] = $this->app_model->getdata('tbl_tahunajaran', 'id_tahunajaran', 'ASC')->result();
    $data['page'] = 'form/form_krs';
    $this->load->view('template/template', $data);    
  }

  function viewform()
  {
    if ($this->session->userdata('sess_mhs') == TRUE) {
      $loggedmhs = $this->session->userdata('sess_mhs');
      if ($loggedmhs['strata'] == 1) {
      	$aktif = $this->setting_model->getaktivasi('krsfeeder')->result();
      	if ($aktif == TRUE) {
      		$tahunakademik = $this->app_model->getdetail('tbl_tahunakademik', 'status', 1, 'kode', 'ASC')->row();
      		//$krsan = $this->app_model->getkrsanmhs($loggedmhs['nimmhs'],$tahunakademik->kode)->row();
      		$krsan = $this->app_model->getkrsanmhs($loggedmhs['nimmhs'],'20161')->row();
      		//var_dump($krsan);exit();
      		if (($krsan == FALSE) or ($krsan == TRUE)) {
      			  $mhs = $this->app_model->getdetail('tbl_mahasiswa', 'NIMHSMSMHS', $loggedmhs['nimmhs'], 'KDPSTMSMHS', 'ASC')->row_array();
			      $data['npm'] = $loggedmhs['nimmhs'];
				  $data['prodi'] = $mhs['KDPSTMSMHS'];
				  $data['nama_mahasiswa'] = $mhs['NMMHSMSMHS'];
				  $data['semester'] = $this->app_model->get_semester($mhs['SMAWLMSMHS']);
				  	  
				  $data['data_krs'] = $this->app_model->get_all_khs_mahasiswa($loggedmhs['nimmhs'])->result();
					//echo $data['semester'];
				  $data_krs = $this->app_model->get_krs_mahasiswa($data['npm'], $data['semester']);
				  if($data_krs->num_rows() > 0){
					$data['data_matakuliah'] = $data_krs->result();
					$data['kode_krs'] = $data_krs->row()->kd_krs;
					$data['catatan_pembimbing'] = $this->app_model->get_pembimbing_krs($data['kode_krs'])->row()->keterangan_krs;
					
					$data['status_krs'] = 1;
					
					}else{
						$data['data_matakuliah'] = $this->app_model->get_matkul_krs($data['semester'], $data['prodi'], $data['npm'])->result();
						$data['status_krs'] = 0;
					  }
			      $data['page'] = 'form/krs_view2';
			      $this->load->view('template/template', $data); 
      		} else {
      			echo "<script>alert('Mohon Penuhi Biaya Administrasi');document.location.href='".base_url()."form/formkrs';</script>";
      		} 
	    } else {
	        echo "<script>alert('Akses Tidak Diizinkan');document.location.href='".base_url()."form/formkrs';</script>";
	        //echo "gabisa";
	    }
      } else {
      	$aktif = $this->setting_model->getaktivasi('krsfeeder')->result();
      	if ($aktif == TRUE) {
      		$tahunakademik = $this->app_model->getdetail('tbl_tahunakademik', 'status', 1, 'kode', 'ASC')->row();
      		$krsan = $this->app_model->getkrsanmhs($loggedmhs['nimmhs'],'20161')->row();
      		if (($krsan == FALSE) or ($krsan == TRUE)) {
	    	  $mhs = $this->app_model->getdetail('tbl_mahasiswa', 'NIMHSMSMHS', $loggedmhs['nimmhs'], 'KDPSTMSMHS', 'ASC')->row_array();
		      $data['npm'] = $loggedmhs['nimmhs'];
			  $data['prodi'] = $mhs['KDPSTMSMHS'];
			  $data['nama_mahasiswa'] = $mhs['NMMHSMSMHS'];
			  $data['semester'] = $this->app_model->get_semester($mhs['SMAWLMSMHS']);
			  	  
			  $data['data_krs'] = $this->app_model->get_all_khs_mahasiswa($loggedmhs['nimmhs'])->result();
				//echo $data['semester'];
			  $data_krs = $this->app_model->get_krs_mahasiswa($data['npm'], $data['semester']);
			  if($data_krs->num_rows() > 0){
				$data['data_matakuliah'] = $data_krs->result();
				$data['kode_krs'] = $data_krs->row()->kd_krs;
				$data['catatan_pembimbing'] = $this->app_model->get_pembimbing_krs($data['kode_krs'])->row()->keterangan_krs;
				
				$data['status_krs'] = 1;
				
				}else{
				$data['data_matakuliah'] = $this->app_model->get_matkul_krs($data['semester'], $data['prodi'], $data['npm'])->result();
				$data['status_krs'] = 0;
			  }
		      $data['page'] = 'form/krs_view2';
		      $this->load->view('template/template', $data);
		    } else {
      			echo "<script>alert('Mohon Penuhi Biaya Administrasi');document.location.href='".base_url()."form/formkrs';</script>";
      		} 
	    } else {
	        echo "<script>alert('Akses Tidak Diizinkan');document.location.href='".base_url()."form/formkrs';</script>";
	        //echo "gabisa";
	    }

      }
	 
    } else {
      echo "<script>alert('Anda Tidak Berhak Mengakses !!');history.go(-1);</script>";
    }
  }

  function reviewform()
  {
  	if ($this->session->userdata('sess_mhs') == TRUE) {
      $loggedmhs = $this->session->userdata('sess_mhs');
      if ($loggedmhs['strata'] == 1) {
      	$aktif = $this->setting_model->getaktivasi('krsfeeder')->result();
      	if (count($aktif) != 0) {
      		$tahunakademik = $this->app_model->getdetail('tbl_tahunakademik', 'status', 1, 'kode', 'ASC')->row();
      		//$krsan = $this->app_model->getkrsanmhs($loggedmhs['nimmhs'],$tahunakademik->kode)->row();
      		$krsan = $this->app_model->getkrsanmhs($loggedmhs['nimmhs'],'20161')->row();
      		if (($krsan == FALSE) or ($krsan == TRUE)) {
	    	  $mhs = $this->app_model->getdetail('tbl_mahasiswa', 'NIMHSMSMHS', $loggedmhs['nimmhs'], 'KDPSTMSMHS', 'ASC')->row_array();
		      $data['npm'] = $loggedmhs['nimmhs'];
			  $data['prodi'] = $mhs['KDPSTMSMHS'];
			  $data['nama_mahasiswa'] = $mhs['NMMHSMSMHS'];
			  $data['semester'] = $this->app_model->get_semester($mhs['SMAWLMSMHS']);
			  	  
			  $data['data_krs'] = $this->app_model->get_all_khs_mahasiswa($loggedmhs['nimmhs'])->result();
				//echo $data['semester'];
			  $data_krs = $this->app_model->get_krs_mahasiswa($data['npm'], $data['semester']);
			  if($data_krs->num_rows() > 0){
				$data['data_matakuliah'] = $data_krs->result();
				$data['kode_krs'] = $data_krs->row()->kd_krs;
				$data['catatan_pembimbing'] = $this->app_model->get_pembimbing_krs($data['kode_krs'])->row()->keterangan_krs;
				
				$data['status_krs'] = 1;
				
				}else{
					echo "<script>alert('Akses Tidak Diizinkan');document.location.href='".base_url()."home';</script>";
			  }
		      $data['page'] = 'form/krs_view2';
		      $this->load->view('template/template', $data); 
		    } else {
      			echo "<script>alert('Mohon Penuhi Biaya Administrasi');document.location.href='".base_url()."form/formkrs';</script>";
      		} 
	    } else {
	        echo "<script>alert('Akses Tidak Diizinkan');document.location.href='".base_url()."home';</script>";
	        //echo "gabisa";
	    }
      } else {
      	$aktif = $this->setting_model->getaktivasi('krsfeeder')->result();
      	if (count($aktif) != 0) {
      		$tahunakademik = $this->app_model->getdetail('tbl_tahunakademik', 'status', 1, 'kode', 'ASC')->row();
      		$krsan = $this->app_model->getkrsanmhs($loggedmhs['nimmhs'],'20161')->row();
      		if (($krsan == FALSE) or ($krsan == TRUE)) {
	    	  $mhs = $this->app_model->getdetail('tbl_mahasiswa', 'NIMHSMSMHS', $loggedmhs['nimmhs'], 'KDPSTMSMHS', 'ASC')->row_array();
		      $data['npm'] = $loggedmhs['nimmhs'];
			  $data['prodi'] = $mhs['KDPSTMSMHS'];
			  $data['nama_mahasiswa'] = $mhs['NMMHSMSMHS'];
			  $data['semester'] = $this->app_model->get_semester($mhs['SMAWLMSMHS']);
			  	  
			  $data['data_krs'] = $this->app_model->get_all_khs_mahasiswa($loggedmhs['nimmhs'])->result();
				//echo $data['semester'];
			  $data_krs = $this->app_model->get_krs_mahasiswa($data['npm'], $data['semester']);
			  if($data_krs->num_rows() > 0){
				$data['data_matakuliah'] = $data_krs->result();
				$data['kode_krs'] = $data_krs->row()->kd_krs;
				$data['catatan_pembimbing'] = $this->app_model->get_pembimbing_krs($data['kode_krs'])->row()->keterangan_krs;
				
				$data['status_krs'] = 1;
				
				}else{
					echo "<script>alert('Akses Tidak Diizinkan');document.location.href='".base_url()."home';</script>";
			  }
		      $data['page'] = 'form/krs_view2';
		      $this->load->view('template/template', $data);
		    } else {
      			echo "<script>alert('Mohon Penuhi Biaya Administrasi');document.location.href='".base_url()."form/formkrs';</script>";
      		}
	    } else {
	        echo "<script>alert('Akses Tidak Diizinkan');document.location.href='".base_url()."home';</script>";
	        //echo "gabisa";
	    }

      }
	 
    } else {
      echo "<script>alert('Anda Tidak Berhak Mengakses !!');history.go(-1);</script>";
    }
  }
  
  function view($npm, $id)
	{
		
		$data['npm'] = $npm;

		$data['ips'] = $this->app_model->get_ips_mahasiswa($npm, $id)->row()->ips;

		$data['mhs'] = $this->app_model->getdetail('tbl_mahasiswa','NIMHSMSMHS',$npm,'NIMHSMSMHS','asc')->row();
		
		$data['semester'] = $id;
		$data['kode'] = $this->db->query("select distinct kd_krs from tbl_krs where npm_mahasiswa = '".$npm."' and semester_krs = ".$id." ")->row();
		//$data['detail_khs'] = $this->app_model->get_detail_khs_mahasiswa($npm, $id)->result();
		$data['detail_khs'] = $this->db->query("select a.*,b.nama_matakuliah,b.sks_matakuliah from tbl_krs a join tbl_matakuliah b on a.kd_matakuliah = b.kd_matakuliah 
												where a.npm_mahasiswa = '".$npm."' and semester_krs = ".$id." and b.kd_prodi = '".$data['mhs']->KDPSTMSMHS."'")->result();
		$data['page'] = 'form/khs_detail';
		$this->load->view('template/template',$data);	
	}
  
  function get_matkul(){
	$semester = $_POST['semester'];
	$npm = $_POST['npm'];
	$prodi = $_POST['prodi'];
     
    $data = $this->app_model->get_matkul_krs($semester, $prodi, $npm)->result();
                
    $js = json_encode($data);
    echo $js;
  }
  
  function get_krs(){
	$semester = $_POST['semester'];
	$npm = $_POST['npm'];
	
    $data = $this->app_model->get_matkul_krs_rekam($semester, $npm)->result();
                
    $js = json_encode($data);
    echo $js;
  }
  
  function cek_prasyarat(){
	$sym  = array('[', ']');
	$prasyarat = explode(',', str_replace($sym,'',$_POST['prasyarat']));
	  
    $data = $this->app_model->get_prasyarat($prasyarat)->num_rows();               
    echo $data;
  }
  
  function save_data(){
  	//$ta = $this->app_model->getdetail('tbl_tahunakademik','status','1','kode','asc')->row();
	$kd_matkuliah = $this->input->post('kd_matkuliah');
	$data['semester_krs'] = $this->input->post('semester');
	$data['npm_mahasiswa'] = substr($this->input->post('npm_mahasiswa'), 0,12);
	$kodebaru = $this->input->post('kode_krs');
	$this->db->order_by('kode', 'desc');
	$this->db->where('status', '1');
	//$tahunakademik = $this->app_model->getdetail('tbl_tahunakademik', 'status', 1, 'kode', 'ASC')->row();
	$ta = '20161';
	if($kodebaru != ''){
		$kodeawal = $data['npm_mahasiswa'].$ta;
		$hitung = strlen($kodeawal);
		$jumlah = 0;
		for ($i=0; $i <= $hitung; $i++) {
			$char = substr($kodeawal,$i,1);
			$jumlah = $jumlah + $char;
		}
		$mod = $jumlah%10;
		$kodebaru = $data['npm_mahasiswa'].$ta.$mod;
		//$this->app_model->deletedata('tbl_krs','kd_krs',$kodebaru);
		$mkbaru = array();
		foreach ($kd_matkuliah as $key => $n) {
			$mkbaru[] = $n;
			$getkrsbefore = $this->app_model->getkrsbefore_feeder($kodebaru,$n)->row();
			if (count($getkrsbefore) == 0) {
				$data['kd_matakuliah'] = $n;
				$data['kd_krs'] = $kodebaru;									
				$this->app_model->insertdata('tbl_krs_feeder', $data);
				//$mkbaru == $n;
				//echo $n;
			}
		}
		$getmkkrsbefore = $this->app_model->cekmkbarukrs($kodebaru,$mkbaru)->result();
		foreach ($getmkkrsbefore as $value) {
			$this->app_model->deletedata('tbl_krs','id_krs',$value->id_krs);
		}
		$logged = $this->session->userdata('sess_login');
		$datas['id_pembimbing'] = $logged['userid'];
		$datas['kd_krs'] = $kodebaru;
		$datas['keterangan_krs'] = $this->input->post('keterangan_krs');
		$datas['tgl_bimbingan'] = date('Y-m-d h:i:s');
		$datas['key'] = $this->generateRandomString();
		$datas['tahunajaran'] = $ta;
		$datas['npm_mahasiswa'] = $data['npm_mahasiswa'];
		$jurusan = $this->app_model->getdetail('tbl_mahasiswa','NIMHSMSMHS',$datas['npm_mahasiswa'],'NIMHSMSMHS','asc')->row();
		$datas['kd_jurusan'] = $jurusan->KDPSTMSMHS;
		$datas['jumlah_sks'] = $this->input->post('jumlah_sks', TRUE);
		$kodver = $kodebaru.$datas['jumlah_sks'].$datas['key'];
		$datas['kd_krs_verifikasi'] = md5(md5($kodver).key_verifikasi);
		//var_dump($datas);exit();
		//qr code
		$this->load->library('ciqrcode');
		$params['data'] = base_url().'welcome/welcome/cekkodeverifikasi/'.$datas['kd_krs_verifikasi'].'';
		$params['level'] = 'H';
		$params['size'] = 10;
		$params['savename'] = FCPATH.'QRImage/'.$datas['kd_krs_verifikasi'].'.png';
		$this->ciqrcode->generate($params);
		//end qr code

		$this->app_model->deletedata('tbl_verifikasi_krs','kd_krs',$kodebaru);
		$nama = $this->app_model->getdetail('tbl_mahasiswa','NIMHSMSMHS',$datas['npm_mahasiswa'],'NIMHSMSMHS','asc')->row();
		$slug = url_title($nama->NMMHSMSMHS, '_', TRUE);
		$datas['slug_url'] = $slug;
		$this->app_model->insertdata('tbl_verifikasi_krs ', $datas);
	}else{
		$kodeawal = $data['npm_mahasiswa'].$ta;
		$hitung = strlen($kodeawal);
		$jumlah = 0;
		for ($i=0; $i <= $hitung; $i++) {
			$char = substr($kodeawal,$i,1);
			$jumlah = $jumlah + $char;
		}
		$mod = $jumlah%10;
		$kodebaru = $data['npm_mahasiswa'].$ta.$mod;
		
		foreach ($kd_matkuliah as $key => $n) {
			$data['kd_matakuliah'] = $n;
			$data['kd_krs'] = $kodebaru;		

			$this->app_model->insertdata('tbl_krs_feeder', $data);
		}
		//var_dump($datas);exit();
		$logged = $this->session->userdata('sess_login');
		$datas['id_pembimbing'] = $logged['userid'];
		$datas['kd_krs'] = $kodebaru;
		$datas['npm_mahasiswa'] = $data['npm_mahasiswa'];
		$jurusan = $this->app_model->getdetail('tbl_mahasiswa','NIMHSMSMHS',$datas['npm_mahasiswa'],'NIMHSMSMHS','asc')->row();
		$datas['kd_jurusan'] = $jurusan->KDPSTMSMHS;
		$datas['keterangan_krs'] = $this->input->post('keterangan_krs');
		$datas['tgl_bimbingan'] = date('Y-m-d h:i:s');
		$datas['key'] = $this->generateRandomString();
		$datas['tahunajaran'] = $ta;
		$datas['jumlah_sks'] = $this->input->post('jumlah_sks', TRUE);
		$kodver = $kodebaru.$datas['jumlah_sks'].$datas['key'];
		$datas['kd_krs_verifikasi'] = md5(md5($kodver).key_verifikasi);

		$this->load->library('ciqrcode');
		$params['data'] = base_url().'welcome/welcome/cekkodeverifikasi/'.$datas['kd_krs_verifikasi'].'';
		$params['level'] = 'H';
		$params['size'] = 10;
		$params['savename'] = FCPATH.'QRImage/'.$datas['kd_krs_verifikasi'].'.png';
		$this->ciqrcode->generate($params);
		$nama = $this->app_model->getdetail('tbl_mahasiswa','NIMHSMSMHS',$datas['npm_mahasiswa'],'NIMHSMSMHS','asc')->row();
		$slug = url_title($nama->NMMHSMSMHS, '_', TRUE);
		$datas['slug_url'] = $slug;
		$this->app_model->insertdata('tbl_verifikasi_krs ', $datas);
	}
	
	$data['pembimbing'] = $this->app_model->get_pembimbing_krs($kodebaru)->row_array();
	$data['detail_krs'] = $this->app_model->get_detail_print_krs_mahasiswa($kodebaru)->result();
	$data['kd_krs'] = $kodebaru;

	$this->jadwal_kuliah($data['npm_mahasiswa'],$data['kd_krs']);
	// $data['page'] = 'form/viewkrs_detail';
	// $this->load->view('template/template',$data);    
    	
	//echo '<script>alert("Tersimpan"); window.location.href = "'.base_url().'/form/formkrs/jadwal_kuliah/"'.$data['npm_mahasiswa'].'/'.$data['kd_krs'].';</script>';
  }



  	function generateRandomString() {
	    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
	    $charactersLength = strlen($characters);
	    $randomString = '';
	    for ($i = 0; $i < 5; $i++) {
	        $randomString .= $characters[rand(0, $charactersLength - 1)];
	    }
	    return $randomString;
	}
  
  function printkrs($kodebaru){
	$a=substr($kodebaru, 16,1);
	$npm=substr($kodebaru, 0,12);
	$ta=substr($kodebaru, 12,5);

    if ($a == 1) {
      $b = 'Ganjil';
    } else {
      $b = 'Genap';
    }

    $cetak['kdver'] = $this->db->query('SELECT kd_krs_verifikasi from tbl_verifikasi_krs WHERE npm_mahasiswa = "'.$npm.'" and tahunajaran = "'.$ta.'" order by kd_krs_verifikasi desc limit 1')->row();

	$cetak['footer'] = $this->db->select('npm_mahasiswa,id_pembimbing')
								->from('tbl_verifikasi_krs')
								->like('npm_mahasiswa', $npm,'both')
								->get()->row();

	$cetak['kd_krs'] = $kodebaru;
	
    $cetak['gg']  = $b;
    $cetak['npm'] = $npm;
    $cetak['ta'] = $ta;

	$this->load->view('welcome/print/krs_pdf',$cetak);
  }

	function get_jadwal(){
	  	$tahunakademik = $this->app_model->getdetail('tbl_tahunakademik', 'status', 1, 'kode', 'ASC')->row();
	  	$loggedmhs = $this->session->userdata('sess_mhs');
		$kd_matakuliah = $_POST['kd_matakuliah'];
		$kelas = $loggedmhs['kelas'];
		$user = $this->session->userdata('sess_login');

		$npm  = $user['username'];

		$mhs  = $this->app_model->get_jurusan_mhs($loggedmhs['nimmhs'])->row();
		
		// $data = $this->db->query('SELECT * 
		// 					FROM tbl_jadwal_matkul a
		// 					LEFT JOIN tbl_ruangan b ON a.`kelas` = b.`id_ruangan`
		// 					LEFT JOIN tbl_karyawan c ON a.`kd_dosen` = c.`nid`
		// 					LEFT JOIN tbl_matakuliah d ON a.`kd_matakuliah` = d.`kd_matakuliah`
		// 					WHERE  a.`kd_jadwal` LIKE "'.$mhs->KDPSTMSMHS.'%" AND a.`kd_matakuliah` = "'.$kd_matakuliah.'" AND d.`kd_prodi` LIKE "'.$mhs->KDPSTMSMHS.'"');

		$data = $this->app_model->get_pilih_jadwal_krs_feeder($kd_matakuliah,$mhs->KDPSTMSMHS,'20161',$kelas)->result();
                
		$js = json_encode($data);
		echo $js;
	}

	function save_jadwal(){
		$data['kd_jadwal'] = $this->input->post('kd_jadwal');
		$kd_matakuliah = $this->input->post('kd_matakuliah');
		$kd_krs= $this->input->post('kd_krs');

		$npm_mahasiswa = substr($kd_krs, 0,12);
		//die($npm_mahasiswa);
		
		$this->app_model->updatedata_krs_feeder($kd_matakuliah, $kd_krs, $data);
		//redirect('form/formkrs/jadwal_kuliah','refresh');
		$this->jadwal_kuliah($npm_mahasiswa,$kd_krs);

	}

	function jadwal_kuliah($npm_mahasiswa,$kd_krs){
		//die($semester_krs.'-'.$npm_mahasiswa.'-'.$kd_krs);


		//$data['semester_krs']  = $semester_krs;
		$data['npm_mahasiswa'] = $npm_mahasiswa;
		$data['kd_krs']		= $kd_krs;

		$data['pembimbing'] = $this->app_model->get_pembimbing_krs($kd_krs)->row_array();
		$data['detail_krs'] = $this->app_model->get_detail_print_krs_mahasiswa_feeder($kd_krs)->result();

		//var_dump($data['detail_krs']);die();

		$data['page'] = 'form/viewkrs_detail';
		$this->load->view('template/template',$data);    


	}

	function kosongkan_jadwal($id,$id2){
		//die('test'.$id.'');
		//$logged = $this->session->userdata('sess_login');
		//$idkd = $this->app_model->getdetailslug(substr($logged['userid'], 0,12),$id)->row();
		$data = array('kd_jadwal' => null );
		$npm_mahasiswa = substr($id, 0,12);


		$this->db->where('kd_krs', $id);
		$this->db->where('kd_matakuliah', $id2);
		$this->db->update('tbl_krs', $data);

		$q = $this->db->select('kd_krs')
						->from('tbl_krs')
						->where('id_krs',$id)
						->get()->row();

		$this->jadwal_kuliah($npm_mahasiswa,$id);

	}

	function cekjumlahsks()
	{
		$loggedmhs = $this->session->userdata('sess_mhs');
		$npm = $loggedmhs['nimmhs'];
		$mhs = $this->app_model->getdetail('tbl_mahasiswa', 'NIMHSMSMHS', $loggedmhs['nimmhs'], 'KDPSTMSMHS', 'ASC')->row_array();
	    $data['prodi'] = $mhs['KDPSTMSMHS'];
	    //$data['nama_mahasiswa'] = $mhs['NMMHSMSMHS'];
	    $hitung_ips = $this->db->query('SELECT distinct a.`NIMHSTRLNM`,a.`KDKMKTRLNM`,a.`NLAKHTRLNM`,a.`BOBOTTRLNM`,b.`sks_matakuliah` FROM tbl_transaksi_nilai a
        JOIN tbl_matakuliah b ON a.`KDKMKTRLNM` = b.`kd_matakuliah`
        WHERE a.`kd_transaksi_nilai` IS NOT NULL AND kd_prodi = "'.$data['prodi'].'" AND NIMHSTRLNM = "'.$npm.'" and THSMSTRLNM = "20152" ')->result();

        $st=0;
        $ht=0;
        foreach ($hitung_ips as $iso) {
            $h = 0;

            $h = $iso->BOBOTTRLNM * $iso->sks_matakuliah;
            $ht=  $ht + $h;

            $st=$st+$iso->sks_matakuliah;
        }

        $ips_nr = $ht/$st;
	    $semester = $this->app_model->get_semester($mhs['SMAWLMSMHS']);
		//$sms = $semester - 1;
		$sks = $_POST['sks'];
		if ($sks > 24) {
			echo 0;
		} else {
			$b = $this->app_model->ketentuan_sks(number_format($ips_nr,2),$sks);
			if (($semester == 1) or ($semester == 2) or ($semester > 8)) {
				echo 1;
			} elseif(($mhs['KDPSTMSMHS'] == '25201' or $mhs['KDPSTMSMHS'] == '32201') and (substr($npm, 0,4) < 2015)){
				echo 1;
			} elseif (($mhs['STMHSMSMHS'] == 'CA') and (number_format($ips_nr,2) == 0.00)) {
				echo 1;
			} else {
				echo $b;
			}
		}
		
		//$a = $this->app_model->get_ips_mahasiswa($npm, $sms)->row();
		 
		
		//var_dump($a);exit();
	}

}

/* End of file Formkrs.php */
/* Location: .//C/Users/danum246/AppData/Local/Temp/fz3temp-1/Formkrs.php */