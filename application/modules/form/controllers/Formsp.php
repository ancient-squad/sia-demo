<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Formsp extends MY_Controller {

	function __construct()
	{
		parent::__construct();
		// $this->load->model('setting_model');
		// if ($this->session->userdata('sess_login') == TRUE) {
		//   $akses = $this->role_model->cekakses(79)->result();
		//   $aktif = $this->setting_model->getaktivasi('sp')->result();
		//   if (count($aktif) != 0) {
		// 		if ($akses != TRUE) {
		// 			echo "<script>alert('Akses Tidak Diizinkan');document.location.href='".base_url()."home';</script>";
		// 			//echo "gabisa";
		// 		}
		// 	} else {
		// 		echo "<script>alert('Akses Tidak Diizinkan');document.location.href='".base_url()."home';</script>";
		// 		//echo "gabisa";
		// 	}
		// } else {
		//   redirect('auth','refresh');
		// }
	}

	function index()
	{
		$user = $this->session->userdata('sess_login');
		$prodi = $user['userid'];

		$a = $this->db->where('status',1)->get('tbl_tahunakademik', 1)->row();
		$ta = substr($a->kode, 0,-1);
		$b = 3;
		$ta_sp = "{$ta}.{$b}";

		$data['mhs'] = $this->db->query("SELECT vkrs.`npm_mahasiswa`,mhs.`NMMHSMSMHS`,vkrs.`jumlah_sks`,krs.`kd_krs`,
								IF(krs.`kd_jadwal` != '',mk.`sks_matakuliah`,0) AS jml_open
								FROM tbl_verifikasi_krs_sp vkrs 
								JOIN tbl_krs_sp	krs ON vkrs.`kd_krs` = krs.`kd_krs`
								JOIN tbl_mahasiswa mhs ON mhs.`NIMHSMSMHS` = vkrs.`npm_mahasiswa`
								JOIN tbl_matakuliah mk ON mk.`kd_matakuliah` = krs.`kd_matakuliah`
								WHERE vkrs.`tahunajaran` = '20163' AND kd_jurusan = '".$prodi."' 
								AND mk.`kd_prodi` = '".$prodi."'
							    GROUP BY vkrs.`kd_krs`")->result();

		$data['page'] = 'form/form_sp';
    	$this->load->view('template/template', $data);    
	}

	function viewform()
	{
		if ($this->session->userdata('sess_mhs') == TRUE) {
			$loggedmhs = $this->session->userdata('sess_mhs');
			//$aktif = $this->setting_model->getaktivasi('sp')->result();
			$user = $this->session->userdata('sess_login');
			$aktif = 1;

			//die($loggedmhs['nimmhs']); 

			if (count($aktif) != 0) {
				$mhs = $this->app_model->getdetail('tbl_mahasiswa', 'NIMHSMSMHS', $loggedmhs['nimmhs'], 'KDPSTMSMHS', 'ASC')->row_array();
				$data['npm'] = $loggedmhs['nimmhs'];
				$data['prodi'] = $mhs['KDPSTMSMHS'];
				$data['nama_mahasiswa'] = $mhs['NMMHSMSMHS'];
				$data['semester'] = $this->app_model->get_semester($mhs['SMAWLMSMHS']);
					  
				$data['data_krs'] = $this->app_model->get_all_khs_mahasiswa($loggedmhs['nimmhs'])->result();
				$data_krs = $this->db->query("SELECT * FROM tbl_verifikasi_krs_sp vsp
														JOIN tbl_krs_sp ksp ON vsp.kd_krs = ksp.kd_krs
														JOIN tbl_matakuliah mk ON mk.kd_matakuliah = ksp.kd_matakuliah
														WHERE vsp.npm_mahasiswa = '".$loggedmhs['nimmhs']."' AND vsp.tahunajaran = '20164' AND mk.kd_prodi = ".get_mhs_jur($loggedmhs['nimmhs'])."");
				//echo $data['semester'];
				//$data_krs = $this->app_model->get_krs_mahasiswa_sp($data['npm'], $data['semester']);
				
				
				if($data_krs->num_rows() > 0){
					$data['data_matakuliah'] = $data_krs->result();
					$data['kode_krs'] = $data_krs->row()->kd_krs;
					//$data['catatan_pembimbing'] = $this->app_model->get_pembimbing_krs($data['kode_krs'])->row()->keterangan_krs;
					$data['status_krs'] = 1;
				}else{
					//$data['data_matakuliah'] = $this->app_model->get_matkul_krs($data['semester'], $data['prodi'], $data['npm'])->result();
					$data['data_matakuliah'] = $data_krs->result();
					$data['status_krs'] = 0;
				}
				$data['page'] = 'form/sp_view'; 
				$this->load->view('template/template', $data);  
			} else { 
				echo "<script>alert('Akses Tidak Diizinkan');document.location.href='".base_url()."home';</script>"; 
				//echo "gabisa";
			}

		} else {
			echo "<script>alert('Anda Tidak Berhak Mengakses !!');history.go(-1);</script>";
		}
	}

	function get_matkul(){
		$ta = '20161';
		$npm = $_POST['npm'];
		$prodi = $_POST['prodi'];
	     
	    $data = $this->app_model->get_matkul_perbaikan($ta, $prodi, $npm)->result();
	                
	    $js = json_encode($data);
	    echo $js;
  	}
  
  	function cek_prasyarat(){
		$sym  = array('[', ']');
		$prasyarat = explode(',', str_replace($sym,'',$_POST['prasyarat']));
		  
	    $data = $this->app_model->get_prasyarat($prasyarat)->num_rows();               
	    echo $data;
  	}

  	function generateRandomString() {
	    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
	    $charactersLength = strlen($characters);
	    $randomString = '';
	    for ($i = 0; $i < 5; $i++) {
	        $randomString .= $characters[rand(0, $charactersLength - 1)];
	    }
	    return $randomString;
	}

	function save_data(){
	
	$kd_matkuliah = $this->input->post('kd_matkuliah');
	$data['semester_krs'] = $this->input->post('semester');
	$data['npm_mahasiswa'] = substr($this->input->post('npm_mahasiswa'), 0,12);
	$data['flag_open'] = 0;
	$kodebaru = $this->input->post('kode_krs');
	$this->db->order_by('kode', 'desc');
	$this->db->where('status', '1');
	$tahunakademik = $this->db->get('tbl_tahunakademik', 1)->row();
	$ta_tmp = substr($tahunakademik->kode, 0,-1);
	$a = substr($tahunakademik->kode,-1);

	if ($a == '1') {
	    $ta = $ta_tmp.'3';
	}elseif($a == '2'){	    
	    $ta = $ta_tmp.'4';
	}

	if($kodebaru != ''){
		//$this->app_model->deletedata('tbl_krs_sp','kd_krs',$kodebaru);
		$this->db->query('DELETE FROM `tbl_krs_sp` WHERE `kd_krs` = '.$kodebaru.'');

		foreach ($kd_matkuliah as $key => $n) {
			$data['kd_matakuliah'] = $n;
			$data['kd_krs'] = $kodebaru;		
			//var_dump($data);exit();
			$this->app_model->insertdata('tbl_krs_sp', $data);
		}
		
		$logged = $this->session->userdata('sess_login');
		$datas['id_pembimbing'] = $logged['userid'];
		$datas['kd_krs'] = $kodebaru;
		$datas['keterangan_krs'] = $this->input->post('keterangan_krs');
		$datas['tgl_bimbingan'] = date('Y-m-d h:i:s');
		$datas['key'] = $this->generateRandomString();
		$datas['tahunajaran'] = $ta;
		$datas['npm_mahasiswa'] = $data['npm_mahasiswa'];
		$jurusan = $this->app_model->getdetail('tbl_mahasiswa','NIMHSMSMHS',$datas['npm_mahasiswa'],'NIMHSMSMHS','asc')->row();
		$datas['kd_jurusan'] = $jurusan->KDPSTMSMHS;
		$datas['jumlah_sks'] = $this->input->post('jumlah_sks', TRUE);
		$datas['status_verifikasi'] = 2;
		$kodver = $kodebaru.$datas['jumlah_sks'].$datas['key'];
		$datas['kd_krs_verifikasi'] = md5(md5($kodver).key_verifikasi);
		//var_dump($datas);exit();
		//qr code
		$this->load->library('ciqrcode');
		$params['data'] = base_url().'welcome/welcome/cekkodeverifikasi/'.$datas['kd_krs_verifikasi'].'';
		$params['level'] = 'H';
		$params['size'] = 10;
		$params['savename'] = FCPATH.'QRImage/'.$datas['kd_krs_verifikasi'].'.png';
		$this->ciqrcode->generate($params);
		//end qr code

		$this->app_model->deletedata('tbl_verifikasi_krs_sp','kd_krs',$kodebaru);
		$nama = $this->app_model->getdetail('tbl_mahasiswa','NIMHSMSMHS',$datas['npm_mahasiswa'],'NIMHSMSMHS','asc')->row();
		$slug = url_title($nama->NMMHSMSMHS, '_', TRUE);
		$datas['slug_url'] = $slug;
		$this->app_model->insertdata('tbl_verifikasi_krs_sp', $datas);
	}else{
		$kodeawal = $data['npm_mahasiswa'].$ta;
		$hitung = strlen($kodeawal);
		$jumlah = 0;
		for ($i=0; $i <= $hitung; $i++) {
			$char = substr($kodeawal,$i,1);
			$jumlah = $jumlah + $char;
		}
		$mod = $jumlah%10;
		$kodebaru = $data['npm_mahasiswa'].$ta.$mod.'2';
		//var_dump($kd_matkuliah);exit();
		foreach ($kd_matkuliah as $key => $n) {
			$data['kd_matakuliah'] = $n;
			$data['kd_krs'] = $kodebaru;		
			$this->app_model->insertdata('tbl_krs_sp', $data);
		}
		
		$logged = $this->session->userdata('sess_login');
		$datas['id_pembimbing'] = $logged['userid'];
		$datas['kd_krs'] = $kodebaru;
		$datas['npm_mahasiswa'] = $data['npm_mahasiswa'];
		$jurusan = $this->app_model->getdetail('tbl_mahasiswa','NIMHSMSMHS',$datas['npm_mahasiswa'],'NIMHSMSMHS','asc')->row();
		$datas['kd_jurusan'] = $jurusan->KDPSTMSMHS;
		$datas['keterangan_krs'] = $this->input->post('keterangan_krs');
		$datas['tgl_bimbingan'] = date('Y-m-d h:i:s');
		$datas['key'] = $this->generateRandomString();
		$datas['tahunajaran'] = $ta;
		$datas['jumlah_sks'] = $this->input->post('jumlah_sks', TRUE);
		$datas['status_verifikasi'] = 2;
		$kodver = $kodebaru.$datas['jumlah_sks'].$datas['key'];
		$datas['kd_krs_verifikasi'] = md5(md5($kodver).key_verifikasi);

		$this->load->library('ciqrcode');
		$params['data'] = base_url().'welcome/welcome/cekkodeverifikasi/'.$datas['kd_krs_verifikasi'].'';
		$params['level'] = 'H';
		$params['size'] = 10;
		$params['savename'] = FCPATH.'QRImage/'.$datas['kd_krs_verifikasi'].'.png';
		$this->ciqrcode->generate($params);
		$nama = $this->app_model->getdetail('tbl_mahasiswa','NIMHSMSMHS',$datas['npm_mahasiswa'],'NIMHSMSMHS','asc')->row();
		$slug = url_title($nama->NMMHSMSMHS, '_', TRUE);
		$datas['slug_url'] = $slug;
		$this->app_model->insertdata('tbl_verifikasi_krs_sp', $datas);
	}
	//exit();
	//$data['pembimbing'] = $this->app_model->get_pembimbing_krs($kodebaru)->row_array();
	//$data['detail_krs'] = $this->app_model->get_detail_print_krs_mahasiswa_sp($kodebaru)->result();
	$data['kd_krs'] = $kodebaru;
	//$kd_krs = $kodebaru;
	$this->jadwal_kuliah($data['npm_mahasiswa'],$data['kd_krs']);
	//echo "sukses";
	//$this->print_sp($kd_krs);
	}

	function get_jadwal(){
	  	//$tahunakademik = $this->app_model->getdetail('tbl_tahunakademik', 'status', 1, 'kode', 'ASC')->row();
	  	$loggedmhs = $this->session->userdata('sess_mhs');
		$kd_matakuliah = $_POST['kd_matakuliah'];
		$kelas = $loggedmhs['kelas'];
		$user = $this->session->userdata('sess_login');

		$npm  = $user['username'];

		$mhs  = $this->app_model->get_jurusan_mhs($loggedmhs['nimmhs'])->row();

		$data = $this->app_model->get_pilih_jadwal_krs_sp($kd_matakuliah,$mhs->KDPSTMSMHS,'20163',$kelas)->result();
                
		$js = json_encode($data);
		echo $js;
	}

	function save_jadwal(){
		$data['kd_jadwal'] = $this->input->post('kd_jadwal');
		$kd_matakuliah = $this->input->post('kd_matakuliah');
		$kd_krs= $this->input->post('kd_krs');

		$npm_mahasiswa = substr($kd_krs, 0,12);
		//die($npm_mahasiswa);
		
		$this->app_model->updatedata_krs_sp($kd_matakuliah, $kd_krs, $data);
		//redirect('form/formkrs/jadwal_kuliah/'.$npm_mahasiswa.''.$kd_krs,'refresh');
		$this->jadwal_kuliah($npm_mahasiswa,$kd_krs);

	}

	function jadwal_kuliah($npm_mahasiswa,$kd_krs){
		//die($semester_krs.'-'.$npm_mahasiswa.'-'.$kd_krs);

		//$data['semester_krs']  = $semester_krs;
		$data['npm_mahasiswa'] = $npm_mahasiswa;
		$data['kd_krs']		= $kd_krs;

		$data['pembimbing'] = $this->app_model->get_pembimbing_krs_sp($kd_krs)->row_array();
		$data['detail_krs'] = $this->app_model->get_detail_print_krs_mahasiswa_sp($kd_krs);

		//var_dump($data['detail_krs']);die();

		$data['page'] = 'form/viewkrs_sp_detail';
		$this->load->view('template/template',$data);   
	}

	function kosongkan_jadwal($id,$id2){
		//die('test'.$id.'');
		//$logged = $this->session->userdata('sess_login');
		//$idkd = $this->app_model->getdetailslug(substr($logged['userid'], 0,12),$id)->row();
		$data = array('kd_jadwal' => null );
		$npm_mahasiswa = substr($id, 0,12);


		$this->db->where('kd_krs', $id);
		$this->db->where('kd_matakuliah', $id2);
		$this->db->update('tbl_krs', $data);

		$q = $this->db->select('kd_krs')
						->from('tbl_krs_sp')
						->where('id_krs',$id)
						->get()->row();

		$this->jadwal_kuliah($npm_mahasiswa,$id);

	}

	function print_sp($kode){
		$this->load->library('Cfpdf');

		$log = $this->session->userdata('sess_login');
        
        $data['prodi'] = $log['userid'];
		$ta = $this->db->where('status',1)->get('tbl_tahunakademik', 1)->row();

		$data['tahun'] = substr($ta->tahun_akademik, 0,9);
		$data['smtr'] = $this->app_model->getdetail('tbl_mahasiswa','NIMHSMSMHS',substr($kode, 0,12),'NIMHSMSMHS','asc')->row();
		$a = substr($kode, 16,1);

		if ($a = 3) {
			$data['ganjilgenap'] = 'Ganjil';
		}elseif ($a = 4) {
			$data['ganjilgenap'] = 'Genap';
		}


		$npm = substr($kode, 0,12);
		
		$this->db->select('a.NIMHSMSMHS,a.NMMHSMSMHS,a.TAHUNMSMHS,b.prodi');
		$this->db->from('tbl_mahasiswa a');
		$this->db->join('tbl_jurusan_prodi b', 'a.KDPSTMSMHS = b.kd_prodi');
		//$this->db->join('tbl_fakultas c', 'c.kd_fakultas = b.kd_fakultas');
		$this->db->where('NIMHSMSMHS', $npm);
		$data['info_mhs'] = $this->db->get()->row();

		$this->db->where('npm_mahasiswa', $npm);
        $this->db->order_by('id_verifikasi', 'desc');
        $dsn = $this->db->get('tbl_verifikasi_krs', 1)->row()->id_pembimbing;

        $this->db->where('nid', $dsn);
        $data['dosen'] = $this->db->get('tbl_karyawan',1)->row();

		$qq=$this->app_model->get_KDPSTMSMHS($npm)->row();

		$data['matkul'] = $this->db->query('select distinct * from tbl_krs_sp b join tbl_verifikasi_krs_sp a on a.kd_krs = b.kd_krs
		join tbl_matakuliah c on b.kd_matakuliah = c.kd_matakuliah
		join tbl_jadwal_matkul_sp d on d.kd_jadwal = b.kd_jadwal
		join tbl_karyawan e on e.nid = d.kd_dosen 
		where b.kd_krs = "'.$kode.'" AND a.status_verifikasi = 2 AND b.flag_open = 1 AND c.kd_prodi = "'.$qq->KDPSTMSMHS.'"')->result();

		//var_dump($data['matkul']);die();

		$this->load->view('akademik/espe', $data);


	}

}

/* End of file Formsp.php */
/* Location: ./application/modules/form/controllers/Formsp.php */