<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Formpmbmhs extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		//error_reporting(0);
		if ($this->session->userdata('sess_login') == TRUE) {
			$cekakses = $this->role_model->cekakses(87)->result();
			if ($cekakses != TRUE) {
				echo "<script>alert('Anda Tidak Berhak Mengakses !!');history.go(-1);</script>";
			}
		} else {
			redirect('auth','refresh');
		}
		date_default_timezone_set('Asia/Jakarta'); 
	}

	public function index()
	{
		$data['page'] = 'form_pmb_mhs';
		$this->load->view('template/template', $data);
	}

	function bio_cmb()
	{

		$this->load->helper('inflector');
		$nama = underscore($_FILES['userfile']['name']);

		$config['upload_path'] = './upload/imgpmb';
		$config['allowed_types'] = 'jpg|jpeg|png';
		$config['file_name'] = $nama;
		$config['max_size'] = 1000000;
		//chmod('"'.$config['upload_path'].'"/"'.$nama.'"', 0777);
        
		$this->load->library('upload', $config);

		$workb = $this->input->post('workdad');
		$worki = $this->input->post('workmom');

		if ($workb == ''){
			$workb = $this->input->post('workdad_dll');
		}else{
			$workb = $this->input->post('workdad');
		}

		if ($worki == ''){
			$worki = $this->input->post('workmom_dll');
		}else{
			$worki = $this->input->post('workmom');
		}

		$data = array(
			'ID_registrasi'	=> $this->input->post(''),
			'username'	=> $this->input->post(''),
			'password'	=> $this->input->post(''),
			'nama'	=> $this->input->post('nmcm'),
			'tlp'	=> $this->input->post('tlp'),
			'email'	=> $this->input->post('email'),
			'no_identitas'	=> $this->input->post('noid'),
			'tpt_lahir'	=> $this->input->post('tptlhr'),
			'tgl_lahir'	=> $this->input->post('tglhr'),
			'status'	=> $this->input->post('stts'),
			'agama'	=> $this->input->post('agm'),
			'asal_sekolah'	=> $this->input->post('aslskl'),
			'th_lulus'	=> $this->input->post('thls'),
			'ijazah'	=> $this->input->post('noijz'),
			'nm_ayah'	=> $this->input->post('nmdad'),
			'nm_ibu'	=> $this->input->post('nmom'),
			'tlp_ortu'	=> $this->input->post('tlpot'),
			'alamat_ortu'	=> $this->input->post('almtot'),
			'agama_ayah'	=> $this->input->post('agmdad'),
			'agama_ibu'	=> $this->input->post('agmom'),
			'status_ayah'	=> $this->input->post('stdad'),
			'status_ibu'	=> $this->input->post('stmom'),
			'pddk_ayah'	=> $this->input->post('pdkdad'),
			'pddk_ibu'	=> $this->input->post('pdkmom'),
			'work_dad'	=> $workb,
			'work_mom'	=> $worki,
			'lokasi_kamp'	=> $this->input->post('opsikps'),
			'pilih_jur'	=> $this->input->post('opsijur'),
			'pilih_kls'	=> $this->input->post('opsikls'),
			'pts_lain'	=> $this->input->post('ptsln'),
			'jur_lain'	=> $this->input->post('jurptsln'),
			'tm_tl'	=> $this->input->post('tmtl'),
			'refrensi'	=> $this->input->post('ref'),
			'jk'	=> $this->input->post('jk'),
			'alamat'	=> $this->input->post('almt'),
			'foto'	=> $config['upload_path'].'/'.$nama
			);
		//var_dump($data);exit();
		$this->app_model->insertdata('tbl_bio_cmb',$data);
		echo "<script>alert('Sukses');
		document.location.href='".base_url()."form/formpmbmhs/';</script>";
	}


}

/* End of file Gelombangpmb.php */
/* Location: ./application/modules/pmb/Gelombangpmb.php */