<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Formnilaisp extends MY_Controller {

	function __construct()
	{
		parent::__construct();
		error_reporting(0);
		$this->load->model('setting_model');
		$this->load->model('temph_model');
		$this->load->library('Cfpdf');
		//$id_menu = 55 (database); cek apakah user memiliki akses
		if ($this->session->userdata('sess_login') == TRUE) {
			$user = $this->session->userdata('sess_login');
			$akses = $this->role_model->cekakses(156)->result();
			//$aktif = $this->setting_model->getaktivasi('nilai')->result();
			//if ((count($aktif) != 0) or ($user['userid'] == '61201' or $user['userid'] == '62201')) {
			if ((count($aktif) != 0)) {
				//die('mati');
				if ($akses != TRUE) {
					echo "<script>alert('Akses Tidak Diizinkan');document.location.href='".base_url()."home';</script>";
					//echo "gabisa";
				}
			}
		} else {
			redirect('auth','refresh');
		}
		//$this->load->library('excel');
	}

	public function index()
	{
		$data['page']='form/nilai_view_sp';
		$this->load->view('template/template', $data);
	}

	function sess()
	{
		$a = $this->input->post('tahun');
		$this->session->set_userdata('mksp',$a);
		redirect(base_url('form/formnilaisp/view'));
	}

	public function view()
	{
		$log = $this->session->userdata('sess_login');
		// $subs = substr($this->session->userdata('mksp'), -1);
		// if ($subs == 3) {
			$data['user'] = $log['userid'];
			$data['year'] = $this->session->userdata('mksp');
			$data['query'] = $this->temph_model->list_improve($log['userid'],$this->session->userdata('mksp'))->result();
			$data['page']='v_list_mksp';
			$this->load->view('template/template', $data);
		// } else {
		// 	echo "
		// 	<center>
		// 		<h1>Whooops !!</h1>
		// 		<p>Maaf, halaman yang anda minta belum tersedia.</p>
		// 		<a href='".base_url('form/formnilaisp')."'>Kembali</a>
		// 	</center>
		// 	";
		// }
	}

	function seepoint($kode)
	{
		$kd = get_kd_jdl_remid($kode);
		$data['load'] = $this->temph_model->load_mhs_sp($kd)->result();
		$data['rows'] = $this->temph_model->load_mhs_sp($kd)->row();
		$data['kode_jadwal_bro'] = get_kd_jdl_remid($kode);
		$data['idjadwal'] = $this->app_model->getdetail('tbl_jadwal_matkul_sp','kd_jadwal',$data['kode_jadwal_bro'],'kd_jadwal','asc')->row()->id_jadwal;
		$data['year'] = $this->session->userdata('mksp');
		$data['page'] = "v_nilai_sp";
		$this->load->view('template/template', $data);
	}

	function dwld_excel($id)
	{
		$data['tayang_nilai'] = 'tidak';
		$data['rows'] = $this->db->query("SELECT * FROM tbl_jadwal_matkul_sp a
											JOIN tbl_matakuliah b ON a.`kd_matakuliah` = b.`kd_matakuliah` 
											JOIN tbl_karyawan c ON c.`nid` = a.`kd_dosen`
											JOIN tbl_kurikulum_matkul_new d ON a.`kd_matakuliah` = d.`kd_matakuliah`
											WHERE id_jadwal = ".$id." AND b.kd_prodi = SUBSTR(a.kd_jadwal,1,5)")->row();

		$data['ping'] = $this->db->query('SELECT DISTINCT NIMHSMSMHS,NMMHSMSMHS FROM tbl_mahasiswa WHERE NIMHSMSMHS IN 
											(SELECT a.npm_mahasiswa FROM tbl_krs_sp a 
											JOIN tbl_sinkronisasi_renkeu rk ON a.npm_mahasiswa = rk.npm_mahasiswa WHERE kd_jadwal = "'.$data['rows']->kd_jadwal.'"
											and rk.tahunajaran = SUBSTRING(a.kd_krs FROM 13 FOR 5)) order by NIMHSMSMHS asc')->result();
		$data['absendosen'] = $this->db->query("SELECT MAX(pertemuan) as satu FROM tbl_absensi_mhs_new where kd_jadwal = '".$data['rows']->kd_jadwal."'")->row();


		$data['prodi'] = $this->app_model->getdetail('tbl_jurusan_prodi','kd_prodi',substr($data['rows']->kd_jadwal, 0,5),'kd_prodi','asc')->row();
		$data['jmlh'] = $this->db->query('SELECT count(distinct npm_mahasiswa) as jumlah FROM tbl_krs_sp
										WHERE `kd_jadwal` = "'.$data['rows']->kd_jadwal.'" and npm_mahasiswa in (SELECT npm_mahasiswa from tbl_sinkronisasi_renkeu where tahunajaran = "'.yearImprove().'") ORDER BY `npm_mahasiswa` asc')->row();

		$this->load->library('excel');
		$this->load->view('excel_nilai_sp', $data);
	}

	function upload_excel()
	{
		$session=$this->session->userdata('sess_dosen');
		$user = $session['userid'];

		if ($_FILES['userfile']) {
			$t 			= $this->input->post('tipe');
			$kd 		= $this->input->post('kode_jdl');
			$id_jadwal 	= $this->input->post('id_jadwal');

			$mk = $this->db->query('SELECT distinct a.kd_matakuliah,b.nama_matakuliah,b.semester_matakuliah FROM tbl_jadwal_matkul a 
											JOIN tbl_matakuliah b ON a.`kd_matakuliah` = b.`kd_matakuliah`
											WHERE a.`kd_jadwal` = "'.$kd.'" ')->row();

			$data['jmlh'] = $this->db->query('SELECT count(distinct NIMHSMSMHS) as jumlah FROM tbl_mahasiswa a
										JOIN tbl_krs_sp b ON a.`NIMHSMSMHS` = b.`npm_mahasiswa`
										JOIN tbl_jadwal_matkul_sp c ON b.`kd_jadwal` = c.`kd_jadwal`
										join tbl_sinkronisasi_renkeu rk on rk.npm_mahasiswa = b.npm_mahasiswa
										WHERE c.`kd_jadwal` = "'.$kd.'" and rk.tahunajaran = SUBSTRING(b.kd_krs FROM 13 FOR 5) ORDER BY a.`NIMHSMSMHS` asc')->row();
			// var_dump($data['jmlh']);exit();
			$this->load->helper('inflector');
			$nama = rand(1,10000000).underscore(str_replace('/', '', $this->security->sanitize_filename($_FILES['userfile']['name'])));
			$kadal = explode('.', $nama);
			if (count($kadal) > 2) {
				echo "<script>alert('FORMAT FILE MENCURIGAKAN!');history.go(-1)';</script>";exit();
			}

			$jumlah 					= $data['jmlh']->jumlah;
			$config['upload_path']		= './upload/improvement_class/';
			$config['allowed_types']	= 'xls|xlsx';
			$config['file_name'] 		= $nama;
			$config['max_size'] 		= 1000000;

			// var_dump($config);exit();
            
			$this->load->library('upload', $config);

			if (!$this->upload->do_upload("userfile")) {
				$data = array('error', $this->upload->display_errors());
				echo "<script>alert('Gagal');document.location.href='".base_url()."form/formnilaisp/';</script>";
			} else {
				$dfuser 			= $this->session->userdata('sess_login');
				$datf['nama_file']	= $config['file_name'];
				$datf['waktu'] 		= date('Y-m-d H:i:s');
				$datf['tipe'] 		= 1;
				$datf['kode'] 		= $kd;
				$datf['path'] 		= $config['upload_path'].$nama;
				$datf['userid'] 	= $dfuser['userid'];
				$datf['mac_addr'] 	= $mac;
				$datf['ip_addr'] 	= str_replace('(', '', str_replace(')', '', $ip));
				//var_dump($datf);exit();
				$this->app_model->insertdata('tbl_file_upload', $datf);
				//$data = array('error' => false);
	   			$upload_data = $this->upload->data(); 
	   			$this->load->library('excel_reader');
	   			$this->excel_reader->setOutputEncoding('CP1251');
				$file=$upload_data['full_path'];
				$this->excel_reader->read($file);
				$data=$this->excel_reader->sheets[0];
				$dataexcel = array();
				
				for ($i = 0; $i < $jumlah; $i++) {
				
	                $dataexcel[$i]['npm'] = substr($data['cells'][$i+8][2], 0,12);
	                $dataexcel[$i]['nilai_1'] = number_format($data['cells'][$i+8][7],2); //tugas
	                $dataexcel[$i]['nilai_2'] = number_format($data['cells'][$i+8][6],2); //absen
	                $dataexcel[$i]['nilai_3'] = number_format($data['cells'][$i+8][8],2); //uts
	                $dataexcel[$i]['nilai_4'] = number_format($data['cells'][$i+8][9],2); //uas
	                $dataexcel[$i]['nilai_5'] = $data['cells'][$i+8][10]; //nilai akhir angka
	                $dataexcel[$i]['nilai_6'] = number_format($data['cells'][$i+8][4],2); //detil absen dosen
	                $dataexcel[$i]['nilai_7'] = number_format($data['cells'][$i+8][5],2); //detil absen mhs
	                $dataexcel[$i]['code'] = $kd;
	                $dataexcel[$i]['tran'] = $v;

	                $ceknpm = $this->db->query("SELECT x.npm_mahasiswa from tbl_krs_sp x join tbl_sinkronisasi_renkeu rk on rk.npm_mahasiswa = '".$dataexcel[$i]['npm']."' where x.kd_jadwal = '".$kd."'
								 and rk.tahunajaran = SUBSTRING(x.kd_krs FROM 13 FOR 5)")->result();
	                // var_dump(count($ceknpm));exit();
	                if (count($ceknpm) < 1){
	                	echo "Gagal Upload, Inputan Data Kelas Tidak Sesuai . <a href='javascript:history.go(-1)'><< Kembali</a>";
	                	var_dump("SELECT x.npm_mahasiswa from tbl_krs_sp x join tbl_sinkronisasi_renkeu rk on rk.npm_mahasiswa = '".$dataexcel[$i]['npm']."' where x.kd_jadwal = '".$kd."'
								 and rk.tahunajaran = SUBSTRING(x.kd_krs FROM 13 FOR 5)");
	                	exit();
	                }

	                $datas[] = $dataexcel[$i]['npm'];

	                if ( (($dataexcel[$i]['nilai_1']) < 0) or (($dataexcel[$i]['nilai_2']) < 0) or (($dataexcel[$i]['nilai_3']) < 0) or (($dataexcel[$i]['nilai_4']) < 0) or (($dataexcel[$i]['nilai_1']) > 101) or (($dataexcel[$i]['nilai_2']) > 101) or (($dataexcel[$i]['nilai_3']) > 101) or (($dataexcel[$i]['nilai_4']) > 101) or (ctype_alpha(($dataexcel[$i]['nilai_1']))) or (ctype_alpha(($dataexcel[$i]['nilai_3']))) or (ctype_alpha(($dataexcel[$i]['nilai_4']))) ) {
	                	echo "Gagal Upload, Inputan Nilai Tidak Sesuai . <a href='javascript:history.go(-1)'><< Kembali</a>";
	                	exit();
	          
	                }
		                
		    	}

		    	$data['nama_dokumen'] = $config['file_name'];

    			$hitungpeserta = $this->db->query("SELECT distinct count(x.npm_mahasiswa) as jum from tbl_krs_sp x join tbl_sinkronisasi_renkeu rk on rk.npm_mahasiswa = x.npm_mahasiswa where x.kd_jadwal = '".$kd."'
								 					and rk.tahunajaran = SUBSTRING(x.kd_krs FROM 13 FOR 5)")->row()->jum;
                
    			// var_dump($hitungpeserta.'==='.count(array_unique($datas)));exit();

                if ($hitungpeserta != count(array_unique($datas))) {
                	echo "Gagal Upload, Mohon cek data mahasiswa. <a href='javascript:history.go(-1)'><< Kembali</a>";
					var_dump($hitungpeserta);
                	exit();
                }

		    	$this->tambahnilai($dataexcel,$kd);	
			    			    	
			}
	    	
	    }

	}

	function tambahnilai($dataarray,$kd)
    {
    	//var_dump($dataarray);exit();
    	$userr = $this->session->userdata('sess_login');
		$nikk  = $this->db->query("SELECT b.nid from tbl_jadwal_matkul_sp a join tbl_karyawan b on a.kd_dosen = b.nid where a.kd_jadwal = '".$kd."'")->row()->nid;
		$tahunn = $this->session->userdata('mksp');
		$this->db->query('DELETE from tbl_nilai_detail_sp where kd_jadwal like "%'.$kd.'%"');
		//var_dump('delete from tbl_nilai_detail where kd_jadwal="'.$kd.'"');exit();
        for($i=0; $i < count($dataarray); $i++){
        	$mk = $this->db->query('SELECT distinct a.kd_matakuliah,b.nama_matakuliah,b.semester_matakuliah FROM tbl_jadwal_matkul_sp a 
											JOIN tbl_matakuliah b ON a.`kd_matakuliah` = b.`kd_matakuliah`
											WHERE a.`kd_jadwal` = "'.$kd.'" ')->row();
        	$data['lala'] =  $this->db->query('SELECT * from tbl_verifikasi_krs_sp a join tbl_mahasiswa b
    										on a.`npm_mahasiswa`= b.`NIMHSMSMHS`
    										where a.`npm_mahasiswa`="'.$dataarray[$i]['npm'].'" and tahunajaran = "'.$tahunn.'"')->row();
        	//$aw = $this->db->query('delete from tbl_nilai_detail where npm_mahasiswa = "'.$dataarray[$i]['npm'].'" and kd_jadwal="'.$dataarray[$i]['code'].'"');
        	for ($z=1; $z < 8; $z++) { 

        		$a = $dataarray[$i]['nilai_'.$z.''];
        		$b = $a[0];
        		$c = $a[1];

        		if ($a == '' or is_null($a)) {
        			$a = NULL;
        		}
        		
        		
	            $datax[] = array(
	                'npm_mahasiswa'		=> $dataarray[$i]['npm'],
	                'kd_krs'			=> $data['lala']->kd_krs,
	                'kd_matakuliah'		=> $mk->kd_matakuliah,
	                'tahun_ajaran'		=> $tahunn,
	                'tipe'				=> $z,
	                'kd_prodi'			=> $data['lala']->KDPSTMSMHS,
	                'nid'				=> $nikk,
	                'audit_date'		=> date('Y-m-d H:i:s'),
	                'kd_transaksi_nilai'=> $kd.'zzz'.$dataarray[$i]['npm'], //$dataarray[$i]['tran'],
	                'nilai'				=> $a,
	                'flag_publikasi'	=> 1,
	                'kd_jadwal'			=> $dataarray[$i]['code'],
	                'nilai_huruf_old'	=> '',
	                'nilai_angka_old'	=> ''
	                    );
	            //}
        	}

    	}
    	// var_dump($datax);exit();
    	$datax = $this->security->xss_clean($datax);
    	$this->db->insert_batch('tbl_nilai_detail_sp',$datax);
    	$idjadwal = $this->app_model->getdetail('tbl_jadwal_matkul_sp','kd_jadwal',$kd,'kd_jadwal','asc')->row()->id_jadwal;
        echo "<script>alert('Sukses');
		document.location.href='".base_url()."form/formnilaisp/seepoint/".$idjadwal."';</script>";
        
    }

    function pub_all($id){

		$kodejdl = $this->app_model->getdetail('tbl_jadwal_matkul_sp','id_jadwal',$id,'id_jadwal','asc')->row();

		$kelas = $this->db->query('SELECT a.`npm_mahasiswa`,a.`nilai`,a.`kd_matakuliah`,a.`kd_transaksi_nilai` FROM tbl_nilai_detail_sp a
									WHERE a.`tipe` = 5 AND a.`kd_jadwal` = "'.$kodejdl->kd_jadwal.'" AND tahun_ajaran = '.$this->session->userdata('mksp').'')->result();
		
		$this->db->query('DELETE from tbl_transaksi_nilai_sp where kd_transaksi_nilai like "'.$kodejdl->kd_jadwal.'zzz%"');

		foreach ($kelas as $isi) {

			//$rt = $this->app_model->getnilai($isi->kd_jadwal,$isi->npm_mahasiswa,10,$isi->kd_matakuliah)->row()->nilai;

			$logged = $this->session->userdata('sess_login');
			
			$kd_nilai = $isi->kd_transaksi_nilai;

			// $logged = $this->session->userdata('sess_login');
			// if (($logged['userid'] == '74101')) {
				$rtnew = number_format($isi->nilai,2);
				if (($rtnew >= 80) and ($rtnew <= 100)) {
					$rw = "B";
					$nilai_angka= 4;
				} elseif (($rtnew >= 76) and ($rtnew <= 79.99)) {
					$rw = "B"; 
					$nilai_angka= 3.70;
				} elseif (($rtnew >= 72) and ($rtnew <= 75.99)) {
					$rw = "B";
					$nilai_angka= 3.30;
				} elseif (($rtnew >= 68) and ($rtnew <= 71.99)) {
					$rw = "B";
					$nilai_angka= 3;
				} elseif (($rtnew >= 64) and ($rtnew <= 67.99)) {
					$rw = "B-";
					$nilai_angka= 2.70;
				} elseif (($rtnew >= 60) and ($rtnew <= 63.99)) {
					$rw = "C+";
					$nilai_angka= 2;
				} elseif (($rtnew >= 56) and ($rtnew <= 59.99)) {
					$rw = "C";
					$nilai_angka= 2;
				} elseif (($rtnew >= 45) and ($rtnew <= 55.99)) {
					$rw = "D";
					$nilai_angka= 1;
				} elseif (($rtnew >= 0) and ($rtnew <= 44.99)) {
					$rw = "E";
					$nilai_angka= 0;
				} elseif (($rtnew > 100)) {
					$rw = "T";
					$nilai_angka= 0;
				}
			/**} else {
				$rtnew = number_format($isi->nilai,2);
				if (($rtnew >= 80) and ($rtnew <= 100)) {
					$rw = "B";
					$nilai_angka= 3;
				} elseif (($rtnew >= 65) and ($rtnew <= 79.99)) {
					$rw = "B";
					$nilai_angka= 3;
				} elseif (($rtnew >= 55) and ($rtnew <= 64.99)) {
					$rw = "C";
					$nilai_angka= 2;
				} elseif (($rtnew >= 45) and ($rtnew <= 54.99)) {
					$rw = "D";
					$nilai_angka= 1;
				} elseif (($rtnew >= 0) and ($rtnew <= 44.99)) {
					$rw = "E";
					$nilai_angka= 0;
				} elseif (($rtnew > 100)) {
					$rw = "T";
					$nilai_angka= 0;
				}
			} **/

			// update nilai sebelumnya //
			$bf_point = $this->temph_model->load_nilai_bfore($isi->npm_mahasiswa,$this->session->userdata('mksp')-2,$isi->kd_matakuliah)->row();
			$arrBfPoint[] = $bf_point;
			$arrAffPoint[$isi->npm_mahasiswa][] = $nilai_angka;
			//var_dump($bf_point);exit();
				
			if ($bf_point->BOBOTTRLNM < $nilai_angka) {
				$dats = array(
						'BOBOTTRLNM' => $nilai_angka, 
						'NLAKHTRLNM' => $rw,
						'nilai_akhir'=> $rtnew
					);

				$THSMSTRLNM = $this->session->userdata('mksp')-2;

				$this->db->where('THSMSTRLNM', $THSMSTRLNM);
				$this->db->where('NIMHSTRLNM', $isi->npm_mahasiswa);
				$this->db->where('KDKMKTRLNM', $isi->kd_matakuliah);
				$this->db->update('tbl_transaksi_nilai', $dats);
			}

			$data[] = array(
				'THSMSTRLNM' => $this->session->userdata('mksp'), 
				'KDPTITRLNM' => '031036',
				'KDJENTRLNM' => 'C',
				'KDPSTTRLNM' => $logged['userid'],
				'NIMHSTRLNM' => $isi->npm_mahasiswa,
				'KDKMKTRLNM' => $isi->kd_matakuliah,
				'NLAKHTRLNM' => $rw,
				'BOBOTTRLNM' => $nilai_angka,
				'KELASTRLNM' => 17,
				'nilai_akhir' => $rtnew,
				'nilai_sebelum' => $bf_point->nilai_akhir,
				'kd_transaksi_nilai' => $isi->kd_transaksi_nilai
				);
 
		}

		// var_dump($data);exit();
    	$this->db->insert_batch('tbl_transaksi_nilai_sp', $data);

    	$datax['flag_publikasi'] = 2;
		$datax['publis_date'] = date('Y-m-d h:i:s');
    	$this->db->like('kd_transaksi_nilai', $kodejdl->kd_jadwal.'zzz','after');
		$this->db->update('tbl_nilai_detail_sp', $datax);

		echo "<script>alert('Publikasi Sukses');
		document.location.href='".base_url()."form/formnilaisp/view/".$this->session->userdata('mksp')."';</script>";
		//redirect(base_url('akademik/publikasi/view'));
	}

	function back()
	{
		$this->session->unset_userdata('mksp');
		redirect(base_url('form/formnilaisp'),'refresh');
	}

	function cetakkhs($kd)
	{
		$log = $this->session->userdata('sess_login');
		$data['prodi'] = $this->db->query("SELECT * from tbl_jurusan_prodi a join tbl_fakultas b on a.kd_fakultas = b.kd_fakultas where a.kd_prodi = '".$log['userid']."'")->row();
		$data['mhs'] = $this->db->query("SELECT distinct a.semester_krs,b.tahunajaran,c.NIMHSMSMHS,c.NMMHSMSMHS from tbl_krs_sp a 
											join tbl_verifikasi_krs_sp b on a.kd_krs = b.kd_krs 
											join tbl_mahasiswa c on c.NIMHSMSMHS = b.npm_mahasiswa where a.kd_krs = '".$kd."'")->row();

		$data['idp'] = $this->db->query("SELECT * from tbl_verifikasi_krs a join tbl_karyawan b on a.id_pembimbing = b.nid where a.npm_mahasiswa = '".$data['mhs']->NIMHSMSMHS."'")->row();

		$data['query'] = $this->db->query("SELECT a.tipe,a.nilai,a.kd_matakuliah,c.nama_matakuliah,c.sks_matakuliah,d.nama,b.NLAKHTRLNM,b.THSMSTRLNM,b.NIMHSTRLNM from tbl_nilai_detail_sp a join tbl_transaksi_nilai b on a.kd_transaksi_nilai = b.kd_transaksi_nilai 
											join tbl_matakuliah c on c.kd_matakuliah = a.kd_matakuliah 
											join tbl_karyawan d on d.nid = a.nid where a.kd_krs = '".$kd."'
											and a.tipe = 5 and c.kd_prodi = '".$log['userid']."'")->result();
		$this->load->view('pdf_khs_sp',$data);
	}

	function cetak_nilai($id)
	{
		$this->load->library('Cfpdf');

		$data['id_jadwal']=$id;

		$data['rows'] = $this->db->query('SELECT * FROM tbl_jadwal_matkul_sp a
											JOIN tbl_matakuliah b ON a.`kd_matakuliah` = b.`kd_matakuliah` 
											JOIN tbl_karyawan c ON c.`nid` = a.`kd_dosen`
											WHERE a.id_jadwal = "'.$id.'"
											and b.kd_prodi = SUBSTRING(a.kd_jadwal, 1, 5) ')->row();

		$data['absendosen'] = $this->db->query("SELECT count(distinct pertemuan) as satu FROM tbl_absensi_mhs_new where kd_jadwal = '".$data['rows']->kd_jadwal."'")->row();

		$data['title'] = $this->db->query('SELECT * FROM tbl_matakuliah a 
											JOIN tbl_jurusan_prodi b ON a.`kd_prodi` = b.`kd_prodi`
											JOIN tbl_fakultas c ON b.`kd_fakultas` = c.`kd_fakultas`
											WHERE a.`kd_matakuliah` = "'.$data['rows']->kd_matakuliah.'" and a.kd_prodi = "'.$data['rows']->kd_prodi.'"')->row();


		// $data['mhs'] = $this->db->query('SELECT distinct mhs.`NIMHSMSMHS`,mhs.`NMMHSMSMHS`,b.`kd_jadwal`,b.`kd_matakuliah` FROM tbl_krs b
		// 									JOIN tbl_mahasiswa mhs ON b.`npm_mahasiswa` = mhs.`NIMHSMSMHS`
		// 									WHERE kd_jadwal = "'.$data['rows']->kd_jadwal.'" ORDER BY mhs.`NIMHSMSMHS` asc')->result();

		$yearimp = yearImprove();
		$data['mhs'] = $this->db->query('SELECT DISTINCT NIMHSMSMHS,NMMHSMSMHS FROM tbl_mahasiswa WHERE NIMHSMSMHS IN (SELECT a.npm_mahasiswa FROM tbl_krs_sp a JOIN tbl_sinkronisasi_renkeu b on a.npm_mahasiswa = b.npm_mahasiswa WHERE kd_jadwal = "'.$data['rows']->kd_jadwal.'" and b.tahunajaran = "20184")')->result();
		
		$this->load->view('form/pdf_nilai_sp',$data);
		
	}

}

/* End of file Formnilaisp.php */
/* Location: ./application/modules/form/controllers/Formnilaisp.php */