<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Krspaket extends MY_Controller {

	function __construct()
	{
		parent::__construct();
		error_reporting(0);
		ini_set('memory_limit', '1024M');
		$this->load->model('temph_model');
		$this->load->model('krspaket_model', 'paket');
		$this->load->model('hilman_model', 'hilm');

		$this->dbreg = $this->load->database('regis', TRUE);
		// $this->load->library('Cfpdf');
		// $this->load->model('setting_model');
		if ($this->session->userdata('sess_login') == TRUE) {
			$user = $this->session->userdata('sess_login');
		// 	$akses = $this->role_model->cekakses(58)->result();
		// 		if ($akses != TRUE) {
		// 			echo "<script>alert('Akses Tidak Diizinkan');document.location.href='".base_url()."home';</script>";
		// 		}
		} else {
			redirect('auth','refresh');
		}
	}

	public function index()
	{
		$y = getactyear();
		$u = $this->session->userdata('sess_login');
		$data['kelas'] = $this->temph_model->load_list_kelas($u['userid'],$y)->result();
		$data['page'] = 'paket_view';
		$this->load->view('template/template', $data);
	}	

	function save_sess()
	{
		$this->session->set_userdata('angkatan', $this->input->post('tahun', TRUE));
		$this->session->set_userdata('dsn',$this->input->post('dsn', TRUE));
		$this->session->set_userdata('kls',$this->input->post('kelas', TRUE));
		redirect(base_url('form/krspaket/load'),'refresh');
	}

	function load()
	{
		//$tahun = $this->input->post('', TRUE);
		$logged = $this->session->userdata('sess_login');
		$data['uid'] = $logged['userid'];
		$typCls = typeofclass_byName($this->session->userdata('kls'),$logged['userid']);
		$data['user'] = $logged['userid'];
		$data['dosen'] = nama_dsn($this->session->userdata('dsn'));
		$data['kelas'] = $this->session->userdata('kls');
		$data['pckg'] = $this->temph_model->load_pkg_for_mk($logged['userid'])->result();
		// if ($logged['userid'] == '74101' or $logged['userid'] == '61101') {
			// $data['getData'] = $this->temph_model->load_krs_pasca($logged['userid'])->result(); //ini digunakan saat pmb masih 1 aplikasi dgn sia
			// $data['getData'] = $this->temph_model->load_krs_pasca_newpmb($logged['userid'])->result();
			// var_dump($data['getData']);exit();
		// } else {
			// $this->db->where('KDPSTMSMHS', $logged['userid']);
		$angkatan = $this->session->userdata('angkatan');
		$data['getData'] = $this->temph_model->load_krs_pasca_newpmb($logged['userid'], $typCls, $angkatan)->result();
			// $data['getData'] = $this->db->get('view_mhs_paket')->result();
		// }
		$data['page'] = 'paket_load';
		$this->load->view('template/template', $data);
	}

	function save()
	{
		if (count($this->input->post('mhs', TRUE)) == 0) {
			echo "<script>alert('Tidak Ada Data Terinput !');location.href='".base_url('form/krspaket/load')."';</script>";
		} 

		$tahun 	= getactyear();
		$logged = $this->session->userdata('sess_login');
		$tung 	= count($this->input->post('mhs'));
		
		for ($i=0; $i < $tung; $i++) {

			$data['npm_mahasiswa'] = $this->input->post('mhs['.$i.']', TRUE);
			$kodeawal = $data['npm_mahasiswa'].$tahun;
			$hitung = strlen($kodeawal);
			$jumlah = 0;

			for ($a=0; $a <= $hitung; $a++) {
				$char = substr($kodeawal,$a,1);
				$jumlah = $jumlah + $char;
			}
	
			$mod 					= $jumlah%10;
			$kodebaru 				= $data['npm_mahasiswa'].$tahun.$mod;
			$data['semester_krs']	= 1;
			$data['kd_krs'] 		= $kodebaru;
			$getkur 				= $this->paket->getKurikulum($logged['userid'])->row();
			$kd_matkuliah 			= $this->paket->getKdMk($getkur->kd_kurikulum)->result();
			$nol = 0;

			foreach ($kd_matkuliah as $n) {
				$data['kd_matakuliah'] = $n->kd_matakuliah;

				// pilih jadwal untuk tiap mahasiswa
				$sessclass = $this->session->userdata('kls');
				$jdlmk = $this->paket->getJadwal($logged['userid'],$tahun,$n->kd_matakuliah,$sessclass)->row();
				
				// cek kuota kelas, bila penuh alihkan ke kelas lain
				//$kelas = $this->db->query("SELECT kuota from tbl_ruangan where id_ruangan = '".$jdlmk->kd_ruangan."'")->row()->kuota;
				//$subst = number_format(substr($kelas, 0,2));

				// gunakan _tes jika testing
				//$countkelas = $this->db->query("SELECT npm_mahasiswa from tbl_krs where kd_jadwal = '".$jdlmk->kd_jadwal."'")->num_rows();

				// $add = $subst+1;

				// var_dump($n->kd_matakuliah.'--'.$countkelas.'--'.$subst.'--'.$jdlmk->kd_ruangan);
				//if ($countkelas <= $subst) {
					$jadwal = $jdlmk->kd_jadwal;
				//} else {
				//	$typeclass = typeofclass_byName($this->session->userdata('kls'));
				//	$jdlmkk = $this->db->query("SELECT kd_jadwal from tbl_jadwal_matkul where kd_jadwal like '".$logged['userid']."%' and kd_tahunajaran = '".$tahun->kode."' 
				//							and kd_matakuliah = '".$n->kd_matakuliah."'and waktu_kelas = '".$typeclass."' and kd_jadwal != '".$jdlmk->kd_jadwal."'")->row();
				//	$jadwal = $jdlmkk->kd_jadwal;
				//}
				$data['kd_jadwal'] = $jadwal;



				// cek ketersediaan KRS
				$arr = ['kd_krs' => $kodebaru, 'kd_matakuliah' => $n->kd_matakuliah ];
				$cekkrs = $this->hilm->moreWhere($arr, 'tbl_krs')->result();

				// jika sudah ada KRS
				if (count($cekkrs) > 0) {

					$this->db->where('kd_krs', $kodebaru);
					$this->db->update('tbl_krs', $data);

				// jika belum ada KRS
				} else {

					$this->app_model->insertdata('tbl_krs', $data);	

				}

				// sum sks
				$sumsks = $this->paket->amountSks($n->kd_matakuliah,$logged['userid'])->result();
				foreach ($sumsks as $key) {
					$nol = $nol + $key->sks_matakuliah;
				}
			}
			// var_dump($nol);exit();
			// insert array to tbl_pa
			$pa['kd_dosen']			= $this->session->userdata('dsn');
			$pa['npm_mahasiswa']	= $data['npm_mahasiswa'];
			$pa['audit_user']		= $logged['userid'];
			$pa['audit_time']		= date('Y-m-d H:i:d');
			
			// insert to tbl_verifikasi_krs
			$datas['id_pembimbing']		= $this->session->userdata('dsn');
			$datas['kd_krs'] 			= $kodebaru;
			$datas['keterangan_krs']	= 'Selamat Datang Di  , Semangat Belajar';
			$datas['tgl_bimbingan'] 	= date('Y-m-d h:i:s');
			$datas['key'] 				= $this->generateRandomString();
			$datas['tahunajaran'] 		= $tahun;
			$datas['npm_mahasiswa'] 	= $data['npm_mahasiswa'];
			$datas['kd_jurusan'] 		= get_mhs_jur($datas['npm_mahasiswa']);
			$datas['jumlah_sks'] 		= $nol;
			$kodver 					= $kodebaru.$datas['jumlah_sks'].$datas['key'];
			$datas['kd_krs_verifikasi'] = md5(md5($kodver).key_verifikasi);
			$datas['status_verifikasi']	= '1';
			
			//qr code
			$this->load->library('ciqrcode');
			$params['data'] = base_url().'welcome/welcome/cekkodeverifikasi/'.$datas['kd_krs_verifikasi'].'';
			$params['level'] = 'H';
			$params['size'] = 10;
			$params['savename'] = FCPATH.'QRImage/'.$datas['kd_krs_verifikasi'].'.png';
			$this->ciqrcode->generate($params);

			$this->dbreg->where('npm_baru', $datas['npm_mahasiswa']);
			$kelas = $this->dbreg->get('tbl_form_pmb')->row()->kelas;
			switch ($kelas) {
				case 'PG':
					$datas['kelas'] = 'PG';
					$pa['kelas_mhs']= 'PG';
					break;

				case 'SR':
					$datas['kelas'] = 'SR';
					$pa['kelas_mhs']= 'SR';
					break;
				
				case 'KY':
					$datas['kelas'] = 'PK';
					$pa['kelas_mhs']= 'PK';
					break;
			}

			$nama = $this->app_model->getdetail('tbl_mahasiswa','NIMHSMSMHS',$datas['npm_mahasiswa'],'NIMHSMSMHS','asc')->row();
			$slug = url_title($nama->NMMHSMSMHS, '_', TRUE);
			$datas['slug_url'] = $slug;

			// cek krs
			$verif = $this->app_model->getdetail('tbl_verifikasi_krs','kd_krs',$kodebaru,'kd_krs','asc')->num_rows();

			// jika verif sudah ada
			if ($verif > 0) {
			
				$this->db->where('kd_krs', $kodebaru);
				$this->db->update('tbl_verifikasi_krs', $datas);
			
			// jika verif belum ada
			} else {
			
				$this->app_model->insertdata('tbl_verifikasi_krs', $datas);
			
			}	
			
			// insert to tbl_pa
			$cekpa = $this->app_model->getdetail('tbl_pa','npm_mahasiswa',$data['npm_mahasiswa'],'npm_mahasiswa','asc')->num_rows();

			// jika pa sudah ada
			if ($cekpa > 0) {

				$this->db->where('npm_mahasiswa', $data['npm_mahasiswa']);
				$this->db->update('tbl_pa', $pa);

			// jika pa belum ada
			} else {

				$this->app_model->insertdata('tbl_pa', $pa);

			}
			
		}
		echo "<script>alert('Berhasil');document.location.href='".base_url()."form/krspaket/load';</script>";
	}

	function generateRandomString() 
	{
	    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
	    $charactersLength = strlen($characters);
	    $randomString = '';
	    for ($i = 0; $i < 5; $i++) {
	        $randomString .= $characters[rand(0, $charactersLength - 1)];
	    }
	    return $randomString;
	}

	function jadwal($kd_krs,$kls){
		if ($kls == '5f4845') {
			$kl = 'PG';
		} elseif ($kls == 'f5b5bd') {
			$kl = 'SR';
		} elseif ($kls == 'd53464') {
			$kl = 'PK';
		}

		$this->session->set_userdata('kelas', $kl);
		//$this->get_jadwal($this->session->userdata('kelas'));
		
			$data['npm_mahasiswa']	= substr($kd_krs, 0, 9);
			$data['namamhs']		= $this->db->query("SELECT NMMHSMSMHS from tbl_mahasiswa where NIMHSMSMHS = '".substr($kd_krs, 0, 9)."'")->row();
			$data['kd_krs']			= $kd_krs;
			$data['pembimbing'] 	= $this->app_model->get_pembimbing_krs($kd_krs)->row_array(); // gunakan _tes jika testing
			
			$tahunakademik 			= $this->app_model->getdetail('tbl_tahunakademik', 'status', 1, 'kode', 'ASC')->row();

			$data['noteformhs'] 	= $this->db->query("SELECT * from tbl_verifikasi_krs where kd_krs like '".substr($kd_krs, 0, 9).$tahunakademik->kode."%'")->row_array();
			$data['detail_krs'] 	= $this->app_model->get_detail_print_krs_mahasiswa($kd_krs)->result(); // gunakan _tes jika testing

			$data['page'] = 'form/krs_mhs_jdl_maba';
			$this->load->view('template/template',$data);   
  		// }
	}

	function get_jadwal(){
	  	$tahunakademik = $this->app_model->getdetail('tbl_tahunakademik', 'status', 1, 'kode', 'ASC')->row();
		$kd_matakuliah = $this->input->post('kd_matakuliah');
		$user = $this->session->userdata('sess_login');
		$nim  = $user['userid'];
		$mhs  = $this->app_model->get_jurusan_mhs($nim)->row();
		
		// $data = $this->db->query('SELECT * 
		// 					FROM tbl_jadwal_matkul a
		// 					LEFT JOIN tbl_ruangan b ON a.`kelas` = b.`id_ruangan`
		// 					LEFT JOIN tbl_karyawan c ON a.`kd_dosen` = c.`nid`
		// 					LEFT JOIN tbl_matakuliah d ON a.`kd_matakuliah` = d.`kd_matakuliah`
		// 					WHERE  a.`kd_jadwal` LIKE "'.$mhs->KDPSTMSMHS.'%" AND a.`kd_matakuliah` = "'.$kd_matakuliah.'" AND d.`kd_prodi` LIKE "'.$mhs->KDPSTMSMHS.'"');
		// var_dump($kd_matakuliah.'--'.$nim.'--'.$tahunakademik->kode.'--'.$this->session->userdata('kelas'));exit();
		
		// $data = $this->app_model->get_pilih_jadwal_krs($kd_matakuliah,$nim,'20161',$this->session->userdata('kelas'))->result();
		$data = $this->app_model->get_pilih_jadwal_krs($kd_matakuliah,$nim,$tahunakademik->kode,$this->session->userdata('kelas'))->result();
                
		$js = json_encode($data);
		echo $js;
	}

	function save_jadwal()
	{
		$data['kd_jadwal'] = $this->input->post('kd_jadwal');

		$kd_matakuliah = $this->input->post('kd_matakuliah');

		$kd_krs= $this->input->post('kd_krs');

		$npm_mahasiswa = substr($kd_krs, 0,12);
		
		$this->app_model->updatedata_krs($kd_matakuliah, $kd_krs, $data); // gunakan _tes jika testing

		if ($this->session->userdata('kelas') == 'PG') {
			$cls = 'PAGI';
		} elseif ($this->session->userdata('kelas') == 'SR') {
			$cls = 'SORE';
		} else {
			$cls = 'P2K';
		}
		
		redirect(base_url('form/krspaket/jadwal/'.$kd_krs.'/'.substr(md5($cls), 0,6)),'refresh');

	}

	function kosongkan_jadwal($id,$id2)
	{

		$data = array('kd_jadwal' => null );
		$npm_mahasiswa = substr($id, 0,12);


		$this->db->where('kd_krs', $id);
		$this->db->where('kd_matakuliah', $id2);
		$this->db->update('tbl_krs', $data); // gunakan _tes jika testing

		// gunakan _tes jika testing
		$q = $this->db->select('kd_krs')
						->from('tbl_krs') 
						->where('id_krs',$id)
						->get()->row();

		if ($this->session->userdata('kelas') == 'PG') {
			$cls = 'PAGI';
		} elseif ($this->session->userdata('kelas') == 'SR') {
			$cls = 'SORE';
		} else {
			$cls = 'P2K';
		}

		redirect(base_url().'form/krspaket/jadwal/'.$id.'/'.substr(md5($cls), 0,6),'refresh');

	}

	function load_dosen_autocomplete()
	{

        $this->db->distinct();

        $this->db->select("a.id_kary,a.nid,a.nama");

        $this->db->from('tbl_karyawan a');

        $this->db->like('a.nama', $_GET['term'], 'both');

        $this->db->or_like('a.nid', $_GET['term'], 'both');

        $sql  = $this->db->get();

        $data = array();

        foreach ($sql->result() as $row) {

            $data[] = array(

                            'id_kary'       => $row->id_kary,

                            'nid'           => $row->nid,

                            'value'         => $row->nid.' - '.$row->nama

                            );

        }

        echo json_encode($data);

    }

    function printkrs($id)
    {
		$data['kd_krs'] = $id;
		$data['npm'] = substr($id, 0,12);
		$data['ta']  = substr($id, 12,4);
		$nim		= substr($id, 0,12);
		$tah		= substr($id, 12,5);
		$renkeu = $this->app_model->renkeu_printKRS($nim,$tah)->row();
		if ($renkeu == false)
		{
			echo "<script>alert('Maaf, Silahkan menyelesaikan administrasi terlebih dahulu');
			document.location.href='".base_url()."';</script>";
		} 
		elseif ($renkeu == true)
		{
		
			// $data['prodi']= $this->db->where('NIMHSMSMHS',$data['npm'])
			// 					 	 ->get('tbl_mahasiswa')->row();

			//$data['kdver'] = $this->db->query('SELECT kd_krs_verifikasi from tbl_verifikasi_krs WHERE npm_mahasiswa = "'.$data['npm'].'"')->row();
			$data['kdver'] = $this->db->query('SELECT kd_krs_verifikasi from tbl_verifikasi_krs WHERE kd_krs = "'.$id.'" order by kd_krs_verifikasi desc limit 1')->row();

			$data['footer'] = $this->db->select('npm_mahasiswa,id_pembimbing')
									->from('tbl_verifikasi_krs')
									->like('npm_mahasiswa', $data['npm'],'both')
									->get()->row();
			
			$a=substr($id, 16,1);

			if ($a == 1) {
				$b = 'Ganjil';
			} else {
				$b = 'Genap';
			}

			$data['gg']  = $b;

			$this->load->view('welcome/print/krs_pdf',$data);
		}
	}

	function destroysess()
	{
		$arr = array('dsn','kls');
		$this->session->unset_userdata($arr);
		redirect(base_url('form/krspaket'),'refresh');
	}

}

/* End of file Krspaket.php */
/* Location: ./application/modules/form/controllers/Krspaket.php */