<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Nilai extends MY_Controller
{

	private $userid, $usergroup;

	public function __construct()
	{
		parent::__construct();
		if (!$this->session->userdata('sess_login')) {
			redirect(base_url('auth/logout'), 'refresh');
		}
		$this->load->model('form/nilai_model', 'nilai');
		$this->userid = $this->session->userdata('sess_login')['userid'];
		$this->usergroup = get_group($this->session->userdata('sess_login')['id_user_group']);
	}

	public function index()
	{
		$this->session->unset_userdata('tahunajar_nilai');
		$this->session->unset_userdata('prodi_nilai');
		$data['usergroup']   = $this->usergroup;
		$data['prodi']       = $this->app_model->getdata('tbl_jurusan_prodi', 'kd_prodi', 'asc')->result();
		$data['tahunajaran'] = $this->app_model->getdata('tbl_tahunakademik', 'kode', 'asc')->result();
		$data['page']        = 'select_tahunajar_nilai';
		$this->load->view('template/template', $data);
	}

	public function set_tahunajar()
	{
		// for login as BAA
		if ($this->input->post('prodi')) {
			$this->session->set_userdata('prodi_nilai',$this->input->post('prodi'));
		}

		$this->session->set_userdata('tahunajar_nilai', $this->input->post('tahunajaran'));
		if (in_array(10, $this->usergroup) || in_array(8, $this->usergroup) || in_array(19, $this->usergroup)) {
			redirect(base_url('form/nilai/lecturer_list'));
		}
		redirect(base_url('form/nilai/courses_list'));
	}

	public function lecturer_list()
	{
		$data['prodi']     = in_array(10, $this->usergroup) ? $this->session->userdata('prodi_nilai') : $this->userid;
		$data['tahunajar'] = $this->session->userdata('tahunajar_nilai');
		$data['lecturer']  = $this->nilai->get_non_general_courses($data['prodi'], $data['tahunajar']);
		$data['page']      = "lecturer_list_v";
		$this->load->view('template/template', $data);
	}

	public function courses_list($nid='')
	{
		$data['userid']    = empty($nid) ? $this->userid : $nid;
		$data['usergroup'] = $this->usergroup;
		$data['tahunajar'] = $this->session->userdata('tahunajar_nilai');
		$data['courses']   = $this->nilai->get_courses($data['userid'], $data['tahunajar']);
		$data['page']      = 'courses_list_v';
		$this->load->view('template/template', $data);
	}

	public function participants_ta($id_jadwal='')
	{
		$this->_is_id_available($id_jadwal);

		$jadwal            = $this->db->get_where('tbl_jadwal_matkul', ['id_jadwal' => $id_jadwal])->row();
		$nama_mk           = get_nama_mk($jadwal->kd_matakuliah, explode('/', $jadwal->kd_jadwal)[0]);
		$data['usergroup'] = $this->usergroup;
		$data['dosen']     = $jadwal->kd_dosen;
		$data['matakuliah']   = $jadwal->kd_matakuliah;
		$data['id_jadwal']    = $id_jadwal;
		$data['tahunajar']    = $this->session->userdata('tahunajar_nilai');
		$data['detail']       = 'Kelas ' . $jadwal->kelas . ' Matakuliah ' . $jadwal->kd_matakuliah . ' - ' . $nama_mk;
		$data['participants'] = $this->nilai->get_detail_participants_ta($jadwal->kd_jadwal)->result();
		$data['page']         = 'detail_kelas_ta_v';
		$this->load->view('template/template', $data);
	}
	public function participants_nilai_prodi($id_jadwal='')
	{
		$this->_is_id_available($id_jadwal);

		$jadwal                     = $this->db->get_where('tbl_jadwal_matkul', ['id_jadwal' => $id_jadwal])->row();
		$nama_mk                    = get_nama_mk($jadwal->kd_matakuliah, explode('/', $jadwal->kd_jadwal)[0]);
		$data['usergroup']          = $this->usergroup;
		$data['dosen']              = $jadwal->kd_dosen;
		$data['is_active_schedule'] = $this->_is_still_can_assesst($id_jadwal);
		$data['matakuliah']         = $jadwal->kd_matakuliah;
		$data['id_jadwal']          = $id_jadwal;
		$data['tahunajar']          = $this->session->userdata('tahunajar_nilai');
		$data['detail']             = 'Kelas ' . $jadwal->kelas . ' Matakuliah ' . $jadwal->kd_matakuliah . ' - ' . $nama_mk;
		$data['participants']       = $this->nilai->get_detail_participants_prodi($jadwal->kd_jadwal)->result();
		$data['page']               = 'detail_kelas_nilai_v';
		$this->load->view('template/template', $data);
	}

	public function total_participant($id_jadwal)
	{
		$kode_jadwal = get_kd_jdl($id_jadwal);
		$total       = $this->nilai->get_participant($kode_jadwal)->jumlah;
		echo $total;
	}

	public function participants_list($id_jadwal = '')
	{
		$this->_is_id_available($id_jadwal);

		$jadwal            = $this->db->get_where('tbl_jadwal_matkul', ['id_jadwal' => $id_jadwal])->row();
		$nama_mk           = get_nama_mk($jadwal->kd_matakuliah, explode('/', $jadwal->kd_jadwal)[0]);
		$data['usergroup'] = $this->usergroup;
		$data['dosen']     = $jadwal->kd_dosen;
		$data['is_active_schedule'] = $this->_is_still_can_assesst($id_jadwal);
		$data['matakuliah']   = $jadwal->kd_matakuliah;
		$data['id_jadwal']    = $id_jadwal;
		$data['tahunajar']    = $this->session->userdata('tahunajar_nilai');
		$data['detail']       = 'Kelas ' . $jadwal->kelas . ' Matakuliah ' . $jadwal->kd_matakuliah . ' - ' . $nama_mk;
		$data['participants'] = $this->nilai->get_detail_participants($jadwal->kd_jadwal)->result();
		$data['page']         = 'detail_kelas_v';
		$this->load->view('template/template', $data);
	}

	private function _is_active_schedule($id_jadwal)
	{
		$tahunajar = $this->db->get('tbl_jadwal_matkul', ['id_jadwal' => $id_jadwal])->row()->kd_tahunajaran;
		if ($this->session->userdata('tahunajar_nilai') == $tahunajar) {
			return TRUE;
		}
		return FALSE;
	}

	private function _is_id_available($id = '')
	{
		if (empty($id)) {
			echo '<script>alert("Data tidak ditemukan!");</script>';

			if (in_array(19, $this->usergroup)) {
				redirect(base_url('form/nilai/lecturer_list'),'refresh');
			}

			redirect(base_url('form/nilai/courses_list'), 'refresh');
		} else {
			$jadwal = $this->db->get_where('tbl_jadwal_matkul', ['id_jadwal' => $id])->num_rows();
			if ($jadwal == 0) {
				echo '<script>alert("Data tidak ditemukan!");</script>';

				if (in_array(19, $this->usergroup)) {
					redirect(base_url('form/nilai/lecturer_list'),'refresh');
				}

				redirect(base_url('form/nilai/courses_list'), 'refresh');
			}
			return;
		}
	}

	public function get_score($npm, $tipe, $tahunajar)
	{
		$nilai = $this->nilai->get_score($npm, $tipe, $tahunajar);
		echo $nilai;
	}

	public function store_ta($id_jadwal)
	{
		$this->_is_id_available($id_jadwal);
		$npm = $this->input->post('npm');

		// data storing will declined if there's no student
		$this->_if_no_students(count($npm), $id_jadwal);

		// data storing will decline if number of student is not equal with the stored data
		$this->_is_student_complete(count($npm), $id_jadwal);

		$kodeJadwal = get_kd_jdl($id_jadwal);
		// $absendsn   = $this->nilai->get_last_meeting($kodeJadwal);
		$matakuliah = $this->input->post('kode_matakuliah');

		// looping permahasiswa
		foreach ($npm as $key => $val) {
			$nim          = $this->input->post('npm[' . $val . ']');
			$nid          = $this->userid;
			$tahunajar    = $this->session->userdata('tahunajar_nilai');
			$transaksiKrs = $this->nilai->get_krs_transaction($nim . $tahunajar);
			$final        = $this->input->post('final[' . $nim . ']');

			if ($final !== '') {
				$data[] = [
					'npm_mahasiswa'      => $nim,
					'kd_krs'             => $transaksiKrs->kd_krs,
					'kd_matakuliah'      => $matakuliah,
					'tahun_ajaran'       => $tahunajar,
					'tipe'               => 10,
					'kd_prodi'           => $transaksiKrs->kd_jurusan,
					'nid'                => $nid,
					'audit_date'         => date('Y-m-d H:i:s'),
					'kd_transaksi_nilai' => $kodeJadwal . 'zzz' . $nim,
					'nilai'              => ($final === '') ? NULL : number_format($final, 2),
					'flag_publikasi'     => 1,
					'kd_jadwal'          => $kodeJadwal
				];	
			}
		}
		// loop end
		
		// array yang masuk ke tbl_nilai_detail_log
		$log = [
			'kd_jadwal'   => $kodeJadwal,
			'audit_by'    => $nid,
			'created_at'  => date('Y-m-d H:i:s'),
			'created_by'  => $this->userid
		];
		//end

		/* cek kd_jadwal di tbl_nilai_detail, 
		jika ada hapus dahulu sebelum insert */
		$cek_kdJadwal = $this->db->query("SELECT kd_jadwal FROM tbl_nilai_detail WHERE kd_jadwal = '$kodeJadwal'")->row();
		if ($cek_kdJadwal) {
			$this->app_model->deletedata('tbl_nilai_detail', 'kd_jadwal', $kodeJadwal);
		}
		//end

		$this->db->insert_batch('tbl_nilai_detail', $data);
		$this->app_model->insertdata('tbl_nilai_detail_log', $log);
		echo '<script>alert("Berhasi disimpan!");</script>';
		redirect(base_url('form/nilai/participants_ta/'.$id_jadwal), 'refresh');
	}

	public function store($id_jadwal)
	{
		$this->_is_id_available($id_jadwal);
		$npm = $this->input->post('npm');

		// data storing will declined if there's no student
		$this->_if_no_students(count($npm), $id_jadwal);

		// data storing will decline if number of student is not equal with the stored data
		$this->_is_student_complete(count($npm), $id_jadwal);

		$kodeJadwal = get_kd_jdl($id_jadwal);
		// $absendsn   = $this->nilai->get_last_meeting($kodeJadwal);
		$matakuliah = $this->input->post('kode_matakuliah');

		//looping permahasiswa
		foreach ($npm as $key => $val) {
			$nim          = $this->input->post('npm[' . $val . ']');
			$nid          = $this->userid;
			$tahunajar    = $this->session->userdata('tahunajar_nilai');
			$transaksiKrs = $this->nilai->get_krs_transaction($nim . $tahunajar);
			$absenmhs = $this->input->post('absen[' . $nim . ']');
			$task[0]  = $this->input->post('tugas1[' . $nim . ']');
			$task[1]  = $this->input->post('tugas2[' . $nim . ']');
			$task[2]  = $this->input->post('tugas3[' . $nim . ']');
			$task[3]  = $this->input->post('tugas4[' . $nim . ']');
			$task[4]  = $this->input->post('tugas5[' . $nim . ']');
			$uts      = $this->input->post('uts[' . $nim . ']');
			$_uas     = $this->_is_set_to_zero($this->input->post('uas[' . $nim . ']'));
			$uas      = $this->_uas_validation(4, $_uas, $kodeJadwal, $nim);
			$avg      = $this->_set_task_average($task);
			// dd($task,2);
			
			//nilai2 di buat jadi 0 jika null(agar tidak error saat number_format)
			$average  = $this->_is_set_to_zero($avg);
			$absen    = $this->_is_set_to_zero($absenmhs);
			$uts      = $this->_is_set_to_zero($uts);
			$uas      = $this->_is_set_to_zero($uas);
			$tugas[0] = ($task[0] === '') ? NULL : number_format($task[0], 2);
			$tugas[1] = ($task[1] === '') ? NULL : number_format($task[1], 2);
			$tugas[2] = ($task[2] === '') ? NULL : number_format($task[2], 2);
			$tugas[3] = ($task[3] === '') ? NULL : number_format($task[3], 2);
			$tugas[4] = ($task[4] === '') ? NULL : number_format($task[4], 2);
			//end

			// hitung nilai akhir
			$absen_hitung   = $absen * 0.1;
			$average_hitung = $average * 0.2;
			$uts_hitung     = $uts * 0.3;
			$uas_hitung     = $uas * 0.4;
			$nilai_akhir    = $absen_hitung + $average_hitung + $uts_hitung + $uas_hitung;
			//end

			// Hasil yang di input di field nilai 
			$nilai[0] = number_format($average, 2);
			$nilai[1] = number_format($absenmhs, 2);
			$nilai[2] = number_format($uts, 2);
			$nilai[3] = number_format($uas, 2);

			$nilai[4] = $tugas[0];
			$nilai[5] = $tugas[1];
			$nilai[6] = $tugas[2];
			$nilai[7] = $tugas[3];
			$nilai[8] = $tugas[4];
			$nilai[9] = number_format($nilai_akhir, 2);
			// end

			//array yang masuk ke tbl_nilai_detail
			for ($i = 0; $i < 10; $i++) {
				$data[] = [
					'npm_mahasiswa'      => $nim,
					'kd_krs'             => $transaksiKrs->kd_krs,
					'kd_matakuliah'      => $matakuliah,
					'tahun_ajaran'       => $tahunajar,
					'tipe'               => $i + 1,
					'kd_prodi'           => $transaksiKrs->kd_jurusan,
					'nid'                => $nid,
					'audit_date'         => date('Y-m-d H:i:s'),
					'kd_transaksi_nilai' => $kodeJadwal . 'zzz' . $nim,
					'nilai'              => $nilai[$i],
					'flag_publikasi'     => 1,
					'kd_jadwal'          => $kodeJadwal
				];
			}
		}
		// end
		
		// array yang masuk ke tbl_nilai_detail_log
		$log = [
			'kd_jadwal'   => $kodeJadwal,
			'audit_by'    => $nid,
			'created_at'  => date('Y-m-d H:i:s'),
			'created_by'  => $nid
		];
		//end

		/* cek kd_jadwal di tbl_nilai_detail, 
		jika ada hapus dahulu sebelum insert */
		$cek_kdJadwal = $this->db->query("SELECT kd_jadwal FROM tbl_nilai_detail 
										WHERE kd_jadwal = '{$kodeJadwal}'")->num_rows();
		if ($cek_kdJadwal > 0) {
			$this->app_model->deletedata('tbl_nilai_detail', 'kd_jadwal', $kodeJadwal);
		}
		//end
		$this->db->insert_batch('tbl_nilai_detail', $data);
		$this->app_model->insertdata('tbl_nilai_detail_log', $log);
		echo '<script>alert("Berhasi disimpan!");</script>';
		redirect(base_url('form/nilai/participants_list/'.$id_jadwal), 'refresh');
	}

	private function _if_no_students($total, $id_jadwal)
	{
		if ($total < 1) {
			$this->session->set_flashdata('fail_storing','Data tidak tersimpan! Pastikan jadwal memiliki peserta kelas!');
			redirect(base_url('form/nilai/participants_list/'.$id_jadwal),'refresh');
		}
		return;
	}

	private function _is_student_complete($total_stored, $id_jadwal)
	{
		$kode_jadwal = get_kd_jdl($id_jadwal);
		$number_of_students = $this->db->get_where('tbl_krs', ['kd_jadwal' => $kode_jadwal])->num_rows();

		if ($total_stored != $number_of_students) {
			$this->session->set_flashdata('fail_storing','Data tidak tersimpan! Pastikan kolom "SEARCH" kosong ketika anda menyimpan nilai! Baca petunjuk');
			redirect(base_url('form/nilai/participants_list/'.$id_jadwal),'refresh');
		}
		return;
	}

	private function _is_set_to_zero($score)
	{
		if (is_null($score) || empty($score)) {
			return 0;
		}
		return $score;
	}

	private function _set_task_average($task)
	{
		$nilai_plus = 0;
		$jml = 0;
		for ($i = 0; $i < 5; $i++) {
			if (!empty($task[$i]) || $task[$i] != '') {
				$nilai_plus = $nilai_plus + $task[$i];
				$jml = $jml + 1;
			}
		}
		$average = ($jml > 0) ? $nilai_plus / $jml : 0;
		return $average;
	}

	private function _is_still_can_assesst($id_jadwal)
	{
		$param_uts = ['id_jadwal' => $id_jadwal, 'tipe_uji' => 1];
		$uts = $this->db->get_where('tbl_jadwaluji', $param_uts);

		$param_uas = ['id_jadwal' => $id_jadwal, 'tipe_uji' => 2];
		$uas = $this->db->get_where('tbl_jadwaluji', $param_uas);

		$now = date('Y-m-d');
		$actyear = getactyear();
		$uts_exist = $uts->num_rows() > 0 ? TRUE : FALSE;
		$uas_exist = $uas->num_rows() > 0 ? TRUE : FALSE;
		$uts_start = !is_null($uts->row()) ? $uts->row()->start_date : NULL;
		$uts_end   = !is_null($uts->row()) ? $uts->row()->end_date : NULL;
		$uas_start = !is_null($uas->row()) ? $uas->row()->start_date : NULL;;
		$uas_end   = !is_null($uas->row()) ? $uas->row()->end_date : NULL;;

		// if uts exist and uts date still valid
		if ($uts_exist && ($uts_end >= $now && $uts_start <= $now)) {
			return TRUE;

		// if uas exist and uas date still valid
		} elseif ($uas_exist && ($uas_end >= $now && $uas_start <= $now)) {
			return TRUE;

		// if uts exist and uas unavailable
		} elseif (!$uts_exist && !$uas_exist) {
			return FALSE;

		// if uts and uas was exist but out of date
		} elseif (($uts_exist && $now > $uts_end) || ($uas_exist && $now > $uas_end)) {
			// whether lecturer get dispensation to scoring?
			$lecturer_dispensation 	= $this->db->query("SELECT * FROM tbl_exception 
														WHERE nid = '$this->userid' 
														AND tahunakademik = '$actyear' 
														AND is_active = '1'")->num_rows();
			if ($lecturer_dispensation > 0) {
				return TRUE;
			}
			return FALSE;
		}

		return FALSE;
	}

	function _uas_validation($testType, $dataPoin, $kodeJadwal, $npm)
	{
		// if dispensasi
		if ($testType == 'all') {
			return number_format($dataPoin, 2);
		} else {
			// prevent filling abnormal values (for UAS)
			$isAbsenFull = $this->app_model->bisa_ujiankah($kodeJadwal,$npm);

			if ($isAbsenFull >= 75) {
				return number_format($dataPoin, 2);
			} else {
				return 0;
			}
		}
	}
}

/* End of file Nilai.php */
/* Location: ./application/modules/form/controllers/Nilai.php */
