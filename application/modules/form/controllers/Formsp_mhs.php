<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Formsp_mhs extends MY_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->model('setting_model');
		// if ($this->session->userdata('sess_login') == TRUE) {
		//   $akses = $this->role_model->cekakses(80)->result();
		//   $aktif = $this->setting_model->getaktivasi('sp')->result();
		//   if (count($aktif) != 0) {
		// 		if ($akses != TRUE) {
		// 			echo "<script>alert('Akses Tidak Diizinkan');document.location.href='".base_url()."home';</script>";
		// 			//echo "gabisa";
		// 		}
		// 	} else {
		// 		echo "<script>alert('Akses Tidak Diizinkan');document.location.href='".base_url()."home';</script>";
		// 		//echo "gabisa";
		// 	}
		// } else {
		//   redirect('auth','refresh');
		// }

	}

	function index()
	{
		$user = $this->session->userdata('userid');
		$data['rows'] = $this->db->where('npm_mahasiswa',$user['userid'])->get('tbl_verifikasi_krs_sp')->row();

		$data['page'] = 'form/mk_sp_mhs';
    	$this->load->view('template/template', $data);    
	}
  
  	function cek_prasyarat(){
		$sym  = array('[', ']');
		$prasyarat = explode(',', str_replace($sym,'',$_POST['prasyarat']));
		  
	    $data = $this->app_model->get_prasyarat($prasyarat)->num_rows();               
	    echo $data;
  	}

  	function generateRandomString() {
	    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
	    $charactersLength = strlen($characters);
	    $randomString = '';
	    for ($i = 0; $i < 5; $i++) {
	        $randomString .= $characters[rand(0, $charactersLength - 1)];
	    }
	    return $randomString;
	}

	

	function jadwal_kuliah($npm_mahasiswa,$kd_krs){
		//die($semester_krs.'-'.$npm_mahasiswa.'-'.$kd_krs);

		//$data['semester_krs']  = $semester_krs;
		$data['npm_mahasiswa'] = $npm_mahasiswa;
		$data['kd_krs']		= $kd_krs;

		$data['pembimbing'] = $this->app_model->get_pembimbing_krs_sp($kd_krs)->row_array();
		$data['detail_krs'] = $this->app_model->get_detail_print_krs_mahasiswa_sp($kd_krs);

		//var_dump($data['detail_krs']);die();

		$data['page'] = 'form/viewkrs_sp_detail';
		$this->load->view('template/template',$data);   
	}

	function kosongkan_jadwal($id,$id2){
		//die('test'.$id.'');
		//$logged = $this->session->userdata('sess_login');
		//$idkd = $this->app_model->getdetailslug(substr($logged['userid'], 0,12),$id)->row();
		$data = array('kd_jadwal' => null );
		$npm_mahasiswa = substr($id, 0,12);


		$this->db->where('kd_krs', $id);
		$this->db->where('kd_matakuliah', $id2);
		$this->db->update('tbl_krs', $data);

		$q = $this->db->select('kd_krs')
						->from('tbl_krs_sp')
						->where('id_krs',$id)
						->get()->row();

		$this->jadwal_kuliah($npm_mahasiswa,$id);

	}

	function print_sp($kode){
		$this->load->library('Cfpdf');
		$ta = $this->db->get('tbl_tahunakademik', 1)->row();

		$data['tahun'] = substr($ta->tahun_akademik, 0,9);
		$data['smtr'] = $this->app_model->getdetail('tbl_mahasiswa','NIMHSMSMHS',substr($kode, 0,12),'NIMHSMSMHS','asc')->row();

		$npm = substr($kode, 0,12);
		
		$this->db->select('a.NIMHSMSMHS,a.NMMHSMSMHS,a.TAHUNMSMHS,b.prodi');
		$this->db->from('tbl_mahasiswa a');
		$this->db->join('tbl_jurusan_prodi b', 'a.KDPSTMSMHS = b.kd_prodi');
		//$this->db->join('tbl_fakultas c', 'c.kd_fakultas = b.kd_fakultas');
		$this->db->where('NIMHSMSMHS', $npm);
		$data['info_mhs'] = $this->db->get()->row();

		$qq=$this->app_model->get_KDPSTMSMHS($npm)->row();

		$data['matkul'] = $this->db->query('select distinct * from tbl_krs_sp b join tbl_verifikasi_krs_sp a on a.kd_krs = b.kd_krs
		join tbl_matakuliah c on b.kd_matakuliah = c.kd_matakuliah
		where b.kd_krs = "'.$kode.'" AND a.status_verifikasi = 2 AND b.flag_open = 1 AND c.kd_prodi = "'.$qq->KDPSTMSMHS.'"')->result();

		//var_dump($data['matkul']);die();

		$this->load->view('akademik/espe', $data);


	}

}

/* End of file Formsp.php */
/* Location: ./application/modules/form/controllers/Formsp.php */