<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Formpmb extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		error_reporting(0);
		if ($this->session->userdata('sess_login') == TRUE) {
			$cekakses = $this->role_model->cekakses(84)->result();
			if ($cekakses != TRUE) {
				echo "<script>alert('Anda Tidak Berhak Mengakses !!');history.go(-1);</script>";
			}
		} else {
			redirect('auth','refresh');
		}
		date_default_timezone_set('Asia/Jakarta'); 
	}

	public function index()
	{
		$data['opsi'] = $this->db->query('SELECT * from tbl_fakultas')->result();
		$data['IDReg'] = $this->db->query('SELECT ID_registrasi from tbl_calon_mhs order by ID_registrasi desc LIMIT 1')->row();
		$data['gel'] = $this->db->query('SELECT * from tbl_gel_pmb WHERE mulai_gel <= "'.date('Y-m-d').'" AND akhir_gel >= "'.date('Y-m-d').'" ')->row();
		//var_dump($data['gel']);exit();
		$data['page'] = 'form_pmb_staff';
		$this->load->view('template/template', $data);
	}

	function add_bio()
	{
		$data = array(
				'ID_registrasi'	=> $this->input->post('ID'),
				'nama'			=> $this->input->post('nama'),
				'asal_skl'		=> $this->input->post('asal_sch'),
				'tlp'			=> '+62'.$this->input->post('tlp'),
				'email'			=> $this->input->post('email'),
				'alamat'		=> $this->input->post('alamat'),
				'kd_prodi'		=> $this->input->post('choose'),
				'status'		=> 0
			);
		$this->app_model->insertdata('tbl_calon_mhs', $data);
		echo "<script>alert('Sukses');
		document.location.href='".base_url()."form/formpmb/';</script>";
	}

}

/* End of file Formpmb.php */
/* Location: ./application/modules/form/controllers/Formpmb.php */