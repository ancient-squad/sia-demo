<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class APIM extends MY_Controller {

    public function __construct()
	{
		parent::__construct();
        $this->load->library('session');
        error_reporting(0);
	}

    public function appauth()
    {
        $payload = file_get_contents('php://input');
		$decodePayload = json_decode($payload);
        
		$check 	= $this->login_model->cekuser($decodePayload->username, $decodePayload->username)->result();

		if (count($check) > 0) {
			// prepare user response
			$this->_prepare_response($check);
		} else {
			$response = ['status' => 60, 'message' => 'user not found'];
			$this->output
				->set_status_header(404)
				->set_content_type('application/json', 'utf-8')
				->set_output(json_encode($response))
				->_display();
			exit();
		}
    }

    public function applogout()
    {
        $this->db->query("DELETE FROM tbl_tokenizer WHERE userid = '0010036306'");
        $response = ['status' => 201, 'message' => 'logout success'];
			$this->output
				->set_status_header(200)
				->set_content_type('application/json', 'utf-8')
				->set_output(json_encode($response))
				->_display();
			exit();
    }

    private function _prepare_response($user)
	{
		foreach ($user as $row) {
			$user = $row->username;
			$pass = $row->password_plain;
			$type = $row->user_type;
		}

		$user_log = $this->login_model->datauser($user, $pass, $type)->result();

		foreach ($user_log as $key) {
            $session['token']           = $this->_create_token($key->userid);
			$session['id']		 		= $key->id_user;
			$session['userid']		 	= $key->userid;
			$session['group_id']		= $key->id_user_group;
			$session['group_name']		= get_group_user($key->id_user_group);
			$session['is_active']		= $key->status;

			$response = ['status' => 201, 'message' => 'login success', 'data' => $session];
			$this->output
				->set_status_header(200)
				->set_content_type('application/json', 'utf-8')
				->set_output(json_encode($response))
				->_display();
			exit();
		}
	}

    private function _create_token($user)
    {
        $tokenizer = hash('ripemd160', $user.date('Ymdhis'));
        $this->db->query("DELETE FROM tbl_tokenizer WHERE userid = '".$user."'");
        $this->db->query("INSERT INTO tbl_tokenizer (userid,token) VALUES ('".$user."','".$tokenizer."')");
        $this->session->set_userdata('nim', $user);
        return $tokenizer;
    }

    private function _check_access_token($token)
    {
        $cek = $this->db->query("SELECT * FROM tbl_tokenizer WHERE userid = '".$this->session->userdata('nim')."' AND token = '".$token."'")->result();
        if (count($cek) > 0) {
            return true;
        }
        $response = ['status' => 401, 'message' => 'invalid access'];
            $this->output
                ->set_status_header(200)
                ->set_content_type('application/json', 'utf-8')
                ->set_output(json_encode($response))
                ->_display();
            exit();
    }

    private function _create_response($status_code, $response)
	{
		$this->output
			->set_status_header($status_code)
			->set_content_type('application/json', 'utf-8')
			->set_output(json_encode($response))
			->_display();
		exit();
	}

    private function _gettranscript($nim)
    {
        $data['hitung_ips'] = $this->db->query("SELECT nl.`KDKMKTRLNM`,
                                                (SELECT DISTINCT nama_matakuliah FROM tbl_matakuliah 
                                                WHERE kd_matakuliah = nl.`KDKMKTRLNM` 
                                                AND kd_prodi = nl.`KDPSTTRLNM` 
                                                ORDER BY id_matakuliah DESC LIMIT 1)  AS nama_matakuliah,
                                                (SELECT DISTINCT sks_matakuliah FROM tbl_matakuliah 
                                                WHERE kd_matakuliah = nl.`KDKMKTRLNM` 
                                                AND kd_prodi = nl.`KDPSTTRLNM` 
                                                ORDER BY id_matakuliah DESC LIMIT 1)  AS sks_matakuliah,

                                            MIN(nl.`NLAKHTRLNM`) AS NLAKHTRLNM,
                                            MAX(nl.`THSMSTRLNM`) AS THSMSTRLNM,
                                            MAX(nl.`BOBOTTRLNM`) AS BOBOTTRLNM 
                                        FROM tbl_transaksi_nilai nl 
                                        WHERE nl.`NIMHSTRLNM` = '".$nim."' 
                                        AND NLAKHTRLNM != 'T'
                                        GROUP BY nl.`KDKMKTRLNM` ORDER BY THSMSTRLNM ASC")->result();

        $st = 0;
        $ht = 0;
        foreach ($data['hitung_ips'] as $iso) {
            $h = 0;
            $h = $iso->BOBOTTRLNM * $iso->sks_matakuliah;
            $ht=  $ht + $h;
            $st=$st+$iso->sks_matakuliah;
        }

        $data['ipk'] = $ht/$st;
        return $data;

    }

    private function _gettranscriptips($nim,$ta)
    {
        $data['hitung_ips'] = $this->db->query("SELECT nl.`KDKMKTRLNM`,
                                                (SELECT DISTINCT nama_matakuliah FROM tbl_matakuliah 
                                                WHERE kd_matakuliah = nl.`KDKMKTRLNM` 
                                                AND kd_prodi = nl.`KDPSTTRLNM` 
                                                ORDER BY id_matakuliah DESC LIMIT 1)  AS nama_matakuliah,
                                                (SELECT DISTINCT sks_matakuliah FROM tbl_matakuliah 
                                                WHERE kd_matakuliah = nl.`KDKMKTRLNM` 
                                                AND kd_prodi = nl.`KDPSTTRLNM` 
                                                ORDER BY id_matakuliah DESC LIMIT 1)  AS sks_matakuliah,

                                            MIN(nl.`NLAKHTRLNM`) AS NLAKHTRLNM,
                                            MAX(nl.`THSMSTRLNM`) AS THSMSTRLNM,
                                            MAX(nl.`BOBOTTRLNM`) AS BOBOTTRLNM 
                                        FROM tbl_transaksi_nilai nl 
                                        WHERE nl.`NIMHSTRLNM` = '".$nim."' 
                                        AND THSMSTRLNM = '".$ta."'
                                        AND NLAKHTRLNM != 'T'
                                        GROUP BY nl.`KDKMKTRLNM` ORDER BY THSMSTRLNM ASC")->result();

        $st = 0;
        $ht = 0;
        foreach ($data['hitung_ips'] as $iso) {
            $h = 0;
            $h = $iso->BOBOTTRLNM * $iso->sks_matakuliah;
            $ht=  $ht + $h;
            $st=$st+$iso->sks_matakuliah;
        }
        $data['totalsks'] = $st;
        $data['ipk'] = $ht/$st;
        return $data;

    }

    public function profile()
    {
        $payload = file_get_contents('php://input');
		$decodePayload = json_decode($payload);
        $getval = $this->_check_access_token($decodePayload->token);
        if ($getval) {
            $mhs = $this->app_model->getdetail('tbl_mahasiswa','NIMHSMSMHS',$this->session->userdata('nim'),'NIMHSMSMHS','asc')->row();
            $getipk = $this->_gettranscript($this->session->userdata('nim'));
            $session['ipk'] = number_format($getipk['ipk'],2);
            $totalsks = $tskss+$st;
            $session['semester'] = $this->app_model->get_semester_khs($mhs->SMAWLMSMHS, getactyear());
            $session['nama_mhs']           = $mhs->NMMHSMSMHS;
			$session['nim']		 		= $mhs->NIMHSMSMHS;
			$session['prodi']		 	= $mhs->KDPSTMSMHS;
			$session['fakultas']		= 'PENDIDIKAN';
            $response = ['status' => 400, 'message' => 'success','data' => $session];
            $this->output
                ->set_status_header(200)
                ->set_content_type('application/json', 'utf-8')
                ->set_output(json_encode($response))
                ->_display();
            exit();
        }
    }

    // private function _getschedulesms($sms)
    // {
    //     $this->app_model->jdl_matkul_by_semester($sms);
    // }

    public function schedule()
    {
        $payload = file_get_contents('php://input');
		$decodePayload = json_decode($payload);
        $getval = $this->_check_access_token($decodePayload->token);
        if ($getval) {
            $schedule = $this->app_model->jdl_matkul_by_semester($decodePayload->semester);
            $response = ['status' => 400, 'message' => 'success','data' => $schedule];
            $this->output
                ->set_status_header(200)
                ->set_content_type('application/json', 'utf-8')
                ->set_output(json_encode($response))
                ->_display();
            exit();
        }
    }

    private function _getnews()
    {
        $berita = array();
        $berita[1]['img'] = 'http://stkipbima.ac.id/wp-content/uploads/2021/05/WhatsApp-Image-2021-05-05-at-20.01.05-768x576.jpeg';
        $berita[1]['judul'] = 'STKIP Bima Mendapat Pujian Dari Dinas Perpustakaan Kab. Bima Dalam Gerakan Literasi Desa Tonggorisa';
        $berita[1]['isi'] = '<p>Kuliah Kerja Nyata (KKN-T) Mahasiswa STKIP Bima Launching Rumah Baca dan Penyerahan hadiah Festival Literasi dengan tema &ldquo;Tranformasi Nilai Islam dalam mencetak Generasi yang memiliki Ahlakul Qorimah Di Bulan Suci Ramadan&rdquo;, Rabu (03/05/2021)</p>

        <p>Mewakili Dinas Perpustakaan dan Arsip Kab. Bima menyampaikan &ldquo;Kami ucapkan terimah kasih kepada mahasiswa, karena sudah membantu serta mendukung program dari pemerintah daerah adalah pengadaan Rumah baca, masa depan itu adalah milik mereka yang rajin belajar dan membaca. Terima kasih kepada kades tonggorisa yang selalu antusias dalam mendukung kegiatan mahasiswa KKN-T STKIP Bima, harapan saya bahwa desa tonggorisa sebagai rolle model gerakan Literasi di Kab. Bima, program mendirikan rumah baca tidak mudah dilakukan, karena program tersebut merupakan prestasi mahasiswa dan dosen pembimbingnya, karena beliau juga merupakan tokoh literasi kabupaten dan kota bima (Ungkap Basirun, M.Pd).</p>
        
        <p>Proses peresmian dan launching rumah baca dipimpin langsung oleh Dinas Perpustakaan dan Arsip Kabupaten Bima didamping oleh kepala desa, Dosen pembimbing, Ketua SGI Kab. Bima, Ketua BPD Desa Tonggorisa dan anak-anak.</p>
        
        <p>&ldquo;Terima kasih kepada pemerintah desa telah mendukung program KKN Tematik STKIP Bima dan alhamdulillah dipuncak kegiatan ini adalah Launching Rumah Baca desa tonggorisa, sebagai bentuk keseriusan dalam gerakan literasi di kabupaten bima, kami berharap kepala desa bisa melanjutkan program yang telah kami torehkan selama 2 bulan berjalan&rdquo; (Ungkap Amiruddin, S.Pd.,M.Pd Selaku dosen Pembimbing Desa Tonggorisa).</p>
        
        <p>Ketua SGI Kab. Bima dalam sambutannya &ldquo;Saya sangat apreasi kepada adik-adik mahasiswa dalam memunculkan program yang baru, sesuai dengan instruksi perbup No 11 tahun 2019 tentang gerakan literasi, kehadiran literasi ini yaitu budaya membaca, menulis akan memberikan nilai tambah bagi desa tonggorisa, sehingga anak-anak bisa rajin membaca dan menulis di masyarakat dan perlu ada kolaborasi dari semua pihak, agar membaca, menulis ini menjadi pilar kesuksesan anak&rdquo; (Ungkap Eka Ilham, M.Si).</p>
        
        <p>Selanjutnya pemerintah desa menyampaikan dalam sambutannya &ldquo;Saya akan segera membentuk pengurusnya rumah baca dan akan segera menganggarkan ADD, keberlansungan rumah baca di desa kami dan mohon kepala dinas mensuport terus kam, (Ungkap Muhamad Ahyar).</p>
        
        <p>Ketua Posko KKN Tematik Desa Tonggorisa menyampaikan &ldquo;Ucapan terima kasih banyak kepada pemerintah desa yang selalu bergandengan tangan serta mendukung program mahasiswa KKN-T sehingga sampai hari ini kita masi mampu melaksanakan program yang sudah kita ranvang secara bersama, (Ujar Bunyamin)</p>';
        
        $berita[2]['img'] = 'http://stkipbima.ac.id/wp-content/uploads/2021/04/photobk1-1-768x429.jpg';
        $berita[2]['judul'] = 'Keluarga Besar Prodi Bimbingan dan Konseling STKIP Bima dan Mitra menyalurkan bantuan untuk korban Banjir Bima Tahap Kedua';
        $berita[2]['isi'] = '<p>Penyerahan bantuan Korban banjir di Desa Rabakodo Oleh Ketua Prodi BK STKIP Bima (16 April 2021)</p>

        <p> 28 April 2021. Unsur Dosen, Mahasiswa dan Alumni Program Studi Bimbingan dan Konseling menyalurkan bantuan tahap kedua untuk korban banjir bandang Kabupaten Bima sebanyak 180 paket bantuan yang terdiri dari gula pasir, minyak goreng, kurma dan sarung sholat.</p>
        
        <p>Penyaluran perdana tahap kedua ini dilaksanakan pada Bulan Ramadhan yaitu dimulai pada Hari Jumat, 16 April 2021. Civitas akademika Prodi Bimbingan dan Konseling menyerahkan bantuan kepada keluarga yang tertimpa musibah banjir di Kecamatann Woha yaitu di Desa Rabakodo, Tente, Baralau dan Tangga Kecamatan Monta.</p>
        
        <p>Penyerahan lanjutan bantuan korban banjir oleh Dosen, mahasiswa dan Alumni dilakukan Ahad, 25 April 2021. mulai dari Kecamatan Bolo yaitu Desa Bontokape, Desa Leu dan masuk di Desa Dena Kecamatan Madapangga.</p>
        
        <p>Di Desa Leu sendiri penyaluran bantuan didampingi oleh Anggota Komunitas Oi Sanahi yang keanggotaanya terdapat beberapa Alumni Program Studi Bimbingan dan Konseling. Bersama team BK STKIP Bima peduli menyerahkan sarung untuk keluarga korban yang rumahnya sudah terbawa arus banjir. Sasaran bantuan diprioritaskan pada keluarga yang terdampak banjir yang menurut data pemerintah adalah yang paling parah atau yang rumahnya terbawa arus banjir.</p>
        
        <p>Abdurahman, S.Pd. salah satu alumni Prodi BK yang mendampingi penyaluran sumbangan menyampaikan bahwa kegiatan penyaluran ini berjalan dengan lancar dan berharap dibulan Suci Ramadhan ini, Kami Do,akan semoga amal ibadah dan upaya kemanusian yang dilakukan akan dibalas oleh Allah dengan Kebaikan serta dapat mengetuk Pintu Syurga.</p>
        
        <p>Terimakasih juga karena telah mempercayakan kami, menitip amanat untuk di Salurkan</p>
        
        <p>Sarbudin, M.Pd. selaku Ketua Program Studi Bimbingan dan Konseling STKIP Bima yang langsung memimpin kegiatan menjelaskan bahwa sumbangan ini berasal dari Dosen dan Mahasiswa dan dukungan dari Keluarga Besar Kecamatan Langgudu Kab. Bima yang berada di Pangkalan Bun Kalimantan Tengah.</p>
        
        <p>Ketua Program Studi Bimbingan dan Konseling STKIP Bima ini menambahkan bahwa Keluarga besar dari Pangkalan Bun Kalimantan Tengah ini merupakan mitra dalam kegiatan Prodi BK Mengabdi di Desa Kangga Bulan lalu dan menjadi Donatur dalam kegiatan baksos Masjid di Desa Kangga.</p>
        
        <p>Atas nama Keluarga Besar Prodi Bimbingan dan Konseling STKIP Bima saya menyampaikan ucapan terima kasih kepada keluarga di Kalimantan Tengah atas kepercayaan dan kerja sama selama ini, semoga akan terus terjalin. Begitu juga kepada Dosen, Alumni dan mahasiswa Prodi BK yang selalu berkontribusi dalam setiap kegiatan. Semoga Allah SWT selalu meridhoi dan membalas kebaikan ini dengan pahala yang berlimpah.</p>
        
        <p></p>
        
        <p>Penyerahan bantuan banjir di Desa Leu Kecamatan Bolo kabupaten Bima oleh Ketua Prodi Bimbingan dan Konseling STKIP Bima. (25 April 2021)</p>';
        return $berita;
    }

    public function transcript()
    {
        $payload = file_get_contents('php://input');
		$decodePayload = json_decode($payload);
        $getval = $this->_check_access_token($decodePayload->token);
        if ($getval) {
            $mhs = $this->app_model->getdetail('tbl_mahasiswa','NIMHSMSMHS',$this->session->userdata('nim'),'NIMHSMSMHS','asc')->row();
            $getipk = $this->_gettranscript($this->session->userdata('nim'));
            $session['listdata'] = $getipk['hitung_ips'];
            $session['ipk'] = number_format($getipk['ipk'],2);
            $session['nama_mhs']           = $mhs->NMMHSMSMHS;
			$session['nim']		 		= $mhs->NIMHSMSMHS;
			$session['prodi']		 	= $mhs->KDPSTMSMHS;
			$session['fakultas']		= 'PENDIDIKAN';
            $response = ['status' => 400, 'message' => 'success','data' => $session];
            $this->output
                ->set_status_header(200)
                ->set_content_type('application/json', 'utf-8')
                ->set_output(json_encode($response))
                ->_display();
            exit();
        }
    }

    private function _getstatusmhs($nim,$smawl, $ta)
    {
        $getsemester = $this->app_model->get_semester_khs($smawl, $smawl);
        $current = $this->app_model->get_semester_khs($smawl, $ta);
        $sms = $smawl;
        //while($getsemester <= $current){
        for($i = $getsemester;$i <= $current;$i++){
            $statusms = $this->app_model->get_semester_khs($smawl, $sms);
            $data[$sms]['semester'] = $statusms;
            $getaktip = $this->db->query("SELECT COUNT(*) AS total FROM tbl_transaksi_nilai WHERE nimhstrlnm = '".$nim."' AND thsmstrlnm = '".$sms."'")->row()->total;
            if ($getaktip > 0) {
                $data[$sms]['status'] = 'AKTIF';
            } else {
                $data[$sms]['status'] = 'NON AKTIF';
            }
            $getips = $this->_gettranscriptips($nim,$sms);
            $data[$sms]['totalsks'] = $getips['totalsks'];
            $data[$sms]['ips'] = number_format($getips['ipk'],2);
            $plus = $smawl+1;
            if (substr($plus,3,1) == 3) {
                $sms = substr($plus,0,4).'1';
            } else {
                $sms = $plus;
            }
            
        }
        return $data;
    }

    public function status()
    {
        $payload = file_get_contents('php://input');
		$decodePayload = json_decode($payload);
        $getval = $this->_check_access_token($decodePayload->token);
        if ($getval) {
            $mhs = $this->app_model->getdetail('tbl_mahasiswa','NIMHSMSMHS',$this->session->userdata('nim'),'NIMHSMSMHS','asc')->row();
            $session = $this->_getstatusmhs($mhs->NIMHSMSMHS,$mhs->SMAWLMSMHS,getactyear());
            $response = ['status' => 400, 'message' => 'success','data' => $session];
            $this->output
                ->set_status_header(200)
                ->set_content_type('application/json', 'utf-8')
                ->set_output(json_encode($response))
                ->_display();
            exit();
        }
    }

    public function info()
    {
        $response = ['status' => 400, 'message' => 'success','data' => $this->_getnews()];
        $this->output
            ->set_status_header(200)
            ->set_content_type('application/json', 'utf-8')
            ->set_output(json_encode($response))
            ->_display();
        exit(); 
    }
}
