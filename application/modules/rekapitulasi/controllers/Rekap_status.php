<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Rekap_status extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		if ($this->session->userdata('sess_login') == TRUE) {
			$akses = $this->role_model->cekakses(37)->result();
		} else {
			redirect('auth','refresh');
		}
	}

	public function index()
	{
		$data['fakultas'] =$this->app_model->getdata('tbl_fakultas', 'id_fakultas', 'ASC')->result();
        $data['tahunajar']=$this->app_model->getdata('tbl_tahunakademik', 'kode', 'ASC')->result();
		$data['page'] = 'rekap_view';
		$this->load->view('template/template', $data);
	}

	function get_jurusan($id){

		$jrs = explode('-',$id);

        $jurusan = $this->app_model->getdetail('tbl_jurusan_prodi', 'kd_fakultas', $jrs[0], 'id_prodi', 'ASC')->result();

		$out = "<select class='form-control' name='jurusan' id='jurs'><option>--Pilih Jurusan--</option>";

        foreach ($jurusan as $row) {

            $out .= "<option value='".$row->kd_prodi."'>".$row->prodi. "</option>";

        }

        $out .= "</select>";

        echo $out;

	}

	function simpan_sesi()
	{
		$fakultas 	= $this->input->post('fakultas');
		$jurusan 	= $this->input->post('jur');
		$tahun 		= $this->input->post('thnajar');
		//var_dump($fakultas,$jurusan,$tahun);die();
		$opsi 		= $this->input->post('import');

		$this->session->set_userdata('faks', $fakultas);
		$this->session->set_userdata('jurs', $jurusan);
		$this->session->set_userdata('tahun', $tahun);
		$this->session->set_userdata('import', $opsi);

		redirect(base_url('rekapitulasi/rekap_status/rekap'));
	}

	function rekap()
	{
		$this->db->select('*');
		$this->db->from('tbl_mahasiswa a');
		$this->db->join('tbl_jurusan_prodi b', 'a.KDPSTMSMHS = b.kd_prodi');
		$this->db->join('tbl_fakultas c', 'c.kd_fakultas = b.kd_fakultas');
		$this->db->join('tbl_verifikasi_krs d', 'd.npm_mahasiswa = a.NIMHSMSMHS');
		$this->db->join('tbl_tahunakademik e', 'e.kode = d.tahunajaran');
		$this->db->where('c.kd_fakultas', $this->session->userdata('faks'));
		$this->db->where('b.kd_prodi', $this->session->userdata('jurs'));
		$this->db->where('e.kode', $this->session->userdata('tahun'));
		$data['q'] = $this->db->get()->result();
		if ($this->session->userdata('import') == 'exc') {
			$this->load->view('print_status', $data);
		} else {
			$this->load->view('print_status_csv', $data);
		}
	}

}

/* End of file Rekap_status.php */
/* Location: ./application/controllers/Rekap_status.php */