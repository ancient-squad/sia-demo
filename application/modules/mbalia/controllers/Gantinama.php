<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Gantinama extends MY_Controller {

	public function index()
	{
		$data['page'] = 'v_nama';
		$this->load->view('template/template', $data);
	}

	function load()
	{
		$gen = $this->input->post('jenis');
		$ih = $this->input->post('aww');
		$this->session->set_userdata('idn', $ih);
		if ($gen == 'mhs') {
			$data['kueh'] = $this->db->query('SELECT * from tbl_mahasiswa where NIMHSMSMHS = "'.$this->session->userdata('idn').'"')->row();
			if ($data['kueh'] == FALSE) {
				echo "<script>alert('Akun Tidak Ditemukan! Periksa User ID Pengguna!');document.location.href='".base_url()."adminpuskom/gantinama';</script>";
			} else {
				$data['page'] = 'v_nama_mhs';
				$this->load->view('template/template', $data);
			}

		} else {
			$data['kry'] = $this->db->query('SELECT * from tbl_karyawan where nid = "'.$this->session->userdata('idn').'"')->row();
			if ($data['kry'] == FALSE) {
				echo "<script>alert('Akun Tidak Ditemukan! Periksa User ID Pengguna!');document.location.href='".base_url()."adminpuskom/gantinama';</script>";
			} else {
				$data['page'] = 'v_nama_kry';
				$this->load->view('template/template', $data);
			}
			
		}
		
	}

	function ganti_mhs()
	{
		$this->db->query('UPDATE tbl_mahasiswa set NMMHSMSMHS = "'.$this->input->post('xxx').'" where NIMHSMSMHS = "'.$this->session->userdata('idn').'"');
		echo "<script>alert('Rubah Nama Berhasil !');document.location.href='".base_url()."adminpuskom/gantinama';</script>";
	}

	function ganti_kry()
	{
		$this->db->query('UPDATE tbl_karyawan set nama = "'.$this->input->post('www').'" where nid = "'.$this->session->userdata('idn').'"');
		echo "<script>alert('Rubah Nama Berhasil !');document.location.href='".base_url()."adminpuskom/gantinama';</script>";
	}

}

/* End of file Gantinama.php */
/* Location: ./application/modules/mbalia/controllers/Gantinama.php */