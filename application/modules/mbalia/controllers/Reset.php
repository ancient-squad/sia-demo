<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Reset extends MY_Controller {

	public function index()
	{
		$data['page'] = 'v_reset';
		$this->load->view('template/template', $data);
	}

	function ganti()
	{
		$a = $this->input->post('old');
		$e = $this->db->query('SELECT * from tbl_user_login where userid = "'.$a.'"')->row();
		if ($e == FALSE) {
			echo "<script>alert('Akun Tidak Ditemukan! Periksa User ID Pengguna!');document.location.href='".base_url()."adminpuskom/reset';</script>";
		} else {
			$kiw = sha1(md5('STKIPB1M4').key);
			$this->db->query('UPDATE tbl_user_login set password = "'.$kiw.'", password_plain = "STKIPB1M4" where userid = "'.$a.'"');
			echo "<script>alert('Reset Password Berhasil !');document.location.href='".base_url()."adminpuskom/reset';</script>";
		}

	}

}

/* End of file Reset.php */
/* Location: ./application/modules/adminpuskom/controllers/Reset.php */