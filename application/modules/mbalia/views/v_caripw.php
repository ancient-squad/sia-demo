<script type="text/javascript">

// function checkPass()
// {
//     var pass1 = document.getElementById('pass1');
//     var pass2 = document.getElementById('pass2');
//     var message = document.getElementById('confirmMessage');
//     //Set the colors we will be using ...
//     var goodColor = "#66cc66";
//     var badColor = "#ff6666";
//     if(pass1.value == pass2.value){
//         pass2.style.backgroundColor = goodColor;
//         message.style.color = goodColor;
//         document.getElementById('save').disabled = false;
//         message.innerHTML = "Passwords Match!"
//     }else{
//         pass2.style.backgroundColor = badColor;
//         message.style.color = badColor;
//         document.getElementById('save').disabled = true;
//         message.innerHTML = "Passwords Do Not Match!"
//     }
// }  

</script>

<?php $user = $this->session->userdata('sess_mbalia'); ?>

<div class="row">
	<div class="span12">      		  		
  		<div class="widget ">
  			<div class="widget-header">
  				<i class="icon-user"></i>
  				<h3>Ubah Password</h3>
			</div> <!-- /widget-header -->
			
			<div class="widget-content">
				<div class="span11">
					<a href="<?php echo base_url(); ?>adminpuskom/board">Home</a> / <a href="<?php echo base_url(); ?>adminpuskom/cari_pw">Cari Akun</a>
					<b><center>Cari Password Akun</center></b><br>
					<form id="edit-profile" class="form-horizontal" method="post" action="<?php echo base_url(); ?>adminpuskom/cari_pw/cari">
						<fieldset>
							<div class="control-group">											
								<label class="control-label">NPM : </label>
								<div class="controls">
									<input type="text" class="span3" name="npm" placeholder="Masukan NPM" required>
								</div> <!-- /controls -->				
							</div> <!-- /control-group -->
							
							<div class="form-actions">
								<input type="submit" class="btn btn-primary" id="save" value="Cari Password"/>
							</div> <!-- /form-actions -->
						</fieldset>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>