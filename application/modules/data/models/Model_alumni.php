<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Model_alumni extends CI_Model {

	public function __construct()
	{
		parent::__construct();
		
	}

	function getDataAlumni($prodi)
	{
		$this->db->where('STMHSMSMHS', 'L');
		$this->db->where('KDPSTMSMHS', $prodi);
		$query = $this->db->get('tbl_mahasiswa');
		if ($query->num_rows() > 0) {
			return $query->result();
		} else {
			return array();
		}
	}
}

/* End of file Do_model.php */
/* Location: ./application/models/Do_model.php */