<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Do_model extends CI_Model {

	public $variable;

	public function __construct()
	{
		parent::__construct();
		
	}

	function listSkep($uid)
	{
		$this->db->select('*, count(skep) as jml');
		$this->db->from('tbl_dropout');
		$this->db->where('audit_user', $uid);
		$this->db->group_by('skep');
		return $this->db->get();
	}

	function getDataByTa($uid, $ta)
	{
		$this->db->where('tahunajaran', $ta);
		$this->db->where('audit_user', $uid);
		return $this->db->get('tbl_dropout')->result();
	}

	function getDataByAkt($uid, $akt)
	{
		$this->db->where('audit_user', $uid);
		$this->db->like('npm_mahasiswa', $akt, 'after');
		return $this->db->get('tbl_dropout')->result();
	}

}

/* End of file Do_model.php */
/* Location: ./application/models/Do_model.php */