<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Fakultas extends MY_Controller {

	function __construct()
	{
		parent::__construct();
		//$this->load->library('form_validation');
		if ($this->session->userdata('sess_login') == TRUE) {
			$cekakses = $this->role_model->cekakses(28)->result();
			if ($cekakses != TRUE) {
				echo "<script>alert('Anda Tidak Berhak Mengakses !!');history.go(-1);</script>";
			}
		} else {
			redirect('auth','refresh');
		}
	}

	public function index()
	{
		$data['fakultas']=$this->app_model->getdata('tbl_fakultas','kd_fakultas','asc');
		$data['page']="fakultas_view";
		$this->load->view('template/template', $data);
	}

	function del_fakultas($id)
	{
		$this->app_model->deletedata('tbl_fakultas','id_fakultas',$id);
		echo "<script>alert('Berhasil');
		document.location.href='".base_url()."data/fakultas';</script>";
	}

	function save_fakultas()
	{
		$data = array(
		'kd_fakultas' 	=> $this->form_validation->set_rules('kd_fak', 'Kode Fakultas', 'trim|required'),
		'fakultas' 		=> $this->form_validation->set_rules('nama_fak', 'Nama Fakultas', 'trim|required')
		);
		if ($this->form_validation->run() == FALSE) {
			echo "<script>alert('Gagal Simpan Data');history.go(-1);</script>";
		} else {
			$data = array(
			'kd_fakultas' 	=> $this->input->post('kd_fak', TRUE),
			'fakultas' 		=> $this->input->post('nama_fak', TRUE)
			);
			$this->app_model->insertdata('tbl_fakultas',$data);
			echo "<script>alert('Sukses');
			document.location.href='".base_url()."data/fakultas';</script>";
		}
	}

	function update_fakultas()
	{
		$data = array(
			'kd_fakultas'	=> $this->form_validation->set_rules('kode', 'Kode Fakultas', 'trim|required|xss_clean'),
			'fakultas'		=> $this->form_validation->set_rules('fakultas', 'Nama Fakultas', 'trim|required|xss_clean')
		);
		if ($this->form_validation->run() == FALSE) {
			echo "<script>alert('Gagal Simpan Data');history.go(-1);</script>";
		} else {
			$data['fakultas']=$this->app_model->updatedata('tbl_fakultas','id_fakultas',$this->input->post('id_fakultas'),$data);
			echo "<script>alert('Sukses');
			document.location.href='".base_url()."data/fakultas';</script>";
		}
	}

	// function loaddata($idf)
	// {
	// 	$data['fakultas']=$this->app_model->getdata('tbl_fakultas','kd_fakultas','asc');
		
	// }

	function get_list_faks($idf)
	{
		// $data['fakultas']=$this->app_model->getdata('tbl_fakultas','kd_fakultas','asc');
		$data['list'] = $this->db->query("select * from tbl_fakultas where id_fakultas = '$idf'")->row();
		$this->load->view('data/edit_fakultas_view',$data);
	}

}

/* End of file  */
/* Location: ./application/controllers/ */