<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Karyawan extends MY_Controller {

	function __construct()
	{
		parent::__construct();
		if ($this->session->userdata('sess_login') == TRUE) {
			$cekakses = $this->role_model->cekakses(8)->result();
			if ($cekakses != TRUE) {
				echo "<script>alert('Anda Tidak Berhak Mengakses !!');history.go(-1);</script>";
			}
		} else {
			redirect('auth','refresh');
		}
	}

	function index()
	{	
		$data['karyawan'] = $this->app_model->get_karyawan();
		$data['group']  = $this->db->get('tbl_user_group')->result();
		$data['divisi'] = $this->app_model->getdata('tbl_divisi','divisi','asc');
	
		$data['page'] = 'data/karyawan_view';
		$this->load->view('template/template',$data);
	}
	
	function load_edit($idk){
		$data['divisi'] = $this->db->get('tbl_divisi')->result();
		$data['group']  = $this->db->get('tbl_user_group')->result();
		$data['row'] = $this->db->select('*')
								->from('tbl_karyawan a')
								->join('tbl_jabatan b','a.jabatan_id = b.id_jabatan')
								->join('tbl_divisi c','b.kd_divisi = c.kd_divisi')
								->where('id_kary',$idk)
								->get()->row();

		//var_dump($data['row']);die();
		$this->load->view('data/karyawan_edit',$data);
	}
	
	
	
	function get_listjab($id){
		$data = $this->db->query("select * from tbl_jabatan where kd_divisi = '$id'")->result();
		$list = "<option> -- </option>";
		foreach($data as $row){
		$list .= "<option value='".$row->id_jabatan."'>".$row->jabatan."</option>";
		}
		die($list);
	}

	function upload_photo($data,$name){
		$file = $data;
		$folder = "./upload/";
		$folder = $folder . basename($name);
		move_uploaded_file($data['tmp_name'], $folder);	
	}
	
	function save_karyawan(){
		date_default_timezone_set('Asia/Jakarta');
		$tgl  = date('YmdHis');
		$photo = ''.$tgl.'_'.$_FILES['foto']['name'].'';
		//$this->upload_photo($_FILES['foto'],$photo);
		
		$data = array(
		'nik'		  => $this->input->post('nik'),
		'nik_type'	  => $this->input->post('tipe'),
		'nama'		  => $this->input->post('nama'),
		'jns_kel'	  => $this->input->post('jk'),
		'alamat'      => $this->input->post('alamat'),
		'hp'		  => '+62'.$this->input->post('telepon'),
		'email'		  => $this->input->post('email'),
		'jabatan_id'  => $this->input->post('jabatan'),
		'status'	  => $this->input->post('status'),
		'pictures'	  => $photo
		);

		//$this->app_model->insertdata('tbl_karyawan',$data);

		//var_dump($data);die();

		$this->db->insert('tbl_karyawan', $data);
		echo "<script>alert('Sukses');
		document.location.href='".base_url()."data/karyawan';</script>";
	}
	
	function update_karyawan(){
		//$photo = date('Ymdhis').'_'.$_FILES['foto']['name'];
		//$this->upload_photo($_FILES['foto'],$photo);
		$data = array(
		'nik'		  => $this->input->post('nik'),
		'nama'		  => $this->input->post('nama'),
		'jns_kel'	  => $this->input->post('jk'),
		'alamat'      => $this->input->post('alamat'),
		'hp'		  => '+62'.$this->input->post('telepon'),
		'email'		  => $this->input->post('email'),
		'jabatan_id'  => $this->input->post('jabatan'),
		'status'	  => $this->input->post('status')
		//'pictures'	  => $photo
		);

		//var_dump($data);die();

		$this->db->where('id_kary',$this->input->post('id_karyawan'));
		$this->db->update('tbl_karyawan',$data);
		echo "<script>alert('Sukses');
		document.location.href='".base_url()."data/karyawan';</script>";
	}
	
	function del_karyawan($id){
		$this->app_model->deletedata('tbl_karyawan','id_kary',$id);
		echo "<script>alert('Berhasil');
		document.location.href='".base_url()."data/karyawan';</script>";
	}

}

/* End of file karyawan.php */
/* Location: ./application/controllers/karyawan.php */