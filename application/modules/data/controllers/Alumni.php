<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');



class Alumni extends MY_Controller {
	public function __construct()
	{
		parent::__construct();
		/* if ($this->session->userdata('sess_login') == TRUE) {

			$cekakses = $this->role_model->cekakses(40)->result();

			if ($cekakses != TRUE) {

				echo "<script>alert('Anda Tidak Berhak Mengakses !!');history.go(-1);</script>";

			}

		} else {

			redirect('auth','refresh');

		} */
	}



	public function index()
	{
		$user = $this->session->userdata('sess_login');
		$nik   = $user['userid'];
		$pecah = explode(',', $user['id_user_group']);
		$jmlh = count($pecah);
		for ($i=0; $i < $jmlh; $i++) { 
			$grup[] = $pecah[$i];
		}
		$cek_bea=$this->app_model->getDetail('tbl_beasiswa','npm',$nik,'id_beasiswa','desc','1')->row();
		if ((in_array(20, $grup))){
			$data['fakultas'] =$this->app_model->getdata('tbl_fakultas', 'id_fakultas', 'ASC')->result();
	    	$data['page']='cetak_alumni_filter';
	    }
		$this->load->view('template/template', $data);
	}
	function get_jurusan($id)
	{
        $jurusan = $this->app_model->getdetail('tbl_jurusan_prodi', 'kd_fakultas', $id, 'id_prodi', 'ASC')->result();
		$out = "<select class='form-control' name='jurusan' id='jurs'><option hidden disabled selected>--Pilih Program Studi--</option>";
        foreach ($jurusan as $row) {
            $out .= "<option value='".$row->kd_prodi."'>".$row->prodi. "</option>";
        }
        $out .= "</select>";
        echo $out;
	}
	function save_session()
	{
		$fakultas = $this->input->post('fakultas');
		$jurusan = $this->input->post('jurusan');
		$this->session->set_userdata('kd_fakultas', $fakultas);
		$this->session->set_userdata('kd_jurusan', $jurusan);
		redirect(base_url('data/alumni/daftar_alumni'));
	}
	function daftar_alumni()
	{
		$this->load->model('Model_alumni');
		$data['mahasiswa'] =$this->Model_alumni->getDataAlumni($this->session->userdata('kd_jurusan'));
		$data['page']='cetak_alumni_list';
		$this->load->view('template/template', $data);
	}
	function cetak_kartu($npm)
	{
		$lulusan = $this->app_model->getDetail('tbl_lulusan','npm_mahasiswa',$npm,'id_lulusan','DESC','1');
		if($lulusan->num_rows() > 0 ){
			$lulus = $lulusan->row();
		}else{
			$lulus = array();
		}
		$bio = $this->app_model->getDetail('tbl_alumni_bio','npm',$npm,'id_alumni','DESC','1');
		$file = './image/foto_alumni/'.$npm.'.jpg'; 
		//dd($file);
		if($bio->num_rows() > 0 ){
			$biod = $bio->row();
			if(file_exists($file) and empty($biod->foto)) {
				$data = array(
						'npm'		=> $npm,
						'foto'		=> $npm.'.jpg'
						);
				$this->app_model->updatedata('tbl_alumni_bio','npm',$npm,$data);
			}
			$new_bio = $this->app_model->getDetail('tbl_alumni_bio','npm',$npm,'id_alumni','DESC','1');
			$biodata = $new_bio->row();
		}else{
			if(file_exists($file)) {
				$data = array(
						'npm'		=> $npm,
						'foto'		=> $npm.'.jpg'
						);
				$this->app_model->insertdata('tbl_alumni_bio',$data);
			}
			$new_bio = $this->app_model->getDetail('tbl_alumni_bio','npm',$npm,'id_alumni','DESC','1');
			$biodata = $new_bio->row();
		}
		$data['mhs'] = $this->app_model->getDetail('tbl_mahasiswa','NIMHSMSMHS',$npm,'id_mhs','DESC','1')->row();
		$data['lulusan'] = $lulus;
		$data['bio'] = $biodata;

		try {
		    
			$mpdf = new \Mpdf\Mpdf();
			$html = $this->load->view('cetak_alumni_kartu',$data,true);
			$mpdf->WriteHTML($html);
			$mpdf->Output();
		} catch (\Mpdf\MpdfException $e) { // Note: safer fully qualified exception name used for catch
		    // Process the exception, log, print etc.
		    echo $e->getMessage();
		}	


	}
	function isi_bio($npm)
	{
		$data['npm'] = $npm;
		$data['mhs'] = $this->app_model->getDetail('tbl_mahasiswa','NIMHSMSMHS',$npm,'id_mhs','DESC','1')->row();
		
		$bio_mhs = $this->app_model->getDetail('tbl_bio_mhs','npm',$npm,'id','DESC','1');
		if($bio_mhs->num_rows() > 0 ){
			$biodata_mhs = $bio_mhs->row();
		}else{
			$biodata_mhs = array();
		}
		$data['bio_mhs'] = $biodata_mhs;

		$bio = $this->app_model->getDetail('tbl_alumni_bio','npm',$npm,'id_alumni','DESC','1');
		if($bio->num_rows() > 0 ){
			$biodata = $bio->row();
		}else{
			$biodata = array();
		}
		$data['bio'] = $biodata;

		$this->load->view('cetak_alumni_modal', $data);
	}
	function add_biodata()
	{
		$row = $this->db->where('npm',$this->input->post('npm'))->get('tbl_alumni_bio')->row();
		
		$bio_mhs = $this->db->where('npm',$this->input->post('npm'))->get('tbl_bio_mhs')->row();
		$data = array(
			'npm'		=> $this->input->post('npm'),
			'alamat'	=> $this->input->post('alamat'),
			'no_hp'		=> $this->input->post('tlp'),
			'email'		=> $this->input->post('email')
			);
			
		if($bio_mhs){
			$this->app_model->updatedata('tbl_bio_mhs','npm',$this->input->post('npm'),$data);
		}else{
			$this->app_model->insertdata('tbl_bio_mhs',$data);
		}

		$this->load->helper('inflector');
		$this->load->helper('security');
		$this->load->library('secure_library');
		
		$config = array();
		$tem = $_FILES['foto']['name'];
		$npmfoto = $this->input->post('npm');
		$ext = pathinfo($tem, PATHINFO_EXTENSION);
		$save_name = $npmfoto . '.' . $ext;
		
		$config['upload_path']   = './image/foto_alumni/';
		$config['allowed_types'] = 'gif|jpg|jpeg|png';
		$config['file_name']     = $save_name;
		$config['max_size']      = '800';
		$config['max_width']     = '102400';
		$config['max_height']    = '102400';
		$config['overwrite'] 	 = TRUE;
		
		$this->load->library('upload', $config);
		
		if (!$this->upload->do_upload('foto')) {
			$data = array(
					'npm'		=> $this->input->post('npm'),
					'alamat'	=> $this->input->post('alamat'),
					'tlp'		=> $this->input->post('tlp'),
					'email'		=> $this->input->post('email'),
					'pekerjaan'	=> $this->input->post('pekerjaan'),
					'mutasi'	=> $this->input->post('mutasi')
					);
					
				if($row){
					$this->app_model->updatedata('tbl_alumni_bio','npm',$this->input->post('npm'),$data);
				}else{
					$this->app_model->insertdata('tbl_alumni_bio',$data);
				}
				redirect('data/alumni/daftar_alumni', 'refresh');
		}else{
			$this->secure_library->filter_post($_FILES['foto']['name'], 'xss_clean');
			if ($this->secure_library->start_post()) {
				$data = array(
					'npm'		=> $this->input->post('npm'),
					'alamat'	=> $this->input->post('alamat'),
					'tlp'		=> $this->input->post('tlp'),
					'email'		=> $this->input->post('email'),
					'pekerjaan'	=> $this->input->post('pekerjaan'),
					'mutasi'	=> $this->input->post('mutasi'),
					'foto'		=> xss_clean($save_name)
					);
					
				if($row){
					$this->app_model->updatedata('tbl_alumni_bio','npm',$this->input->post('npm'),$data);
				}else{
					$this->app_model->insertdata('tbl_alumni_bio',$data);
				}
				redirect('data/alumni/daftar_alumni', 'refresh');
			} else {
				echo '<pre>';
				print_r($this->upload->display_errors());
				echo 'Pengunggahan mengalami kendala. Mohon Cek kembali file anda. <a style="color:blue" onclick="history.go(-1)"> << Kembali </a>';
				exit();
			}
		}
	}
	
}



/* End of file Biodata.php */

/* Location: ./application/controllers/Biodata.php */