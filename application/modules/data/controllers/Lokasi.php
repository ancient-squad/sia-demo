<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Lokasi extends MY_Controller {

	// function __construct()
	// {
	// 	parent::__construct();
	// 	//$id_menu = 32 (database); cek apakah user memiliki akses
	// 	if ($this->session->userdata('sess_login') == TRUE) {
	// 		$akses = $this->role_model->cekakses(51)->result();
	// 		if ($akses != TRUE) {
	// 			redirect('home','refresh');
	// 		}
	// 	} else {
	// 		redirect('auth','refresh');
	// 	}
	// }

	function index()
	{	
		$data['page'] = 'organisasi/lokasi_view';
		$this->load->view('template/template',$data);
	}

	function view_lokasi(){
		$data['page'] = 'organisasi/lokasi_view';
		$this->load->view('template/template',$data);
	}

	function cek()
	{
		echo date('Y-m-d H:i');
	}

}

/* End of file mahasiswa.php */
/* Location: ./application/modules/data/controllers/mahasiswa.php */