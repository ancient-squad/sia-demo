<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Lulusan extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
        $this->load->model('setting_model');
        error_reporting(0);
        if ($this->session->userdata('sess_login') == TRUE) {
            $cekakses = $this->role_model->cekakses(147)->result();
            if ($cekakses != TRUE) {
                echo "<script>alert('Anda Tidak Berhak Mengakses !!');history.go(-1);</script>";
            }
        } else {
            redirect('auth','refresh');
        }
        date_default_timezone_set('Asia/Jakarta');
	}

	public function form()
	{
		$data['page']='v_lulusan_form';
        $this->load->view('template/template', $data);
	}

    function index(){
        $this->session->unset_userdata('tahun');
        $this->session->unset_userdata('jurusan');

        $logged = $this->session->userdata('sess_login');
        $pecah = explode(',', $logged['id_user_group']);
        $jmlh = count($pecah);

        for ($i=0; $i < $jmlh; $i++) { 
            $grup[] = $pecah[$i];
        }
        
        if ((in_array(10, $grup))) { //baa
            $data['fakultas'] = $this->app_model->getdata('tbl_fakultas', 'id_fakultas', 'ASC')->result();
            $data['akademik'] = $this->db->get('tbl_tahunakademik')->result();

            $data['page'] = 'data/v_lulusan_select_baa';

        } elseif ((in_array(9, $grup))) { // fakultas
            $data['jurusan']=$this->app_model->getdetail('tbl_jurusan_prodi','kd_fakultas',$logged['userid'],'prodi','asc')->result();
            $data['akademik'] = $this->db->get('tbl_tahunakademik')->result();

            $data['page'] = 'data/v_lulusan_select_fak';

        } elseif ((in_array(8, $grup))) { // prodi
            $data['jurusan']=$this->db->where('kd_prodi',$logged['userid'])->get('tbl_jurusan_prodi')->result();
            $data['akademik'] = $this->db->get('tbl_tahunakademik')->result();

            $data['page'] = 'data/v_lulusan_select_prodi';

        } elseif ((in_array(1, $grup))) { // admin
            $data['fakultas']=$this->app_model->getdata('tbl_fakultas', 'id_fakultas', 'ASC')->result();
            $data['akademik'] = $this->db->get('tbl_tahunakademik')->result();

            $data['page'] = 'data/v_lulusan_select_baa';
        }

        $this->load->view('template/template', $data);   
    }

    function view(){
        $jurusan = $this->input->post('jurusan');
        $tahun = $this->input->post('tahun');

        $this->session->set_userdata('tahun',$tahun);
        $this->session->set_userdata('jurusan',$jurusan);

        redirect(base_url().'data/lulusan/view_lulusan','refresh');
    }

    function view_lulusan(){

        if ($this->session->userdata('jurusan')) {
            $data['rows']   = $this->db->query("SELECT * FROM tbl_lulusan lls 
                                                JOIN tbl_mahasiswa mhs ON mhs.`NIMHSMSMHS` = lls.`npm_mahasiswa`
                                                WHERE mhs.`KDPSTMSMHS` = '".$this->session->userdata('jurusan')."' 
                                                AND lls.`ta_lulus` = ".$this->session->userdata('tahun')."")->result();

            $data['page']='v_lulusan_view';
            $this->load->view('template/template', $data);    
        }else{
            redirect(base_url().'data/lulusan','refresh');
        }

        
    }

    function save(){
    	$data = array(
    					'npm_mahasiswa' => $this->input->post('npm'), 
    					'sk_yudisium' => $this->input->post('sk_yudi'),
    					'tgl_yudisium' => $this->input->post('tgl_yudi'),
    					'tgl_lulus' => $this->input->post('tgl_lulus'),
    					'ipk' => $this->input->post('ipk'),
    					'no_ijazah' => $this->input->post('no_ijazah'),
    					'jdl_skripsi' => $this->input->post('jdl_skripsi'),
    					'mulai_bim' => $this->input->post('mulai_bim'),
    					'ahir_bim' => $this->input->post('ahir_bim'),
                        'ta_lulus' => $this->input->post('ta'),
                        'sks' => $this->input->post('sks'),
                        'flag_feeder' => 1,
                        'pem1' => $this->input->post('pem1'),
                        'pem2' => $this->input->post('pem2')
    					);

        $this->db->insert('tbl_lulusan', $data);
        echo "<script>alert('Sukses');
        document.location.href='".base_url()."data/lulusan';</script>";
    }

    function edit($id){
        //die($id);

         $data['row'] = $this->db->select('lls.*,mhs.NMMHSMSMHS')
                                    ->from('tbl_lulusan lls')
                                    ->join('tbl_mahasiswa mhs','lls.npm_mahasiswa = mhs.NIMHSMSMHS')
                                    ->where('id_lulusan',$id)
                                    ->get()->row();


        $data['page']='v_lulusan_form_edit';
        $this->load->view('template/template', $data);
    }

    function save_edit(){

        $id = $this->input->post('id');

        $data = array(
                        'sk_yudisium' => $this->input->post('sk_yudi'),
                        'tgl_yudisium' => $this->input->post('tgl_yudi'),
                        'tgl_lulus' => $this->input->post('tgl_lulus'),
                        'ipk' => $this->input->post('ipk'),
                        'no_ijazah' => $this->input->post('no_ijazah'),
                        'jdl_skripsi' => $this->input->post('jdl_skripsi'),
                        'mulai_bim' => $this->input->post('mulai_bim'),
                        'ahir_bim' => $this->input->post('ahir_bim'),
                        'sks' => $this->input->post('sks'),
                        );

        $this->db->where('id_lulusan', $id);
        $this->db->update('tbl_lulusan', $data);

        echo "<script>alert('Sukses');
        document.location.href='".base_url()."data/lulusan';</script>";
    }

    function delete_data($id){
        $this->db->where('id_lulusan', $id)
        ->delete('tbl_lulusan');

        echo "<script>alert('Sukses');
        document.location.href='".base_url()."data/lulusan';</script>";
    }

    function get_jurusan($id)
    {
        $jurusan = $this->app_model->getdetail('tbl_jurusan_prodi', 'kd_fakultas', $id, 'id_prodi', 'ASC')->result();
        $out = "<select class='form-control' name='jurusan' id='jurs'><option>--Pilih Jurusan--</option>";
        foreach ($jurusan as $row) {
            $out .= "<option value='".$row->kd_prodi."'>".$row->prodi. "</option>";
        }
        
        $out .= "</select>";

        echo $out;
    }

    function load_mhs_autocomplete(){

    $prodi = $this->session->userdata('jurusan');

    $this->db->distinct();

    $this->db->select("a.NIMHSMSMHS,a.NMMHSMSMHS");

    $this->db->from('tbl_mahasiswa a');

    $this->db->where('a.KDPSTMSMHS', $prodi);

    $this->db->like('a.NMMHSMSMHS', $_GET['term'], 'both');

    $this->db->or_like('a.NIMHSMSMHS', $_GET['term'], 'both');

    $sql  = $this->db->get();

    $data = array();

    foreach ($sql->result() as $row) {

        $data[] = array(

                        'nama'       => $row->NMMHSMSMHS,

                        'npm'           => $row->NIMHSMSMHS,

                        'value'         => $row->NIMHSMSMHS.' - '.$row->NMMHSMSMHS

                        );

    }

    echo json_encode($data);

    }

    function load_dospem(){

    $prodi = $this->session->userdata('jurusan');

    $sql = $this->db->query("SELECT * FROM tbl_karyawan kry
                            JOIN tbl_jabatan jbt ON kry.`jabatan_id` = jbt.`id_jabatan`
                            WHERE jbt.`kd_divisi` = ".$prodi." AND (kry.`nidn` IS NOT NULL OR kry.`nupn` IS NOT NULL)
                            AND kry.nama LIKE '%".$_GET['term']."%' ");

    $data = array();

    foreach ($sql->result() as $row) {

        $data[] = array(

                        'nama'       => $row->nama,

                        'nid'           => $row->nid,

                        'value'         => $row->nid.' - '.$row->nama

                        );

    }

    echo json_encode($data);

    }

}

/* End of file Lulusan.php */
/* Location: ./application/controllers/Lulusan.php */