<div class="row">
	<div class="span12">      		  		
  		<div class="widget ">
  			<div class="widget-header">
                <i class="icon-home"></i>
  				<h3>Data PMB Program Pasca Sarjana (S.2)</h3>
			</div>
			<div class="widget-content">
				<form class="form-horizontal" action="<?php echo base_url(); ?>data/mahasiswapmb/update_s2" method="post" enctype="multipart/form-data">
					<fieldset>
						<div class="control-group">
							<label class="control-label">Jenis PMB</label>
							<div class="controls">
								<select class="form-control span4" name="jenis" disabled/>
									<option disabled="">--Pilih Jenis PMB--</option>
									<?php if ($cek->jenis_pmb == 1) { ?>
										<option value="1" id="rm" selected="">Readmisi</option>
									<?php } elseif ($cek->jenis_pmb == 2) { ?>
										<option value="2" id="mhs" selected="">Mahasiswa Baru</option>
									<?php } elseif ($cek->jenis_pmb == 3) { ?>
										<option value="3" id="mhs" selected="">Mahasiswa Konversi</option>
									<?php } ?>
								</select>
							</div>
						</div>
						<div class="control-group">
							<label class="control-label">Pilihan Kampus</label>
							<div class="controls">
								<select class="form-control span4" name="kampus" required/>
									<option disabled="">--Pilih Kampus--</option>
									<option value="bks" <?php if ($cek->kampus == 'bks') { echo "selected=''";}?>>Bekasi</option>
										<option value="jkt" <?php if ($cek->kampus == 'jkt') { echo "selected=''";}?>>Jakarta</option>
								</select>
							</div>
						</div>
						<!-- <div class="control-group">
							<label class="control-label">Pilihan Kelas</label>
							<div class="controls">
								<select class="form-control span4" name="kelas" required/>
									<option disabled="" selected="">--Pilih Kelas--</option>
									<option value="pg" <?php if ($cek->kelas == 'pg') { echo "selected=''";}?>>Pagi</option>
									<option value="sr" <?php if ($cek->kelas == 'sr') { echo "selected=''";}?>>Sore</option>
								</select>
							</div>
						</div> -->
						<div class="control-group">
							<label class="control-label">Nama</label>
							<div class="controls">
								<input type="text" class="form-control span4" value="<?php echo $cek->nama; ?>" name="nama" required/><small>*sesuai ijazah/akte kelahiran</small>
							</div>
						</div>
						<div class="control-group">
							<label class="control-label">Jenis Kelamin</label>
							<div class="controls">
								<input type="radio" name="jk" value="L" <?php if ($cek->kelamin == 'L') { echo "checked=''";}?> required/> Laki - Laki
								<input type="radio" name="jk" value="P" <?php if ($cek->kelamin == 'P') { echo "checked=''";}?> required/> Perempuan
							</div>
						</div>
						<div class="control-group">
							<label class="control-label">Kewarganegaraan</label>
							<div class="controls">
								<script type="text/javascript">
									$(document).ready(function () {
						                $('#ni').click(function () {
						                    $('#no').hide('fast');
						                });
						                $('#na').click(function () {
						                    $('#no').show('fast');
						                });
						            });
								</script>
								<input type="radio" id="ni" name="wn" <?php if ($cek->negara == 'WNI') { echo "checked=''";}?> value="WNI"> WNI
								<input type="radio" id="na" name="wn" <?php if ($cek->negara == 'WNA') { echo "checked=''";}?> value="WNA"> WNA
								<input type="text" id="no" name="jns_wn" <?php echo $cek->negara_wna;?> class="form-control span4" placeholder="Isi Kewarganegaraan (jika WNA)">
							</div>
						</div>
						<div class="control-group">
							<label class="control-label">Tempat Lahir</label>
							<div class="controls">
								<input class="form-control span4" value="<?php echo $cek->tempat_lahir; ?>" name="tpt" required/>
							</div>
						</div>
						<div class="control-group">
							<label class="control-label">Tanggal Lahir</label>
							<script>
							  	$(function() {
							    	$( "#tgllhr" ).datepicker({
								      	changeMonth: true,
								      	changeYear: true,
								      	dateFormat: "yy-mm-dd",
								      	yearRange: "1950:2030"
							    	});
							  	});
							</script>
							<div class="controls">
								<input class="form-control span4" type="text" value="<?php echo $cek->tgl_lahir; ?>" name="tgl" id="tgllhr" required/>
							</div>
						</div>
						<div class="control-group">
							<label class="control-label">Agama</label>
							<div class="controls">
								<select class="form-control span4" name="agama" required/>
									<option disabled="" selected="">-- Pilih Agama --</option>
									<option <?php if ($cek->agama == 'Islam') { echo "selected=''";}?> value="Islam">Islam</option>
									<option <?php if ($cek->agama == 'Katolik') { echo "selected=''";}?> value="Katolik">Katolik</option>
									<option <?php if ($cek->agama == 'Protestan') { echo "selected=''";}?> value="Protestan">Protestan</option>
									<option <?php if ($cek->agama == 'Budha') { echo "selected=''";}?> value="Budha">Budha</option>
									<option <?php if ($cek->agama == 'Hindu') { echo "selected=''";}?> value="Hindu">Hindu</option>
									<option <?php if ($cek->agama == 'Lainnya') { echo "selected=''";}?> value="Lainnya">Lainnya</option>
								</select>
							</div>
						</div>
						<div class="control-group">
							<label class="control-label">Status</label>
							<div class="controls">
								<input type="radio" name="stsm" <?php if ($cek->status_nikah == 'Y') { echo "checked=''";}?> value="Y"> Menikah
								<input type="radio" name="stsm" <?php if ($cek->status_nikah == 'N') { echo "checked=''";}?> value="N"> Belum Menikah
							</div>
							<div class="controls">
								<script type="text/javascript">
									$(document).ready(function () {
						                $('#bkr').click(function () {
						                    $('#ttbkr').hide('fast');
						                });
						                $('#tbkr').click(function () {
						                    $('#ttbkr').show('fast');
						                });
						            });
								</script>
								<input type="radio" id="bkr" <?php if ($cek->status_kerja == 'N') { echo "checked=''";}?> name="stsb" value="N"> Belum Bekerja
								<input type="radio" id="tbkr" <?php if ($cek->status_kerja == 'Y') { echo "checked=''";}?> name="stsb" value="Y"> Bekerja
								<input class="form-control span4" id="ttbkr" type="text" name="tpt_kerja" placeholder="Isi pekerjaan (jika bekerja)">
							</div>
						</div>
						<div class="control-group">
							<label class="control-label">Alamat</label>
							<div class="controls">
								<textarea class="form-control span4" type="text" name="alamat" required/><?php echo $cek->alamat; ?></textarea>
							</div>
						</div>
						<div class="control-group">
							<label class="control-label">Kode Pos</label>
							<div class="controls">
								<input class="form-control span4" type="text" value="<?php echo $cek->kd_pos; ?>" name="kdpos" required/>
							</div>
						</div>
						<div class="control-group">
							<label class="control-label">No. Telpon / HP</label>
							<div class="controls">
								<input class="form-control span4" type="text" value="<?php echo $cek->tlp; ?>" name="tlp" required/>
							</div>
						</div>
						<script type="text/javascript">
							$(document).ready(function () {
				                $('#rm').click(function () {
				                    $('#konv').hide('fast');
				                    $('#read').show('fast');
				                });
				                $('#mhs').click(function () {
				                	$('#read').hide('fast');
				                    $('#konv').show('fast');
				                });
				            });
						</script>
						<?php if ($cek->jenis_pmb == 1) { ?>
							<div id="read">
								<div class="control-group">
									<label class="control-label">NPM Lama</label>
									<div class="controls">
										<input class="form-control span4" value="<?php echo $cek->npm_lama; ?>" type="number" name="npm">
									</div>
								</div>
								<div class="control-group">
									<label class="control-label">Tahun Masuk di <?= $this->ORG_NAME ?></label>
									<div class="controls">
										<input class="form-control span4" type="number" value="<?php echo $cek->thmsubj; ?>" name="thmasuk">
									</div>
								</div>
							</div>
						<?php } elseif ($cek->jenis_pmb == 2 || $cek->jenis_pmb == 3) { ?>
							<div id="konv">
								<div class="control-group">
									<label class="control-label">Nama Perguruan Tinggi</label>
									<div class="controls">
										<input class="form-control span4" value="<?php echo $cek->nm_univ; ?>" type="text" name="nm_pt">
									</div>
								</div>
								<div class="control-group">
									<label class="control-label">Program Studi</label>
									<div class="controls">
										<input class="form-control span4" type="text" value="<?php echo $cek->prodi; ?>" name="prodi_lm">
									</div>
								</div>
								<div class="control-group">
									<label class="control-label">Tahun lulus/Semester</label>
									<div class="controls">
										<input class="form-control span4" type="text" value="<?php echo $cek->th_lulus; ?>" name="thlulus">
									</div>
								</div>
								<div class="control-group">
									<label class="control-label">Kota Asal PTS/PTN</label>
									<div class="controls">
										<input class="form-control span4" type="text" value="<?php echo $cek->kota_asl_univ; ?>" name="kota_asal">
									</div>
								</div>
								<div class="control-group">
									<label class="control-label">NPM/NIM</label>
									<div class="controls">
										<input class="form-control span4" type="text" value="<?php echo $cek->npm_nim; ?>" name="npmnim">
									</div>
								</div>
							</div>
						<?php } ?>
						<div class="control-group">
							<label class="control-label">Program Pasca Sarjana(S.2)</label>
							<div class="controls">
								<select class="form-control span4" name="prodi" required/>
									<option disabled="" selected="">-- Pilih Program Studi--</option>
									<option <?php if ($cek->opsi_prodi_s2 == '61101') { echo "selected=''";}?> value="61101">Magister Manajemen</option>
									<option <?php if ($cek->opsi_prodi_s2 == '74101') { echo "selected=''";}?> value="74101">Magister Hukum</option>
								</select>
							</div>
						</div>
						<div class="control-group">
							<label class="control-label">Pekerjaan</label>
							<div class="controls">
								<select class="form-control span4" name="pekerjaan" required/>
									<option disabled="" selected="">-- Pilih Pekerjaan --</option>
									<option <?php if ($cek->pekerjaan == 'PN') { echo "selected=''";}?> value="PN">Pegawai Negeri</option>
									<option <?php if ($cek->pekerjaan == 'TP') { echo "selected=''";}?> value="TP">TNI / POLRI</option>
									<option <?php if ($cek->pekerjaan == 'PS') { echo "selected=''";}?> value="PS">Pegawai Swasta</option>
									<option <?php if ($cek->pekerjaan == 'WU') { echo "selected=''";}?> value="WU">Wirausaha</option>
									<option <?php if ($cek->pekerjaan == 'PSN') { echo "selected=''";}?> value="PSN">Pensiun</option>
									<option <?php if ($cek->pekerjaan == 'LL') { echo "selected=''";}?> value="LL">Lain-lain</option>
								</select>
							</div>
						</div>
						<div class="control-group">
							<label class="control-label">Nama Ayah</label>
							<div class="controls">
								<input class="form-control span4" type="text" value="<?php echo $cek->nm_ayah; ?>" name="nm_ayah" required/>
							</div>
						</div>
						<div class="control-group">
							<label class="control-label">Nama Ibu</label>
							<div class="controls">
								<input class="form-control span4" type="text" value="<?php echo $cek->nm_ibu; ?>" name="nm_ibu" required/>
							</div>
						</div>
						<div class="control-group">
							<label class="control-label">Penghasilan Sendiri</label>
							<div class="controls">
								<table>
									<tbody>
										<tr>
											<td>
												<input type="radio" value="1-2" <?php if ($cek->penghasilan == '1-2') { echo "checked=''";}?> name="gaji" required/>
												Rp 1,000,000 - 2,000,000
											</td>
											<td>
												<input type="radio" value="2,1-4" <?php if ($cek->penghasilan == '2,1-4') { echo "checked=''";}?> name="gaji" required/>
												Rp 2,100,000 - 4,000,000
											</td>
											<td>
												<input type="radio" value="4,1-6" <?php if ($cek->penghasilan == '4,1-6') { echo "checked=''";}?> name="gaji" required/>
												Rp 4,100,000 - 6,000,000
											</td>
											<td>
												<input type="radio" value=">6" <?php if ($cek->penghasilan == '>6') { echo "checked=''";}?> name="gaji" required/>
												> Rp 6,000,000
											</td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>
						<div class="control-group">
							<label class="control-label">Upload Foto</label>
							<div class="controls">
								<?php if ($cek->foto == './upload/imgpmb/') { ?>
									<input id="wizard-picture" type="file" name="img">
								<?php } else { ?>
									<input id="wizard-picture" type="text" name="img" value="<?php echo $cek->foto; ?>" disabled/>
								<?php } ?>
							</div>
						</div>
						<div class="control-group">
							<label class="control-label">Referensi</label>
							<div class="controls">
								<input class="form-control span4" type="text" value="<?php echo $cek->informasi_from; ?>" name="infrom" required/>
								<input type="hidden" value="<?php echo $cek->ID_registrasi; ?>" name="ole">
							</div>
						</div>
						<div class="form-actions">
							<input class="btn btn-large btn-primary" type="submit" value="Update">
							<input class="btn btn-large btn-default" type="reset" value="Clear">
						</div>
					</fieldset>
				</form>
			</div>
		</div>
	</div>
</div>
