<script type="text/javascript">
    $(document).ready(function($) {
        $('input[name^=mhs]').autocomplete({
            source: '<?= base_url('data/Dropout/load_mhs_autocomplete');?>',
            minLength: 1,
            select: function (evt, ui) {
                this.form.mhs.value = ui.item.value;
                this.form.npm.value = ui.item.npm;
            }
        });

        $('#tgl_skep').datepicker({
			dateFormat: "yy-mm-dd",
			yearRange: "<?= date('Y')-5 ?>:<?= date('Y') ?>",
			changeMonth: true,
			changeYear: true
		});   
    });

    function edit(idk){
        $('#edit_dropout').load('<?= base_url();?>data/Dropout/edit_dropout/'+idk);
    }
</script>

<?php $sess = $this->session->userdata('sess_login'); ?>

<div class="row">
    <div class="span12">                    
        <div class="widget ">
            <div class="widget-header">
                <a 
                    href="<?= base_url('data/dropout') ?>" 
                    class="btn btn-default" 
                    style="margin-left: 10px" 
                    data-toggle="tooltip" title="kembali">
                    <i class="icon-chevron-left" style="margin-left: 0"></i>
                </a>
                <?php if ($sess['id_user_group'] == 10) {
                    $logbaa = $this->session->userdata('sessforselectbaa');
                    $forta = $logbaa['tahunajaran'];
                } else {
                    $forta = $this->session->userdata('tahunajaran');
                } ?>
                <h3>Data dropout tahun ajaran <?= get_thnajar($forta) .' - '. $prodi ?></h3>
            </div> <!-- /widget-header -->
            
            <div class="widget-content">
                <?php
                if ($sess['id_user_group'] == 10) { ?>
                    <a href="<?= base_url('sync_feed/dropout/sync_do') ?>" class="btn btn-primary" ><i class="btn-icon-only icon-refresh"> </i> Sinkronisasi Drop Out</a>
                    <a href="<?= base_url('sync_feed/dropout/update_sync_do') ?>" data-toggle="tooltip" title="Hanya gunakan fitur ini jika anda telah melakukan sinkronisasi sebelumnya!" class="btn btn-default" ><i class="btn-icon-only icon-refresh"> </i> Update Sinkronisasi Drop Out</a>
                    <hr>
                <?php } ?>
                <div class="span11">
                    <table id="example1" class="table table-bordered table-striped">
                        <thead>
	                        <tr>
	                        	<th>No. SKEP</th>
	                            <th>NPM</th>
	                            <th>Nama Mahasiswa</th>
	                            <th>Alasan</th>
	                        </tr>
	                    </thead>
	                    <tbody>
							<?php  foreach($rows as $row) {
								if ($row->alasan == 1) {
									$res = 'Mengundurkan Diri';
								} elseif ($row->alasan == 2){
									$res = 'Dikeluarkan';
								} elseif ($row->alasan == 3) {
                                    $res = 'Wafat';
                                } ?>
	                        <tr>
	                        	<td><?= $row->skep ?></td>
	                        	<td><?= $row->npm_mahasiswa ?></td>
	                            <td><?= get_nm_mhs($row->npm_mahasiswa) ?></td>
	                            <td><?= $res;?></td>
	                        </tr>
							<?php } ?>
	                    </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- modal edit -->
<div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content" id="edit_dropout">
            
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<!-- modal tambah -->
<div class="modal fade" id="addModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Tambah Data Dropout</h4>
            </div>
            <form class ='form-horizontal' action="<?= base_url();?>data/Dropout/add" method="post" enctype="multipart/form-data">
                <div class="modal-body" style="margin-left: -60px;">
                	<div class="control-group" id="">
                        <label class="control-label">No. SKEP DO</label>
                        <div class="controls">
                            <input type="text" class="span4" name="skep" placeholder="Input SKEP Drop Out" class="form-control" value="" required/>
                        </div>
                    </div>
                    <div class="control-group" id="">
                        <label class="control-label">Tanggal SKEP</label>
                        <div class="controls">
                            <input type="text" class="span2" name="tgl_skep" id="tgl_skep" placeholder="Masukan Tgl SKEP" value=""  required>
                        </div>
                    </div>   
                    <div class="control-group" id="">
                        <label class="control-label">Mahasiswa</label>
                        <div class="controls">
                            <input type="text" class="span4" name="mhs" placeholder="Input Nama Mahasiswa" class="form-control" value="" required/>
                            <input type="hidden" class="span4" name="npm" class="form-control" value="" required/>
                        </div>
                    </div>              
                    <div class="control-group" id="">
                        <label class="control-label">Alasan</label>
                        <div class="controls">
                            <select name="res" class="span2" class="form-control">
                            	<option disabled>-- Pilih Alasan --</option>
                                <!-- //2 = dikeluarkan, 1 = mengundurkan diri, 3 = wafat-->
                            	<option value="1">Mengundurkan Diri</option>
                            	<option value="2">Dikeluarkan</option>
                                <option value="3">Wafat</option>
                            </select>
                        </div>
                    </div>
                </div> 
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Keluar</button>
                    <input type="submit" class="btn btn-primary" value="Simpan"/>
                </div>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->