<div class="row">
  <div class="span12">                
    <div class="widget ">
      <div class="widget-header">
        <i class="icon-user"></i>
        <h3>Data Lulusan</h3>
      </div> <!-- /widget-header -->
      
      <div class="widget-content">
        <div class="span11">
          <form method="post" class="form-horizontal" action="<?= base_url(); ?>data/lulusan/view" target="_blank">
            <fieldset>
              <div id="jurusan" class="control-group">
                <label class="control-label">Prodi</label>
                <div class="controls">
                  <select class="form-control span6" name="jurs" id="jurs" disabled>
                    <option disabled selected>-- Pilih Program Studi --</option>
                    <?php foreach ($jurusan as $key) { ?>
                      <option value="<?= $key->kd_prodi; ?>" <?= $key->kd_prodi == $userid ? "selected" : '' ?> >
                        <?= $key->prodi; ?>
                      </option>
                    <?php } ?>
                    <input type="hidden" name="jurusan" value="<?= $logged['userid']; ?>">
                  </select>
                </div>
              </div>
              <div id="tahun" class="control-group">
                <label class="control-label">Tahun Ajaran</label>
                <div class="controls">
                  <select class="form-control span6" name="tahun" id="thn" required>
                    <option disabled selected>-- Pilih Tahun Akademik --</option>
                    <?php foreach ($akademik as $key) { ?>
                      <option value="<?= $key->kode; ?>"><?= $key->tahun_akademik; ?></option>
                    <?php } ?>
                  </select>
                </div>
              </div>
              <div class="form-actions">
                  <input type="submit" class="btn btn-large btn-success" value="Cari"/> 
              </div>
            </fieldset>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>

