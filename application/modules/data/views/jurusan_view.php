<script type="text/javascript">
    function edit(idj) {
        $("#edit_jurusan").load('<?php echo base_url()?>data/jurusan/view_edit_jurs/'+idj);
    }
</script>

<div class="row">
	<div class="span12">      		  		
  		<div class="widget ">
  			<div class="widget-header">
  				<i class="icon-user"></i>
  				<h3>Data Fakultas</h3>
			</div> <!-- /widget-header -->
			
			<div class="widget-content">
				<div class="span11">
                    <?php dd($this->session->userdata('sess_login')['id_user_group']) ?>
                    <a data-toggle="modal" href="#tambahModal" class="btn btn-primary"> Tambah Data </a><br><hr>
					<table id="example1" class="table table-bordered table-striped">
	                	<thead>
	                        <tr> 
	                        	<th>No</th>
                                <th>Kode</th>
                                <th>Jurusan</th>
                                <th>Fakultas</th>
                                <?php if ($this->session->userdata('sess_login')['id_user_group'] != 9) : ?>
	                               <th width="120">Aksi</th> 
                                <?php endif; ?>
	                        </tr>
	                    </thead>
	                    <tbody>
							<?php $no = 1; foreach($jurusan as $row){?>
	                        <tr>
	                        	<td><?php echo $no;?></td>
	                        	<td><?php echo $row->kd_prodi;?></td>
	                        	<td><?php echo $row->prodi;?></td>
                                <td><?php echo $row->fakultas;?></td>
                                <?php if ($this->session->userdata('sess_login')['id_user_group'] != 9) : ?>
    	                        	<td class="td-actions">
    									<a onclick="edit(<?php echo $row->id_prodi;?>)" class="btn btn-primary btn-small" href="#editModal" data-toggle="modal"><i class="btn-icon-only icon-pencil"> </i></a>
    									<a onclick="return confirm('Apakah Anda Yakin?');" class="btn btn-danger btn-small" href="<?php echo base_url();?>data/jurusan/del_jurusan/<?php echo $row->id_prodi;?>"><i class="btn-icon-only icon-remove"> </i></a>
    								</td>
                                <?php endif; ?>
	                        </tr>
							<?php $no++; } ?>
	                    </tbody>
	               	</table>
				</div>
			</div>
		</div>
	</div>
</div>

<!-- modal edit -->
<div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content" id="edit_jurusan">
            
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->



<!-- modal tambah -->
<div class="modal fade" id="tambahModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Tambah Data Dropout</h4>
            </div>
            <form class ='form-horizontal' action="<?php echo base_url();?>data/jurusan/save_jurusan" method="post" enctype="multipart/form-data">
                <div class="modal-body" style="margin-left: -60px;">  
                    <div class="control-group" id="">
                        <label class="control-label">Mahasiswa</label>
                        <div class="controls">
                            <input type="text" class="span4" name="kd_jur" placeholder="Input Kode" class="form-control" value="" required/>
                        </div>
                    </div>              
                    <div class="control-group" id="">
                        <label class="control-label">Jurusan</label>
                        <div class="controls">
                            <input type="text" class="span4" name="nm_jur" placeholder="Input Jurusan" class="form-control" value="" required/>
                        </div>
                    </div>
                    <div class="control-group" id="">
                        <label class="control-label">Fakultas</label>
                        <div class="controls">
                            <select class="form-control" name='fakuls'>
                            <option>--Pilih Fakultas--</option>
                            <?php foreach ($fakultas as $row) { ?>
                                <option value="<?php echo $row->kd_fakultas;?>"><?php echo $row->fakultas;?></option>
                            <?php } ?>
                            </select>
                        </div>
                    </div>
                </div> 
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Keluar</button>
                    <input type="submit" class="btn btn-primary" value="Simpan"/>
                </div>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->