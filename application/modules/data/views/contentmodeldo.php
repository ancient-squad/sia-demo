<script>
  function remove (nim,tahunajaran) {
    if (confirm('Yakin ingin menghapus mahasiswa dari daftar ?')) {
      $.post('<?= base_url() ?>data/dropout/removeFromList/'+nim+'/'+tahunajaran, function(){
        alert('Berhasil!');
      })
    }
  }
</script>
<script type="text/javascript">
  $(document).ready(function() {
    $('#tgl_skep').datepicker({dateFormat: 'yy-mm-dd',
                    yearRange: "1945:2015",
                    changeMonth: true,
                    changeYear: true
    });
});
</script>


<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
    <h4 class="modal-title">Detil Data Dropout (<?= $dats->row()->skep; ?>)</h4>
</div>
<form action="<?php echo base_url();?>data/Dropout/updateDataDo" method="post">

  <input type="hidden" value="<?= $dats->row()->skep; ?>" name="idskep">
  <input type="hidden" value="<?= $dats->row()->tahunajaran; ?>" name="tajar">

  <div class="modal-body">
    <div class="control-group">
      <label for="noskep">Nomor SKEP</label>
      <input type="text" name="noskep" value="<?= $dats->row()->skep; ?>" id="noskep" class="form-control">
    </div>

    <div class="control-group">
      <label for="tahun">Tahun Ajaran</label>
      <select name="tahunajar" id="tahun">
        <?php foreach ($years as $vals) { ?>
          <option value="<?= $vals->kode ?>" <?php if($dats->row()->tahunajaran == $vals->kode) { echo 'selected=""'; }?>>
            <?= $vals->tahun_akademik ?>
          </option>
        <?php } ?>
      </select>
    </div>
    <div class="control-group">
      <label for="noskep">Tanggal SKEP</label>
		<input type="text" name="tgl_skep" value="<?= $dats->row()->tgl_skep; ?>" id="tgl_skep" class="form-control">
    </div>

    <hr>

    <table class="table table-stripped table-bordered">
      <thead>
          <tr>
            <th>No</th>
            <th>NPM</th>
            <th>Nama</th>
            <th>Prodi</th>
            <th>Hapus</th>
          </tr>
      </thead>
        <?php $no = 1; foreach ($dats->result() as $key) { ?>
            <tr>
                <td><?= $no; ?></td>
                <td><?= $key->npm_mahasiswa; ?></td>
                <td><?= get_nm_mhs($key->npm_mahasiswa); ?></td>
                <td><?= get_jur($key->audit_user); ?></td>
                <td>
                  <button class="btn btn-danger btn-sm" onclick="remove('<?= $key->npm_mahasiswa ?>','<?= $key->tahunajaran ?>')">
                    <i class="icon-remove"></i>
                  </button>
                </td>
            </tr>
        <?php $no++; } ?>
    </table>
  </div> 
  <div class="modal-footer">
    <button type="submit" class="btn btn-primary">Update</button>
    <button type="button" class="btn btn-warning" data-dismiss="modal">Keluar</button>
  </div>
</form>