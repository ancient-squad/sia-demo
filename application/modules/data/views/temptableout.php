<?php 
	$team = $this->session->userdata('drop');
	$c = $this->session->userdata('jml_array');
	$no = 1;
	for ($i=0; $i < $c; $i++) { 
		if (isset($team[$i])) { 

			if ($team[$i]['alasan'] == 1) {
				$res = 'Mengundurkan Diri';
			} elseif ($team[$i]['alasan'] == 2){
				$res = 'Dikeluarkan';
			} elseif ($team[$i]['alasan'] == 3) {
                $res = 'Wafat';
            } ?>
			
			<tr>
				<td><?= $no; ?></td>
				<td>
					<?= $team[$i]['nim']; ?>
					<input type="hidden" name="nim[]" value="<?= $team[$i]['nim']; ?>">
					<input type="hidden" name="skep[]" value="<?= $team[$i]['noskep']; ?>">
					<input type="hidden" name="tgl[]" value="<?= $team[$i]['tgskep']; ?>">
					<input type="hidden" name="thajar[]" value="<?= $team[$i]['thajar']; ?>">
					<input type="hidden" name="smawl[]" value="<?= $team[$i]['smawl']; ?>">
					<input type="hidden" name="alasan[]" value="<?= $team[$i]['alasan']; ?>">
				</td>
				<td><?= $team[$i]['nama']; ?></td>
				<td><?= $team[$i]['smawl']; ?></td>
				<td><?= $res; ?></td>
				<td><a class="btn btn-danger btn-sm" onclick="rmData(<?= $i ?>)"><i class="icon icon-remove"></i></a></td>	
			</tr>

		<?php }
		$no++;
	}
?>