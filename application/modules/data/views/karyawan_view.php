<script>

function edit(idk){

$('#editkary').load('<?php echo base_url();?>data/karyawan/load_edit/'+idk);

}

</script>

<div class="row">

	<div class="span12">      		  		

  		<div class="widget ">

  			<div class="widget-header">

  				<i class="icon-user"></i>

  				<h3>Data Karyawan dan Dosen</h3>

			</div> <!-- /widget-header -->

			

			<div class="widget-content">

				<div class="span11">

					<a data-toggle="modal" href="#myModal" class="btn btn-primary"> Tambah Data </a><br><hr>

					<table id="example1" class="table table-bordered table-striped">

	                	<thead>

	                        <tr> 

	                        	<th>No</th>

                                <th>NIK</th>

                                <th>Nama</th>

                                <th>Telepon</th>

                                <th>Divisi</th>

                                <th>Jabatan</th>

	                            <th width="120">Aksi</th>

	                        </tr>

	                    </thead>

	                    <tbody>

							<?php $no = 1; foreach($karyawan as $row){?>

	                        <tr>

	                        	<td><?php echo $no;?></td>

	                        	<td><?php echo $row->nid;?></td>

	                        	<td><?php echo $row->nama;?></td>

	                        	<td><?php echo $row->hp;?></td>

                                <td><?php echo $row->divisi;?></td>

                                <td><?php echo $row->jabatan;?></td>

	                        	<td class="td-actions">

									<a data-toggle="modal" href="#editModal"class="btn btn-primary btn-small" onclick="edit(<?php echo $row->id_kary;?>)"><i class="btn-icon-only icon-pencil"> </i></a>

									<a onclick="return confirm('Apakah Anda Yakin?');" class="btn btn-danger btn-small" href="<?php echo base_url();?>data/karyawan/del_karyawan/<?php echo $row->id_kary;?>"><i class="btn-icon-only icon-remove"> </i></a>

								</td>

	                        </tr>

							<?php $no++; } ?>

	                    </tbody>

	               	</table>

				</div>

			</div>

		</div>

	</div>

</div>



<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">

    <div class="modal-dialog">

        <div class="modal-content">

            <div class="modal-header">

                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>

                <h4 class="modal-title">Tambah Data Karyawan</h4>

            </div>

            <form class ='form-horizontal' action="<?php echo base_url();?>data/karyawan/save_karyawan" method="post" enctype="multipart/form-data">

                <div class="modal-body" style="margin-left: -30px;">    

                    <div class="control-group" id="">

                        <label class="control-label">Tipe Identitas</label>

                        <div class="controls">

                            <input type="radio" name="tipe" value="1" required/> NIK

                            <input type="radio" name="tipe" value="2" required/> NIDN

                            <input type="radio" name="tipe" value="3" required/> KTP

                            <input type="radio" name="tipe" value="4" required/> SIM

                            <input type="radio" name="tipe" value="5" required/> Paspor

                        </div>

                    </div>

                    <div class="control-group" id="">

                        <label class="control-label">Nomor Identitas</label>

                        <div class="controls">

                            <input type="text" class="span4" name="nik" placeholder="Input Nomor Identitas" class="form-control" value="" required/>

                        </div>

                    </div>

                    <div class="control-group" id="">

                        <label class="control-label">Nama</label>

                        <div class="controls">

                            <input type="text" class="span4" name="nama" placeholder="Input Nama" class="form-control" value="" required/>

                        </div>

                    </div>

                    <div class="control-group" id="">

                        <label class="control-label">Jenis Kelamin</label>

                        <div class="controls">

                            <label class="radio inline">

                                <input type="radio" name="jk" value="P" required/>

                                Pria

                            </label>

                            <label class="radio inline">

                                <input type="radio" name="jk" value="W" required/>

                                Wanita

                            </label>

                        </div>

                    </div>

                    <div class="control-group" id="">

                        <label class="control-label">Telepon</label>

                        <div class="controls">

                            +62 ( <input type="text" class="span3" name="telepon" id="telepon" placeholder="Input Telepon" class="form-control" value="" required/> ) 

                        </div>

                    </div>

                    <div class="control-group" id="">

                        <label class="control-label">Email</label>

                        <div class="controls">

                            <input type="text" class="span4" name="email" placeholder="Input Email" class="form-control" value="" required/>

                        </div>

                    </div>

                    <div class="control-group" id="">

                        <label class="control-label">Alamat</label>

                        <div class="controls">

                            <textarea class="span4" name="alamat" placeholder="Input Alamat" class="form-control" value="" required></textarea>

                        </div>

                    </div>

                    <div class="control-group" id="">

                        <label class="control-label">Foto</label>

                        <div class="controls">

                            <input type="file" class="span4" name="foto" class="form-control" required/>

                        </div>

                    </div>

					<script>

					$(document).ready(function(){

    					$('#lembaga').change(function(){

        					$.post('<?php echo base_url();?>data/karyawan/get_listjab/'+$(this).val(),{},function(get){

        					   $('#jabatan').html(get);

        					});

    					});

					});

					</script>

					</script>

					<div class="control-group" id="">

                        <label class="control-label">Divisi</label>

                        <div class="controls">

                            <select class="span4" name="lembaga" id="lembaga" class="form-control" value="" required>

                                <option> -- </option>

								<?php foreach($divisi->result() as $row){?>

								<option value="<?php echo $row->kd_divisi;?>"><?php echo $row->divisi;?></option>

								<?php } ?>

                            </select>

                        </div>

                    </div>

                    <div class="control-group" id="">

                        <label class="control-label">Jabatan</label>

                        <div class="controls">

                            <select class="span4" name="jabatan" id="jabatan" class="form-control" value="" required>                               

                            </select>

                        </div>

                    </div>

                    <div class="control-group" id="">

                        <label class="control-label">User Grup</label>

                        <div class="controls">

                             <select name="group" multiple>
                                <?php foreach ($group as $isi) {
                                    echo "<option value='".$isi->id_user_group."'>".$isi->user_group."</option>";
                                } ?>
                                
                            </select> 

                        </div>

                    </div>

                    <div class="control-group" id="">

                        <label class="control-label">Status</label>

                        <div class="controls">

                            <label class="radio inline">

								<input type="radio" name="status" value="1" required/>

								Aktif

							</label>

							<label class="radio inline">

								<input type="radio" name="status" value="0" required/>

								Tidak Aktif

							</label>

                        </div>

                    </div>

                </div> 

                <div class="modal-footer">

                    <button type="button" class="btn btn-default" data-dismiss="modal">Keluar</button>

                    <input type="submit" class="btn btn-primary" value="Simpan"/>

                </div>

            </form>

        </div><!-- /.modal-content -->

    </div><!-- /.modal-dialog -->

</div><!-- /.modal -->



<div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">

    <div class="modal-dialog">

        <div class="modal-content" id="editkary">

            

        </div><!-- /.modal-content -->

    </div><!-- /.modal-dialog -->

</div><!-- /.modal -->