<div class="row">
  <div class="span12">                
      <div class="widget ">
        <div class="widget-header">
          <i class="icon-user"></i>
          <h3>Data Publikasi</h3>
      </div> <!-- /widget-header -->
      
      <div class="widget-content">
        <div class="span11">
        <form method="post" class="form-horizontal" action="<?php echo base_url()?>data/data_publikasi/simpan_sesi">
              <fieldset>
                <script>
                  $(document).ready(function(){
                    $('#faks').change(function(){
                      $.post('<?php echo base_url()?>data/data_publikasi/get_jurusan/'+$(this).val(),{},function(get){
                        $('#jurs').html(get);
                      });
                    });
                  });
                </script>

                <?php  
                $logged = $this->session->userdata('sess_login');
                $pecah = explode(',', $logged['id_user_group']);
                $jmlh = count($pecah);
                for ($i=0; $i < $jmlh; $i++) { 
                  $grup[] = $pecah[$i];
                }
                ?>

                <?php if ((in_array(10, $grup)) or (in_array(1, $grup))) { ?>
                <div class="control-group">
                  <label class="control-label">Fakultas</label>
                  <div class="controls">
                    <select class="form-control span6" name="fakultas" id="faks">
                      <option>--Pilih Fakultas--</option>
                      <?php foreach ($fakultas as $row) { ?>
                      <option value="<?php echo $row->kd_fakultas;?>"><?php echo $row->fakultas;?></option>
                      <?php } ?>
                    </select>
                  </div>
                </div>
                <div class="control-group">
                  <label class="control-label">Program Studi</label>
                  <div class="controls">
                    <select class="form-control span6" name="prodi" id="jurs">
                      <option>--Pilih Program Studi--</option>
                    </select>
                  </div>
                </div>

                <div class="control-group">
                  <label class="control-label">Semester</label>
                  <div class="controls">
                    <select class="form-control span6" name="semester" id="jurs">
                      <option>--Pilih Semester--</option>
                      <option value="1">1</option>
                      <option value="2">2</option>
                      <option value="3">3</option>
                      <option value="4">4</option>
                      <option value="5">5</option>
                      <option value="6">6</option>
                      <option value="7">7</option>
                      <option value="8">8</option>
                    </select>
                  </div>
                </div>
                              <div class="control-group">
                                <label class="control-label">Tahun Akademik</label>
                                <div class="controls">
                                  <select class="form-control span6" name="tahunajaran" id="tahunajaran">
                                    <option>--Pilih Tahun Akademik--</option>
                                    <?php foreach ($tahunajar as $row) { ?>
                                    <option value="<?php echo $row->kode;?>"><?php echo $row->tahun_akademik;?></option>
                                    <?php } ?>
                                  </select>
                                </div>
                              </div>
                <?php } elseif ((in_array(9, $grup))) { ?>
                <input type="hidden" value="" name="fakultas" />
                <div class="control-group">
                  <label class="control-label">Jurusan</label>
                  <div class="controls">
                    <select class="form-control span6" name="prodi" id="jurs">
                      <option disabled selected>--Pilih Jurusan--</option>
                      <?php foreach ($prodi as $row) { ?>

                      <option value="<?php echo $row->kd_prodi;?>"><?php echo $row->prodi;?></option>

                      <?php } ?>
                    </select>
                  </div>
                </div>
                <div class="control-group">
                  <label class="control-label">Semester</label>
                  <div class="controls">
                    <select class="form-control span6" name="semester" id="jurs">
                      <option>--Pilih Semester--</option>
                      <option value="1">1</option>
                      <option value="2">2</option>
                      <option value="3">3</option>
                      <option value="4">4</option>
                      <option value="5">5</option>
                      <option value="6">6</option>
                      <option value="7">7</option>
                      <option value="8">8</option>
                    </select>
                  </div>
                </div>
                              <div class="control-group">
                                <label class="control-label">Tahun Akademik</label>
                                <div class="controls">
                                  <select class="form-control span6" name="tahunajaran" id="tahunajaran">
                                    <option>--Pilih Tahun Akademik--</option>
                                    <?php foreach ($tahunajar as $row) { ?>
                                    <option value="<?php echo $row->kode;?>"><?php echo $row->tahun_akademik;?></option>
                                    <?php } ?>
                                  </select>
                                </div>
                              </div>
                <!-- <div class="control-group">
                  <label class="control-label">Angkatan</label>
                    <div class="controls">
                      <select class="form-control span6" name="angkatan" id="tahun">
                        <option>--Pilih Angkatan--</option>
                          <?php $tahun= date('Y'); $awal = $tahun-7; for ($i=$awal; $i <= $tahun; $i++) { 
                            echo "<option value='".$i."'>".$i."</option>";
                          }; ?>
                       </select>
                    </div>
                </div> -->
                <?php } elseif ((in_array(8, $grup))) { ?>
                <input type="hidden" value="" name="fakultas" />
                <input type="hidden" value="" name="jurusan" />
                <div class="control-group">
                                <label class="control-label">Tahun Akademik</label>
                                <div class="controls">
                                  <select class="form-control span6" name="tahunajaran" id="tahunajaran">
                                    <option>--Pilih Tahun Akademik--</option>
                                    <?php foreach ($tahunajar as $row) { ?>
                                    <option value="<?php echo $row->kode;?>"><?php echo $row->tahun_akademik;?></option>
                                    <?php } ?>
                                  </select>
                                </div>
                              </div>
                <div class="control-group">
                  <label class="control-label">Semester</label>
                  <div class="controls">
                    <select class="form-control span6" name="semester" id="jurs">
                      <option>--Pilih Semester--</option>
                      <option value="1">1</option>
                      <option value="2">2</option>
                      <option value="3">3</option>
                      <option value="4">4</option>
                      <option value="5">5</option>
                      <option value="6">6</option>
                      <option value="7">7</option>
                      <option value="8">8</option>
                    </select>
                  </div>
                </div>
                <?php } ?>
              <br />
              <div class="form-actions">
                  <input type="submit" class="btn btn-large btn-success" value="Submit"/> 
              </div> <!-- /form-actions -->
            </fieldset>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
