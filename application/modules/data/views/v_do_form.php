<script type="text/javascript">
  $(document).ready(function() {
    $('#tgl_skep').datepicker({dateFormat: 'yy-mm-dd',
                    yearRange: "1945:" + "<?php echo date('Y') ?>",
                    changeMonth: true,
                    changeYear: true
    });
});
</script>

<script type="text/javascript">
  jQuery(document).ready(function($) {
      $('input[name^=npm_mhs]').autocomplete({

          source: '<?php echo base_url('data/dropout/load_mhs_autocomplete');?>',

          minLength: 1,

          select: function (evt, ui) {

              this.form.npm_mhs.value = ui.item.value;

              // this.form.nm_mhs.value = ui.item.nama;

              this.form.tm_mhs.value = ui.item.tahun_masuk;
          }
      });
  });
</script>

<script type="text/javascript">
  function getTable() {
    $.post('<?php echo base_url(); ?>data/dropout/loadTable/', function(data) {
      $('#appearHere').html(data);
      });
    }

  $(function () {
    $('#throw').click(function (e) {
      $.ajax({
        type: 'POST',
        url: '<?= base_url('data/dropout/outtemporary'); ?>',
        data: $('#temporaryform').serialize(),
        error: function (xhr, ajaxOption, thrownError) {
          return false;
        },
        success: function () {
                  getTable();
                  $('#toRemove').hide();
                  $('#npm,#awl,#reason').val('');
        }
      });
      e.preventDefault();
    });
  });

  function rmData(id) {
      //alert(id);
      $.ajax({
          type: 'POST',
          url: '<?= base_url('data/dropout/deleteList/');?>'+id,
          error: function (xhr, ajaxOptions, thrownError) {
              return false;           
          },
          success: function () {
              getTable();
          }
      });
  }
</script>

<div class="row">
  <div class="span12">                
      <div class="widget ">
        <div class="widget-header">
          <i class="icon-user"></i>
          <h3>Form SKEP. Drop Out</h3>
        </div> <!-- /widget-header -->
      
        <div class="widget-content">
          <fieldset>
            <form class="form-horizontal" id="temporaryform" method="post">
              <div class="control-group">
                <label class="control-label">Data SKEP. </label>
                <div class="controls">
                  <input class="form-control span3" type="text" placeholder="Nomor SKEP."  name="no_skep" >
                  <input class="form-control span3" type="text" placeholder="Tanggal SKEP."  name="tgl_skep" id="tgl_skep" >
                  <select class="form-control span3" name="ta_skep" required>
                    <option disabled selected>-- Pilih Tahun Ajaran --</option>
                    <?php foreach ($ta as $key) {
                      echo '<option value="'.$key->kode.'">'.$key->tahun_akademik.'</option>  ';
                    } ?>
                  </select>
                </div>
              </div>
              <br>
              <div class="control-group">
                <label class="control-label">Tambah Mahasiswa</label>
                <div class="controls">
                  <input class="form-control span6" type="text" id="npm" placeholder="Isi NPM Mahasiswa Dropout"  name="npm_mhs" required>
                  <input class="form-control span3" type="text" id="awl" placeholder="Tahun Masuk Mahasiswa"  name="tm_mhs" readonly>
                </div>
              </div>
              <div class="control-group">
                <label class="control-label">Alasan</label>
                <div class="controls">
                  <select class="form-control" name="alasan" id="reason">
                    <option value="1">Mengundurkan diri</option>
                    <option value="2">Dikeluarkan</option>
                    <option value="3">Wafat</option>
                  </select>
                </div>
              </div>
              <div class="control-group">
                <center>
                  <button class="btn btn-success" id="throw"><i class="icon icon-plus"></i> Tambah ke daftar</button>
                </center>
              </div>
              <hr>
            </form>
            <form class="form-horizontal" action="<?php echo base_url(); ?>data/dropout/add" method="post">
              <h3><i class="icon icon-list"></i> Daftar mahasiswa Drop Out</h3>
              <br>
              <table id="example99" class="table table-bordered table-striped">
                <thead>
                      <tr> 
                        <th>No</th>
                        <th>NPM</th>
                        <th>Nama Mahasiswa</th>
                        <th>Angkatan</th>
                        <th>Alasan</th>
                        <th width="80">Aksi</th>
                      </tr>
                  </thead>
                  <tbody id="appearHere">
                      <tr id="toRemove">
                        <td colspan="6"><i>No data available</i></td>
                      </tr>
                  </tbody>
              </table>
              <div class="form-actions">
                <input class="btn btn-default btn-primary" type="submit" value="Simpan SKEP.">
              </div>
            </form>
          </fieldset>    
        </div>
      </div>
  </div>
</div>