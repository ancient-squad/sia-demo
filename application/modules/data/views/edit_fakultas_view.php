            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Edit Data Fakultas</h4>
            </div>
            <form class ='form-horizontal' action="<?php echo base_url();?>data/fakultas/update_fakultas" method="post">
                <div class="modal-body" style="margin-left: -60px;"> 
                    <script type="text/javascript">
                        $(document).ready(function(){
                            $.post('<?php echo base_url()?>data/fakultas/get_list_faks'+$(this).val(),{},function(get){
                                $("#kd_faks").html(get);
                            });
                        });
                    </script>
                    <div class="control-group" id="">
                        <label class="control-label">Kode Fakultas</label>
                        <div class="controls">
                            <input type="hidden" name="id_fakultas" value="<?php echo $list->id_fakultas;?>">
                            <input type="text" id="kd_faks" class="span4" name="kode" placeholder="Input Kode" class="form-control" value="<?php echo $list->kd_fakultas;?>"required/>
                        </div>
                    </div>              
                    <div class="control-group" id="">
                        <label class="control-label">Fakultas</label>
                        <div class="controls">
                            <input type="text" id="nama_faks" class="span4" name="fakultas" placeholder="Input Fakultas" class="form-control" value="<?php echo $list->fakultas?>" required/>
                        </div>
                    </div>
                </div> 
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Keluar</button>
                    <input type="submit" class="btn btn-primary" value="Simpan"/>
                </div>
            </form>