<script>
  function loads(idk){
    $('#contentDetil').load('<?= base_url();?>data/dropout/loaddetil/'+idk);
  }

  function add(id){
    $('#addDetil').load('<?= base_url();?>data/dropout/addFromEdit/'+id);
  }
</script>

<div class="row">
  <div class="span12">                
      <div class="widget ">
        <div class="widget-header">
          <i class="icon-user"></i>
          <h3>Data Drop Out</h3>
      </div> <!-- /widget-header -->
      
      <div class="widget-content">
        <div class="span11">
          <ul class="nav nav-tabs">
              <li class="active"><a data-toggle="tab" href="#home1">SKEP. Drop Out</a></li>
              <li><a data-toggle="tab" href="#home2">Mahasiswa Drop Out</a></li>
          </ul>
          <div class="tab-content">
            <div id="home1" class="tab-pane fade in active">
              <center>
                <b>Data SKEP. Drop Out </i></u></b>
              </center>
              <a href="<?= base_url(); ?>data/dropout/form_skep_do" data-toggle="tooltip" title="Input SKEP Drop Out" class="btn btn-info"><i class="btn-icon-only icon-plus"> </i> INPUT SKEP. </a>
              <hr>
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                      <tr> 
                        <th>No</th>
                        <th>No. SKEP.</th>
                        <th>Tanggal SKEP. </th>
                        <!-- <th>Tahun Ajaran</th> -->
                        <th>Jumlah Mahasiswa</th>
                        <th width="157">Aksi</th>
                      </tr>
                  </thead>
                  <tbody>
                    <?php $no = 1; foreach ($list as $key) { ?>
                      <tr>
                        <td><?= $no; ?></td>
                        <td><?= $key->skep; ?></td>
                        <td><?= $key->tgl_skep; ?></td>
                        <!-- <td><?= $key->tahunajaran; ?></td> -->
                        <td><?= $key->jml; ?></td>
                        <td>
                          <button class="btn btn-info btn-sm" data-target="#detilModal" data-toggle="modal" onclick="loads('<?= $key->key_skep; ?>')">
                            <i class="icon icon-eye-open"></i>
                          </button>
                          <button class="btn btn-success btn-sm" data-target="#addModal" data-toggle="modal" onclick="add('<?= $key->key_skep; ?>')">
                            <i class="icon icon-plus"></i>
                          </button>
                        </td>
                      </tr>
                    <?php $no++; } ?>
                  </tbody>
              </table>
            </div>
            <div id="home2" class="tab-pane fade in">
              <b><center>Data Mahasiswa Drop Out</center></b><br>
              <form method="post" class="form-horizontal" action="<?= base_url(); ?>data/dropout/view" target="_blank">
              <fieldset>
                <div id="jurusan" class="control-group">
                  <label class="control-label">Prodi</label>
                  <div class="controls">
                    <?php if ($this->session->userdata('sess_login')['id_user_group'] == '26'): ?>
                      <select class="form-control span6" name="jurusan" id="jurs">
                        <option selected>--Pilih Program Studi--</option>
                        <?php foreach ($jurusan as $key) : ?>
                          <option value="<?= $key->kd_prodi; ?>"><?= $key->prodi; ?></option>
                        <?php endforeach; ?>
                      </select>
                    <?php else: ?>
                      <select class="form-control span6" name="jurusan" id="jurs" disabled>
                        <option selected>--Pilih Program Studi--</option>
                        <?php $logged = $this->session->userdata('sess_login');
                        foreach ($jurusan as $key) : ?>
                          <option value="<?= $key->kd_prodi; ?>" <?= $key->kd_prodi == $logged['userid'] ? "selected" : ''; ?> ><?= $key->prodi; ?></option>
                        <?php endforeach; ?>
                        <input type="hidden" name="jurusan" value="<?= $logged['userid']; ?>">
                      </select>
                    <?php endif ?>
                  </div>
                </div>
                <div id="tahun" class="control-group">
                  <label class="control-label">Jenis Filter</label>
                  <div class="controls">
                    <select class="form-control span6" name="jfilter" id="jf" required>
                      <option disabled selected>--Pilih Jenis Filter--</option>
                      <option value="1">Tahun Akademik Dikeluarkan</option>
                      <option value="2">Angkatan Mahasiswa</option>
                    </select>
                  </div>
                </div>
                <script type="text/javascript">
                  $(document).ready(function(){
                      $('#jf').change(function(){
                        $.post('<?= base_url();?>data/Dropout/get_filter_do/'+$(this).val(),{},function(get){
                          $('#fl').html(get);
                        });
                      });
                    });
                </script>

                <div id="tahun" class="control-group">
                  <label class="control-label">Filter</label>
                  <div class="controls">
                    <select class="form-control span6" name="filter" id="fl" required>
                      <option disabled selected>--Pilih Filter--</option>
                    </select>
                  </div>
                </div>    
              <br/>
              <div class="form-actions">
                  <input type="submit" class="btn btn-large btn-success" value="Cari"/> 
              </div> <!-- /form-actions -->
                </fieldset>
            </form>
          </div>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="detilModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content" id="contentDetil">
            
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div class="modal fade" id="addModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content" id="addDetil">
            
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->