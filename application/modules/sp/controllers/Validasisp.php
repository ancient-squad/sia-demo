<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Validasisp extends MY_Controller {

    function __construct()
    {
        parent::__construct();
 
        if ($this->session->userdata('sess_login') == TRUE) {
            $user = $this->session->userdata('sess_login');
            $akses = $this->role_model->cekakses(134)->result();
            if ($akses != TRUE) {
                echo "<script>alert('Akses Tidak Diizinkan');document.location.href='".base_url()."home';</script>";
                exit();
            }
        } else {
            redirect('auth','refresh');
        }
    }

	function index()
	{
		$data['select'] = $this->db->where('kode =', getactyear())->get('tbl_tahunakademik')->result();
		$data['page'] = 'validasi_select';
		$this->load->view('template/template', $data);
	}

	function session_create()
    {
		$tahun = $this->input->post('tahunajaran');
		$this->session->set_userdata('ta',$tahun);
		redirect(base_url().'sp/validasisp/list_mk','refresh');
	}

	function list_mk()
    {
		$user = $this->session->userdata('sess_login');
		$data['ta'] = $this->session->userdata('ta');
        $prodi = $user['userid'];
		$data['pro']  = $prodi;
        $data['mhs'] = $this->db->query("SELECT vkrs.`npm_mahasiswa`,mhs.`NMMHSMSMHS`,vkrs.`jumlah_sks`,krs.`kd_krs`,
                                SUM(IF(krs.`kd_jadwal` != '',mk.`sks_matakuliah`,0)) AS jml_open
                                FROM tbl_verifikasi_krs_sp vkrs 
                                JOIN tbl_krs_sp krs ON vkrs.`kd_krs` = krs.`kd_krs`
                                JOIN tbl_mahasiswa mhs ON mhs.`NIMHSMSMHS` = vkrs.`npm_mahasiswa`
                                JOIN tbl_matakuliah mk ON mk.`kd_matakuliah` = krs.`kd_matakuliah`
                                WHERE vkrs.`tahunajaran` = '".$this->session->userdata('ta')."' AND kd_jurusan = '".$prodi."' 
                                AND mk.`kd_prodi` = '".$prodi."'
                                GROUP BY vkrs.`kd_krs`")->result();

		$data['rows'] = $q = $this->db->query('SELECT distinct mk.audit_user,mk.kd_jadwal,mk.id_jadwal,mk.open,mk.kelas,mk.kd_matakuliah,mk.hari,mk.waktu_mulai,mk.waktu_selesai,km.semester_kd_matakuliah as smtr,a.nama_matakuliah,a.sks_matakuliah,r.*,kry.nama FROM tbl_jadwal_matkul_sp mk
                                JOIN tbl_matakuliah a ON mk.`kd_matakuliah`=a.`kd_matakuliah`
                                left JOIN tbl_ruangan r ON r.`id_ruangan` = mk.`kd_ruangan`
                                JOIN tbl_fakultas b ON a.`kd_fakultas` = b.`kd_fakultas`
                                LEFT JOIN tbl_karyawan kry ON kry.`nid` = mk.`kd_dosen`
                                JOIN tbl_kurikulum_matkul_new km ON mk.`kd_matakuliah` = km.`kd_matakuliah`
                                WHERE mk.`kd_jadwal` LIKE "'.$prodi.'%"
                                AND a.`kd_prodi` = "'.$prodi.'"
                                AND km.`kd_kurikulum` LIKE "%'.$prodi.'%"
                                AND mk.`kd_tahunajaran` = "'.$this->session->userdata('ta').'"
                                order by mk.kd_matakuliah ASC')->result();

		$data['page'] = 'list_mk_sp';
		$this->load->view('template/template', $data);
	}

	function list_mhs($value,$id_jdl)
    {
        $this->session->set_userdata('id_jdl',$id_jdl);
        $this->session->set_userdata('kd_mk',$value);

		$user = $this->session->userdata('sess_login');
		$ta = $this->session->userdata('ta');

        $data['status'] = $this->db->where('id_jadwal',$id_jdl)->get('tbl_jadwal_matkul_sp')->row();
		
		$data['rows'] = $this->db->query('SELECT mhs.`NIMHSMSMHS`,mhs.`NMMHSMSMHS`,ksp.`kd_krs`,ksp.`id_krs` 
                                    FROM tbl_krs_sp ksp 
									JOIN tbl_mahasiswa mhs ON ksp.`npm_mahasiswa` = mhs.`NIMHSMSMHS`
									WHERE ksp.`kd_jadwal` = "'.$data['status']->kd_jadwal.'"
									')->result();

        $data['id_jdl'] = $id_jdl;
        $data['kd_mk'] = (string) $value;
        $data['tahunajaran'] = $this->session->userdata('ta');        

        $data['nm_mk'] = get_nama_mk($value,$user["userid"]);

		$data['page'] = 'list_mhs_sp';
		$this->load->view('template/template', $data);
	}

	function penugasan($id)
    {
		$user = $this->session->userdata('sess_login');

		$data['mk']  = $this->db->where('id_matakuliah',$id)->get('tbl_matakuliah',1)->row();
		$data['jur'] = $user['userid'];
		$data['gedung'] = $this->db->get('tbl_gedung')->result();
		$this->load->view('penugasan',$data);
	}

    function verifikasi($id)
    {
        $data['id_jadwal'] = $id;
        $this->load->view('app_kls',$data);
    }

    function app_mhs($id)
    {
        $data['kd_krs'] = $id;
        $this->load->view('app_mhs',$data);
    }

    function save_open()
    {
        $id = $this->input->post('id_jadwal');
        $data = array('open' => $this->input->post('open_kelas'));

        $this->db->where('id_jadwal',$id)->update('tbl_jadwal_matkul_sp', $data);
        redirect(base_url().'sp/validasisp/list_mk/','refresh');
    }

    function save_open_mhs()
    {
        $id = $this->input->post('kd_krs');
        $data = array('flag_open' => $this->input->post('open_mhs'));

        $this->db->where('id_krs',$id)->update('tbl_krs_sp', $data);
        redirect(base_url().'sp/validasisp/list_mhs/'.$this->session->userdata('kd_mk').'/'.$this->session->userdata('id_jdl'),'refresh');
    }

    function penugasan1($id)
    {
        $user = $this->session->userdata('sess_login');

        $this->db->select('*');
        $this->db->from('tbl_jadwal_matkul_sp jdl');
        $this->db->join('tbl_matakuliah mk', 'mk.kd_matakuliah = jdl.kd_matakuliah', 'left');
        $this->db->join('tbl_karyawan kry', 'kry.nid = jdl.kd_dosen', 'left');
        $data['jur'] = $user['userid'];
        $data['gedung'] = $this->db->get('tbl_gedung')->result();
        $this->load->view('penugasan_isi',$data);
    }

    function update_status_kelas()
    {
        $user = $this->session->userdata('sess_login');
        $sts = $this->input->post('status');
        $kd_mk = $this->input->post('kd_mk');
        $id_jdl = $this->input->post('id_jdl');

        $this->db->select('id_krs');
        $this->db->from('tbl_verifikasi_krs_sp vkrs');
        $this->db->join('tbl_krs_sp krs', 'vkrs.kd_krs = krs.kd_krs');
        $this->db->where('vkrs.kd_jurusan', $user['userid']);
        $this->db->where('vkrs.tahunajaran', $this->session->userdata('ta'));
        $this->db->where('krs.kd_matakuliah', $kd_mk);
        $jdl_up=$this->db->get()->result();

        $update_jdl = array('flag_open' => $sts);

        foreach ($jdl_up as $var) {
            $this->db->where('id_krs', $var->id_krs);
            $this->db->update('tbl_krs_sp', $update_jdl);
        }

        redirect(base_url().'sp/validasisp/list_mhs/'.$kd_mk.'/'.$id_jdl,'refresh');

    }

	function save_jdl_sp()
    {
		date_default_timezone_set("Asia/Jakarta"); 
    	extract(PopulateForm());

    	$user = $this->session->userdata('sess_login');
        $q = $this->db->where('id_matakuliah', $id_mk)->get('tbl_matakuliah',1)->row();
        $w = ($q->sks_matakuliah)*50;
        $kd_mk = $q->kd_matakuliah;
        $session = $this->session->userdata('sess_login');
        $prodi = $session['userid'];
        $kd_prodi = $session['userid'];

        $gpass = NULL;
        $n = 6; // jumlah karakter yang akan di bentuk.
        $chr = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvqxyz0123456789";

        for ($i = 0; $i < $n; $i++) {
            $rIdx = rand(1, strlen($chr));
            $gpass .=substr($chr, $rIdx, 1);
        }
             
        $cc = $this->db->query('SELECT count(kd_jadwal) as jml FROM tbl_jadwal_matkul_sp WHERE kd_jadwal LIKE "'.$kd_prodi.'%" and kd_tahunajaran = '.$this->session->userdata('ta').'')->row();
        $hasil = $cc->jml;
        if($hasil==0)
        {
            $hasilakhir="".$kd_prodi."/".$gpass."/001";
            $kelas = 'SP01';
        }
        elseif($hasil < 10){
            $hasilakhir="".$kd_prodi."/".$gpass."/00".($hasil+1);
            $kelas = 'SP0'.($hasil+1);
        }
        elseif($hasil < 100){
            $hasilakhir="".$kd_prodi."/".$gpass."/0".($hasil+1);
            $kelas = 'SP'.($hasil+1);
        }
        elseif ($hasil < 1000){
            $hasilakhir="".$kd_prodi."/".$gpass."/".($hasil+1);
        }elseif ($hasil < 10000){
            $hasilakhir="".$kd_prodi."/".$gpass."/".($hasil+1);
        }

        //edit danu
        $time2 = strtotime($jam_masuk) + $w*60;
        $jumlah_time = date('H:i', $time2);
        $session = $this->session->userdata('sess_login');
        $user = $session['userid'];

        $cekpemakaianruangan = $this->db->query("select * from tbl_jadwal_matkul where (waktu_selesai > '".$jam_masuk."' and waktu_mulai < '".$jumlah_time."') and kd_ruangan = '".$ruangan."' and hari = ".$hari_kuliah." and kd_tahunajaran = '".$this->session->userdata('tahunajaran')."'")->result();
        $cekpemakaianruangan_sp = $this->db->query("select * from tbl_jadwal_matkul_sp where (waktu_selesai > '".$jam_masuk."' and waktu_mulai < '".$jumlah_time."') and kd_ruangan = '".$ruangan."' and hari = ".$hari_kuliah." and kd_tahunajaran = '".$this->session->userdata('tahunajaran')."'")->result();
        

        if (count($cekpemakaianruangan) > 0) {
            echo "<script>alert('Ruangan Sudah Digunakan Pada Waktu Tersebut');history.go(-1);</script>";
        }elseif (count($cekpemakaianruangan_sp) > 0) {
        	echo "<script>alert('Ruangan Sudah Digunakan Pada Waktu Tersebut');history.go(-1);</script>";
    	}else {
            $this->db->select('id_krs');
            $this->db->from('tbl_verifikasi_krs_sp vkrs');
            $this->db->join('tbl_krs_sp krs', 'vkrs.kd_krs = krs.kd_krs');
            $this->db->where('vkrs.kd_jurusan', $prodi);
            $this->db->where('vkrs.tahunajaran', $this->session->userdata('ta'));
            $this->db->where('krs.kd_matakuliah', $kd_mk);
            $jdl_up=$this->db->get()->result();

            $update_jdl = array('kd_jadwal' => $hasilakhir, 'flag_open' => 1);

            foreach ($jdl_up as $var) {
                $this->db->where('id_krs', $var->id_krs);
                $this->db->update('tbl_krs_sp', $update_jdl);
            }

            $data = array(
                'kelas'         => $kelas,
                'kd_dosen'      => $kd_dosen,
                'kd_jadwal'     => $hasilakhir,
                'hari'          => $hari_kuliah,
                'kd_matakuliah' => $kd_mk,
                'kd_ruangan'    => $ruangan,
                'waktu_mulai'   => $jam_masuk,
                'waktu_selesai' => $jumlah_time,
                'kd_tahunajaran'=> $this->session->userdata('ta'),
                'audit_date'    => date('Y-m-d H:i:s'),
                'audit_user'    => $user
            );

            $this->db->insert('tbl_jadwal_matkul_sp', $data);
            redirect('sp/validasisp/list_mk','refresh');
        }
	}

	function delete_mk($id)
    {
		$this->db->query('DELETE from tbl_krs_sp WHERE kd_matakuliah = "'.$id.'" AND kd_krs LIKE CONCAT(npm_mahasiswa,"20153%")');
		redirect(base_url().'sp/validasisp/list_mk','refresh');
	}

    function cetak_absensi($id)
    {
        $this->load->library('Cfpdf');

        $user = $this->session->userdata('sess_login');
        $nik = $user['userid'];

        $data['rows'] = $this->db->query('SELECT * FROM tbl_jadwal_matkul_sp a
                                            JOIN tbl_matakuliah b ON a.`kd_matakuliah` = b.`kd_matakuliah` 
                                            JOIN tbl_karyawan c ON c.`nid` = a.`kd_dosen`
                                            LEFT JOIN tbl_ruangan d ON d.`id_ruangan` = a.`kd_ruangan`
                                            WHERE a.`id_jadwal` = '.$id.'')->row();

        $kodeprodi = substr($data['rows']->kd_jadwal, 0,5);

        $data['titles'] = $this->db->query('SELECT * FROM tbl_jurusan_prodi b
                                            JOIN tbl_fakultas c ON b.`kd_fakultas` = c.`kd_fakultas`
                                            WHERE b.`kd_prodi` = "'.$kodeprodi.'"')->row();

        $data['mhs'] = $this->db->query('SELECT DISTINCT mhs.`NIMHSMSMHS`,mhs.`NMMHSMSMHS`,b.kd_jadwal FROM tbl_krs_sp b
                                            JOIN tbl_mahasiswa mhs ON b.`npm_mahasiswa` = mhs.`NIMHSMSMHS`
                                            JOIN tbl_sinkronisasi_renkeu renkeu ON renkeu.`npm_mahasiswa` = mhs.`NIMHSMSMHS`
                                            WHERE b.kd_jadwal = "'.$data['rows']->kd_jadwal.'" AND renkeu.`tahunajaran` = SUBSTRING(b.`kd_krs` FROM 13 FOR 5) ORDER BY mhs.NIMHSMSMHS ASC;')->result();

        if ($user['userid'] == '74101' || $user['userid'] == '61101') {
            $this->load->view('cetak_absen_s2',$data);
        } else {
            $this->load->view('cetak_absen',$data);
        }
        
    }

	function get_lantai($id)
    {
		$data = $this->app_model->get_lantai($id);
		$list = "<option> -- </option>";
		foreach($data as $row){
            $list .= "<option value='".$row->id_lantai."'>".$row->lantai."</option>";
		}
		die($list);
	}

	function get_ruangan($id)
    {
		$data = $this->app_model->get_ruang($id);
		$list = "<option> -- </option>";
		foreach($data as $row){
            $list .= "<option value='".$row->id_ruangan."'>".$row->kode_ruangan."</option>";
		}
		die($list);
	}

    function cetak_list_mk()
    {
        $user = $this->session->userdata('sess_login');

        $data['ta']   = $this->session->userdata('ta');
        $data['pro']   = $user['userid'];

        $this->load->library('excel');

        $data['rows'] = $this->db->query('SELECT distinct mk.audit_user,mk.kd_jadwal,mk.id_jadwal,mk.open,mk.kelas,mk.kd_matakuliah,mk.hari,mk.waktu_mulai,mk.waktu_selesai,km.semester_kd_matakuliah as smtr,a.nama_matakuliah,a.sks_matakuliah,r.*,kry.nama FROM tbl_jadwal_matkul_sp mk
                                JOIN tbl_matakuliah a ON mk.`kd_matakuliah`=a.`kd_matakuliah`
                                left JOIN tbl_ruangan r ON r.`id_ruangan` = mk.`kd_ruangan`
                                JOIN tbl_fakultas b ON a.`kd_fakultas` = b.`kd_fakultas`
                                LEFT JOIN tbl_karyawan kry ON kry.`nid` = mk.`kd_dosen`
                                JOIN tbl_kurikulum_matkul km ON mk.`kd_matakuliah` = km.`kd_matakuliah`
                                WHERE mk.`kd_jadwal` LIKE "'.$user['userid'].'%"
                                AND a.`kd_prodi` = "'.$user['userid'].'"
                                AND km.`kd_kurikulum` LIKE "'.$user['userid'].'%"
                                AND mk.`kd_tahunajaran` = "'.$this->session->userdata('ta').'"
                                order by mk.kd_matakuliah ASC')->result();
    
        $this->load->view('welcome/print/phpexcel_sp_list_mk', $data);
    }

	function getdosen()
    {
        $this->db->distinct();
        $this->db->select("a.id_kary,a.nid,a.nama");
        $this->db->from('tbl_karyawan a');
        $this->db->like('a.nama', $_GET['term'], 'both');
        $this->db->or_like('a.nid', $_GET['term'], 'both');
        $sql  = $this->db->get();
        $data = array();

        foreach ($sql->result() as $row) {
            $data[] = array(
                            'id_kary'       => $row->id_kary,
                            'nid'           => $row->nid,
                            'value'         => $row->nid.' - '.$row->nama
                            );
        }
        echo json_encode($data);
    }
    

    function print_formulir($kode)
    {
        $this->load->library('Cfpdf');
        $log = $this->session->userdata('sess_login');
        $data['prodi'] = $log['userid'];
        $data['tahun'] = '2016/2017';
        $data['smtr'] = $this->app_model->getdetail('tbl_mahasiswa','NIMHSMSMHS',substr($kode, 0,12),'NIMHSMSMHS','asc')->row();

        $npm = substr($kode, 0,12);

        $a = substr($kode, 16,1);

        if ($a = 3) {
            $data['ganjilgenap'] = 'Perbaikan';
        }elseif ($a = 4) {
            $data['ganjilgenap'] = 'Khusus';
        }
        
        $this->db->select('a.NIMHSMSMHS,a.NMMHSMSMHS,a.TAHUNMSMHS,b.prodi,a.KDPSTMSMHS');
        $this->db->from('tbl_mahasiswa a');
        $this->db->join('tbl_jurusan_prodi b', 'a.KDPSTMSMHS = b.kd_prodi');
        $this->db->where('NIMHSMSMHS', $npm);
        $data['info_mhs'] = $this->db->get()->row();

        $this->db->where('npm_mahasiswa', $npm);
        $this->db->order_by('id_verifikasi', 'desc');
        $dsn = $this->db->get('tbl_verifikasi_krs', 1)->row()->id_pembimbing;

        $this->db->where('nid', $dsn);
        $data['dosen'] = $this->db->get('tbl_karyawan',1)->row();

        $qq=$this->app_model->get_KDPSTMSMHS($npm)->row();

        $data['matkul'] = $this->db->query('select distinct * from tbl_krs_sp b join tbl_verifikasi_krs_sp a on a.kd_krs = b.kd_krs
        join tbl_matakuliah c on b.kd_matakuliah = c.kd_matakuliah
        join tbl_jadwal_matkul_sp d on d.kd_jadwal = b.kd_jadwal
        join tbl_karyawan e on e.nid = d.kd_dosen 
        where b.kd_krs = "'.$kode.'" AND c.kd_prodi = "'.$qq->KDPSTMSMHS.'"')->result();

        $this->load->view('akademik/espe', $data);

    }

    function print_krssp($kode)
    {
        $this->load->library('Cfpdf');
        $log = $this->session->userdata('sess_login');
        $data['prodi'] = $log['userid'];
        $data['tahun'] = '2016/2017';
        $data['smtr'] = $this->app_model->getdetail('tbl_mahasiswa','NIMHSMSMHS',substr($kode, 0,12),'NIMHSMSMHS','asc')->row();
        $npm = substr($kode, 0,12);
        $a = substr($kode, 16,1);
        if ($a = 3) {
            $data['ganjilgenap'] = 'Perbaikan';
        }elseif ($a = 4) {
            $data['ganjilgenap'] = 'Khusus';
        }
        
        $this->db->select('a.NIMHSMSMHS,a.NMMHSMSMHS,a.TAHUNMSMHS,b.prodi');
        $this->db->from('tbl_mahasiswa a');
        $this->db->join('tbl_jurusan_prodi b', 'a.KDPSTMSMHS = b.kd_prodi');
        $this->db->where('NIMHSMSMHS', $npm);
        $data['info_mhs'] = $this->db->get()->row();

        $this->db->where('npm_mahasiswa', $npm);
        $this->db->order_by('id_verifikasi', 'desc');
        $dsn = $this->db->get('tbl_verifikasi_krs', 1)->row()->id_pembimbing;

        $this->db->where('nid', $dsn);
        $data['dosen'] = $this->db->get('tbl_karyawan',1)->row();

        $qq=$this->app_model->get_KDPSTMSMHS($npm)->row();

        $data['matkul'] = $this->db->query('select distinct * from tbl_krs_sp b join tbl_verifikasi_krs_sp a on a.kd_krs = b.kd_krs
        join tbl_matakuliah c on b.kd_matakuliah = c.kd_matakuliah
        join tbl_jadwal_matkul_sp d on d.kd_jadwal = b.kd_jadwal
        join tbl_karyawan e on e.nid = d.kd_dosen 
        where b.kd_krs = "'.$kode.'" AND c.kd_prodi = "'.$qq->KDPSTMSMHS.'"')->result();

        $this->load->view('akademik/espe_formulir', $data);
    }

}

/* End of file Validasisp.php */
/* Location: ./application/modules/sp/controllers/Validasisp.php */