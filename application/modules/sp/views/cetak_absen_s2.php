<?php

$pdf = new FPDF("L","mm", "A4");

$pdf->AliasNbPages();

$pdf->AddPage();





$pdf->SetMargins(3, 3 ,0);
$pdf->SetAutoPageBreak(TRUE, 3);

$pdf->SetFont('Arial','B',10); 



//$pdf->Image(''.base_url().'assets/img/logo-stkip-bima.png',60,30,90);

$pdf->Ln(0);

$pdf->Cell(200,5,strtoupper($titles->prodi),0,0,'L');

$pdf->Ln(4);

$pdf->SetFont('Arial','',10); 

$pdf->Cell(200,5,''.strtoupper($titles->fakultas).' - '.$this->ORG_NAME,0,0,'L');

$pdf->Ln(4);



$pdf->SetFont('Arial','',6);

$pdf->Cell(13,3,'KAMPUS I',0,0,'L');

$pdf->Cell(3,3,' : ',0,0,'L');

$pdf->Cell(140,3,'Jl. piere tendean,Mpunda, Nusa Tenggara Bar., Mande, Mpunda, Bima, Nusa Tenggara Bar. 84111, Indonesia',0,0,'L');

$pdf->Ln(2);



$pdf->SetFont('Arial','',6);

$pdf->Cell(13,3,'',0,0,'L');

$pdf->Cell(3,3,' : ',0,0,'L');

$pdf->Cell(140,3,'',0,0,'L');

$pdf->Ln(4);

$pdf->Cell(290,0,'',1,0,'C');



$pdf->ln(4);

$pdf->SetFont('Arial','',8);

$pdf->Cell(15,5,'KODE MK',0,0,'L');

$pdf->Cell(2,5,':',0,0,'L');

$pdf->Cell(60,5,$rows->kd_matakuliah ,0,0,'L');

$pdf->Cell(15,5,'Smtr/Thn',0,0,'L');

if (substr($rows->kd_tahunajaran, 4) == 1) {
	$ta = 'Ganjil';
} else {
	$ta = 'Genap';
}

$pdf->Cell(2,5,':',0,0,'L');

$pdf->Cell(50,5,$rows->semester_matakuliah.' / '.substr($rows->kd_tahunajaran, 0, 4).' - '.$ta,0,0,'L');

$pdf->Cell(20,5,'NAMA DOSEN',0,0,'L');

$pdf->Cell(2,5,':',0,0,'L');

$pdf->Cell(50,5,$rows->nama,0,0,'L');

$pdf->Cell(20,5,'KELAS',0,0,'L');

$pdf->Cell(2,5,':',0,0,'L');

$pdf->Cell(50,5,$rows->kelas,0,0,'L');

$pdf->ln(4);

$pdf->SetFont('Arial','',8);

$pdf->Cell(15,5,'NAMA MK',0,0,'L');

$pdf->Cell(2,5,':',0,0,'L');

$pdf->Cell(60,5,$rows->nama_matakuliah,0,0,'L');

$pdf->Cell(15,5,'SKS',0,0,'L');

$pdf->Cell(2,5,':',0,0,'L');

$pdf->Cell(50,5,$rows->sks_matakuliah,0,0,'L');

$pdf->Cell(20,5,'NID',0,0,'L');

$pdf->Cell(2,5,':',0,0,'L');

$pdf->Cell(50,5,$rows->kd_dosen,0,0,'L');

$pdf->Cell(20,5,'Kuota',0,0,'L');

$pdf->Cell(2,5,':',0,0,'L');

$pdf->Cell(50,5,$rows->kuota,0,0,'L');

$pdf->ln(4);

$pdf->SetFont('Arial','',8);

$pdf->Cell(15,5,'RUANG',0,0,'L');

$pdf->Cell(2,5,':',0,0,'L');

$pdf->Cell(60,5,$rows->kode_ruangan,0,0,'L');

$pdf->Cell(15,5,'WAKTU',0,0,'L');

$pdf->Cell(2,5,':',0,0,'L');

$pdf->Cell(50,5,notohari($rows->hari).' / '.del_ms($rows->waktu_mulai).' - '.del_ms($rows->waktu_selesai),0,0,'L');

$pdf->Cell(20,5,'KAMPUS',0,0,'L');

$pdf->Cell(2,5,':',0,0,'L');

$pdf->Cell(50,5,'Bekasi',0,0,'L');

$pdf->Cell(20,5,'Jumlah Peserta',0,0,'L');

$pdf->Cell(2,5,':',0,0,'L');

$jumlah = $this->db->query("select count(npm_mahasiswa) as mhs from tbl_krs_sp where kd_jadwal = '".$rows->kd_jadwal."'")->row()->mhs;

$pdf->Cell(50,5,$jumlah.' orang',0,0,'L');




$pdf->ln(10);

$pdf->SetLeftMargin(5);

$pdf->SetFont('Arial','',12);

$pdf->Cell(288,8,'DAFTAR HADIR PESERTA KULIAH',1,0,'C');

$pdf->ln(8);



$pdf->SetFont('Arial','',8);

$pdf->Cell(8,10,'NO','L,T,R,B',0,'C');

$pdf->Cell(25,10,'NPM','L,T,R,B',0,'C');

$pdf->Cell(80,10,'NAMA','L,T,R,B',0,'C');

$pdf->Cell(175,5,'PERTEMUAN KULIAH','L,T,R,B',0,'C');

$pdf->ln(5);

$pdf->Cell(8,0,'',0,0,'C');

$pdf->Cell(25,0,'',0,0,'C');

$pdf->Cell(80,0,'',0,0,'C');



for ($i=1; $i < 8; $i++) { 

	if ($i < 3) {
			$pdf->Cell(25,5,$i,1,0,'C');
		} else {
			$pdf->Cell(25,5,'',1,0,'C',1);
		}		

}

$no=1;
$noo=1;

foreach ($mhs as $key) {
	
	$pdf->ln(5);

	$pdf->Cell(8,5,$no,1,0,'C');

	$pdf->Cell(25,5,$key->NIMHSMSMHS,1,0,'C');

	$pdf->Cell(80,5,$key->NMMHSMSMHS,1,0,'L');

	for ($i=1; $i < 8; $i++) { 
		//$absen = $this->db->query("select * from tbl_absensi_mhs where npm_mahasiswa = '".$key->NIMHSMSMHS."' and kd_jadwal = '".$key->kd_jadwal."' and pertemuan = ".$i." ")->result();
		
		if ($i < 3) {
			$pdf->Cell(25,5,'',1,0,'C');
		} else {
			$pdf->Cell(25,5,'',1,0,'C',1);
		}	
		/*if ($absen == true) {
			$pdf->Cell(11,5,'v',1,0,'C');
		} else {
			$pdf->Cell(11,5,'-',1,0,'C');
		}*/
	}
/*$jumlahabsen = $this->db->query("select distinct pertemuan from tbl_absensi_mhs where kd_jadwal = '".$key->kd_jadwal."' ")->result();
$angka = 1;
foreach ($jumlahabsen as $value) {
	$absen = $this->db->query("select * from tbl_absensi_mhs where npm_mahasiswa = '".$key->NIMHSMSMHS."' and kd_jadwal = '".$key->kd_jadwal."' and pertemuan = ".$angka." ")->result();
	if ($absen == true) {
		$pdf->Cell(11,5,'x',1,0,'C');
	} else {
		$pdf->Cell(11,5,'-',1,0,'C');
	}
	
	$angka++;
}

	$sisa =  16 - $angka;

	if ($sisa <= 16) {
		for ($i=0; $i <= $sisa; $i++) { 
			$pdf->Cell(11,5,'',1,0,'C');
		}
	}*/

	$no++;
	$noo++;

	if ($noo == 21) {
		$pdf->ln(5);

		$pdf->Cell(113,5,'Tanggal Kuliah',1,0,'R');
		$pdf->SetFont('Arial','',7);
		for ($i=1; $i < 8; $i++) { 
			//$absen = $this->db->query("select tanggal from tbl_absensi_mhs where kd_jadwal = '".$rows->kd_jadwal."' and pertemuan = ".$i." ")->row();
			$pdf->Cell(25,5,'',1,0,'C');
			// if ($absen == true) {
			// 	$pdf->Cell(11,5,date("d/m/y", strtotime($absen->tanggal)),1,0,'C');
			// } else {
			// 	$pdf->Cell(11,5,'-',1,0,'C');
			// }
		}
		$pdf->SetFont('Arial','',8);

		$pdf->ln(5);

		$pdf->Cell(113,5,'Jumlah Hadir',1,0,'R');



		for ($i=1; $i < 8; $i++) { 
				$pdf->Cell(25,5,'',1,0,'C');
		}



		$pdf->ln(5);

		$pdf->Cell(113,5,'Paraf Dosen',1,0,'R');



		for ($i=0; $i < 8; $i++) { 

			$pdf->Cell(25,5,'','L,R,B',0,'C');	

		}

		$pdf->ln(8);

		$pdf->Cell(110,5,'',0,0,'C');
		$pdf->Cell(30,5,'Biro Administrasi Akademik',0,0,'C');
		$x = $pdf->GetX();
    	$y = $pdf->GetY();
		//$pdf->image(base_url().'/assets/ttd_baa.png',($x-30),($y+2),27);
		$pdf->Cell(50,5,'',0,0,'C');
		$pdf->Cell(113,5,'Kepala Program Studi',0,0,'C');

		$pdf->ln(20);
		$pdf->Cell(110,5,'',0,0,'C');
		$pdf->Cell(30,5,'ROULY G RATNA S, ST., MM',0,0,'C');
		$pdf->Cell(50,5,'',0,0,'C');
		$pdf->Cell(113,5,'(.............................................................)',0,0,'C');

		$noo = 1;

		$pdf->AliasNbPages();

$pdf->AddPage();



$pdf->SetMargins(3, 3 ,0);

$pdf->SetFont('Arial','B',10); 



//$pdf->Image(''.base_url().'assets/img/logo-stkip-bima.png',60,30,90);

$pdf->Ln(0);

$pdf->Cell(200,5,strtoupper($titles->prodi),0,0,'L');

$pdf->Ln(4);

$pdf->SetFont('Arial','',10); 

$pdf->Cell(200,5,''.strtoupper($titles->fakultas).' - '.$this->ORG_NAME,0,0,'L');

$pdf->Ln(4);



$pdf->SetFont('Arial','',6);

$pdf->Cell(13,3,'KAMPUS I',0,0,'L');

$pdf->Cell(3,3,' : ',0,0,'L');

$pdf->Cell(140,3,'Jl. piere tendean,Mpunda, Nusa Tenggara Bar., Mande, Mpunda, Bima, Nusa Tenggara Bar. 84111, Indonesia',0,0,'L');

$pdf->Ln(2);



$pdf->SetFont('Arial','',6);

$pdf->Cell(13,3,'',0,0,'L');

$pdf->Cell(3,3,' : ',0,0,'L');

$pdf->Cell(140,3,'',0,0,'L');

$pdf->Ln(4);

$pdf->Cell(290,0,'',1,0,'C');



$pdf->ln(4);

$pdf->SetFont('Arial','',8);

$pdf->Cell(15,5,'KODE MK',0,0,'L');

$pdf->Cell(2,5,':',0,0,'L');

$pdf->Cell(60,5,$rows->kd_matakuliah ,0,0,'L');

$pdf->Cell(15,5,'Smtr/Thn',0,0,'L');

if (substr($rows->kd_tahunajaran, 4)) {
	$ta = 'Ganjil';
} else {
	$ta = 'Genap';
}

$pdf->Cell(2,5,':',0,0,'L');

$pdf->Cell(50,5,$rows->semester_matakuliah.' / '.substr($rows->kd_tahunajaran, 0, 4).' - '.$ta,0,0,'L');

$pdf->Cell(20,5,'NAMA DOSEN',0,0,'L');

$pdf->Cell(2,5,':',0,0,'L');

$pdf->Cell(50,5,$rows->nama,0,0,'L');

$pdf->Cell(20,5,'KELAS',0,0,'L');

$pdf->Cell(2,5,':',0,0,'L');

$pdf->Cell(50,5,$rows->kelas,0,0,'L');

$pdf->ln(4);

$pdf->SetFont('Arial','',8);

$pdf->Cell(15,5,'NAMA MK',0,0,'L');

$pdf->Cell(2,5,':',0,0,'L');

$pdf->Cell(60,5,$rows->nama_matakuliah,0,0,'L');

$pdf->Cell(15,5,'SKS',0,0,'L');

$pdf->Cell(2,5,':',0,0,'L');

$pdf->Cell(50,5,$rows->sks_matakuliah,0,0,'L');

$pdf->Cell(20,5,'NID',0,0,'L');

$pdf->Cell(2,5,':',0,0,'L');

$pdf->Cell(50,5,$rows->kd_dosen,0,0,'L');

$pdf->Cell(20,5,'Kuota',0,0,'L');

$pdf->Cell(2,5,':',0,0,'L');

$pdf->Cell(50,5,$rows->kuota,0,0,'L');

$pdf->ln(4);

$pdf->SetFont('Arial','',8);

$pdf->Cell(15,5,'RUANG',0,0,'L');

$pdf->Cell(2,5,':',0,0,'L');

$pdf->Cell(60,5,$rows->kode_ruangan,0,0,'L');

$pdf->Cell(15,5,'WAKTU',0,0,'L');

$pdf->Cell(2,5,':',0,0,'L');

$pdf->Cell(50,5,notohari($rows->hari).'/'.del_ms($rows->waktu_mulai).'-'.del_ms($rows->waktu_selesai),0,0,'L');

$pdf->Cell(20,5,'KAMPUS',0,0,'L');

$pdf->Cell(2,5,':',0,0,'L');

$pdf->Cell(50,5,'Bekasi',0,0,'L');

$pdf->Cell(20,5,'Jumlah Peserta',0,0,'L');

$pdf->Cell(2,5,':',0,0,'L');

$jumlah = $this->db->query("select count(npm_mahasiswa) as mhs from tbl_krs_sp where kd_jadwal = '".$rows->kd_jadwal."'")->row()->mhs;

$pdf->Cell(50,5,$jumlah.' orang',0,0,'L');



$pdf->ln(10);

$pdf->SetLeftMargin(5);

$pdf->SetFont('Arial','',12);

$pdf->Cell(288,8,'DAFTAR HADIR PESERTA KULIAH',1,0,'C');

$pdf->ln(8);



$pdf->SetFont('Arial','',8);

$pdf->Cell(8,10,'NO','L,T,R,B',0,'C');

$pdf->Cell(25,10,'NPM','L,T,R,B',0,'C');

$pdf->Cell(80,10,'NAMA','L,T,R,B',0,'C');

$pdf->Cell(175,5,'PERTEMUAN KULIAH','L,T,R,B',0,'C');

$pdf->ln(5);

$pdf->Cell(8,0,'',0,0,'C');

$pdf->Cell(25,0,'',0,0,'C');

$pdf->Cell(80,0,'',0,0,'C');



for ($i=1; $i < 8; $i++) { 
	if ($i < 3) {
		$pdf->Cell(25,5,$i,1,0,'C');
	} else {
		$pdf->Cell(25,5,'',1,0,'C',1);
	}	

}
	}

}



$pdf->ln(5);

$pdf->Cell(113,5,'Tanggal Kuliah',1,0,'R');
$pdf->SetFont('Arial','',7);
for ($i=1; $i < 8; $i++) {
	if ($i < 3) {
		$absen = $this->db->query("select tanggal from tbl_absensi_mhs where kd_jadwal = '".$rows->kd_jadwal."' and pertemuan = ".$i." ")->row();
		$pdf->Cell(25,5,'',1,0,'C');
	} else {
		$pdf->Cell(25,5,'',1,0,'C',1);
	}
	
}
$pdf->SetFont('Arial','',8);

$pdf->ln(5);

$pdf->Cell(113,5,'Jumlah Hadir',1,0,'R');


for ($i=1; $i < 8; $i++) { 
	$absen = $this->db->query("select count(npm_mahasiswa) as jml from tbl_absensi_mhs where kd_jadwal = '".$rows->kd_jadwal."' and pertemuan = ".$i." ")->row();
	if ($i < 3) {
		if ($absen == true) {
			$pdf->Cell(25,5,'',1,0,'C');
			//$pdf->Cell(11,5,$absen->jml,1,0,'C');
		} else {
			$pdf->Cell(25,5,'-',1,0,'C');
		}
	} else {
		$pdf->Cell(25,5,'','L,R,B',0,'C',1);
	}

}



$pdf->ln(5);

$pdf->Cell(113,5,'Paraf Dosen',1,0,'R');



for ($i=1; $i < 8; $i++) { 

	if ($i < 3) {
		$pdf->Cell(25,5,'','L,R,B',0,'C');
	} else {
		$pdf->Cell(25,5,'','L,R,B',0,'C',1);
	}
		

}

$pdf->ln(8);
		
$pdf->Cell(110,5,'',0,0,'C');
$pdf->Cell(30,5,'Biro Administrasi Akademik',0,0,'C');
$x = $pdf->GetX();
$y = $pdf->GetY();
$pdf->image(base_url().'/assets/ttd_baa.png',($x-30),($y+2),27);
$pdf->Cell(50,5,'',0,0,'C');
$pdf->Cell(113,5,'Kepala Program Studi',0,0,'C');

$pdf->ln(20);
$pdf->Cell(110,5,'',0,0,'C');
$pdf->Cell(30,5,'ROULY G RATNA S, ST., MM',0,0,'C');
$pdf->Cell(50,5,'',0,0,'C');
$pdf->Cell(113,5,'(.............................................................)',0,0,'C');



$pdf->Output('ABSENSI.PDF','I');



?>

