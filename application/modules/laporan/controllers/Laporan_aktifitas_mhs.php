<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Laporan_aktifitas_mhs extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		if (!$this->session->userdata('sess_login')) {
			redirect('auth/logout','refresh');
		}
		$this->load->model('laporan/m_aktifitas_laporan_mhs', 'ak');
	}

	public function index()
	{
		$data['tahunajar'] = getactyear();
		$data['data'] = $this->ak->get_members($data['tahunajar']);
		$data['page'] = 'aktivitas_laporan_v';
		$this->load->view('template/template', $data);
	}

	public function export_aktifitas($tahunajar)
	{
		$data['tahunajar'] = getactyear();
		$data['data'] = $this->ak->get_members($data['tahunajar']);
		$this->load->view('export_aktifitas_excel', $data);
	}

}

/* End of file Laporan_aktifitas_mhs.php */
/* Location: ./application/modules/laporan/controllers/Laporan_aktifitas_mhs.php */