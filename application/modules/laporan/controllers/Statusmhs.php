<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Statusmhs extends MY_Controller {

	function __construct()
	{
		parent::__construct();
		ini_set('memory_limit', '2048M');
		//$this->load->library('Cfpdf');
		//error_reporting(0);
		if ($this->session->userdata('sess_login') == TRUE) {
			$cekakses = $this->role_model->cekakses(105)->result();
			if ($cekakses != TRUE) {
				echo "<script>alert('Anda Tidak Berhak Mengakses !!');history.go(-1);</script>";
			}
		} else {
			redirect('auth','refresh');
		}
	}

	function index()
	{
		$data['fakultas'] =$this->app_model->getdata('tbl_fakultas', 'id_fakultas', 'ASC')->result();
		$data['tahunajar']=$this->app_model->getdata('tbl_tahunakademik', 'kode', 'ASC')->result();
		$data['page'] = 'status_select';
	    $this->load->view('template/template', $data);
	}

	function get_jurusan($id){
        $jurusan = $this->app_model->getdetail('tbl_jurusan_prodi', 'kd_fakultas', $id, 'id_prodi', 'ASC')->result();
		$out = "<select class='form-control' name='jurusan' id='jurs' required><option disabled selected>--Pilih Program Studi--</option>";
        foreach ($jurusan as $row) {
            $out .= "<option value='".$row->kd_prodi."'>".$row->prodi. "</option>";
        }
        $out .= "</select>";
        echo $out;
	}

	function export_data()
	{
		$prodi = $this->input->post('jurusan', TRUE);
		$tahun = $this->input->post('tahunajaran', TRUE);
		//var_dump($prodi);exit();
		$data['mhs'] = $this->app_model->getmhs($prodi,$tahun)->result();
		//var_dump($data['mhs']);exit();
		$data['tahun'] = $tahun;
		$this->load->view('excels',$data);
	}

}

/* End of file Statusmhs.php */
/* Location: ./application/modules/laporan/controllers/Statusmhs.php */