<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Evaluasi extends MY_Controller {

	private $userid;

	public function __construct()
	{
		parent::__construct();
		ini_set('memory_limit', '512M');
		error_reporting(0);
		if ($this->session->userdata('sess_login') == TRUE) {
			$cekakses = $this->role_model->cekakses(97)->result();
			if ($cekakses != TRUE) {
				echo "<script>alert('Anda Tidak Berhak Mengakses !');history.go(-1);</script>";
			}
		} else {
			redirect('auth','refresh');
		}

		$this->userid = $this->session->userdata('sess_login')['userid'];
		$this->edom = $this->load->database('eval', TRUE);
		$this->load->model('model_laporan','laporan');
	}

	public function index()
	{
		$this->session->unset_userdata('prod');
		$this->session->unset_userdata('ta');
		$data['tahunajar'] = $this->app_model->getdata('tbl_tahunakademik','kode','asc')->result();
		$data['prodi']     = $this->app_model->getdata('tbl_jurusan_prodi','kd_prodi','asc')->result();
		$data['fakultas']  = $this->app_model->getdata('tbl_fakultas','kd_fakultas','asc')->result();
		$data['page']      = 'laporan/evluasi_select';
		$this->load->view('template/template', $data);
	}

	public function savesess()
	{
		$this->session->set_userdata('prod', $this->input->post('prodi', TRUE));
		$this->session->set_userdata('ta',$this->input->post('tahunajaran', TRUE));
		redirect(base_url('laporan/evaluasi/view'));
	}

	public function savefaculty()
	{
		extract(PopulateForm());
		$this->session->set_userdata('evalforfac',$tahunajaran);
		redirect(base_url('laporan/evaluasi/listfaculty'));
	}

	public function listfaculty()
	{
		$data['sesi'] 		= $this->session->userdata('evalforfac');
		$data['faculty'] 	= $this->app_model->getdata('tbl_fakultas','kd_fakultas','asc')->result();
		$data['page']		= "vlistprodieval";
		$this->load->view('template/template', $data);
	}

	public function perProdi($id)
	{
		$data['arry'] = $id;
		$data['sesi'] = $this->session->userdata('evalforfac');
		$data['prod'] = $this->app_model->getdetail('tbl_jurusan_prodi','kd_fakultas',$id,'kd_prodi','asc')->result();
		$data['page'] = "vdetilprodieval";
		$this->load->view('template/template', $data);
	}

	public function perPoin($id)
	{
		$data['sesi'] = $this->session->userdata('evalforfac');

		$this->edom = $this->load->database('eval', TRUE);
		$this->edom->distinct();
		$this->edom->select('topik,parameter,bobot,id_parameter');
		$this->edom->from('tbl_parameter a');
		$this->edom->join('tbl_topik_parameter b', 'a.kd_topik = b.kd_topik');
		$data['getData'] = $this->edom->get()->result();

		$data['prodi'] = $id;
		
		$this->session->set_userdata('idjadwal',$id);

		$data['page'] = 'laporan/vdetailpoinprodi';
		$this->load->view('template/template', $data);
	}

	public function excelListFaculty()
	{
		$data['sesi'] 		= $this->session->userdata('evalforfac');
		$data['faculty'] 	= $this->app_model->getdata('tbl_fakultas','kd_fakultas','asc')->result();
		$this->load->view('export_list_prodi', $data);
	}

	public function excelListProdi($id)
	{
		$data['arr']  = $id;
		$data['sesi'] = $this->session->userdata('evalforfac');
		$data['prod'] = $this->app_model->getdetail('tbl_jurusan_prodi','kd_fakultas',$id,'kd_prodi','asc')->result();
		$this->load->view('exportperprodi', $data);
	}

	public function excelPoin($id)
	{
		$data['sesi'] = $this->session->userdata('evalforfac');

		$this->edom = $this->load->database('eval', TRUE);
		$this->edom->distinct();
		$this->edom->select('topik,parameter,bobot,id_parameter');
		$this->edom->from('tbl_parameter a');
		$this->edom->join('tbl_topik_parameter b', 'a.kd_topik = b.kd_topik');
		$data['getData'] = $this->edom->get()->result();

		$data['prodi'] = $id;
		
		$this->session->set_userdata('idjadwal',$id);

		$this->load->view('exportpoin', $data);
	}

	public function view()
	{
		if ($this->userid == 'LPM' OR $this->userid == 'BAA') {
			$loging = $this->session->userdata('prod');
			$this->session->set_userdata('sesi_buat_viewdetil', $loging);
			$data['prodi'] = $loging;
		} else {
			$loging = $this->userid;
			$data['prodi'] = $this->userid;
		}

		$data['tahunajaran'] = $this->session->userdata('ta');
		$data['getData'] = $this->laporan->get_eval_dosen($data['tahunajaran'], $loging)->result();

		$this->session->set_userdata('year', $this->session->userdata('ta'));
		$data['page'] = 'laporan/evluasi_view';
		$this->load->view('template/template', $data);
	}

	public function view_detail($nid)
	{
		$data['kry'] = $this->db->get_where('tbl_karyawan', ['nid' => $nid])->row();	
		$this->session->set_userdata('nama_dosen', $data['kry']->nama);

		if (in_array($this->userid, ['LPM', 'BAA'])) {
			$prodi = $this->session->userdata('sesi_buat_viewdetil');
		} else {
			$prodi = $this->userid;
		}

		$data['tahunajaran'] = $this->session->userdata('ta');
		$data['getData'] = $this->laporan->detail_eval_dosen($this->session->userdata('ta'), $prodi, $nid)->result();
		
		$this->session->set_userdata('dosen', $nid );
		$data['page'] = 'laporan/evluasi_detail';
		$this->load->view('template/template', $data);
	}

	public function view_eval($id)
	{
		$data['kdjadwal'] = $this->db->select('kd_jadwal,kelas')->get_where('tbl_jadwal_matkul',['id_jadwal' => $id])->row();
		$tahunajaran = $this->session->userdata('ta');
		$where = ['kd_jadwal' => $data['kdjadwal']->kd_jadwal];

		switch ($tahunajaran) {
			case '20171':
				# get data nilai
				$data['getData'] = $this->edom->query("SELECT AVG(c.nilai) as nilai,a.parameter 
													FROM tbl_parameter a
													JOIN tbl_nilai_parameter c ON a.id_parameter = c.parameter_id
													WHERE kd_jadwal = '{$data['kdjadwal']->kd_jadwal}' 
													GROUP BY a.parameter")->result();

				#get data saran
				$data['getData2'] = $this->edom
										->select('saran')
										->get_where('tbl_pengisian_kuisioner', $where)
										->result();
				break;
			
			default:
				# get data nilai
				$data['getData'] = $this->edom->query("SELECT AVG(c.nilai) as nilai,a.parameter 
													FROM tbl_parameter a
													JOIN tbl_nilai_parameter_{$tahunajaran} c 
													ON a.id_parameter = c.parameter_id
													WHERE kd_jadwal = '{$data['kdjadwal']->kd_jadwal}' 
													GROUP BY a.parameter")->result();

				#get data saran
				$data['getData2'] = $this->edom
										->select('saran')
										->get_where('tbl_pengisian_kuisioner_'.$tahunajaran, $where)
										->result();
				break;
		}

		$this->session->set_userdata('idjadwal',$id);
		$data['page'] = 'laporan/evluasi_detail_view';
		$this->load->view('template/template', $data);
	}

	public function export_evl_dosen()
	{		
		if (in_array($this->userid, ['LPM', 'BAA'])) {
			$prodi = $this->session->userdata('sesi_buat_viewdetil');
		} else {
			$prodi = $this->userid;
		}
		$tahunajaran = $this->session->userdata('ta');
		$fakultas = get_kdfak_byprodi($prodi);
		$data['prodi'] = $prodi;

		$sql = "SELECT DISTINCT 
					b.kd_dosen,
					(SELECT SUM(mk.sks_matakuliah) AS sks FROM tbl_jadwal_matkul jdl
					JOIN tbl_matakuliah mk ON jdl.kd_matakuliah = mk.kd_matakuliah
					WHERE jdl.kd_dosen = b.kd_dosen 
					AND jdl.kd_tahunajaran = '{$tahunajaran}' 
					AND mk.kd_prodi = '{$prodi}'
					AND (jdl.kd_jadwal LIKE '{$prodi}%' OR jdl.kd_jadwal LIKE '{$fakultas}/%') 
					AND mk.mk_ta IS NULL) AS sks 
				FROM tbl_jadwal_matkul b
				JOIN tbl_matakuliah d ON b.kd_matakuliah = d.kd_matakuliah
				WHERE (b.kd_jadwal LIKE '{$prodi}%' OR b.kd_jadwal LIKE '{$fakultas}/%') 
				AND b.kd_tahunajaran = '{$tahunajaran}'
				AND d.kd_prodi = '{$prodi}' 
				AND d.mk_ta IS NULL";

		$data['catch'] = $this->db->query($sql)->result();
		$this->load->view('export_eval_dosen', $data);
	}

	public function export_per_dosen()
	{
		if (in_array($this->userid, ['LPM', 'BAA'])) {
			$prodi = $this->session->userdata('sesi_buat_viewdetil');
		} else {
			$prodi = $this->userid;
		}

		$fakultas = get_kdfak_byprodi($prodi);
		$data['tahunakad'] = $this->session->userdata('ta');
		$data['prodi'] = $prodi;
		$data['kry'] = $this->db->get_where('tbl_karyawan', ['nid' => $this->session->userdata('dosen')])->row();

		$data['eval'] 	= $this->db->query("SELECT DISTINCT 
												a.`kd_matakuliah`,
												b.`nama_matakuliah`,
												a.`kd_jadwal`,
												a.`id_jadwal`,
												a.`kelas`,
												b.`sks_matakuliah` 
											FROM tbl_jadwal_matkul a
											JOIN tbl_matakuliah b ON a.`kd_matakuliah` = b.`kd_matakuliah` 
											WHERE a.`kd_tahunajaran` = '{$data['tahunakad']}'
											AND (a.`kd_jadwal` LIKE '{$prodi}%' OR a.`kd_jadwal` LIKE '{$fakultas}/%')
											AND a.`kd_dosen` = '{$this->session->userdata('dosen')}' 
											AND b.`kd_prodi` = '{$prodi}'")->result();

		$this->load->view('eport_eval_per_dosen', $data);
	}

	public function export_hasil()
	{
		$data['kdjadwal'] 	= $this->db->query("SELECT kd_jadwal,kelas FROM tbl_jadwal_matkul 
												WHERE id_jadwal = ".$this->session->userdata('idjadwal')."
												")->row();

		$this->edom = $this->load->database('eval', TRUE);
		$this->edom->distinct();
		$this->edom->select('topik,parameter,bobot,id_parameter');
		$this->edom->from('tbl_parameter a');
		$this->edom->join('tbl_topik_parameter b', 'a.kd_topik = b.kd_topik');
		
		$data['getData'] = $this->edom->get()->result();
		$this->load->view('export_hasil_eval', $data);
	}

	public function export_saran()
	{
		$this->edom = $this->load->database('eval', TRUE);
		$year = $this->session->userdata('ta');
		$data['kdjadwal'] = $this->db
									->select('kd_jadwal,kelas')
									->get_where('tbl_jadwal_matkul', ['id_jadwal' => $this->session->userdata('idjadwal')])
									->row();

		$where = ['kd_jadwal' => $data['kdjadwal']->kd_jadwal];
		if ($this->session->userdata('ta') == '20171') {
			$data['getData2'] = $this->edom->get_where('tbl_pengisian_kuisioner_20171', $where)->result();
		} else {
			$data['getData2'] = $this->edom->get_where('tbl_pengisian_kuisioner_'.$year, $where)->result();
		}
		$this->load->view('export_saran', $data);
	}

	public function export_eval()
	{
		$this->load->model('temph_model');
		$this->load->library('excel');
		$data['averageprodi'] = $this->edom->query('SELECT * from tbl_topik_parameter order by id_topik asc')->result();
		$this->load->view('excel_eval',$data);
	}

}

/* End of file Evaluasi.php */
/* Location: ./application/modules/laporan/controllers/Evaluasi.php */