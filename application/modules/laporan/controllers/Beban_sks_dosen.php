<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Beban_sks_dosen extends MY_Controller {

	protected $tahunakademik;

	public function __construct()
	{
		parent::__construct();
		if (!$this->session->userdata('sess_login')) {
			redirect('auth/logout','refresh');
		}
		$this->load->model('laporan/m_beban_sks', 'cw' /** credit weight */);
		$this->tahunakademik = getactyear();
	}

	public function index()
	{
		$data['tahunajar'] = $this->tahunakademik;
		$data['data'] = $this->cw->get_credit_weight($this->tahunakademik);
		$data['page'] = 'beban_sks_v';
		$this->load->view('template/template', $data);
	}

	public function detail_mengajar($nid)
	{
		$this->_is_id_exist($nid);
		$data['tahunajar'] = $this->tahunakademik;
		$data['nid']  = $nid;
		$data['data'] = $this->cw->detail_mengajar($nid, $this->tahunakademik);
		$data['page'] = 'detail_mengajar_v';
		$this->load->view('template/template', $data);
	}

	public function print_credit_report()
	{
		$data['tahunajar'] = $this->tahunakademik;
		$data['data'] = $this->cw->get_credit_weight($this->tahunakademik);
		$this->load->view('print/excel_credit_report', $data);
	}

	public function print_teaching_report($nid)
	{
		$this->_is_id_exist($nid);
		$data['tahunajar'] = $this->tahunakademik;
		$data['name'] = str_replace(' ', '_', nama_dsn($nid));
		$data['data'] = $this->cw->detail_mengajar($nid, $this->tahunakademik);
		$this->load->view('print/excel_teaching_report', $data);
	}

	protected function _is_id_exist($nid)
	{
		$is_exist = $this->db->get_where('tbl_karyawan', ['nid' => $nid])->num_rows();
		if ($is_exist == 0) {
			echo '<script>alert("ID dosen tidak ditemukan!");location.href="'.base_url('laporan/beban_sks_dosen').'"</script>';
			exit();
		}
		return;
	}

}

/* End of file Beban_sks_dosen.php */
/* Location: ./application/modules/laporan/controllers/Beban_sks_dosen.php */