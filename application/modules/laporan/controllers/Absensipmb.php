<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Absensipmb extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		//error_reporting(0);
		if ($this->session->userdata('sess_login') != TRUE) {
			//$cekakses = $this->role_model->cekakses(100)->result();
			//if ($cekakses != TRUE) {
				//echo "<script>alert('Anda Tidak Berhak Mengakses !!');history.go(-
					//1);</script>";
			//}
		//} else {
			redirect('auth','refresh');
		}
		$this->dbreg = $this->load->database('regis', TRUE);
		date_default_timezone_set('Asia/Jakarta'); 
		$this->load->library('Cfpdf');
	}

	public function index()
	{
		$data['fakultas'] =$this->app_model->getdata('tbl_fakultas', 'id_fakultas', 'ASC')->result();
		$data['page'] = 'absensipmb_view';

		$this->load->view('template/template', $data);
	}

	function get_jurusan($id){

		$jrs = explode('-',$id);

        $jurusan = $this->app_model->getdetail('tbl_jurusan_prodi', 'kd_fakultas', $jrs[0], 'id_prodi', 'ASC')->result();

		$out = "<select class='form-control' name='jurusan' id='jurs'><option>--Pilih Program Studi--</option>";

        foreach ($jurusan as $row) {

            $out .= "<option value='".$row->kd_prodi."'>".$row->prodi. "</option>";

        }

        $out .= "</select>";

        echo $out;

	}

	function load_data()
	{
		$data['gel'] = $this->input->post('gelombang', TRUE);
		$program = $this->input->post('program', TRUE);
		$jenis = $this->input->post('jenis', TRUE);

		if ($this->input->post('tahun') == '16') {
			$dbcuy = [$this->db,'tbl_form_camaba_2016','ID_registrasi','opsi_prodi_s2','tbl_pmb_s2_2016'];
		} elseif ($this->input->post('tahun') == '17') {
			$dbcuy = [$this->db,'tbl_form_camaba','ID_registrasi','opsi_prodi_s2','tbl_pmb_s2'];
		} elseif ($this->input->post('tahun') >= '18') {
			$dbcuy = [$this->dbreg,'tbl_form_pmb','nomor_registrasi','prodi','tbl_form_pmb'];
		}

		if ($program == 'S1') {
			if ($jenis == 0) {
				$data['absenpmb'] = $dbcuy[0]->query("select nomor_registrasi as noreg,prodi as prodi,kelas as kelas,nama from ".$dbcuy[1]." where gelombang = ".$this->input->post('gelombang', TRUE)." AND prodi = ".$this->input->post('jur')." and ".$dbcuy[2]." like '".$this->input->post('tahun')."%'")->result();
			} else {
				$data['absenpmb'] = $dbcuy[0]->query("select nomor_registrasi as noreg,prodi as prodi,kelas as kelas,nama from ".$dbcuy[1]." where gelombang = ".$this->input->post('gelombang', TRUE)." and jenis_pmb = '".$jenis."' AND prodi = ".$this->input->post('jur')." and ".$dbcuy[2]." like '".$this->input->post('tahun')."%'")->result();
			}
		} else {
			if ($jenis == 0) {
				$data['absenpmb'] = $dbcuy[0]->query("select ".$dbcuy[2]." as noreg,".$dbcuy[3]." as prodi,kelas as kelas,nama from ".$dbcuy[4]." where gelombang = ".$this->input->post('gelombang', TRUE)." AND ".$dbcuy[3]." = ".$this->input->post('jur')." and ".$dbcuy[2]." like '".$this->input->post('tahun')."%'")->result();
			} else {
				$data['absenpmb'] = $dbcuy[0]->query("select ".$dbcuy[2]." as noreg,".$dbcuy[3]." as prodi,kelas as kelas,nama from ".$dbcuy[4]." where gelombang = ".$this->input->post('gelombang', TRUE)." and jenis_pmb = '".$jenis."' AND ".$dbcuy[3]." = ".$this->input->post('jur')."
					and ".$dbcuy[2]." like '".$this->input->post('tahun')."%'")->result();
			}
		}
		
		
		$this->load->view('print_absen_pmb', $data);
	}

}

/* End of file Absensipmb.php */
/* Location: ./application/modules/laporan/controllers/Absensipmb.php */