<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Report_krs extends MY_Controller {

	public function index()
	{
		$data['ta'] = $this->db->get('tbl_tahunakademik')->result();
		$mari = $this->session->userdata('sess_login');

		$pecah = explode(',', $mari['id_user_group']);
		$jmlh = count($pecah);
		for ($i=0; $i < $jmlh; $i++) { 
			$grup[] = $pecah[$i];
		}

		if ((in_array(8, $grup))) {
			$data['pr'] = $this->db->query("SELECT * from tbl_jurusan_prodi where kd_prodi = '".$mari['userid']."'")->row();
			$data['krw'] = $this->db->query("SELECT * from tbl_karyawan")->result();
		} elseif ((in_array(9, $grup))) {
			$data['pr'] = $this->db->query("SELECT * from tbl_jurusan_prodi where kd_fakultas = '".$mari['userid']."'")->result();
		} elseif ((in_array(10, $grup))|| (in_array(14, $grup)) || (in_array(1, $grup))) {
			$data['pr'] = $this->db->query("SELECT * from tbl_jurusan_prodi")->result();
		}
		
		$data['page'] = "v_repkrs";
		$this->load->view('template/template', $data);
	}

	function savesesi()
	{
		$prodi = $this->input->post('pro');
		$tahun = $this->input->post('tahun');
		$dpa = $this->input->post('papnya');
		$this->session->set_userdata('tahunkrs',$tahun);
		$this->session->set_userdata('prod',$prodi );
		$this->session->set_userdata('dpa',$dpa );
		if ($dpa==''){
			redirect('laporan/report_krs/load');
		} else {
			redirect('laporan/report_krs/load2');
		}
		
	}

	public function autopa() {    
         $kode=$this->input->post('kode',TRUE);

		  $query=$this->app_model->cari($kode);
		  $json_array = array();
				foreach ($query as $row)
					$json_array[]=$row->nama;
				echo json_encode($json_array);
    }
	function load()
	{
		$sess = $this->session->userdata('sess_login');
		$prodi = $sess['userid'];
		$data['quer'] = $this->app_model->rekap_krs($this->session->userdata('tahunkrs'),$this->session->userdata('prod'))->result();
		$data['page'] = "v_krs_mhs";
		$this->load->view('template/template', $data);
	}
	function load2()
	{
		$sess = $this->session->userdata('sess_login');
		$prodi = $sess['userid'];
		$data['quer'] = $this->app_model->rekap_krs2($this->session->userdata('tahunkrs'),$this->session->userdata('prod'),$this->session->userdata('dpa'))->result();
		$data['page'] = "v_krs_mhs";
		$this->load->view('template/template', $data);
	}

	function exp()
	{
		$data['pro'] = $this->db->query("SELECT * from tbl_jurusan_prodi where kd_prodi = '".$this->session->userdata('prod')."'")->row()->prodi;
		$data['quer'] = $this->app_model->rekap_krs($this->session->userdata('tahunkrs'),$this->session->userdata('prod'))->result();
		$this->load->view('exp_krs', $data);
	}

}

/* End of file Report_krs.php */
/* Location: ./application/modules/laporan/controllers/Report_krs.php */