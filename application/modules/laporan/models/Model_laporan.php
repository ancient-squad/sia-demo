<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Model_laporan extends CI_Model
{

    function laporan_krs($before_year, $current_year, $log, $userid)
    {
        $this->db->select('*');

        if (($log == 8)) {
            $this->db->where('kd_fakultas', get_kdfak_byprodi($userid));
        }

        $data = $this->db->get('tbl_fakultas')->result();

        $arr = [];
        foreach ($data as $key => $value) {
            if (($log == 8)) {
                $userfakprodi = ['kd_fakultas' => $value->kd_fakultas, 'kd_prodi' =>   $userid];
            } else {
                $userfakprodi = ['kd_fakultas' => $value->kd_fakultas];
            }

            $prodi = $this->db->get_where('tbl_jurusan_prodi', $userfakprodi)->result_array();

            foreach ($prodi as $key => $val) {
                // jumlah_mhs --> before_year
                $jml = $this->get_jumlah_krs($before_year, $val['kd_prodi'])->row_array();
                $arr[$value->fakultas][$val['prodi']]['jml'] = $jml['jml'];

                // jumlah_revisi
                $jml_revisi = $this->get_jumlah_revisi($current_year, $val['kd_prodi'])->row_array();
                $arr[$value->fakultas][$val['prodi']]['revisi'] = $jml_revisi['revisi'];

                // jumlah_verifikasi
                $jml_verifikasi = $this->get_jumlah_verifikasi($current_year, $val['kd_prodi'])->row_array();
                $arr[$value->fakultas][$val['prodi']]['verifikasi'] = $jml_verifikasi['verifikasi'];

                // jumlah_terajukan
                $jumlah_terajukan = $this->get_jumlah_terajukan($current_year, $val['kd_prodi'])->row_array();
                $arr[$value->fakultas][$val['prodi']]['terajukan'] = $jumlah_terajukan['terajukan'];

                // jumlah_belumdiajukan
                $jumlah_belumdiajukan = $this->get_jumlah_belumdiajukan($current_year, $val['kd_prodi'])->row_array();
                $arr[$value->fakultas][$val['prodi']]['belum_diajukan'] = $jumlah_belumdiajukan['belum_diajukan'];

                // total yang sudah mengisi krs
                // total = revisi + verifikasi + terajukan + belumdiajukan
                $total_krsan = $jml_revisi['revisi'] + $jml_verifikasi['verifikasi'] + $jumlah_terajukan['terajukan'] + $jumlah_belumdiajukan['belum_diajukan'];
                $arr[$value->fakultas][$val['prodi']]['total_krsan'] = $total_krsan;

                // belum input krs
                // belum_input = jumlah_mhs - total_krsan
                $belum_input = $jml['jml'] - $total_krsan;
                $arr[$value->fakultas][$val['prodi']]['belum_input'] = $belum_input;

                // Persentase
                // persentase = total_krsan / jumlah_mhs * 100
                $persentase = ceil($total_krsan / $jml['jml'] * 100);
                $arr[$value->fakultas][$val['prodi']]['persentase'] = $persentase . " %";
            }
        }

        return $arr;
    }


    public function get_jumlah_krs($tahunajaran, $prodi = '')
    {
        $and = '';

        if (!empty($prodi)) {
            $and = " AND kd_jurusan = " . $prodi;
        }
        // -- total mahasiswa krs
        $sql = "
                select
                    count(1) as jml
                from
                    (
                    select
                        distinct kd_krs
                    from
                        tbl_verifikasi_krs
                    where
                        tahunajaran = $tahunajaran " . $and . " ) tbl_verifikasi_krs";

        return $this->db->query($sql);
    }

    public function get_jumlah_revisi($tahunajaran, $prodi = '')
    {
        $and = '';

        if (!empty($prodi)) {
            $and = " AND kd_jurusan = " . $prodi;
        }
        // -- total mahasiswa revisi
        $sql = "select
                    count(1) as revisi
                from
                    (
                    select
                        distinct kd_krs
                    from
                        tbl_verifikasi_krs
                    where
                        tahunajaran = $tahunajaran " . $and . " and status_verifikasi = 2  ) tbl_verifikasi_krs;";

        return $this->db->query($sql);
    }

    public function get_jumlah_verifikasi($tahunajaran, $prodi = '')
    {
        $and = '';

        if (!empty($prodi)) {
            $and = " AND kd_jurusan = " . $prodi;
        }
        // -- total mahasiswa verifikasi
        $sql = "select
                    count(1) as verifikasi
                from
                    (
                    select
                        distinct kd_krs
                    from
                        tbl_verifikasi_krs
                    where
                        tahunajaran = $tahunajaran " . $and . " and status_verifikasi = 1 ) tbl_verifikasi_krs";

        return $this->db->query($sql);
    }

    public function get_jumlah_terajukan($tahunajaran, $prodi = '')
    {
        $and = '';

        if (!empty($prodi)) {
            $and = " AND kd_jurusan = " . $prodi;
        }
        // -- total mahasiswa terajukan
        $sql = "select
                    count(1) as terajukan
                from
                    (
                    select
                        distinct kd_krs
                    from
                        tbl_verifikasi_krs
                    where
                        tahunajaran = $tahunajaran " . $and . " and status_verifikasi = 4 ) tbl_verifikasi_krs";

        return $this->db->query($sql);
    }

    public function get_jumlah_belumdiajukan($tahunajaran, $prodi = '')
    {
        $and = '';

        if (!empty($prodi)) {
            $and = " AND kd_jurusan = " . $prodi;
        }
        // -- total mahasiswa belum diajukan
        $sql = "select
                    count(1) as belum_diajukan
                from
                    (
                    select
                        distinct kd_krs
                    from
                        tbl_verifikasi_krs
                    where
                        tahunajaran = $tahunajaran" . $and . " and status_verifikasi is NULL ) tbl_verifikasi_krs;";

        return $this->db->query($sql);
    }

    public function get_jurusan($prodi = '')
    {
        $this->db->select('*');
        $this->db->from('tbl_jurusan_prodi a');

        if (!empty($prodi)) {
            $this->db->where('a.kd_prodi', $prodi, FALSE);
        }

        $this->db->join('tbl_fakultas b', 'a.kd_fakultas = b.kd_fakultas');
        return $this->db->get()->result();
    }

    public function getrekap($jurusan, $tahunajaran)
    {
        $query  = $this->db->query("SELECT 
                                        a.`npm_mahasiswa`,
                                        b.`NMMHSMSMHS`, 
                                        b.`KDPSTMSMHS`, 
                                        a.`kd_matakuliah`, 
                                        a.`semester_krs`,
                                        a.`kd_jadwal`
                                    FROM tbl_krs a
                                    JOIN tbl_mahasiswa b ON a.`npm_mahasiswa` = b.`NIMHSMSMHS`
                                    WHERE kd_krs LIKE CONCAT(npm_mahasiswa, '$tahunajaran%')
                                    AND a.kd_matakuliah IN 
                                        (SELECT DISTINCT a1.kd_matakuliah FROM tbl_matakuliah a1
                                        JOIN tbl_kurikulum_matkul_new b1 ON a1.`kd_matakuliah` = b1.`kd_matakuliah`
                                        WHERE ( a1.`nama_matakuliah` LIKE 'skripsi%' OR a1.`nama_matakuliah` ))
                                    AND b.`KDPSTMSMHS` = $jurusan
                                    ORDER BY b.`NMMHSMSMHS` ASC");

        return $query->num_rows() > 0 ? $query->result() : array();
    }

    public function get_eval_dosen($tahunajaran, $prodi)
    {
        $kode_fakultas = get_kdfak_byprodi($prodi);
        $data = $this->db->query("SELECT DISTINCT 
                                    b.kd_dosen,
                                    (SELECT SUM(mk.`sks_matakuliah`) AS sks FROM tbl_jadwal_matkul jdl
                                    JOIN tbl_matakuliah mk ON jdl.`kd_matakuliah` = mk.`kd_matakuliah`
                                    WHERE jdl.`kd_dosen` = b.`kd_dosen` 
                                    AND jdl.kd_tahunajaran = '{$tahunajaran}' 
                                    AND mk.kd_prodi = '{$prodi}'
                                    AND jdl.kd_jadwal LIKE '{$prodi}%' 
                                    AND mk.mk_ta IS NULL
                                    ) AS sks 
                                FROM tbl_jadwal_matkul b
                                JOIN tbl_matakuliah d ON b.`kd_matakuliah` = d.`kd_matakuliah`
                                WHERE (b.`kd_jadwal` LIKE '{$prodi}%' OR b.`kd_jadwal` LIKE '{$fakultas}/%') 
                                AND b.`kd_tahunajaran` = '{$tahunajaran}'
                                AND d.kd_prodi = '{$prodi}'
                                AND d.mk_ta IS NULL");
        return $data;
    }

    public function detail_eval_dosen($tahunajaran, $prodi, $nid)
    {
        $kode_fakultas = get_kdfak_byprodi($prodi);
        $data = $this->db->query("SELECT DISTINCT 
                                    a.`kd_matakuliah`,
                                    b.`nama_matakuliah`,
                                    a.`kd_jadwal`,
                                    a.`id_jadwal`,
                                    a.`kelas`,
                                    b.`sks_matakuliah` 
                                FROM tbl_jadwal_matkul a
                                JOIN tbl_matakuliah b ON a.`kd_matakuliah` = b.`kd_matakuliah`
                                WHERE a.`kd_tahunajaran` = '{$tahunajaran}' 
                                AND (a.`kd_jadwal` LIKE '{$prodi}%' OR a.`kd_jadwal` LIKE '{$kode_fakultas}/%') 
                                AND a.`kd_dosen` = '{$nid}' 
                                AND b.`kd_prodi` = '{$prodi}'"
                            );
        return $data;
    }
}
