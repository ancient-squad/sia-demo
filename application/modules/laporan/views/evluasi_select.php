<div class="row">
  <div class="span12">                
      <div class="widget ">
        <div class="widget-header">
          <i class="icon-user"></i>
          <h3>Evaluasi Belajar Mengajar</h3>
      </div>

      <div class="widget-content">
        <ul class="nav nav-tabs">
          <li class="active"><a data-toggle="tab" href="#home">Per dosen</a></li>
          <?php if ($log['userid'] == 'LPM') { ?>
            <li><a data-toggle="tab" href="#menu1">Per fakultas</a></li>
          <?php } ?>
        </ul>

        <div class="tab-content">
          <div id="home" class="tab-pane fade in active">
            <div class="span11">
              <form method="post" class="form-horizontal" action="<?= base_url(); ?>laporan/evaluasi/savesess">
                <fieldset>
                  <div class="control-group">
                    <label class="control-label">Tahun Akademik</label>
                    <div class="controls">
                      <select class="form-control span6" name="tahunajaran" id="tahunajaran" required>
                        <option disabled selected>--Pilih Tahun Akademik--</option>
                        <?php foreach ($tahunajar as $row) { ?>
                        <option value="<?= $row->kode;?>"><?= $row->tahun_akademik;?></option>
                        <?php } ?>
                      </select>
                    </div>
                  </div>

                  <?php $log = $this->session->userdata('sess_login'); 
                    if ($log['userid'] == 'LPM' OR $log['userid'] == 'BAA') : ?>
                      <div class="control-group">
                        <label class="control-label">Prodi</label>
                        <div class="controls">
                          <select class="form-control span6" name="prodi" id="" required>
                            <option disabled selected>--Pilih Program Studi--</option>
                            <?php foreach ($prodi as $val) : ?>
                            <option value="<?= $val->kd_prodi;?>"><?= $val->prodi;?></option>
                            <?php endforeach; ?>
                          </select>
                        </div>
                      </div>
                  <?php endif; ?>

                  <div class="form-actions">
                    <input type="submit" class="btn btn-large btn-success" value="Cari"/> 
                  </div>
                </fieldset>
              </form>
            </div>
          </div>
          <div id="menu1" class="tab-pane fade">
            <div class="span11">
              <form method="post" class="form-horizontal" action="<?= base_url(); ?>laporan/evaluasi/savefaculty">
                <fieldset>
                  <div class="control-group">
                    <label class="control-label">Tahun Akademik</label>
                    <div class="controls">
                      <select class="form-control span6" name="tahunajaran" id="tahunajaran" required>
                        <option disabled selected>--Pilih Tahun Akademik--</option>
                        <?php foreach ($tahunajar as $row) { ?>
                        <option value="<?= $row->kode;?>"><?= $row->tahun_akademik;?></option>
                        <?php } ?>
                      </select>
                    </div>
                  </div>
                  <div class="form-actions">
                      <input type="submit" class="btn btn-large btn-success" value="Cari"/> 
                  </div> <!-- /form-actions -->
                </fieldset>
              </form>
            </div>         
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

