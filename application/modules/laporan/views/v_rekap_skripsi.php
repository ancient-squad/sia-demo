<div class="row">
	<div class="span12">
		<div class="widget ">
			<div class="widget-header">
				<i class="icon-user"></i>
				<h3>Rekapitulasi Skripsi <?php echo get_jur($this->session->userdata('jurusan')) ?> Tahun Ajaran <?php echo $this->session->userdata('tahunajaran'); ?></h3>
			</div> <!-- /widget-header -->
			<div class="widget-content">
				<div class="span11">
					<table id="example1" class="table table-bordered table-striped">
						<thead>
							<tr>
								<td>No</td>
								<td>NPM</td>
								<td>Nama</td>
								<td>Kode Matakuliah</td>
								<td>Semester</td>
								<td>Kelas</td>
							</tr>
						</thead>
						<tbody>
							<?php $no = 1;
							foreach ($rekap as $row) { ?>
								<tr>
									<td><?php echo $no; ?></td>
									<td><?php echo $row->npm_mahasiswa; ?></td>
									<td><?php echo $row->NMMHSMSMHS; ?></td>
									<td><?php echo $row->kd_matakuliah; ?></td>
									<td><?php echo $row->semester_krs; ?></td>
									<td><?php echo getNamaKelas(get_id_jdl($row->kd_jadwal)); ?></td>
								</tr>
							<?php $no++;
							} ?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>