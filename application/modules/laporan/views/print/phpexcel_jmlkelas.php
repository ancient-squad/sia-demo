<?php 

$excel = new PHPExcel();
$BStyle = array(
  'borders' => array(
    'allborders' => array(
      'style' => PHPExcel_Style_Border::BORDER_THIN
    )
  )
);

$shit=0;

    $ts = $this->db->where('kd_prodi', $jur)->get('tbl_jurusan_prodi')->row();    
    
    //ACTIVE SHEET
    $excel->setActiveSheetIndex($shit);
    //name the worksheet
    $excel->getActiveSheet()->setTitle($ts->prodi);

    //border
    //$excel->getActiveSheet()->getStyle('B3:C4')->applyFromArray($BStyle);
    //$excel->getActiveSheet()->getStyle('E3:F4')->applyFromArray($BStyle);
    //$excel->getActiveSheet()->getStyle('H3:L4')->applyFromArray($BStyle);

    //tittle
    $excel->getActiveSheet()->setCellValue('C1', 'DATA JUMLAH KELAS');
    $excel->getActiveSheet()->mergeCells('C1:F1');
    $excel->getActiveSheet()->setCellValue('C2', 'PROGRAM STUDI '.$ts->prodi.'');
    $excel->getActiveSheet()->mergeCells('C2:F2');

    //semester
    $h1=4;
    $hy=$h1+1;
    $excel->getActiveSheet()->setCellValue('A'.$h1, 'SEMESTER');
    $excel->getActiveSheet()->mergeCells('A4:A5');
    $excel->getActiveSheet()->setCellValue('B'.$h1, 'KELAS');
    $excel->getActiveSheet()->mergeCells('B4:D4');
    $excel->getActiveSheet()->setCellValue('B'.$hy, 'PAGI');
    $excel->getActiveSheet()->setCellValue('C'.$hy, 'SORE');
    $excel->getActiveSheet()->setCellValue('D'.$hy, 'P2K');
    //isi semester
    $hy++;


    foreach ($sem as $v) {

        $excel->getActiveSheet()->setCellValue('A'.$hy, $v->semes);

        $pg = $this->db->query("SELECT mk.`semester_matakuliah`,COUNT(kd_jadwal) AS jml FROM tbl_jadwal_matkul jdl
                    JOIN tbl_matakuliah mk ON jdl.`kd_matakuliah` = mk.`kd_matakuliah` 
                    WHERE SUBSTRING_INDEX(jdl.`kd_jadwal`, '/', 1) = '".$jur."' AND jdl.`kd_tahunajaran` = '".$thn."' AND mk.`kd_prodi` = '".$jur."'
                    AND (jdl.`kd_dosen` != '' OR jdl.`kd_dosen` IS NOT NULL) AND mk.`semester_matakuliah` = ".$v->semes." AND jdl.`waktu_kelas` = 'PG'
                    GROUP BY mk.`semester_matakuliah`")->row();

        $sr = $this->db->query("SELECT mk.`semester_matakuliah`,COUNT(kd_jadwal) AS jml FROM tbl_jadwal_matkul jdl
                    JOIN tbl_matakuliah mk ON jdl.`kd_matakuliah` = mk.`kd_matakuliah` 
                    WHERE SUBSTRING_INDEX(jdl.`kd_jadwal`, '/', 1) = '".$jur."' AND jdl.`kd_tahunajaran` = '".$thn."' AND mk.`kd_prodi` = '".$jur."'
                    AND (jdl.`kd_dosen` != '' OR jdl.`kd_dosen` IS NOT NULL) AND mk.`semester_matakuliah` = ".$v->semes." AND jdl.`waktu_kelas` = 'SR'
                    GROUP BY mk.`semester_matakuliah`")->row();

        $pk = $this->db->query("SELECT mk.`semester_matakuliah`,COUNT(kd_jadwal) AS jml FROM tbl_jadwal_matkul jdl
                    JOIN tbl_matakuliah mk ON jdl.`kd_matakuliah` = mk.`kd_matakuliah` 
                    WHERE SUBSTRING_INDEX(jdl.`kd_jadwal`, '/', 1) = '".$jur."' AND jdl.`kd_tahunajaran` = '".$thn."' AND mk.`kd_prodi` = '".$jur."'
                    AND (jdl.`kd_dosen` != '' OR jdl.`kd_dosen` IS NOT NULL) AND mk.`semester_matakuliah` = ".$v->semes." AND jdl.`waktu_kelas` = 'PK'
                    GROUP BY mk.`semester_matakuliah`")->row();
        

        // if ($pg->jml == 0) {
        //     $excel->getActiveSheet()->setCellValue('B'.$hy, 0);
        // }else{
            $excel->getActiveSheet()->setCellValue('B'.$hy, 0);
        //}

        // if ($sr->jml == 0) {
        //     $excel->getActiveSheet()->setCellValue('C'.$hy, 0);
        // }else{
            $excel->getActiveSheet()->setCellValue('C'.$hy, 0);
        //}

        // if ($pk->jml == 0) {
        //     $excel->getActiveSheet()->setCellValue('D'.$hy, 0);
        // }else{
            $excel->getActiveSheet()->setCellValue('D'.$hy, 0);
        //}
        $hy++;
    } //end foreach semester

    //header
    $aa = 3;
    $h=$hy+$aa;
    $excel->getActiveSheet()->setCellValue('A'.$h, 'NO');
    $excel->getActiveSheet()->setCellValue('B'.$h, 'KELAS');
    $excel->getActiveSheet()->setCellValue('C'.$h, 'NID');
    $excel->getActiveSheet()->setCellValue('D'.$h, 'DOSEN');
    $excel->getActiveSheet()->setCellValue('E'.$h, 'KODE MK');
    $excel->getActiveSheet()->setCellValue('F'.$h, 'MATAKULIAH');
    $excel->getActiveSheet()->setCellValue('G'.$h, 'SKS');
    $excel->getActiveSheet()->setCellValue('H'.$h, 'KELAS');
    $excel->getActiveSheet()->setCellValue('J'.$h, 'WAKTU');

    //isi
    $a=$h+1;
    $no=1;
    foreach ($rows as $key) {

        if($key->kd_dosen != NULL || $key->kd_dosen != ''){ $kd=$key->kd_dosen;}else{$kd="-";}

        //if($key->tetap == 1 ){$tetap = 'Tetap';}else{$tetap = 'Tidak Tetap';}

        $excel->getActiveSheet()->setCellValue('A'.$a, $no);
        $excel->getActiveSheet()->setCellValue('B'.$a, $kd);
        $excel->getActiveSheet()->setCellValue('C'.$a, $kd);
        $excel->getActiveSheet()->setCellValue('D'.$a, $key->nama);
        $excel->getActiveSheet()->setCellValue('E'.$a, $key->kd_matakuliah);
        $excel->getActiveSheet()->setCellValue('F'.$a, $key->nama_matakuliah);
        $excel->getActiveSheet()->setCellValue('G'.$a, $key->sks_matakuliah);
        $excel->getActiveSheet()->setCellValue('H'.$a, $key->kelas);
        $excel->getActiveSheet()->setCellValue('J'.$a, $key->waktu_kelas);
                    
        $no++;
        $a++;
    }//end foreach kelas

    //align
    $style = array(
        'alignment' => array(
            'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
        )
    );

    $excel->getActiveSheet()->getStyle("C1:E2")->applyFromArray($style);
    $excel->createSheet();

$shit++;

$filename = 'Data_Jumlah_kelas_'.$ts->prodi.'.xls'; //save our workbook as this file name
header('Content-Type: application/vnd.ms-excel'); //mime type
header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
header('Cache-Control: max-age=0'); //no cache
$objWriter = PHPExcel_IOFactory::createWriter($excel, 'Excel5');  
//force user to download the Excel file without writing it to server's HD
$objWriter->save('php://output');
?>