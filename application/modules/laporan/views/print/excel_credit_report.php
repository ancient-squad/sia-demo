<?php
	header("Content-Type: application/xls");    
	header("Content-Disposition: attachment; filename=beban_mengajar_dosen_".getactyear().".xls");  
	header("Pragma: no-cache"); 
	header("Expires: 0");
?>

<table id="example1" class="table table-bordered table-striped">
	<thead>
        <tr> 
        	<th>No</th>
            <th>NIDN</th>
            <th>NUPN</th>
            <th>NAMA</th>
            <th>SKS</th>
        </tr>
    </thead>
    <tbody>
        <?php $no = 1; foreach ($data as $value) { ?>
        <tr>
        	<td><?= $no; ?></td>
        	<td><?= (!empty($value->nidn) OR !is_null($value->nidn)) ? $value->nidn : '-'; ?></td>
        	<td><?= (!empty($value->nupn) OR !is_null($value->nupn)) ? $value->nupn : '-'; ?></td>
        	<td><?= $value->nama; ?></td>
        	<td><?= $value->total_sks; ?></td>
        </tr>
        <?php $no++; } ?>
    </tbody>
</table>