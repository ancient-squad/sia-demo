<?php
header("Content-Type: application/xls");
header("Content-Disposition: attachment; filename=LAPORAN_PENGISIAN_KRS.xls");
header("Pragma: no-cache");
header("Expires: 0");
?>

<style>
    table,
    td,
    th {
        border: 1px solid black;
    }

    th {
        background-color: blue;
        color: black;
    }
</style>
<?php if ($log == 8) {
    $prodi = ' - ' . get_jur($userid);
} else {
    $prodi = '';
}
?>
<table>
    <tr>
        <td colspan="11" style="font-weight:bold;">
            <center>Laporan Pengisian KRS <?php echo $this->session->userdata('ta') . $prodi ?></center>
        </td>
    </tr>
    <tr>
    </tr>
</table>
<table border="1px" class="table table-bordered table-striped">
    <thead>
        <tr>
            <th>No</th>
            <th>Fakultas</th>
            <th>Prodi</th>
            <th>Jumlah Mahasiswa <?php echo $before_year ?></th>
            <th>Revisi</th>
            <th>Verifikasi</th>
            <th>Terajukan</th>
            <th>Belum Diajukan</th>
            <th>Sudah isi KRS</th>
            <th>Belum isi KRS</th>
            <th>Persentase</th>
        </tr>
    </thead>

    <tbody>
        <?php
        $i = 1;
        $arrow = '';
        foreach ($ini as $row => $rw) {
            // $arrow .= $row . ',';
            $arrfak = [];
            $fakultas = '';
            foreach ($rw as $val => $vals) {
                if (!in_array($row, $arrfak)) {
                    $arrfak[] = $row;
                    $no = '<td rowspan="' . count($rw) . '">' . $i . '</td>';
                    $fakultas = '<td rowspan="' . count($rw) . '">' . $row . '</td>';
                } else {
                    $no = '';
                    $fakultas = '';
                }
        ?>
                <tr>
                    <?php echo $no; ?>
                    <?php echo $fakultas ?>
                    <td><?php echo $val ?></td>
                    <td><?php echo $vals['jml'] ?></td>
                    <td><?php echo $vals['revisi'] ?></td>
                    <td><?php echo $vals['verifikasi'] ?></td>
                    <td><?php echo $vals['terajukan'] ?></td>
                    <td><?php echo $vals['belum_diajukan'] ?></td>
                    <td><?php echo $vals['total_krsan'] ?></td>
                    <td><?php echo $vals['belum_input'] ?></td>
                    <td><?php echo $vals['persentase'] ?></td>
                </tr>
        <?php
            }
            $i++;
        }
        ?>
    </tbody>
</table>