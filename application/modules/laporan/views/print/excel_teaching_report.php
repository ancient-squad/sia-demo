<?php
	header("Content-Type: application/xls");    
	header("Content-Disposition: attachment; filename=detail_mengajar_".$name."_".getactyear().".xls");  
	header("Pragma: no-cache"); 
	header("Expires: 0");
?>

<table id="example1" class="table table-bordered table-striped">
	<thead>
        <tr> 
        	<th>No</th>
            <th>Kode Mata Kuliah</th>
            <th>Nama Mata Kuliah</th>
            <th>SKS</th>
            <th>Kelas</th>
            <th>Hari</th>
            <th>Waktu Mulai</th>
            <th>Waktu Selesai</th>
        </tr>
    </thead>
    <tbody>
        <?php $no = 1; foreach ($data as $value) { ?>
        <tr>
        	<td><?= $no; ?></td>
        	<td><?= $value->kd_matakuliah ?></td>
        	<td><?= $value->nama_matakuliah ?></td>
        	<td><?= $value->sks_matakuliah; ?></td>
        	<td><?= $value->kelas; ?></td>
        	<td><?= notohari($value->hari) ?></td>
        	<td><?= $value->waktu_mulai; ?></td>
        	<td><?= $value->waktu_selesai; ?></td>
        </tr>
        <?php $no++; } ?>
    </tbody>
</table>