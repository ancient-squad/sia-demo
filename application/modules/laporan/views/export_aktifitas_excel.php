<?php
	header("Content-Type: application/xls");    
	header("Content-Disposition: attachment; filename=export_aktifitas_mahasiswa_".$tahunajar.".xls");  
	header("Pragma: no-cache"); 
	header("Expires: 0");
?>

<table id="example1" class="table table-bordered table-striped">
	<thead>
        <tr> 
        	<th>No</th>
            <th>NPM</th>
            <th>NAMA</th>
            <th>PRODI</th>
            <th>JENIS</th>
            <th>JUDUL</th>
            <th>LOKASI</th>
            <th>No. SK</th>
            <th>Tanggal SK</th>
        </tr>
    </thead>
    <tbody>
        <?php $no = 1; foreach ($data as $value) { ?>
        <tr>
        	<td><?= $no ?></td>
        	<td><?= $value->npm ?></td>
        	<td><?= $value->NMMHSMSMHS ?></td>
        	<td><?= get_jur($value->KDPSTMSMHS) ?></td>
        	<td><?= $value->jenis; ?></td>
        	<td><?= $value->judul; ?></td>
        	<td><?= $value->lokasi ?></td>
        	<td><?= $value->no_sk_tugas ?></td>
        	<td><?= $value->tgl_sk_tugas ?></td>
        </tr>
        <?php $no++; } ?>
    </tbody>
</table>