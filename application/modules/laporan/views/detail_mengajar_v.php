<div class="row">
	<div class="span12">      		  		
  		<div class="widget ">
  			<div class="widget-header">
  				<i class="icon-file"></i>
  				<h3>Data mengajar <?= nama_dsn($nid).' Tahun Ajaran '.get_thnajar($tahunajar); ?></h3>
			</div> 

			<div class="widget-content">
				<div class="span11">
                    
                    <a href="<?= base_url('laporan/beban_sks_dosen/print_teaching_report/'.$nid); ?>" class="btn btn-success">
                    	<i class="btn-icon-only icon-print"> </i> Print Excel
                    </a>
                    <hr>
					<table id="example1" class="table table-bordered table-striped">
	                	<thead>
	                        <tr> 
	                        	<th>No</th>
                                <th>Kode Mata Kuliah</th>
                                <th>Nama Mata Kuliah</th>
                                <th>SKS</th>
                                <th>Kelas</th>
                                <th>Hari</th>
                                <th>Waktu Mulai</th>
	                            <th>Waktu Selesai</th>
	                        </tr>
	                    </thead>
	                    <tbody>
                            <?php $no = 1; foreach ($data as $value) { ?>
	                        <tr>
	                        	<td><?= $no; ?></td>
	                        	<td><?= $value->kd_matakuliah ?></td>
	                        	<td><?= $value->nama_matakuliah ?></td>
	                        	<td><?= $value->sks_matakuliah; ?></td>
	                        	<td><?= $value->kelas; ?></td>
	                        	<td><?= notohari($value->hari) ?></td>
	                        	<td><?= $value->waktu_mulai; ?></td>
	                        	<td><?= $value->waktu_selesai; ?></td>
	                        </tr>
                            <?php $no++; } ?>
	                    </tbody>
	               	</table>
				</div>
			</div>
		</div>
	</div>
</div>