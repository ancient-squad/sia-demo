<?php
header("Content-Type: application/xls");    
header("Content-Disposition: attachment; filename=DATA_AKTIFITAS_MHS_.xls");
header("Pragma: no-cache"); 
header("Expires: 0");
?>

<style>
table, td, th {
    border: 1px solid black;
}

th {
    background-color: blue;
    color: black;
}
</style>

<table id="example1" class="table table-bordered table-striped">
	<thead>
	    <tr> 
	    	<th>No</th>
	        <th>NPM</th>
	        <th>SKS</th>
	        <th>IPS</th>
	        <th>Total SKS</th>
	        <th>IPK</th> 
	    </tr>
	</thead>
	<tbody>
	<?php foreach($value as $row){?>
	    <tr>
	    	<td><?php echo $row->NIMHSTRAKM; ?></td>
	    	<td><?php echo $row->SKSEMTRAKM; ?></td>
	    	<td><?php echo $row->NLIPSTRAKM; ?></td>
	    	<td><?php echo $row->SKSTTTRAKM; ?></td>
	    	<td><?php echo $row->NLIPKTRAKM; ?></td>
	    </tr>
	<?php } ?>
	</tbody>
</table>