<?php
header("Content-Type: application/xls");    
header("Content-Disposition: attachment; filename=hasil_evaluasi_dosen_". changeTo_($this->session->userdata('nama_dosen')).".xls");  
header("Pragma: no-cache"); 
header("Expires: 0");
?>

<style>
	table, td, th {
	    border: 1px solid black;
	}

	th {
	    background-color: blue;
	    color: black;
	}
</style>


<table id="example1" class="table table-bordered table-striped">
	<thead>
		<tr>
			<td colspan="3" style="text-align:center;vertical-align:middle;background:#8FBC8B;">
				<h4>Data Evaluasi Dosen <?php echo $this->session->userdata('nama_dosen').' # '.$kdjadwal->kelas; ?> <?php $tahunajar = $this->app_model->getdetail('tbl_tahunakademik','kode',$this->session->userdata('ta'),'kode','asc')->row()->tahun_akademik; echo $tahunajar; ?></h4>
			</td>
		</tr>
	    <tr style="background:yellow"> 
	    	<th>No</th>
	        <th>Parameter</th>
	        <th>Nilai</th>
	    </tr>
	</thead>
	<tbody>
	    <?php $no = 1; foreach ($getData as $value) { ?>
	    <tr>
	    	<td><?php echo number_format($no); ?></td>
	    	<td><?php echo $value->parameter; ?></td>
	    	<?php 	$this->db2 = $this->load->database('eval', TRUE); 
	    			if ($this->session->userdata('ta') < '20171') {
        				$nilairat = $this->db2->query("SELECT AVG(nilai) as nilaibro from tbl_nilai_parameter where parameter_id = ".$value->id_parameter." and kd_jadwal = '".$kdjadwal->kd_jadwal."'")->row()->nilaibro;
        			} else {
        				$nilairat = $this->db2->query("SELECT AVG(nilai) as nilaibro from tbl_nilai_parameter_".$this->session->userdata('ta')." where parameter_id = ".$value->id_parameter." and kd_jadwal = '".$kdjadwal->kd_jadwal."'")->row()->nilaibro;
        			}
	    	?>

	    	<!-- nilai rata-rata -->
            <?php if ($this->session->userdata('ta') < '20172') {
                $average = number_format(($nilairat/20),2);
            } else {
                $average = number_format(($nilairat*1.25),2);
            }
             ?>

	    	<td><?php echo $average; ?></td>
	    </tr>
	    <?php $no++; } ?>
	</tbody>
</table>