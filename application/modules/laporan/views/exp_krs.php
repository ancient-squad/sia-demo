<?php
header("Content-Type: application/xls");    
header("Content-Disposition: attachment; filename=DATA_KRS_MHS_".$pro.".xls");
header("Pragma: no-cache"); 
header("Expires: 0");
?>

<style>
table, td, th {
    border: 1px solid black;
}

th {
    background-color: blue;
    color: black;
}
</style>

<table id="example1" class="table table-bordered table-striped">
	<thead>
		<tr>
			<th colspan="5" style="text-align: center;">DATA KRS MAHASISWA <?= strtoupper($pro) ?></th>
		</tr>
	    <tr> 
	    	<th>No</th>
	        <th>NPM</th>
	        <th>NAMA</th>
	        <th>Semester</th>
	        <th>Total SKS</th>
	    </tr>
	</thead>
	<tbody>
		<?php $no = 1; foreach($quer as $row){?>
	    <tr>
	    	<td><?php echo $no; ?></td>
	    	<td><?php echo $row->npm_mahasiswa; ?></td>
	    	<td><?php echo $row->NMMHSMSMHS; ?></td>
	    	<td><?php echo $row->semester_krs; ?></td>
	    	<td><?php echo $row->tot; ?></td>
	    </tr>
		<?php $no++; } ?>
	  
	</tbody>
</table>