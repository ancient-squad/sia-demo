<div class="row">
  <div class="span12">
    <div class="widget ">
      <div class="widget-header">
        <i class="icon-user"></i>
        <h3>Laporan KRS</h3>
      </div> <!-- /widget-header -->
      <div class="widget-content">
        <div class="span11">
          <form method="post" class="form-horizontal" action="<?php echo base_url(); ?>laporan/laporan_krs/save_session">
            <fieldset>
              <div class="control-group">
                <label class="control-label">Tahun Akademik</label>
                <div class="controls">
                  <select class="form-control span6" name="tahunajaran" id="ta">
                    <option disabled selected>--Pilih Tahun Akademik--</option>
                    <?php foreach ($tahunajar as $value) {
                      echo "<option value='" . $value->kode . "'> " . $value->tahun_akademik . " </option>";
                    } ?>
                  </select>
                </div>
              </div>
              <br />
              <div class="form-actions">
                <input type="submit" class="btn btn-large btn-success" value="Submit" />
              </div> <!-- /form-actions -->
            </fieldset>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>