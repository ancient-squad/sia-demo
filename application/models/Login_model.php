<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class login_model extends CI_Model
{

	function cekuser($user, $pass)
	{
		$this->db->select('*');
		$this->db->from('tbl_user_login');
		$this->db->where('username', $user);
		$this->db->where('password', sha1(md5($pass) . key));
		$this->db->where('status', 1);
		$q = $this->db->get();

		return $q;
	}

	function datauser($user, $pass, $type)
	{
		if ($type == 1) {

			$this->db->select('*');
			$this->db->from('tbl_user_login a');
			$this->db->join('tbl_karyawan c', 'a.userid = c.nid');
			$this->db->where('a.username', $user);
			$this->db->where('a.password', sha1(md5($pass) . key));
			$this->db->where('a.status', 1);
		} elseif ($type == 2) {

			$this->db->select('*');
			$this->db->from('tbl_user_login a');
			$this->db->join('tbl_mahasiswa c', 'a.userid = c.NIMHSMSMHS');
			$this->db->where('a.username', $user);
			$this->db->where('a.password', sha1(md5($pass) . key));
			$this->db->where('a.status', 1);
		} elseif ($type == 3) {

			$this->db->select('*');
			$this->db->from('tbl_user_login a');
			$this->db->join('tbl_divisi c', 'a.userid = c.kd_divisi');
			$this->db->where('a.username', $user);
			$this->db->where('a.password', sha1(md5($pass) . key));
			$this->db->where('a.status', 1);
		} elseif ($type == 5 || $type == 6 || $type == 7 || $type == 8) {

			$this->db->select('*');
			$this->db->from('tbl_user_login a');
			$this->db->join('tbl_divisi c', 'a.username = c.kd_divisi');
			$this->db->where('a.username', $user);
			$this->db->where('a.password', sha1(md5($pass) . key));
			$this->db->where('a.status', 1);
		} else {

			$this->db->select('*');
			$this->db->from('tbl_user_login a');
			$this->db->join('tbl_karyawan_2 c', 'a.userid = c.nip');
			$this->db->where('a.username', $user);
			$this->db->where('a.password', sha1(md5($pass) . key));
			$this->db->where('a.status', 1);
		}

		$q = $this->db->get();
		return $q;
	}

	function loginnilai($user)
	{
		$this->db->select('*');
		$this->db->from('tbl_user_login a');
		$this->db->join('tbl_karyawan c', 'a.userid = c.nid');
		$this->db->where('a.username', $user);
		$this->db->where('a.status', 1);
		$q = $this->db->get();

		return $q;
	}

	function cekmbalia($pass)
	{
		$this->db->select('*');
		$this->db->from('tbl_verifikasi_mbalia');
		$this->db->where('kode', sha1(md5($pass) . key_mbalia));
		$q = $this->db->get();

		return $q;
	}

	function cek_kode($kode, $id)
	{
		$this->db->select('*');
		$this->db->from('tbl_jadwal_matkul');
		$this->db->where('kd_dosen', $id);
		$this->db->where('kd_jadwal like "%' . $kode . '%"');
		$q = $this->db->get();

		return $q;
	}
}


/* End of file login_model.php */

/* Location: ./application/models/login_model.php */
