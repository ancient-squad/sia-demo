<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Feeder_perkuliahan_model extends CI_Model {

	function get_class_participant($kodematakuliah, $tahunajaran, $kodejadwal)
	{
		return $this->db->query("SELECT DISTINCT a.npm_mahasiswa FROM tbl_krs_feeder a 
                                JOIN tbl_verifikasi_krs c ON a.kd_krs = c.kd_krs
                                WHERE a.kd_matakuliah = '$kodematakuliah' 
                                AND c.tahunajaran = '$tahunajaran' 
                                AND a.kd_jadwal = '$kodejadwal'")->result();
	}

	function get_akm($year, $prodi, $angkatan)
	{
		$akm = $this->db->select('a.NIMHSMSMHS,a.NMMHSMSMHS,a.TAHUNMSMHS,a.kategori_kelas, a.biaya_semester, b.*, c.kd_krs')
						->from('tbl_mahasiswa a')
						->join('tbl_aktifitas_kuliah_mahasiswa b','a.NIMHSMSMHS = b.NIMHSTRAKM')
						->join('tbl_verifikasi_krs c','c.npm_mahasiswa = a.NIMHSMSMHS')
						->where('b.THSMSTRAKM', $year)
						->where('b.KDPSTTRAKM', $prodi)
						->where('c.tahunajaran', $year)
						->like('b.NIMHSTRAKM', $angkatan, 'after')
						->get()->result();
    	return $akm;
	}

	public function get_krs($angkatan, $userid, $year)
	{
		$data = $this->db->query("SELECT 
									mhs.NIMHSMSMHS, 
									mhs.NMMHSMSMHS, 
									mhs.TAHUNMSMHS,
									mhs.biaya_semester,
									krs.kd_krs,
									SUM(mk.sks_matakuliah) AS sks 
								FROM tbl_mahasiswa mhs 
								JOIN tbl_krs_feeder krs ON mhs.NIMHSMSMHS = krs.npm_mahasiswa 
								JOIN tbl_matakuliah mk ON mk.kd_matakuliah = krs.kd_matakuliah
								WHERE 
									CASE WHEN SUBSTRING_INDEX(mk.kd_matakuliah, '-', 1) <> 'MKDU'
									OR SUBSTRING_INDEX(mk.kd_matakuliah, '-', 1) <> 'MKU'
									OR SUBSTRING_INDEX(mk.kd_matakuliah, '-', 1) <> 'MKWU'
									THEN mk.kd_prodi = '$userid' ELSE TRUE END
								AND mhs.TAHUNMSMHS = '$angkatan'
								AND mhs.KDPSTMSMHS = '$userid'
								AND krs.kd_krs LIKE CONCAT(krs.npm_mahasiswa, '$year%')
								GROUP BY mhs.NIMHSMSMHS")->result();
		return $data;
	}

	function get_cuti_list($act_year, $prodi)
	{
		$list = $this->db->query("SELECT
									b.NIMHSMSMHS,
									b.NMMHSMSMHS,
									a.index_biaya,

									(SELECT NLIPKTRAKM FROM tbl_aktifitas_kuliah_mahasiswa takm 
									WHERE NIMHSTRAKM = a.npm 
									AND THSMSTRAKM = 
										(SELECT MAX(THSMSTRAKM) 
										FROM tbl_aktifitas_kuliah_mahasiswa 
										WHERE NIMHSTRAKM = a.npm)) AS ipk,

									(SELECT SKSTTTRAKM FROM tbl_aktifitas_kuliah_mahasiswa takm 
									WHERE NIMHSTRAKM = a.npm 
									AND THSMSTRAKM = 
										(SELECT MAX(THSMSTRAKM) 
										FROM tbl_aktifitas_kuliah_mahasiswa 
										WHERE NIMHSTRAKM = a.npm)) AS sks
								FROM
									tbl_status_mahasiswa a
								JOIN tbl_mahasiswa b ON
									a.npm = b.NIMHSMSMHS
								WHERE
									a.status = 'C'
									AND a.tahunajaran = '$act_year'
									AND b.KDPSTMSMHS = '$prodi'
									AND a.validate = 1")->result();
		return $list;
	}

	function get_unsync_cuti($act_year, $prodi, $mahasiswa)
	{
		$list = $this->db->query("SELECT
									b.NIMHSMSMHS,
									b.NMMHSMSMHS,
									b.biaya_semester,

									(SELECT NLIPKTRAKM FROM tbl_aktifitas_kuliah_mahasiswa takm 
									WHERE NIMHSTRAKM = a.npm 
									AND THSMSTRAKM = 
										(SELECT MAX(THSMSTRAKM) 
										FROM tbl_aktifitas_kuliah_mahasiswa 
										WHERE NIMHSTRAKM = a.npm)) AS ipk,

									(SELECT SKSTTTRAKM FROM tbl_aktifitas_kuliah_mahasiswa takm 
									WHERE NIMHSTRAKM = a.npm 
									AND THSMSTRAKM = 
										(SELECT MAX(THSMSTRAKM) 
										FROM tbl_aktifitas_kuliah_mahasiswa 
										WHERE NIMHSTRAKM = a.npm)) AS sks
								FROM
									tbl_status_mahasiswa a
								JOIN tbl_mahasiswa b ON
									a.npm = b.NIMHSMSMHS
								WHERE
									a.status = 'C'
									AND a.tahunajaran = '$act_year'
									AND b.KDPSTMSMHS = '$prodi'
									AND a.npm NOT IN $mahasiswa
									AND a.validate = 1")->result();
		return $list;
	}

	function get_non_active($prodi, $actyear, $start_year)
	{
		$nonactive 	= $this->db->query("SELECT DISTINCT 
											NIMHSMSMHS,
											NMMHSMSMHS,
											biaya_semester,

											(SELECT NLIPKTRAKM FROM tbl_aktifitas_kuliah_mahasiswa takm 
											WHERE NIMHSTRAKM = a.NIMHSMSMHS 
											AND THSMSTRAKM = 
												(SELECT MAX(THSMSTRAKM) 
												FROM tbl_aktifitas_kuliah_mahasiswa 
												WHERE NIMHSTRAKM = a.NIMHSMSMHS)) AS ipk,

											(SELECT SKSTTTRAKM FROM tbl_aktifitas_kuliah_mahasiswa takm 
											WHERE NIMHSTRAKM = a.NIMHSMSMHS 
											AND THSMSTRAKM = 
												(SELECT MAX(THSMSTRAKM) 
												FROM tbl_aktifitas_kuliah_mahasiswa 
												WHERE NIMHSTRAKM = a.NIMHSMSMHS)) AS sks

										FROM tbl_mahasiswa a
										WHERE a.NIMHSMSMHS NOT IN (
										    SELECT npm_mahasiswa FROM tbl_verifikasi_krs 
										    WHERE tahunajaran = '$actyear'
										) AND a.NIMHSMSMHS NOT IN (
										    SELECT npm FROM tbl_status_mahasiswa 
										    WHERE tahunajaran = '$actyear' 
										    AND validate = 1
										) 
										AND a.KDPSTMSMHS = '$prodi' 
										AND a.BTSTUMSMHS >= '$actyear' 
										AND a.STMHSMSMHS IN ('A','N') 
										AND a.SMAWLMSMHS >= '$start_year'
										")->result();
		return $nonactive;
	}

	function get_nonactive_current_year($prodi, $tahunakademik, $start_year)
	{
		$nonactive 	= $this->db->query("SELECT DISTINCT 
											NIMHSMSMHS,
											NMMHSMSMHS,
											biaya_semester,

											(SELECT NLIPKTRAKM FROM tbl_aktifitas_kuliah_mahasiswa takm 
											WHERE NIMHSTRAKM = a.NIMHSMSMHS 
											AND THSMSTRAKM = 
												(SELECT MAX(THSMSTRAKM) 
												FROM tbl_aktifitas_kuliah_mahasiswa 
												WHERE NIMHSTRAKM = a.NIMHSMSMHS)) AS ipk,

											(SELECT SKSTTTRAKM FROM tbl_aktifitas_kuliah_mahasiswa takm 
											WHERE NIMHSTRAKM = a.NIMHSMSMHS 
											AND THSMSTRAKM = 
												(SELECT MAX(THSMSTRAKM) 
												FROM tbl_aktifitas_kuliah_mahasiswa 
												WHERE NIMHSTRAKM = a.NIMHSMSMHS)) AS sks

										FROM tbl_mahasiswa a
										WHERE a.NIMHSMSMHS NOT IN (
										    SELECT npm_mahasiswa FROM tbl_verifikasi_krs 
										    WHERE tahunajaran = '$tahunakademik'
										) AND a.NIMHSMSMHS NOT IN (
										    SELECT npm FROM tbl_status_mahasiswa 
										    WHERE tahunajaran = '$tahunakademik' 
										    AND validate = 1
										) 
										AND a.KDPSTMSMHS = '$prodi' 
										AND a.STMHSMSMHS IN ('A','N') 
										AND a.SMAWLMSMHS <= '$tahunakademik'
										AND a.SMAWLMSMHS >= '$start_year'
										")->result();
		return $nonactive;
	}

	function get_unsync_nonaktif($actyear, $prodi, $mhs)
	{
		$start_year = studyStart($actyear);
		$nonactive 	= $this->db->query("SELECT DISTINCT 
											a.NIMHSMSMHS,
											a.NMMHSMSMHS,
											a.biaya_semester,

											(SELECT NLIPKTRAKM FROM tbl_aktifitas_kuliah_mahasiswa takm 
											WHERE NIMHSTRAKM = a.NIMHSMSMHS 
											AND THSMSTRAKM = 
												(SELECT MAX(THSMSTRAKM) 
												FROM tbl_aktifitas_kuliah_mahasiswa 
												WHERE NIMHSTRAKM = a.NIMHSMSMHS)) AS ipk,

											(SELECT SKSTTTRAKM FROM tbl_aktifitas_kuliah_mahasiswa takm 
											WHERE NIMHSTRAKM = a.NIMHSMSMHS 
											AND THSMSTRAKM = 
												(SELECT MAX(THSMSTRAKM) 
												FROM tbl_aktifitas_kuliah_mahasiswa 
												WHERE NIMHSTRAKM = a.NIMHSMSMHS)) AS sks

										FROM tbl_mahasiswa a
										WHERE a.NIMHSMSMHS NOT IN (
										    SELECT npm_mahasiswa FROM tbl_verifikasi_krs 
										    WHERE tahunajaran = '$actyear'
										) AND a.NIMHSMSMHS NOT IN (
										    SELECT npm FROM tbl_status_mahasiswa 
										    WHERE tahunajaran = '$actyear' 
										    AND validate = 1
										) 
										AND a.NIMHSMSMHS NOT IN $mhs
										AND a.KDPSTMSMHS = '$prodi' 
										AND a.BTSTUMSMHS >= '$actyear' 
										AND a.STMHSMSMHS IN ('A','N') 
										AND a.SMAWLMSMHS >= '$start_year'
										")->result();
		return $nonactive;
	}

	function get_keluar($prodi)
	{
		$out = $this->db->query("SELECT p.*, mhs.NMMHSMSMHS FROM tbl_pengunduran p
								JOIN tbl_mahasiswa mhs ON p.npm = mhs.NIMHSMSMHS
								WHERE (tipe = 'K' OR tipe = 'D')
								AND prodi = '$prodi'")->result();
		return $out;
	}

	function get_dropout($prodi, $tahunakademik)
	{
		$dropout = $this->db->query("SELECT 
										mhs.NIMHSMSMHS,
										mhs.NMMHSMSMHS,
										mhs.biaya_semester,

										(SELECT NLIPKTRAKM FROM tbl_aktifitas_kuliah_mahasiswa takm 
											WHERE NIMHSTRAKM = mhs.NIMHSMSMHS 
											AND THSMSTRAKM = 
												(SELECT MAX(THSMSTRAKM) 
												FROM tbl_aktifitas_kuliah_mahasiswa 
												WHERE NIMHSTRAKM = mhs.NIMHSMSMHS)) AS ipk,

										(SELECT SKSTTTRAKM FROM tbl_aktifitas_kuliah_mahasiswa takm 
										WHERE NIMHSTRAKM = mhs.NIMHSMSMHS 
										AND THSMSTRAKM = 
											(SELECT MAX(THSMSTRAKM) 
											FROM tbl_aktifitas_kuliah_mahasiswa 
											WHERE NIMHSTRAKM = mhs.NIMHSMSMHS)) AS sks

									FROM tbl_dropout do
									JOIN tbl_mahasiswa mhs ON do.npm_mahasiswa = mhs.NIMHSMSMHS
									WHERE do.tahunajaran = '$tahunakademik' 
									AND mhs.KDPSTMSMHS = '$prodi' ")->result();
		return $dropout;
	}

	function get_lulus($prodi, $year)
	{
		$next = $year+1;
		$graduate 	= $this->db->query("SELECT * FROM tbl_mahasiswa 
										WHERE STMHSMSMHS = 'L'
										AND KDPSTMSMHS = '$prodi' 
										AND (YEAR(TGLLSMSMHS) = '$year' OR YEAR(TGLLSMSMHS) = '$next')")->result();
		return $graduate;
	}

	function get_unit_mhs($nim)
	{
		$last_study_year = $this->db->query("SELECT MAX(THSMSTRAKM) AS last_year FROM tbl_aktifitas_kuliah_mahasiswa
											WHERE NIMHSTRAKM = '$nim' ")->row()->last_year;

		$data = $this->db->query("SELECT a.*, b.biaya_semester FROM tbl_aktifitas_kuliah_mahasiswa a
								JOIN tbl_mahasiswa b ON a.NIMHSTRAKM = b.NIMHSMSMHS
								WHERE a.NIMHSTRAKM = '$nim'
								AND a.THSMSTRAKM = '$last_study_year'")->row();
		return $data;
	}

	function get_akm_unit($npm)
	{
		$last_study_year = $this->db->query("SELECT MAX(THSMSTRAKM) AS last_year FROM tbl_aktifitas_kuliah_mahasiswa
											WHERE NIMHSTRAKM = '$npm' ")->row()->last_year;

		$param = ['NIMHSTRAKM' => $npm, 'THSMSTRAKM' => $last_study_year];
    	$data = $this->db->get_where('tbl_aktifitas_kuliah_mahasiswa', $param)->row();
    	return $data;
	}

	function update_akm_sia($tahunakademik, $npm, $sks, $ipk, $userid)
    {
    	$object = ['SKSTTTRAKM' => $sks, 'NLIPKTRAKM' => $ipk, 'updated_at' => date('Y-m-d H:i:s'), 'updated_by' => $userid];
    	$this->db->where('THSMSTRAKM', $tahunakademik);
    	$this->db->where('NIMHSTRAKM', $npm);
    	$this->db->update('tbl_aktifitas_kuliah_mahasiswa', $object);
    	return;
    }

    function get_tabs()
    {
    	$data = $this->db->query("SELECT DISTINCT a.`semester_kd_matakuliah` FROM tbl_kurikulum_matkul_new a
                                JOIN tbl_kurikulum_new b ON a.kd_kurikulum = b.kd_kurikulum
                                WHERE a.`semester_kd_matakuliah` IS NOT NULL 
                                AND b.status = '1'
                                AND a.`semester_kd_matakuliah` <> ''
                                ORDER BY a.semester_kd_matakuliah ASC ")->result();
    	return $data;
    }

    function get_tabs_magister()
    {
    	$data = $this->db->query("SELECT DISTINCT a.`semester_kd_matakuliah` FROM tbl_kurikulum_matkul_new a
                                JOIN tbl_kurikulum_new b ON a.kd_kurikulum = b.kd_kurikulum
                                WHERE a.`semester_kd_matakuliah` IS NOT NULL 
                                AND b.status = '1'
                                AND a.`semester_kd_matakuliah` <> ''
                                AND a.semester_kd_matakuliah < 5
                                ORDER BY a.semester_kd_matakuliah ASC ")->result();
    	return $data;
    }

    function jdl_matkul_semester($smtr)
	{
		if ($this->session->userdata('tahunajaran_kelas') > 20162) {
			$tabel = 'tbl_kurikulum_matkul_new';
			$kur   = 'tbl_kurikulum_new';
		} else {
			$tabel = 'tbl_kurikulum_matkul';
			$kur   = 'tbl_kurikulum';
		}

		$tahunajaran = $this->session->userdata('tahunajaran_kelas')['tahunajaran'];
		$userid      = $this->session->userdata('sess_login')['id_user_group'] == '26' ? $this->session->userdata('tahunajaran_kelas')['prodi'] : $this->session->userdata('sess_login')['userid'];
		$kd_fakultas = get_kdfak_byprodi($userid);
		$get_kelas   = $this->db->query("SELECT distinct 
											mk.status_feeder,
											mk.id_jadwal,
											mk.kd_matakuliah,
											mk.kelas,
											mk.kd_jadwal, 
											a.semester_matakuliah,
											a.nama_matakuliah,
											a.sks_matakuliah, 
											kry.nupn,
											kry.nidn,
											kry.nid,
											kry.nama 
										FROM tbl_jadwal_matkul_feeder mk
		                                JOIN tbl_matakuliah a ON mk.`kd_matakuliah` = a.`kd_matakuliah`
		                                LEFT JOIN tbl_karyawan kry ON kry.`nid` = mk.`kd_dosen`
		                                JOIN $tabel km ON mk.`kd_matakuliah` = km.`kd_matakuliah`
		                                JOIN $kur kur ON kur.`kd_kurikulum` = km.`kd_kurikulum` 
		                                WHERE (mk.`kd_jadwal` LIKE '$userid/%' OR mk.kd_jadwal LIKE '$kd_fakultas/%')
		                                AND a.`kd_prodi` = '$userid'
		                                AND kur.`kd_prodi` = '$userid'
		                                AND kur.status = '1'
		                                AND mk.`kd_tahunajaran` = '$tahunajaran'
		                                AND km.`semester_kd_matakuliah` = '$smtr'
		                                order by mk.kd_matakuliah,mk.kelas ASC")->result();

		//$q = $this->db->query("CALL sp_jadwal(".$smtr.",'".$this->session->userdata('tahunajaran')."','".$as."')");
		return $get_kelas;
	}

	function get_jadwal($tahunakademik, $prodi)
	{
		$fakultas = get_kdfak_byprodi($prodi);

		$jadwal = $this->db->query("SELECT DISTINCT kd_jadwal,kelas,kd_matakuliah FROM tbl_jadwal_matkul_feeder 
									WHERE (kd_jadwal LIKE '$prodi/%' OR kd_jadwal LIKE '$fakultas/%')
									AND kd_tahunajaran = '$tahunakademik' ")->result();
		return $jadwal;
	}

	function jadwal_dosen($prodi, $tahunakademik)
	{
		$fakultas = get_kdfak_byprodi($prodi);

		$jadwal = $this->db->query("SELECT 
										ky.nama,
										ky.nid, 
										ky.nidn, 
										ky.nupn, 
										jdl.kd_matakuliah, 
										jdl.kelas,
										jdl.kd_tahunajaran,
										mk.sks_matakuliah 
									FROM tbl_jadwal_matkul_feeder jdl
									JOIN tbl_matakuliah mk ON jdl.kd_matakuliah = mk.kd_matakuliah
									JOIN tbl_karyawan ky ON jdl.kd_dosen = ky.nid
									WHERE (jdl.kd_jadwal LIKE '$prodi/%' OR jdl.kd_jadwal LIKE '$fakultas/%')
									AND jdl.kd_tahunajaran = '$tahunakademik'
									AND mk.kd_prodi = '$prodi'
									AND (ky.nidn <> '' OR ky.nupn <> '') ")->result();
		return $jadwal;
	}

	function get_nilai_kuliah($kode_jadwal, $tahunakademik)
	{
		$data = $this->db->query("SELECT kr.npm_mahasiswa, tn.nilai_akhir, tn.BOBOTTRLNM, tn.NLAKHTRLNM FROM tbl_krs_feeder kr 
								JOIN tbl_transaksi_nilai tn ON kr.npm_mahasiswa = tn.NIMHSTRLNM
								WHERE kr.kd_jadwal = '$kode_jadwal'
								AND tn.THSMSTRLNM = '$tahunakademik'
								AND tn.KDKMKTRLNM = kr.kd_matakuliah")->result();
		return $data;
	}

	function print_schedule($prodi, $tahunakademik)
	{
		$fakultas = get_kdfak_byprodi($prodi);
		$data = $this->db->query("SELECT * FROM tbl_jadwal_matkul_feeder jdl
                                JOIN tbl_matakuliah mk ON jdl.`kd_matakuliah`=mk.`kd_matakuliah`
                                JOIN tbl_fakultas b ON mk.`kd_fakultas` = b.`kd_fakultas`
                                LEFT JOIN tbl_karyawan kry ON kry.`nid` = jdl.`kd_dosen`
                                JOIN tbl_kurikulum_matkul_new km ON jdl.`kd_matakuliah` = km.`kd_matakuliah`
                                JOIN tbl_kurikulum_new kur ON kur.`kd_kurikulum` = km.`kd_kurikulum` 
                                WHERE (jdl.`kd_jadwal` LIKE '$prodi%' OR jdl.kd_jadwal LIKE '$fakultas/%')
                                AND mk.`kd_prodi` = '$prodi'
                                AND kur.`kd_prodi` = '$prodi'
                                AND jdl.`kd_tahunajaran` LIKE '$tahunakademik%'
                                GROUP BY jdl.`kd_jadwal`
                                ORDER BY semester_kd_matakuliah");
		return $data;
	}

}

/* End of file Feeder_perkuliahan_model.php */
/* Location: ./application/models/Feeder_perkuliahan_model.php */