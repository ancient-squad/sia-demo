<?php if (!defined('BASEPATH')) exit('No direct script access allowed');



class App_model extends CI_Model
{



	function getdata($table, $key, $order)

	{

		$this->db->order_by($key, $order);

		$q = $this->db->get($table);

		return $q;
	}



	function insertdata($table, $data)

	{

		$q = $this->db->insert($table, $data);

		return $q;
	}



	function get_lembaga()
	{

		return $this->db->get('tbl_lembaga')->result();
	}

	function get_KDPSTMSMHS($mhs)
	{
		$this->db->select('KDPSTMSMHS');
		$this->db->from('tbl_mahasiswa');
		$this->db->where('NIMHSMSMHS', $mhs);
		return $this->db->get();
	}



	function get_jabatan()
	{

		$this->db->select('a.*,b.divisi');

		$this->db->from('tbl_jabatan a');

		$this->db->join('tbl_divisi b', 'a.kd_divisi = b.kd_divisi');

		return $this->db->get()->result();
	}



	function getdetail($table, $pk, $value, $key, $order, $lim = '')

	{

		$this->db->where($pk, $value);

		$this->db->order_by($key, $order);

		if ($lim != '') {
			$this->db->limit($lim);
		}

		$q = $this->db->get($table);

		return $q;
	}
	function getPeriodebeasiswa($table, $pk, $value, $key, $order, $lim = '')
	{

		$this->db->where('YEAR(createddate)', date('Y'));

		$this->db->where($pk, $value);

		$this->db->order_by($key, $order);

		if ($lim != '') {
			$this->db->limit($lim);
		}

		$q = $this->db->get($table);

		return $q;
	}
	function getdetailprintmhs($table, $pk, $value, $pk2, $value2, $key, $order, $lim = '')

	{

		$this->db->where($pk, $value);
		$this->db->where($pk2, $value2);

		$this->db->order_by($key, $order);

		if ($lim != '') {
			$this->db->limit($lim);
		}

		$q = $this->db->get($table);

		return $q;
	}



	function updatedata($table, $pk, $value, $data)

	{

		$this->db->where($pk, $value);

		$q = $this->db->update($table, $data);

		return $q;
	}

	function updatedata_by_smt($table, $pk, $value, $data, $tahunajaran)

	{

		$this->db->where($pk, $value);

		$this->db->where("tahunajaran", $tahunajaran);


		$q = $this->db->update($table, $data);

		return $q;
	}


	function updatedatamk($table, $pk1, $value1, $pk2, $value2, $data)

	{

		$this->db->where($pk2, $value2);

		$this->db->where($pk1, $value1);

		$q = $this->db->update($table, $data);

		return $q;
	}



	function deletedata($table, $pk, $value)

	{

		$this->db->where($pk, $value);

		$q = $this->db->delete($table);

		return $q;
	}



	function getlistjab($id)
	{

		$this->db->where('lembaga_id', $id);

		return $this->db->get('tbl_jabatan')->result();
	}



	function get_karyawan()
	{

		$this->db->select('a.*,b.prodi');

		$this->db->from('tbl_karyawan a');

		$this->db->join('tbl_jurusan_prodi b', 'a.jabatan_id = b.kd_prodi');

		// $this->db->join('tbl_divisi c','b.kd_divisi = c.kd_divisi','left');

		$this->db->where('status', 1);

		$this->db->order_by('nama', 'asc');

		return $this->db->get()->result();
	}



	function get_jabatan_user($id)
	{

		$this->db->select('a.*,b.jabatan,c.kd_divisi,c.divisi');

		$this->db->from('tbl_karyawan a');

		$this->db->join('tbl_jabatan b', 'a.jabatan_id = b.id_jabatan');

		$this->db->join('tbl_divisi c', 'b.kd_divisi = c.kd_divisi');

		$this->db->where('a.nid', $id);

		return $this->db->get();
	}



	function get_matkul_by_kurikulum()
	{

		$this->db->select('a.kd_fakultas,a.kd_prodi,b.kd_matakuliah,c.nama_matakuliah,c.sks_matakuliah,d.tahunajaran');

		$this->db->from('tbl_kurikulum a');

		$this->db->join('tbl_kurikulum_matkul b', 'a.kd_kurikulum = b.kd_kurikulum', 'left');

		$this->db->join('tbl_matakuliah c', 'b.kd_matakuliah = c.kd_matakuliah', 'left');

		$this->db->join('tbl_tahunajaran d', 'a.tahun_ajaran_kurikulum = d.id_tahunajaran', 'left');

		$this->db->where('a.kd_prodi', $prodi);

		$this->db->where('d.tahunajaran', $tahunajaran);

		return $this->db->get();
	}



	function get_detail_matakuliah($fakultas, $jurusan)
	{

		$this->db->select('a.*,b.fakultas,c.prodi');

		$this->db->from('tbl_matakuliah a');

		$this->db->join('tbl_fakultas b', 'a.kd_fakultas = b.kd_fakultas');

		$this->db->join('tbl_jurusan_prodi c', 'a.kd_prodi = c.kd_prodi');

		$this->db->where('a.kd_fakultas', $fakultas);

		$this->db->like('a.kd_prodi', $jurusan, 'both');

		$this->db->order_by('kd_matakuliah', 'asc');

		return $this->db->get()->result();
	}



	function get_detail_jdl_matakuliah($fakultas, $jurusan)
	{

		$this->db->select('a.*,b.fakultas,c.prodi,mk.*,r.*,kry.*');

		$this->db->from('tbl_jadwal_matkul mk');

		$this->db->join('tbl_matakuliah a', 'a.kd_matakuliah = mk.kd_matakuliah', 'left');

		$this->db->join('tbl_karyawan kry', 'kry.nid = mk.kd_dosen', 'left');

		$this->db->join('tbl_ruangan r', 'r.id_ruangan = mk.kd_ruangan', 'left');



		//$this->db->join('tbl_fakultas b','a.kd_fakultas = b.kd_fakultas');

		//$this->db->join('tbl_jurusan_prodi c','a.kd_prodi = c.kd_prodi');

		$this->db->where('a.kd_fakultas', $fakultas);

		$this->db->where('a.kd_prodi', $jurusan);

		return $this->db->get()->result();
	}



	function get_detail_matakuliah_by_semester($fakultas, $jurusan, $semester)
	{

		$this->db->select('a.*,b.fakultas,c.prodi');

		$this->db->from('tbl_matakuliah a');

		$this->db->join('tbl_fakultas b', 'a.kd_fakultas = b.kd_fakultas');

		$this->db->join('tbl_jurusan_prodi c', 'b.kd_fakultas = c.kd_fakultas');

		//$this->db->where('b.kd_fakultas',$fakultas);

		$this->db->where('c.kd_prodi', $jurusan);

		$this->db->where('a.semester_matakuliah', $semester);

		return $this->db->get()->result();
	}



	function get_detail_kurikulum()
	{

		$this->db->select('a.*, a.status AS status_kurikulum,b.fakultas,c.prodi,d.*');

		$this->db->from('tbl_kurikulum_new a');

		$this->db->join('tbl_fakultas b', 'a.kd_fakultas = b.kd_fakultas');

		$this->db->join('tbl_jurusan_prodi c', 'a.kd_prodi = c.kd_prodi');

		$this->db->join('tbl_tahunakademik d', 'd.kode = a.tahun_ajaran_kurikulum');

		$this->db->where('a.deleted_at IS NULL', NULL, FALSE);

		return $this->db->get()->result();
	}

	function get_detail_kurikulum_prodi($prodi)
	{

		$this->db->select('a.*, a.status AS status_kurikulum,b.fakultas,c.prodi,d.*');

		$this->db->from('tbl_kurikulum_new a');

		$this->db->join('tbl_fakultas b', 'a.kd_fakultas = b.kd_fakultas');

		$this->db->join('tbl_jurusan_prodi c', 'a.kd_prodi = c.kd_prodi');

		$this->db->join('tbl_tahunakademik d', 'd.kode = a.tahun_ajaran_kurikulum');

		$this->db->where('c.kd_prodi', $prodi);

		return $this->db->get()->result();
	}

	function get_detail_kurikulum_fakultas($fak)
	{
		$this->db->select('a.*,b.fakultas,c.prodi,d.*');

		$this->db->from('tbl_kurikulum_new a');

		$this->db->join('tbl_fakultas b', 'a.kd_fakultas = b.kd_fakultas');

		$this->db->join('tbl_jurusan_prodi c', 'a.kd_prodi = c.kd_prodi');

		$this->db->join('tbl_tahunajaran d', 'd.id_tahunajaran = a.tahun_ajaran_kurikulum');

		$this->db->where('b.kd_fakultas', $fak);

		return $this->db->get()->result();
	}

	function get_matkul_krs($semester, $prodi, $npm)
	{
		$thn 	= $this->db->query('SELECT TAHUNMSMHS FROM tbl_mahasiswa WHERE NIMHSMSMHS="' . $npm . '"')->row();
		$ajaran = $this->db->query('SELECT a.kd_kurikulum, b.id_tahunajaran 
									FROM tbl_kurikulum_new a 
									JOIN tbl_tahunajaran b ON ((b.id_tahunajaran = a.tahun_ajaran_kurikulum)) 
									WHERE a.kd_prodi="' . $prodi . '" 
									AND SUBSTR(b.tahunajaran, 1, 4) >= "' . $thn->TAHUNMSMHS . '" 
									ORDER BY id_tahunajaran DESC LIMIT 1')->row();

		if (count($ajaran) > 0) {
			$tahun = $ajaran->id_tahunajaran;
		} else {
			$ajaran = $this->db->query('SELECT a.kd_kurikulum, b.id_tahunajaran FROM tbl_kurikulum_new a 
										JOIN tbl_tahunajaran b ON ((b.id_tahunajaran = a.tahun_ajaran_kurikulum)) 
										where a.kd_prodi="' . $prodi . '" AND SUBSTR(b.`tahunajaran`, 1, 4) <= "' . $thn->TAHUNMSMHS . '" 
										ORDER BY id_tahunajaran DESC LIMIT 1')->row();
			$tahun = $ajaran->id_tahunajaran;
		}

		$this->db->distinct();
		$this->db->select('a.*,b.*,c.*,d.*,
							(SELECT mk.nama_matakuliah FROM tbl_matakuliah mk 
							WHERE mk.kd_matakuliah = b.`prasyarat_matakuliah` 
							LIMIT 1) AS nama');
		$this->db->from('tbl_kurikulum_matkul_new a');
		$this->db->join('tbl_matakuliah b', 'a.kd_matakuliah = b.kd_matakuliah');
		$this->db->join('tbl_kurikulum_new c', 'a.kd_kurikulum = c.kd_kurikulum');
		$this->db->join('tbl_tahunajaran d', 'd.id_tahunajaran = c.tahun_ajaran_kurikulum');
		$this->db->where('d.id_tahunajaran', $tahun);
		$this->db->where('a.semester_kd_matakuliah', $semester);
		$this->db->where('b.kd_prodi', $prodi);
		$this->db->where('c.kd_prodi', $prodi);
		$this->db->where('c.status', '1');
		return $this->db->get();
	}

	function get_matkul_krs_spesial($semester, $prodi, $npm, $tipe)
	{
		// var_dump($semester);exit();
		// $mk = $this->db->query('select KDKMKTRLNM from tbl_transaksi_nilai where NIMHSTRLNM="'.$npm.'"')->row_array();
		$this->db->distinct();
		$this->db->select('a.*,b.*,c.*,d.*');
		$this->db->from('tbl_kurikulum_matkul_new a');
		$this->db->join('tbl_matakuliah b', 'a.kd_matakuliah = b.kd_matakuliah');
		$this->db->join('tbl_kurikulum_new c', 'a.kd_kurikulum = c.kd_kurikulum');
		$this->db->join('tbl_tahunajaran d', 'd.id_tahunajaran = c.tahun_ajaran_kurikulum');

		$thn = $this->db->query('SELECT TAHUNMSMHS FROM tbl_mahasiswa WHERE NIMHSMSMHS="' . $npm . '"')->row();
		$ajaran = $this->db->query('SELECT a.kd_kurikulum, b.id_tahunajaran FROM tbl_kurikulum_new a 
									JOIN tbl_tahunajaran b ON ((b.id_tahunajaran = a.tahun_ajaran_kurikulum)) 
									where a.kd_prodi="' . $prodi . '" 
									AND SUBSTR(b.tahunajaran, 1, 4) >= "' . $thn->TAHUNMSMHS . '" 
									ORDER BY id_tahunajaran DESC LIMIT 1')->row();

		if (count($ajaran) > 0) {
			$tahun = $ajaran->id_tahunajaran;
		} else {
			$ajaran = $this->db->query('SELECT a.kd_kurikulum, b.id_tahunajaran FROM tbl_kurikulum_new a 
										JOIN tbl_tahunajaran b ON ((b.id_tahunajaran = a.tahun_ajaran_kurikulum)) 
										where a.kd_prodi="' . $prodi . '" 
										AND SUBSTR(b.`tahunajaran`, 1, 4) <= "' . $thn->TAHUNMSMHS . '" 
										ORDER BY id_tahunajaran DESC LIMIT 1')->row();
			$tahun = $ajaran->id_tahunajaran;
		}

		if ($tipe == 1) {
			if ($prodi == '25201') {
				$this->db->like('b.nama_matakuliah', "tugas akhir", 'both');
			} else {
				$this->db->like('UPPER(b.nama_matakuliah)', "SKRIPSI", 'both');
			}
		} else {

			$this->db->group_start();
			$this->db->like('b.nama_matakuliah', "kerja praktek", 'both');
			$this->db->or_like('b.nama_matakuliah', 'magang', 'BOTH');
			$this->db->or_like('b.nama_matakuliah', 'KKN', 'BOTH');
			$this->db->or_like('b.nama_matakuliah', 'Kuliah Kerja Nyata', 'BOTH');
			$this->db->or_like('UPPER(b.nama_matakuliah)', "SKRIPSI", 'both');

			// case industri
			if ($prodi == '61201' || $prodi == '62201') {
				$this->db->or_like('b.nama_matakuliah', 'kewirausahaan', 'BOTH');
			}

			$this->db->group_end();
		}
		$this->db->where('d.id_tahunajaran', $tahun);
		$this->db->where('a.semester_kd_matakuliah', $semester);
		$this->db->where('b.kd_prodi', $prodi);
		$this->db->where('c.kd_prodi', $prodi);
		$this->db->where('c.status', '1');

		//$this->db->where_not_in('a.kd_matakuliah',$mk);

		return $this->db->get();
	}

	function get_matkul_krs_reguler($semester, $prodi, $npm)
	{
		$mk = $this->db->query('select KDKMKTRLNM from tbl_transaksi_nilai where NIMHSTRLNM="' . $npm . '"')->row_array();
		$ta = getactyear();
		$kd = "" . $npm . $ta . "";

		$krs_mhs = $this->db->like('kd_krs', $kd, 'after')->get('tbl_krs')->result();
		foreach ($krs_mhs as $row) {
			$mk_krs[] = $row->kd_matakuliah;
		}

		$tahunmasuk = $this->db->query('SELECT TAHUNMSMHS FROM tbl_mahasiswa WHERE NIMHSMSMHS="' . $npm . '"')->row();
		$ajaran     = $this->db->query('SELECT 
											a.kd_kurikulum, 
											b.id_tahunajaran 
										FROM tbl_kurikulum_new a 
										JOIN tbl_tahunajaran b ON b.id_tahunajaran = a.tahun_ajaran_kurikulum
										WHERE a.kd_prodi = "' . $prodi . '" 
										AND SUBSTR(b.tahunajaran, 1, 4) >= "' . $tahunmasuk->TAHUNMSMHS . '" 
										ORDER BY id_tahunajaran DESC LIMIT 1')->row();
		if (count($ajaran) > 0) {
			$tahun = $ajaran->id_tahunajaran;
		} else {
			$ajaran = $this->db->query('SELECT a.kd_kurikulum, b.id_tahunajaran FROM tbl_kurikulum_new a 
										JOIN tbl_tahunajaran b ON ((b.id_tahunajaran = a.tahun_ajaran_kurikulum)) 
										where a.kd_prodi="' . $prodi . '" 
										AND SUBSTR(b.`tahunajaran`, 1, 4) <= "' . $tahunmasuk->TAHUNMSMHS . '" 
										ORDER BY id_tahunajaran DESC LIMIT 1')->row();
			$tahun = $ajaran->id_tahunajaran;
		}

		$this->db->distinct();
		$this->db->select('a.*,b.*,c.*,d.*');
		$this->db->from('tbl_kurikulum_matkul_new a');
		$this->db->join('tbl_matakuliah b', 'a.kd_matakuliah = b.kd_matakuliah');
		$this->db->join('tbl_kurikulum_new c', 'a.kd_kurikulum = c.kd_kurikulum');
		$this->db->join('tbl_tahunajaran d', 'd.id_tahunajaran = c.tahun_ajaran_kurikulum');
		$this->db->where('d.id_tahunajaran', $tahun);
		$this->db->where('a.semester_kd_matakuliah', $semester);
		$this->db->where('b.kd_prodi', $prodi);
		$this->db->where('c.kd_prodi', $prodi);
		$this->db->where('c.status', '1');

		if (isset($mk_krs)) {
			$this->db->where_not_in('b.kd_matakuliah', $mk_krs);
		}

		return $this->db->get();
	}



	function get_matkul_krs_rekam($semester, $npm)
	{

		$this->db->distinct();

		$this->db->select('a.*, b.nama_matakuliah,b.sks_matakuliah');

		$this->db->from('tbl_krs a');

		$this->db->join('tbl_matakuliah b', 'a.kd_matakuliah = b.kd_matakuliah');

		$this->db->where('a.npm_mahasiswa', $npm);

		$this->db->where('a.semester_krs', $semester);

		return $this->db->get();
	}



	function get_prasyarat($prasyarat)
	{

		$this->db->select('a.*');

		$this->db->from('tbl_transaksi_nilai a');

		$this->db->where_in('a.KDKMKTRLNM', $prasyarat);

		return $this->db->get();
	}


	function get_matakuliah_kurikulum($id, $semester)
	{

		return $this->db->query("SELECT distinct a.*,b.*,c.* FROM tbl_kurikulum_matkul a 
				JOIN tbl_matakuliah b ON((a.kd_matakuliah = b.kd_matakuliah)) 
				JOIN tbl_kurikulum c ON((a.kd_kurikulum = c.kd_kurikulum))
				WHERE c.kd_kurikulum = '" . $id . "' 
				AND b.kd_prodi = REPLACE(REPLACE(c.kd_prodi, '\r', ''), '\n', '') 
				AND a.semester_kd_matakuliah='" . $semester . "' 
				ORDER BY b.kd_matakuliah ASC")->result();
	}

	function get_matakuliah_kurikulum_new($id, $semester)
	{

		return $this->db->query("SELECT distinct a.*,b.nama_matakuliah,b.sks_matakuliah,b.prasyarat_matakuliah,c.* FROM tbl_kurikulum_matkul_new a 
				JOIN tbl_matakuliah b ON((a.kd_matakuliah = b.kd_matakuliah)) 
				JOIN tbl_kurikulum_new c ON((a.kd_kurikulum = c.kd_kurikulum))
				WHERE c.kd_kurikulum = '" . $id . "' 
				AND b.kd_prodi = REPLACE(REPLACE(c.kd_prodi, '\r', ''), '\n', '') 
				AND a.semester_kd_matakuliah='" . $semester . "' 
				ORDER BY b.kd_matakuliah ASC")->result();
	}


	function get_matakuliah_baru($id)
	{

		$this->db->select('a.*,b.*');

		$this->db->from('tbl_kurikulum a');

		$this->db->join('tbl_matakuliah b', 'b.kd_prodi = REPLACE(REPLACE(a.kd_prodi, "\r", ""), "\n", "")');

		$this->db->where('a.kd_kurikulum', $id);

		//$kd = $this->db->query('select kd_prodi from tbl_kurikulum where kd_kurikulum="'.$id.'"')->row()->kd_prodi;
		//if(trim($kd) == '62201' || trim($kd) == '70201' || trim($kd) == '55201'){
		$this->db->like('kd_matakuliah', '-', 'both');
		//}

		//$this->db->join('tbl_kurikulum_matkul c',' c.kd_kurikulum ="'.$id.'"');

		$mk = $this->db->query('select kd_matakuliah from tbl_kurikulum_matkul where kd_kurikulum="' . $id . '"');

		if ($mk->num_rows() > 0) {

			foreach ($mk->result() as $row) {

				$mkt[] = $row->kd_matakuliah;
			}

			$this->db->where_not_in('b.kd_matakuliah', $mkt);
		}

		$this->db->order_by('b.kd_matakuliah', 'asc');

		return $this->db->get()->result();
	}

	function get_matakuliah_baru_new($id)
	{

		$this->db->select('a.*,b.*');

		$this->db->from('tbl_kurikulum_new a');

		$this->db->join('tbl_matakuliah b', 'b.kd_prodi = REPLACE(REPLACE(a.kd_prodi, "\r", ""), "\n", "")');

		$this->db->where('a.kd_kurikulum', $id);

		//$this->db->join('tbl_kurikulum_matkul c',' c.kd_kurikulum ="'.$id.'"');

		$mk = $this->db->query('select kd_matakuliah from tbl_kurikulum_matkul_new where kd_kurikulum="' . $id . '"');

		if ($mk->num_rows() > 0) {

			foreach ($mk->result() as $row) {

				$mkt[] = $row->kd_matakuliah;
			}

			$this->db->where_not_in('b.kd_matakuliah', $mkt);
		}

		$this->db->order_by('b.kd_matakuliah', 'asc');

		return $this->db->get()->result();
	}



	function get_all_krs($npm)
	{

		return $this->db->query('SELECT SUM(sks_matakuliah) AS jum_sks, (SUBSTR(b.THSMSTRLNM - c.SMAWLMSMHS, 0,1) * 2) + (SUBSTR(b.THSMSTRLNM - c.SMAWLMSMHS, 1,2) + 1) AS semester_krs, a.*, b.*, c.NIMHSMSMHS FROM tbl_matakuliah a JOIN tbl_transaksi_nilai b ON ((a.kd_matakuliah=REPLACE(REPLACE(b.KDKMKTRLNM, "\r", ""), "\n", ""))) JOIN tbl_mahasiswa c ON ((REPLACE(REPLACE(b.NIMHSTRLNM, "\r", ""), "\n", "")=REPLACE(REPLACE(c.NIMHSMSMHS, "\r", ""), "\n", "")))  WHERE REPLACE(REPLACE(b.NIMHSTRLNM, "\r", ""), "\n", "")="' . $npm . '" GROUP BY b.THSMSTRLNM');
	}


	function get_all_krs_mahasiswa($npm)
	{
		$kd_prodi = $this->db->query('SELECT KDPSTMSMHS from tbl_mahasiswa where NIMHSMSMHS="' . $npm . '"')->row();

		$result = $this->db->query('SELECT 
									SUM(sks_matakuliah) AS jum_sks, 
									(SUBSTR(SUBSTR(b.kd_krs, 13, 5) - c.SMAWLMSMHS, 0,1) * 2) + (SUBSTR(SUBSTR(b.kd_krs, 13, 5) - c.SMAWLMSMHS, 1,2) + 1) AS semester_krs, 
									a.*, 
									b.*, 
									c.NIMHSMSMHS 
								FROM tbl_matakuliah a 
								JOIN tbl_krs b ON ((a.kd_matakuliah=b.kd_matakuliah)) 
								JOIN tbl_mahasiswa c 
								ON ((b.npm_mahasiswa=REPLACE(REPLACE(c.NIMHSMSMHS, "\r", ""), "\n", "")))  
								WHERE b.npm_mahasiswa="' . $npm . '" AND a.kd_prodi="' . $kd_prodi->KDPSTMSMHS . '"
								GROUP BY b.kd_krs');
		return $result;
	}

	function bisa_ujiankah($kd_jadwal, $npm)
	{
		$absenmhs = $this->db->query("SELECT npm_mahasiswa from tbl_absensi_mhs_new_20171 where kd_jadwal = '" . $kd_jadwal . "' and npm_mahasiswa = '" . $npm . "' and kehadiran <> 'A' ")->num_rows();
		
		$absendsn = $this->db->query("SELECT max(pertemuan) as pertemuan from tbl_absensi_mhs_new_20171 where kd_jadwal = '" . $kd_jadwal . "'")->row()->pertemuan;

		$count = ($absenmhs / $absendsn) * 100;
		$persentase = number_format($count, 2);
		return $persentase;
	}

	function get_detail_krs_mahasiswa($id)
	{
		$kd_prodi = $this->db->query('select KDPSTMSMHS from tbl_mahasiswa where NIMHSMSMHS="' . substr($id, 0, 9) . '"')->row();
		$this->db->distinct();
		$this->db->select('a.kd_matakuliah,a.npm_mahasiswa,a.kd_jadwal,a.kd_krs,a.semester_krs,b.nama_matakuliah,b.sks_matakuliah,c.kelas,
		c.kd_jadwal,c.hari,c.waktu_mulai,c.waktu_selesai,c.id_jadwal,d.*,e.kode_ruangan,e.ruangan');
		$this->db->from('tbl_krs a');
		$this->db->join('tbl_matakuliah b', 'a.kd_matakuliah=b.kd_matakuliah');
		$this->db->join('tbl_jadwal_matkul c', 'a.kd_jadwal=c.kd_jadwal', 'left');
		$this->db->join('tbl_karyawan d', 'c.kd_dosen=d.nid', 'left');
		$this->db->join('tbl_ruangan e', 'c.kd_ruangan=e.id_ruangan', 'left');
		$this->db->where('b.kd_prodi', $kd_prodi->KDPSTMSMHS);
		$this->db->where('a.kd_krs', $id);
		return $this->db->get();
	}

	function get_detail_krs_mahasiswa_tes($id)
	{
		$kd_prodi = $this->db->query('select KDPSTMSMHS from tbl_mahasiswa where NIMHSMSMHS="' . substr($id, 0, 9) . '"')->row();
		$this->db->distinct();
		$this->db->select('a.*,b.nama_matakuliah,b.sks_matakuliah,c.kelas,
		c.kd_jadwal,c.hari,c.waktu_mulai,c.waktu_selesai,d.*,e.*');
		$this->db->from('tbl_krs_tes a');
		$this->db->join('tbl_matakuliah b', 'a.kd_matakuliah=b.kd_matakuliah');
		$this->db->join('tbl_jadwal_matkul c', 'a.kd_jadwal=c.kd_jadwal', 'left');
		$this->db->join('tbl_karyawan d', 'c.kd_dosen=d.nid', 'left');
		$this->db->join('tbl_ruangan e', 'c.kd_ruangan=e.id_ruangan', 'left');
		$this->db->where('b.kd_prodi', $kd_prodi->KDPSTMSMHS);
		$this->db->where('a.kd_krs', $id);
		return $this->db->get();
	}

	function get_detail_krs_mahasiswa_feeder($id)
	{
		$kd_prodi = $this->db->query('select KDPSTMSMHS from tbl_mahasiswa where NIMHSMSMHS="' . substr($id, 0, 9) . '"')->row();
		$this->db->distinct();
		$this->db->select('a.*,b.nama_matakuliah,b.sks_matakuliah,c.kelas,
		c.kd_jadwal,d.*,e.*');
		$this->db->from('tbl_krs_feeder a');
		$this->db->join('tbl_matakuliah b', 'a.kd_matakuliah=b.kd_matakuliah');
		$this->db->join('tbl_jadwal_matkul_feeder c', 'a.kd_jadwal=c.kd_jadwal');
		$this->db->join('tbl_karyawan d', 'c.kd_dosen=d.nid', 'left');
		$this->db->join('tbl_ruangan e', 'c.kd_ruangan=e.id_ruangan', 'left');
		$this->db->where('b.kd_prodi', $kd_prodi->KDPSTMSMHS);
		$this->db->where('a.kd_krs', $id);
		return $this->db->get();
	}

	function get_detail_print_krs_mahasiswa($id)
	{
		$kd_prodi = $this->db->query('select KDPSTMSMHS from tbl_mahasiswa where NIMHSMSMHS = "' . substr($id, 0, 9) . '" OR NIMHSMSMHS LIKE "' . substr($id, 0, 9) . '%" ')->row();

		$this->db->select('a.*,b.nama_matakuliah,b.sks_matakuliah,c.NIMHSMSMHS,e.nama,f.kode_ruangan,d.hari,d.waktu_mulai,d.waktu_selesai');
		$this->db->from('tbl_krs a');
		$this->db->join('tbl_matakuliah b', 'a.kd_matakuliah=b.kd_matakuliah');
		$this->db->join('tbl_mahasiswa c', 'c.NIMHSMSMHS=a.npm_mahasiswa');
		$this->db->join('tbl_jadwal_matkul d', 'd.kd_jadwal = a.kd_jadwal', 'left');
		$this->db->join('tbl_karyawan e', 'e.nid = d.kd_dosen', 'left');
		$this->db->join('tbl_ruangan f', 'f.id_ruangan = d.kd_ruangan', 'left');
		$this->db->where('b.kd_prodi', $kd_prodi->KDPSTMSMHS);
		$this->db->where('a.kd_krs', $id);
		return $this->db->get();
	}

	function get_detail_print_krs_mahasiswa_sp($id)
	{
		$kd_prodi = $this->db->query('select KDPSTMSMHS from tbl_mahasiswa where NIMHSMSMHS = "' . substr($id, 0, 9) . '" OR NIMHSMSMHS LIKE "' . substr($id, 0, 9) . '%" ')->row();

		$this->db->select('a.*,b.nama_matakuliah,b.sks_matakuliah,c.NIMHSMSMHS,e.nama,f.kode_ruangan,d.hari,d.waktu_mulai,d.waktu_selesai');
		$this->db->from('tbl_krs_sp a');
		$this->db->join('tbl_matakuliah b', 'a.kd_matakuliah=b.kd_matakuliah');
		$this->db->join('tbl_mahasiswa c', 'c.NIMHSMSMHS=a.npm_mahasiswa');
		$this->db->join('tbl_jadwal_matkul_sp d', 'd.kd_jadwal = a.kd_jadwal', 'left');
		$this->db->join('tbl_karyawan e', 'e.nid = d.kd_dosen', 'left');
		$this->db->join('tbl_ruangan f', 'f.id_ruangan = d.kd_ruangan', 'left');
		$this->db->where('b.kd_prodi', $kd_prodi->KDPSTMSMHS);
		$this->db->where('a.kd_krs', $id);
		return $this->db->get();
	}

	function get_detail_print_krs_mahasiswa_tes($id)
	{
		$kd_prodi = $this->db->query('select KDPSTMSMHS from tbl_mahasiswa where NIMHSMSMHS = "' . substr($id, 0, 9) . '" OR NIMHSMSMHS LIKE "' . substr($id, 0, 9) . '%" ')->row();

		$this->db->select('a.*,b.nama_matakuliah,b.sks_matakuliah,c.NIMHSMSMHS,e.nama,f.kode_ruangan,d.hari,d.waktu_mulai,d.waktu_selesai');
		$this->db->from('tbl_krs_tes a');
		$this->db->join('tbl_matakuliah b', 'a.kd_matakuliah=b.kd_matakuliah');
		$this->db->join('tbl_mahasiswa c', 'c.NIMHSMSMHS=a.npm_mahasiswa');
		$this->db->join('tbl_jadwal_matkul d', 'd.kd_jadwal = a.kd_jadwal', 'left');
		$this->db->join('tbl_karyawan e', 'e.nid = d.kd_dosen', 'left');
		$this->db->join('tbl_ruangan f', 'f.id_ruangan = d.kd_ruangan', 'left');
		$this->db->where('b.kd_prodi', $kd_prodi->KDPSTMSMHS);
		$this->db->where('a.kd_krs', $id);
		return $this->db->get();
	}

	function get_detail_print_krs_mahasiswa_feeder($id)
	{
		$kd_prodi = $this->db->query('select KDPSTMSMHS from tbl_mahasiswa where NIMHSMSMHS = "' . substr($id, 0, 9) . '" OR NIMHSMSMHS LIKE "' . substr($id, 0, 9) . '%" ')->row();

		$this->db->select('a.*,b.nama_matakuliah,b.sks_matakuliah,c.NIMHSMSMHS,e.nama,f.kode_ruangan,d.hari,d.waktu_mulai,d.waktu_selesai');
		$this->db->from('tbl_krs_feeder a');
		$this->db->join('tbl_matakuliah b', 'a.kd_matakuliah=b.kd_matakuliah');
		$this->db->join('tbl_mahasiswa c', 'c.NIMHSMSMHS=a.npm_mahasiswa');
		$this->db->join('tbl_jadwal_matkul_feeder d', 'd.kd_jadwal = a.kd_jadwal', 'left');
		$this->db->join('tbl_karyawan e', 'e.nid = d.kd_dosen', 'left');
		$this->db->join('tbl_ruangan f', 'f.id_ruangan = d.kd_ruangan', 'left');
		$this->db->where('b.kd_prodi', $kd_prodi->KDPSTMSMHS);
		$this->db->where('a.kd_krs', $id);
		return $this->db->get();
	}

	function get_matakuliah_sp($kd_krs, $prodi)
	{
		$this->db->select('*');
		$this->db->from('tbl_krs b');
		$this->db->join('tbl_verifikasi_krs a', 'a.kd_krs = b.kd_krs');
		$this->db->join('tbl_matakuliah c', 'c.kd_matakuliah = b.kd_matakuliah');
		$this->db->where('b.kd_krs', $kd_krs);
		$this->db->where('a.status_verifikasi', 2);
		//$this->db->where('c.kd_prodi', $prodi);
		return $this->db->get();
	}

	function get_all_khs_mahasiswa($npm)
	{
		$kd_prodi = $this->db->query('SELECT KDPSTMSMHS from tbl_mahasiswa where NIMHSMSMHS="' . $npm . '"')->row();

		//ditambahin like -
		return $this->db->query('SELECT distinct b.THSMSTRLNM,b.KDPSTTRLNM from tbl_transaksi_nilai b
		WHERE REPLACE(REPLACE(b.NIMHSTRLNM, "\r", ""), "\n", "")="' . $npm . '" ORDER BY THSMSTRLNM ASC');
	}

	function get_all_khs_mahasiswa_ii($npm)
	{
		$kd_prodi = $this->db->query('SELECT KDPSTMSMHS from tbl_mahasiswa where NIMHSMSMHS="' . $npm . '"')->row();

		//ditambahin like -
		return $this->db->query('SELECT b.THSMSTRLNM,c.SMAWLMSMHS,SUM(sks_matakuliah) AS jum_sks, a.*, b.*, c.NIMHSMSMHS 
		,SUM(a.sks_matakuliah*b.BOBOTTRLNM)/SUM(a.sks_matakuliah) AS ips
		FROM tbl_matakuliah a 
		JOIN tbl_transaksi_nilai b ON ((a.kd_matakuliah=REPLACE(REPLACE(b.KDKMKTRLNM, "\r", ""), "\n", ""))) 
		JOIN tbl_mahasiswa c ON ((REPLACE(REPLACE(c.NIMHSMSMHS, "\r", ""), "\n", "")=REPLACE(REPLACE(b.NIMHSTRLNM, "\r", ""), "\n", "")))  
		WHERE REPLACE(REPLACE(b.NIMHSTRLNM, "\r", ""), "\n", "")="' . $npm . '"
		AND a.kd_prodi="' . $kd_prodi->KDPSTMSMHS . '" AND kd_matakuliah LIKE "%-%" and b.THSMSTRLNM = "20152" GROUP BY b.THSMSTRLNM');
	}


	function get_ips_mahasiswa($npm, $id)
	{

		return $this->db->query('

		SELECT SUM(a.sks_matakuliah*b.BOBOTTRLNM)/SUM(a.sks_matakuliah) AS ips

		FROM tbl_matakuliah a 

		JOIN tbl_transaksi_nilai b ON ((a.kd_matakuliah=REPLACE(REPLACE(b.KDKMKTRLNM, "\r", ""), "\n", ""))) 

		JOIN tbl_mahasiswa c ON ((REPLACE(REPLACE(b.NIMHSTRLNM, "\r", ""), "\n", "")=REPLACE(REPLACE(c.NIMHSMSMHS, "\r", ""), "\n", "")))  

		WHERE REPLACE(REPLACE(b.NIMHSTRLNM, "\r", ""), "\n", "")="' . $npm . '" AND kd_matakuliah LIKE "%-%"

		AND (SUBSTR(LPAD(b.THSMSTRLNM - c.SMAWLMSMHS, 2, "0"), 1,1) * 2) + (SUBSTR(LPAD(b.THSMSTRLNM - c.SMAWLMSMHS, 2, "0"), 2,1) + 1)="' . $id . '"

		GROUP BY b.THSMSTRLNM');
	}

	function get_ipk_mahasiswa($npm)
	{

		return $this->db->query('

		SELECT SUM(a.sks_matakuliah*b.BOBOTTRLNM)/SUM(a.sks_matakuliah) AS ipk

		FROM tbl_matakuliah a 

		JOIN tbl_transaksi_nilai b ON ((a.kd_matakuliah=REPLACE(REPLACE(b.KDKMKTRLNM, "\r", ""), "\n", ""))) 

		JOIN tbl_mahasiswa c ON ((REPLACE(REPLACE(b.NIMHSTRLNM, "\r", ""), "\n", "")=REPLACE(REPLACE(c.NIMHSMSMHS, "\r", ""), "\n", "")))  

		WHERE REPLACE(REPLACE(b.NIMHSTRLNM, "\r", ""), "\n", "")="' . $npm . '" AND kd_matakuliah LIKE "%-%"

		GROUP BY b.THSMSTRLNM');
	}



	function get_detail_khs_mahasiswa($npm, $id)
	{
		$kd_prodi = $this->db->query('select KDPSTMSMHS from tbl_mahasiswa where NIMHSMSMHS = "' . $npm . '"')->row();
		return $this->db->query('SELECT distinct a.`id`,a.`NLAKHTRLNM`,a.`kd_transaksi_nilai`, b.*, c.NIMHSMSMHS FROM tbl_transaksi_nilai a 
	JOIN tbl_matakuliah b ON ((b.kd_matakuliah=REPLACE(REPLACE(a.KDKMKTRLNM, "\r", ""), "\n", ""))) 
	JOIN tbl_mahasiswa c ON ((REPLACE(REPLACE(c.NIMHSMSMHS, "\r", ""), "\n", "")=REPLACE(REPLACE(a.NIMHSTRLNM, "\r", ""), "\n", ""))) 
	WHERE (SUBSTR(LPAD(a.THSMSTRLNM - c.SMAWLMSMHS, 2, "0"), 1,1) * 2) + (SUBSTR(LPAD(a.THSMSTRLNM - c.SMAWLMSMHS, 2, "0"), 2,1) + 1)="' . $id . '" 
	AND b.kd_prodi="' . $kd_prodi->KDPSTMSMHS . '" AND kd_matakuliah LIKE "%-%"
	AND c.NIMHSMSMHS="' . $npm . '"');
	}


	function getpembimbingall($id)

	{

		$this->db->select('a.* , b.nama, c.perihal as perihalan');

		$this->db->from('tbl_penugasan_dosen a');

		$this->db->join('tbl_karyawan b', 'a.nik = b.nid');

		$this->db->join('tbl_perihal c', 'a.perihal = c.kd_perihal');

		$this->db->like('a.penugasan', $id, 'both');

		return $this->db->get();
	}


	function getpembimbingdsn($id)

	{
		$this->db->distinct();

		$this->db->select('a.nik,a.perihal , b.nama, c.perihal as perihalan');

		$this->db->from('tbl_penugasan_dosen a');

		$this->db->join('tbl_karyawan b', 'a.nik = b.nid');

		$this->db->join('tbl_perihal c', 'a.perihal = c.kd_perihal');

		$this->db->where('a.nik', $id);

		return $this->db->get();
	}

	function getpembimbingdsnall($id, $dsn)

	{

		$this->db->select('a.* , b.nama, c.perihal as perihalan');

		$this->db->from('tbl_penugasan_dosen a');

		$this->db->join('tbl_karyawan b', 'a.nik = b.nid');

		$this->db->join('tbl_perihal c', 'a.perihal = c.kd_perihal');

		$this->db->where('a.nik', $dsn);

		$this->db->where('a.perihal', $id);

		return $this->db->get();
	}


	function get_detail_krs($id)
	{

		return $this->db->query('SELECT a.*, b.*, c.NIMHSMSMHS from tbl_transaksi_nilai a 

			join tbl_matakuliah b on ((b.kd_matakuliah=REPLACE(REPLACE(a.KDKMKTRLNM, "\r", ""), "\n", ""))) 

			JOIN tbl_mahasiswa c ON ((REPLACE(REPLACE(a.NIMHSTRLNM, "\r", ""), "\n", "")=REPLACE(REPLACE(c.NIMHSMSMHS, "\r", ""), "\n", "")))  

			WHERE (SUBSTR(a.THSMSTRLNM - c.SMAWLMSMHS, 0,1) * 2) + (SUBSTR(a.THSMSTRLNM - c.SMAWLMSMHS, 1,2) + 1)="' . $id . '"');

		//$this->db->select(',a.*,b.*,c.NIMHSMSMHS');

		//$this->db->from('tbl_transaksi_nilai a');		

		//$this->db->join('tbl_matakuliah b','b.kd_matakuliah=REPLACE(REPLACE(a.KDKMKTRLNM, "\r", ""), "\n", "")');

		//$this->db->join('tbl_mahasiswa c','REPLACE(REPLACE(a.NIMHSTRLNM, "\r", ""), "\n", "")=REPLACE(REPLACE(c.NIMHSMSMHS, "\r", ""), "\n", "")');

		//$this->db->where('(a.THSMSTRLNM-c.SMAWLMSMHS)+1',$id);

		//return $this->db->get();

	}


	public function cari($kode)
	{
		$this->db->like('nama', $kode);
		$query = $this->db->get('tbl_karyawan');
		return $query->result();
	}


	function get_pembimbing_krs($kd)
	{

		$this->db->select('a.*,b.*');

		$this->db->from('tbl_verifikasi_krs a');

		$this->db->join('tbl_karyawan b', 'b.nid = a.id_pembimbing');

		$this->db->where('a.kd_krs', $kd);

		return $this->db->get();
	}

	function get_pembimbing_krs_sp($kd)
	{

		$this->db->select('a.*,b.*');

		$this->db->from('tbl_verifikasi_krs_sp a');

		$this->db->join('tbl_karyawan b', 'b.nid = a.id_pembimbing');

		$this->db->where('a.kd_krs', $kd);

		return $this->db->get();
	}

	function get_pembimbing_krs_tes($kd)
	{

		$this->db->select('a.*,b.*');

		$this->db->from('tbl_verifikasi_krs_tes a');

		$this->db->join('tbl_karyawan b', 'b.nid = a.id_pembimbing');

		$this->db->where('a.kd_krs', $kd);

		return $this->db->get();
	}



	function get_fakultas_karyawan($id)
	{

		$this->db->select('a.*,b.*,c.*,d.*');

		$this->db->from('tbl_karyawan a');

		$this->db->join('tbl_jabatan b', 'a.jabatan_id = b.id_jabatan');

		$this->db->join('tbl_divisi c', 'b.kd_divisi = c.kd_divisi');

		$this->db->join('tbl_fakultas d', 'd.kd_fakultas = c.kd_divisi');

		$this->db->where('a.nid', $id);

		return $this->db->get();
	}



	function get_prodi_karyawan($id)
	{

		$this->db->select('a.*,b.*,c.*,d.*');

		$this->db->from('tbl_karyawan a');

		$this->db->join('tbl_jabatan b', 'a.jabatan_id = b.id_jabatan');

		$this->db->join('tbl_divisi c', 'b.kd_divisi = c.kd_divisi');

		$this->db->join('tbl_jurusan_prodi d', 'd.kd_prodi = c.kd_divisi');

		$this->db->where('a.nid', $id);

		return $this->db->get();
	}



	function getlistmenu($id)
	{

		$this->db->where('user_group_id', $id);

		$getmenu = $this->db->get('tbl_role_access')->result();

		foreach ($getmenu as $row) {

			$menu[] = $row->menu_id;
		}

		$this->db->where_not_in('id_menu', $menu);

		$q = $this->db->get('tbl_menu');

		return $q;
	}



	function getfakultas()

	{

		$this->db->select('*');

		$this->db->from('tbl_fakultas');

		return $this->db->get()->result();
	}



	function get_jurusan($fakultas='')

	{
		$this->db->select('*');

		$this->db->from('tbl_jurusan_prodi a');

		$this->db->join('tbl_fakultas b', 'a.kd_fakultas = b.kd_fakultas');

		if (!empty($fakultas) && (strlen($fakultas) < 2)) {
			$this->db->where('a.kd_fakultas', $fakultas);
		}
		return $this->db->get()->result();
	}



	function get_lokasi()

	{

		$this->db->select('*');

		$this->db->from('tbl_gedung a');

		$this->db->join('tbl_lembaga b', 'a.kode_lembaga = b.kode_lembaga', 'left');

		return $this->db->get()->result();
	}



	function get_lantai($id)

	{

		$this->db->select('*');

		$this->db->from('tbl_lantai a');

		$this->db->join('tbl_gedung b', 'a.id_gedung = b.id_gedung');

		$this->db->where('a.id_gedung', $id);

		return $this->db->get()->result();
	}



	function get_ruang($id)

	{

		$this->db->select('*');

		$this->db->from('tbl_ruangan a');

		$this->db->join('tbl_lantai b', 'a.id_lantai = b.id_lantai');

		$this->db->where('a.id_lantai', $id);

		return $this->db->get()->result();
	}



	function get_sync()

	{

		$this->db->select('*');

		$this->db->from('tbl_sync');

		return $this->db->get()->result();
	}



	function getIndexPembayaran($jur, $angk)

	{

		$this->db->select('a.*, b.jenis_pembayaran');

		$this->db->from('tbl_index_pembayaran a');

		$this->db->join('tbl_jenis_pembayaran b', 'a.kd_jenis = b.kd_jenis');

		$this->db->where('a.kd_prodi', $jur);

		$this->db->where('a.tahunajaran', $angk);

		return $this->db->get()->result();
	}



	function get_jadwal($jurusan, $tahun)
	{

		$this->db->select('a.*, b.*');

		$this->db->from('tbl_jadwal_matkul a');

		$this->db->join('tbl_matakuliah b', 'a.kd_matakuliah = b.kd_matakuliah');

		$this->db->where('a.kd_tahunajaran', $tahun);

		$this->db->where('b.kd_prodi', $jurusan);

		return $this->db->get();
	}


	function getdatadosen($value)

	{

		$this->db->select('*');

		$this->db->from('tbl_karyawan a');

		$this->db->join('tbl_jabatan b', 'a.jabatan_id = b.id_jabatan');

		$this->db->where('a.status', '1');

		$this->db->where('b.kd_divisi', $value);

		$this->db->like('b.jabatan', 'Pendidik', 'both');

		return $this->db->get();
	}



	function getdatathn()

	{

		$this->db->select('*');

		$this->db->from('tbl_tahunakademik');

		//$this->db->join('tbl_penugasan_dosen b', 'a.kode = b.penugasan','left');

		//$this->db->where('b.penugasan is null', null, false);

		return $this->db->get();
	}



	function getdivisi($value)
	{

		$this->db->select('*');

		$this->db->from('tbl_karyawan a');

		$this->db->join('tbl_jabatan b', 'b.jabatan_id = ', 'left');

		$this->db->where('a.nid', $value);

		return $this->db->get();
	}



	function join_sync_detail()

	{

		$this->db->select('id_sync_detail, COUNT(id_sync_detail) as total');

		$this->db->from('tbl_sync_detail a');

		$this->db->join('tbl_sync b', 'a.date_sync = b.tanggal_sinkron');

		$this->db->group_by('tanggal_sinkron');

		$this->db->order_by('total', 'desc');

		return $this->db->get();
	}



	function ambil_dt_mhs($id)

	{

		$this->db->select('*');

		$this->db->from('tbl_mahasiswa');

		$this->db->where('id_mhs', $id);

		return $this->db->get()->row();
	}



	function get_jadwalujian()
	{
		$this->db->select('a.*, b.*, c.*, d.*');

		$this->db->from('tbl_jadwal_ujian a');

		$this->db->join('tbl_ruangan b', 'a.kode_ruangan = b.id_ruangan');

		$this->db->join('tbl_jadwal_matkul c', 'a.kd_jadwal_matkul = c.kd_jadwal');

		$this->db->join('tbl_matakuliah d', 'c.kd_matakuliah = d.kd_matakuliah');

		$this->db->order_by('tanggal_jadwal_ujian', 'asc');

		$this->db->order_by('mulai_jadwal_ujian', 'asc');

		return $this->db->get();
	}



	function get_semester($tahun)
	{
		$act = getactyear();

		$selisih = $act - $tahun;

		if ($selisih == 1) {
			$semester = $selisih / 0.5;
		} elseif ($selisih == 0) {
			$semester = 1;
		} else {
			$awal  = substr($selisih, 0, 1);
			$akhir = substr($selisih, 1, 1);

			if ($akhir == 0) {
				$end = 1;
			} else {
				$end = $akhir / 0.5;
			}

			$semester = ($awal / 0.5) + ($end);
		}

		return $semester;
	}

	function get_smt_awal_genap($tahun)
	{
		$act = getactyear();
		$lastdigitact = substr($act, 4, 1);

		// get one digit in last $tahun
		$lastdigit = substr($tahun, 4, 1);

		if ($lastdigitact != $lastdigit) {
			$const = 0;
		} else {
			$const = 1;
		}

		$selisih = substr($act, 0, 4) - substr($tahun, 0, 4);

		if ($selisih == 0) {
			$semester = 1;
		} else {
			$semester = ($selisih / 0.5) + $const;
		}

		return $semester;
	}

	function get_semester_new($tahun)
	{

		$this->db->order_by('kode', 'desc');

		$this->db->where('status', '1');

		$tahunakademik = $this->db->get('tbl_tahunakademik', 1)->row();

		if ($tahun % 2 == 0) {
			$awal_masuk = $tahun - 1;
		} else {
			$awal_masuk = $tahun;
		}

		$selisih = ($tahunakademik->kode) - ($awal_masuk);

		if ($selisih == 1) {
			$sls = '01';
		} else {
			$sls = $selisih;
		}


		$semawal = (substr($sls, 0, 1)) * 2;

		$semtambah = substr($sls, 1, 2) + 1;

		$semester = $semawal + $semtambah;

		return $semester;
	}



	function get_matakuliah_jadwal($prodi, $semester)
	{

		$this->db->select('a.*,b.*,c.*');

		$this->db->from('tbl_kurikulum_matkul a');

		$this->db->join('tbl_matakuliah b', 'a.kd_matakuliah = b.kd_matakuliah');

		$this->db->join('tbl_kurikulum c', 'a.kd_kurikulum = c.kd_kurikulum');

		$this->db->where('c.kd_prodi', $prodi);

		$this->db->where('c.status', '1');

		$this->db->where('b.semester_matakuliah', $semester);

		return $this->db->get()->result();
	}


	//ingetin harus dirubah
	function get_pilih_jadwal($kd, $prodi, $ta)
	{

		/*$this->db->select('a.*, b.*,c.*');

		$this->db->from('tbl_jadwal_matkul a');

		$this->db->join('tbl_ruangan b','a.kd_ruangan = b.id_ruangan', 'left');

		$this->db->join('tbl_karyawan c','a.kd_dosen = c.nid');

		$this->db->join('tbl_matakuliah d', 'a.kd_matakuliah = d.kd_matakuliah');

		$this->db->like('a.kd_jadwal', $prodi);

		$this->db->where('a.kd_matakuliah', $kd);

		$this->db->like('d.kd_prodi', $prodi);*/

		if ($prodi == '70201') {
			$q = $this->db->query("SELECT a.*,b.`kuota`,b.`ruangan`,c.`nama`,b.`kode_ruangan`, 
									IF(a.gabung > 0,
									(SELECT COUNT(DISTINCT z.`npm_mahasiswa`) FROM tbl_krs z WHERE z.`kd_jadwal` = a.`kd_jadwal` OR z.`kd_jadwal` = a.`referensi`) ,
									(SELECT COUNT(DISTINCT z.`npm_mahasiswa`) FROM tbl_krs z WHERE z.`kd_jadwal` = a.`kd_jadwal`) 
									) AS jumlah
								FROM tbl_jadwal_matkul a
								LEFT JOIN tbl_ruangan b ON a.`kd_ruangan` = b.`id_ruangan`
								JOIN tbl_karyawan c ON a.`kd_dosen` = c.`nid`
								JOIN tbl_matakuliah d ON a.`kd_matakuliah` = d.`kd_matakuliah`
								WHERE d.`kd_prodi` = '" . $prodi . "' AND a.`kd_matakuliah` = '" . $kd . "' AND a.`kd_jadwal` LIKE '" . $prodi . "%' AND kd_tahunajaran = '" . $ta . "'");
		} else {
			$q = $this->db->query("SELECT a.*,b.`kuota`,b.`ruangan`,c.`nama`,b.`kode_ruangan`, 
									IF(a.gabung > 0,
									(SELECT COUNT(DISTINCT z.`npm_mahasiswa`) FROM tbl_krs z WHERE z.`kd_jadwal` = a.`kd_jadwal` OR z.`kd_jadwal` = a.`referensi`) ,
									(SELECT COUNT(DISTINCT z.`npm_mahasiswa`) FROM tbl_krs z WHERE z.`kd_jadwal` = a.`kd_jadwal`) 
									) AS jumlah
								FROM tbl_jadwal_matkul a
								LEFT JOIN tbl_ruangan b ON a.`kd_ruangan` = b.`id_ruangan`
								JOIN tbl_karyawan c ON a.`kd_dosen` = c.`nid`
								JOIN tbl_matakuliah d ON a.`kd_matakuliah` = d.`kd_matakuliah`
								WHERE d.`kd_prodi` = '" . $prodi . "' AND a.`kd_matakuliah` = '" . $kd . "' AND a.`kd_jadwal` LIKE '" . $prodi . "%' AND kd_tahunajaran = '" . $ta . "' AND (SUBSTR(b.`kuota`,1,2) - (SELECT COUNT(DISTINCT z.npm_mahasiswa) FROM tbl_krs z WHERE z.kd_jadwal = a.`kd_jadwal`)) > 0");
		}

		// $q = $this->db->query("SELECT a.*,b.`kuota`,b.`ruangan`,c.`nama`,b.`kode_ruangan`,(SELECT COUNT(z.`npm_mahasiswa`) FROM tbl_krs z WHERE z.`kd_jadwal` = a.`kd_jadwal`) AS jumlah FROM tbl_jadwal_matkul a
		// 						JOIN tbl_ruangan b ON a.`kd_ruangan` = b.`id_ruangan`
		// 						JOIN tbl_karyawan c ON a.`kd_dosen` = c.`nid`
		// 						JOIN tbl_matakuliah d ON a.`kd_matakuliah` = d.`kd_matakuliah`
		// 						WHERE d.`kd_prodi` = '".$prodi."' AND a.`kd_matakuliah` = '".$kd."' AND a.`kd_jadwal` LIKE '".$prodi."%' AND kd_tahunajaran = '".$ta."'
		// 						AND (SUBSTR(b.`kuota`,1,2) - (SELECT COUNT(z.npm_mahasiswa) FROM tbl_krs z WHERE z.kd_jadwal = a.`kd_jadwal`)) >= -5
		// 						");

		//return $this->db->get();

		return $q;
	}

	function get_pilih_jadwal_feeder($kd, $prodi, $ta)
	{


		$q = $this->db->query("SELECT a.*,b.`kuota`,b.`ruangan`,c.`nama`,b.`kode_ruangan`,(SELECT COUNT(DISTINCT z.`npm_mahasiswa`) FROM tbl_krs_feeder z WHERE z.`kd_jadwal` = a.`kd_jadwal`) AS jumlah FROM tbl_jadwal_matkul_feeder a
								left JOIN tbl_ruangan b ON a.`kd_ruangan` = b.`id_ruangan`
								JOIN tbl_karyawan c ON a.`kd_dosen` = c.`nid`
								JOIN tbl_matakuliah d ON a.`kd_matakuliah` = d.`kd_matakuliah`
								WHERE d.`kd_prodi` = '" . $prodi . "' AND a.`kd_matakuliah` = '" . $kd . "' AND a.`kd_jadwal` LIKE '" . $prodi . "%' and a.kd_tahunajaran = '" . $ta . "'
								");

		return $q;
	}

	function get_pilih_jadwal_krs($kd, $prodi, $ta, $kelas)
	{
		if (preg_match('/\bMKDU\b/', $kd) || preg_match('/\bMKU\b/', $kd) || preg_match('/\bMKWU\b/', $kd)) {
			$getKodeFakultas = $this->db->where('kd_prodi', $prodi)->get('tbl_jurusan_prodi')->row()->kd_fakultas;
			$kode            = $getKodeFakultas;
		} else {
			$kode = $prodi;
		}

		$queryPartReg = "(SUBSTR(b.kuota,1,2) - (SELECT COUNT(DISTINCT z.npm_mahasiswa) FROM tbl_krs z WHERE z.kd_jadwal = a.kd_jadwal))";

		$queryPartJoin = "IF(a.gabung = 0,
							(SUBSTR(b.kuota,1,2) - (SELECT COUNT(DISTINCT z.npm_mahasiswa) FROM tbl_krs z WHERE z.kd_jadwal = a.kd_jadwal) ) > 0,
							(SUBSTR((SELECT kuota FROM tbl_ruangan r JOIN tbl_jadwal_matkul j ON r.id_ruangan = j.kd_ruangan WHERE j.kd_jadwal = a.referensi),1,2) - 
							(SELECT COUNT(DISTINCT z.npm_mahasiswa) FROM tbl_krs z WHERE z.kd_jadwal = a.kd_jadwal) ) )";

		if (preg_match('/\bMKDU\b/', $kd) || preg_match('/\bMKU\b/', $kd) || preg_match('/\bMKWU\b/', $kd)) {
			$fx = $this->_for_mkdu($kelas, $kd, $kode, $ta, $queryPartJoin);
		} else {
			$fx = $this->_for_reg_jadwal($kelas, $kd, $kode, $ta, $queryPartReg);
		}
		return $fx;
	}

	function _for_mkdu($kelas, $kdMatakuliah, $kdJadwal, $tahunajaran, $remainQuota)
	{
		$fx = $this->db->query("SELECT 
									IF(a.gabung = 0, a.hari, 
										(SELECT hari FROM tbl_jadwal_matkul WHERE kd_jadwal = a.referensi)) AS hari,
									IF(a.gabung = 0, a.waktu_selesai, 
										(SELECT waktu_selesai FROM tbl_jadwal_matkul WHERE kd_jadwal = a.referensi)) AS waktu_selesai,
									IF(a.gabung = 0, a.waktu_mulai, 
										(SELECT waktu_mulai FROM tbl_jadwal_matkul WHERE kd_jadwal = a.referensi)) AS waktu_mulai,
									IF(a.gabung = 0, kd_matakuliah, 
										(SELECT kd_matakuliah FROM tbl_jadwal_matkul WHERE kd_jadwal = a.referensi)) AS kd_matakuliah,
									IF(a.gabung = 0, a.kelas, 
										(SELECT kelas FROM tbl_jadwal_matkul WHERE kd_jadwal = a.referensi)) AS kelas,
									IF(a.gabung = 0, b.kuota, 
										(SELECT r.kuota FROM tbl_ruangan r JOIN tbl_jadwal_matkul j ON r.id_ruangan = j.kd_ruangan WHERE j.kd_jadwal = a.referensi)) AS kuota,
									IF(a.gabung = 0, b.ruangan, 
										(SELECT r.ruangan FROM tbl_ruangan r JOIN tbl_jadwal_matkul j ON r.id_ruangan = j.kd_ruangan WHERE j.kd_jadwal = a.referensi)) AS ruangan,
									IF(a.gabung = 0, b.kode_ruangan, 
										(SELECT r.kode_ruangan FROM tbl_ruangan r JOIN tbl_jadwal_matkul j ON r.id_ruangan = j.kd_ruangan WHERE kd_jadwal = a.referensi)) AS kode_ruangan,
									IF(a.gabung = 0, c.nama, 
										(SELECT k.nama FROM tbl_karyawan k JOIN tbl_jadwal_matkul j ON k.nid = j.kd_dosen WHERE kd_jadwal = a.referensi)) AS nama,
									IF(a.gabung <> 0,a.`referensi`, a.`kd_jadwal`) AS kd_jadwal,
									(SELECT COUNT(DISTINCT z.npm_mahasiswa) FROM tbl_krs z 
									WHERE z.kd_jadwal = IF(a.gabung <> 0,a.referensi,a.kd_jadwal)) AS jumlah
								FROM tbl_jadwal_matkul a
								LEFT JOIN tbl_ruangan b ON a.kd_ruangan = b.id_ruangan
								LEFT JOIN tbl_karyawan c ON a.kd_dosen = c.nid
								WHERE a.kd_matakuliah = '$kdMatakuliah' 
								AND a.kd_jadwal LIKE '$kdJadwal/%' 
								AND a.kd_tahunajaran = '$tahunajaran' 
								-- jika jadwal tersebut adalah jadwal induk maka ambil yang sesuai dengan kategori kelas mahasiswa (PG/SR/PK)
								-- jika jadwal turunan, maka cek data jadwal induknya untuk melihat kategori kelas dari jadwal induk tsb
								AND IF('$kelas' = 'PG', 
										a.waktu_kelas = 'PG', 
										a.waktu_kelas IN ('SR','PK'))
								AND IF(SUBSTR(a.kd_matakuliah, 1,2) = 'MK', 
										a.open = 1, 
										a.open IS NULL)
								AND $remainQuota > 0");
		return $fx;
	}

	function _for_reg_jadwal($kelas, $kdMatakuliah, $kdJadwal, $tahunajaran, $remainQuota)
	{
		$fx = $this->db->query("SELECT 
									a.hari,
									a.waktu_selesai,
									a.waktu_mulai,
									a.kelas,
									a.kd_matakuliah,
									b.kuota,
									b.ruangan,
									c.nama,
									b.kode_ruangan, 
									a.kd_jadwal,
									(SELECT COUNT(DISTINCT z.npm_mahasiswa) 
									FROM tbl_krs z 
									WHERE z.kd_jadwal = a.kd_jadwal) AS jumlah
								FROM tbl_jadwal_matkul a
								JOIN tbl_ruangan b ON a.kd_ruangan = b.id_ruangan
								JOIN tbl_karyawan c ON a.kd_dosen = c.nid
								WHERE a.kd_matakuliah = '$kdMatakuliah' 
								AND a.kd_jadwal LIKE '$kdJadwal/%' 
								AND a.kd_tahunajaran = '$tahunajaran'
								AND a.waktu_kelas = '$kelas'
								AND $remainQuota > 0");
		return $fx;
	}

	function get_pilih_jadwal_krs_feeder($kd, $prodi, $ta, $kelas)
	{
		$jadwal = $this->db->query("SELECT 
										a.*,
										b.kuota,
										b.ruangan,
										c.nama,
										b.kode_ruangan,
										(SELECT COUNT(z.npm_mahasiswa) FROM tbl_krs_feeder z 
										WHERE z.`kd_jadwal` = a.kd_jadwal) AS jumlah 
									FROM tbl_jadwal_matkul_feeder a
									LEFT JOIN tbl_ruangan b ON a.kd_ruangan = b.id_ruangan
									JOIN tbl_karyawan c ON a.kd_dosen = c.nid
									JOIN tbl_matakuliah d ON a.`kd_matakuliah` = d.kd_matakuliah
									WHERE d.kd_prodi = '$prodi' 
									AND a.kd_matakuliah = '$kd'
									AND a.kd_jadwal LIKE '$prodi%' 
									AND a.kd_tahunajaran = '$ta' 
									AND a.waktu_kelas = '$kelas'");

		return $jadwal;
	}



	function updatedata_krs($kd, $krs, $data)

	{

		$this->db->where('kd_matakuliah', $kd);

		$this->db->where('kd_krs', $krs);

		$q = $this->db->update('tbl_krs', $data);

		return $q;
	}

	function updatedata_krs_tes($kd, $krs, $data)

	{

		$this->db->where('kd_matakuliah', $kd);

		$this->db->where('kd_krs', $krs);

		$q = $this->db->update('tbl_krs_tes', $data);

		return $q;
	}

	function updatedata_krs_feeder($kd, $krs, $data)

	{

		$this->db->where('kd_matakuliah', $kd);

		$this->db->where('kd_krs', $krs);

		$q = $this->db->update('tbl_krs_feeder', $data);

		return $q;
	}



	function getmhsbimbingan($id)

	{

		$this->db->distinct();

		$this->db->select('b.NIMHSMSMHS, b.NMMHSMSMHS, b.TAHUNMSMHS');

		$this->db->from('tbl_verifikasi_krs a');

		$this->db->join('tbl_krs c', 'a.kd_krs = c.kd_krs');

		$this->db->join('tbl_mahasiswa b', 'b.NIMHSMSMHS = c.npm_mahasiswa');

		$this->db->where('id_pembimbing', $id);

		//$this->db->where('TAHUNMSMHS', $tahun);

		return $this->db->get();
	}


	function get_krs_mahasiswa($npm, $semester)
	{
		$kd_prodi = $this->db->query('SELECT KDPSTMSMHS FROM tbl_mahasiswa WHERE NIMHSMSMHS="' . $npm . '"')->row();

		$this->db->distinct();
		$this->db->select('a.*, b.nama_matakuliah,b.sks_matakuliah,b.prasyarat_matakuliah,c.semester_kd_matakuliah');
		$this->db->from('tbl_krs a');
		$this->db->join('tbl_matakuliah b', 'b.kd_matakuliah=a.kd_matakuliah');
		$this->db->join('tbl_kurikulum_matkul_new c', 'b.kd_matakuliah = c.kd_matakuliah');
		$this->db->join('tbl_kurikulum_new d', 'd.kd_kurikulum = c.kd_kurikulum');
		$this->db->where('a.npm_mahasiswa', $npm);
		$this->db->where('b.kd_prodi', $kd_prodi->KDPSTMSMHS);
		$this->db->where('SUBSTR(c.kd_kurikulum,1,5)', $kd_prodi->KDPSTMSMHS);
		$this->db->where('a.semester_krs', $semester);
		$this->db->where('d.status', '1');
		return $this->db->get();
	}

	function get_krs_mahasiswa_tes($npm, $semester)
	{

		$kd_prodi = $this->db->query('select KDPSTMSMHS from tbl_mahasiswa where NIMHSMSMHS="' . $npm . '"')->row();

		$this->db->distinct();

		$this->db->select('a.*, b.nama_matakuliah,b.sks_matakuliah,b.prasyarat_matakuliah,c.semester_kd_matakuliah');

		$this->db->from('tbl_krs_tes a');

		$this->db->join('tbl_matakuliah b', 'b.kd_matakuliah=a.kd_matakuliah');

		$this->db->join('tbl_kurikulum_matkul_new c', 'b.kd_matakuliah = c.kd_matakuliah');

		$this->db->where('npm_mahasiswa', $npm);

		$this->db->where('b.kd_prodi', $kd_prodi->KDPSTMSMHS);

		$this->db->where('SUBSTR(c.kd_kurikulum,1,5)', $kd_prodi->KDPSTMSMHS);

		$this->db->where('semester_krs', $semester);

		return $this->db->get();
	}

	function jdl_matkul_by_semester($smtr)
	{
		if ($this->session->userdata('tahunajaran') > 20162) {
			$tabel = 'tbl_kurikulum_matkul_new';
			$kurikulum = 'tbl_kurikulum_new';
		} else {
			$tabel = 'tbl_kurikulum_matkul';
			$kurikulum = 'tbl_kurikulum';
		}

		$kode_prodi    = $this->session->userdata('id_jurusan_prasyarat');
		$kode_fakultas = $this->db->query("SELECT kd_fakultas FROM tbl_jurusan_prodi WHERE kd_prodi = '$kode_prodi'")->row();


		$query1 = 'SELECT distinct 
						mk.gabung,mk.referensi,mk.audit_user,mk.kd_jadwal,mk.id_jadwal,mk.kelas,
						mk.kd_matakuliah,mk.hari,mk.waktu_mulai,mk.waktu_selesai,a.nama_matakuliah,
						a.sks_matakuliah,r.*,kry.nama
					FROM tbl_jadwal_matkul mk
			        JOIN tbl_matakuliah a ON mk.`kd_matakuliah`=a.`kd_matakuliah`
			        left JOIN tbl_ruangan r ON r.`id_ruangan` = mk.`kd_ruangan`
			        LEFT JOIN tbl_karyawan kry ON kry.`nid` = mk.`kd_dosen`
			        WHERE mk.`kd_jadwal` LIKE "' . $this->session->userdata('id_jurusan_prasyarat') . '%"
			        AND a.`kd_prodi` = SUBSTR(mk.`kd_jadwal`,1,5)
			        AND mk.`kd_tahunajaran` = "' . $this->session->userdata('tahunajaran') . '"
			        AND a.`kd_matakuliah` IN 
			        	(SELECT DISTINCT kd_matakuliah FROM ' . $tabel . ' kurmat
			        	JOIN ' . $kurikulum . ' kur
			        	WHERE kurmat.kd_kurikulum LIKE "%' . $this->session->userdata('id_jurusan_prasyarat') . '%" 
			        	AND kurmat.semester_kd_matakuliah = ' . $smtr . '
			        	AND kur.status = "1")';

		$query2 = 'SELECT 
					    mk.gabung,
					    mk.referensi,
					    mk.audit_user,
					    mk.kd_jadwal,
					    mk.id_jadwal,
					    mk.kelas,
					    mk.kd_matakuliah,
					    mk.hari,
					    mk.waktu_mulai,
					    mk.waktu_selesai,
					    a.nama_matakuliah,
					    a.sks_matakuliah,
					    r.*,
					    kry.nama
					FROM
					    tbl_jadwal_matkul mk
					        JOIN
					    tbl_matakuliah a ON mk.`id_matakuliah` = a.`id_matakuliah`
					      LEFT JOIN
					    tbl_ruangan r ON r.`id_ruangan` = mk.`kd_ruangan`
					     LEFT JOIN
					    tbl_karyawan kry ON kry.`nid` = mk.`kd_dosen`
					WHERE
					    kd_jadwal LIKE "' . $kode_fakultas->kd_fakultas . '/%"
					 AND mk.`kd_tahunajaran` = "' . $this->session->userdata('tahunajaran') . '"
					 AND a.`kd_matakuliah` IN 
                                	(SELECT DISTINCT kd_matakuliah FROM ' . $tabel . ' kurmat
                                	JOIN ' . $kurikulum . ' kur
                                	WHERE kurmat.kd_kurikulum LIKE "%' . $this->session->userdata('id_jurusan_prasyarat') . '%" 
                                	AND kurmat.semester_kd_matakuliah = ' . $smtr . '
                                	AND kur.status = "1")
					    ';


		$fullQuery = $query1 . " UNION ALL " . $query2 . " order by kd_matakuliah ASC";

		$query = $this->db->query($fullQuery)->result();

		return $query;
	}

	function jdl_matkul_by_semester_test_mhs($smtr)
	{
		if ($this->session->userdata('tahunajaran') > 20162) {
			$tabel = 'tbl_kurikulum_matkul_new';
		} else {
			$tabel = 'tbl_kurikulum_matkul';
		}
		$q = $this->db->query('SELECT distinct mk.gabung,mk.referensi,mk.audit_user,mk.kd_jadwal,mk.id_jadwal,mk.kelas,mk.kd_matakuliah,mk.hari,mk.waktu_mulai,mk.waktu_selesai,a.nama_matakuliah,a.sks_matakuliah,r.*,kry.nama FROM tbl_jadwal_matkul mk
                                JOIN tbl_matakuliah a ON mk.`kd_matakuliah`=a.`kd_matakuliah`
                                left JOIN tbl_ruangan r ON r.`id_ruangan` = mk.`kd_ruangan`
                                LEFT JOIN tbl_karyawan kry ON kry.`nid` = mk.`kd_dosen`
                                WHERE mk.`kd_jadwal` LIKE "' . $this->session->userdata('id_jurusan_prasyarat') . '%"
                                AND a.`kd_prodi` = SUBSTR(mk.`kd_jadwal`,1,5)
                                AND mk.`kd_tahunajaran` = "20171"
                                AND a.`kd_matakuliah` IN (SELECT DISTINCT kd_matakuliah FROM ' . $tabel . ' WHERE kd_kurikulum LIKE "' . $this->session->userdata('id_jurusan_prasyarat') . '%" AND semester_kd_matakuliah = ' . $smtr . ')
                                order by mk.kd_matakuliah ASC')->result();
		return $q;
	}

	function jdl_matkul_by_semester_sp($smtr)
	{
		if ($this->session->userdata('tahunajaran') > 20162) {
			$tabel = 'tbl_kurikulum_matkul_new';
		} else {
			$tabel = 'tbl_kurikulum_matkul';
		}
		$q = $this->db->query('SELECT distinct mk.audit_user,mk.kd_jadwal,mk.id_jadwal,mk.kelas,mk.kd_matakuliah,mk.hari,mk.waktu_mulai,mk.waktu_selesai,a.nama_matakuliah,a.sks_matakuliah,r.*,kry.nama FROM tbl_jadwal_matkul_sp mk
                                JOIN tbl_matakuliah a ON mk.`kd_matakuliah`=a.`kd_matakuliah`
                                left JOIN tbl_ruangan r ON r.`id_ruangan` = mk.`kd_ruangan`
                                LEFT JOIN tbl_karyawan kry ON kry.`nid` = mk.`kd_dosen`
                                JOIN ' . $tabel . ' km ON mk.`kd_matakuliah` = km.`kd_matakuliah`
                                WHERE mk.`kd_jadwal` LIKE "' . $this->session->userdata('id_jurusan_prasyarat') . '%"
                                AND a.`kd_prodi` = "' . $this->session->userdata('id_jurusan_prasyarat') . '"
                                AND km.`kd_kurikulum` LIKE "%' . $this->session->userdata('id_jurusan_prasyarat') . '%"
                                AND mk.`kd_tahunajaran` = "' . $this->session->userdata('tahunajaran') . '"
                                AND km.`semester_kd_matakuliah` = ' . $smtr . '
                                order by mk.kd_matakuliah ASC')->result();
		return $q;
	}

	function jdl_matkul_by_semester_feed($smtr)
	{
		if ($this->session->userdata('tahunajaran') > 20162) {
			$tabel = 'tbl_kurikulum_matkul_new';
			$kur = 'tbl_kurikulum_new';
		} else {
			$tabel = 'tbl_kurikulum_matkul';
			$kur = 'tbl_kurikulum';
		}
		$te = $this->session->userdata('sess_login');
		$as = $te['userid'];
		$q 	= $this->db->query('SELECT distinct 
									mk.status_feeder,
									mk.id_jadwal,
									mk.kd_matakuliah,
									mk.kelas,
									mk.kd_jadwal, 
									a.semester_matakuliah,
									a.nama_matakuliah,
									a.sks_matakuliah, 
									kry.nupn,
									kry.nidn,
									kry.nid,
									kry.nama 
								FROM tbl_jadwal_matkul_feeder mk
                                JOIN tbl_matakuliah a ON mk.`kd_matakuliah`=a.`kd_matakuliah`
                                LEFT JOIN tbl_karyawan kry ON kry.`nid` = mk.`kd_dosen`
                                JOIN ' . $tabel . ' km ON mk.`kd_matakuliah` = km.`kd_matakuliah`
                                JOIN ' . $kur . ' kur ON kur.`kd_kurikulum` = km.`kd_kurikulum` 
                                WHERE mk.`kd_jadwal` LIKE "' . $as . '%"
                                AND a.`kd_prodi` = "' . $as . '"
                                AND kur.`kd_prodi` = "' . $as . '"
                                AND mk.`kd_tahunajaran` = "' . $this->session->userdata('tahunajaran') . '"
                                AND km.`semester_kd_matakuliah` = ' . $smtr . '
                                AND kur.status = 1
                                order by mk.kd_matakuliah,mk.kelas ASC')->result();
		//$q = $this->db->query("CALL sp_jadwal(".$smtr.",'".$this->session->userdata('tahunajaran')."','".$as."')");
		return $q;
	}


	function jdl_matkul_by_semester_feed_new($smtr)
	{
		if ($this->session->userdata('tahunajaran') > 20162) {
			$tabel = 'tbl_kurikulum_matkul_new';
			$kur = 'tbl_kurikulum_new';
		} else {
			$tabel = 'tbl_kurikulum_matkul';
			$kur = 'tbl_kurikulum';
		}

		$tahunajaran = $this->session->userdata('tahunajaran');
		$userid      = $this->session->userdata('sess_login')['userid'];
		$kd_fakultas = get_kdfak_byprodi($userid);
		$get_kelas   = $this->db->query("SELECT distinct 
											mk.status_feeder,
											mk.id_jadwal,
											mk.kd_matakuliah,
											mk.kelas,
											mk.kd_jadwal, 
											a.semester_matakuliah,
											a.nama_matakuliah,
											a.sks_matakuliah, 
											kry.nupn,
											kry.nidn,
											kry.nid,
											kry.nama 
										FROM tbl_jadwal_matkul_feeder mk
		                                JOIN tbl_matakuliah a ON mk.`kd_matakuliah` = a.`kd_matakuliah`
		                                LEFT JOIN tbl_karyawan kry ON kry.`nid` = mk.`kd_dosen`
		                                JOIN $tabel km ON mk.`kd_matakuliah` = km.`kd_matakuliah`
		                                JOIN $kur kur ON kur.`kd_kurikulum` = km.`kd_kurikulum` 
		                                WHERE (mk.`kd_jadwal` LIKE '%$userid%' OR mk.kd_jadwal LIKE '$kd_fakultas/%')
		                                AND a.`kd_prodi` = '$userid'
		                                AND kur.`kd_prodi` = '$userid'
		                                AND kur.status = '1'
		                                AND mk.`kd_tahunajaran` = '$tahunajaran'
		                                AND km.`semester_kd_matakuliah` = '$smtr'
		                                order by mk.kd_matakuliah,mk.kelas ASC")->result();

		//$q = $this->db->query("CALL sp_jadwal(".$smtr.",'".$this->session->userdata('tahunajaran')."','".$as."')");
		return $get_kelas;
	}

	function dosen_ajar_feed($sesi, $ta)
	{
		$ajarDosen 	= $this->db->query("SELECT DISTINCT 
											mk.sks_matakuliah,
											mk.kd_prodi,
											mk.nama_matakuliah,
											mk.kd_matakuliah,
											mk.semester_matakuliah, 
											jdl.kd_jadwal,
											jdl.kd_tahunajaran,
											jdl.kelas,
											jdl.kd_dosen ,
											kry.nama,
											kry.nid,
											kry.nidn,
											kry.nupn,
											(SELECT SUM(mk.`sks_matakuliah`) AS sks 
											FROM tbl_jadwal_matkul_feeder jdl
											JOIN tbl_matakuliah mk ON jdl.`kd_matakuliah` = mk.`kd_matakuliah`
											WHERE jdl.`kd_dosen` = kry.`nid` 
											AND jdl.kd_tahunajaran = '$ta' 
											AND mk.kd_prodi = '$sesi'
											AND jdl.kd_jadwal LIKE '$sesi%') AS sks
										FROM tbl_jadwal_matkul_feeder jdl
										JOIN tbl_matakuliah mk ON jdl.`kd_matakuliah` = mk.`kd_matakuliah`
										JOIN tbl_karyawan kry ON jdl.`kd_dosen` = kry.`nid`
										WHERE jdl.kd_tahunajaran = '$ta' 
										AND mk.kd_prodi = '$sesi'
										AND jdl.kd_jadwal LIKE '$sesi%'
										ORDER BY kry.`nama` ASC")->result();
		return $ajarDosen;
	}

	function getkrsmhskp($prodi, $ta)
	{
		return $this->db->query("SELECT DISTINCT NIMHSMSMHS,NMMHSMSMHS FROM tbl_krs a
						JOIN tbl_mahasiswa b ON a.`npm_mahasiswa` = b.`NIMHSMSMHS`
						JOIN tbl_matakuliah c ON a.`kd_matakuliah` = c.`kd_matakuliah`
						WHERE a.`kd_krs` LIKE CONCAT(a.`npm_mahasiswa`,'" . $ta . "%') AND b.`KDPSTMSMHS` = '" . $prodi . "' AND c.`kd_prodi` = '" . $prodi . "'
						AND (c.`nama_matakuliah` LIKE '%kerja praktek%' OR c.`nama_matakuliah` LIKE '%magang%')");
	}

	function getkrsmhsskripsi($prodi, $ta)
	{
		return $this->db->query("SELECT DISTINCT NIMHSMSMHS,NMMHSMSMHS FROM tbl_krs a
						JOIN tbl_mahasiswa b ON a.`npm_mahasiswa` = b.`NIMHSMSMHS`
						JOIN tbl_matakuliah c ON a.`kd_matakuliah` = c.`kd_matakuliah`
						WHERE a.`kd_krs` LIKE CONCAT(a.`npm_mahasiswa`,'" . $ta . "%') AND b.`KDPSTMSMHS` = '" . $prodi . "' AND c.`kd_prodi` = '" . $prodi . "'
						AND c.`nama_matakuliah` LIKE 'skripsi%'");
	}

	function getkrsmhstesis($prodi, $ta)
	{
		return $this->db->query("SELECT DISTINCT NIMHSMSMHS,NMMHSMSMHS FROM tbl_krs a
						JOIN tbl_mahasiswa b ON a.`npm_mahasiswa` = b.`NIMHSMSMHS`
						JOIN tbl_matakuliah c ON a.`kd_matakuliah` = c.`kd_matakuliah`
						WHERE a.`kd_krs` LIKE CONCAT(a.`npm_mahasiswa`,'" . $ta . "%') AND b.`KDPSTMSMHS` = '" . $prodi . "' AND c.`kd_prodi` = '" . $prodi . "'
						AND c.`nama_matakuliah` LIKE 'tesis%'");
	}

	function get_jurusan_mhs($npm)
	{
		$this->db->select('NIMHSMSMHS,KDPSTMSMHS');
		$this->db->from('tbl_mahasiswa');
		$this->db->where('NIMHSMSMHS', $npm);
		return $this->db->get();
	}

	function flag($isi)
	{
		$query = $this->db->query('select * from tbl_nilai where kd_jadwal = "' . $isi . '"');
		return $query;
	}

	function get_kelas_mahasiswa($id)
	{
		$kode = $this->db->query("SELECT * from tbl_jadwal_matkul where id_jadwal = " . $id . "")->row();
		if ($kode->gabung > 0) {
			$q = $this->db->query('SELECT distinct mhs.`NIMHSMSMHS`,mhs.`NMMHSMSMHS`,b.`kd_jadwal` FROM tbl_krs b
											JOIN tbl_mahasiswa mhs ON b.`npm_mahasiswa` = mhs.`NIMHSMSMHS`
											JOIN tbl_verifikasi_krs v ON b.kd_krs = v.kd_krs
											WHERE
												v.status_verifikasi = 1
											AND 
												b.kd_jadwal = "' . $kode->kd_jadwal . '" OR kd_jadwal = "' . $kode->referensi . '"
											OR kd_jadwal IN (SELECT DISTINCT kd_jadwal FROM tbl_jadwal_matkul WHERE referensi = "' . $kode->referensi . '")
											ORDER BY mhs.NIMHSMSMHS ASC')->result();
		} else {
			$q = $this->db->query('SELECT distinct mhs.`NIMHSMSMHS`,mhs.`NMMHSMSMHS`,b.`kd_jadwal` FROM tbl_krs b
											JOIN tbl_mahasiswa mhs ON b.`npm_mahasiswa` = mhs.`NIMHSMSMHS`
											JOIN tbl_verifikasi_krs v ON b.kd_krs = v.kd_krs
											WHERE
												v.status_verifikasi = 1
											AND 
												b.kd_jadwal = "' . $kode->kd_jadwal . '" 
											OR kd_jadwal IN (SELECT DISTINCT kd_jadwal FROM tbl_jadwal_matkul WHERE referensi = "' . $kode->kd_jadwal . '")
											ORDER BY mhs.NIMHSMSMHS ASC')->result();
		}
		return $q;
	}

	function get_kelas_mahasiswa_nilai($id, $tipe)
	{
		// $this->db->select('a.kd_jadwal,c.id_jadwal,b.NIMHSMSMHS,b.NMMHSMSMHS,d.nilai,d.flag_publikasi');
		// $this->db->from('tbl_krs a');
		// $this->db->join('tbl_mahasiswa b', 'a.npm_mahasiswa = b.NIMHSMSMHS');
		// $this->db->join('tbl_jadwal_matkul c', 'a.kd_jadwal = c.kd_jadwal');
		// $this->db->join('tbl_nilai_detail d', 'd.npm_mahasiswa = a.npm_mahasiswa', 'left');
		// $this->db->where('c.id_jadwal', $id);
		// $this->db->where('d.tipe', $tipe);
		// $this->db->where('a.kd_matakuliah', 'd.kd_matakuliah');
		// $this->db->order_by('b.NIMHSMSMHS', 'asc');
		// return $this->db->get();

		$query = $this->db->query('
				SELECT mhs.NIMHSMSMHS,mhs.NMMHSMSMHS,nd.nilai,nd.flag_publikasi FROM tbl_krs krs
					JOIN tbl_mahasiswa mhs ON krs.`npm_mahasiswa` = mhs.`NIMHSMSMHS`
					JOIN tbl_jadwal_matkul jdl ON krs.`kd_jadwal` = jdl.`kd_jadwal`
					LEFT JOIN tbl_nilai_detail nd ON nd.`npm_mahasiswa` = krs.`npm_mahasiswa`
					WHERE jdl.`id_jadwal` = ' . $id . '
					AND nd.`tipe` = ' . $tipe . '
					AND krs.`kd_matakuliah` = nd.`kd_matakuliah`
			');

		return $query;
	}

	function get_kelas_mahasiswa_ujian($id)
	{
		$this->db->select('a.kd_jadwal,b.NIMHSMSMHS,b.NMMHSMSMHS');
		$this->db->from('tbl_krs a');
		$this->db->join('tbl_mahasiswa b', 'a.npm_mahasiswa = b.NIMHSMSMHS');
		$this->db->join('tbl_jadwal_matkul c', 'a.kd_jadwal = c.kd_jadwal');
		$this->db->join('tbl_sinkronisasi_renkeu d', 'b.NIMHSMSMHS = d.npm_mahasiswa');
		$this->db->where('c.id_jadwal', $id);
		$this->db->where('d.status >=', '2');
		$this->db->order_by('b.NIMHSMSMHS', 'asc');
		return $this->db->get();
	}

	function get_kelas_mahasiswa_ujian2($type1, $jadwal, $ta)
	{
		return $this->db->query("SELECT DISTINCT 
									a.npm_mahasiswa,
									b.status,
									(
										(
											(SELECT count(npm_mahasiswa) FROM tbl_absensi_mhs_new_20171 
											WHERE kd_jadwal = '$jadwal' 
											AND npm_mahasiswa = a.npm_mahasiswa 
											AND kehadiran <> 'A')
												/
											(SELECT MAX(pertemuan) FROM tbl_absensi_mhs_new_20171 
											WHERE kd_jadwal = '$jadwal')
										) * 100 
									) as persentaseabsen,

									(
										(SELECT count(npm_mahasiswa) FROM tbl_absensi_mhs_new_20171 
										WHERE kd_jadwal = '$jadwal' 
										AND npm_mahasiswa = a.npm_mahasiswa 
										AND kehadiran <> 'A')
									) as jumlahabsen

								FROM tbl_krs a
								JOIN tbl_sinkronisasi_renkeu b ON a.`npm_mahasiswa` = b.`npm_mahasiswa`
								WHERE a.`kd_jadwal` = '$jadwal' AND b.tahunajaran = '$ta'
								HAVING persentaseabsen >= 75
								ORDER BY npm_mahasiswa ASC");

		/*----------  QUERY JIKA ABSEN UTS < 75% tidak muncul di absen UTS  ----------*/
		/*
		SELECT DISTINCT 
			a.npm_mahasiswa,
			b.status,
			(
				(
					(SELECT count(npm_mahasiswa) FROM tbl_absensi_mhs_new_20171 
					where kd_jadwal = "'.$jadwal.'" 
					and npm_mahasiswa = a.npm_mahasiswa 
					and kehadiran <> "A")
						/
					(SELECT MAX(pertemuan) FROM tbl_absensi_mhs_new_20171 
					where kd_jadwal = "'.$jadwal.'")
				) * 100 
			) as persentaseabsen,
			(
				(SELECT count(npm_mahasiswa) FROM tbl_absensi_mhs_new_20171 
				where kd_jadwal = "'.$jadwal.'" 
				and npm_mahasiswa = a.npm_mahasiswa 
				and kehadiran <> "A")
			) as jumlahabsen
			FROM tbl_krs a
			JOIN tbl_sinkronisasi_renkeu b ON a.`npm_mahasiswa` = b.`npm_mahasiswa`
			WHERE a.`kd_jadwal` = "'.$jadwal.'" AND b.tahunajaran = "'.$ta.'"
			HAVING persentaseabsen >= 75
			order by npm_mahasiswa asc
		/*----------------------------------------------------------------------------*/
	}

	function get_matkul_ajar($ta, $jurusan, $semester)
	{
		$fakultas = get_kdfak_byprodi($jurusan);

		$this->db->distinct();
		$this->db->select('a.kd_matakuliah,b.nama_matakuliah,d.kd_jadwal,d.hari,d.kelas,c.nama,d.id_jadwal');

		if ($ta >= '20171') {
			$this->db->from('tbl_kurikulum_matkul_new a');
		} else {
			$this->db->from('tbl_kurikulum_matkul a');
		}

		$this->db->join('tbl_matakuliah b', 'a.kd_matakuliah = b.kd_matakuliah');
		$this->db->join('tbl_jadwal_matkul d', 'a.kd_matakuliah = d.kd_matakuliah');
		$this->db->join('tbl_karyawan c', 'd.kd_dosen = c.nid');
		$this->db->where('b.kd_prodi', $jurusan);
		$this->db->where('a.semester_kd_matakuliah', $semester);
		$this->db->group_start();
		$this->db->like('d.kd_jadwal', $jurusan, 'after');
		$this->db->or_like('d.kd_jadwal', $fakultas, 'after');
		$this->db->group_end();
		$this->db->where('d.kd_tahunajaran', $ta);
		return $this->db->get();
	}

	function getnilai($id, $npm, $tipe, $mk)
	{
		$this->db->select('a.*');
		$this->db->from('tbl_nilai_detail a');
		$this->db->join('tbl_jadwal_matkul b', 'a.kd_jadwal=b.kd_jadwal');
		$this->db->where('a.kd_jadwal', $id);
		$this->db->where('a.kd_matakuliah', $mk);
		$this->db->where('a.npm_mahasiswa', $npm);
		$this->db->where('a.tipe', $tipe);
		return $this->db->get();
	}

	function getnilaixy($id, $nim, $tp)
	{
		$hasil = $this->db->select('nilai')
			->from('tbl_nilai_detail_20161')
			->where('kd_jadwal', $id)
			->where('npm_mahasiswa', $nim)
			->where('tipe', $tp)
			->get();
		if ($hasil->num_rows() > 0) {
			return $hasil->row();
		} else {
			return array();
		}
	}
	function getnilaixyuas($id, $nim, $tp)
	{
		$hasil = $this->db->select('nilai')
			->from('tbl_nilai_detail_20161')
			->where('kd_jadwal', $id)
			->where('npm_mahasiswa', $nim)
			->where('tipe', $tp)
			->where('flag_publikasi', 2)
			->get();
		if ($hasil->num_rows() > 0) {
			return $hasil->row();
		} else {
			return array();
		}
	}
	function getnilaixxxy($id, $nim)
	{
		$hasil = $this->db->select('*')
			->from('tbl_nilai_detail_20161')
			->where('kd_jadwal', $id)
			->where('npm_mahasiswa', $nim)
			->get();
		if ($hasil->num_rows() > 0) {
			return $hasil->row();
		} else {
			return array();
		}
	}
	function getnilaix($id, $nim, $tp)
	{
		$hasil = $this->db->select('nilai')
			->from('tbl_nilai_detail')
			->where('kd_jadwal', $id)
			->where('npm_mahasiswa', $nim)
			->where('tipe', $tp)
			->get();
		if ($hasil->num_rows() > 0) {
			return $hasil->row();
		} else {
			return array();
		}
	}
	function getnilaixuas($id, $nim, $tp)
	{
		$hasil = $this->db->select('nilai')
			->from('tbl_nilai_detail')
			->where('kd_jadwal', $id)
			->where('npm_mahasiswa', $nim)
			->where('tipe', $tp)
			->where('flag_publikasi', 2)
			->get();
		if ($hasil->num_rows() > 0) {
			return $hasil->row();
		} else {
			return array();
		}
	}
	function getnilaixxx($id, $nim)
	{
		$hasil = $this->db->select('*')
			->from('tbl_nilai_detail')
			->where('kd_jadwal', $id)
			->where('npm_mahasiswa', $nim)
			->get();
		if ($hasil->num_rows() > 0) {
			return $hasil->row();
		} else {
			return array();
		}
	}
	function get_dosen($dosen)
	{
		$this->db->select('nama');
		$this->db->from('tbl_karyawan');
		$this->db->where('nid', $dosen);
		return $this->db->get();
	}

	function get_mk_by_jadwal($id)
	{
		$this->db->select('tbl_jadwal_matkul.kd_matakuliah,kelas,nama_matakuliah,kd_tahunajaran');
		$this->db->from('tbl_jadwal_matkul');
		$this->db->join('tbl_matakuliah', 'tbl_jadwal_matkul.kd_matakuliah = tbl_matakuliah.kd_matakuliah');
		$this->db->where('kd_jadwal', $id);
		return $this->db->get();
	}

	function get_krs_mahasiswa_sp($npm, $semester)
	{
		$kd_prodi = $this->db->query('select KDPSTMSMHS from tbl_mahasiswa where NIMHSMSMHS="' . $npm . '"')->row();
		$this->db->select('a.*, b.*,c.semester_kd_matakuliah');
		$this->db->from('tbl_krs_sp a');
		$this->db->join('tbl_matakuliah b', 'b.kd_matakuliah=a.kd_matakuliah');
		$this->db->join('tbl_verifikasi_krs_sp d', 'a.kd_krs = d.kd_krs');
		$this->db->join('tbl_kurikulum_matkul c', 'b.kd_matakuliah = c.kd_matakuliah');
		$this->db->where('d.npm_mahasiswa', $npm);
		$this->db->where('b.kd_prodi', $kd_prodi->KDPSTMSMHS);
		$this->db->where('SUBSTR(c.kd_kurikulum,1,5)', $kd_prodi->KDPSTMSMHS);
		$this->db->where('semester_krs', $semester);
		$this->db->where('d.status_verifikasi', '2');
		return $this->db->get();
	}

	function get_matkul_sp($prodi)
	{
		$this->db->distinct();
		$this->db->select('a.kd_matakuliah,c.nama_matakuliah,c.sks_matakuliah');
		$this->db->from('tbl_krs a');
		$this->db->join('tbl_verifikasi_krs b', 'a.kd_krs = b.kd_krs');
		$this->db->join('tbl_matakuliah c', 'a.kd_matakuliah = c.kd_matakuliah');
		$this->db->where('b.status_verifikasi', '2');
		$this->db->where('b.kd_jurusan', $prodi);
		$this->db->where('c.kd_prodi', $prodi);
		return $this->db->get();
	}

	function getkrsmhsbyprodi($prodi, $tahunMasuk, $tahunAkademik)
	{
		$this->db->distinct();
		$this->db->select('a.NIMHSMSMHS,a.NMMHSMSMHS,a.TAHUNMSMHS,b.tahunajaran,b.status_verifikasi,b.kd_krs');
		$this->db->from('tbl_mahasiswa a');
		$this->db->join('tbl_verifikasi_krs b', 'a.NIMHSMSMHS = b.npm_mahasiswa');

		if ($prodi != 'all') {
			$this->db->where('a.KDPSTMSMHS', $prodi);
		}
		if ($tahunMasuk != 'all') {
			$this->db->where('a.TAHUNMSMHS', $tahunMasuk);
		}

		$this->db->where('b.tahunajaran', $tahunAkademik);
		$this->db->order_by('a.NIMHSMSMHS', 'asc');
		return $this->db->get();
	}

	function getdetailslug($npm, $slug)
	{
		$this->db->select('*');
		$this->db->from('tbl_verifikasi_krs');
		$this->db->where('npm_mahasiswa', $npm);
		$this->db->where('slug_url', $slug);
		return $this->db->get();
	}

	function getkartumhs($kodefakultas, $atas, $bawah)
	{
		return $this->db->query("select a.NMMHSMSMHS,a.NIMHSMSMHS,a.TAHUNMSMHS from tbl_mahasiswa a 
								join tbl_jurusan_prodi b on b.kd_prodi = a.KDPSTMSMHS
								join tbl_fakultas c on c.kd_fakultas = b.kd_fakultas
								where FLAG_RENKEU = 5 and c.kd_fakultas = " . $kodefakultas . "
								and TAHUNMSMHS BETWEEN '" . $atas . "' AND '" . $bawah . "'
								order by NIMHSMSMHS")->result();
	}

	function get_peserta_sp($kode, $prodi)
	{
		$this->db->select('b.kd_krs,a.kd_matakuliah,c.nama_matakuliah,c.sks_matakuliah,b.npm_mahasiswa,d.NMMHSMSMHS,d.TAHUNMSMHS');
		$this->db->from('tbl_krs a');
		$this->db->join('tbl_verifikasi_krs b', 'a.kd_krs = b.kd_krs');
		$this->db->join('tbl_mahasiswa d', 'b.npm_mahasiswa = d.NIMHSMSMHS');
		$this->db->join('tbl_matakuliah c', 'a.kd_matakuliah = c.kd_matakuliah');
		$this->db->where('b.status_verifikasi', '2');
		$this->db->where('b.kd_jurusan', $prodi);
		$this->db->where('c.kd_prodi', $prodi);
		$this->db->where('a.kd_matakuliah', $kode);
		return $this->db->get();
	}

	function getmhslulus($tahun)
	{
		$this->db->distinct();
		$this->db->select('a.*,c.prodi,d.kd_fakultas,d.fakultas');
		$this->db->from('tbl_mahasiswa a');
		$this->db->join('tbl_sinkronisasi_renkeu b', 'a.NIMHSMSMHS = b.npm_mahasiswa', 'left');
		$this->db->join('tbl_jurusan_prodi c', 'a.KDPSTMSMHS = c.kd_prodi');
		$this->db->join('tbl_fakultas d', 'c.kd_fakultas = d.kd_fakultas');
		$this->db->where('a.STMHSMSMHS', 'L');
		$this->db->where('YEAR(TGLLSMSMHS)', $tahun);
		$this->db->order_by('a.KDPSTMSMHS', 'asc');
		return $this->db->get();
	}

	function getkrsanmhs($npm, $ta)
	{
		return $this->db->query("select * from tbl_sinkronisasi_renkeu where npm_mahasiswa = '" . $npm . "' and tahunajaran = '" . $ta . "'");
	}

	function renkeu_printKRS($npm_mahasiswa, $ta)
	{
		return $this->db->query("select * from tbl_sinkronisasi_renkeu where npm_mahasiswa = '" . $npm_mahasiswa . "' and tahunajaran ='" . $ta . "'");
	}
	function validjadwal($kodekrs)
	{
		$mhs  = $this->app_model->get_jurusan_mhs(substr($kodekrs, 0, 9))->row();
		return $this->db->query("SELECT a.`kd_jadwal`,b.`nama_matakuliah` FROM tbl_krs a
								JOIN tbl_matakuliah b ON a.`kd_matakuliah` = b.`kd_matakuliah`
								WHERE (b.`nama_matakuliah` NOT LIKE 'skripsi%' AND b.`nama_matakuliah` NOT LIKE '%ahir%' AND b.`nama_matakuliah` NOT LIKE '%kerja praktek%' AND b.`nama_matakuliah` NOT LIKE '%kerja mahasiswa%' AND b.`nama_matakuliah` NOT LIKE '%magang%' AND b.`nama_matakuliah` NOT LIKE '%tugas%' AND b.`nama_matakuliah` NOT LIKE '%seminar%' AND b.`nama_matakuliah` NOT LIKE '%tesis%')
								AND a.`kd_krs` = '" . $kodekrs . "' AND b.`kd_prodi` = '" . $mhs->KDPSTMSMHS . "' AND a.`kd_jadwal` IS NULL");
	}

	function validjadwal_tes($kodekrs)
	{
		$mhs  = $this->app_model->get_jurusan_mhs(substr($kodekrs, 0, 9))->row();
		return $this->db->query("SELECT a.`kd_jadwal`,b.`nama_matakuliah` FROM tbl_krs a
								JOIN tbl_matakuliah b ON a.`kd_matakuliah` = b.`kd_matakuliah`
								WHERE a.`kd_krs` = '" . $kodekrs . "' AND b.`kd_prodi` = '" . $mhs->KDPSTMSMHS . "' AND a.`kd_jadwal` IS NULL");
	}

	function validjadwal_2017($kodekrs)
	{
		$mhs  = $this->app_model->get_jurusan_mhs(substr($kodekrs, 0, 9))->row();
		return $this->db->query("SELECT a.`kd_jadwal`,b.`nama_matakuliah` FROM tbl_krs_tes a
								JOIN tbl_matakuliah b ON a.`kd_matakuliah` = b.`kd_matakuliah`
								WHERE a.`kd_krs` = '" . $kodekrs . "' AND b.`kd_prodi` = '" . $mhs->KDPSTMSMHS . "' AND a.`kd_jadwal` IS NULL");
	}

	function ketentuan_sks($ipss, $sks)
	{
		$ips = number_format($ipss, 2);
		if ($sks <= 24) {
			if ((($ips >= 0.00) && ($ips < 1.40)) && ($sks <= 11)) {
				$boleh = 1;
			} elseif ((($ips >= 1.40) && ($ips <= 1.99)) && ($sks <= 15)) {
				$boleh = 1;
			} elseif ((($ips >= 1.40) && ($ips <= 1.99)) && ($sks <= 15)) {
				$boleh = 1;
			} elseif ((($ips >= 2.00) && ($ips <= 2.49)) && ($sks <= 18)) {
				$boleh = 1;
			} elseif ((($ips >= 2.50) && ($ips <= 2.99)) && ($sks <= 21)) {
				$boleh = 1;
			} elseif ((($ips >= 3.00) && ($ips <= 4.00)) && ($sks <= 24)) {
				$boleh = 1;
			} else {
				$boleh = 0;
			}
			return $boleh;
		} else {
			$boleh = 0;
			return $boleh;
		}
	}

	function ketentuan_sks_2017($ipss, $sks)
	{
		$ips = number_format($ipss, 2);
		if ($sks <= 24) {
			if ((($ips >= 0.00) and ($ips < 1.40)) and ($sks <= 12)) {
				$boleh = 1;
			} elseif ((($ips >= 1.40) and ($ips <= 1.99)) and ($sks <= 15)) {
				$boleh = 1;
			} elseif ((($ips >= 2.00) and ($ips <= 2.49)) and ($sks <= 18)) {
				$boleh = 1;
			} elseif ((($ips >= 2.50) and ($ips <= 2.99)) and ($sks <= 21)) {
				$boleh = 1;
			} elseif ((($ips >= 3.00) and ($ips <= 4.00)) and ($sks <= 24)) {
				$boleh = 1;
			} elseif ($ips == 0.00) {
				$boleh = 1;
			} else {
				$boleh = 0;
			}
			return $boleh;
		} else {
			$boleh = 0;
			return $boleh;
		}
	}

	function getmabas1($tahun)
	{

		$this->db->select('*');

		$this->db->from('tbl_form_camaba a');

		//$this->db->join('tbl_mahasiswa b', 'b.NIMHSMSMHS = a.npm_baru');

		$this->db->join('tbl_jurusan_prodi c', 'a.prodi = c.kd_prodi');

		$this->db->where('a.status >=', 1);

		// $this->db->where('a.gelombang', 2);

		//$names = array('3','4','5');
		//$this->db->where_in('a.gelombang', $names);

		$this->db->where('SUBSTRING(nomor_registrasi, 1, 2) =', substr($tahun, 2, 4));

		$this->db->order_by('a.nomor_registrasi', 'asc');

		return $this->db->get();
	}

	function getmaba_new($tahun, $tipe)
	{
		$this->db2 = $this->load->database('regis', TRUE);
		$sql = "SELECT userid from tbl_file where userid like '19%' group by userid HAVING COUNT(*) > 4
								order by userid asc";


		// jika melakukan pengecekan jumlah file yg telah diunggah (PMB 2019)
		return $this->db2->query("SELECT nomor_registrasi,npm_baru,nama,prodi,kelas,status,gelombang 
								from tbl_form_pmb where status != 2 
								and status > 0 and program = '" . $tipe . "'
								and SUBSTRING(nomor_registrasi, 1, 2) = '" . substr($tahun, 2, 4) . "'
								order by nomor_registrasi asc");

		//OLD
		/*return $this->db2->query("SELECT nomor_registrasi,npm_baru,nama,prodi,kelas,status,gelombang 
								from tbl_form_pmb where status != 2 
								and status > 0 and program = '".$tipe."'
								and SUBSTRING(nomor_registrasi, 1, 2) = '".substr($tahun, 2,4)."'
								and user_input in (".$sql.")
								order by nomor_registrasi asc");*/

		// jika tidak melakukan pengecekan jumlah file prasyarat yg telah diunggah (PMB 2018)
		/*
		$this->db2->select('nomor_registrasi,npm_baru,nama,prodi,kelas,status');
		$this->db2->from('tbl_form_pmb a');
		$this->db2->where('a.status !=', 2);
		$this->db2->where('a.status >', 0);
		$this->db2->where('a.program', $tipe);
		$this->db2->where('SUBSTRING(nomor_registrasi, 1, 2) =', substr($tahun, 2,4));
		$this->db2->order_by('a.nomor_registrasi', 'asc');

		return $this->db2->get();
		*/
	}

	function getmabas2($tahun)
	{
		$this->db->select('*');
		$this->db->from('tbl_pmb_s2 a');
		$this->db->join('tbl_jurusan_prodi c', 'a.opsi_prodi_s2 = c.kd_prodi');
		$this->db->where('a.status >=', 1);
		$this->db->where('SUBSTRING(ID_registrasi, 1, 2) =', substr($tahun, 2, 4));
		$this->db->order_by('a.ID_registrasi', 'asc');
		return $this->db->get();
	}

	function get_semester_khs($smsawl, $smsnilai)
	{
		// buat semua tahun awal kuliah di ganjil
		if ($smsawl % 2 == 0) {
			$awal_masuk = $smsawl - 1;
		} else {
			$awal_masuk = $smsawl;
		}

		// hitung selisih
		$selisih = ($smsnilai) - ($awal_masuk);

		// ambil karakter pertama untuk proses pembagian
		$subs = substr($selisih, 0, 1);

		// jika tahun ajaran genap
		if ($smsnilai % 2 == 0) {

			// jika tahun awal kuliah di semester genap (S2)
			if ($smsawl % 2 == 0) {
				$addconst = 1;

				// jika tahun awal kulaih di ganjil
			} else {

				// untuk membedakan bla bla bla
				if ($subs == 1 and $selisih == 1) {
					$addconst = 0;
				} else {
					$addconst = 2;
				}
			}

			// jika tahun ajaran ganjil
		} else {

			// jika tahun awal kuliah di semester genap (S2)
			if ($smsawl % 2 == 0) {
				$addconst = 0;

				// jika tahun awal kuliah di ganjil
			} else {
				$addconst = 1;
			}
		}

		// jika semester awal kuliah di genap (S2)
		if ($smsawl % 2 == 0) {

			if ($subs == 1 and $selisih == 1) {
				$semawal = ($subs / 0.5) - $addconst;
			} else {
				$semawal = ($subs / 0.5) + $addconst;
			}

			// jika semester awal kuliah di ganjil
		} else {
			$semawal = ($subs / 0.5) + $addconst;
		}

		return $semawal;
	}

	function getdatapublish($prodi, $ta, $semester)
	{
		$q = $this->db->query("select distinct a.kd_matakuliah,a.nid,a.audit_date,a.publis_date,b.kelas,c.nama,d.nama_matakuliah from tbl_nilai_detail a 
								join tbl_jadwal_matkul b on a.kd_jadwal = b.kd_jadwal
								join tbl_karyawan c on a.nid = c.nid
								join tbl_matakuliah d on a.kd_matakuliah = d.kd_matakuliah
								where d.kd_prodi = '" . $prodi . "' and d.semester_matakuliah = " . $semester . " and a.kd_tahunakademik = '" . $ta . "'
								group by a.kd_jadwal order by a.kd_matakuliah asc ");
		return $q;
	}

	function get_data_jadwal($id, $prodi)
	{
		$query = $this->db->query("select distinct kd_jadwal,nama_matakuliah,kelas,nama from tbl_jadwal_matkul a
									join tbl_karyawan b on a.kd_dosen = b.nid
									join tbl_matakuliah c on a.kd_matakuliah = c.kd_matakuliah
									where a.id_jadwal = " . $id . " and c.kd_prodi = '" . $prodi . "'");
		return $query;
	}

	function getkrsbefore_feeder($kode, $kdmatukul)
	{
		$this->db->select('*');
		$this->db->from('tbl_krs_feeder');
		$this->db->where('kd_krs', $kode);
		$this->db->where('kd_matakuliah', $kdmatukul);
		return $this->db->get();
	}

	function getkrsbefore($kode, $kdmatukul)
	{
		$this->db->select('*');
		$this->db->from('tbl_krs');
		$this->db->where('kd_krs', $kode);
		$this->db->where('kd_matakuliah', $kdmatukul);
		return $this->db->get();
	}

	function getkrsbefore2($kode)
	{
		$this->db->select('*');
		$this->db->from('tbl_krs');
		$this->db->where('kd_krs', $kode);
		return $this->db->get();
	}

	function getkrsbefore_tes($kode, $kdmatukul)
	{
		$this->db->select('*');
		$this->db->from('tbl_krs_tes');
		$this->db->where('kd_krs', $kode);
		$this->db->where('kd_matakuliah', $kdmatukul);
		return $this->db->get();
	}

	function cekmkbarukrs($kode, $mkbaru)
	{
		$this->db->select('id_krs');
		$this->db->from('tbl_krs');
		$this->db->where('kd_krs', $kode);
		$this->db->where_not_in('kd_matakuliah', $mkbaru);
		return $this->db->get();
		//var_dump($q);exit();
	}

	function cekmkbarukrs_tes($kode, $mkbaru)
	{
		$this->db->select('id_krs');
		$this->db->from('tbl_krs_tes');
		$this->db->where('kd_krs', $kode);
		$this->db->where_not_in('kd_matakuliah', $mkbaru);
		return $this->db->get();
		//var_dump($q);exit();
	}

	function getstatusmahasiswa($fak, $prodi, $ta)
	{
		if (($fak == 0) and ($prodi == 0)) {
			$this->db->distinct();
			$this->db->select('`a`.*,b.`NMMHSMSMHS`,c.`prodi`,d.`fakultas`');
			$this->db->from('tbl_status_mahasiswa a');
			$this->db->join('tbl_mahasiswa b', 'a.npm = b.NIMHSMSMHS');
			$this->db->join('tbl_jurusan_prodi c', 'b.KDPSTMSMHS = c.kd_prodi');
			$this->db->join('tbl_fakultas d', 'c.kd_fakultas = d.kd_fakultas');
			$this->db->where('a.tahunajaran', $ta);
		} elseif (($fak != 0) and ($prodi == 0)) {
			$this->db->distinct();
			$this->db->select('`a`.*,b.`NMMHSMSMHS`,c.`prodi`,d.`fakultas`');
			$this->db->from('tbl_status_mahasiswa a');
			$this->db->join('tbl_mahasiswa b', 'a.npm = b.NIMHSMSMHS');
			$this->db->join('tbl_jurusan_prodi c', 'b.KDPSTMSMHS = c.kd_prodi');
			$this->db->join('tbl_fakultas d', 'c.kd_fakultas = d.kd_fakultas');
			$this->db->where('d.kd_fakultas', $fak);
			$this->db->where('a.tahunajaran', $ta);
		} else {
			//var_dump('aaaa');exit();
			$this->db->distinct();
			$this->db->select('`a`.*,b.`NMMHSMSMHS`,c.`prodi`,d.`fakultas`');
			$this->db->from('tbl_status_mahasiswa a');
			$this->db->join('tbl_mahasiswa b', 'a.npm = b.NIMHSMSMHS');
			$this->db->join('tbl_jurusan_prodi c', 'b.KDPSTMSMHS = c.kd_prodi');
			$this->db->join('tbl_fakultas d', 'c.kd_fakultas = d.kd_fakultas');
			$this->db->where('d.kd_fakultas', $fak);
			$this->db->where('b.KDPSTMSMHS', $prodi);
			$this->db->where('a.tahunajaran', $ta);
		}
		$this->db->order_by('npm', 'asc');
		return $this->db->get();
	}

	function getmhs($prodi, $tahun)
	{
		$this->db->distinct();
		$this->db->select('NIMHSMSMHS,NMMHSMSMHS,prodi,fakultas');
		$this->db->from('tbl_mahasiswa a');
		$this->db->join('tbl_jurusan_prodi c', 'a.KDPSTMSMHS = c.kd_prodi');
		$this->db->join('tbl_fakultas d', 'c.kd_fakultas = d.kd_fakultas');
		$this->db->where('KDPSTMSMHS', $prodi);
		$this->db->where('TAHUNMSMHS', $tahun);
		return $this->db->get();
	}

	function getsmsmhs($npm, $ta)
	{
		$query = $this->db->query("SELECT DISTINCT semester_krs,kd_krs FROM tbl_krs WHERE kd_krs like concat('" . $npm . "','" . $ta . "')");
		return $query;
	}

	function getsmsmhsnon($npm, $ta)
	{
		$query = $this->db->query("SELECT DISTINCT tahunajaran,status FROM tbl_status_mahasiswa WHERE npm = '" . $npm . "' AND tahunajaran = " . $ta . "");
		return $query;
	}

	function getmhsrenkeu($npm, $ta)
	{
		$this->db->select('status');
		$this->db->from('tbl_sinkronisasi_renkeu');
		$this->db->where('npm_mahasiswa', $npm);
		$this->db->where('tahunajaran', $ta);
		return $this->db->get();
	}

	function getjumlahkrsperprodi($prodi, $ta)
	{
		return $this->db->query("SELECT DISTINCT TAHUNMSMHS,COUNT(DISTINCT npm_mahasiswa) as total FROM tbl_verifikasi_krs a
								JOIN tbl_mahasiswa b ON a.`npm_mahasiswa` = b.`NIMHSMSMHS` 
								WHERE a.`tahunajaran` = '" . $ta . "' AND b.`KDPSTMSMHS` = '" . $prodi . "'
								GROUP BY b.`TAHUNMSMHS` ORDER BY b.`TAHUNMSMHS` ASC")->result();
	}

	function gettotalkrsperprodi($prodi, $ta)
	{
		return $this->db->query("SELECT COUNT(DISTINCT npm_mahasiswa) as total FROM tbl_verifikasi_krs a
								JOIN tbl_mahasiswa b ON a.`npm_mahasiswa` = b.`NIMHSMSMHS` 
								WHERE a.`tahunajaran` = '" . $ta . "' AND b.`KDPSTMSMHS` = '" . $prodi . "'")->row();
	}

	function getabcdeperprodi($prodi, $ta)
	{
		return $this->db->query("SELECT DISTINCT NLAKHTRLNM,COUNT(DISTINCT NIMHSMSMHS) as total FROM tbl_transaksi_nilai a
								JOIN tbl_mahasiswa b ON a.`NIMHSTRLNM` = b.`NIMHSMSMHS` 
								WHERE a.`THSMSTRLNM` = '" . $ta . "' AND b.`KDPSTMSMHS` = '" . $prodi . "' AND a.`NLAKHTRLNM` != ''
								GROUP BY a.`NLAKHTRLNM` ORDER BY a.`NLAKHTRLNM` ASC")->result();
	}

	function getipsperprodi1($prodi, $ta)
	{
		return $this->db->query("SELECT COUNT(DISTINCT NIMHSMSMHS) as total FROM tbl_ips a
								JOIN tbl_mahasiswa b ON a.`npm_mahasiswa` = b.`NIMHSMSMHS` 
								WHERE a.`tahunajaran` = '" . $ta . "' AND b.`KDPSTMSMHS` = '" . $prodi . "' AND ips < 2.500")->row();
	}

	function getipsperprodi2($prodi, $ta)
	{
		return $this->db->query("SELECT COUNT(DISTINCT NIMHSMSMHS) as total FROM tbl_ips a
								JOIN tbl_mahasiswa b ON a.`npm_mahasiswa` = b.`NIMHSMSMHS` 
								WHERE a.`tahunajaran` = '" . $ta . "' AND b.`KDPSTMSMHS` = '" . $prodi . "' AND (ips > 2.499 AND ips < 3.000)")->row();
	}

	function getipsperprodi3($prodi, $ta)
	{
		return $this->db->query("SELECT COUNT(DISTINCT NIMHSMSMHS) as total FROM tbl_ips a
								JOIN tbl_mahasiswa b ON a.`npm_mahasiswa` = b.`NIMHSMSMHS` 
								WHERE a.`tahunajaran` = '" . $ta . "' AND b.`KDPSTMSMHS` = '" . $prodi . "' AND ips > 2.999")->row();
	}

	function get_laporan_cuti($prodi, $thn)
	{
		return $this->db->query('SELECT DISTINCT b.`prodi`,COUNT(npm) AS jml FROM tbl_status_mahasiswa a JOIN tbl_mahasiswa g ON a.`npm`=g.`NIMHSMSMHS`
											JOIN tbl_jurusan_prodi b ON b.`kd_prodi`=g.`KDPSTMSMHS` WHERE STATUS = "C" AND validate = 1
											AND b.`kd_prodi` = "' . $prodi . '" AND a.`tahunajaran` = "' . $thn . '"')->row();
	}

	function get_dostitap($prodi, $th)
	{
		return $this->db->query("SELECT COUNT(DISTINCT nid,nama) as juml_dsn FROM tbl_karyawan a
								JOIN tbl_jadwal_matkul b ON a.nid = b.kd_dosen
								WHERE tetap IS NULL AND b.kd_tahunajaran = '" . $th . "' AND b.kd_jadwal LIKE '" . $prodi . "%'")->row();
	}

	function get_dostap($prodi, $th)
	{
		return $this->db->query("SELECT COUNT(DISTINCT nid,nama) as juml_dsn FROM tbl_karyawan a
								WHERE tetap = 1 AND a.jabatan_id = '" . $prodi . "'")->row();
	}

	function getipkmhs($npm)
	{
		$hitung_ips = $this->db->query('SELECT nl.`KDKMKTRLNM`,
										IF(nl.`THSMSTRLNM`<"20151",(SELECT DISTINCT nama_matakuliah FROM tbl_matakuliah_copy WHERE kd_matakuliah = nl.`KDKMKTRLNM` AND kd_prodi = nl.`KDPSTTRLNM` AND tahunakademik = nl.`THSMSTRLNM` ORDER BY id_matakuliah DESC LIMIT 1) ,
											(SELECT DISTINCT nama_matakuliah FROM tbl_matakuliah WHERE kd_matakuliah = nl.`KDKMKTRLNM` AND kd_prodi = nl.`KDPSTTRLNM` ORDER BY id_matakuliah DESC LIMIT 1) ) AS nama_matakuliah,
										IF(nl.`THSMSTRLNM`<"20151",(SELECT DISTINCT sks_matakuliah FROM tbl_matakuliah_copy WHERE kd_matakuliah = nl.`KDKMKTRLNM` AND kd_prodi = nl.`KDPSTTRLNM` AND tahunakademik = nl.`THSMSTRLNM` ORDER BY id_matakuliah DESC LIMIT 1) ,
											(SELECT DISTINCT sks_matakuliah FROM tbl_matakuliah WHERE kd_matakuliah = nl.`KDKMKTRLNM` AND kd_prodi = nl.`KDPSTTRLNM` ORDER BY id_matakuliah DESC LIMIT 1) ) AS sks_matakuliah,
										MIN(nl.`NLAKHTRLNM`) AS NLAKHTRLNM,MAX(nl.`THSMSTRLNM`) AS THSMSTRLNM,MAX(nl.`BOBOTTRLNM`) AS BOBOTTRLNM FROM tbl_transaksi_nilai nl 
										WHERE nl.`NIMHSTRLNM` = "' . $npm . '" and NLAKHTRLNM != "T"
										GROUP BY nl.`KDKMKTRLNM` ORDER BY THSMSTRLNM ASC')->result();
		// var_dump($hitung_ips);exit();

		$st = 0;
		$ht = 0;
		foreach ($hitung_ips as $iso) {
			$h = 0;


			$h = $iso->BOBOTTRLNM * $iso->sks_matakuliah;
			$ht =  $ht + $h;
			//echo $iso->NLAKHTRLNM.' - '.$iso->BOBOTTRLNM.' - '.$iso->sks_matakuliah.'<br>';
			$st = $st + $iso->sks_matakuliah;
		}
		//echo $st.'<br>';
		//echo $ht.'<br>';
		$ipk_nr = $ht / $st;
		return number_format($ipk_nr, 2);
	}

	function getipkmhslain($npm, $prodi)
	{
		$q = $this->db->query('SELECT nl.`KDKMKTRLNM`,
								IF(nl.`THSMSTRLNM`<"20151",(SELECT DISTINCT nama_matakuliah FROM tbl_matakuliah_copy WHERE kd_matakuliah = nl.`KDKMKTRLNM` AND kd_prodi = nl.`KDPSTTRLNM` AND tahunakademik = nl.`THSMSTRLNM` ORDER BY id_matakuliah DESC LIMIT 1) ,
									(SELECT DISTINCT nama_matakuliah FROM tbl_matakuliah WHERE kd_matakuliah = nl.`KDKMKTRLNM` AND kd_prodi = nl.`KDPSTTRLNM` ORDER BY id_matakuliah DESC LIMIT 1) ) AS nama_matakuliah,
								IF(nl.`THSMSTRLNM`<"20151",(SELECT DISTINCT sks_matakuliah FROM tbl_matakuliah_copy WHERE kd_matakuliah = nl.`KDKMKTRLNM` AND kd_prodi = nl.`KDPSTTRLNM` AND tahunakademik = nl.`THSMSTRLNM` ORDER BY id_matakuliah DESC LIMIT 1) ,
									(SELECT DISTINCT sks_matakuliah FROM tbl_matakuliah WHERE kd_matakuliah = nl.`KDKMKTRLNM` AND kd_prodi = nl.`KDPSTTRLNM` ORDER BY id_matakuliah DESC LIMIT 1) ) AS sks_matakuliah,
								MIN(nl.`NLAKHTRLNM`) AS NLAKHTRLNM,nl.`THSMSTRLNM`,MAX(nl.`BOBOTTRLNM`) AS BOBOTTRLNM FROM tbl_transaksi_nilai nl 
								WHERE nl.`NIMHSTRLNM` = "' . $npm . '" and NLAKHTRLNM != "T"
								GROUP BY nl.`KDKMKTRLNM` ORDER BY nl.`THSMSTRLNM` ASC
								')->result();
		$tskss = 0;
		$tnl = 0;
		foreach ($q as $row) {
			$nk = ($row->sks_matakuliah * $row->BOBOTTRLNM);
			$tskss = $tskss + $row->sks_matakuliah;
			$tnl = $tnl + $nk;
		}
		$ipk = number_format($tnl / $tskss, 2);
		return $ipk;
	}

	function getipsmhs($prodi, $npm, $ta)
	{
		$hitung_ips = $this->db->query('SELECT a.`NIMHSTRLNM`,a.`KDKMKTRLNM`,a.`NLAKHTRLNM`,a.`BOBOTTRLNM`,b.`sks_matakuliah` FROM tbl_transaksi_nilai a
										JOIN tbl_matakuliah b ON a.`KDKMKTRLNM` = b.`kd_matakuliah`
										WHERE a.`kd_transaksi_nilai` IS NOT NULL AND kd_prodi = ' . $prodi . ' 
										AND NIMHSTRLNM = "' . $npm . '" and THSMSTRLNM = "' . $ta . '" and NLAKHTRLNM <> "T" ')->result();

		$st = 0;
		$ht = 0;
		foreach ($hitung_ips as $iso) {
			$h = 0;

			$h = $iso->BOBOTTRLNM * $iso->sks_matakuliah;
			$ht =  $ht + $h;

			$st = $st + $iso->sks_matakuliah;
		}
		if ($ht == 0) {
			$ips_nr = 0.00;
		} else {
			$ips_nr = $ht / $st;
		}

		return number_format($ips_nr, 2);
	}

	function get_jml_sts_unused($tahunajar)
	{
		$data = $this->db->query("SELECT 
									COUNT(DISTINCT kd_krs) AS jml, 
									b.prodi, 
									b.kd_prodi, 
									c.fakultas, 
									SUBSTR(kd_krs,13,5) AS tahunajaran 
								FROM tbl_krs a 
								JOIN tbl_mahasiswa d ON a.`npm_mahasiswa` = d.`NIMHSMSMHS`
								JOIN tbl_jurusan_prodi b ON d.`KDPSTMSMHS`= b.kd_prodi
								JOIN tbl_fakultas c ON b.kd_fakultas = c.kd_fakultas 
								WHERE (npm_mahasiswa IS NOT NULL OR npm_mahasiswa != '' OR npm_mahasiswa != 0)
								AND a.`kd_krs` LIKE CONCAT(npm_mahasiswa,'$tahunajar%') 
								GROUP BY b.kd_prodi 
								ORDER BY b.kd_prodi ASC")->result();
		return $data;
	}

	function get_sts_cuti($th)
	{
		return $this->db->query('SELECT COUNT(DISTINCT npm) as jum FROM tbl_status_mahasiswa a JOIN tbl_mahasiswa b ON a.`npm`=b.`NIMHSMSMHS`
								WHERE a.`status`="C" AND a.`tahunajaran` = "' . $th . '" GROUP BY b.`KDPSTMSMHS` order by b.`KDPSTMSMHS` asc')->result();
	}

	function jml_detil($prodi, $tahunakademik)
	{
		return $this->db->query("SELECT DISTINCT 
									d.`NIMHSMSMHS`,
									d.`NMMHSMSMHS`,
									a.`semester_krs`,
									a.`kd_krs`,
									SUM(c.`sks_matakuliah`) AS tot
								FROM tbl_krs a 
								JOIN tbl_matakuliah c ON a.`kd_matakuliah` = c.`kd_matakuliah`
								JOIN tbl_mahasiswa d ON a.`npm_mahasiswa` = d.`NIMHSMSMHS`
								WHERE a.kd_krs LIKE CONCAT (a.npm_mahasiswa,'$tahunakademik%') 
								AND d.KDPSTMSMHS = '$prodi'
								AND c.`kd_prodi` = '$prodi' 
								GROUP BY a.`kd_krs`")->result();
	}

	function jml_cuti($prd, $tah)
	{
		return $this->db->query('SELECT distinct a.NIMHSMSMHS,a.NMMHSMSMHS FROM tbl_mahasiswa a JOIN tbl_status_mahasiswa b ON a.NIMHSMSMHS = b.npm
								WHERE b.status = "C" AND a.KDPSTMSMHS = "' . $prd . '" AND b.tahunajaran = "' . $tah . '" and validate >= 1')->result();
	}

	function getdatamku()
	{
		return $this->db->query("SELECT DISTINCT 
									id_matakuliah,
									kd_matakuliah,
									nama_matakuliah,
									sks_matakuliah 
								FROM tbl_matakuliah
								WHERE kd_matakuliah like 'MKU%' 
								OR kd_matakuliah like 'MKDU%' 
								OR kd_matakuliah like 'MKWU%' 
								GROUP BY kd_matakuliah
								ORDER BY kd_matakuliah ASC");
	}

	function getdatamku_edit($prodi)
	{
		return $this->db->query("SELECT DISTINCT 
									id_matakuliah,
									kd_matakuliah,
									nama_matakuliah,
									sks_matakuliah 
								FROM tbl_matakuliah
								WHERE kd_matakuliah LIKE 'MKU-%' 
								OR kd_matakuliah LIKE 'MKWU-%' 
								OR kd_matakuliah LIKE 'MKDU-%'
								GROUP BY kd_matakuliah
								ORDER BY kd_matakuliah ASC");
	}

	function getdatajadwalmku($ta)
	{
		return $this->db->query("SELECT * FROM tbl_jadwal_matkul a
								JOIN tbl_ruangan b ON a.kd_ruangan = b.id_ruangan
								WHERE audit_user = 'baa' 
								AND kd_tahunajaran = '" . $ta . "'");
	}

	function get_beban_bio($dosen, $tahun)
	{
		return $this->db->query("SELECT DISTINCT c.nama_matakuliah,c.sks_matakuliah,b.kd_matakuliah,b.kd_jadwal,b.kd_tahunajaran,b.kelas FROM tbl_jadwal_matkul b
								JOIN tbl_matakuliah c ON c.`kd_matakuliah` = b.`kd_matakuliah`
								WHERE b.kd_dosen = '" . $dosen . "' AND b.kd_tahunajaran = '" . $tahun . "' AND c.kd_prodi = SUBSTR(b.kd_jadwal,1,5)
								AND (c.nama_matakuliah NOT LIKE 'skripsi%' AND c.nama_matakuliah NOT LIKE 'kerja praktek%' AND c.nama_matakuliah NOT LIKE 'tesis%' AND c.nama_matakuliah NOT LIKE 'magang%'
								AND c.nama_matakuliah NOT LIKE 'kuliah kerja%') AND b.`kd_jadwal` IN (SELECT kd_jadwal FROM tbl_krs WHERE kd_jadwal IS NOT NULL)");
	}

	function getcekform3($npm, $ta)
	{
		$this->db->select('status_verifikasi');
		$this->db->from('tbl_verifikasi_krs');
		$this->db->where('npm_mahasiswa', $npm);
		$this->db->where('tahunajaran', $ta);
		return $this->db->get();
	}

	function getcekform3smt($kd)
	{
		$this->db->select('status_verifikasi');
		$this->db->from('tbl_verifikasi_krs');
		$this->db->where('kd_krs', $kd);
		$this->db->where('status_verifikasi', 10);
		return $this->db->get();
	}

	function get_nama_kary($id)
	{
		$this->db->select('nama');
		$this->db->from('tbl_karyawan');
		$this->db->where('kelas', $id);
		return $this->db->get()->result()->nama;
	}

	function rekap_krs($tahun, $prodi)
	{
		return $this->db->query("SELECT DISTINCT a.`npm_mahasiswa`,d.`NMMHSMSMHS`,a.`semester_krs`,SUM(c.`sks_matakuliah`) AS tot
								FROM tbl_krs a JOIN tbl_matakuliah c ON a.`kd_matakuliah` = c.`kd_matakuliah`
								JOIN tbl_mahasiswa d ON a.`npm_mahasiswa` = d.`NIMHSMSMHS`
								WHERE a.kd_krs LIKE CONCAT (a.npm_mahasiswa,'" . $tahun . "%') AND d.KDPSTMSMHS = '" . $prodi . "'
								AND c.`kd_prodi` = '" . $prodi . "' GROUP BY a.`kd_krs`");
	}
	function rekap_krs2($tahun, $prodi, $dpa)
	{
		return $this->db->query("SELECT DISTINCT a.`npm_mahasiswa`,d.`NMMHSMSMHS`,a.`semester_krs`,
								(SELECT SUM(c.`sks_matakuliah`) FROM tbl_matakuliah c WHERE a.`kd_matakuliah` = c.`kd_matakuliah` AND c.`kd_prodi` = '" . $prodi . "'  )AS tot,f.`nama`,a.`kd_krs`
								FROM tbl_krs a 
								JOIN tbl_mahasiswa d ON a.`npm_mahasiswa` = d.`NIMHSMSMHS`
								JOIN tbl_verifikasi_krs e ON a.`kd_krs` = e.`kd_krs`
								JOIN tbl_karyawan f ON e.`id_pembimbing` = f.`nid`
								WHERE a.kd_krs LIKE CONCAT (a.npm_mahasiswa,'" . $tahun . "%') AND d.KDPSTMSMHS = '" . $prodi . "' AND f.`nama` = '" . $dpa . "'
								GROUP BY a.`kd_krs`");
	}

	function getindekspembayaran($id)
	{
		if ($id == 0) {
			$this->db->distinct();
			$this->db->select('a.*,b.prodi,c.fakultas,d.tahunajaran as ta');
			$this->db->from('tbl_index_pembayaran a');
			$this->db->join('tbl_jurusan_prodi b', 'a.kd_prodi = b.kd_prodi');
			$this->db->join('tbl_fakultas c', 'b.kd_fakultas = c.kd_fakultas');
			$this->db->join('tbl_tahunajaran d', 'a.tahunajaran = d.id_tahunajaran');
		} else {
			$this->db->distinct();
			$this->db->select('a.*,b.prodi,c.fakultas,d.tahunajaran as ta');
			$this->db->from('tbl_index_pembayaran a');
			$this->db->join('tbl_jurusan_prodi b', 'a.kd_prodi = b.kd_prodi');
			$this->db->join('tbl_fakultas c', 'b.kd_fakultas = c.kd_fakultas');
			$this->db->join('tbl_tahunajaran d', 'a.tahunajaran = d.id_tahunajaran');
			$this->db->where('a.id_index', $id);
		}
		return $this->db->get();
	}

	function gotipklulus($tahun, $user, $ta)
	{
		if ($user == 0) {
			return $this->db->query("SELECT AVG(a.`NLIPKTRAKM`) AS ipk FROM tbl_aktifitas_kuliah_mahasiswa a JOIN tbl_mahasiswa b ON a.`NIMHSTRAKM` = b.`NIMHSMSMHS` 
									 WHERE b.`STMHSMSMHS` = 'L' AND YEAR(b.`TGLLSMSMHS`) = '" . $tahun . "' AND a.`THSMSTRAKM` = '" . $ta . "'")->row()->ipk;
		} elseif ($user > 0 and $user < 20) {
			$prd = $this->app_model->getdetail('tbl_jurusan_prodi', 'kd_fakultas', $user, 'kd_fakultas', 'asc')->result();
			$jumlh = '';
			foreach ($prd as $key) {
				$jumlh = $jumlh . "'" . $key->kd_prodi . "',";
			}
			$akhir = substr($jumlh, 0, -1);
			return $this->db->query("SELECT AVG(a.`NLIPKTRAKM`) AS ipk FROM tbl_aktifitas_kuliah_mahasiswa a JOIN tbl_mahasiswa b ON a.`NIMHSTRAKM` = b.`NIMHSMSMHS` 
									 WHERE b.`STMHSMSMHS` = 'L' AND YEAR(b.`TGLLSMSMHS`) = '" . $tahun . "' AND a.`THSMSTRAKM` = '" . $ta . "'
									 and b.KDPSTMSMHS IN (" . $akhir . ")")->row()->ipk;
		} else {
			return $this->db->query("SELECT AVG(a.`NLIPKTRAKM`) AS ipk FROM tbl_aktifitas_kuliah_mahasiswa a JOIN tbl_mahasiswa b ON a.`NIMHSTRAKM` = b.`NIMHSMSMHS` 
									 WHERE b.`STMHSMSMHS` = 'L' AND YEAR(b.`TGLLSMSMHS`) = '" . $tahun . "' AND a.`THSMSTRAKM` = '" . $ta . "'
									 and b.KDPSTMSMHS = '" . $user . "'")->row()->ipk;
		}
	}

	function getipklulusan($tahun)
	{
		$getMhsLulus = $this->db->query("SELECT NIMHSMSMHS from tbl_mahasiswa where STMHSMSMHS = 'L' and TGLLSMSMHS = '" . $tahun . "'");
		foreach ($getMhsLulus->result() as $mahasiswa) {
		}
	}

	function gotipkon($ta, $user)
	{
		if ($user == 0) {
			return $this->db->query("SELECT AVG(a.`NLIPKTRAKM`) AS ipk FROM tbl_aktifitas_kuliah_mahasiswa a 
									JOIN tbl_mahasiswa b ON a.`NIMHSTRAKM` = b.`NIMHSMSMHS`
									WHERE a.`THSMSTRAKM` = '" . $ta . "'")->row()->ipk;
		} elseif ($user > 0 and $user < 20) {
			$prd = $this->app_model->getdetail('tbl_jurusan_prodi', 'kd_fakultas', $user, 'kd_fakultas', 'asc')->result();
			$jumlh = '';
			foreach ($prd as $key) {
				$jumlh = $jumlh . "'" . $key->kd_prodi . "',";
			}
			$akhir = substr($jumlh, 0, -1);
			return $this->db->query("SELECT AVG(a.`NLIPKTRAKM`) AS ipk FROM tbl_aktifitas_kuliah_mahasiswa a 
									JOIN tbl_mahasiswa b ON a.`NIMHSTRAKM` = b.`NIMHSMSMHS`
									WHERE a.`THSMSTRAKM` = '" . $ta . "' and b.KDPSTMSMHS IN (" . $akhir . ")")->row()->ipk;
		} else {
			return $this->db->query("SELECT AVG(a.`NLIPKTRAKM`) AS ipk FROM tbl_aktifitas_kuliah_mahasiswa a 
									JOIN tbl_mahasiswa b ON a.`NIMHSTRAKM` = b.`NIMHSMSMHS`
								 	WHERE a.`THSMSTRAKM` = '" . $ta . "' and b.KDPSTMSMHS = '" . $user . "'")->row()->ipk;
		}
	}

	function getdataaktifitas($prodi, $ta)
	{
		return $this->db->query("SELECT 
									a.* , 
									b.NMMHSMSMHS,
									b.TAHUNMSMHS 
								from tbl_aktifitas_kuliah_mahasiswa a join tbl_mahasiswa b on a.NIMHSTRAKM = b.NIMHSMSMHS 
								where a.THSMSTRAKM = '" . $ta . "' 
								and b.KDPSTMSMHS = '" . $prodi . "' 
								order by a.NIMHSTRAKM asc")->result();
	}

	function editbio($nim)
	{
		$this->db->select('a.NIMHSMSMHS,a.NMMHSMSMHS,a.TGLHRMSMHS,a.TPLHRMSMHS,b.nama_ibu,a.NIKMSMHS,a.NISNMSMHS,b.provinsi,b.kota,b.kecamatan,b.kelurahan,b.alamat,b.no_hp,b.email');
		$this->db->from('tbl_mahasiswa a');
		$this->db->join('tbl_bio_mhs b', 'a.NIMHSMSMHS = b.npm', 'left');
		$this->db->where('a.NIMHSMSMHS', $nim);
		return $this->db->get();
	}

	function cekbio($npm)
	{
		return $this->db->select('*')->from('tbl_bio_mhs')->where('npm', $npm);
	}

	function pstpmb($th, $pr, $gl)
	{
		if ($th == date('Y')) {
			if ($pr == 'S1') {
				return $this->db->select("*")
					->from("tbl_form_camaba")
					->where("gelombang", $gl)
					->get();
			} else {
				return $this->db->select("*")
					->from("tbl_pmb_s2")
					->where("gelombang", $gl)
					->get();
			}
		} else {
			if ($pr == 'S1') {
				return $this->db->select("*")
					->from("tbl_form_camaba_" . $th . "")
					->where("gelombang", $gl)
					->get();
			} else {
				return $this->db->select("*")
					->from("tbl_pmb_s2_" . $th . "")
					->where("gelombang", $gl)
					->get();
			}
		}
	}

	function dwld_pstpmb($th, $pr, $gl)
	{
		if ($th == date('Y')) {
			if ($pr == 'S1') {
				return $this->db->select("*")
					->from("tbl_form_camaba")
					->where("gelombang", $gl)
					->get();
			} else {
				return $this->db->select("*")
					->from("tbl_pmb_s2")
					->where("gelombang", $gl)
					->get();
			}
		} else {
			if ($pr == 'S1') {
				return $this->db->select("*")
					->from("tbl_form_camaba_" . $th . "")
					->where("gelombang", $gl)
					->get();
			} else {
				return $this->db->select("*")
					->from("tbl_pmb_s2_" . $th . "")
					->where("gelombang", $gl)
					->get();
			}
		}
	}

	function get_matkul_perbaikan($ta, $prodi, $npm)
	{
		$q = $this->db->query("SELECT DISTINCT b.`kd_matakuliah`,b.nama_matakuliah,b.sks_matakuliah,b.prasyarat_matakuliah,d.semester_kd_matakuliah,c.NLAKHTRLNM 
								FROM tbl_matakuliah b
								JOIN tbl_transaksi_nilai c ON b.`kd_matakuliah` = c.`KDKMKTRLNM`
								JOIN tbl_kurikulum_matkul d ON b.kd_matakuliah = d.kd_matakuliah
								WHERE b.`kd_prodi` = '" . $prodi . "' AND c.`THSMSTRLNM` = '" . $ta . "' AND c.`NIMHSTRLNM` = '" . $npm . "' 
								AND (NLAKHTRLNM = 'D' OR NLAKHTRLNM = 'E') AND d.`kd_kurikulum` LIKE '" . $prodi . "%'");
		return $q;
	}

	function getta_remid()
	{
		return $this->db->query("SELECT * from tbl_tahunakademik where kode < '20162'");
	}

	function pst_sp($nim, $thn, $kdmk)
	{
		$kdkrs = $nim . substr($thn, 0, 4);
		return $this->db->query("SELECT * from tbl_krs_sp where kd_krs like concat(" . $kdkrs . ",'3%') and kd_matakuliah = '" . $kdmk . "'");
	}

	function getbayarsp($kodemk)
	{
		$year = $this->session->userdata('ta');
		$q = $this->db->query("SELECT count(distinct a.npm_mahasiswa) as jml from tbl_sinkronisasi_renkeu a
								join tbl_krs_sp b on a.npm_mahasiswa = b.npm_mahasiswa
								where b.kd_jadwal = '" . $kodemk . "' and b.kd_krs like concat(b.npm_mahasiswa,'" . $year . "%') and a.tahunajaran = '" . $year . "'")->row()->jml;
		return $q;
	}

	function get_matkul_ngulang($ganjilgenap, $npm)
	{
		return $this->db->query("SELECT 
									d.*,
									a.semester_kd_matakuliah,
									c.NLAKHTRLNM,
									c.THSMSTRLNM
								FROM tbl_kurikulum_matkul_new a 
							    JOIN tbl_kurikulum_new b ON b.kd_kurikulum = a.kd_kurikulum
								JOIN tbl_transaksi_nilai c ON a.kd_matakuliah = c.KDKMKTRLNM
							    JOIN tbl_matakuliah d ON a.`kd_matakuliah` = d.kd_matakuliah
								WHERE c.NIMHSTRLNM = '$npm' 
							  	AND c.NLAKHTRLNM IN ('C','C+','D', 'E') 
							  	AND c.KDPSTTRLNM = SUBSTR(b.kd_kurikulum, 1, 5) 
							  	AND c.KDPSTTRLNM = d.kd_prodi
							  	AND b.status = 1
							    AND CASE WHEN INSTR(d.nama_matakuliah, 'KKN') < 1 THEN SUBSTR(THSMSTRLNM, -1, 4) = '$ganjilgenap' 
							    -- HAVING MOD(THSMSTRLNM, '$ganjilgenap') < 1
								END");
	}

	function angkatan_mhs($prodi)
	{
		$query = $this->db->query('SELECT mhs.`TAHUNMSMHS` FROM tbl_mahasiswa mhs WHERE mhs.`KDPSTMSMHS` = ' . $prodi . ' GROUP BY mhs.`TAHUNMSMHS`')->result();

		return $query;
	}

	function sum_sks_konversi($nim, $prodi, $ta)
	{
		if ($ta < '20151') {
			$query = $this->db->query('SELECT SUM(mk.`sks_matakuliah`) as sks FROM tbl_transaksi_nilai_konversi nl
									JOIN tbl_matakuliah_copy mk ON nl.`KDKMKTRLNM` = mk.`kd_matakuliah`
									WHERE nl.`NIMHSTRLNM` = "' . $nim . '" AND mk.`kd_prodi` = ' . $prodi . ' and tahunakademik  = "' . $ta . '"
									')->row();
		} else {
			$query = $this->db->query('SELECT SUM(mk.`sks_matakuliah`) as sks FROM tbl_transaksi_nilai_konversi nl
									JOIN tbl_matakuliah mk ON nl.`KDKMKTRLNM` = mk.`kd_matakuliah`
									WHERE nl.`NIMHSTRLNM` = "' . $nim . '" AND mk.`kd_prodi` = ' . $prodi . '
									')->row();
		}

		return $query;
	}

	function calender_akademik1($klp)
	{
		$active_year = getactyear();
		$query = $this->db->select('*')
			->from('tbl_kalender cal')
			->join('tbl_kalender_dtl dtl', 'cal.kd_kalender = dtl.kd_kalender', 'join')
			->where('cal.flag', 2)
			->where('dtl.kelompok', $klp)
			->like('cal.kd_kalender', $active_year, 'after')
			->get()->result();

		return $query;
	}
	function getMasterbeasiswa()
	{
		$this->db->where('periode', date('Y'));
		$this->db->where('isactivated', '1');
		$this->db->order_by('id_bea', 'asc');
		return $this->db->get('tbl_master_beasiswa');
	}
	function getMasterKkn()
	{
		$this->db->where('isactivated', '1');
		$this->db->order_by('id_master_kkn', 'asc');
		return $this->db->get('tbl_master_kkn');
	}
	function cari_kd($table, $pk, $value)
	{
		$this->db->where($pk, $value);
		$this->db->from($table);
		$this->db->limit(1);
		$query = $this->db->get();
		if ($query->num_rows() == 1) {
			return TRUE;
		} else {
			return FALSE;
		}
	}

	function get_total_sks($npm)
	{
		$kodeProdiMahasiswa = get_mhs_jur($npm);

		$this->db->select('SUM(a.sks_matakuliah) as total_sks');
		$this->db->from('tbl_matakuliah a');
		$this->db->join('tbl_krs b', 'a.kd_matakuliah = b.kd_matakuliah');
		$this->db->where('b.npm_mahasiswa', $npm);
		$this->db->where('a.kd_prodi', $kodeProdiMahasiswa);
		return $this->db->get()->row()->total_sks;
	}

	public function get_app_config($key)
	{
		$app = $this->db->get_where('app_config', ['key' => $key])->row();
		return $app;
	}
}



/* End of file app_model.php */

/* Location: ./application/models/app_model.php */
