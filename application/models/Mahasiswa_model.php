<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mahasiswa_model extends MY_Model {

	public $_table = 'tbl_mahasiswa';

	public $primaryKey = 'id_mhs';

}

/* End of file Mahasiswa_model.php */
/* Location: ./application/models/Mahasiswa_model.php */