<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Semester_fee_model extends CI_Model {

	function get_mahasiswa($prodi, $angkatan, $kelas)
	{
		$this->db->like('NIMHSMSMHS', $angkatan, 'after');
		$this->db->where('KDPSTMSMHS', $prodi);
		$this->db->where('kategori_kelas', $kelas);
		$data = $this->db->get('tbl_mahasiswa')->result();
		return $data;
	}

	function set_fee($set, $angkatan, $prodi, $kelas)
	{
		$this->db->like('NIMHSMSMHS', $angkatan, 'AFTER');
		$this->db->where('KDPSTMSMHS', $prodi);
		$this->db->where('kategori_kelas', $kelas);
		$this->db->update('tbl_mahasiswa', $set);
		return;
	}

}

/* End of file Semester_fee_model.php */
/* Location: ./application/models/Semester_fee_model.php */