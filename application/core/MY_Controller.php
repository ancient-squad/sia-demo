<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MY_Controller extends CI_Controller {

	public $APP_NAME, $ORG_NAME, $URL, $ADDR, $PHONE, $EMAIL, $FAX;

	public function __construct()
	{
		parent::__construct();
		$this->APP_NAME = $this->app_model->get_app_config('APP_NAME')->value;
		$this->ORG_NAME = $this->app_model->get_app_config('ORGANIZATION')->value;
		$this->URL = $this->app_model->get_app_config('URL')->value;
		$this->URLFEEDER = $this->app_model->get_app_config('URLFEEDERLOC')->value;
		$this->ADDR = $this->app_model->get_app_config('ADDR')->value;
		$this->PHONE = $this->app_model->get_app_config('PHONE')->value;
		$this->EMAIL = $this->app_model->get_app_config('EMAIL')->value;
		$this->FAX = $this->app_model->get_app_config('FAX')->value;
		$this->ID_WILAYAH = $this->app_model->get_app_config('ID_WIL_FEEDER')->value;
		$this->ID_PT_FEEDER = $this->app_model->get_app_config('ID_PT_FEEDER')->value;
		$this->KODE_PT_FEEDER = $this->app_model->get_app_config('KODE_PT_FEEDER')->value;
	}

}

/* End of file MY_Controller.php */
/* Location: ./application/core/MY_Controller.php */