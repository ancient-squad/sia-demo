<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Api_response {

	protected $ci;

	public function __construct()
	{
		$this->ci =& get_instance();
	}

	public function header_validation($key)
	{
		if ($key !== PMB_APP_KEY) {
			$response = ['status' => 90, 'message' => 'App key not valid'];
			$this->_create_response(400, $response);
		}
		return;
	}

	/**
	 * Create response for all request
	 * @param int 	$status_code
	 * @param array $response
	 * @return void
	 */
	private function _create_response($status_code, $response)
	{
		$this->ci->output
			->set_status_header($status_code)
			->set_content_type('application/json', 'utf-8')
			->set_output(json_encode($response))
			->_display();
		exit;
	}

	public function success_response($payload)
	{
		$response = ['status' => 1, 'message' => 'success', 'data' => $payload];
		$this->_create_response(200, $response);
	}

	public function empty_response()
	{
		$response = ['status' => 23, 'message' => 'data is empty'];
		$this->_create_response(204,$response);
	}

	/**
	 * To check whether the object has valid property or not
	 * 
	 * @param object 	$class
	 * @param array 	$property
	 * @return bool
	 */
	protected function is_property_exist($class, $property)
	{
		$i = 0;
		foreach ($class as $key => $value) {
			if (!property_exists($class, $property[$i])) {
				$data[] = 'invalid parameter for '.strtoupper($key);
			}
			$i++;
		}

		if (isset($data)) {
			$response = ['status' => 4, 'message' => 'invalid parameter', 'desc' => $data];
			$this->_create_response(400, $response);
		}
		return $this;
	}

	/**
	 * Check the number of parameter should be sent, if invalid then reject
	 * 
	 * @param object 	$prop
	 * @param int 		$total_param
	 * @return void
	 */
	public function is_number_of_property_valid($prop, $total_param)
	{
		if (count((array)$prop) != $total_param) {
			$response = [
				'status' => 4, 
				'message' => 'invalid parameter', 
				'desc' => 'number of parameter is invalid'
			];
			$this->_create_response(400, $response);
		}
		return $this;
	}

	/**
	 * Validation for body request
	 * 
	 * @param object 	$property
	 * @param int 		$allowedNumberOfProperty
	 * @param array  	$allowedProperties
	 * @return bool
	 */
	public function request_validation($property, $allowedNumberOfProperty, array $allowedProperties)
	{
		$this
			->is_number_of_property_valid($property, $allowedNumberOfProperty)
			->is_property_exist($property, $allowedProperties);
		return TRUE;
	}

}

/* End of file Api_response.php */
/* Location: ./application/modules/api/controllers/Api_response.php */