<?php

class Pdf extends FPDF
{

	function tableHeadTransWisuda($y=103, $y2 = 106.2)
	{
		$txt = [
			'Kode',
			'Mata Kuliah',
			'Sks',
			'Nilai'
		];

		$this->setXY(11, $y);
		$this->SetFont('Arial', 'B', 6);
		$this->Cell(10, 10, 'NO', 'L,T,R,B', 0, 'C');

		$this->setXY(21, $y);
		$this->Cell(15, 10, strtoupper($txt[0]).' / ', 'L,T,B', 0, 'C');
		$this->setXY(21, $y2);
		$this->SetFont('Arial', 'BI', 6);
		$this->Cell(15, 10, strtoupper(lang($txt[0])), '', 0, 'C');

		$this->setXY(36.2, $y);
		$this->Cell(135, 10, strtoupper($txt[1]) .' / '. strtoupper(lang($txt[1])), 'L,T,R,B', 0, 'C');

		$this->setXY(171, $y);
		$this->Cell(15, 10, strtoupper($txt[2]).' / ', 'R,T,B', 0, 'C');
		$this->setXY(171, $y2);
		$this->SetFont('Arial', 'BI', 6);
		$this->Cell(15, 10, strtoupper(lang($txt[2])), '', 0, 'C');

		$this->setXY(185.8, $y);
		$this->Cell(15, 10, strtoupper($txt[3]).' / ', 'R,T,B', 0, 'C');
		$this->setXY(186, $y2);
		$this->SetFont('Arial', 'BI', 6);
		$this->Cell(15, 10, strtoupper(lang($txt[3])), '', 1, 'C');

	}

	function Header()
	{
		$this->SetMargins(11, 10, 11);
	
	    $this->Image(base_url().'/assets/logo.gif', 12, 7, 28, 30.6);


		$header1 = [
			'UNIVERSITAS ' . $this->ORG_NAME,
			'Alamat: ' . $this->ADDR,
			'Tlp. '.$this->PHONE.', 7231948 Fax: ' . $this->FAX,
			'website : www.'.$this->URL
		];

		$this->SetFont('Arial', '', 10);
		$i = 0;

		foreach ($header1 as $value) {
			$this->Cell(220, 5, $header1[$i], 0, 0, 'C');
			$this->Ln(5);
			$i++;
		}

		// Line1
		$y = 40;
		$x = 10;
		$x2 = $this->w - $x;
		$this->SetLineWidth(0.8);
		$this->Line($x, $y, $x2, $y);

		// Line2
		$y = 41;
		$x = 15;
		$x2 = $this->w - $x;
		$this->SetLineWidth(0.2);
		$this->Line($x, $y, $x2, $y);
		/*End Header*/
	}


	function Footer()
	{
	    // Go to 1.5 cm from bottom
	    $this->SetY(-15);
	    // Select Arial italic 8
	    $this->SetFont('Arial','I',8);
	    // Print centered page number

	    $this->Cell(0,10,'Academic Transcript of name',0,0,'L');
	    $this->Cell(0,10,'Halaman '.$this->PageNo().' dari {nb} / Page '.$this->PageNo() .' of {nb}',0,0,'R');
	}
}