<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pddikti
{
	protected $ci;

	public function __construct()
	{
        $this->ci =& get_instance();
        $this->URLFEEDER = $this->ci->app_model->get_app_config('URLFEEDERPUB')->value;
	}

	function get_token()
	{
		$username      = userfeeder;
		$password      = passwordfeeder;
		$data          = ['act' => 'GetToken', 'username' => $username, 'password' => $password];
		$result_string = $this->runWS($data);
		return json_decode($result_string)->data->token;
	}

	function runWS($data, $type='json') 
	{
		$url = $this->URLFEEDER.'/ws/live2.php';
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_POST, 1);
		$headers = array();

		if ($type == 'xml')
			$headers[] = 'Content-Type: application/xml';
		else
			$headers[] = 'Content-Type: application/json';
		
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		if ($data) {
			if ($type == 'xml') {
				/* contoh xml:
				<?xml
				version="1.0"?><data><act>GetToken</act><username>agus</username><password>abcdef</password>
				</data>
				*/
				$data = stringXML($data);
			} else {
				/* contoh json:
				{"act":"GetToken","username":"agus","password":"abcdef"}
				*/
				$data = json_encode($data);
			}
			curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
		}
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		$result = curl_exec($ch);
		curl_close($ch);
		return $result;
	}

	function stringXML($data) 
	{
		$xml = new SimpleXMLElement('<?xml version="1.0"?><data></data>');
		array_to_xml($data, $xml);
		return $xml->asXML();
	}

	function array_to_xml( $data, &$xml_data ) 
	{
		foreach( $data as $key => $value ) {
			if( is_array($value) ) {
				$subnode = $xml_data->addChild($key);
				array_to_xml($value, $subnode);
			} else {
				//$xml_data->addChild("$key",htmlspecialchars("$value"));
				$xml_data->addChild("$key",$value);
			}
		}
	}

	function intoTables($rows) 
	{
		$i = 0;
		$str = '<table class="data_grid">';
		foreach ($rows as $row) {
			if (!$i) {
				$str .= '<tr>';
				$str .= '<th>No</th>';
				foreach(array_keys($row) as $k=>$v){
					$str .= '<th>';
					$str .= $v;
					$str .= '</th>';
				}
				$str .= '</tr>';
			}

			$str .= '<tr>';
			$i++;
			$style='';
			foreach($row as $k=>$v){
				if (strtolower($k) == 'soft_delete' && $v == '1') {
					$style='style="text-decoration:line-through"';
				}
			}

			$str .= "<td $style >$i.</td>";
			foreach($row as $k=>$v){
			$str .= "<td $style>";
				if (!is_array($v))
				$str .= $v;
				$str .= '&nbsp;</td>';
			}
			$str .= '</tr>';
		}
		$str .= '</table>';
		return $str;
	}

}

/* End of file pddikti.php */
/* Location: ./application/libraries/pddikti.php */
