function sync(tahunajaran) {
	$.LoadingOverlay('show');
	let xhr = new XMLHttpRequest();
	xhr.open('GET', baseurl + 'sync_feed/migrate_jadwal_mk/transfer/' + tahunajaran);
	xhr.onload = function () {
		if (this.readyState === xhr.DONE) {
			let res = JSON.parse(xhr.responseText);
			if (this.status === 200) {
				if (res.status === 1) {
					document.getElementById('alert-response-status').innerHTML = alertResponse('success', res.message);
					document.getElementById('col-' + tahunajaran).innerHTML = '<span class="label label-success">Sudah Tersedia</span>';
				} else {
					document.getElementById('alert-response-status').innerHTML = alertResponse('info', res.message);
				}
			} else {
				document.getElementById('alert-response-status').innerHTML = alertResponse('danger', res.message);
			}
			$.LoadingOverlay('hide');
		}
	}
	xhr.send();
}

function alertResponse(type, message) {
	let tpl = `<div class="alert alert-${type} alert-dismissable">
		<a href="#" class="close" data-dismiss="alert">&times;</a>
		${message}
	</div>`;
	return tpl;
}